/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.tests;

import es.esi.gemde.methodmanager.methodmodel.method.ExecutableElement;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Executable Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ExecutableElementTest extends TestCase
{

  /**
   * The fixture for this Executable Element test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ExecutableElement fixture = null;

  /**
   * Constructs a new Executable Element test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ExecutableElementTest(String name)
  {
    super(name);
  }

  /**
   * Sets the fixture for this Executable Element test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void setFixture(ExecutableElement fixture)
  {
    this.fixture = fixture;
  }

  /**
   * Returns the fixture for this Executable Element test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ExecutableElement getFixture()
  {
    return fixture;
  }

} //ExecutableElementTest
