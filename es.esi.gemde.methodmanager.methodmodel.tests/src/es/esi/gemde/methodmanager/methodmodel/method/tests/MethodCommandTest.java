/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.tests;

import es.esi.gemde.methodmanager.methodmodel.method.MethodCommand;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Command</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class MethodCommandTest extends TestCase
{

  /**
   * The fixture for this Command test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MethodCommand fixture = null;

  /**
   * Constructs a new Command test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodCommandTest(String name)
  {
    super(name);
  }

  /**
   * Sets the fixture for this Command test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void setFixture(MethodCommand fixture)
  {
    this.fixture = fixture;
  }

  /**
   * Returns the fixture for this Command test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MethodCommand getFixture()
  {
    return fixture;
  }

} //MethodCommandTest
