/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.tests;

import es.esi.gemde.methodmanager.methodmodel.method.MethodFactory;
import es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Select Model Element From File Command</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SelectModelElementFromFileCommandTest extends SimpleCommandTest
{

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static void main(String[] args)
  {
    TestRunner.run(SelectModelElementFromFileCommandTest.class);
  }

  /**
   * Constructs a new Select Model Element From File Command test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SelectModelElementFromFileCommandTest(String name)
  {
    super(name);
  }

  /**
   * Returns the fixture for this Select Model Element From File Command test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected SelectModelElementFromFileCommand getFixture()
  {
    return (SelectModelElementFromFileCommand)fixture;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see junit.framework.TestCase#setUp()
   * @generated
   */
  @Override
  protected void setUp() throws Exception
  {
    setFixture(MethodFactory.eINSTANCE.createSelectModelElementFromFileCommand());
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see junit.framework.TestCase#tearDown()
   * @generated
   */
  @Override
  protected void tearDown() throws Exception
  {
    setFixture(null);
  }

} //SelectModelElementFromFileCommandTest
