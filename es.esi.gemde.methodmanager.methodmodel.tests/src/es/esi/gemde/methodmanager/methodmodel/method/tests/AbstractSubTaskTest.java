/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.tests;

import es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Abstract Sub Task</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class AbstractSubTaskTest extends MethodStepTest
{

  /**
   * Constructs a new Abstract Sub Task test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AbstractSubTaskTest(String name)
  {
    super(name);
  }

  /**
   * Returns the fixture for this Abstract Sub Task test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected AbstractSubTask getFixture()
  {
    return (AbstractSubTask)fixture;
  }

} //AbstractSubTaskTest
