/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.tests;

import es.esi.gemde.methodmanager.methodmodel.method.CommandParameter;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Command Parameter</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class CommandParameterTest extends TestCase
{

  /**
   * The fixture for this Command Parameter test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CommandParameter fixture = null;

  /**
   * Constructs a new Command Parameter test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameterTest(String name)
  {
    super(name);
  }

  /**
   * Sets the fixture for this Command Parameter test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void setFixture(CommandParameter fixture)
  {
    this.fixture = fixture;
  }

  /**
   * Returns the fixture for this Command Parameter test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CommandParameter getFixture()
  {
    return fixture;
  }

} //CommandParameterTest
