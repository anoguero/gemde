/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.tests;

import es.esi.gemde.methodmanager.methodmodel.method.MethodStep;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Step</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class MethodStepTest extends TestCase
{

  /**
   * The fixture for this Step test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MethodStep fixture = null;

  /**
   * Constructs a new Step test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodStepTest(String name)
  {
    super(name);
  }

  /**
   * Sets the fixture for this Step test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void setFixture(MethodStep fixture)
  {
    this.fixture = fixture;
  }

  /**
   * Returns the fixture for this Step test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MethodStep getFixture()
  {
    return fixture;
  }

} //MethodStepTest
