/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.tests;

import es.esi.gemde.methodmanager.methodmodel.method.ConditionalSubTask;
import es.esi.gemde.methodmanager.methodmodel.method.MethodFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Conditional Sub Task</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ConditionalSubTaskTest extends AbstractSubTaskTest
{

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static void main(String[] args)
  {
    TestRunner.run(ConditionalSubTaskTest.class);
  }

  /**
   * Constructs a new Conditional Sub Task test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConditionalSubTaskTest(String name)
  {
    super(name);
  }

  /**
   * Returns the fixture for this Conditional Sub Task test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected ConditionalSubTask getFixture()
  {
    return (ConditionalSubTask)fixture;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see junit.framework.TestCase#setUp()
   * @generated
   */
  @Override
  protected void setUp() throws Exception
  {
    setFixture(MethodFactory.eINSTANCE.createConditionalSubTask());
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see junit.framework.TestCase#tearDown()
   * @generated
   */
  @Override
  protected void tearDown() throws Exception
  {
    setFixture(null);
  }

} //ConditionalSubTaskTest
