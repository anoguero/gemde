/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.tests;

import es.esi.gemde.methodmanager.methodmodel.method.DataStorageElement;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Data Storage Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class DataStorageElementTest extends TestCase
{

  /**
   * The fixture for this Data Storage Element test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DataStorageElement fixture = null;

  /**
   * Constructs a new Data Storage Element test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DataStorageElementTest(String name)
  {
    super(name);
  }

  /**
   * Sets the fixture for this Data Storage Element test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void setFixture(DataStorageElement fixture)
  {
    this.fixture = fixture;
  }

  /**
   * Returns the fixture for this Data Storage Element test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected DataStorageElement getFixture()
  {
    return fixture;
  }

} //DataStorageElementTest
