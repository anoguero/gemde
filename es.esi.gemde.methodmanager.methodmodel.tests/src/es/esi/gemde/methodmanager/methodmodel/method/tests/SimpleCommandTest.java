/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.tests;

import es.esi.gemde.methodmanager.methodmodel.method.SimpleCommand;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Simple Command</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class SimpleCommandTest extends MethodCommandTest
{

  /**
   * Constructs a new Simple Command test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SimpleCommandTest(String name)
  {
    super(name);
  }

  /**
   * Returns the fixture for this Simple Command test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected SimpleCommand getFixture()
  {
    return (SimpleCommand)fixture;
  }

} //SimpleCommandTest
