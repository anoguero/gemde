/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.tests;

import es.esi.gemde.methodmanager.methodmodel.method.MethodPart;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Part</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class MethodPartTest extends MethodStepTest
{

  /**
   * Constructs a new Part test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodPartTest(String name)
  {
    super(name);
  }

  /**
   * Returns the fixture for this Part test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected MethodPart getFixture()
  {
    return (MethodPart)fixture;
  }

} //MethodPartTest
