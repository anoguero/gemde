/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.tests;

import es.esi.gemde.methodmanager.methodmodel.method.MethodFactory;
import es.esi.gemde.methodmanager.methodmodel.method.SelectMetamodelCommand;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Select Metamodel Command</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class SelectMetamodelCommandTest extends SimpleCommandTest
{

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static void main(String[] args)
  {
    TestRunner.run(SelectMetamodelCommandTest.class);
  }

  /**
   * Constructs a new Select Metamodel Command test case with the given name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SelectMetamodelCommandTest(String name)
  {
    super(name);
  }

  /**
   * Returns the fixture for this Select Metamodel Command test case.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected SelectMetamodelCommand getFixture()
  {
    return (SelectMetamodelCommand)fixture;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see junit.framework.TestCase#setUp()
   * @generated
   */
  @Override
  protected void setUp() throws Exception
  {
    setFixture(MethodFactory.eINSTANCE.createSelectMetamodelCommand());
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see junit.framework.TestCase#tearDown()
   * @generated
   */
  @Override
  protected void tearDown() throws Exception
  {
    setFixture(null);
  }

} //SelectMetamodelCommandTest
