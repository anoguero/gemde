/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.util;

import es.esi.gemde.vv.mast.mastmodel.*;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage
 * @generated
 */
public class ModelSwitch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ModelPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelSwitch() {
		if (modelPackage == null) {
			modelPackage = ModelPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ModelPackage.ACTIVITY: {
				Activity activity = (Activity)theEObject;
				T result = caseActivity(activity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.ALARM_CLOCK_SYSTEM_TIMER: {
				Alarm_Clock_System_Timer alarm_Clock_System_Timer = (Alarm_Clock_System_Timer)theEObject;
				T result = caseAlarm_Clock_System_Timer(alarm_Clock_System_Timer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.BARRIER: {
				Barrier barrier = (Barrier)theEObject;
				T result = caseBarrier(barrier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.BURSTY_EXTERNAL_EVENT: {
				Bursty_External_Event bursty_External_Event = (Bursty_External_Event)theEObject;
				T result = caseBursty_External_Event(bursty_External_Event);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.CHARACTER_PACKET_DRIVER: {
				Character_Packet_Driver character_Packet_Driver = (Character_Packet_Driver)theEObject;
				T result = caseCharacter_Packet_Driver(character_Packet_Driver);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.COMPOSITE_OPERATION: {
				Composite_Operation composite_Operation = (Composite_Operation)theEObject;
				T result = caseComposite_Operation(composite_Operation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.COMPOSITE_TIMING_REQUIREMENT: {
				Composite_Timing_Requirement composite_Timing_Requirement = (Composite_Timing_Requirement)theEObject;
				T result = caseComposite_Timing_Requirement(composite_Timing_Requirement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.CONCENTRATOR: {
				Concentrator concentrator = (Concentrator)theEObject;
				T result = caseConcentrator(concentrator);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.DELAY: {
				Delay delay = (Delay)theEObject;
				T result = caseDelay(delay);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.DELIVERY_SERVER: {
				Delivery_Server delivery_Server = (Delivery_Server)theEObject;
				T result = caseDelivery_Server(delivery_Server);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.DOCUMENT_ROOT: {
				Document_Root document_Root = (Document_Root)theEObject;
				T result = caseDocument_Root(document_Root);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.EDF_POLICY: {
				EDF_Policy edF_Policy = (EDF_Policy)theEObject;
				T result = caseEDF_Policy(edF_Policy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.EDF_SCHEDULER: {
				EDF_Scheduler edF_Scheduler = (EDF_Scheduler)theEObject;
				T result = caseEDF_Scheduler(edF_Scheduler);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.ENCLOSING_OPERATION: {
				Enclosing_Operation enclosing_Operation = (Enclosing_Operation)theEObject;
				T result = caseEnclosing_Operation(enclosing_Operation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.FIXED_PRIORITY_POLICY: {
				Fixed_Priority_Policy fixed_Priority_Policy = (Fixed_Priority_Policy)theEObject;
				T result = caseFixed_Priority_Policy(fixed_Priority_Policy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.FIXED_PRIORITY_SCHEDULER: {
				Fixed_Priority_Scheduler fixed_Priority_Scheduler = (Fixed_Priority_Scheduler)theEObject;
				T result = caseFixed_Priority_Scheduler(fixed_Priority_Scheduler);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.FP_PACKET_BASED_SCHEDULER: {
				FP_Packet_Based_Scheduler fP_Packet_Based_Scheduler = (FP_Packet_Based_Scheduler)theEObject;
				T result = caseFP_Packet_Based_Scheduler(fP_Packet_Based_Scheduler);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.GLOBAL_MAX_MISS_RATIO: {
				Global_Max_Miss_Ratio global_Max_Miss_Ratio = (Global_Max_Miss_Ratio)theEObject;
				T result = caseGlobal_Max_Miss_Ratio(global_Max_Miss_Ratio);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.HARD_GLOBAL_DEADLINE: {
				Hard_Global_Deadline hard_Global_Deadline = (Hard_Global_Deadline)theEObject;
				T result = caseHard_Global_Deadline(hard_Global_Deadline);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.HARD_LOCAL_DEADLINE: {
				Hard_Local_Deadline hard_Local_Deadline = (Hard_Local_Deadline)theEObject;
				T result = caseHard_Local_Deadline(hard_Local_Deadline);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.IMMEDIATE_CEILING_RESOURCE: {
				Immediate_Ceiling_Resource immediate_Ceiling_Resource = (Immediate_Ceiling_Resource)theEObject;
				T result = caseImmediate_Ceiling_Resource(immediate_Ceiling_Resource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.INTERRUPT_FP_POLICY: {
				Interrupt_FP_Policy interrupt_FP_Policy = (Interrupt_FP_Policy)theEObject;
				T result = caseInterrupt_FP_Policy(interrupt_FP_Policy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.LIST_OF_DRIVERS: {
				List_of_Drivers list_of_Drivers = (List_of_Drivers)theEObject;
				T result = caseList_of_Drivers(list_of_Drivers);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.LOCAL_MAX_MISS_RATIO: {
				Local_Max_Miss_Ratio local_Max_Miss_Ratio = (Local_Max_Miss_Ratio)theEObject;
				T result = caseLocal_Max_Miss_Ratio(local_Max_Miss_Ratio);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.MAST_MODEL: {
				MAST_MODEL masT_MODEL = (MAST_MODEL)theEObject;
				T result = caseMAST_MODEL(masT_MODEL);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.MAX_OUTPUT_JITTER_REQ: {
				Max_Output_Jitter_Req max_Output_Jitter_Req = (Max_Output_Jitter_Req)theEObject;
				T result = caseMax_Output_Jitter_Req(max_Output_Jitter_Req);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.MESSAGE_TRANSMISSION: {
				Message_Transmission message_Transmission = (Message_Transmission)theEObject;
				T result = caseMessage_Transmission(message_Transmission);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.MULTICAST: {
				Multicast multicast = (Multicast)theEObject;
				T result = caseMulticast(multicast);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.NON_PREEMPTIBLE_FP_POLICY: {
				Non_Preemptible_FP_Policy non_Preemptible_FP_Policy = (Non_Preemptible_FP_Policy)theEObject;
				T result = caseNon_Preemptible_FP_Policy(non_Preemptible_FP_Policy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.OFFSET: {
				Offset offset = (Offset)theEObject;
				T result = caseOffset(offset);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.OVERRIDDEN_FIXED_PRIORITY: {
				Overridden_Fixed_Priority overridden_Fixed_Priority = (Overridden_Fixed_Priority)theEObject;
				T result = caseOverridden_Fixed_Priority(overridden_Fixed_Priority);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.OVERRIDDEN_PERMANENT_FP: {
				Overridden_Permanent_FP overridden_Permanent_FP = (Overridden_Permanent_FP)theEObject;
				T result = caseOverridden_Permanent_FP(overridden_Permanent_FP);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.PACKET_BASED_NETWORK: {
				Packet_Based_Network packet_Based_Network = (Packet_Based_Network)theEObject;
				T result = casePacket_Based_Network(packet_Based_Network);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.PACKET_DRIVER: {
				Packet_Driver packet_Driver = (Packet_Driver)theEObject;
				T result = casePacket_Driver(packet_Driver);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.PERIODIC_EXTERNAL_EVENT: {
				Periodic_External_Event periodic_External_Event = (Periodic_External_Event)theEObject;
				T result = casePeriodic_External_Event(periodic_External_Event);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.POLLING_POLICY: {
				Polling_Policy polling_Policy = (Polling_Policy)theEObject;
				T result = casePolling_Policy(polling_Policy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.PRIMARY_SCHEDULER: {
				Primary_Scheduler primary_Scheduler = (Primary_Scheduler)theEObject;
				T result = casePrimary_Scheduler(primary_Scheduler);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.PRIORITY_INHERITANCE_RESOURCE: {
				Priority_Inheritance_Resource priority_Inheritance_Resource = (Priority_Inheritance_Resource)theEObject;
				T result = casePriority_Inheritance_Resource(priority_Inheritance_Resource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.QUERY_SERVER: {
				Query_Server query_Server = (Query_Server)theEObject;
				T result = caseQuery_Server(query_Server);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.RATE_DIVISOR: {
				Rate_Divisor rate_Divisor = (Rate_Divisor)theEObject;
				T result = caseRate_Divisor(rate_Divisor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.REGULAR_EVENT: {
				Regular_Event regular_Event = (Regular_Event)theEObject;
				T result = caseRegular_Event(regular_Event);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.REGULAR_PROCESSOR: {
				Regular_Processor regular_Processor = (Regular_Processor)theEObject;
				T result = caseRegular_Processor(regular_Processor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.REGULAR_SCHEDULING_SERVER: {
				Regular_Scheduling_Server regular_Scheduling_Server = (Regular_Scheduling_Server)theEObject;
				T result = caseRegular_Scheduling_Server(regular_Scheduling_Server);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.REGULAR_TRANSACTION: {
				Regular_Transaction regular_Transaction = (Regular_Transaction)theEObject;
				T result = caseRegular_Transaction(regular_Transaction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.RTEP_PACKET_DRIVER: {
				RTEP_PacketDriver rteP_PacketDriver = (RTEP_PacketDriver)theEObject;
				T result = caseRTEP_PacketDriver(rteP_PacketDriver);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.SECONDARY_SCHEDULER: {
				Secondary_Scheduler secondary_Scheduler = (Secondary_Scheduler)theEObject;
				T result = caseSecondary_Scheduler(secondary_Scheduler);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.SIMPLE_OPERATION: {
				Simple_Operation simple_Operation = (Simple_Operation)theEObject;
				T result = caseSimple_Operation(simple_Operation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.SINGULAR_EXTERNAL_EVENT: {
				Singular_External_Event singular_External_Event = (Singular_External_Event)theEObject;
				T result = caseSingular_External_Event(singular_External_Event);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.SOFT_GLOBAL_DEADLINE: {
				Soft_Global_Deadline soft_Global_Deadline = (Soft_Global_Deadline)theEObject;
				T result = caseSoft_Global_Deadline(soft_Global_Deadline);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.SOFT_LOCAL_DEADLINE: {
				Soft_Local_Deadline soft_Local_Deadline = (Soft_Local_Deadline)theEObject;
				T result = caseSoft_Local_Deadline(soft_Local_Deadline);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.SPORADIC_EXTERNAL_EVENT: {
				Sporadic_External_Event sporadic_External_Event = (Sporadic_External_Event)theEObject;
				T result = caseSporadic_External_Event(sporadic_External_Event);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.SPORADIC_SERVER_POLICY: {
				Sporadic_Server_Policy sporadic_Server_Policy = (Sporadic_Server_Policy)theEObject;
				T result = caseSporadic_Server_Policy(sporadic_Server_Policy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.SRP_PARAMETERS: {
				SRP_Parameters srP_Parameters = (SRP_Parameters)theEObject;
				T result = caseSRP_Parameters(srP_Parameters);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.SRP_RESOURCE: {
				SRP_Resource srP_Resource = (SRP_Resource)theEObject;
				T result = caseSRP_Resource(srP_Resource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.SYSTEM_TIMED_ACTIVITY: {
				System_Timed_Activity system_Timed_Activity = (System_Timed_Activity)theEObject;
				T result = caseSystem_Timed_Activity(system_Timed_Activity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.TICKER_SYSTEM_TIMER: {
				Ticker_System_Timer ticker_System_Timer = (Ticker_System_Timer)theEObject;
				T result = caseTicker_System_Timer(ticker_System_Timer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModelPackage.UNBOUNDED_EXTERNAL_EVENT: {
				Unbounded_External_Event unbounded_External_Event = (Unbounded_External_Event)theEObject;
				T result = caseUnbounded_External_Event(unbounded_External_Event);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Activity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Activity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActivity(Activity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Alarm Clock System Timer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Alarm Clock System Timer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAlarm_Clock_System_Timer(Alarm_Clock_System_Timer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Barrier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Barrier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBarrier(Barrier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bursty External Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bursty External Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBursty_External_Event(Bursty_External_Event object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Character Packet Driver</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Character Packet Driver</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCharacter_Packet_Driver(Character_Packet_Driver object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Composite Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Composite Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComposite_Operation(Composite_Operation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Composite Timing Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Composite Timing Requirement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComposite_Timing_Requirement(Composite_Timing_Requirement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Concentrator</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Concentrator</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConcentrator(Concentrator object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Delay</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Delay</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDelay(Delay object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Delivery Server</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Delivery Server</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDelivery_Server(Delivery_Server object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDocument_Root(Document_Root object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EDF Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EDF Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEDF_Policy(EDF_Policy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EDF Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EDF Scheduler</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEDF_Scheduler(EDF_Scheduler object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Enclosing Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Enclosing Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnclosing_Operation(Enclosing_Operation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fixed Priority Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fixed Priority Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFixed_Priority_Policy(Fixed_Priority_Policy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fixed Priority Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fixed Priority Scheduler</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFixed_Priority_Scheduler(Fixed_Priority_Scheduler object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>FP Packet Based Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>FP Packet Based Scheduler</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFP_Packet_Based_Scheduler(FP_Packet_Based_Scheduler object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Global Max Miss Ratio</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Global Max Miss Ratio</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGlobal_Max_Miss_Ratio(Global_Max_Miss_Ratio object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hard Global Deadline</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hard Global Deadline</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHard_Global_Deadline(Hard_Global_Deadline object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hard Local Deadline</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hard Local Deadline</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHard_Local_Deadline(Hard_Local_Deadline object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Immediate Ceiling Resource</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Immediate Ceiling Resource</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImmediate_Ceiling_Resource(Immediate_Ceiling_Resource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interrupt FP Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interrupt FP Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInterrupt_FP_Policy(Interrupt_FP_Policy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>List of Drivers</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>List of Drivers</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseList_of_Drivers(List_of_Drivers object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Local Max Miss Ratio</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Local Max Miss Ratio</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLocal_Max_Miss_Ratio(Local_Max_Miss_Ratio object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MAST MODEL</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MAST MODEL</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMAST_MODEL(MAST_MODEL object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Max Output Jitter Req</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Max Output Jitter Req</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMax_Output_Jitter_Req(Max_Output_Jitter_Req object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Message Transmission</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Message Transmission</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMessage_Transmission(Message_Transmission object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Multicast</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Multicast</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMulticast(Multicast object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Non Preemptible FP Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Non Preemptible FP Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNon_Preemptible_FP_Policy(Non_Preemptible_FP_Policy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Offset</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Offset</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOffset(Offset object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Overridden Fixed Priority</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Overridden Fixed Priority</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOverridden_Fixed_Priority(Overridden_Fixed_Priority object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Overridden Permanent FP</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Overridden Permanent FP</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOverridden_Permanent_FP(Overridden_Permanent_FP object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Packet Based Network</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Packet Based Network</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePacket_Based_Network(Packet_Based_Network object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Packet Driver</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Packet Driver</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePacket_Driver(Packet_Driver object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Periodic External Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Periodic External Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePeriodic_External_Event(Periodic_External_Event object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Polling Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Polling Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePolling_Policy(Polling_Policy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Primary Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Primary Scheduler</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePrimary_Scheduler(Primary_Scheduler object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Priority Inheritance Resource</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Priority Inheritance Resource</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePriority_Inheritance_Resource(Priority_Inheritance_Resource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Query Server</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Query Server</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQuery_Server(Query_Server object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rate Divisor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rate Divisor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRate_Divisor(Rate_Divisor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Regular Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Regular Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRegular_Event(Regular_Event object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Regular Processor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Regular Processor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRegular_Processor(Regular_Processor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Regular Scheduling Server</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Regular Scheduling Server</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRegular_Scheduling_Server(Regular_Scheduling_Server object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Regular Transaction</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Regular Transaction</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRegular_Transaction(Regular_Transaction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>RTEP Packet Driver</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>RTEP Packet Driver</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRTEP_PacketDriver(RTEP_PacketDriver object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Secondary Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Secondary Scheduler</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSecondary_Scheduler(Secondary_Scheduler object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimple_Operation(Simple_Operation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Singular External Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Singular External Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSingular_External_Event(Singular_External_Event object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Soft Global Deadline</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Soft Global Deadline</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSoft_Global_Deadline(Soft_Global_Deadline object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Soft Local Deadline</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Soft Local Deadline</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSoft_Local_Deadline(Soft_Local_Deadline object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sporadic External Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sporadic External Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSporadic_External_Event(Sporadic_External_Event object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sporadic Server Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sporadic Server Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSporadic_Server_Policy(Sporadic_Server_Policy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SRP Parameters</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SRP Parameters</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSRP_Parameters(SRP_Parameters object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SRP Resource</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SRP Resource</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSRP_Resource(SRP_Resource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>System Timed Activity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>System Timed Activity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSystem_Timed_Activity(System_Timed_Activity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ticker System Timer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ticker System Timer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTicker_System_Timer(Ticker_System_Timer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Unbounded External Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Unbounded External Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUnbounded_External_Event(Unbounded_External_Event object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} //ModelSwitch
