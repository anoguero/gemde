/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Query_Server;
import es.esi.gemde.vv.mast.mastmodel.Request_Policy;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Query Server</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Query_ServerImpl#getInput_Event <em>Input Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Query_ServerImpl#getOutput_Events_List <em>Output Events List</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Query_ServerImpl#getRequest_Policy <em>Request Policy</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Query_ServerImpl extends EObjectImpl implements Query_Server {
	/**
	 * The default value of the '{@link #getInput_Event() <em>Input Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInput_Event()
	 * @generated
	 * @ordered
	 */
	protected static final String INPUT_EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInput_Event() <em>Input Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInput_Event()
	 * @generated
	 * @ordered
	 */
	protected String input_Event = INPUT_EVENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getOutput_Events_List() <em>Output Events List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutput_Events_List()
	 * @generated
	 * @ordered
	 */
	protected static final List<String> OUTPUT_EVENTS_LIST_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOutput_Events_List() <em>Output Events List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutput_Events_List()
	 * @generated
	 * @ordered
	 */
	protected List<String> output_Events_List = OUTPUT_EVENTS_LIST_EDEFAULT;

	/**
	 * The default value of the '{@link #getRequest_Policy() <em>Request Policy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequest_Policy()
	 * @generated
	 * @ordered
	 */
	protected static final Request_Policy REQUEST_POLICY_EDEFAULT = Request_Policy.PRIORITY;

	/**
	 * The cached value of the '{@link #getRequest_Policy() <em>Request Policy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequest_Policy()
	 * @generated
	 * @ordered
	 */
	protected Request_Policy request_Policy = REQUEST_POLICY_EDEFAULT;

	/**
	 * This is true if the Request Policy attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean request_PolicyESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Query_ServerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.QUERY_SERVER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInput_Event() {
		return input_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInput_Event(String newInput_Event) {
		String oldInput_Event = input_Event;
		input_Event = newInput_Event;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.QUERY_SERVER__INPUT_EVENT, oldInput_Event, input_Event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<String> getOutput_Events_List() {
		return output_Events_List;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutput_Events_List(List<String> newOutput_Events_List) {
		List<String> oldOutput_Events_List = output_Events_List;
		output_Events_List = newOutput_Events_List;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.QUERY_SERVER__OUTPUT_EVENTS_LIST, oldOutput_Events_List, output_Events_List));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Request_Policy getRequest_Policy() {
		return request_Policy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequest_Policy(Request_Policy newRequest_Policy) {
		Request_Policy oldRequest_Policy = request_Policy;
		request_Policy = newRequest_Policy == null ? REQUEST_POLICY_EDEFAULT : newRequest_Policy;
		boolean oldRequest_PolicyESet = request_PolicyESet;
		request_PolicyESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.QUERY_SERVER__REQUEST_POLICY, oldRequest_Policy, request_Policy, !oldRequest_PolicyESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetRequest_Policy() {
		Request_Policy oldRequest_Policy = request_Policy;
		boolean oldRequest_PolicyESet = request_PolicyESet;
		request_Policy = REQUEST_POLICY_EDEFAULT;
		request_PolicyESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ModelPackage.QUERY_SERVER__REQUEST_POLICY, oldRequest_Policy, REQUEST_POLICY_EDEFAULT, oldRequest_PolicyESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetRequest_Policy() {
		return request_PolicyESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.QUERY_SERVER__INPUT_EVENT:
				return getInput_Event();
			case ModelPackage.QUERY_SERVER__OUTPUT_EVENTS_LIST:
				return getOutput_Events_List();
			case ModelPackage.QUERY_SERVER__REQUEST_POLICY:
				return getRequest_Policy();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.QUERY_SERVER__INPUT_EVENT:
				setInput_Event((String)newValue);
				return;
			case ModelPackage.QUERY_SERVER__OUTPUT_EVENTS_LIST:
				setOutput_Events_List((List<String>)newValue);
				return;
			case ModelPackage.QUERY_SERVER__REQUEST_POLICY:
				setRequest_Policy((Request_Policy)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.QUERY_SERVER__INPUT_EVENT:
				setInput_Event(INPUT_EVENT_EDEFAULT);
				return;
			case ModelPackage.QUERY_SERVER__OUTPUT_EVENTS_LIST:
				setOutput_Events_List(OUTPUT_EVENTS_LIST_EDEFAULT);
				return;
			case ModelPackage.QUERY_SERVER__REQUEST_POLICY:
				unsetRequest_Policy();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.QUERY_SERVER__INPUT_EVENT:
				return INPUT_EVENT_EDEFAULT == null ? input_Event != null : !INPUT_EVENT_EDEFAULT.equals(input_Event);
			case ModelPackage.QUERY_SERVER__OUTPUT_EVENTS_LIST:
				return OUTPUT_EVENTS_LIST_EDEFAULT == null ? output_Events_List != null : !OUTPUT_EVENTS_LIST_EDEFAULT.equals(output_Events_List);
			case ModelPackage.QUERY_SERVER__REQUEST_POLICY:
				return isSetRequest_Policy();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Input_Event: ");
		result.append(input_Event);
		result.append(", Output_Events_List: ");
		result.append(output_Events_List);
		result.append(", Request_Policy: ");
		if (request_PolicyESet) result.append(request_Policy); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //Query_ServerImpl
