/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Immediate Ceiling Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource#getCeiling <em>Ceiling</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource#getName <em>Name</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource#getPreassigned <em>Preassigned</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getImmediate_Ceiling_Resource()
 * @model extendedMetaData="name='Immediate_Ceiling_Resource' kind='empty'"
 * @generated
 */
public interface Immediate_Ceiling_Resource extends EObject {
	/**
	 * Returns the value of the '<em><b>Ceiling</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ceiling</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ceiling</em>' attribute.
	 * @see #setCeiling(int)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getImmediate_Ceiling_Resource_Ceiling()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Priority"
	 *        extendedMetaData="kind='attribute' name='Ceiling'"
	 * @generated
	 */
	int getCeiling();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource#getCeiling <em>Ceiling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ceiling</em>' attribute.
	 * @see #getCeiling()
	 * @generated
	 */
	void setCeiling(int value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getImmediate_Ceiling_Resource_Name()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier" required="true"
	 *        extendedMetaData="kind='attribute' name='Name'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Preassigned</b></em>' attribute.
	 * The literals are from the enumeration {@link es.esi.gemde.vv.mast.mastmodel.Assertion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preassigned</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preassigned</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Assertion
	 * @see #isSetPreassigned()
	 * @see #unsetPreassigned()
	 * @see #setPreassigned(Assertion)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getImmediate_Ceiling_Resource_Preassigned()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='Preassigned'"
	 * @generated
	 */
	Assertion getPreassigned();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource#getPreassigned <em>Preassigned</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preassigned</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Assertion
	 * @see #isSetPreassigned()
	 * @see #unsetPreassigned()
	 * @see #getPreassigned()
	 * @generated
	 */
	void setPreassigned(Assertion value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource#getPreassigned <em>Preassigned</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPreassigned()
	 * @see #getPreassigned()
	 * @see #setPreassigned(Assertion)
	 * @generated
	 */
	void unsetPreassigned();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource#getPreassigned <em>Preassigned</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Preassigned</em>' attribute is set.
	 * @see #unsetPreassigned()
	 * @see #getPreassigned()
	 * @see #setPreassigned(Assertion)
	 * @generated
	 */
	boolean isSetPreassigned();

} // Immediate_Ceiling_Resource
