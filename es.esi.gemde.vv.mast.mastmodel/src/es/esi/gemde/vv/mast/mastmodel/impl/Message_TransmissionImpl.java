/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Message_Transmission;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Overridden_Fixed_Priority;
import es.esi.gemde.vv.mast.mastmodel.Overridden_Permanent_FP;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Message Transmission</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Message_TransmissionImpl#getOverridden_Fixed_Priority <em>Overridden Fixed Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Message_TransmissionImpl#getOverridden_Permanent_FP <em>Overridden Permanent FP</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Message_TransmissionImpl#getAvg_Message_Size <em>Avg Message Size</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Message_TransmissionImpl#getMax_Message_Size <em>Max Message Size</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Message_TransmissionImpl#getMin_Message_Size <em>Min Message Size</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Message_TransmissionImpl#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Message_TransmissionImpl extends EObjectImpl implements Message_Transmission {
	/**
	 * The cached value of the '{@link #getOverridden_Fixed_Priority() <em>Overridden Fixed Priority</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverridden_Fixed_Priority()
	 * @generated
	 * @ordered
	 */
	protected Overridden_Fixed_Priority overridden_Fixed_Priority;

	/**
	 * The cached value of the '{@link #getOverridden_Permanent_FP() <em>Overridden Permanent FP</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverridden_Permanent_FP()
	 * @generated
	 * @ordered
	 */
	protected Overridden_Permanent_FP overridden_Permanent_FP;

	/**
	 * The default value of the '{@link #getAvg_Message_Size() <em>Avg Message Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvg_Message_Size()
	 * @generated
	 * @ordered
	 */
	protected static final double AVG_MESSAGE_SIZE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getAvg_Message_Size() <em>Avg Message Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvg_Message_Size()
	 * @generated
	 * @ordered
	 */
	protected double avg_Message_Size = AVG_MESSAGE_SIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMax_Message_Size() <em>Max Message Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_Message_Size()
	 * @generated
	 * @ordered
	 */
	protected static final double MAX_MESSAGE_SIZE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getMax_Message_Size() <em>Max Message Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_Message_Size()
	 * @generated
	 * @ordered
	 */
	protected double max_Message_Size = MAX_MESSAGE_SIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMin_Message_Size() <em>Min Message Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin_Message_Size()
	 * @generated
	 * @ordered
	 */
	protected static final double MIN_MESSAGE_SIZE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getMin_Message_Size() <em>Min Message Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin_Message_Size()
	 * @generated
	 * @ordered
	 */
	protected double min_Message_Size = MIN_MESSAGE_SIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Message_TransmissionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.MESSAGE_TRANSMISSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Overridden_Fixed_Priority getOverridden_Fixed_Priority() {
		return overridden_Fixed_Priority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOverridden_Fixed_Priority(Overridden_Fixed_Priority newOverridden_Fixed_Priority, NotificationChain msgs) {
		Overridden_Fixed_Priority oldOverridden_Fixed_Priority = overridden_Fixed_Priority;
		overridden_Fixed_Priority = newOverridden_Fixed_Priority;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.MESSAGE_TRANSMISSION__OVERRIDDEN_FIXED_PRIORITY, oldOverridden_Fixed_Priority, newOverridden_Fixed_Priority);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOverridden_Fixed_Priority(Overridden_Fixed_Priority newOverridden_Fixed_Priority) {
		if (newOverridden_Fixed_Priority != overridden_Fixed_Priority) {
			NotificationChain msgs = null;
			if (overridden_Fixed_Priority != null)
				msgs = ((InternalEObject)overridden_Fixed_Priority).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.MESSAGE_TRANSMISSION__OVERRIDDEN_FIXED_PRIORITY, null, msgs);
			if (newOverridden_Fixed_Priority != null)
				msgs = ((InternalEObject)newOverridden_Fixed_Priority).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.MESSAGE_TRANSMISSION__OVERRIDDEN_FIXED_PRIORITY, null, msgs);
			msgs = basicSetOverridden_Fixed_Priority(newOverridden_Fixed_Priority, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.MESSAGE_TRANSMISSION__OVERRIDDEN_FIXED_PRIORITY, newOverridden_Fixed_Priority, newOverridden_Fixed_Priority));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Overridden_Permanent_FP getOverridden_Permanent_FP() {
		return overridden_Permanent_FP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOverridden_Permanent_FP(Overridden_Permanent_FP newOverridden_Permanent_FP, NotificationChain msgs) {
		Overridden_Permanent_FP oldOverridden_Permanent_FP = overridden_Permanent_FP;
		overridden_Permanent_FP = newOverridden_Permanent_FP;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.MESSAGE_TRANSMISSION__OVERRIDDEN_PERMANENT_FP, oldOverridden_Permanent_FP, newOverridden_Permanent_FP);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOverridden_Permanent_FP(Overridden_Permanent_FP newOverridden_Permanent_FP) {
		if (newOverridden_Permanent_FP != overridden_Permanent_FP) {
			NotificationChain msgs = null;
			if (overridden_Permanent_FP != null)
				msgs = ((InternalEObject)overridden_Permanent_FP).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.MESSAGE_TRANSMISSION__OVERRIDDEN_PERMANENT_FP, null, msgs);
			if (newOverridden_Permanent_FP != null)
				msgs = ((InternalEObject)newOverridden_Permanent_FP).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.MESSAGE_TRANSMISSION__OVERRIDDEN_PERMANENT_FP, null, msgs);
			msgs = basicSetOverridden_Permanent_FP(newOverridden_Permanent_FP, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.MESSAGE_TRANSMISSION__OVERRIDDEN_PERMANENT_FP, newOverridden_Permanent_FP, newOverridden_Permanent_FP));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getAvg_Message_Size() {
		return avg_Message_Size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAvg_Message_Size(double newAvg_Message_Size) {
		double oldAvg_Message_Size = avg_Message_Size;
		avg_Message_Size = newAvg_Message_Size;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.MESSAGE_TRANSMISSION__AVG_MESSAGE_SIZE, oldAvg_Message_Size, avg_Message_Size));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getMax_Message_Size() {
		return max_Message_Size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMax_Message_Size(double newMax_Message_Size) {
		double oldMax_Message_Size = max_Message_Size;
		max_Message_Size = newMax_Message_Size;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.MESSAGE_TRANSMISSION__MAX_MESSAGE_SIZE, oldMax_Message_Size, max_Message_Size));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getMin_Message_Size() {
		return min_Message_Size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMin_Message_Size(double newMin_Message_Size) {
		double oldMin_Message_Size = min_Message_Size;
		min_Message_Size = newMin_Message_Size;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.MESSAGE_TRANSMISSION__MIN_MESSAGE_SIZE, oldMin_Message_Size, min_Message_Size));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.MESSAGE_TRANSMISSION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelPackage.MESSAGE_TRANSMISSION__OVERRIDDEN_FIXED_PRIORITY:
				return basicSetOverridden_Fixed_Priority(null, msgs);
			case ModelPackage.MESSAGE_TRANSMISSION__OVERRIDDEN_PERMANENT_FP:
				return basicSetOverridden_Permanent_FP(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.MESSAGE_TRANSMISSION__OVERRIDDEN_FIXED_PRIORITY:
				return getOverridden_Fixed_Priority();
			case ModelPackage.MESSAGE_TRANSMISSION__OVERRIDDEN_PERMANENT_FP:
				return getOverridden_Permanent_FP();
			case ModelPackage.MESSAGE_TRANSMISSION__AVG_MESSAGE_SIZE:
				return getAvg_Message_Size();
			case ModelPackage.MESSAGE_TRANSMISSION__MAX_MESSAGE_SIZE:
				return getMax_Message_Size();
			case ModelPackage.MESSAGE_TRANSMISSION__MIN_MESSAGE_SIZE:
				return getMin_Message_Size();
			case ModelPackage.MESSAGE_TRANSMISSION__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.MESSAGE_TRANSMISSION__OVERRIDDEN_FIXED_PRIORITY:
				setOverridden_Fixed_Priority((Overridden_Fixed_Priority)newValue);
				return;
			case ModelPackage.MESSAGE_TRANSMISSION__OVERRIDDEN_PERMANENT_FP:
				setOverridden_Permanent_FP((Overridden_Permanent_FP)newValue);
				return;
			case ModelPackage.MESSAGE_TRANSMISSION__AVG_MESSAGE_SIZE:
				setAvg_Message_Size((Double)newValue);
				return;
			case ModelPackage.MESSAGE_TRANSMISSION__MAX_MESSAGE_SIZE:
				setMax_Message_Size((Double)newValue);
				return;
			case ModelPackage.MESSAGE_TRANSMISSION__MIN_MESSAGE_SIZE:
				setMin_Message_Size((Double)newValue);
				return;
			case ModelPackage.MESSAGE_TRANSMISSION__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.MESSAGE_TRANSMISSION__OVERRIDDEN_FIXED_PRIORITY:
				setOverridden_Fixed_Priority((Overridden_Fixed_Priority)null);
				return;
			case ModelPackage.MESSAGE_TRANSMISSION__OVERRIDDEN_PERMANENT_FP:
				setOverridden_Permanent_FP((Overridden_Permanent_FP)null);
				return;
			case ModelPackage.MESSAGE_TRANSMISSION__AVG_MESSAGE_SIZE:
				setAvg_Message_Size(AVG_MESSAGE_SIZE_EDEFAULT);
				return;
			case ModelPackage.MESSAGE_TRANSMISSION__MAX_MESSAGE_SIZE:
				setMax_Message_Size(MAX_MESSAGE_SIZE_EDEFAULT);
				return;
			case ModelPackage.MESSAGE_TRANSMISSION__MIN_MESSAGE_SIZE:
				setMin_Message_Size(MIN_MESSAGE_SIZE_EDEFAULT);
				return;
			case ModelPackage.MESSAGE_TRANSMISSION__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.MESSAGE_TRANSMISSION__OVERRIDDEN_FIXED_PRIORITY:
				return overridden_Fixed_Priority != null;
			case ModelPackage.MESSAGE_TRANSMISSION__OVERRIDDEN_PERMANENT_FP:
				return overridden_Permanent_FP != null;
			case ModelPackage.MESSAGE_TRANSMISSION__AVG_MESSAGE_SIZE:
				return avg_Message_Size != AVG_MESSAGE_SIZE_EDEFAULT;
			case ModelPackage.MESSAGE_TRANSMISSION__MAX_MESSAGE_SIZE:
				return max_Message_Size != MAX_MESSAGE_SIZE_EDEFAULT;
			case ModelPackage.MESSAGE_TRANSMISSION__MIN_MESSAGE_SIZE:
				return min_Message_Size != MIN_MESSAGE_SIZE_EDEFAULT;
			case ModelPackage.MESSAGE_TRANSMISSION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Avg_Message_Size: ");
		result.append(avg_Message_Size);
		result.append(", Max_Message_Size: ");
		result.append(max_Message_Size);
		result.append(", Min_Message_Size: ");
		result.append(min_Message_Size);
		result.append(", Name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //Message_TransmissionImpl
