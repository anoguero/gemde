/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.util;

import java.util.List;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.xmi.XMLSave;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;
import org.eclipse.emf.ecore.xmi.impl.XMLSaveImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see es.esi.gemde.vv.mast.mastmodel.util.ModelResourceFactoryImpl
 * @generated
 */
public class ModelResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public ModelResourceImpl(URI uri) {
		super(uri);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl#createXMLSave()
	 */
	@Override
	protected XMLSave createXMLSave()
	{
		/*
		 * Modification by Huascar Espinoza to force the XML saver of Eclipse
		 * to include the MAST file version info line.
		 * 
		 * It is required by the tool parser!
		 */
	    return new XMLSaveImpl(createXMLHelper()){
	    	

	    	/* (non-Javadoc)
	    	 * @see org.eclipse.emf.ecore.xmi.impl.XMLSaveImpl#traverse(java.util.List)
	    	 */
	    	public void traverse(List<? extends EObject> contents)
	    	  {
	    	    if (!toDOM && declareXML)
	    	    {
	    	      doc.add("<?xml version=\"" + xmlVersion + "\" encoding=\"" + encoding + "\"?>");
	    	      doc.addLine();
	    	      
	    	      // add MAST FILE version info
	    	      doc.add("<?mast fileType=\"XML-Mast-Model-File\" version=\"1.1\"?>");
	    	      doc.addLine();
	    	    }
	    	    
	    	   

	    	    int size = contents.size();

	    	    // Reserve a place to insert xmlns declarations after we know what they all are.
	    	    //
	    	    Object mark;

	    	    if (size == 1)
	    	    {
	    	      mark = writeTopObject(contents.get(0));
	    	    }
	    	    else
	    	    {
	    	      mark = writeTopObjects(contents);
	    	    }
	    	    if (!toDOM)
	    	    {
	    	      // Go back and add all the XMLNS stuff.
	    	      //
	    	      doc.resetToMark(mark);
	    	    }
	    	    else
	    	    {
	    	      currentNode = document.getDocumentElement();
	    	    }
	    	    addNamespaceDeclarations();
	    	    addDoctypeInformation();
	    	  }
	    	
	    };
	}

} //ModelResourceImpl
