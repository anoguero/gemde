/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Periodic External Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Periodic_External_EventImpl#getMax_Jitter <em>Max Jitter</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Periodic_External_EventImpl#getName <em>Name</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Periodic_External_EventImpl#getPeriod <em>Period</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Periodic_External_EventImpl#getPhase <em>Phase</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Periodic_External_EventImpl extends EObjectImpl implements Periodic_External_Event {
	/**
	 * The default value of the '{@link #getMax_Jitter() <em>Max Jitter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_Jitter()
	 * @generated
	 * @ordered
	 */
	protected static final double MAX_JITTER_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getMax_Jitter() <em>Max Jitter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_Jitter()
	 * @generated
	 * @ordered
	 */
	protected double max_Jitter = MAX_JITTER_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPeriod() <em>Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPeriod()
	 * @generated
	 * @ordered
	 */
	protected static final double PERIOD_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getPeriod() <em>Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPeriod()
	 * @generated
	 * @ordered
	 */
	protected double period = PERIOD_EDEFAULT;

	/**
	 * The default value of the '{@link #getPhase() <em>Phase</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhase()
	 * @generated
	 * @ordered
	 */
	protected static final double PHASE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getPhase() <em>Phase</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhase()
	 * @generated
	 * @ordered
	 */
	protected double phase = PHASE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Periodic_External_EventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.PERIODIC_EXTERNAL_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getMax_Jitter() {
		return max_Jitter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMax_Jitter(double newMax_Jitter) {
		double oldMax_Jitter = max_Jitter;
		max_Jitter = newMax_Jitter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.PERIODIC_EXTERNAL_EVENT__MAX_JITTER, oldMax_Jitter, max_Jitter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.PERIODIC_EXTERNAL_EVENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPeriod() {
		return period;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPeriod(double newPeriod) {
		double oldPeriod = period;
		period = newPeriod;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.PERIODIC_EXTERNAL_EVENT__PERIOD, oldPeriod, period));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPhase() {
		return phase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhase(double newPhase) {
		double oldPhase = phase;
		phase = newPhase;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.PERIODIC_EXTERNAL_EVENT__PHASE, oldPhase, phase));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.PERIODIC_EXTERNAL_EVENT__MAX_JITTER:
				return getMax_Jitter();
			case ModelPackage.PERIODIC_EXTERNAL_EVENT__NAME:
				return getName();
			case ModelPackage.PERIODIC_EXTERNAL_EVENT__PERIOD:
				return getPeriod();
			case ModelPackage.PERIODIC_EXTERNAL_EVENT__PHASE:
				return getPhase();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.PERIODIC_EXTERNAL_EVENT__MAX_JITTER:
				setMax_Jitter((Double)newValue);
				return;
			case ModelPackage.PERIODIC_EXTERNAL_EVENT__NAME:
				setName((String)newValue);
				return;
			case ModelPackage.PERIODIC_EXTERNAL_EVENT__PERIOD:
				setPeriod((Double)newValue);
				return;
			case ModelPackage.PERIODIC_EXTERNAL_EVENT__PHASE:
				setPhase((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.PERIODIC_EXTERNAL_EVENT__MAX_JITTER:
				setMax_Jitter(MAX_JITTER_EDEFAULT);
				return;
			case ModelPackage.PERIODIC_EXTERNAL_EVENT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ModelPackage.PERIODIC_EXTERNAL_EVENT__PERIOD:
				setPeriod(PERIOD_EDEFAULT);
				return;
			case ModelPackage.PERIODIC_EXTERNAL_EVENT__PHASE:
				setPhase(PHASE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.PERIODIC_EXTERNAL_EVENT__MAX_JITTER:
				return max_Jitter != MAX_JITTER_EDEFAULT;
			case ModelPackage.PERIODIC_EXTERNAL_EVENT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ModelPackage.PERIODIC_EXTERNAL_EVENT__PERIOD:
				return period != PERIOD_EDEFAULT;
			case ModelPackage.PERIODIC_EXTERNAL_EVENT__PHASE:
				return phase != PHASE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Max_Jitter: ");
		result.append(max_Jitter);
		result.append(", Name: ");
		result.append(name);
		result.append(", Period: ");
		result.append(period);
		result.append(", Phase: ");
		result.append(phase);
		result.append(')');
		return result.toString();
	}

} //Periodic_External_EventImpl
