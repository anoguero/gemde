/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.validation;

import java.util.List;

/**
 * A sample validator interface for {@link es.esi.gemde.vv.mast.mastmodel.Barrier}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface BarrierValidator {
	boolean validate();

	boolean validateInput_Events_List(List<String> value);
	boolean validateOutput_Event(String value);
}
