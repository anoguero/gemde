/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Assertion;
import es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Policy;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fixed Priority Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Fixed_Priority_PolicyImpl#getPreassigned <em>Preassigned</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Fixed_Priority_PolicyImpl#getThe_Priority <em>The Priority</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Fixed_Priority_PolicyImpl extends EObjectImpl implements Fixed_Priority_Policy {
	/**
	 * The default value of the '{@link #getPreassigned() <em>Preassigned</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreassigned()
	 * @generated
	 * @ordered
	 */
	protected static final Assertion PREASSIGNED_EDEFAULT = Assertion.YES;

	/**
	 * The cached value of the '{@link #getPreassigned() <em>Preassigned</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreassigned()
	 * @generated
	 * @ordered
	 */
	protected Assertion preassigned = PREASSIGNED_EDEFAULT;

	/**
	 * This is true if the Preassigned attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean preassignedESet;

	/**
	 * The default value of the '{@link #getThe_Priority() <em>The Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThe_Priority()
	 * @generated
	 * @ordered
	 */
	protected static final int THE_PRIORITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getThe_Priority() <em>The Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThe_Priority()
	 * @generated
	 * @ordered
	 */
	protected int the_Priority = THE_PRIORITY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Fixed_Priority_PolicyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.FIXED_PRIORITY_POLICY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Assertion getPreassigned() {
		return preassigned;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreassigned(Assertion newPreassigned) {
		Assertion oldPreassigned = preassigned;
		preassigned = newPreassigned == null ? PREASSIGNED_EDEFAULT : newPreassigned;
		boolean oldPreassignedESet = preassignedESet;
		preassignedESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.FIXED_PRIORITY_POLICY__PREASSIGNED, oldPreassigned, preassigned, !oldPreassignedESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetPreassigned() {
		Assertion oldPreassigned = preassigned;
		boolean oldPreassignedESet = preassignedESet;
		preassigned = PREASSIGNED_EDEFAULT;
		preassignedESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ModelPackage.FIXED_PRIORITY_POLICY__PREASSIGNED, oldPreassigned, PREASSIGNED_EDEFAULT, oldPreassignedESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetPreassigned() {
		return preassignedESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getThe_Priority() {
		return the_Priority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setThe_Priority(int newThe_Priority) {
		int oldThe_Priority = the_Priority;
		the_Priority = newThe_Priority;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.FIXED_PRIORITY_POLICY__THE_PRIORITY, oldThe_Priority, the_Priority));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.FIXED_PRIORITY_POLICY__PREASSIGNED:
				return getPreassigned();
			case ModelPackage.FIXED_PRIORITY_POLICY__THE_PRIORITY:
				return getThe_Priority();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.FIXED_PRIORITY_POLICY__PREASSIGNED:
				setPreassigned((Assertion)newValue);
				return;
			case ModelPackage.FIXED_PRIORITY_POLICY__THE_PRIORITY:
				setThe_Priority((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.FIXED_PRIORITY_POLICY__PREASSIGNED:
				unsetPreassigned();
				return;
			case ModelPackage.FIXED_PRIORITY_POLICY__THE_PRIORITY:
				setThe_Priority(THE_PRIORITY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.FIXED_PRIORITY_POLICY__PREASSIGNED:
				return isSetPreassigned();
			case ModelPackage.FIXED_PRIORITY_POLICY__THE_PRIORITY:
				return the_Priority != THE_PRIORITY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Preassigned: ");
		if (preassignedESet) result.append(preassigned); else result.append("<unset>");
		result.append(", The_Priority: ");
		result.append(the_Priority);
		result.append(')');
		return result.toString();
	}

} //Fixed_Priority_PolicyImpl
