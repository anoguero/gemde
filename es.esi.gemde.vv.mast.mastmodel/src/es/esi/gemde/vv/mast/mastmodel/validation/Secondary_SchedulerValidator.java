/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.validation;

import es.esi.gemde.vv.mast.mastmodel.EDF_Scheduler;
import es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler;
import es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler;

/**
 * A sample validator interface for {@link es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface Secondary_SchedulerValidator {
	boolean validate();

	boolean validateFixed_Priority_Scheduler(Fixed_Priority_Scheduler value);
	boolean validateEDF_Scheduler(EDF_Scheduler value);
	boolean validateFP_Packet_Based_Scheduler(FP_Packet_Based_Scheduler value);
	boolean validateHost(String value);
	boolean validateName(String value);
}
