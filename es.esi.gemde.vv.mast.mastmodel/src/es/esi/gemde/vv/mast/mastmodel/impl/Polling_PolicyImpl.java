/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Assertion;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Polling_Policy;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Polling Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Polling_PolicyImpl#getPolling_Avg_Overhead <em>Polling Avg Overhead</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Polling_PolicyImpl#getPolling_Best_Overhead <em>Polling Best Overhead</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Polling_PolicyImpl#getPolling_Period <em>Polling Period</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Polling_PolicyImpl#getPolling_Worst_Overhead <em>Polling Worst Overhead</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Polling_PolicyImpl#getPreassigned <em>Preassigned</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Polling_PolicyImpl#getThe_Priority <em>The Priority</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Polling_PolicyImpl extends EObjectImpl implements Polling_Policy {
	/**
	 * The default value of the '{@link #getPolling_Avg_Overhead() <em>Polling Avg Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPolling_Avg_Overhead()
	 * @generated
	 * @ordered
	 */
	protected static final double POLLING_AVG_OVERHEAD_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getPolling_Avg_Overhead() <em>Polling Avg Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPolling_Avg_Overhead()
	 * @generated
	 * @ordered
	 */
	protected double polling_Avg_Overhead = POLLING_AVG_OVERHEAD_EDEFAULT;

	/**
	 * The default value of the '{@link #getPolling_Best_Overhead() <em>Polling Best Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPolling_Best_Overhead()
	 * @generated
	 * @ordered
	 */
	protected static final double POLLING_BEST_OVERHEAD_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getPolling_Best_Overhead() <em>Polling Best Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPolling_Best_Overhead()
	 * @generated
	 * @ordered
	 */
	protected double polling_Best_Overhead = POLLING_BEST_OVERHEAD_EDEFAULT;

	/**
	 * The default value of the '{@link #getPolling_Period() <em>Polling Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPolling_Period()
	 * @generated
	 * @ordered
	 */
	protected static final double POLLING_PERIOD_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getPolling_Period() <em>Polling Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPolling_Period()
	 * @generated
	 * @ordered
	 */
	protected double polling_Period = POLLING_PERIOD_EDEFAULT;

	/**
	 * The default value of the '{@link #getPolling_Worst_Overhead() <em>Polling Worst Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPolling_Worst_Overhead()
	 * @generated
	 * @ordered
	 */
	protected static final double POLLING_WORST_OVERHEAD_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getPolling_Worst_Overhead() <em>Polling Worst Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPolling_Worst_Overhead()
	 * @generated
	 * @ordered
	 */
	protected double polling_Worst_Overhead = POLLING_WORST_OVERHEAD_EDEFAULT;

	/**
	 * The default value of the '{@link #getPreassigned() <em>Preassigned</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreassigned()
	 * @generated
	 * @ordered
	 */
	protected static final Assertion PREASSIGNED_EDEFAULT = Assertion.YES;

	/**
	 * The cached value of the '{@link #getPreassigned() <em>Preassigned</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreassigned()
	 * @generated
	 * @ordered
	 */
	protected Assertion preassigned = PREASSIGNED_EDEFAULT;

	/**
	 * This is true if the Preassigned attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean preassignedESet;

	/**
	 * The default value of the '{@link #getThe_Priority() <em>The Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThe_Priority()
	 * @generated
	 * @ordered
	 */
	protected static final int THE_PRIORITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getThe_Priority() <em>The Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThe_Priority()
	 * @generated
	 * @ordered
	 */
	protected int the_Priority = THE_PRIORITY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Polling_PolicyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.POLLING_POLICY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPolling_Avg_Overhead() {
		return polling_Avg_Overhead;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPolling_Avg_Overhead(double newPolling_Avg_Overhead) {
		double oldPolling_Avg_Overhead = polling_Avg_Overhead;
		polling_Avg_Overhead = newPolling_Avg_Overhead;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.POLLING_POLICY__POLLING_AVG_OVERHEAD, oldPolling_Avg_Overhead, polling_Avg_Overhead));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPolling_Best_Overhead() {
		return polling_Best_Overhead;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPolling_Best_Overhead(double newPolling_Best_Overhead) {
		double oldPolling_Best_Overhead = polling_Best_Overhead;
		polling_Best_Overhead = newPolling_Best_Overhead;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.POLLING_POLICY__POLLING_BEST_OVERHEAD, oldPolling_Best_Overhead, polling_Best_Overhead));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPolling_Period() {
		return polling_Period;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPolling_Period(double newPolling_Period) {
		double oldPolling_Period = polling_Period;
		polling_Period = newPolling_Period;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.POLLING_POLICY__POLLING_PERIOD, oldPolling_Period, polling_Period));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPolling_Worst_Overhead() {
		return polling_Worst_Overhead;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPolling_Worst_Overhead(double newPolling_Worst_Overhead) {
		double oldPolling_Worst_Overhead = polling_Worst_Overhead;
		polling_Worst_Overhead = newPolling_Worst_Overhead;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.POLLING_POLICY__POLLING_WORST_OVERHEAD, oldPolling_Worst_Overhead, polling_Worst_Overhead));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Assertion getPreassigned() {
		return preassigned;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreassigned(Assertion newPreassigned) {
		Assertion oldPreassigned = preassigned;
		preassigned = newPreassigned == null ? PREASSIGNED_EDEFAULT : newPreassigned;
		boolean oldPreassignedESet = preassignedESet;
		preassignedESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.POLLING_POLICY__PREASSIGNED, oldPreassigned, preassigned, !oldPreassignedESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetPreassigned() {
		Assertion oldPreassigned = preassigned;
		boolean oldPreassignedESet = preassignedESet;
		preassigned = PREASSIGNED_EDEFAULT;
		preassignedESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ModelPackage.POLLING_POLICY__PREASSIGNED, oldPreassigned, PREASSIGNED_EDEFAULT, oldPreassignedESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetPreassigned() {
		return preassignedESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getThe_Priority() {
		return the_Priority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setThe_Priority(int newThe_Priority) {
		int oldThe_Priority = the_Priority;
		the_Priority = newThe_Priority;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.POLLING_POLICY__THE_PRIORITY, oldThe_Priority, the_Priority));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.POLLING_POLICY__POLLING_AVG_OVERHEAD:
				return getPolling_Avg_Overhead();
			case ModelPackage.POLLING_POLICY__POLLING_BEST_OVERHEAD:
				return getPolling_Best_Overhead();
			case ModelPackage.POLLING_POLICY__POLLING_PERIOD:
				return getPolling_Period();
			case ModelPackage.POLLING_POLICY__POLLING_WORST_OVERHEAD:
				return getPolling_Worst_Overhead();
			case ModelPackage.POLLING_POLICY__PREASSIGNED:
				return getPreassigned();
			case ModelPackage.POLLING_POLICY__THE_PRIORITY:
				return getThe_Priority();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.POLLING_POLICY__POLLING_AVG_OVERHEAD:
				setPolling_Avg_Overhead((Double)newValue);
				return;
			case ModelPackage.POLLING_POLICY__POLLING_BEST_OVERHEAD:
				setPolling_Best_Overhead((Double)newValue);
				return;
			case ModelPackage.POLLING_POLICY__POLLING_PERIOD:
				setPolling_Period((Double)newValue);
				return;
			case ModelPackage.POLLING_POLICY__POLLING_WORST_OVERHEAD:
				setPolling_Worst_Overhead((Double)newValue);
				return;
			case ModelPackage.POLLING_POLICY__PREASSIGNED:
				setPreassigned((Assertion)newValue);
				return;
			case ModelPackage.POLLING_POLICY__THE_PRIORITY:
				setThe_Priority((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.POLLING_POLICY__POLLING_AVG_OVERHEAD:
				setPolling_Avg_Overhead(POLLING_AVG_OVERHEAD_EDEFAULT);
				return;
			case ModelPackage.POLLING_POLICY__POLLING_BEST_OVERHEAD:
				setPolling_Best_Overhead(POLLING_BEST_OVERHEAD_EDEFAULT);
				return;
			case ModelPackage.POLLING_POLICY__POLLING_PERIOD:
				setPolling_Period(POLLING_PERIOD_EDEFAULT);
				return;
			case ModelPackage.POLLING_POLICY__POLLING_WORST_OVERHEAD:
				setPolling_Worst_Overhead(POLLING_WORST_OVERHEAD_EDEFAULT);
				return;
			case ModelPackage.POLLING_POLICY__PREASSIGNED:
				unsetPreassigned();
				return;
			case ModelPackage.POLLING_POLICY__THE_PRIORITY:
				setThe_Priority(THE_PRIORITY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.POLLING_POLICY__POLLING_AVG_OVERHEAD:
				return polling_Avg_Overhead != POLLING_AVG_OVERHEAD_EDEFAULT;
			case ModelPackage.POLLING_POLICY__POLLING_BEST_OVERHEAD:
				return polling_Best_Overhead != POLLING_BEST_OVERHEAD_EDEFAULT;
			case ModelPackage.POLLING_POLICY__POLLING_PERIOD:
				return polling_Period != POLLING_PERIOD_EDEFAULT;
			case ModelPackage.POLLING_POLICY__POLLING_WORST_OVERHEAD:
				return polling_Worst_Overhead != POLLING_WORST_OVERHEAD_EDEFAULT;
			case ModelPackage.POLLING_POLICY__PREASSIGNED:
				return isSetPreassigned();
			case ModelPackage.POLLING_POLICY__THE_PRIORITY:
				return the_Priority != THE_PRIORITY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Polling_Avg_Overhead: ");
		result.append(polling_Avg_Overhead);
		result.append(", Polling_Best_Overhead: ");
		result.append(polling_Best_Overhead);
		result.append(", Polling_Period: ");
		result.append(polling_Period);
		result.append(", Polling_Worst_Overhead: ");
		result.append(polling_Worst_Overhead);
		result.append(", Preassigned: ");
		if (preassignedESet) result.append(preassigned); else result.append("<unset>");
		result.append(", The_Priority: ");
		result.append(the_Priority);
		result.append(')');
		return result.toString();
	}

} //Polling_PolicyImpl
