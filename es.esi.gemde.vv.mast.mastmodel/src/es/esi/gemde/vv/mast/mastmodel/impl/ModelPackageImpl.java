/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Activity;
import es.esi.gemde.vv.mast.mastmodel.Affirmative_Assertion;
import es.esi.gemde.vv.mast.mastmodel.Alarm_Clock_System_Timer;
import es.esi.gemde.vv.mast.mastmodel.Assertion;
import es.esi.gemde.vv.mast.mastmodel.Barrier;
import es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event;
import es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver;
import es.esi.gemde.vv.mast.mastmodel.Composite_Operation;
import es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement;
import es.esi.gemde.vv.mast.mastmodel.Concentrator;
import es.esi.gemde.vv.mast.mastmodel.Delay;
import es.esi.gemde.vv.mast.mastmodel.Delivery_Policy;
import es.esi.gemde.vv.mast.mastmodel.Delivery_Server;
import es.esi.gemde.vv.mast.mastmodel.Distribution_Type;
import es.esi.gemde.vv.mast.mastmodel.Document_Root;
import es.esi.gemde.vv.mast.mastmodel.EDF_Policy;
import es.esi.gemde.vv.mast.mastmodel.EDF_Scheduler;
import es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation;
import es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler;
import es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Policy;
import es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler;
import es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio;
import es.esi.gemde.vv.mast.mastmodel.Hard_Global_Deadline;
import es.esi.gemde.vv.mast.mastmodel.Hard_Local_Deadline;
import es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource;
import es.esi.gemde.vv.mast.mastmodel.Interrupt_FP_Policy;
import es.esi.gemde.vv.mast.mastmodel.List_of_Drivers;
import es.esi.gemde.vv.mast.mastmodel.Local_Max_Miss_Ratio;
import es.esi.gemde.vv.mast.mastmodel.Max_Output_Jitter_Req;
import es.esi.gemde.vv.mast.mastmodel.Message_Transmission;
import es.esi.gemde.vv.mast.mastmodel.ModelFactory;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Multicast;
import es.esi.gemde.vv.mast.mastmodel.Negative_Assertion;
import es.esi.gemde.vv.mast.mastmodel.Non_Preemptible_FP_Policy;
import es.esi.gemde.vv.mast.mastmodel.Offset;
import es.esi.gemde.vv.mast.mastmodel.Overhead_Type;
import es.esi.gemde.vv.mast.mastmodel.Overridden_Fixed_Priority;
import es.esi.gemde.vv.mast.mastmodel.Overridden_Permanent_FP;
import es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network;
import es.esi.gemde.vv.mast.mastmodel.Packet_Driver;
import es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event;
import es.esi.gemde.vv.mast.mastmodel.Polling_Policy;
import es.esi.gemde.vv.mast.mastmodel.Primary_Scheduler;
import es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Protocol;
import es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Resource;
import es.esi.gemde.vv.mast.mastmodel.Query_Server;
import es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver;
import es.esi.gemde.vv.mast.mastmodel.Rate_Divisor;
import es.esi.gemde.vv.mast.mastmodel.Regular_Event;
import es.esi.gemde.vv.mast.mastmodel.Regular_Processor;
import es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server;
import es.esi.gemde.vv.mast.mastmodel.Regular_Transaction;
import es.esi.gemde.vv.mast.mastmodel.Request_Policy;
import es.esi.gemde.vv.mast.mastmodel.SRP_Parameters;
import es.esi.gemde.vv.mast.mastmodel.SRP_Resource;
import es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler;
import es.esi.gemde.vv.mast.mastmodel.Simple_Operation;
import es.esi.gemde.vv.mast.mastmodel.Singular_External_Event;
import es.esi.gemde.vv.mast.mastmodel.Soft_Global_Deadline;
import es.esi.gemde.vv.mast.mastmodel.Soft_Local_Deadline;
import es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event;
import es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy;
import es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity;
import es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer;
import es.esi.gemde.vv.mast.mastmodel.Transmission_Type;
import es.esi.gemde.vv.mast.mastmodel.Unbounded_External_Event;

import es.esi.gemde.vv.mast.mastmodel.util.ModelValidator;

import java.util.List;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ModelPackageImpl extends EPackageImpl implements ModelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass alarm_Clock_System_TimerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass barrierEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bursty_External_EventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass character_Packet_DriverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass composite_OperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass composite_Timing_RequirementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass concentratorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass delayEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass delivery_ServerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass document_RootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass edF_PolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass edF_SchedulerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass enclosing_OperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fixed_Priority_PolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fixed_Priority_SchedulerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fP_Packet_Based_SchedulerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass global_Max_Miss_RatioEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hard_Global_DeadlineEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hard_Local_DeadlineEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass immediate_Ceiling_ResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interrupt_FP_PolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass list_of_DriversEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass local_Max_Miss_RatioEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass masT_MODELEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass max_Output_Jitter_ReqEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass message_TransmissionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multicastEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass non_Preemptible_FP_PolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass offsetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass overridden_Fixed_PriorityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass overridden_Permanent_FPEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass packet_Based_NetworkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass packet_DriverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass periodic_External_EventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass polling_PolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass primary_SchedulerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass priority_Inheritance_ResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass query_ServerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rate_DivisorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass regular_EventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass regular_ProcessorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass regular_Scheduling_ServerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass regular_TransactionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rteP_PacketDriverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass secondary_SchedulerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simple_OperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass singular_External_EventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass soft_Global_DeadlineEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass soft_Local_DeadlineEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sporadic_External_EventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sporadic_Server_PolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass srP_ParametersEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass srP_ResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass system_Timed_ActivityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ticker_System_TimerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unbounded_External_EventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum affirmative_AssertionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum assertionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum delivery_PolicyEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum distribution_TypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum negative_AssertionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum overhead_TypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum priority_Inheritance_ProtocolEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum request_PolicyEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum transmission_TypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType absolute_TimeEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType affirmative_Assertion_ObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType any_PriorityEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType assertion_ObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType bit_CountEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType date_TimeEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType delivery_Policy_ObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType distribution_Type_ObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType floatEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType identifierEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType identifier_RefEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType identifier_Ref_ListEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType interrupt_PriorityEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType naturalEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType negative_Assertion_ObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType normalized_Execution_TimeEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType overhead_Type_ObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType pathnameEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType percentageEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType positiveEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType preemption_LevelEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType priorityEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType priority_Inheritance_Protocol_ObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType request_Policy_ObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType throughputEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType timeEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType transmission_Type_ObjectEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ModelPackageImpl() {
		super(eNS_URI, ModelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ModelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ModelPackage init() {
		if (isInited) return (ModelPackage)EPackage.Registry.INSTANCE.getEPackage(ModelPackage.eNS_URI);

		// Obtain or create and register package
		ModelPackageImpl theModelPackage = (ModelPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ModelPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ModelPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theModelPackage.createPackageContents();

		// Initialize created meta-data
		theModelPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theModelPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return ModelValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theModelPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ModelPackage.eNS_URI, theModelPackage);
		return theModelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActivity() {
		return activityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActivity_Activity_Operation() {
		return (EAttribute)activityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActivity_Activity_Server() {
		return (EAttribute)activityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActivity_Input_Event() {
		return (EAttribute)activityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActivity_Output_Event() {
		return (EAttribute)activityEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAlarm_Clock_System_Timer() {
		return alarm_Clock_System_TimerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAlarm_Clock_System_Timer_Avg_Overhead() {
		return (EAttribute)alarm_Clock_System_TimerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAlarm_Clock_System_Timer_Best_Overhead() {
		return (EAttribute)alarm_Clock_System_TimerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAlarm_Clock_System_Timer_Worst_Overhead() {
		return (EAttribute)alarm_Clock_System_TimerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBarrier() {
		return barrierEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBarrier_Input_Events_List() {
		return (EAttribute)barrierEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBarrier_Output_Event() {
		return (EAttribute)barrierEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBursty_External_Event() {
		return bursty_External_EventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBursty_External_Event_Avg_Interarrival() {
		return (EAttribute)bursty_External_EventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBursty_External_Event_Bound_Interval() {
		return (EAttribute)bursty_External_EventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBursty_External_Event_Distribution() {
		return (EAttribute)bursty_External_EventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBursty_External_Event_Max_Arrivals() {
		return (EAttribute)bursty_External_EventEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBursty_External_Event_Name() {
		return (EAttribute)bursty_External_EventEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCharacter_Packet_Driver() {
		return character_Packet_DriverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCharacter_Packet_Driver_Character_Receive_Operation() {
		return (EAttribute)character_Packet_DriverEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCharacter_Packet_Driver_Character_Send_Operation() {
		return (EAttribute)character_Packet_DriverEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCharacter_Packet_Driver_Character_Server() {
		return (EAttribute)character_Packet_DriverEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCharacter_Packet_Driver_Character_Transmission_Time() {
		return (EAttribute)character_Packet_DriverEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCharacter_Packet_Driver_Message_Partitioning() {
		return (EAttribute)character_Packet_DriverEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCharacter_Packet_Driver_Packet_Receive_Operation() {
		return (EAttribute)character_Packet_DriverEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCharacter_Packet_Driver_Packet_Send_Operation() {
		return (EAttribute)character_Packet_DriverEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCharacter_Packet_Driver_Packet_Server() {
		return (EAttribute)character_Packet_DriverEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCharacter_Packet_Driver_RTA_Overhead_Model() {
		return (EAttribute)character_Packet_DriverEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComposite_Operation() {
		return composite_OperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComposite_Operation_Overridden_Fixed_Priority() {
		return (EReference)composite_OperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComposite_Operation_Overridden_Permanent_FP() {
		return (EReference)composite_OperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComposite_Operation_Operation_List() {
		return (EAttribute)composite_OperationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComposite_Operation_Name() {
		return (EAttribute)composite_OperationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComposite_Timing_Requirement() {
		return composite_Timing_RequirementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComposite_Timing_Requirement_Group() {
		return (EAttribute)composite_Timing_RequirementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComposite_Timing_Requirement_Max_Output_Jitter_Req() {
		return (EReference)composite_Timing_RequirementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComposite_Timing_Requirement_Hard_Global_Deadline() {
		return (EReference)composite_Timing_RequirementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComposite_Timing_Requirement_Soft_Global_Deadline() {
		return (EReference)composite_Timing_RequirementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComposite_Timing_Requirement_Global_Max_Miss_Ratio() {
		return (EReference)composite_Timing_RequirementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComposite_Timing_Requirement_Hard_Local_Deadline() {
		return (EReference)composite_Timing_RequirementEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComposite_Timing_Requirement_Soft_Local_Deadline() {
		return (EReference)composite_Timing_RequirementEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComposite_Timing_Requirement_Local_Max_Miss_Ratio() {
		return (EReference)composite_Timing_RequirementEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConcentrator() {
		return concentratorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConcentrator_Input_Events_List() {
		return (EAttribute)concentratorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConcentrator_Output_Event() {
		return (EAttribute)concentratorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDelay() {
		return delayEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDelay_Delay_Max_Interval() {
		return (EAttribute)delayEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDelay_Delay_Min_Interval() {
		return (EAttribute)delayEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDelay_Input_Event() {
		return (EAttribute)delayEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDelay_Output_Event() {
		return (EAttribute)delayEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDelivery_Server() {
		return delivery_ServerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDelivery_Server_Delivery_Policy() {
		return (EAttribute)delivery_ServerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDelivery_Server_Input_Event() {
		return (EAttribute)delivery_ServerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDelivery_Server_Output_Events_List() {
		return (EAttribute)delivery_ServerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDocument_Root() {
		return document_RootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDocument_Root_Mixed() {
		return (EAttribute)document_RootEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocument_Root_XMLNSPrefixMap() {
		return (EReference)document_RootEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocument_Root_XSISchemaLocation() {
		return (EReference)document_RootEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocument_Root_MAST_MODEL() {
		return (EReference)document_RootEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEDF_Policy() {
		return edF_PolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEDF_Policy_Deadline() {
		return (EAttribute)edF_PolicyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEDF_Policy_Preassigned() {
		return (EAttribute)edF_PolicyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEDF_Scheduler() {
		return edF_SchedulerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEDF_Scheduler_Avg_Context_Switch() {
		return (EAttribute)edF_SchedulerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEDF_Scheduler_Best_Context_Switch() {
		return (EAttribute)edF_SchedulerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEDF_Scheduler_Worst_Context_Switch() {
		return (EAttribute)edF_SchedulerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnclosing_Operation() {
		return enclosing_OperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnclosing_Operation_Overridden_Fixed_Priority() {
		return (EReference)enclosing_OperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnclosing_Operation_Overridden_Permanent_FP() {
		return (EReference)enclosing_OperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEnclosing_Operation_Operation_List() {
		return (EAttribute)enclosing_OperationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEnclosing_Operation_Average_Case_Execution_Time() {
		return (EAttribute)enclosing_OperationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEnclosing_Operation_Best_Case_Execution_Time() {
		return (EAttribute)enclosing_OperationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEnclosing_Operation_Name() {
		return (EAttribute)enclosing_OperationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEnclosing_Operation_Worst_Case_Execution_Time() {
		return (EAttribute)enclosing_OperationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFixed_Priority_Policy() {
		return fixed_Priority_PolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixed_Priority_Policy_Preassigned() {
		return (EAttribute)fixed_Priority_PolicyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixed_Priority_Policy_The_Priority() {
		return (EAttribute)fixed_Priority_PolicyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFixed_Priority_Scheduler() {
		return fixed_Priority_SchedulerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixed_Priority_Scheduler_Avg_Context_Switch() {
		return (EAttribute)fixed_Priority_SchedulerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixed_Priority_Scheduler_Best_Context_Switch() {
		return (EAttribute)fixed_Priority_SchedulerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixed_Priority_Scheduler_Max_Priority() {
		return (EAttribute)fixed_Priority_SchedulerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixed_Priority_Scheduler_Min_Priority() {
		return (EAttribute)fixed_Priority_SchedulerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixed_Priority_Scheduler_Worst_Context_Switch() {
		return (EAttribute)fixed_Priority_SchedulerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFP_Packet_Based_Scheduler() {
		return fP_Packet_Based_SchedulerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFP_Packet_Based_Scheduler_Max_Priority() {
		return (EAttribute)fP_Packet_Based_SchedulerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFP_Packet_Based_Scheduler_Min_Priority() {
		return (EAttribute)fP_Packet_Based_SchedulerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFP_Packet_Based_Scheduler_Packet_Overhead_Avg_Size() {
		return (EAttribute)fP_Packet_Based_SchedulerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFP_Packet_Based_Scheduler_Packet_Overhead_Max_Size() {
		return (EAttribute)fP_Packet_Based_SchedulerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFP_Packet_Based_Scheduler_Packet_Overhead_Min_Size() {
		return (EAttribute)fP_Packet_Based_SchedulerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGlobal_Max_Miss_Ratio() {
		return global_Max_Miss_RatioEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGlobal_Max_Miss_Ratio_Deadline() {
		return (EAttribute)global_Max_Miss_RatioEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGlobal_Max_Miss_Ratio_Ratio() {
		return (EAttribute)global_Max_Miss_RatioEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGlobal_Max_Miss_Ratio_Referenced_Event() {
		return (EAttribute)global_Max_Miss_RatioEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHard_Global_Deadline() {
		return hard_Global_DeadlineEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHard_Global_Deadline_Deadline() {
		return (EAttribute)hard_Global_DeadlineEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHard_Global_Deadline_Referenced_Event() {
		return (EAttribute)hard_Global_DeadlineEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHard_Local_Deadline() {
		return hard_Local_DeadlineEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHard_Local_Deadline_Deadline() {
		return (EAttribute)hard_Local_DeadlineEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImmediate_Ceiling_Resource() {
		return immediate_Ceiling_ResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImmediate_Ceiling_Resource_Ceiling() {
		return (EAttribute)immediate_Ceiling_ResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImmediate_Ceiling_Resource_Name() {
		return (EAttribute)immediate_Ceiling_ResourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImmediate_Ceiling_Resource_Preassigned() {
		return (EAttribute)immediate_Ceiling_ResourceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInterrupt_FP_Policy() {
		return interrupt_FP_PolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInterrupt_FP_Policy_Preassigned() {
		return (EAttribute)interrupt_FP_PolicyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInterrupt_FP_Policy_The_Priority() {
		return (EAttribute)interrupt_FP_PolicyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getList_of_Drivers() {
		return list_of_DriversEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getList_of_Drivers_Group() {
		return (EAttribute)list_of_DriversEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getList_of_Drivers_Packet_Driver() {
		return (EReference)list_of_DriversEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getList_of_Drivers_Character_Packet_Driver() {
		return (EReference)list_of_DriversEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getList_of_Drivers_RTEP_Packet_Driver() {
		return (EReference)list_of_DriversEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLocal_Max_Miss_Ratio() {
		return local_Max_Miss_RatioEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLocal_Max_Miss_Ratio_Deadline() {
		return (EAttribute)local_Max_Miss_RatioEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLocal_Max_Miss_Ratio_Ratio() {
		return (EAttribute)local_Max_Miss_RatioEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMAST_MODEL() {
		return masT_MODELEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAST_MODEL_Group() {
		return (EAttribute)masT_MODELEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAST_MODEL_Regular_Processor() {
		return (EReference)masT_MODELEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAST_MODEL_Packet_Based_Network() {
		return (EReference)masT_MODELEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAST_MODEL_Regular_Scheduling_Server() {
		return (EReference)masT_MODELEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAST_MODEL_Immediate_Ceiling_Resource() {
		return (EReference)masT_MODELEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAST_MODEL_Priority_Inheritance_Resource() {
		return (EReference)masT_MODELEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAST_MODEL_SRP_Resource() {
		return (EReference)masT_MODELEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAST_MODEL_Simple_Operation() {
		return (EReference)masT_MODELEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAST_MODEL_Message_Transmission() {
		return (EReference)masT_MODELEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAST_MODEL_Composite_Operation() {
		return (EReference)masT_MODELEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAST_MODEL_Enclosing_Operation() {
		return (EReference)masT_MODELEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAST_MODEL_Regular_Transaction() {
		return (EReference)masT_MODELEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAST_MODEL_Primary_Scheduler() {
		return (EReference)masT_MODELEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAST_MODEL_Secondary_Scheduler() {
		return (EReference)masT_MODELEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAST_MODEL_Model_Date() {
		return (EAttribute)masT_MODELEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAST_MODEL_Model_Name() {
		return (EAttribute)masT_MODELEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAST_MODEL_System_PiP_Behaviour() {
		return (EAttribute)masT_MODELEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMax_Output_Jitter_Req() {
		return max_Output_Jitter_ReqEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMax_Output_Jitter_Req_Max_Output_Jitter() {
		return (EAttribute)max_Output_Jitter_ReqEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMax_Output_Jitter_Req_Referenced_Event() {
		return (EAttribute)max_Output_Jitter_ReqEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMessage_Transmission() {
		return message_TransmissionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessage_Transmission_Overridden_Fixed_Priority() {
		return (EReference)message_TransmissionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMessage_Transmission_Overridden_Permanent_FP() {
		return (EReference)message_TransmissionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessage_Transmission_Avg_Message_Size() {
		return (EAttribute)message_TransmissionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessage_Transmission_Max_Message_Size() {
		return (EAttribute)message_TransmissionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessage_Transmission_Min_Message_Size() {
		return (EAttribute)message_TransmissionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMessage_Transmission_Name() {
		return (EAttribute)message_TransmissionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMulticast() {
		return multicastEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMulticast_Input_Event() {
		return (EAttribute)multicastEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMulticast_Output_Events_List() {
		return (EAttribute)multicastEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNon_Preemptible_FP_Policy() {
		return non_Preemptible_FP_PolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNon_Preemptible_FP_Policy_Preassigned() {
		return (EAttribute)non_Preemptible_FP_PolicyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNon_Preemptible_FP_Policy_The_Priority() {
		return (EAttribute)non_Preemptible_FP_PolicyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOffset() {
		return offsetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOffset_Delay_Max_Interval() {
		return (EAttribute)offsetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOffset_Delay_Min_Interval() {
		return (EAttribute)offsetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOffset_Input_Event() {
		return (EAttribute)offsetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOffset_Output_Event() {
		return (EAttribute)offsetEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOffset_Referenced_Event() {
		return (EAttribute)offsetEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOverridden_Fixed_Priority() {
		return overridden_Fixed_PriorityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOverridden_Fixed_Priority_The_Priority() {
		return (EAttribute)overridden_Fixed_PriorityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOverridden_Permanent_FP() {
		return overridden_Permanent_FPEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOverridden_Permanent_FP_The_Priority() {
		return (EAttribute)overridden_Permanent_FPEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPacket_Based_Network() {
		return packet_Based_NetworkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPacket_Based_Network_List_of_Drivers() {
		return (EReference)packet_Based_NetworkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPacket_Based_Network_Max_Blocking() {
		return (EAttribute)packet_Based_NetworkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPacket_Based_Network_Max_Packet_Size() {
		return (EAttribute)packet_Based_NetworkEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPacket_Based_Network_Min_Packet_Size() {
		return (EAttribute)packet_Based_NetworkEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPacket_Based_Network_Name() {
		return (EAttribute)packet_Based_NetworkEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPacket_Based_Network_Speed_Factor() {
		return (EAttribute)packet_Based_NetworkEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPacket_Based_Network_Throughput() {
		return (EAttribute)packet_Based_NetworkEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPacket_Based_Network_Transmission() {
		return (EAttribute)packet_Based_NetworkEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPacket_Driver() {
		return packet_DriverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPacket_Driver_Message_Partitioning() {
		return (EAttribute)packet_DriverEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPacket_Driver_Packet_Receive_Operation() {
		return (EAttribute)packet_DriverEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPacket_Driver_Packet_Send_Operation() {
		return (EAttribute)packet_DriverEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPacket_Driver_Packet_Server() {
		return (EAttribute)packet_DriverEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPacket_Driver_RTA_Overhead_Model() {
		return (EAttribute)packet_DriverEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPeriodic_External_Event() {
		return periodic_External_EventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPeriodic_External_Event_Max_Jitter() {
		return (EAttribute)periodic_External_EventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPeriodic_External_Event_Name() {
		return (EAttribute)periodic_External_EventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPeriodic_External_Event_Period() {
		return (EAttribute)periodic_External_EventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPeriodic_External_Event_Phase() {
		return (EAttribute)periodic_External_EventEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPolling_Policy() {
		return polling_PolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolling_Policy_Polling_Avg_Overhead() {
		return (EAttribute)polling_PolicyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolling_Policy_Polling_Best_Overhead() {
		return (EAttribute)polling_PolicyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolling_Policy_Polling_Period() {
		return (EAttribute)polling_PolicyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolling_Policy_Polling_Worst_Overhead() {
		return (EAttribute)polling_PolicyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolling_Policy_Preassigned() {
		return (EAttribute)polling_PolicyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolling_Policy_The_Priority() {
		return (EAttribute)polling_PolicyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPrimary_Scheduler() {
		return primary_SchedulerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPrimary_Scheduler_Fixed_Priority_Scheduler() {
		return (EReference)primary_SchedulerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPrimary_Scheduler_EDF_Scheduler() {
		return (EReference)primary_SchedulerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPrimary_Scheduler_FP_Packet_Based_Scheduler() {
		return (EReference)primary_SchedulerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPrimary_Scheduler_Host() {
		return (EAttribute)primary_SchedulerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPrimary_Scheduler_Name() {
		return (EAttribute)primary_SchedulerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPriority_Inheritance_Resource() {
		return priority_Inheritance_ResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPriority_Inheritance_Resource_Name() {
		return (EAttribute)priority_Inheritance_ResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getQuery_Server() {
		return query_ServerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQuery_Server_Input_Event() {
		return (EAttribute)query_ServerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQuery_Server_Output_Events_List() {
		return (EAttribute)query_ServerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQuery_Server_Request_Policy() {
		return (EAttribute)query_ServerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRate_Divisor() {
		return rate_DivisorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRate_Divisor_Input_Event() {
		return (EAttribute)rate_DivisorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRate_Divisor_Output_Event() {
		return (EAttribute)rate_DivisorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRate_Divisor_Rate_Factor() {
		return (EAttribute)rate_DivisorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRegular_Event() {
		return regular_EventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRegular_Event_Group() {
		return (EAttribute)regular_EventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Event_Max_Output_Jitter_Req() {
		return (EReference)regular_EventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Event_Hard_Global_Deadline() {
		return (EReference)regular_EventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Event_Soft_Global_Deadline() {
		return (EReference)regular_EventEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Event_Global_Max_Miss_Ratio() {
		return (EReference)regular_EventEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Event_Hard_Local_Deadline() {
		return (EReference)regular_EventEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Event_Soft_Local_Deadline() {
		return (EReference)regular_EventEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Event_Local_Max_Miss_Ratio() {
		return (EReference)regular_EventEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Event_Composite_Timing_Requirement() {
		return (EReference)regular_EventEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRegular_Event_Event() {
		return (EAttribute)regular_EventEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRegular_Processor() {
		return regular_ProcessorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Processor_Ticker_System_Timer() {
		return (EReference)regular_ProcessorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Processor_Alarm_Clock_System_Timer() {
		return (EReference)regular_ProcessorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRegular_Processor_Avg_ISR_Switch() {
		return (EAttribute)regular_ProcessorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRegular_Processor_Best_ISR_Switch() {
		return (EAttribute)regular_ProcessorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRegular_Processor_Max_Interrupt_Priority() {
		return (EAttribute)regular_ProcessorEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRegular_Processor_Min_Interrupt_Priority() {
		return (EAttribute)regular_ProcessorEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRegular_Processor_Name() {
		return (EAttribute)regular_ProcessorEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRegular_Processor_Speed_Factor() {
		return (EAttribute)regular_ProcessorEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRegular_Processor_Worst_ISR_Switch() {
		return (EAttribute)regular_ProcessorEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRegular_Scheduling_Server() {
		return regular_Scheduling_ServerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Scheduling_Server_Non_Preemptible_FP_Policy() {
		return (EReference)regular_Scheduling_ServerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Scheduling_Server_Fixed_Priority_Policy() {
		return (EReference)regular_Scheduling_ServerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Scheduling_Server_Interrupt_FP_Policy() {
		return (EReference)regular_Scheduling_ServerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Scheduling_Server_Polling_Policy() {
		return (EReference)regular_Scheduling_ServerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Scheduling_Server_Sporadic_Server_Policy() {
		return (EReference)regular_Scheduling_ServerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Scheduling_Server_EDF_Policy() {
		return (EReference)regular_Scheduling_ServerEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Scheduling_Server_SRP_Parameters() {
		return (EReference)regular_Scheduling_ServerEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRegular_Scheduling_Server_Name() {
		return (EAttribute)regular_Scheduling_ServerEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRegular_Scheduling_Server_Scheduler() {
		return (EAttribute)regular_Scheduling_ServerEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRegular_Transaction() {
		return regular_TransactionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRegular_Transaction_Group() {
		return (EAttribute)regular_TransactionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Transaction_Periodic_External_Event() {
		return (EReference)regular_TransactionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Transaction_Sporadic_External_Event() {
		return (EReference)regular_TransactionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Transaction_Unbounded_External_Event() {
		return (EReference)regular_TransactionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Transaction_Bursty_External_Event() {
		return (EReference)regular_TransactionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Transaction_Singular_External_Event() {
		return (EReference)regular_TransactionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Transaction_Regular_Event() {
		return (EReference)regular_TransactionEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Transaction_Activity() {
		return (EReference)regular_TransactionEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Transaction_System_Timed_Activity() {
		return (EReference)regular_TransactionEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Transaction_Concentrator() {
		return (EReference)regular_TransactionEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Transaction_Barrier() {
		return (EReference)regular_TransactionEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Transaction_Delivery_Server() {
		return (EReference)regular_TransactionEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Transaction_Query_Server() {
		return (EReference)regular_TransactionEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Transaction_Multicast() {
		return (EReference)regular_TransactionEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Transaction_Rate_Divisor() {
		return (EReference)regular_TransactionEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Transaction_Delay() {
		return (EReference)regular_TransactionEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRegular_Transaction_Offset() {
		return (EReference)regular_TransactionEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRegular_Transaction_Name() {
		return (EAttribute)regular_TransactionEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRTEP_PacketDriver() {
		return rteP_PacketDriverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRTEP_PacketDriver_Failure_Timeout() {
		return (EAttribute)rteP_PacketDriverEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRTEP_PacketDriver_Message_Partitioning() {
		return (EAttribute)rteP_PacketDriverEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRTEP_PacketDriver_Number_Of_Stations() {
		return (EAttribute)rteP_PacketDriverEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRTEP_PacketDriver_Packet_Discard_Operation() {
		return (EAttribute)rteP_PacketDriverEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRTEP_PacketDriver_Packet_Interrupt_Server() {
		return (EAttribute)rteP_PacketDriverEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRTEP_PacketDriver_Packet_ISR_Operation() {
		return (EAttribute)rteP_PacketDriverEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRTEP_PacketDriver_Packet_Receive_Operation() {
		return (EAttribute)rteP_PacketDriverEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRTEP_PacketDriver_Packet_Retransmission_Operation() {
		return (EAttribute)rteP_PacketDriverEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRTEP_PacketDriver_Packet_Send_Operation() {
		return (EAttribute)rteP_PacketDriverEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRTEP_PacketDriver_Packet_Server() {
		return (EAttribute)rteP_PacketDriverEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRTEP_PacketDriver_Packet_Transmission_Retries() {
		return (EAttribute)rteP_PacketDriverEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRTEP_PacketDriver_RTA_Overhead_Model() {
		return (EAttribute)rteP_PacketDriverEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRTEP_PacketDriver_Token_Check_Operation() {
		return (EAttribute)rteP_PacketDriverEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRTEP_PacketDriver_Token_Delay() {
		return (EAttribute)rteP_PacketDriverEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRTEP_PacketDriver_Token_Manage_Operation() {
		return (EAttribute)rteP_PacketDriverEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRTEP_PacketDriver_Token_Retransmission_Operation() {
		return (EAttribute)rteP_PacketDriverEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRTEP_PacketDriver_Token_Transmission_Retries() {
		return (EAttribute)rteP_PacketDriverEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSecondary_Scheduler() {
		return secondary_SchedulerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSecondary_Scheduler_Fixed_Priority_Scheduler() {
		return (EReference)secondary_SchedulerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSecondary_Scheduler_EDF_Scheduler() {
		return (EReference)secondary_SchedulerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSecondary_Scheduler_FP_Packet_Based_Scheduler() {
		return (EReference)secondary_SchedulerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSecondary_Scheduler_Host() {
		return (EAttribute)secondary_SchedulerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSecondary_Scheduler_Name() {
		return (EAttribute)secondary_SchedulerEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimple_Operation() {
		return simple_OperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimple_Operation_Overridden_Fixed_Priority() {
		return (EReference)simple_OperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimple_Operation_Overridden_Permanent_FP() {
		return (EReference)simple_OperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimple_Operation_Shared_Resources_List() {
		return (EAttribute)simple_OperationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimple_Operation_Shared_Resources_To_Lock() {
		return (EAttribute)simple_OperationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimple_Operation_Shared_Resources_To_Unlock() {
		return (EAttribute)simple_OperationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimple_Operation_Average_Case_Execution_Time() {
		return (EAttribute)simple_OperationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimple_Operation_Best_Case_Execution_Time() {
		return (EAttribute)simple_OperationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimple_Operation_Name() {
		return (EAttribute)simple_OperationEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimple_Operation_Worst_Case_Execution_Time() {
		return (EAttribute)simple_OperationEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSingular_External_Event() {
		return singular_External_EventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSingular_External_Event_Name() {
		return (EAttribute)singular_External_EventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSingular_External_Event_Phase() {
		return (EAttribute)singular_External_EventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSoft_Global_Deadline() {
		return soft_Global_DeadlineEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSoft_Global_Deadline_Deadline() {
		return (EAttribute)soft_Global_DeadlineEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSoft_Global_Deadline_Referenced_Event() {
		return (EAttribute)soft_Global_DeadlineEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSoft_Local_Deadline() {
		return soft_Local_DeadlineEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSoft_Local_Deadline_Deadline() {
		return (EAttribute)soft_Local_DeadlineEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSporadic_External_Event() {
		return sporadic_External_EventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSporadic_External_Event_Avg_Interarrival() {
		return (EAttribute)sporadic_External_EventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSporadic_External_Event_Distribution() {
		return (EAttribute)sporadic_External_EventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSporadic_External_Event_Min_Interarrival() {
		return (EAttribute)sporadic_External_EventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSporadic_External_Event_Name() {
		return (EAttribute)sporadic_External_EventEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSporadic_Server_Policy() {
		return sporadic_Server_PolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSporadic_Server_Policy_Background_Priority() {
		return (EAttribute)sporadic_Server_PolicyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSporadic_Server_Policy_Initial_Capacity() {
		return (EAttribute)sporadic_Server_PolicyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSporadic_Server_Policy_Max_Pending_Replenishments() {
		return (EAttribute)sporadic_Server_PolicyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSporadic_Server_Policy_Normal_Priority() {
		return (EAttribute)sporadic_Server_PolicyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSporadic_Server_Policy_Preassigned() {
		return (EAttribute)sporadic_Server_PolicyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSporadic_Server_Policy_Replenishment_Period() {
		return (EAttribute)sporadic_Server_PolicyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSRP_Parameters() {
		return srP_ParametersEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSRP_Parameters_Preassigned() {
		return (EAttribute)srP_ParametersEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSRP_Parameters_Preemption_Level() {
		return (EAttribute)srP_ParametersEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSRP_Resource() {
		return srP_ResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSRP_Resource_Name() {
		return (EAttribute)srP_ResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSRP_Resource_Preassigned() {
		return (EAttribute)srP_ResourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSRP_Resource_Preemption_Level() {
		return (EAttribute)srP_ResourceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSystem_Timed_Activity() {
		return system_Timed_ActivityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSystem_Timed_Activity_Activity_Operation() {
		return (EAttribute)system_Timed_ActivityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSystem_Timed_Activity_Activity_Server() {
		return (EAttribute)system_Timed_ActivityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSystem_Timed_Activity_Input_Event() {
		return (EAttribute)system_Timed_ActivityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSystem_Timed_Activity_Output_Event() {
		return (EAttribute)system_Timed_ActivityEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTicker_System_Timer() {
		return ticker_System_TimerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTicker_System_Timer_Avg_Overhead() {
		return (EAttribute)ticker_System_TimerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTicker_System_Timer_Best_Overhead() {
		return (EAttribute)ticker_System_TimerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTicker_System_Timer_Period() {
		return (EAttribute)ticker_System_TimerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTicker_System_Timer_Worst_Overhead() {
		return (EAttribute)ticker_System_TimerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnbounded_External_Event() {
		return unbounded_External_EventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUnbounded_External_Event_Avg_Interarrival() {
		return (EAttribute)unbounded_External_EventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUnbounded_External_Event_Distribution() {
		return (EAttribute)unbounded_External_EventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUnbounded_External_Event_Name() {
		return (EAttribute)unbounded_External_EventEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAffirmative_Assertion() {
		return affirmative_AssertionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAssertion() {
		return assertionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDelivery_Policy() {
		return delivery_PolicyEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDistribution_Type() {
		return distribution_TypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getNegative_Assertion() {
		return negative_AssertionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getOverhead_Type() {
		return overhead_TypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPriority_Inheritance_Protocol() {
		return priority_Inheritance_ProtocolEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRequest_Policy() {
		return request_PolicyEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTransmission_Type() {
		return transmission_TypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getAbsolute_Time() {
		return absolute_TimeEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getAffirmative_Assertion_Object() {
		return affirmative_Assertion_ObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getAny_Priority() {
		return any_PriorityEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getAssertion_Object() {
		return assertion_ObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getBit_Count() {
		return bit_CountEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getDate_Time() {
		return date_TimeEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getDelivery_Policy_Object() {
		return delivery_Policy_ObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getDistribution_Type_Object() {
		return distribution_Type_ObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getFloat() {
		return floatEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getIdentifier() {
		return identifierEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getIdentifier_Ref() {
		return identifier_RefEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getIdentifier_Ref_List() {
		return identifier_Ref_ListEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getInterrupt_Priority() {
		return interrupt_PriorityEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getNatural() {
		return naturalEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getNegative_Assertion_Object() {
		return negative_Assertion_ObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getNormalized_Execution_Time() {
		return normalized_Execution_TimeEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getOverhead_Type_Object() {
		return overhead_Type_ObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getPathname() {
		return pathnameEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getPercentage() {
		return percentageEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getPositive() {
		return positiveEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getPreemption_Level() {
		return preemption_LevelEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getPriority() {
		return priorityEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getPriority_Inheritance_Protocol_Object() {
		return priority_Inheritance_Protocol_ObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getRequest_Policy_Object() {
		return request_Policy_ObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getThroughput() {
		return throughputEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getTime() {
		return timeEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getTransmission_Type_Object() {
		return transmission_Type_ObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelFactory getModelFactory() {
		return (ModelFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		activityEClass = createEClass(ACTIVITY);
		createEAttribute(activityEClass, ACTIVITY__ACTIVITY_OPERATION);
		createEAttribute(activityEClass, ACTIVITY__ACTIVITY_SERVER);
		createEAttribute(activityEClass, ACTIVITY__INPUT_EVENT);
		createEAttribute(activityEClass, ACTIVITY__OUTPUT_EVENT);

		alarm_Clock_System_TimerEClass = createEClass(ALARM_CLOCK_SYSTEM_TIMER);
		createEAttribute(alarm_Clock_System_TimerEClass, ALARM_CLOCK_SYSTEM_TIMER__AVG_OVERHEAD);
		createEAttribute(alarm_Clock_System_TimerEClass, ALARM_CLOCK_SYSTEM_TIMER__BEST_OVERHEAD);
		createEAttribute(alarm_Clock_System_TimerEClass, ALARM_CLOCK_SYSTEM_TIMER__WORST_OVERHEAD);

		barrierEClass = createEClass(BARRIER);
		createEAttribute(barrierEClass, BARRIER__INPUT_EVENTS_LIST);
		createEAttribute(barrierEClass, BARRIER__OUTPUT_EVENT);

		bursty_External_EventEClass = createEClass(BURSTY_EXTERNAL_EVENT);
		createEAttribute(bursty_External_EventEClass, BURSTY_EXTERNAL_EVENT__AVG_INTERARRIVAL);
		createEAttribute(bursty_External_EventEClass, BURSTY_EXTERNAL_EVENT__BOUND_INTERVAL);
		createEAttribute(bursty_External_EventEClass, BURSTY_EXTERNAL_EVENT__DISTRIBUTION);
		createEAttribute(bursty_External_EventEClass, BURSTY_EXTERNAL_EVENT__MAX_ARRIVALS);
		createEAttribute(bursty_External_EventEClass, BURSTY_EXTERNAL_EVENT__NAME);

		character_Packet_DriverEClass = createEClass(CHARACTER_PACKET_DRIVER);
		createEAttribute(character_Packet_DriverEClass, CHARACTER_PACKET_DRIVER__CHARACTER_RECEIVE_OPERATION);
		createEAttribute(character_Packet_DriverEClass, CHARACTER_PACKET_DRIVER__CHARACTER_SEND_OPERATION);
		createEAttribute(character_Packet_DriverEClass, CHARACTER_PACKET_DRIVER__CHARACTER_SERVER);
		createEAttribute(character_Packet_DriverEClass, CHARACTER_PACKET_DRIVER__CHARACTER_TRANSMISSION_TIME);
		createEAttribute(character_Packet_DriverEClass, CHARACTER_PACKET_DRIVER__MESSAGE_PARTITIONING);
		createEAttribute(character_Packet_DriverEClass, CHARACTER_PACKET_DRIVER__PACKET_RECEIVE_OPERATION);
		createEAttribute(character_Packet_DriverEClass, CHARACTER_PACKET_DRIVER__PACKET_SEND_OPERATION);
		createEAttribute(character_Packet_DriverEClass, CHARACTER_PACKET_DRIVER__PACKET_SERVER);
		createEAttribute(character_Packet_DriverEClass, CHARACTER_PACKET_DRIVER__RTA_OVERHEAD_MODEL);

		composite_OperationEClass = createEClass(COMPOSITE_OPERATION);
		createEReference(composite_OperationEClass, COMPOSITE_OPERATION__OVERRIDDEN_FIXED_PRIORITY);
		createEReference(composite_OperationEClass, COMPOSITE_OPERATION__OVERRIDDEN_PERMANENT_FP);
		createEAttribute(composite_OperationEClass, COMPOSITE_OPERATION__OPERATION_LIST);
		createEAttribute(composite_OperationEClass, COMPOSITE_OPERATION__NAME);

		composite_Timing_RequirementEClass = createEClass(COMPOSITE_TIMING_REQUIREMENT);
		createEAttribute(composite_Timing_RequirementEClass, COMPOSITE_TIMING_REQUIREMENT__GROUP);
		createEReference(composite_Timing_RequirementEClass, COMPOSITE_TIMING_REQUIREMENT__MAX_OUTPUT_JITTER_REQ);
		createEReference(composite_Timing_RequirementEClass, COMPOSITE_TIMING_REQUIREMENT__HARD_GLOBAL_DEADLINE);
		createEReference(composite_Timing_RequirementEClass, COMPOSITE_TIMING_REQUIREMENT__SOFT_GLOBAL_DEADLINE);
		createEReference(composite_Timing_RequirementEClass, COMPOSITE_TIMING_REQUIREMENT__GLOBAL_MAX_MISS_RATIO);
		createEReference(composite_Timing_RequirementEClass, COMPOSITE_TIMING_REQUIREMENT__HARD_LOCAL_DEADLINE);
		createEReference(composite_Timing_RequirementEClass, COMPOSITE_TIMING_REQUIREMENT__SOFT_LOCAL_DEADLINE);
		createEReference(composite_Timing_RequirementEClass, COMPOSITE_TIMING_REQUIREMENT__LOCAL_MAX_MISS_RATIO);

		concentratorEClass = createEClass(CONCENTRATOR);
		createEAttribute(concentratorEClass, CONCENTRATOR__INPUT_EVENTS_LIST);
		createEAttribute(concentratorEClass, CONCENTRATOR__OUTPUT_EVENT);

		delayEClass = createEClass(DELAY);
		createEAttribute(delayEClass, DELAY__DELAY_MAX_INTERVAL);
		createEAttribute(delayEClass, DELAY__DELAY_MIN_INTERVAL);
		createEAttribute(delayEClass, DELAY__INPUT_EVENT);
		createEAttribute(delayEClass, DELAY__OUTPUT_EVENT);

		delivery_ServerEClass = createEClass(DELIVERY_SERVER);
		createEAttribute(delivery_ServerEClass, DELIVERY_SERVER__DELIVERY_POLICY);
		createEAttribute(delivery_ServerEClass, DELIVERY_SERVER__INPUT_EVENT);
		createEAttribute(delivery_ServerEClass, DELIVERY_SERVER__OUTPUT_EVENTS_LIST);

		document_RootEClass = createEClass(DOCUMENT_ROOT);
		createEAttribute(document_RootEClass, DOCUMENT_ROOT__MIXED);
		createEReference(document_RootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		createEReference(document_RootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		createEReference(document_RootEClass, DOCUMENT_ROOT__MAST_MODEL);

		edF_PolicyEClass = createEClass(EDF_POLICY);
		createEAttribute(edF_PolicyEClass, EDF_POLICY__DEADLINE);
		createEAttribute(edF_PolicyEClass, EDF_POLICY__PREASSIGNED);

		edF_SchedulerEClass = createEClass(EDF_SCHEDULER);
		createEAttribute(edF_SchedulerEClass, EDF_SCHEDULER__AVG_CONTEXT_SWITCH);
		createEAttribute(edF_SchedulerEClass, EDF_SCHEDULER__BEST_CONTEXT_SWITCH);
		createEAttribute(edF_SchedulerEClass, EDF_SCHEDULER__WORST_CONTEXT_SWITCH);

		enclosing_OperationEClass = createEClass(ENCLOSING_OPERATION);
		createEReference(enclosing_OperationEClass, ENCLOSING_OPERATION__OVERRIDDEN_FIXED_PRIORITY);
		createEReference(enclosing_OperationEClass, ENCLOSING_OPERATION__OVERRIDDEN_PERMANENT_FP);
		createEAttribute(enclosing_OperationEClass, ENCLOSING_OPERATION__OPERATION_LIST);
		createEAttribute(enclosing_OperationEClass, ENCLOSING_OPERATION__AVERAGE_CASE_EXECUTION_TIME);
		createEAttribute(enclosing_OperationEClass, ENCLOSING_OPERATION__BEST_CASE_EXECUTION_TIME);
		createEAttribute(enclosing_OperationEClass, ENCLOSING_OPERATION__NAME);
		createEAttribute(enclosing_OperationEClass, ENCLOSING_OPERATION__WORST_CASE_EXECUTION_TIME);

		fixed_Priority_PolicyEClass = createEClass(FIXED_PRIORITY_POLICY);
		createEAttribute(fixed_Priority_PolicyEClass, FIXED_PRIORITY_POLICY__PREASSIGNED);
		createEAttribute(fixed_Priority_PolicyEClass, FIXED_PRIORITY_POLICY__THE_PRIORITY);

		fixed_Priority_SchedulerEClass = createEClass(FIXED_PRIORITY_SCHEDULER);
		createEAttribute(fixed_Priority_SchedulerEClass, FIXED_PRIORITY_SCHEDULER__AVG_CONTEXT_SWITCH);
		createEAttribute(fixed_Priority_SchedulerEClass, FIXED_PRIORITY_SCHEDULER__BEST_CONTEXT_SWITCH);
		createEAttribute(fixed_Priority_SchedulerEClass, FIXED_PRIORITY_SCHEDULER__MAX_PRIORITY);
		createEAttribute(fixed_Priority_SchedulerEClass, FIXED_PRIORITY_SCHEDULER__MIN_PRIORITY);
		createEAttribute(fixed_Priority_SchedulerEClass, FIXED_PRIORITY_SCHEDULER__WORST_CONTEXT_SWITCH);

		fP_Packet_Based_SchedulerEClass = createEClass(FP_PACKET_BASED_SCHEDULER);
		createEAttribute(fP_Packet_Based_SchedulerEClass, FP_PACKET_BASED_SCHEDULER__MAX_PRIORITY);
		createEAttribute(fP_Packet_Based_SchedulerEClass, FP_PACKET_BASED_SCHEDULER__MIN_PRIORITY);
		createEAttribute(fP_Packet_Based_SchedulerEClass, FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_AVG_SIZE);
		createEAttribute(fP_Packet_Based_SchedulerEClass, FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_MAX_SIZE);
		createEAttribute(fP_Packet_Based_SchedulerEClass, FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_MIN_SIZE);

		global_Max_Miss_RatioEClass = createEClass(GLOBAL_MAX_MISS_RATIO);
		createEAttribute(global_Max_Miss_RatioEClass, GLOBAL_MAX_MISS_RATIO__DEADLINE);
		createEAttribute(global_Max_Miss_RatioEClass, GLOBAL_MAX_MISS_RATIO__RATIO);
		createEAttribute(global_Max_Miss_RatioEClass, GLOBAL_MAX_MISS_RATIO__REFERENCED_EVENT);

		hard_Global_DeadlineEClass = createEClass(HARD_GLOBAL_DEADLINE);
		createEAttribute(hard_Global_DeadlineEClass, HARD_GLOBAL_DEADLINE__DEADLINE);
		createEAttribute(hard_Global_DeadlineEClass, HARD_GLOBAL_DEADLINE__REFERENCED_EVENT);

		hard_Local_DeadlineEClass = createEClass(HARD_LOCAL_DEADLINE);
		createEAttribute(hard_Local_DeadlineEClass, HARD_LOCAL_DEADLINE__DEADLINE);

		immediate_Ceiling_ResourceEClass = createEClass(IMMEDIATE_CEILING_RESOURCE);
		createEAttribute(immediate_Ceiling_ResourceEClass, IMMEDIATE_CEILING_RESOURCE__CEILING);
		createEAttribute(immediate_Ceiling_ResourceEClass, IMMEDIATE_CEILING_RESOURCE__NAME);
		createEAttribute(immediate_Ceiling_ResourceEClass, IMMEDIATE_CEILING_RESOURCE__PREASSIGNED);

		interrupt_FP_PolicyEClass = createEClass(INTERRUPT_FP_POLICY);
		createEAttribute(interrupt_FP_PolicyEClass, INTERRUPT_FP_POLICY__PREASSIGNED);
		createEAttribute(interrupt_FP_PolicyEClass, INTERRUPT_FP_POLICY__THE_PRIORITY);

		list_of_DriversEClass = createEClass(LIST_OF_DRIVERS);
		createEAttribute(list_of_DriversEClass, LIST_OF_DRIVERS__GROUP);
		createEReference(list_of_DriversEClass, LIST_OF_DRIVERS__PACKET_DRIVER);
		createEReference(list_of_DriversEClass, LIST_OF_DRIVERS__CHARACTER_PACKET_DRIVER);
		createEReference(list_of_DriversEClass, LIST_OF_DRIVERS__RTEP_PACKET_DRIVER);

		local_Max_Miss_RatioEClass = createEClass(LOCAL_MAX_MISS_RATIO);
		createEAttribute(local_Max_Miss_RatioEClass, LOCAL_MAX_MISS_RATIO__DEADLINE);
		createEAttribute(local_Max_Miss_RatioEClass, LOCAL_MAX_MISS_RATIO__RATIO);

		masT_MODELEClass = createEClass(MAST_MODEL);
		createEAttribute(masT_MODELEClass, MAST_MODEL__GROUP);
		createEReference(masT_MODELEClass, MAST_MODEL__REGULAR_PROCESSOR);
		createEReference(masT_MODELEClass, MAST_MODEL__PACKET_BASED_NETWORK);
		createEReference(masT_MODELEClass, MAST_MODEL__REGULAR_SCHEDULING_SERVER);
		createEReference(masT_MODELEClass, MAST_MODEL__IMMEDIATE_CEILING_RESOURCE);
		createEReference(masT_MODELEClass, MAST_MODEL__PRIORITY_INHERITANCE_RESOURCE);
		createEReference(masT_MODELEClass, MAST_MODEL__SRP_RESOURCE);
		createEReference(masT_MODELEClass, MAST_MODEL__SIMPLE_OPERATION);
		createEReference(masT_MODELEClass, MAST_MODEL__MESSAGE_TRANSMISSION);
		createEReference(masT_MODELEClass, MAST_MODEL__COMPOSITE_OPERATION);
		createEReference(masT_MODELEClass, MAST_MODEL__ENCLOSING_OPERATION);
		createEReference(masT_MODELEClass, MAST_MODEL__REGULAR_TRANSACTION);
		createEReference(masT_MODELEClass, MAST_MODEL__PRIMARY_SCHEDULER);
		createEReference(masT_MODELEClass, MAST_MODEL__SECONDARY_SCHEDULER);
		createEAttribute(masT_MODELEClass, MAST_MODEL__MODEL_DATE);
		createEAttribute(masT_MODELEClass, MAST_MODEL__MODEL_NAME);
		createEAttribute(masT_MODELEClass, MAST_MODEL__SYSTEM_PI_PBEHAVIOUR);

		max_Output_Jitter_ReqEClass = createEClass(MAX_OUTPUT_JITTER_REQ);
		createEAttribute(max_Output_Jitter_ReqEClass, MAX_OUTPUT_JITTER_REQ__MAX_OUTPUT_JITTER);
		createEAttribute(max_Output_Jitter_ReqEClass, MAX_OUTPUT_JITTER_REQ__REFERENCED_EVENT);

		message_TransmissionEClass = createEClass(MESSAGE_TRANSMISSION);
		createEReference(message_TransmissionEClass, MESSAGE_TRANSMISSION__OVERRIDDEN_FIXED_PRIORITY);
		createEReference(message_TransmissionEClass, MESSAGE_TRANSMISSION__OVERRIDDEN_PERMANENT_FP);
		createEAttribute(message_TransmissionEClass, MESSAGE_TRANSMISSION__AVG_MESSAGE_SIZE);
		createEAttribute(message_TransmissionEClass, MESSAGE_TRANSMISSION__MAX_MESSAGE_SIZE);
		createEAttribute(message_TransmissionEClass, MESSAGE_TRANSMISSION__MIN_MESSAGE_SIZE);
		createEAttribute(message_TransmissionEClass, MESSAGE_TRANSMISSION__NAME);

		multicastEClass = createEClass(MULTICAST);
		createEAttribute(multicastEClass, MULTICAST__INPUT_EVENT);
		createEAttribute(multicastEClass, MULTICAST__OUTPUT_EVENTS_LIST);

		non_Preemptible_FP_PolicyEClass = createEClass(NON_PREEMPTIBLE_FP_POLICY);
		createEAttribute(non_Preemptible_FP_PolicyEClass, NON_PREEMPTIBLE_FP_POLICY__PREASSIGNED);
		createEAttribute(non_Preemptible_FP_PolicyEClass, NON_PREEMPTIBLE_FP_POLICY__THE_PRIORITY);

		offsetEClass = createEClass(OFFSET);
		createEAttribute(offsetEClass, OFFSET__DELAY_MAX_INTERVAL);
		createEAttribute(offsetEClass, OFFSET__DELAY_MIN_INTERVAL);
		createEAttribute(offsetEClass, OFFSET__INPUT_EVENT);
		createEAttribute(offsetEClass, OFFSET__OUTPUT_EVENT);
		createEAttribute(offsetEClass, OFFSET__REFERENCED_EVENT);

		overridden_Fixed_PriorityEClass = createEClass(OVERRIDDEN_FIXED_PRIORITY);
		createEAttribute(overridden_Fixed_PriorityEClass, OVERRIDDEN_FIXED_PRIORITY__THE_PRIORITY);

		overridden_Permanent_FPEClass = createEClass(OVERRIDDEN_PERMANENT_FP);
		createEAttribute(overridden_Permanent_FPEClass, OVERRIDDEN_PERMANENT_FP__THE_PRIORITY);

		packet_Based_NetworkEClass = createEClass(PACKET_BASED_NETWORK);
		createEReference(packet_Based_NetworkEClass, PACKET_BASED_NETWORK__LIST_OF_DRIVERS);
		createEAttribute(packet_Based_NetworkEClass, PACKET_BASED_NETWORK__MAX_BLOCKING);
		createEAttribute(packet_Based_NetworkEClass, PACKET_BASED_NETWORK__MAX_PACKET_SIZE);
		createEAttribute(packet_Based_NetworkEClass, PACKET_BASED_NETWORK__MIN_PACKET_SIZE);
		createEAttribute(packet_Based_NetworkEClass, PACKET_BASED_NETWORK__NAME);
		createEAttribute(packet_Based_NetworkEClass, PACKET_BASED_NETWORK__SPEED_FACTOR);
		createEAttribute(packet_Based_NetworkEClass, PACKET_BASED_NETWORK__THROUGHPUT);
		createEAttribute(packet_Based_NetworkEClass, PACKET_BASED_NETWORK__TRANSMISSION);

		packet_DriverEClass = createEClass(PACKET_DRIVER);
		createEAttribute(packet_DriverEClass, PACKET_DRIVER__MESSAGE_PARTITIONING);
		createEAttribute(packet_DriverEClass, PACKET_DRIVER__PACKET_RECEIVE_OPERATION);
		createEAttribute(packet_DriverEClass, PACKET_DRIVER__PACKET_SEND_OPERATION);
		createEAttribute(packet_DriverEClass, PACKET_DRIVER__PACKET_SERVER);
		createEAttribute(packet_DriverEClass, PACKET_DRIVER__RTA_OVERHEAD_MODEL);

		periodic_External_EventEClass = createEClass(PERIODIC_EXTERNAL_EVENT);
		createEAttribute(periodic_External_EventEClass, PERIODIC_EXTERNAL_EVENT__MAX_JITTER);
		createEAttribute(periodic_External_EventEClass, PERIODIC_EXTERNAL_EVENT__NAME);
		createEAttribute(periodic_External_EventEClass, PERIODIC_EXTERNAL_EVENT__PERIOD);
		createEAttribute(periodic_External_EventEClass, PERIODIC_EXTERNAL_EVENT__PHASE);

		polling_PolicyEClass = createEClass(POLLING_POLICY);
		createEAttribute(polling_PolicyEClass, POLLING_POLICY__POLLING_AVG_OVERHEAD);
		createEAttribute(polling_PolicyEClass, POLLING_POLICY__POLLING_BEST_OVERHEAD);
		createEAttribute(polling_PolicyEClass, POLLING_POLICY__POLLING_PERIOD);
		createEAttribute(polling_PolicyEClass, POLLING_POLICY__POLLING_WORST_OVERHEAD);
		createEAttribute(polling_PolicyEClass, POLLING_POLICY__PREASSIGNED);
		createEAttribute(polling_PolicyEClass, POLLING_POLICY__THE_PRIORITY);

		primary_SchedulerEClass = createEClass(PRIMARY_SCHEDULER);
		createEReference(primary_SchedulerEClass, PRIMARY_SCHEDULER__FIXED_PRIORITY_SCHEDULER);
		createEReference(primary_SchedulerEClass, PRIMARY_SCHEDULER__EDF_SCHEDULER);
		createEReference(primary_SchedulerEClass, PRIMARY_SCHEDULER__FP_PACKET_BASED_SCHEDULER);
		createEAttribute(primary_SchedulerEClass, PRIMARY_SCHEDULER__HOST);
		createEAttribute(primary_SchedulerEClass, PRIMARY_SCHEDULER__NAME);

		priority_Inheritance_ResourceEClass = createEClass(PRIORITY_INHERITANCE_RESOURCE);
		createEAttribute(priority_Inheritance_ResourceEClass, PRIORITY_INHERITANCE_RESOURCE__NAME);

		query_ServerEClass = createEClass(QUERY_SERVER);
		createEAttribute(query_ServerEClass, QUERY_SERVER__INPUT_EVENT);
		createEAttribute(query_ServerEClass, QUERY_SERVER__OUTPUT_EVENTS_LIST);
		createEAttribute(query_ServerEClass, QUERY_SERVER__REQUEST_POLICY);

		rate_DivisorEClass = createEClass(RATE_DIVISOR);
		createEAttribute(rate_DivisorEClass, RATE_DIVISOR__INPUT_EVENT);
		createEAttribute(rate_DivisorEClass, RATE_DIVISOR__OUTPUT_EVENT);
		createEAttribute(rate_DivisorEClass, RATE_DIVISOR__RATE_FACTOR);

		regular_EventEClass = createEClass(REGULAR_EVENT);
		createEAttribute(regular_EventEClass, REGULAR_EVENT__GROUP);
		createEReference(regular_EventEClass, REGULAR_EVENT__MAX_OUTPUT_JITTER_REQ);
		createEReference(regular_EventEClass, REGULAR_EVENT__HARD_GLOBAL_DEADLINE);
		createEReference(regular_EventEClass, REGULAR_EVENT__SOFT_GLOBAL_DEADLINE);
		createEReference(regular_EventEClass, REGULAR_EVENT__GLOBAL_MAX_MISS_RATIO);
		createEReference(regular_EventEClass, REGULAR_EVENT__HARD_LOCAL_DEADLINE);
		createEReference(regular_EventEClass, REGULAR_EVENT__SOFT_LOCAL_DEADLINE);
		createEReference(regular_EventEClass, REGULAR_EVENT__LOCAL_MAX_MISS_RATIO);
		createEReference(regular_EventEClass, REGULAR_EVENT__COMPOSITE_TIMING_REQUIREMENT);
		createEAttribute(regular_EventEClass, REGULAR_EVENT__EVENT);

		regular_ProcessorEClass = createEClass(REGULAR_PROCESSOR);
		createEReference(regular_ProcessorEClass, REGULAR_PROCESSOR__TICKER_SYSTEM_TIMER);
		createEReference(regular_ProcessorEClass, REGULAR_PROCESSOR__ALARM_CLOCK_SYSTEM_TIMER);
		createEAttribute(regular_ProcessorEClass, REGULAR_PROCESSOR__AVG_ISR_SWITCH);
		createEAttribute(regular_ProcessorEClass, REGULAR_PROCESSOR__BEST_ISR_SWITCH);
		createEAttribute(regular_ProcessorEClass, REGULAR_PROCESSOR__MAX_INTERRUPT_PRIORITY);
		createEAttribute(regular_ProcessorEClass, REGULAR_PROCESSOR__MIN_INTERRUPT_PRIORITY);
		createEAttribute(regular_ProcessorEClass, REGULAR_PROCESSOR__NAME);
		createEAttribute(regular_ProcessorEClass, REGULAR_PROCESSOR__SPEED_FACTOR);
		createEAttribute(regular_ProcessorEClass, REGULAR_PROCESSOR__WORST_ISR_SWITCH);

		regular_Scheduling_ServerEClass = createEClass(REGULAR_SCHEDULING_SERVER);
		createEReference(regular_Scheduling_ServerEClass, REGULAR_SCHEDULING_SERVER__NON_PREEMPTIBLE_FP_POLICY);
		createEReference(regular_Scheduling_ServerEClass, REGULAR_SCHEDULING_SERVER__FIXED_PRIORITY_POLICY);
		createEReference(regular_Scheduling_ServerEClass, REGULAR_SCHEDULING_SERVER__INTERRUPT_FP_POLICY);
		createEReference(regular_Scheduling_ServerEClass, REGULAR_SCHEDULING_SERVER__POLLING_POLICY);
		createEReference(regular_Scheduling_ServerEClass, REGULAR_SCHEDULING_SERVER__SPORADIC_SERVER_POLICY);
		createEReference(regular_Scheduling_ServerEClass, REGULAR_SCHEDULING_SERVER__EDF_POLICY);
		createEReference(regular_Scheduling_ServerEClass, REGULAR_SCHEDULING_SERVER__SRP_PARAMETERS);
		createEAttribute(regular_Scheduling_ServerEClass, REGULAR_SCHEDULING_SERVER__NAME);
		createEAttribute(regular_Scheduling_ServerEClass, REGULAR_SCHEDULING_SERVER__SCHEDULER);

		regular_TransactionEClass = createEClass(REGULAR_TRANSACTION);
		createEAttribute(regular_TransactionEClass, REGULAR_TRANSACTION__GROUP);
		createEReference(regular_TransactionEClass, REGULAR_TRANSACTION__PERIODIC_EXTERNAL_EVENT);
		createEReference(regular_TransactionEClass, REGULAR_TRANSACTION__SPORADIC_EXTERNAL_EVENT);
		createEReference(regular_TransactionEClass, REGULAR_TRANSACTION__UNBOUNDED_EXTERNAL_EVENT);
		createEReference(regular_TransactionEClass, REGULAR_TRANSACTION__BURSTY_EXTERNAL_EVENT);
		createEReference(regular_TransactionEClass, REGULAR_TRANSACTION__SINGULAR_EXTERNAL_EVENT);
		createEReference(regular_TransactionEClass, REGULAR_TRANSACTION__REGULAR_EVENT);
		createEReference(regular_TransactionEClass, REGULAR_TRANSACTION__ACTIVITY);
		createEReference(regular_TransactionEClass, REGULAR_TRANSACTION__SYSTEM_TIMED_ACTIVITY);
		createEReference(regular_TransactionEClass, REGULAR_TRANSACTION__CONCENTRATOR);
		createEReference(regular_TransactionEClass, REGULAR_TRANSACTION__BARRIER);
		createEReference(regular_TransactionEClass, REGULAR_TRANSACTION__DELIVERY_SERVER);
		createEReference(regular_TransactionEClass, REGULAR_TRANSACTION__QUERY_SERVER);
		createEReference(regular_TransactionEClass, REGULAR_TRANSACTION__MULTICAST);
		createEReference(regular_TransactionEClass, REGULAR_TRANSACTION__RATE_DIVISOR);
		createEReference(regular_TransactionEClass, REGULAR_TRANSACTION__DELAY);
		createEReference(regular_TransactionEClass, REGULAR_TRANSACTION__OFFSET);
		createEAttribute(regular_TransactionEClass, REGULAR_TRANSACTION__NAME);

		rteP_PacketDriverEClass = createEClass(RTEP_PACKET_DRIVER);
		createEAttribute(rteP_PacketDriverEClass, RTEP_PACKET_DRIVER__FAILURE_TIMEOUT);
		createEAttribute(rteP_PacketDriverEClass, RTEP_PACKET_DRIVER__MESSAGE_PARTITIONING);
		createEAttribute(rteP_PacketDriverEClass, RTEP_PACKET_DRIVER__NUMBER_OF_STATIONS);
		createEAttribute(rteP_PacketDriverEClass, RTEP_PACKET_DRIVER__PACKET_DISCARD_OPERATION);
		createEAttribute(rteP_PacketDriverEClass, RTEP_PACKET_DRIVER__PACKET_INTERRUPT_SERVER);
		createEAttribute(rteP_PacketDriverEClass, RTEP_PACKET_DRIVER__PACKET_ISR_OPERATION);
		createEAttribute(rteP_PacketDriverEClass, RTEP_PACKET_DRIVER__PACKET_RECEIVE_OPERATION);
		createEAttribute(rteP_PacketDriverEClass, RTEP_PACKET_DRIVER__PACKET_RETRANSMISSION_OPERATION);
		createEAttribute(rteP_PacketDriverEClass, RTEP_PACKET_DRIVER__PACKET_SEND_OPERATION);
		createEAttribute(rteP_PacketDriverEClass, RTEP_PACKET_DRIVER__PACKET_SERVER);
		createEAttribute(rteP_PacketDriverEClass, RTEP_PACKET_DRIVER__PACKET_TRANSMISSION_RETRIES);
		createEAttribute(rteP_PacketDriverEClass, RTEP_PACKET_DRIVER__RTA_OVERHEAD_MODEL);
		createEAttribute(rteP_PacketDriverEClass, RTEP_PACKET_DRIVER__TOKEN_CHECK_OPERATION);
		createEAttribute(rteP_PacketDriverEClass, RTEP_PACKET_DRIVER__TOKEN_DELAY);
		createEAttribute(rteP_PacketDriverEClass, RTEP_PACKET_DRIVER__TOKEN_MANAGE_OPERATION);
		createEAttribute(rteP_PacketDriverEClass, RTEP_PACKET_DRIVER__TOKEN_RETRANSMISSION_OPERATION);
		createEAttribute(rteP_PacketDriverEClass, RTEP_PACKET_DRIVER__TOKEN_TRANSMISSION_RETRIES);

		secondary_SchedulerEClass = createEClass(SECONDARY_SCHEDULER);
		createEReference(secondary_SchedulerEClass, SECONDARY_SCHEDULER__FIXED_PRIORITY_SCHEDULER);
		createEReference(secondary_SchedulerEClass, SECONDARY_SCHEDULER__EDF_SCHEDULER);
		createEReference(secondary_SchedulerEClass, SECONDARY_SCHEDULER__FP_PACKET_BASED_SCHEDULER);
		createEAttribute(secondary_SchedulerEClass, SECONDARY_SCHEDULER__HOST);
		createEAttribute(secondary_SchedulerEClass, SECONDARY_SCHEDULER__NAME);

		simple_OperationEClass = createEClass(SIMPLE_OPERATION);
		createEReference(simple_OperationEClass, SIMPLE_OPERATION__OVERRIDDEN_FIXED_PRIORITY);
		createEReference(simple_OperationEClass, SIMPLE_OPERATION__OVERRIDDEN_PERMANENT_FP);
		createEAttribute(simple_OperationEClass, SIMPLE_OPERATION__SHARED_RESOURCES_LIST);
		createEAttribute(simple_OperationEClass, SIMPLE_OPERATION__SHARED_RESOURCES_TO_LOCK);
		createEAttribute(simple_OperationEClass, SIMPLE_OPERATION__SHARED_RESOURCES_TO_UNLOCK);
		createEAttribute(simple_OperationEClass, SIMPLE_OPERATION__AVERAGE_CASE_EXECUTION_TIME);
		createEAttribute(simple_OperationEClass, SIMPLE_OPERATION__BEST_CASE_EXECUTION_TIME);
		createEAttribute(simple_OperationEClass, SIMPLE_OPERATION__NAME);
		createEAttribute(simple_OperationEClass, SIMPLE_OPERATION__WORST_CASE_EXECUTION_TIME);

		singular_External_EventEClass = createEClass(SINGULAR_EXTERNAL_EVENT);
		createEAttribute(singular_External_EventEClass, SINGULAR_EXTERNAL_EVENT__NAME);
		createEAttribute(singular_External_EventEClass, SINGULAR_EXTERNAL_EVENT__PHASE);

		soft_Global_DeadlineEClass = createEClass(SOFT_GLOBAL_DEADLINE);
		createEAttribute(soft_Global_DeadlineEClass, SOFT_GLOBAL_DEADLINE__DEADLINE);
		createEAttribute(soft_Global_DeadlineEClass, SOFT_GLOBAL_DEADLINE__REFERENCED_EVENT);

		soft_Local_DeadlineEClass = createEClass(SOFT_LOCAL_DEADLINE);
		createEAttribute(soft_Local_DeadlineEClass, SOFT_LOCAL_DEADLINE__DEADLINE);

		sporadic_External_EventEClass = createEClass(SPORADIC_EXTERNAL_EVENT);
		createEAttribute(sporadic_External_EventEClass, SPORADIC_EXTERNAL_EVENT__AVG_INTERARRIVAL);
		createEAttribute(sporadic_External_EventEClass, SPORADIC_EXTERNAL_EVENT__DISTRIBUTION);
		createEAttribute(sporadic_External_EventEClass, SPORADIC_EXTERNAL_EVENT__MIN_INTERARRIVAL);
		createEAttribute(sporadic_External_EventEClass, SPORADIC_EXTERNAL_EVENT__NAME);

		sporadic_Server_PolicyEClass = createEClass(SPORADIC_SERVER_POLICY);
		createEAttribute(sporadic_Server_PolicyEClass, SPORADIC_SERVER_POLICY__BACKGROUND_PRIORITY);
		createEAttribute(sporadic_Server_PolicyEClass, SPORADIC_SERVER_POLICY__INITIAL_CAPACITY);
		createEAttribute(sporadic_Server_PolicyEClass, SPORADIC_SERVER_POLICY__MAX_PENDING_REPLENISHMENTS);
		createEAttribute(sporadic_Server_PolicyEClass, SPORADIC_SERVER_POLICY__NORMAL_PRIORITY);
		createEAttribute(sporadic_Server_PolicyEClass, SPORADIC_SERVER_POLICY__PREASSIGNED);
		createEAttribute(sporadic_Server_PolicyEClass, SPORADIC_SERVER_POLICY__REPLENISHMENT_PERIOD);

		srP_ParametersEClass = createEClass(SRP_PARAMETERS);
		createEAttribute(srP_ParametersEClass, SRP_PARAMETERS__PREASSIGNED);
		createEAttribute(srP_ParametersEClass, SRP_PARAMETERS__PREEMPTION_LEVEL);

		srP_ResourceEClass = createEClass(SRP_RESOURCE);
		createEAttribute(srP_ResourceEClass, SRP_RESOURCE__NAME);
		createEAttribute(srP_ResourceEClass, SRP_RESOURCE__PREASSIGNED);
		createEAttribute(srP_ResourceEClass, SRP_RESOURCE__PREEMPTION_LEVEL);

		system_Timed_ActivityEClass = createEClass(SYSTEM_TIMED_ACTIVITY);
		createEAttribute(system_Timed_ActivityEClass, SYSTEM_TIMED_ACTIVITY__ACTIVITY_OPERATION);
		createEAttribute(system_Timed_ActivityEClass, SYSTEM_TIMED_ACTIVITY__ACTIVITY_SERVER);
		createEAttribute(system_Timed_ActivityEClass, SYSTEM_TIMED_ACTIVITY__INPUT_EVENT);
		createEAttribute(system_Timed_ActivityEClass, SYSTEM_TIMED_ACTIVITY__OUTPUT_EVENT);

		ticker_System_TimerEClass = createEClass(TICKER_SYSTEM_TIMER);
		createEAttribute(ticker_System_TimerEClass, TICKER_SYSTEM_TIMER__AVG_OVERHEAD);
		createEAttribute(ticker_System_TimerEClass, TICKER_SYSTEM_TIMER__BEST_OVERHEAD);
		createEAttribute(ticker_System_TimerEClass, TICKER_SYSTEM_TIMER__PERIOD);
		createEAttribute(ticker_System_TimerEClass, TICKER_SYSTEM_TIMER__WORST_OVERHEAD);

		unbounded_External_EventEClass = createEClass(UNBOUNDED_EXTERNAL_EVENT);
		createEAttribute(unbounded_External_EventEClass, UNBOUNDED_EXTERNAL_EVENT__AVG_INTERARRIVAL);
		createEAttribute(unbounded_External_EventEClass, UNBOUNDED_EXTERNAL_EVENT__DISTRIBUTION);
		createEAttribute(unbounded_External_EventEClass, UNBOUNDED_EXTERNAL_EVENT__NAME);

		// Create enums
		affirmative_AssertionEEnum = createEEnum(AFFIRMATIVE_ASSERTION);
		assertionEEnum = createEEnum(ASSERTION);
		delivery_PolicyEEnum = createEEnum(DELIVERY_POLICY);
		distribution_TypeEEnum = createEEnum(DISTRIBUTION_TYPE);
		negative_AssertionEEnum = createEEnum(NEGATIVE_ASSERTION);
		overhead_TypeEEnum = createEEnum(OVERHEAD_TYPE);
		priority_Inheritance_ProtocolEEnum = createEEnum(PRIORITY_INHERITANCE_PROTOCOL);
		request_PolicyEEnum = createEEnum(REQUEST_POLICY);
		transmission_TypeEEnum = createEEnum(TRANSMISSION_TYPE);

		// Create data types
		absolute_TimeEDataType = createEDataType(ABSOLUTE_TIME);
		affirmative_Assertion_ObjectEDataType = createEDataType(AFFIRMATIVE_ASSERTION_OBJECT);
		any_PriorityEDataType = createEDataType(ANY_PRIORITY);
		assertion_ObjectEDataType = createEDataType(ASSERTION_OBJECT);
		bit_CountEDataType = createEDataType(BIT_COUNT);
		date_TimeEDataType = createEDataType(DATE_TIME);
		delivery_Policy_ObjectEDataType = createEDataType(DELIVERY_POLICY_OBJECT);
		distribution_Type_ObjectEDataType = createEDataType(DISTRIBUTION_TYPE_OBJECT);
		floatEDataType = createEDataType(FLOAT);
		identifierEDataType = createEDataType(IDENTIFIER);
		identifier_RefEDataType = createEDataType(IDENTIFIER_REF);
		identifier_Ref_ListEDataType = createEDataType(IDENTIFIER_REF_LIST);
		interrupt_PriorityEDataType = createEDataType(INTERRUPT_PRIORITY);
		naturalEDataType = createEDataType(NATURAL);
		negative_Assertion_ObjectEDataType = createEDataType(NEGATIVE_ASSERTION_OBJECT);
		normalized_Execution_TimeEDataType = createEDataType(NORMALIZED_EXECUTION_TIME);
		overhead_Type_ObjectEDataType = createEDataType(OVERHEAD_TYPE_OBJECT);
		pathnameEDataType = createEDataType(PATHNAME);
		percentageEDataType = createEDataType(PERCENTAGE);
		positiveEDataType = createEDataType(POSITIVE);
		preemption_LevelEDataType = createEDataType(PREEMPTION_LEVEL);
		priorityEDataType = createEDataType(PRIORITY);
		priority_Inheritance_Protocol_ObjectEDataType = createEDataType(PRIORITY_INHERITANCE_PROTOCOL_OBJECT);
		request_Policy_ObjectEDataType = createEDataType(REQUEST_POLICY_OBJECT);
		throughputEDataType = createEDataType(THROUGHPUT);
		timeEDataType = createEDataType(TIME);
		transmission_Type_ObjectEDataType = createEDataType(TRANSMISSION_TYPE_OBJECT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(activityEClass, Activity.class, "Activity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getActivity_Activity_Operation(), this.getIdentifier_Ref(), "Activity_Operation", null, 1, 1, Activity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActivity_Activity_Server(), this.getIdentifier_Ref(), "Activity_Server", null, 1, 1, Activity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActivity_Input_Event(), this.getIdentifier_Ref(), "Input_Event", null, 1, 1, Activity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActivity_Output_Event(), this.getIdentifier_Ref(), "Output_Event", null, 1, 1, Activity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(alarm_Clock_System_TimerEClass, Alarm_Clock_System_Timer.class, "Alarm_Clock_System_Timer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAlarm_Clock_System_Timer_Avg_Overhead(), this.getNormalized_Execution_Time(), "Avg_Overhead", null, 0, 1, Alarm_Clock_System_Timer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAlarm_Clock_System_Timer_Best_Overhead(), this.getNormalized_Execution_Time(), "Best_Overhead", null, 0, 1, Alarm_Clock_System_Timer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAlarm_Clock_System_Timer_Worst_Overhead(), this.getNormalized_Execution_Time(), "Worst_Overhead", null, 0, 1, Alarm_Clock_System_Timer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(barrierEClass, Barrier.class, "Barrier", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBarrier_Input_Events_List(), this.getIdentifier_Ref_List(), "Input_Events_List", null, 1, 1, Barrier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBarrier_Output_Event(), this.getIdentifier_Ref(), "Output_Event", null, 1, 1, Barrier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bursty_External_EventEClass, Bursty_External_Event.class, "Bursty_External_Event", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBursty_External_Event_Avg_Interarrival(), this.getTime(), "Avg_Interarrival", null, 0, 1, Bursty_External_Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBursty_External_Event_Bound_Interval(), this.getTime(), "Bound_Interval", null, 0, 1, Bursty_External_Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBursty_External_Event_Distribution(), this.getDistribution_Type(), "Distribution", null, 0, 1, Bursty_External_Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBursty_External_Event_Max_Arrivals(), theXMLTypePackage.getPositiveInteger(), "Max_Arrivals", null, 0, 1, Bursty_External_Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBursty_External_Event_Name(), this.getIdentifier(), "Name", null, 1, 1, Bursty_External_Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(character_Packet_DriverEClass, Character_Packet_Driver.class, "Character_Packet_Driver", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCharacter_Packet_Driver_Character_Receive_Operation(), this.getIdentifier_Ref(), "Character_Receive_Operation", null, 0, 1, Character_Packet_Driver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCharacter_Packet_Driver_Character_Send_Operation(), this.getIdentifier_Ref(), "Character_Send_Operation", null, 0, 1, Character_Packet_Driver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCharacter_Packet_Driver_Character_Server(), this.getIdentifier_Ref(), "Character_Server", null, 1, 1, Character_Packet_Driver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCharacter_Packet_Driver_Character_Transmission_Time(), this.getTime(), "Character_Transmission_Time", null, 0, 1, Character_Packet_Driver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCharacter_Packet_Driver_Message_Partitioning(), this.getAssertion(), "Message_Partitioning", null, 0, 1, Character_Packet_Driver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCharacter_Packet_Driver_Packet_Receive_Operation(), this.getIdentifier_Ref(), "Packet_Receive_Operation", null, 0, 1, Character_Packet_Driver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCharacter_Packet_Driver_Packet_Send_Operation(), this.getIdentifier_Ref(), "Packet_Send_Operation", null, 0, 1, Character_Packet_Driver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCharacter_Packet_Driver_Packet_Server(), this.getIdentifier_Ref(), "Packet_Server", null, 1, 1, Character_Packet_Driver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCharacter_Packet_Driver_RTA_Overhead_Model(), this.getOverhead_Type(), "RTA_Overhead_Model", null, 0, 1, Character_Packet_Driver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(composite_OperationEClass, Composite_Operation.class, "Composite_Operation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComposite_Operation_Overridden_Fixed_Priority(), this.getOverridden_Fixed_Priority(), null, "Overridden_Fixed_Priority", null, 0, 1, Composite_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComposite_Operation_Overridden_Permanent_FP(), this.getOverridden_Permanent_FP(), null, "Overridden_Permanent_FP", null, 0, 1, Composite_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComposite_Operation_Operation_List(), this.getIdentifier_Ref_List(), "Operation_List", null, 1, 1, Composite_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComposite_Operation_Name(), this.getIdentifier(), "Name", null, 1, 1, Composite_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(composite_Timing_RequirementEClass, Composite_Timing_Requirement.class, "Composite_Timing_Requirement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComposite_Timing_Requirement_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, Composite_Timing_Requirement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComposite_Timing_Requirement_Max_Output_Jitter_Req(), this.getMax_Output_Jitter_Req(), null, "Max_Output_Jitter_Req", null, 0, -1, Composite_Timing_Requirement.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getComposite_Timing_Requirement_Hard_Global_Deadline(), this.getHard_Global_Deadline(), null, "Hard_Global_Deadline", null, 0, -1, Composite_Timing_Requirement.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getComposite_Timing_Requirement_Soft_Global_Deadline(), this.getSoft_Global_Deadline(), null, "Soft_Global_Deadline", null, 0, -1, Composite_Timing_Requirement.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getComposite_Timing_Requirement_Global_Max_Miss_Ratio(), this.getGlobal_Max_Miss_Ratio(), null, "Global_Max_Miss_Ratio", null, 0, -1, Composite_Timing_Requirement.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getComposite_Timing_Requirement_Hard_Local_Deadline(), this.getHard_Local_Deadline(), null, "Hard_Local_Deadline", null, 0, -1, Composite_Timing_Requirement.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getComposite_Timing_Requirement_Soft_Local_Deadline(), this.getSoft_Local_Deadline(), null, "Soft_Local_Deadline", null, 0, -1, Composite_Timing_Requirement.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getComposite_Timing_Requirement_Local_Max_Miss_Ratio(), this.getLocal_Max_Miss_Ratio(), null, "Local_Max_Miss_Ratio", null, 0, -1, Composite_Timing_Requirement.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(concentratorEClass, Concentrator.class, "Concentrator", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getConcentrator_Input_Events_List(), this.getIdentifier_Ref_List(), "Input_Events_List", null, 1, 1, Concentrator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getConcentrator_Output_Event(), this.getIdentifier_Ref(), "Output_Event", null, 1, 1, Concentrator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(delayEClass, Delay.class, "Delay", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDelay_Delay_Max_Interval(), this.getTime(), "Delay_Max_Interval", null, 0, 1, Delay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDelay_Delay_Min_Interval(), this.getTime(), "Delay_Min_Interval", null, 0, 1, Delay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDelay_Input_Event(), this.getIdentifier_Ref(), "Input_Event", null, 1, 1, Delay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDelay_Output_Event(), this.getIdentifier_Ref(), "Output_Event", null, 1, 1, Delay.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(delivery_ServerEClass, Delivery_Server.class, "Delivery_Server", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDelivery_Server_Delivery_Policy(), this.getDelivery_Policy(), "Delivery_Policy", null, 0, 1, Delivery_Server.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDelivery_Server_Input_Event(), this.getIdentifier_Ref(), "Input_Event", null, 1, 1, Delivery_Server.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDelivery_Server_Output_Events_List(), this.getIdentifier_Ref_List(), "Output_Events_List", null, 1, 1, Delivery_Server.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(document_RootEClass, Document_Root.class, "Document_Root", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDocument_Root_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocument_Root_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocument_Root_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocument_Root_MAST_MODEL(), this.getMAST_MODEL(), null, "MAST_MODEL", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(edF_PolicyEClass, EDF_Policy.class, "EDF_Policy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEDF_Policy_Deadline(), this.getTime(), "Deadline", null, 0, 1, EDF_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEDF_Policy_Preassigned(), this.getAssertion(), "Preassigned", null, 0, 1, EDF_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(edF_SchedulerEClass, EDF_Scheduler.class, "EDF_Scheduler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEDF_Scheduler_Avg_Context_Switch(), this.getNormalized_Execution_Time(), "Avg_Context_Switch", null, 0, 1, EDF_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEDF_Scheduler_Best_Context_Switch(), this.getNormalized_Execution_Time(), "Best_Context_Switch", null, 0, 1, EDF_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEDF_Scheduler_Worst_Context_Switch(), this.getNormalized_Execution_Time(), "Worst_Context_Switch", null, 0, 1, EDF_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(enclosing_OperationEClass, Enclosing_Operation.class, "Enclosing_Operation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEnclosing_Operation_Overridden_Fixed_Priority(), this.getOverridden_Fixed_Priority(), null, "Overridden_Fixed_Priority", null, 0, 1, Enclosing_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEnclosing_Operation_Overridden_Permanent_FP(), this.getOverridden_Permanent_FP(), null, "Overridden_Permanent_FP", null, 0, 1, Enclosing_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEnclosing_Operation_Operation_List(), this.getIdentifier_Ref_List(), "Operation_List", null, 1, 1, Enclosing_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEnclosing_Operation_Average_Case_Execution_Time(), this.getNormalized_Execution_Time(), "Average_Case_Execution_Time", null, 0, 1, Enclosing_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEnclosing_Operation_Best_Case_Execution_Time(), this.getNormalized_Execution_Time(), "Best_Case_Execution_Time", null, 0, 1, Enclosing_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEnclosing_Operation_Name(), this.getIdentifier(), "Name", null, 1, 1, Enclosing_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEnclosing_Operation_Worst_Case_Execution_Time(), this.getNormalized_Execution_Time(), "Worst_Case_Execution_Time", null, 0, 1, Enclosing_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fixed_Priority_PolicyEClass, Fixed_Priority_Policy.class, "Fixed_Priority_Policy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFixed_Priority_Policy_Preassigned(), this.getAssertion(), "Preassigned", null, 0, 1, Fixed_Priority_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFixed_Priority_Policy_The_Priority(), this.getPriority(), "The_Priority", null, 0, 1, Fixed_Priority_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fixed_Priority_SchedulerEClass, Fixed_Priority_Scheduler.class, "Fixed_Priority_Scheduler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFixed_Priority_Scheduler_Avg_Context_Switch(), this.getNormalized_Execution_Time(), "Avg_Context_Switch", null, 0, 1, Fixed_Priority_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFixed_Priority_Scheduler_Best_Context_Switch(), this.getNormalized_Execution_Time(), "Best_Context_Switch", null, 0, 1, Fixed_Priority_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFixed_Priority_Scheduler_Max_Priority(), this.getPriority(), "Max_Priority", null, 0, 1, Fixed_Priority_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFixed_Priority_Scheduler_Min_Priority(), this.getPriority(), "Min_Priority", null, 0, 1, Fixed_Priority_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFixed_Priority_Scheduler_Worst_Context_Switch(), this.getNormalized_Execution_Time(), "Worst_Context_Switch", null, 0, 1, Fixed_Priority_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(fP_Packet_Based_SchedulerEClass, FP_Packet_Based_Scheduler.class, "FP_Packet_Based_Scheduler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFP_Packet_Based_Scheduler_Max_Priority(), this.getPriority(), "Max_Priority", null, 0, 1, FP_Packet_Based_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFP_Packet_Based_Scheduler_Min_Priority(), this.getPriority(), "Min_Priority", null, 0, 1, FP_Packet_Based_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFP_Packet_Based_Scheduler_Packet_Overhead_Avg_Size(), this.getBit_Count(), "Packet_Overhead_Avg_Size", null, 0, 1, FP_Packet_Based_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFP_Packet_Based_Scheduler_Packet_Overhead_Max_Size(), this.getBit_Count(), "Packet_Overhead_Max_Size", null, 0, 1, FP_Packet_Based_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFP_Packet_Based_Scheduler_Packet_Overhead_Min_Size(), this.getBit_Count(), "Packet_Overhead_Min_Size", null, 0, 1, FP_Packet_Based_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(global_Max_Miss_RatioEClass, Global_Max_Miss_Ratio.class, "Global_Max_Miss_Ratio", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGlobal_Max_Miss_Ratio_Deadline(), this.getTime(), "Deadline", null, 0, 1, Global_Max_Miss_Ratio.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGlobal_Max_Miss_Ratio_Ratio(), this.getPercentage(), "Ratio", null, 0, 1, Global_Max_Miss_Ratio.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGlobal_Max_Miss_Ratio_Referenced_Event(), this.getIdentifier_Ref(), "Referenced_Event", null, 1, 1, Global_Max_Miss_Ratio.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hard_Global_DeadlineEClass, Hard_Global_Deadline.class, "Hard_Global_Deadline", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHard_Global_Deadline_Deadline(), this.getTime(), "Deadline", null, 0, 1, Hard_Global_Deadline.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHard_Global_Deadline_Referenced_Event(), this.getIdentifier_Ref(), "Referenced_Event", null, 1, 1, Hard_Global_Deadline.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hard_Local_DeadlineEClass, Hard_Local_Deadline.class, "Hard_Local_Deadline", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHard_Local_Deadline_Deadline(), this.getTime(), "Deadline", null, 0, 1, Hard_Local_Deadline.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(immediate_Ceiling_ResourceEClass, Immediate_Ceiling_Resource.class, "Immediate_Ceiling_Resource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImmediate_Ceiling_Resource_Ceiling(), this.getPriority(), "Ceiling", null, 0, 1, Immediate_Ceiling_Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImmediate_Ceiling_Resource_Name(), this.getIdentifier(), "Name", null, 1, 1, Immediate_Ceiling_Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getImmediate_Ceiling_Resource_Preassigned(), this.getAssertion(), "Preassigned", null, 0, 1, Immediate_Ceiling_Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(interrupt_FP_PolicyEClass, Interrupt_FP_Policy.class, "Interrupt_FP_Policy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInterrupt_FP_Policy_Preassigned(), this.getAffirmative_Assertion(), "Preassigned", null, 0, 1, Interrupt_FP_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInterrupt_FP_Policy_The_Priority(), this.getInterrupt_Priority(), "The_Priority", null, 0, 1, Interrupt_FP_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(list_of_DriversEClass, List_of_Drivers.class, "List_of_Drivers", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getList_of_Drivers_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, List_of_Drivers.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getList_of_Drivers_Packet_Driver(), this.getPacket_Driver(), null, "Packet_Driver", null, 0, -1, List_of_Drivers.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getList_of_Drivers_Character_Packet_Driver(), this.getCharacter_Packet_Driver(), null, "Character_Packet_Driver", null, 0, -1, List_of_Drivers.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getList_of_Drivers_RTEP_Packet_Driver(), this.getRTEP_PacketDriver(), null, "RTEP_Packet_Driver", null, 0, -1, List_of_Drivers.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(local_Max_Miss_RatioEClass, Local_Max_Miss_Ratio.class, "Local_Max_Miss_Ratio", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLocal_Max_Miss_Ratio_Deadline(), this.getTime(), "Deadline", null, 0, 1, Local_Max_Miss_Ratio.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLocal_Max_Miss_Ratio_Ratio(), this.getPercentage(), "Ratio", null, 0, 1, Local_Max_Miss_Ratio.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(masT_MODELEClass, es.esi.gemde.vv.mast.mastmodel.MAST_MODEL.class, "MAST_MODEL", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMAST_MODEL_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, es.esi.gemde.vv.mast.mastmodel.MAST_MODEL.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMAST_MODEL_Regular_Processor(), this.getRegular_Processor(), null, "Regular_Processor", null, 0, -1, es.esi.gemde.vv.mast.mastmodel.MAST_MODEL.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAST_MODEL_Packet_Based_Network(), this.getPacket_Based_Network(), null, "Packet_Based_Network", null, 0, -1, es.esi.gemde.vv.mast.mastmodel.MAST_MODEL.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAST_MODEL_Regular_Scheduling_Server(), this.getRegular_Scheduling_Server(), null, "Regular_Scheduling_Server", null, 0, -1, es.esi.gemde.vv.mast.mastmodel.MAST_MODEL.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAST_MODEL_Immediate_Ceiling_Resource(), this.getImmediate_Ceiling_Resource(), null, "Immediate_Ceiling_Resource", null, 0, -1, es.esi.gemde.vv.mast.mastmodel.MAST_MODEL.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAST_MODEL_Priority_Inheritance_Resource(), this.getPriority_Inheritance_Resource(), null, "Priority_Inheritance_Resource", null, 0, -1, es.esi.gemde.vv.mast.mastmodel.MAST_MODEL.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAST_MODEL_SRP_Resource(), this.getSRP_Resource(), null, "SRP_Resource", null, 0, -1, es.esi.gemde.vv.mast.mastmodel.MAST_MODEL.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAST_MODEL_Simple_Operation(), this.getSimple_Operation(), null, "Simple_Operation", null, 0, -1, es.esi.gemde.vv.mast.mastmodel.MAST_MODEL.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAST_MODEL_Message_Transmission(), this.getMessage_Transmission(), null, "Message_Transmission", null, 0, -1, es.esi.gemde.vv.mast.mastmodel.MAST_MODEL.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAST_MODEL_Composite_Operation(), this.getComposite_Operation(), null, "Composite_Operation", null, 0, -1, es.esi.gemde.vv.mast.mastmodel.MAST_MODEL.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAST_MODEL_Enclosing_Operation(), this.getEnclosing_Operation(), null, "Enclosing_Operation", null, 0, -1, es.esi.gemde.vv.mast.mastmodel.MAST_MODEL.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAST_MODEL_Regular_Transaction(), this.getRegular_Transaction(), null, "Regular_Transaction", null, 0, -1, es.esi.gemde.vv.mast.mastmodel.MAST_MODEL.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAST_MODEL_Primary_Scheduler(), this.getPrimary_Scheduler(), null, "Primary_Scheduler", null, 0, -1, es.esi.gemde.vv.mast.mastmodel.MAST_MODEL.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAST_MODEL_Secondary_Scheduler(), this.getSecondary_Scheduler(), null, "Secondary_Scheduler", null, 0, -1, es.esi.gemde.vv.mast.mastmodel.MAST_MODEL.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAST_MODEL_Model_Date(), this.getDate_Time(), "Model_Date", null, 1, 1, es.esi.gemde.vv.mast.mastmodel.MAST_MODEL.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAST_MODEL_Model_Name(), this.getIdentifier(), "Model_Name", null, 1, 1, es.esi.gemde.vv.mast.mastmodel.MAST_MODEL.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAST_MODEL_System_PiP_Behaviour(), this.getPriority_Inheritance_Protocol(), "System_PiP_Behaviour", null, 0, 1, es.esi.gemde.vv.mast.mastmodel.MAST_MODEL.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(max_Output_Jitter_ReqEClass, Max_Output_Jitter_Req.class, "Max_Output_Jitter_Req", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMax_Output_Jitter_Req_Max_Output_Jitter(), this.getTime(), "Max_Output_Jitter", null, 0, 1, Max_Output_Jitter_Req.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMax_Output_Jitter_Req_Referenced_Event(), this.getIdentifier_Ref(), "Referenced_Event", null, 1, 1, Max_Output_Jitter_Req.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(message_TransmissionEClass, Message_Transmission.class, "Message_Transmission", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMessage_Transmission_Overridden_Fixed_Priority(), this.getOverridden_Fixed_Priority(), null, "Overridden_Fixed_Priority", null, 0, 1, Message_Transmission.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMessage_Transmission_Overridden_Permanent_FP(), this.getOverridden_Permanent_FP(), null, "Overridden_Permanent_FP", null, 0, 1, Message_Transmission.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMessage_Transmission_Avg_Message_Size(), this.getBit_Count(), "Avg_Message_Size", null, 0, 1, Message_Transmission.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMessage_Transmission_Max_Message_Size(), this.getBit_Count(), "Max_Message_Size", null, 0, 1, Message_Transmission.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMessage_Transmission_Min_Message_Size(), this.getBit_Count(), "Min_Message_Size", null, 0, 1, Message_Transmission.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMessage_Transmission_Name(), this.getIdentifier(), "Name", null, 1, 1, Message_Transmission.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(multicastEClass, Multicast.class, "Multicast", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMulticast_Input_Event(), this.getIdentifier_Ref(), "Input_Event", null, 1, 1, Multicast.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMulticast_Output_Events_List(), this.getIdentifier_Ref_List(), "Output_Events_List", null, 1, 1, Multicast.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(non_Preemptible_FP_PolicyEClass, Non_Preemptible_FP_Policy.class, "Non_Preemptible_FP_Policy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNon_Preemptible_FP_Policy_Preassigned(), this.getAssertion(), "Preassigned", null, 0, 1, Non_Preemptible_FP_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNon_Preemptible_FP_Policy_The_Priority(), this.getPriority(), "The_Priority", null, 0, 1, Non_Preemptible_FP_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(offsetEClass, Offset.class, "Offset", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOffset_Delay_Max_Interval(), this.getTime(), "Delay_Max_Interval", null, 0, 1, Offset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOffset_Delay_Min_Interval(), this.getTime(), "Delay_Min_Interval", null, 0, 1, Offset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOffset_Input_Event(), this.getIdentifier_Ref(), "Input_Event", null, 1, 1, Offset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOffset_Output_Event(), this.getIdentifier_Ref(), "Output_Event", null, 1, 1, Offset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOffset_Referenced_Event(), this.getIdentifier_Ref(), "Referenced_Event", null, 1, 1, Offset.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(overridden_Fixed_PriorityEClass, Overridden_Fixed_Priority.class, "Overridden_Fixed_Priority", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOverridden_Fixed_Priority_The_Priority(), this.getPriority(), "The_Priority", null, 1, 1, Overridden_Fixed_Priority.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(overridden_Permanent_FPEClass, Overridden_Permanent_FP.class, "Overridden_Permanent_FP", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOverridden_Permanent_FP_The_Priority(), this.getPriority(), "The_Priority", null, 1, 1, Overridden_Permanent_FP.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(packet_Based_NetworkEClass, Packet_Based_Network.class, "Packet_Based_Network", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPacket_Based_Network_List_of_Drivers(), this.getList_of_Drivers(), null, "List_of_Drivers", null, 0, 1, Packet_Based_Network.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPacket_Based_Network_Max_Blocking(), this.getNormalized_Execution_Time(), "Max_Blocking", null, 0, 1, Packet_Based_Network.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPacket_Based_Network_Max_Packet_Size(), this.getBit_Count(), "Max_Packet_Size", null, 0, 1, Packet_Based_Network.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPacket_Based_Network_Min_Packet_Size(), this.getBit_Count(), "Min_Packet_Size", null, 0, 1, Packet_Based_Network.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPacket_Based_Network_Name(), this.getIdentifier(), "Name", null, 1, 1, Packet_Based_Network.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPacket_Based_Network_Speed_Factor(), this.getFloat(), "Speed_Factor", null, 0, 1, Packet_Based_Network.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPacket_Based_Network_Throughput(), this.getThroughput(), "Throughput", null, 0, 1, Packet_Based_Network.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPacket_Based_Network_Transmission(), this.getTransmission_Type(), "Transmission", null, 0, 1, Packet_Based_Network.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(packet_DriverEClass, Packet_Driver.class, "Packet_Driver", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPacket_Driver_Message_Partitioning(), this.getAssertion(), "Message_Partitioning", null, 0, 1, Packet_Driver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPacket_Driver_Packet_Receive_Operation(), this.getIdentifier_Ref(), "Packet_Receive_Operation", null, 0, 1, Packet_Driver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPacket_Driver_Packet_Send_Operation(), this.getIdentifier_Ref(), "Packet_Send_Operation", null, 0, 1, Packet_Driver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPacket_Driver_Packet_Server(), this.getIdentifier_Ref(), "Packet_Server", null, 1, 1, Packet_Driver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPacket_Driver_RTA_Overhead_Model(), this.getOverhead_Type(), "RTA_Overhead_Model", null, 0, 1, Packet_Driver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(periodic_External_EventEClass, Periodic_External_Event.class, "Periodic_External_Event", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPeriodic_External_Event_Max_Jitter(), this.getTime(), "Max_Jitter", null, 0, 1, Periodic_External_Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPeriodic_External_Event_Name(), this.getIdentifier(), "Name", null, 1, 1, Periodic_External_Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPeriodic_External_Event_Period(), this.getTime(), "Period", null, 0, 1, Periodic_External_Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPeriodic_External_Event_Phase(), this.getAbsolute_Time(), "Phase", null, 0, 1, Periodic_External_Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(polling_PolicyEClass, Polling_Policy.class, "Polling_Policy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPolling_Policy_Polling_Avg_Overhead(), this.getNormalized_Execution_Time(), "Polling_Avg_Overhead", null, 0, 1, Polling_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPolling_Policy_Polling_Best_Overhead(), this.getNormalized_Execution_Time(), "Polling_Best_Overhead", null, 0, 1, Polling_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPolling_Policy_Polling_Period(), this.getTime(), "Polling_Period", null, 0, 1, Polling_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPolling_Policy_Polling_Worst_Overhead(), this.getNormalized_Execution_Time(), "Polling_Worst_Overhead", null, 0, 1, Polling_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPolling_Policy_Preassigned(), this.getAssertion(), "Preassigned", null, 0, 1, Polling_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPolling_Policy_The_Priority(), this.getPriority(), "The_Priority", null, 0, 1, Polling_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(primary_SchedulerEClass, Primary_Scheduler.class, "Primary_Scheduler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPrimary_Scheduler_Fixed_Priority_Scheduler(), this.getFixed_Priority_Scheduler(), null, "Fixed_Priority_Scheduler", null, 0, 1, Primary_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPrimary_Scheduler_EDF_Scheduler(), this.getEDF_Scheduler(), null, "EDF_Scheduler", null, 0, 1, Primary_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPrimary_Scheduler_FP_Packet_Based_Scheduler(), this.getFP_Packet_Based_Scheduler(), null, "FP_Packet_Based_Scheduler", null, 0, 1, Primary_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPrimary_Scheduler_Host(), this.getIdentifier_Ref(), "Host", null, 1, 1, Primary_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPrimary_Scheduler_Name(), this.getIdentifier(), "Name", null, 1, 1, Primary_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(priority_Inheritance_ResourceEClass, Priority_Inheritance_Resource.class, "Priority_Inheritance_Resource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPriority_Inheritance_Resource_Name(), this.getIdentifier(), "Name", null, 1, 1, Priority_Inheritance_Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(query_ServerEClass, Query_Server.class, "Query_Server", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getQuery_Server_Input_Event(), this.getIdentifier_Ref(), "Input_Event", null, 1, 1, Query_Server.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getQuery_Server_Output_Events_List(), this.getIdentifier_Ref_List(), "Output_Events_List", null, 1, 1, Query_Server.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getQuery_Server_Request_Policy(), this.getRequest_Policy(), "Request_Policy", null, 0, 1, Query_Server.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(rate_DivisorEClass, Rate_Divisor.class, "Rate_Divisor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRate_Divisor_Input_Event(), this.getIdentifier_Ref(), "Input_Event", null, 1, 1, Rate_Divisor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRate_Divisor_Output_Event(), this.getIdentifier_Ref(), "Output_Event", null, 1, 1, Rate_Divisor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRate_Divisor_Rate_Factor(), this.getPositive(), "Rate_Factor", null, 1, 1, Rate_Divisor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(regular_EventEClass, Regular_Event.class, "Regular_Event", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRegular_Event_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, Regular_Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Event_Max_Output_Jitter_Req(), this.getMax_Output_Jitter_Req(), null, "Max_Output_Jitter_Req", null, 0, -1, Regular_Event.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Event_Hard_Global_Deadline(), this.getHard_Global_Deadline(), null, "Hard_Global_Deadline", null, 0, -1, Regular_Event.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Event_Soft_Global_Deadline(), this.getSoft_Global_Deadline(), null, "Soft_Global_Deadline", null, 0, -1, Regular_Event.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Event_Global_Max_Miss_Ratio(), this.getGlobal_Max_Miss_Ratio(), null, "Global_Max_Miss_Ratio", null, 0, -1, Regular_Event.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Event_Hard_Local_Deadline(), this.getHard_Local_Deadline(), null, "Hard_Local_Deadline", null, 0, -1, Regular_Event.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Event_Soft_Local_Deadline(), this.getSoft_Local_Deadline(), null, "Soft_Local_Deadline", null, 0, -1, Regular_Event.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Event_Local_Max_Miss_Ratio(), this.getLocal_Max_Miss_Ratio(), null, "Local_Max_Miss_Ratio", null, 0, -1, Regular_Event.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Event_Composite_Timing_Requirement(), this.getComposite_Timing_Requirement(), null, "Composite_Timing_Requirement", null, 0, -1, Regular_Event.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getRegular_Event_Event(), this.getIdentifier(), "Event", null, 1, 1, Regular_Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(regular_ProcessorEClass, Regular_Processor.class, "Regular_Processor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRegular_Processor_Ticker_System_Timer(), this.getTicker_System_Timer(), null, "Ticker_System_Timer", null, 0, 1, Regular_Processor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Processor_Alarm_Clock_System_Timer(), this.getAlarm_Clock_System_Timer(), null, "Alarm_Clock_System_Timer", null, 0, 1, Regular_Processor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRegular_Processor_Avg_ISR_Switch(), this.getNormalized_Execution_Time(), "Avg_ISR_Switch", null, 0, 1, Regular_Processor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRegular_Processor_Best_ISR_Switch(), this.getNormalized_Execution_Time(), "Best_ISR_Switch", null, 0, 1, Regular_Processor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRegular_Processor_Max_Interrupt_Priority(), this.getInterrupt_Priority(), "Max_Interrupt_Priority", null, 0, 1, Regular_Processor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRegular_Processor_Min_Interrupt_Priority(), this.getInterrupt_Priority(), "Min_Interrupt_Priority", null, 0, 1, Regular_Processor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRegular_Processor_Name(), this.getIdentifier(), "Name", null, 1, 1, Regular_Processor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRegular_Processor_Speed_Factor(), this.getFloat(), "Speed_Factor", null, 0, 1, Regular_Processor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRegular_Processor_Worst_ISR_Switch(), this.getNormalized_Execution_Time(), "Worst_ISR_Switch", null, 0, 1, Regular_Processor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(regular_Scheduling_ServerEClass, Regular_Scheduling_Server.class, "Regular_Scheduling_Server", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRegular_Scheduling_Server_Non_Preemptible_FP_Policy(), this.getNon_Preemptible_FP_Policy(), null, "Non_Preemptible_FP_Policy", null, 0, 1, Regular_Scheduling_Server.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Scheduling_Server_Fixed_Priority_Policy(), this.getFixed_Priority_Policy(), null, "Fixed_Priority_Policy", null, 0, 1, Regular_Scheduling_Server.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Scheduling_Server_Interrupt_FP_Policy(), this.getInterrupt_FP_Policy(), null, "Interrupt_FP_Policy", null, 0, 1, Regular_Scheduling_Server.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Scheduling_Server_Polling_Policy(), this.getPolling_Policy(), null, "Polling_Policy", null, 0, 1, Regular_Scheduling_Server.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Scheduling_Server_Sporadic_Server_Policy(), this.getSporadic_Server_Policy(), null, "Sporadic_Server_Policy", null, 0, 1, Regular_Scheduling_Server.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Scheduling_Server_EDF_Policy(), this.getEDF_Policy(), null, "EDF_Policy", null, 0, 1, Regular_Scheduling_Server.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Scheduling_Server_SRP_Parameters(), this.getSRP_Parameters(), null, "SRP_Parameters", null, 0, 1, Regular_Scheduling_Server.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRegular_Scheduling_Server_Name(), this.getIdentifier(), "Name", null, 1, 1, Regular_Scheduling_Server.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRegular_Scheduling_Server_Scheduler(), this.getIdentifier_Ref(), "Scheduler", null, 1, 1, Regular_Scheduling_Server.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(regular_TransactionEClass, Regular_Transaction.class, "Regular_Transaction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRegular_Transaction_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, Regular_Transaction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Transaction_Periodic_External_Event(), this.getPeriodic_External_Event(), null, "Periodic_External_Event", null, 0, -1, Regular_Transaction.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Transaction_Sporadic_External_Event(), this.getSporadic_External_Event(), null, "Sporadic_External_Event", null, 0, -1, Regular_Transaction.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Transaction_Unbounded_External_Event(), this.getUnbounded_External_Event(), null, "Unbounded_External_Event", null, 0, -1, Regular_Transaction.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Transaction_Bursty_External_Event(), this.getBursty_External_Event(), null, "Bursty_External_Event", null, 0, -1, Regular_Transaction.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Transaction_Singular_External_Event(), this.getSingular_External_Event(), null, "Singular_External_Event", null, 0, -1, Regular_Transaction.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Transaction_Regular_Event(), this.getRegular_Event(), null, "Regular_Event", null, 0, -1, Regular_Transaction.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Transaction_Activity(), this.getActivity(), null, "Activity", null, 0, -1, Regular_Transaction.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Transaction_System_Timed_Activity(), this.getSystem_Timed_Activity(), null, "System_Timed_Activity", null, 0, -1, Regular_Transaction.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Transaction_Concentrator(), this.getConcentrator(), null, "Concentrator", null, 0, -1, Regular_Transaction.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Transaction_Barrier(), this.getBarrier(), null, "Barrier", null, 0, -1, Regular_Transaction.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Transaction_Delivery_Server(), this.getDelivery_Server(), null, "Delivery_Server", null, 0, -1, Regular_Transaction.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Transaction_Query_Server(), this.getQuery_Server(), null, "Query_Server", null, 0, -1, Regular_Transaction.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Transaction_Multicast(), this.getMulticast(), null, "Multicast", null, 0, -1, Regular_Transaction.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Transaction_Rate_Divisor(), this.getRate_Divisor(), null, "Rate_Divisor", null, 0, -1, Regular_Transaction.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Transaction_Delay(), this.getDelay(), null, "Delay", null, 0, -1, Regular_Transaction.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getRegular_Transaction_Offset(), this.getOffset(), null, "Offset", null, 0, -1, Regular_Transaction.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getRegular_Transaction_Name(), this.getIdentifier(), "Name", null, 1, 1, Regular_Transaction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(rteP_PacketDriverEClass, RTEP_PacketDriver.class, "RTEP_PacketDriver", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRTEP_PacketDriver_Failure_Timeout(), this.getTime(), "Failure_Timeout", null, 0, 1, RTEP_PacketDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRTEP_PacketDriver_Message_Partitioning(), this.getAssertion(), "Message_Partitioning", null, 0, 1, RTEP_PacketDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRTEP_PacketDriver_Number_Of_Stations(), this.getPositive(), "Number_Of_Stations", null, 1, 1, RTEP_PacketDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRTEP_PacketDriver_Packet_Discard_Operation(), this.getIdentifier_Ref(), "Packet_Discard_Operation", null, 0, 1, RTEP_PacketDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRTEP_PacketDriver_Packet_Interrupt_Server(), this.getIdentifier_Ref(), "Packet_Interrupt_Server", null, 0, 1, RTEP_PacketDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRTEP_PacketDriver_Packet_ISR_Operation(), this.getIdentifier_Ref(), "Packet_ISR_Operation", null, 0, 1, RTEP_PacketDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRTEP_PacketDriver_Packet_Receive_Operation(), this.getIdentifier_Ref(), "Packet_Receive_Operation", null, 0, 1, RTEP_PacketDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRTEP_PacketDriver_Packet_Retransmission_Operation(), this.getIdentifier_Ref(), "Packet_Retransmission_Operation", null, 0, 1, RTEP_PacketDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRTEP_PacketDriver_Packet_Send_Operation(), this.getIdentifier_Ref(), "Packet_Send_Operation", null, 0, 1, RTEP_PacketDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRTEP_PacketDriver_Packet_Server(), this.getIdentifier_Ref(), "Packet_Server", null, 1, 1, RTEP_PacketDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRTEP_PacketDriver_Packet_Transmission_Retries(), this.getNatural(), "Packet_Transmission_Retries", null, 0, 1, RTEP_PacketDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRTEP_PacketDriver_RTA_Overhead_Model(), this.getOverhead_Type(), "RTA_Overhead_Model", null, 0, 1, RTEP_PacketDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRTEP_PacketDriver_Token_Check_Operation(), this.getIdentifier_Ref(), "Token_Check_Operation", null, 0, 1, RTEP_PacketDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRTEP_PacketDriver_Token_Delay(), this.getTime(), "Token_Delay", null, 0, 1, RTEP_PacketDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRTEP_PacketDriver_Token_Manage_Operation(), this.getIdentifier_Ref(), "Token_Manage_Operation", null, 0, 1, RTEP_PacketDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRTEP_PacketDriver_Token_Retransmission_Operation(), this.getIdentifier_Ref(), "Token_Retransmission_Operation", null, 0, 1, RTEP_PacketDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRTEP_PacketDriver_Token_Transmission_Retries(), this.getNatural(), "Token_Transmission_Retries", null, 0, 1, RTEP_PacketDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(secondary_SchedulerEClass, Secondary_Scheduler.class, "Secondary_Scheduler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSecondary_Scheduler_Fixed_Priority_Scheduler(), this.getFixed_Priority_Scheduler(), null, "Fixed_Priority_Scheduler", null, 0, 1, Secondary_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSecondary_Scheduler_EDF_Scheduler(), this.getEDF_Scheduler(), null, "EDF_Scheduler", null, 0, 1, Secondary_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSecondary_Scheduler_FP_Packet_Based_Scheduler(), this.getFP_Packet_Based_Scheduler(), null, "FP_Packet_Based_Scheduler", null, 0, 1, Secondary_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSecondary_Scheduler_Host(), this.getIdentifier_Ref(), "Host", null, 1, 1, Secondary_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSecondary_Scheduler_Name(), this.getIdentifier(), "Name", null, 1, 1, Secondary_Scheduler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(simple_OperationEClass, Simple_Operation.class, "Simple_Operation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSimple_Operation_Overridden_Fixed_Priority(), this.getOverridden_Fixed_Priority(), null, "Overridden_Fixed_Priority", null, 0, 1, Simple_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimple_Operation_Overridden_Permanent_FP(), this.getOverridden_Permanent_FP(), null, "Overridden_Permanent_FP", null, 0, 1, Simple_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimple_Operation_Shared_Resources_List(), this.getIdentifier_Ref_List(), "Shared_Resources_List", null, 0, 1, Simple_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimple_Operation_Shared_Resources_To_Lock(), this.getIdentifier_Ref_List(), "Shared_Resources_To_Lock", null, 0, 1, Simple_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimple_Operation_Shared_Resources_To_Unlock(), this.getIdentifier_Ref_List(), "Shared_Resources_To_Unlock", null, 0, 1, Simple_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimple_Operation_Average_Case_Execution_Time(), this.getNormalized_Execution_Time(), "Average_Case_Execution_Time", null, 0, 1, Simple_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimple_Operation_Best_Case_Execution_Time(), this.getNormalized_Execution_Time(), "Best_Case_Execution_Time", null, 0, 1, Simple_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimple_Operation_Name(), this.getIdentifier(), "Name", null, 1, 1, Simple_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimple_Operation_Worst_Case_Execution_Time(), this.getNormalized_Execution_Time(), "Worst_Case_Execution_Time", null, 0, 1, Simple_Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(singular_External_EventEClass, Singular_External_Event.class, "Singular_External_Event", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSingular_External_Event_Name(), this.getIdentifier(), "Name", null, 1, 1, Singular_External_Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSingular_External_Event_Phase(), this.getAbsolute_Time(), "Phase", null, 0, 1, Singular_External_Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(soft_Global_DeadlineEClass, Soft_Global_Deadline.class, "Soft_Global_Deadline", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSoft_Global_Deadline_Deadline(), this.getTime(), "Deadline", null, 0, 1, Soft_Global_Deadline.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSoft_Global_Deadline_Referenced_Event(), this.getIdentifier_Ref(), "Referenced_Event", null, 1, 1, Soft_Global_Deadline.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(soft_Local_DeadlineEClass, Soft_Local_Deadline.class, "Soft_Local_Deadline", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSoft_Local_Deadline_Deadline(), this.getTime(), "Deadline", null, 0, 1, Soft_Local_Deadline.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sporadic_External_EventEClass, Sporadic_External_Event.class, "Sporadic_External_Event", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSporadic_External_Event_Avg_Interarrival(), this.getTime(), "Avg_Interarrival", null, 0, 1, Sporadic_External_Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSporadic_External_Event_Distribution(), this.getDistribution_Type(), "Distribution", null, 0, 1, Sporadic_External_Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSporadic_External_Event_Min_Interarrival(), this.getTime(), "Min_Interarrival", null, 0, 1, Sporadic_External_Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSporadic_External_Event_Name(), this.getIdentifier(), "Name", null, 1, 1, Sporadic_External_Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sporadic_Server_PolicyEClass, Sporadic_Server_Policy.class, "Sporadic_Server_Policy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSporadic_Server_Policy_Background_Priority(), this.getPriority(), "Background_Priority", null, 0, 1, Sporadic_Server_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSporadic_Server_Policy_Initial_Capacity(), this.getTime(), "Initial_Capacity", null, 0, 1, Sporadic_Server_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSporadic_Server_Policy_Max_Pending_Replenishments(), this.getPositive(), "Max_Pending_Replenishments", null, 0, 1, Sporadic_Server_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSporadic_Server_Policy_Normal_Priority(), this.getPriority(), "Normal_Priority", null, 0, 1, Sporadic_Server_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSporadic_Server_Policy_Preassigned(), this.getAssertion(), "Preassigned", null, 0, 1, Sporadic_Server_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSporadic_Server_Policy_Replenishment_Period(), this.getTime(), "Replenishment_Period", null, 0, 1, Sporadic_Server_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(srP_ParametersEClass, SRP_Parameters.class, "SRP_Parameters", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSRP_Parameters_Preassigned(), this.getAssertion(), "Preassigned", null, 0, 1, SRP_Parameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSRP_Parameters_Preemption_Level(), this.getPreemption_Level(), "Preemption_Level", null, 1, 1, SRP_Parameters.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(srP_ResourceEClass, SRP_Resource.class, "SRP_Resource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSRP_Resource_Name(), this.getIdentifier(), "Name", null, 1, 1, SRP_Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSRP_Resource_Preassigned(), this.getAssertion(), "Preassigned", null, 0, 1, SRP_Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSRP_Resource_Preemption_Level(), this.getPreemption_Level(), "Preemption_Level", null, 0, 1, SRP_Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(system_Timed_ActivityEClass, System_Timed_Activity.class, "System_Timed_Activity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSystem_Timed_Activity_Activity_Operation(), this.getIdentifier_Ref(), "Activity_Operation", null, 1, 1, System_Timed_Activity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSystem_Timed_Activity_Activity_Server(), this.getIdentifier_Ref(), "Activity_Server", null, 1, 1, System_Timed_Activity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSystem_Timed_Activity_Input_Event(), this.getIdentifier_Ref(), "Input_Event", null, 1, 1, System_Timed_Activity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSystem_Timed_Activity_Output_Event(), this.getIdentifier_Ref(), "Output_Event", null, 1, 1, System_Timed_Activity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ticker_System_TimerEClass, Ticker_System_Timer.class, "Ticker_System_Timer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTicker_System_Timer_Avg_Overhead(), this.getNormalized_Execution_Time(), "Avg_Overhead", null, 0, 1, Ticker_System_Timer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTicker_System_Timer_Best_Overhead(), this.getNormalized_Execution_Time(), "Best_Overhead", null, 0, 1, Ticker_System_Timer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTicker_System_Timer_Period(), this.getTime(), "Period", null, 0, 1, Ticker_System_Timer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTicker_System_Timer_Worst_Overhead(), this.getNormalized_Execution_Time(), "Worst_Overhead", null, 0, 1, Ticker_System_Timer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unbounded_External_EventEClass, Unbounded_External_Event.class, "Unbounded_External_Event", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUnbounded_External_Event_Avg_Interarrival(), this.getTime(), "Avg_Interarrival", null, 0, 1, Unbounded_External_Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUnbounded_External_Event_Distribution(), this.getDistribution_Type(), "Distribution", null, 0, 1, Unbounded_External_Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUnbounded_External_Event_Name(), this.getIdentifier(), "Name", null, 1, 1, Unbounded_External_Event.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(affirmative_AssertionEEnum, Affirmative_Assertion.class, "Affirmative_Assertion");
		addEEnumLiteral(affirmative_AssertionEEnum, Affirmative_Assertion.YES);

		initEEnum(assertionEEnum, Assertion.class, "Assertion");
		addEEnumLiteral(assertionEEnum, Assertion.YES);
		addEEnumLiteral(assertionEEnum, Assertion.NO);

		initEEnum(delivery_PolicyEEnum, Delivery_Policy.class, "Delivery_Policy");
		addEEnumLiteral(delivery_PolicyEEnum, Delivery_Policy.SCAN);
		addEEnumLiteral(delivery_PolicyEEnum, Delivery_Policy.RANDOM);

		initEEnum(distribution_TypeEEnum, Distribution_Type.class, "Distribution_Type");
		addEEnumLiteral(distribution_TypeEEnum, Distribution_Type.UNIFORM);
		addEEnumLiteral(distribution_TypeEEnum, Distribution_Type.POISSON);

		initEEnum(negative_AssertionEEnum, Negative_Assertion.class, "Negative_Assertion");
		addEEnumLiteral(negative_AssertionEEnum, Negative_Assertion.NO);

		initEEnum(overhead_TypeEEnum, Overhead_Type.class, "Overhead_Type");
		addEEnumLiteral(overhead_TypeEEnum, Overhead_Type.COUPLED);
		addEEnumLiteral(overhead_TypeEEnum, Overhead_Type.DECOUPLED);

		initEEnum(priority_Inheritance_ProtocolEEnum, Priority_Inheritance_Protocol.class, "Priority_Inheritance_Protocol");
		addEEnumLiteral(priority_Inheritance_ProtocolEEnum, Priority_Inheritance_Protocol.STRICT);
		addEEnumLiteral(priority_Inheritance_ProtocolEEnum, Priority_Inheritance_Protocol.POSIX);

		initEEnum(request_PolicyEEnum, Request_Policy.class, "Request_Policy");
		addEEnumLiteral(request_PolicyEEnum, Request_Policy.PRIORITY);
		addEEnumLiteral(request_PolicyEEnum, Request_Policy.FIFO);
		addEEnumLiteral(request_PolicyEEnum, Request_Policy.LIFO);
		addEEnumLiteral(request_PolicyEEnum, Request_Policy.SCAN);

		initEEnum(transmission_TypeEEnum, Transmission_Type.class, "Transmission_Type");
		addEEnumLiteral(transmission_TypeEEnum, Transmission_Type.SIMPLEX);
		addEEnumLiteral(transmission_TypeEEnum, Transmission_Type.HALFDUPLEX);
		addEEnumLiteral(transmission_TypeEEnum, Transmission_Type.FULLDUPLEX);

		// Initialize data types
		initEDataType(absolute_TimeEDataType, double.class, "Absolute_Time", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(affirmative_Assertion_ObjectEDataType, Affirmative_Assertion.class, "Affirmative_Assertion_Object", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(any_PriorityEDataType, int.class, "Any_Priority", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(assertion_ObjectEDataType, Assertion.class, "Assertion_Object", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(bit_CountEDataType, double.class, "Bit_Count", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(date_TimeEDataType, String.class, "Date_Time", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(delivery_Policy_ObjectEDataType, Delivery_Policy.class, "Delivery_Policy_Object", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(distribution_Type_ObjectEDataType, Distribution_Type.class, "Distribution_Type_Object", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(floatEDataType, double.class, "Float", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(identifierEDataType, String.class, "Identifier", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(identifier_RefEDataType, String.class, "Identifier_Ref", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(identifier_Ref_ListEDataType, List.class, "Identifier_Ref_List", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(interrupt_PriorityEDataType, int.class, "Interrupt_Priority", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(naturalEDataType, int.class, "Natural", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(negative_Assertion_ObjectEDataType, Negative_Assertion.class, "Negative_Assertion_Object", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(normalized_Execution_TimeEDataType, double.class, "Normalized_Execution_Time", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(overhead_Type_ObjectEDataType, Overhead_Type.class, "Overhead_Type_Object", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(pathnameEDataType, String.class, "Pathname", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(percentageEDataType, double.class, "Percentage", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(positiveEDataType, int.class, "Positive", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(preemption_LevelEDataType, int.class, "Preemption_Level", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(priorityEDataType, int.class, "Priority", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(priority_Inheritance_Protocol_ObjectEDataType, Priority_Inheritance_Protocol.class, "Priority_Inheritance_Protocol_Object", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(request_Policy_ObjectEDataType, Request_Policy.class, "Request_Policy_Object", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(throughputEDataType, double.class, "Throughput", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(timeEDataType, double.class, "Time", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(transmission_Type_ObjectEDataType, Transmission_Type.class, "Transmission_Type_Object", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";		
		addAnnotation
		  (absolute_TimeEDataType, 
		   source, 
		   new String[] {
			 "name", "Absolute_Time",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#double"
		   });		
		addAnnotation
		  (activityEClass, 
		   source, 
		   new String[] {
			 "name", "Activity",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getActivity_Activity_Operation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Activity_Operation"
		   });		
		addAnnotation
		  (getActivity_Activity_Server(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Activity_Server"
		   });		
		addAnnotation
		  (getActivity_Input_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Input_Event"
		   });		
		addAnnotation
		  (getActivity_Output_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Output_Event"
		   });		
		addAnnotation
		  (affirmative_AssertionEEnum, 
		   source, 
		   new String[] {
			 "name", "Affirmative_Assertion"
		   });		
		addAnnotation
		  (affirmative_Assertion_ObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "Affirmative_Assertion:Object",
			 "baseType", "Affirmative_Assertion"
		   });		
		addAnnotation
		  (alarm_Clock_System_TimerEClass, 
		   source, 
		   new String[] {
			 "name", "Alarm_Clock_System_Timer",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getAlarm_Clock_System_Timer_Avg_Overhead(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Avg_Overhead"
		   });		
		addAnnotation
		  (getAlarm_Clock_System_Timer_Best_Overhead(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Best_Overhead"
		   });		
		addAnnotation
		  (getAlarm_Clock_System_Timer_Worst_Overhead(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Worst_Overhead"
		   });		
		addAnnotation
		  (any_PriorityEDataType, 
		   source, 
		   new String[] {
			 "name", "Any_Priority",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#int"
		   });		
		addAnnotation
		  (assertionEEnum, 
		   source, 
		   new String[] {
			 "name", "Assertion"
		   });		
		addAnnotation
		  (assertion_ObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "Assertion:Object",
			 "baseType", "Assertion"
		   });		
		addAnnotation
		  (barrierEClass, 
		   source, 
		   new String[] {
			 "name", "Barrier",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getBarrier_Input_Events_List(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Input_Events_List"
		   });		
		addAnnotation
		  (getBarrier_Output_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Output_Event"
		   });		
		addAnnotation
		  (bit_CountEDataType, 
		   source, 
		   new String[] {
			 "name", "Bit_Count",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#double"
		   });		
		addAnnotation
		  (bursty_External_EventEClass, 
		   source, 
		   new String[] {
			 "name", "Bursty_External_Event",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getBursty_External_Event_Avg_Interarrival(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Avg_Interarrival"
		   });		
		addAnnotation
		  (getBursty_External_Event_Bound_Interval(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Bound_Interval"
		   });		
		addAnnotation
		  (getBursty_External_Event_Distribution(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Distribution"
		   });		
		addAnnotation
		  (getBursty_External_Event_Max_Arrivals(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Max_Arrivals"
		   });		
		addAnnotation
		  (getBursty_External_Event_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (character_Packet_DriverEClass, 
		   source, 
		   new String[] {
			 "name", "Character_Packet_Driver",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getCharacter_Packet_Driver_Character_Receive_Operation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Character_Receive_Operation"
		   });		
		addAnnotation
		  (getCharacter_Packet_Driver_Character_Send_Operation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Character_Send_Operation"
		   });		
		addAnnotation
		  (getCharacter_Packet_Driver_Character_Server(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Character_Server"
		   });		
		addAnnotation
		  (getCharacter_Packet_Driver_Character_Transmission_Time(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Character_Transmission_Time"
		   });		
		addAnnotation
		  (getCharacter_Packet_Driver_Message_Partitioning(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Message_Partitioning"
		   });		
		addAnnotation
		  (getCharacter_Packet_Driver_Packet_Receive_Operation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Packet_Receive_Operation"
		   });		
		addAnnotation
		  (getCharacter_Packet_Driver_Packet_Send_Operation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Packet_Send_Operation"
		   });		
		addAnnotation
		  (getCharacter_Packet_Driver_Packet_Server(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Packet_Server"
		   });		
		addAnnotation
		  (getCharacter_Packet_Driver_RTA_Overhead_Model(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "RTA_Overhead_Model"
		   });		
		addAnnotation
		  (composite_OperationEClass, 
		   source, 
		   new String[] {
			 "name", "Composite_Operation",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getComposite_Operation_Overridden_Fixed_Priority(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Overridden_Fixed_Priority",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getComposite_Operation_Overridden_Permanent_FP(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Overridden_Permanent_FP",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getComposite_Operation_Operation_List(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Operation_List",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getComposite_Operation_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (composite_Timing_RequirementEClass, 
		   source, 
		   new String[] {
			 "name", "Composite_Timing_Requirement",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getComposite_Timing_Requirement_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });		
		addAnnotation
		  (getComposite_Timing_Requirement_Max_Output_Jitter_Req(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Max_Output_Jitter_Req",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getComposite_Timing_Requirement_Hard_Global_Deadline(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Hard_Global_Deadline",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getComposite_Timing_Requirement_Soft_Global_Deadline(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Soft_Global_Deadline",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getComposite_Timing_Requirement_Global_Max_Miss_Ratio(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Global_Max_Miss_Ratio",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getComposite_Timing_Requirement_Hard_Local_Deadline(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Hard_Local_Deadline",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getComposite_Timing_Requirement_Soft_Local_Deadline(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Soft_Local_Deadline",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getComposite_Timing_Requirement_Local_Max_Miss_Ratio(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Local_Max_Miss_Ratio",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (concentratorEClass, 
		   source, 
		   new String[] {
			 "name", "Concentrator",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getConcentrator_Input_Events_List(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Input_Events_List"
		   });		
		addAnnotation
		  (getConcentrator_Output_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Output_Event"
		   });		
		addAnnotation
		  (date_TimeEDataType, 
		   source, 
		   new String[] {
			 "name", "Date_Time",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#string"
		   });		
		addAnnotation
		  (delayEClass, 
		   source, 
		   new String[] {
			 "name", "Delay",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getDelay_Delay_Max_Interval(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Delay_Max_Interval"
		   });		
		addAnnotation
		  (getDelay_Delay_Min_Interval(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Delay_Min_Interval"
		   });		
		addAnnotation
		  (getDelay_Input_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Input_Event"
		   });		
		addAnnotation
		  (getDelay_Output_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Output_Event"
		   });		
		addAnnotation
		  (delivery_PolicyEEnum, 
		   source, 
		   new String[] {
			 "name", "Delivery_Policy"
		   });		
		addAnnotation
		  (delivery_Policy_ObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "Delivery_Policy:Object",
			 "baseType", "Delivery_Policy"
		   });		
		addAnnotation
		  (delivery_ServerEClass, 
		   source, 
		   new String[] {
			 "name", "Delivery_Server",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getDelivery_Server_Delivery_Policy(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Delivery_Policy"
		   });		
		addAnnotation
		  (getDelivery_Server_Input_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Input_Event"
		   });		
		addAnnotation
		  (getDelivery_Server_Output_Events_List(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Output_Events_List"
		   });		
		addAnnotation
		  (distribution_TypeEEnum, 
		   source, 
		   new String[] {
			 "name", "Distribution_Type"
		   });		
		addAnnotation
		  (distribution_Type_ObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "Distribution_Type:Object",
			 "baseType", "Distribution_Type"
		   });		
		addAnnotation
		  (document_RootEClass, 
		   source, 
		   new String[] {
			 "name", "",
			 "kind", "mixed"
		   });		
		addAnnotation
		  (getDocument_Root_Mixed(), 
		   source, 
		   new String[] {
			 "kind", "elementWildcard",
			 "name", ":mixed"
		   });		
		addAnnotation
		  (getDocument_Root_XMLNSPrefixMap(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "xmlns:prefix"
		   });		
		addAnnotation
		  (getDocument_Root_XSISchemaLocation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "xsi:schemaLocation"
		   });		
		addAnnotation
		  (getDocument_Root_MAST_MODEL(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "MAST_MODEL",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (edF_PolicyEClass, 
		   source, 
		   new String[] {
			 "name", "EDF_Policy",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getEDF_Policy_Deadline(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Deadline"
		   });		
		addAnnotation
		  (getEDF_Policy_Preassigned(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Preassigned"
		   });		
		addAnnotation
		  (edF_SchedulerEClass, 
		   source, 
		   new String[] {
			 "name", "EDF_Scheduler",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getEDF_Scheduler_Avg_Context_Switch(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Avg_Context_Switch"
		   });		
		addAnnotation
		  (getEDF_Scheduler_Best_Context_Switch(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Best_Context_Switch"
		   });		
		addAnnotation
		  (getEDF_Scheduler_Worst_Context_Switch(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Worst_Context_Switch"
		   });		
		addAnnotation
		  (enclosing_OperationEClass, 
		   source, 
		   new String[] {
			 "name", "Enclosing_Operation",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getEnclosing_Operation_Overridden_Fixed_Priority(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Overridden_Fixed_Priority",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getEnclosing_Operation_Overridden_Permanent_FP(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Overridden_Permanent_FP",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getEnclosing_Operation_Operation_List(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Operation_List",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getEnclosing_Operation_Average_Case_Execution_Time(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Average_Case_Execution_Time"
		   });		
		addAnnotation
		  (getEnclosing_Operation_Best_Case_Execution_Time(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Best_Case_Execution_Time"
		   });		
		addAnnotation
		  (getEnclosing_Operation_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (getEnclosing_Operation_Worst_Case_Execution_Time(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Worst_Case_Execution_Time"
		   });		
		addAnnotation
		  (fixed_Priority_PolicyEClass, 
		   source, 
		   new String[] {
			 "name", "Fixed_Priority_Policy",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getFixed_Priority_Policy_Preassigned(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Preassigned"
		   });		
		addAnnotation
		  (getFixed_Priority_Policy_The_Priority(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "The_Priority"
		   });		
		addAnnotation
		  (fixed_Priority_SchedulerEClass, 
		   source, 
		   new String[] {
			 "name", "Fixed_Priority_Scheduler",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getFixed_Priority_Scheduler_Avg_Context_Switch(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Avg_Context_Switch"
		   });		
		addAnnotation
		  (getFixed_Priority_Scheduler_Best_Context_Switch(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Best_Context_Switch"
		   });		
		addAnnotation
		  (getFixed_Priority_Scheduler_Max_Priority(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Max_Priority"
		   });		
		addAnnotation
		  (getFixed_Priority_Scheduler_Min_Priority(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Min_Priority"
		   });		
		addAnnotation
		  (getFixed_Priority_Scheduler_Worst_Context_Switch(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Worst_Context_Switch"
		   });		
		addAnnotation
		  (floatEDataType, 
		   source, 
		   new String[] {
			 "name", "Float",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#double"
		   });		
		addAnnotation
		  (fP_Packet_Based_SchedulerEClass, 
		   source, 
		   new String[] {
			 "name", "FP_Packet_Based_Scheduler",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getFP_Packet_Based_Scheduler_Max_Priority(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Max_Priority"
		   });		
		addAnnotation
		  (getFP_Packet_Based_Scheduler_Min_Priority(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Min_Priority"
		   });		
		addAnnotation
		  (getFP_Packet_Based_Scheduler_Packet_Overhead_Avg_Size(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Packet_Overhead_Avg_Size"
		   });		
		addAnnotation
		  (getFP_Packet_Based_Scheduler_Packet_Overhead_Max_Size(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Packet_Overhead_Max_Size"
		   });		
		addAnnotation
		  (getFP_Packet_Based_Scheduler_Packet_Overhead_Min_Size(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Packet_Overhead_Min_Size"
		   });		
		addAnnotation
		  (global_Max_Miss_RatioEClass, 
		   source, 
		   new String[] {
			 "name", "Global_Max_Miss_Ratio",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getGlobal_Max_Miss_Ratio_Deadline(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Deadline"
		   });		
		addAnnotation
		  (getGlobal_Max_Miss_Ratio_Ratio(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Ratio"
		   });		
		addAnnotation
		  (getGlobal_Max_Miss_Ratio_Referenced_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Referenced_Event"
		   });		
		addAnnotation
		  (hard_Global_DeadlineEClass, 
		   source, 
		   new String[] {
			 "name", "Hard_Global_Deadline",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getHard_Global_Deadline_Deadline(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Deadline"
		   });		
		addAnnotation
		  (getHard_Global_Deadline_Referenced_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Referenced_Event"
		   });		
		addAnnotation
		  (hard_Local_DeadlineEClass, 
		   source, 
		   new String[] {
			 "name", "Hard_Local_Deadline",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getHard_Local_Deadline_Deadline(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Deadline"
		   });		
		addAnnotation
		  (identifierEDataType, 
		   source, 
		   new String[] {
			 "name", "Identifier",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#NCName",
			 "pattern", "([a-z]|[A-Z])([a-z]|[A-Z]|[0-9]|.|_)*"
		   });		
		addAnnotation
		  (identifier_RefEDataType, 
		   source, 
		   new String[] {
			 "name", "Identifier_Ref",
			 "baseType", "Identifier"
		   });		
		addAnnotation
		  (identifier_Ref_ListEDataType, 
		   source, 
		   new String[] {
			 "name", "Identifier_Ref_List",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#NMTOKENS"
		   });		
		addAnnotation
		  (immediate_Ceiling_ResourceEClass, 
		   source, 
		   new String[] {
			 "name", "Immediate_Ceiling_Resource",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getImmediate_Ceiling_Resource_Ceiling(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Ceiling"
		   });		
		addAnnotation
		  (getImmediate_Ceiling_Resource_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (getImmediate_Ceiling_Resource_Preassigned(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Preassigned"
		   });		
		addAnnotation
		  (interrupt_FP_PolicyEClass, 
		   source, 
		   new String[] {
			 "name", "Interrupt_FP_Policy",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getInterrupt_FP_Policy_Preassigned(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Preassigned"
		   });		
		addAnnotation
		  (getInterrupt_FP_Policy_The_Priority(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "The_Priority"
		   });		
		addAnnotation
		  (interrupt_PriorityEDataType, 
		   source, 
		   new String[] {
			 "name", "Interrupt_Priority",
			 "baseType", "Any_Priority"
		   });		
		addAnnotation
		  (list_of_DriversEClass, 
		   source, 
		   new String[] {
			 "name", "List_of_Drivers",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getList_of_Drivers_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });		
		addAnnotation
		  (getList_of_Drivers_Packet_Driver(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Packet_Driver",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getList_of_Drivers_Character_Packet_Driver(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Character_Packet_Driver",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getList_of_Drivers_RTEP_Packet_Driver(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "RTEP_Packet_Driver",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (local_Max_Miss_RatioEClass, 
		   source, 
		   new String[] {
			 "name", "Local_Max_Miss_Ratio",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getLocal_Max_Miss_Ratio_Deadline(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Deadline"
		   });		
		addAnnotation
		  (getLocal_Max_Miss_Ratio_Ratio(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Ratio"
		   });		
		addAnnotation
		  (masT_MODELEClass, 
		   source, 
		   new String[] {
			 "name", "MAST_MODEL",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getMAST_MODEL_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });		
		addAnnotation
		  (getMAST_MODEL_Regular_Processor(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Regular_Processor",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getMAST_MODEL_Packet_Based_Network(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Packet_Based_Network",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getMAST_MODEL_Regular_Scheduling_Server(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Regular_Scheduling_Server",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getMAST_MODEL_Immediate_Ceiling_Resource(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Immediate_Ceiling_Resource",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getMAST_MODEL_Priority_Inheritance_Resource(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Priority_Inheritance_Resource",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getMAST_MODEL_SRP_Resource(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "SRP_Resource",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getMAST_MODEL_Simple_Operation(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Simple_Operation",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getMAST_MODEL_Message_Transmission(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Message_Transmission",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getMAST_MODEL_Composite_Operation(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Composite_Operation",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getMAST_MODEL_Enclosing_Operation(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Enclosing_Operation",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getMAST_MODEL_Regular_Transaction(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Regular_Transaction",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getMAST_MODEL_Primary_Scheduler(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Primary_Scheduler",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getMAST_MODEL_Secondary_Scheduler(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Secondary_Scheduler",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getMAST_MODEL_Model_Date(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Model_Date"
		   });		
		addAnnotation
		  (getMAST_MODEL_Model_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Model_Name"
		   });		
		addAnnotation
		  (getMAST_MODEL_System_PiP_Behaviour(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "System_PiP_Behaviour"
		   });		
		addAnnotation
		  (max_Output_Jitter_ReqEClass, 
		   source, 
		   new String[] {
			 "name", "Max_Output_Jitter_Req",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getMax_Output_Jitter_Req_Max_Output_Jitter(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Max_Output_Jitter"
		   });		
		addAnnotation
		  (getMax_Output_Jitter_Req_Referenced_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Referenced_Event"
		   });		
		addAnnotation
		  (message_TransmissionEClass, 
		   source, 
		   new String[] {
			 "name", "Message_Transmission",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getMessage_Transmission_Overridden_Fixed_Priority(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Overridden_Fixed_Priority",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getMessage_Transmission_Overridden_Permanent_FP(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Overridden_Permanent_FP",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getMessage_Transmission_Avg_Message_Size(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Avg_Message_Size"
		   });		
		addAnnotation
		  (getMessage_Transmission_Max_Message_Size(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Max_Message_Size"
		   });		
		addAnnotation
		  (getMessage_Transmission_Min_Message_Size(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Min_Message_Size"
		   });		
		addAnnotation
		  (getMessage_Transmission_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (multicastEClass, 
		   source, 
		   new String[] {
			 "name", "Multicast",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getMulticast_Input_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Input_Event"
		   });		
		addAnnotation
		  (getMulticast_Output_Events_List(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Output_Events_List"
		   });		
		addAnnotation
		  (naturalEDataType, 
		   source, 
		   new String[] {
			 "name", "Natural",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#int"
		   });		
		addAnnotation
		  (negative_AssertionEEnum, 
		   source, 
		   new String[] {
			 "name", "Negative_Assertion"
		   });		
		addAnnotation
		  (negative_Assertion_ObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "Negative_Assertion:Object",
			 "baseType", "Negative_Assertion"
		   });		
		addAnnotation
		  (non_Preemptible_FP_PolicyEClass, 
		   source, 
		   new String[] {
			 "name", "Non_Preemptible_FP_Policy",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getNon_Preemptible_FP_Policy_Preassigned(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Preassigned"
		   });		
		addAnnotation
		  (getNon_Preemptible_FP_Policy_The_Priority(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "The_Priority"
		   });		
		addAnnotation
		  (normalized_Execution_TimeEDataType, 
		   source, 
		   new String[] {
			 "name", "Normalized_Execution_Time",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#double"
		   });		
		addAnnotation
		  (offsetEClass, 
		   source, 
		   new String[] {
			 "name", "Offset",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getOffset_Delay_Max_Interval(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Delay_Max_Interval"
		   });		
		addAnnotation
		  (getOffset_Delay_Min_Interval(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Delay_Min_Interval"
		   });		
		addAnnotation
		  (getOffset_Input_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Input_Event"
		   });		
		addAnnotation
		  (getOffset_Output_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Output_Event"
		   });		
		addAnnotation
		  (getOffset_Referenced_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Referenced_Event"
		   });		
		addAnnotation
		  (overhead_TypeEEnum, 
		   source, 
		   new String[] {
			 "name", "Overhead_Type"
		   });		
		addAnnotation
		  (overhead_Type_ObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "Overhead_Type:Object",
			 "baseType", "Overhead_Type"
		   });		
		addAnnotation
		  (overridden_Fixed_PriorityEClass, 
		   source, 
		   new String[] {
			 "name", "Overridden_Fixed_Priority",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getOverridden_Fixed_Priority_The_Priority(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "The_Priority"
		   });		
		addAnnotation
		  (overridden_Permanent_FPEClass, 
		   source, 
		   new String[] {
			 "name", "Overridden_Permanent_FP",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getOverridden_Permanent_FP_The_Priority(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "The_Priority"
		   });		
		addAnnotation
		  (packet_Based_NetworkEClass, 
		   source, 
		   new String[] {
			 "name", "Packet_Based_Network",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getPacket_Based_Network_List_of_Drivers(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "List_of_Drivers",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getPacket_Based_Network_Max_Blocking(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Max_Blocking"
		   });		
		addAnnotation
		  (getPacket_Based_Network_Max_Packet_Size(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Max_Packet_Size"
		   });		
		addAnnotation
		  (getPacket_Based_Network_Min_Packet_Size(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Min_Packet_Size"
		   });		
		addAnnotation
		  (getPacket_Based_Network_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (getPacket_Based_Network_Speed_Factor(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Speed_Factor"
		   });		
		addAnnotation
		  (getPacket_Based_Network_Throughput(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Throughput"
		   });		
		addAnnotation
		  (getPacket_Based_Network_Transmission(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Transmission"
		   });		
		addAnnotation
		  (packet_DriverEClass, 
		   source, 
		   new String[] {
			 "name", "Packet_Driver",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getPacket_Driver_Message_Partitioning(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Message_Partitioning"
		   });		
		addAnnotation
		  (getPacket_Driver_Packet_Receive_Operation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Packet_Receive_Operation"
		   });		
		addAnnotation
		  (getPacket_Driver_Packet_Send_Operation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Packet_Send_Operation"
		   });		
		addAnnotation
		  (getPacket_Driver_Packet_Server(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Packet_Server"
		   });		
		addAnnotation
		  (getPacket_Driver_RTA_Overhead_Model(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "RTA_Overhead_Model"
		   });		
		addAnnotation
		  (pathnameEDataType, 
		   source, 
		   new String[] {
			 "name", "Pathname",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#string"
		   });		
		addAnnotation
		  (percentageEDataType, 
		   source, 
		   new String[] {
			 "name", "Percentage",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#double"
		   });		
		addAnnotation
		  (periodic_External_EventEClass, 
		   source, 
		   new String[] {
			 "name", "Periodic_External_Event",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getPeriodic_External_Event_Max_Jitter(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Max_Jitter"
		   });		
		addAnnotation
		  (getPeriodic_External_Event_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (getPeriodic_External_Event_Period(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Period"
		   });		
		addAnnotation
		  (getPeriodic_External_Event_Phase(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Phase"
		   });		
		addAnnotation
		  (polling_PolicyEClass, 
		   source, 
		   new String[] {
			 "name", "Polling_Policy",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getPolling_Policy_Polling_Avg_Overhead(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Polling_Avg_Overhead"
		   });		
		addAnnotation
		  (getPolling_Policy_Polling_Best_Overhead(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Polling_Best_Overhead"
		   });		
		addAnnotation
		  (getPolling_Policy_Polling_Period(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Polling_Period"
		   });		
		addAnnotation
		  (getPolling_Policy_Polling_Worst_Overhead(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Polling_Worst_Overhead"
		   });		
		addAnnotation
		  (getPolling_Policy_Preassigned(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Preassigned"
		   });		
		addAnnotation
		  (getPolling_Policy_The_Priority(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "The_Priority"
		   });		
		addAnnotation
		  (positiveEDataType, 
		   source, 
		   new String[] {
			 "name", "Positive",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#int"
		   });		
		addAnnotation
		  (preemption_LevelEDataType, 
		   source, 
		   new String[] {
			 "name", "Preemption_Level",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#int"
		   });		
		addAnnotation
		  (primary_SchedulerEClass, 
		   source, 
		   new String[] {
			 "name", "Primary_Scheduler",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getPrimary_Scheduler_Fixed_Priority_Scheduler(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Fixed_Priority_Scheduler",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getPrimary_Scheduler_EDF_Scheduler(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "EDF_Scheduler",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getPrimary_Scheduler_FP_Packet_Based_Scheduler(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "FP_Packet_Based_Scheduler",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getPrimary_Scheduler_Host(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Host"
		   });		
		addAnnotation
		  (getPrimary_Scheduler_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (priorityEDataType, 
		   source, 
		   new String[] {
			 "name", "Priority",
			 "baseType", "Any_Priority"
		   });		
		addAnnotation
		  (priority_Inheritance_ProtocolEEnum, 
		   source, 
		   new String[] {
			 "name", "Priority_Inheritance_Protocol"
		   });		
		addAnnotation
		  (priority_Inheritance_Protocol_ObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "Priority_Inheritance_Protocol:Object",
			 "baseType", "Priority_Inheritance_Protocol"
		   });		
		addAnnotation
		  (priority_Inheritance_ResourceEClass, 
		   source, 
		   new String[] {
			 "name", "Priority_Inheritance_Resource",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getPriority_Inheritance_Resource_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (query_ServerEClass, 
		   source, 
		   new String[] {
			 "name", "Query_Server",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getQuery_Server_Input_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Input_Event"
		   });		
		addAnnotation
		  (getQuery_Server_Output_Events_List(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Output_Events_List"
		   });		
		addAnnotation
		  (getQuery_Server_Request_Policy(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Request_Policy"
		   });		
		addAnnotation
		  (rate_DivisorEClass, 
		   source, 
		   new String[] {
			 "name", "Rate_Divisor",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getRate_Divisor_Input_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Input_Event"
		   });		
		addAnnotation
		  (getRate_Divisor_Output_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Output_Event"
		   });		
		addAnnotation
		  (getRate_Divisor_Rate_Factor(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Rate_Factor"
		   });		
		addAnnotation
		  (regular_EventEClass, 
		   source, 
		   new String[] {
			 "name", "Regular_Event",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getRegular_Event_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });		
		addAnnotation
		  (getRegular_Event_Max_Output_Jitter_Req(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Max_Output_Jitter_Req",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Event_Hard_Global_Deadline(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Hard_Global_Deadline",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Event_Soft_Global_Deadline(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Soft_Global_Deadline",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Event_Global_Max_Miss_Ratio(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Global_Max_Miss_Ratio",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Event_Hard_Local_Deadline(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Hard_Local_Deadline",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Event_Soft_Local_Deadline(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Soft_Local_Deadline",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Event_Local_Max_Miss_Ratio(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Local_Max_Miss_Ratio",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Event_Composite_Timing_Requirement(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Composite_Timing_Requirement",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Event_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Event"
		   });		
		addAnnotation
		  (regular_ProcessorEClass, 
		   source, 
		   new String[] {
			 "name", "Regular_Processor",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getRegular_Processor_Ticker_System_Timer(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Ticker_System_Timer",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getRegular_Processor_Alarm_Clock_System_Timer(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Alarm_Clock_System_Timer",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getRegular_Processor_Avg_ISR_Switch(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Avg_ISR_Switch"
		   });		
		addAnnotation
		  (getRegular_Processor_Best_ISR_Switch(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Best_ISR_Switch"
		   });		
		addAnnotation
		  (getRegular_Processor_Max_Interrupt_Priority(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Max_Interrupt_Priority"
		   });		
		addAnnotation
		  (getRegular_Processor_Min_Interrupt_Priority(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Min_Interrupt_Priority"
		   });		
		addAnnotation
		  (getRegular_Processor_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (getRegular_Processor_Speed_Factor(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Speed_Factor"
		   });		
		addAnnotation
		  (getRegular_Processor_Worst_ISR_Switch(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Worst_ISR_Switch"
		   });		
		addAnnotation
		  (regular_Scheduling_ServerEClass, 
		   source, 
		   new String[] {
			 "name", "Regular_Scheduling_Server",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getRegular_Scheduling_Server_Non_Preemptible_FP_Policy(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Non_Preemptible_FP_Policy",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getRegular_Scheduling_Server_Fixed_Priority_Policy(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Fixed_Priority_Policy",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getRegular_Scheduling_Server_Interrupt_FP_Policy(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Interrupt_FP_Policy",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getRegular_Scheduling_Server_Polling_Policy(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Polling_Policy",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getRegular_Scheduling_Server_Sporadic_Server_Policy(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Sporadic_Server_Policy",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getRegular_Scheduling_Server_EDF_Policy(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "EDF_Policy",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getRegular_Scheduling_Server_SRP_Parameters(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "SRP_Parameters",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getRegular_Scheduling_Server_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (getRegular_Scheduling_Server_Scheduler(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Scheduler"
		   });		
		addAnnotation
		  (regular_TransactionEClass, 
		   source, 
		   new String[] {
			 "name", "Regular_Transaction",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getRegular_Transaction_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });		
		addAnnotation
		  (getRegular_Transaction_Periodic_External_Event(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Periodic_External_Event",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Transaction_Sporadic_External_Event(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Sporadic_External_Event",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Transaction_Unbounded_External_Event(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Unbounded_External_Event",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Transaction_Bursty_External_Event(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Bursty_External_Event",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Transaction_Singular_External_Event(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Singular_External_Event",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Transaction_Regular_Event(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Regular_Event",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Transaction_Activity(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Activity",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Transaction_System_Timed_Activity(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "System_Timed_Activity",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Transaction_Concentrator(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Concentrator",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Transaction_Barrier(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Barrier",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Transaction_Delivery_Server(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Delivery_Server",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Transaction_Query_Server(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Query_Server",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Transaction_Multicast(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Multicast",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Transaction_Rate_Divisor(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Rate_Divisor",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Transaction_Delay(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Delay",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Transaction_Offset(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Offset",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getRegular_Transaction_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (request_PolicyEEnum, 
		   source, 
		   new String[] {
			 "name", "Request_Policy"
		   });		
		addAnnotation
		  (request_Policy_ObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "Request_Policy:Object",
			 "baseType", "Request_Policy"
		   });		
		addAnnotation
		  (rteP_PacketDriverEClass, 
		   source, 
		   new String[] {
			 "name", "RTEP_Packet_Driver",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getRTEP_PacketDriver_Failure_Timeout(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Failure_Timeout"
		   });		
		addAnnotation
		  (getRTEP_PacketDriver_Message_Partitioning(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Message_Partitioning"
		   });		
		addAnnotation
		  (getRTEP_PacketDriver_Number_Of_Stations(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Number_Of_Stations"
		   });		
		addAnnotation
		  (getRTEP_PacketDriver_Packet_Discard_Operation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Packet_Discard_Operation"
		   });		
		addAnnotation
		  (getRTEP_PacketDriver_Packet_Interrupt_Server(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Packet_Interrupt_Server"
		   });		
		addAnnotation
		  (getRTEP_PacketDriver_Packet_ISR_Operation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Packet_ISR_Operation"
		   });		
		addAnnotation
		  (getRTEP_PacketDriver_Packet_Receive_Operation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Packet_Receive_Operation"
		   });		
		addAnnotation
		  (getRTEP_PacketDriver_Packet_Retransmission_Operation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Packet_Retransmission_Operation"
		   });		
		addAnnotation
		  (getRTEP_PacketDriver_Packet_Send_Operation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Packet_Send_Operation"
		   });		
		addAnnotation
		  (getRTEP_PacketDriver_Packet_Server(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Packet_Server"
		   });		
		addAnnotation
		  (getRTEP_PacketDriver_Packet_Transmission_Retries(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Packet_Transmission_Retries"
		   });		
		addAnnotation
		  (getRTEP_PacketDriver_RTA_Overhead_Model(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "RTA_Overhead_Model"
		   });		
		addAnnotation
		  (getRTEP_PacketDriver_Token_Check_Operation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Token_Check_Operation"
		   });		
		addAnnotation
		  (getRTEP_PacketDriver_Token_Delay(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Token_Delay"
		   });		
		addAnnotation
		  (getRTEP_PacketDriver_Token_Manage_Operation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Token_Manage_Operation"
		   });		
		addAnnotation
		  (getRTEP_PacketDriver_Token_Retransmission_Operation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Token_Retransmission_Operation"
		   });		
		addAnnotation
		  (getRTEP_PacketDriver_Token_Transmission_Retries(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Token_Transmission_Retries"
		   });		
		addAnnotation
		  (secondary_SchedulerEClass, 
		   source, 
		   new String[] {
			 "name", "Secondary_Scheduler",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getSecondary_Scheduler_Fixed_Priority_Scheduler(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Fixed_Priority_Scheduler",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getSecondary_Scheduler_EDF_Scheduler(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "EDF_Scheduler",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getSecondary_Scheduler_FP_Packet_Based_Scheduler(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "FP_Packet_Based_Scheduler",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getSecondary_Scheduler_Host(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Host"
		   });		
		addAnnotation
		  (getSecondary_Scheduler_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (simple_OperationEClass, 
		   source, 
		   new String[] {
			 "name", "Simple_Operation",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getSimple_Operation_Overridden_Fixed_Priority(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Overridden_Fixed_Priority",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getSimple_Operation_Overridden_Permanent_FP(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Overridden_Permanent_FP",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getSimple_Operation_Shared_Resources_List(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Shared_Resources_List",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getSimple_Operation_Shared_Resources_To_Lock(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Shared_Resources_To_Lock",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getSimple_Operation_Shared_Resources_To_Unlock(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Shared_Resources_To_Unlock",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getSimple_Operation_Average_Case_Execution_Time(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Average_Case_Execution_Time"
		   });		
		addAnnotation
		  (getSimple_Operation_Best_Case_Execution_Time(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Best_Case_Execution_Time"
		   });		
		addAnnotation
		  (getSimple_Operation_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (getSimple_Operation_Worst_Case_Execution_Time(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Worst_Case_Execution_Time"
		   });		
		addAnnotation
		  (singular_External_EventEClass, 
		   source, 
		   new String[] {
			 "name", "Singular_External_Event",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getSingular_External_Event_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (getSingular_External_Event_Phase(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Phase"
		   });		
		addAnnotation
		  (soft_Global_DeadlineEClass, 
		   source, 
		   new String[] {
			 "name", "Soft_Global_Deadline",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getSoft_Global_Deadline_Deadline(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Deadline"
		   });		
		addAnnotation
		  (getSoft_Global_Deadline_Referenced_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Referenced_Event"
		   });		
		addAnnotation
		  (soft_Local_DeadlineEClass, 
		   source, 
		   new String[] {
			 "name", "Soft_Local_Deadline",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getSoft_Local_Deadline_Deadline(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Deadline"
		   });		
		addAnnotation
		  (sporadic_External_EventEClass, 
		   source, 
		   new String[] {
			 "name", "Sporadic_External_Event",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getSporadic_External_Event_Avg_Interarrival(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Avg_Interarrival"
		   });		
		addAnnotation
		  (getSporadic_External_Event_Distribution(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Distribution"
		   });		
		addAnnotation
		  (getSporadic_External_Event_Min_Interarrival(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Min_Interarrival"
		   });		
		addAnnotation
		  (getSporadic_External_Event_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (sporadic_Server_PolicyEClass, 
		   source, 
		   new String[] {
			 "name", "Sporadic_Server_Policy",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getSporadic_Server_Policy_Background_Priority(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Background_Priority"
		   });		
		addAnnotation
		  (getSporadic_Server_Policy_Initial_Capacity(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Initial_Capacity"
		   });		
		addAnnotation
		  (getSporadic_Server_Policy_Max_Pending_Replenishments(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Max_Pending_Replenishments"
		   });		
		addAnnotation
		  (getSporadic_Server_Policy_Normal_Priority(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Normal_Priority"
		   });		
		addAnnotation
		  (getSporadic_Server_Policy_Preassigned(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Preassigned"
		   });		
		addAnnotation
		  (getSporadic_Server_Policy_Replenishment_Period(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Replenishment_Period"
		   });		
		addAnnotation
		  (srP_ParametersEClass, 
		   source, 
		   new String[] {
			 "name", "SRP_Parameters",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getSRP_Parameters_Preassigned(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Preassigned"
		   });		
		addAnnotation
		  (getSRP_Parameters_Preemption_Level(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Preemption_Level"
		   });		
		addAnnotation
		  (srP_ResourceEClass, 
		   source, 
		   new String[] {
			 "name", "SRP_Resource",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getSRP_Resource_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (getSRP_Resource_Preassigned(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Preassigned"
		   });		
		addAnnotation
		  (getSRP_Resource_Preemption_Level(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Preemption_Level"
		   });		
		addAnnotation
		  (system_Timed_ActivityEClass, 
		   source, 
		   new String[] {
			 "name", "System_Timed_Activity",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getSystem_Timed_Activity_Activity_Operation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Activity_Operation"
		   });		
		addAnnotation
		  (getSystem_Timed_Activity_Activity_Server(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Activity_Server"
		   });		
		addAnnotation
		  (getSystem_Timed_Activity_Input_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Input_Event"
		   });		
		addAnnotation
		  (getSystem_Timed_Activity_Output_Event(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Output_Event"
		   });		
		addAnnotation
		  (throughputEDataType, 
		   source, 
		   new String[] {
			 "name", "Throughput",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#double"
		   });		
		addAnnotation
		  (ticker_System_TimerEClass, 
		   source, 
		   new String[] {
			 "name", "Ticker_System_Timer",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getTicker_System_Timer_Avg_Overhead(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Avg_Overhead"
		   });		
		addAnnotation
		  (getTicker_System_Timer_Best_Overhead(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Best_Overhead"
		   });		
		addAnnotation
		  (getTicker_System_Timer_Period(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Period"
		   });		
		addAnnotation
		  (getTicker_System_Timer_Worst_Overhead(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Worst_Overhead"
		   });		
		addAnnotation
		  (timeEDataType, 
		   source, 
		   new String[] {
			 "name", "Time",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#double"
		   });		
		addAnnotation
		  (transmission_TypeEEnum, 
		   source, 
		   new String[] {
			 "name", "Transmission_Type"
		   });		
		addAnnotation
		  (transmission_Type_ObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "Transmission_Type:Object",
			 "baseType", "Transmission_Type"
		   });		
		addAnnotation
		  (unbounded_External_EventEClass, 
		   source, 
		   new String[] {
			 "name", "Unbounded_External_Event",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getUnbounded_External_Event_Avg_Interarrival(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Avg_Interarrival"
		   });		
		addAnnotation
		  (getUnbounded_External_Event_Distribution(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Distribution"
		   });		
		addAnnotation
		  (getUnbounded_External_Event_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });
	}

} //ModelPackageImpl
