/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Delay</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Delay#getDelay_Max_Interval <em>Delay Max Interval</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Delay#getDelay_Min_Interval <em>Delay Min Interval</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Delay#getInput_Event <em>Input Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Delay#getOutput_Event <em>Output Event</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getDelay()
 * @model extendedMetaData="name='Delay' kind='empty'"
 * @generated
 */
public interface Delay extends EObject {
	/**
	 * Returns the value of the '<em><b>Delay Max Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delay Max Interval</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delay Max Interval</em>' attribute.
	 * @see #setDelay_Max_Interval(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getDelay_Delay_Max_Interval()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Time"
	 *        extendedMetaData="kind='attribute' name='Delay_Max_Interval'"
	 * @generated
	 */
	double getDelay_Max_Interval();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Delay#getDelay_Max_Interval <em>Delay Max Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delay Max Interval</em>' attribute.
	 * @see #getDelay_Max_Interval()
	 * @generated
	 */
	void setDelay_Max_Interval(double value);

	/**
	 * Returns the value of the '<em><b>Delay Min Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delay Min Interval</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delay Min Interval</em>' attribute.
	 * @see #setDelay_Min_Interval(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getDelay_Delay_Min_Interval()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Time"
	 *        extendedMetaData="kind='attribute' name='Delay_Min_Interval'"
	 * @generated
	 */
	double getDelay_Min_Interval();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Delay#getDelay_Min_Interval <em>Delay Min Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delay Min Interval</em>' attribute.
	 * @see #getDelay_Min_Interval()
	 * @generated
	 */
	void setDelay_Min_Interval(double value);

	/**
	 * Returns the value of the '<em><b>Input Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Event</em>' attribute.
	 * @see #setInput_Event(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getDelay_Input_Event()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref" required="true"
	 *        extendedMetaData="kind='attribute' name='Input_Event'"
	 * @generated
	 */
	String getInput_Event();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Delay#getInput_Event <em>Input Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Event</em>' attribute.
	 * @see #getInput_Event()
	 * @generated
	 */
	void setInput_Event(String value);

	/**
	 * Returns the value of the '<em><b>Output Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Event</em>' attribute.
	 * @see #setOutput_Event(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getDelay_Output_Event()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref" required="true"
	 *        extendedMetaData="kind='attribute' name='Output_Event'"
	 * @generated
	 */
	String getOutput_Event();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Delay#getOutput_Event <em>Output Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Event</em>' attribute.
	 * @see #getOutput_Event()
	 * @generated
	 */
	void setOutput_Event(String value);

} // Delay
