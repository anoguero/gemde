/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Fixed Priority Scheduler</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Fixed_Priority_SchedulerImpl#getAvg_Context_Switch <em>Avg Context Switch</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Fixed_Priority_SchedulerImpl#getBest_Context_Switch <em>Best Context Switch</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Fixed_Priority_SchedulerImpl#getMax_Priority <em>Max Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Fixed_Priority_SchedulerImpl#getMin_Priority <em>Min Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Fixed_Priority_SchedulerImpl#getWorst_Context_Switch <em>Worst Context Switch</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Fixed_Priority_SchedulerImpl extends EObjectImpl implements Fixed_Priority_Scheduler {
	/**
	 * The default value of the '{@link #getAvg_Context_Switch() <em>Avg Context Switch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvg_Context_Switch()
	 * @generated
	 * @ordered
	 */
	protected static final double AVG_CONTEXT_SWITCH_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getAvg_Context_Switch() <em>Avg Context Switch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvg_Context_Switch()
	 * @generated
	 * @ordered
	 */
	protected double avg_Context_Switch = AVG_CONTEXT_SWITCH_EDEFAULT;

	/**
	 * The default value of the '{@link #getBest_Context_Switch() <em>Best Context Switch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBest_Context_Switch()
	 * @generated
	 * @ordered
	 */
	protected static final double BEST_CONTEXT_SWITCH_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getBest_Context_Switch() <em>Best Context Switch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBest_Context_Switch()
	 * @generated
	 * @ordered
	 */
	protected double best_Context_Switch = BEST_CONTEXT_SWITCH_EDEFAULT;

	/**
	 * The default value of the '{@link #getMax_Priority() <em>Max Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_Priority()
	 * @generated
	 * @ordered
	 */
	protected static final int MAX_PRIORITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMax_Priority() <em>Max Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_Priority()
	 * @generated
	 * @ordered
	 */
	protected int max_Priority = MAX_PRIORITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getMin_Priority() <em>Min Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin_Priority()
	 * @generated
	 * @ordered
	 */
	protected static final int MIN_PRIORITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMin_Priority() <em>Min Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin_Priority()
	 * @generated
	 * @ordered
	 */
	protected int min_Priority = MIN_PRIORITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getWorst_Context_Switch() <em>Worst Context Switch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorst_Context_Switch()
	 * @generated
	 * @ordered
	 */
	protected static final double WORST_CONTEXT_SWITCH_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getWorst_Context_Switch() <em>Worst Context Switch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorst_Context_Switch()
	 * @generated
	 * @ordered
	 */
	protected double worst_Context_Switch = WORST_CONTEXT_SWITCH_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Fixed_Priority_SchedulerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.FIXED_PRIORITY_SCHEDULER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getAvg_Context_Switch() {
		return avg_Context_Switch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAvg_Context_Switch(double newAvg_Context_Switch) {
		double oldAvg_Context_Switch = avg_Context_Switch;
		avg_Context_Switch = newAvg_Context_Switch;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.FIXED_PRIORITY_SCHEDULER__AVG_CONTEXT_SWITCH, oldAvg_Context_Switch, avg_Context_Switch));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getBest_Context_Switch() {
		return best_Context_Switch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBest_Context_Switch(double newBest_Context_Switch) {
		double oldBest_Context_Switch = best_Context_Switch;
		best_Context_Switch = newBest_Context_Switch;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.FIXED_PRIORITY_SCHEDULER__BEST_CONTEXT_SWITCH, oldBest_Context_Switch, best_Context_Switch));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMax_Priority() {
		return max_Priority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMax_Priority(int newMax_Priority) {
		int oldMax_Priority = max_Priority;
		max_Priority = newMax_Priority;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.FIXED_PRIORITY_SCHEDULER__MAX_PRIORITY, oldMax_Priority, max_Priority));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMin_Priority() {
		return min_Priority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMin_Priority(int newMin_Priority) {
		int oldMin_Priority = min_Priority;
		min_Priority = newMin_Priority;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.FIXED_PRIORITY_SCHEDULER__MIN_PRIORITY, oldMin_Priority, min_Priority));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getWorst_Context_Switch() {
		return worst_Context_Switch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWorst_Context_Switch(double newWorst_Context_Switch) {
		double oldWorst_Context_Switch = worst_Context_Switch;
		worst_Context_Switch = newWorst_Context_Switch;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.FIXED_PRIORITY_SCHEDULER__WORST_CONTEXT_SWITCH, oldWorst_Context_Switch, worst_Context_Switch));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.FIXED_PRIORITY_SCHEDULER__AVG_CONTEXT_SWITCH:
				return getAvg_Context_Switch();
			case ModelPackage.FIXED_PRIORITY_SCHEDULER__BEST_CONTEXT_SWITCH:
				return getBest_Context_Switch();
			case ModelPackage.FIXED_PRIORITY_SCHEDULER__MAX_PRIORITY:
				return getMax_Priority();
			case ModelPackage.FIXED_PRIORITY_SCHEDULER__MIN_PRIORITY:
				return getMin_Priority();
			case ModelPackage.FIXED_PRIORITY_SCHEDULER__WORST_CONTEXT_SWITCH:
				return getWorst_Context_Switch();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.FIXED_PRIORITY_SCHEDULER__AVG_CONTEXT_SWITCH:
				setAvg_Context_Switch((Double)newValue);
				return;
			case ModelPackage.FIXED_PRIORITY_SCHEDULER__BEST_CONTEXT_SWITCH:
				setBest_Context_Switch((Double)newValue);
				return;
			case ModelPackage.FIXED_PRIORITY_SCHEDULER__MAX_PRIORITY:
				setMax_Priority((Integer)newValue);
				return;
			case ModelPackage.FIXED_PRIORITY_SCHEDULER__MIN_PRIORITY:
				setMin_Priority((Integer)newValue);
				return;
			case ModelPackage.FIXED_PRIORITY_SCHEDULER__WORST_CONTEXT_SWITCH:
				setWorst_Context_Switch((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.FIXED_PRIORITY_SCHEDULER__AVG_CONTEXT_SWITCH:
				setAvg_Context_Switch(AVG_CONTEXT_SWITCH_EDEFAULT);
				return;
			case ModelPackage.FIXED_PRIORITY_SCHEDULER__BEST_CONTEXT_SWITCH:
				setBest_Context_Switch(BEST_CONTEXT_SWITCH_EDEFAULT);
				return;
			case ModelPackage.FIXED_PRIORITY_SCHEDULER__MAX_PRIORITY:
				setMax_Priority(MAX_PRIORITY_EDEFAULT);
				return;
			case ModelPackage.FIXED_PRIORITY_SCHEDULER__MIN_PRIORITY:
				setMin_Priority(MIN_PRIORITY_EDEFAULT);
				return;
			case ModelPackage.FIXED_PRIORITY_SCHEDULER__WORST_CONTEXT_SWITCH:
				setWorst_Context_Switch(WORST_CONTEXT_SWITCH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.FIXED_PRIORITY_SCHEDULER__AVG_CONTEXT_SWITCH:
				return avg_Context_Switch != AVG_CONTEXT_SWITCH_EDEFAULT;
			case ModelPackage.FIXED_PRIORITY_SCHEDULER__BEST_CONTEXT_SWITCH:
				return best_Context_Switch != BEST_CONTEXT_SWITCH_EDEFAULT;
			case ModelPackage.FIXED_PRIORITY_SCHEDULER__MAX_PRIORITY:
				return max_Priority != MAX_PRIORITY_EDEFAULT;
			case ModelPackage.FIXED_PRIORITY_SCHEDULER__MIN_PRIORITY:
				return min_Priority != MIN_PRIORITY_EDEFAULT;
			case ModelPackage.FIXED_PRIORITY_SCHEDULER__WORST_CONTEXT_SWITCH:
				return worst_Context_Switch != WORST_CONTEXT_SWITCH_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Avg_Context_Switch: ");
		result.append(avg_Context_Switch);
		result.append(", Best_Context_Switch: ");
		result.append(best_Context_Switch);
		result.append(", Max_Priority: ");
		result.append(max_Priority);
		result.append(", Min_Priority: ");
		result.append(min_Priority);
		result.append(", Worst_Context_Switch: ");
		result.append(worst_Context_Switch);
		result.append(')');
		return result.toString();
	}

} //Fixed_Priority_SchedulerImpl
