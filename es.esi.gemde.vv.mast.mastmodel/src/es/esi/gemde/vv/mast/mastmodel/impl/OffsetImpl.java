/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Offset;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Offset</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.OffsetImpl#getDelay_Max_Interval <em>Delay Max Interval</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.OffsetImpl#getDelay_Min_Interval <em>Delay Min Interval</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.OffsetImpl#getInput_Event <em>Input Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.OffsetImpl#getOutput_Event <em>Output Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.OffsetImpl#getReferenced_Event <em>Referenced Event</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OffsetImpl extends EObjectImpl implements Offset {
	/**
	 * The default value of the '{@link #getDelay_Max_Interval() <em>Delay Max Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDelay_Max_Interval()
	 * @generated
	 * @ordered
	 */
	protected static final double DELAY_MAX_INTERVAL_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getDelay_Max_Interval() <em>Delay Max Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDelay_Max_Interval()
	 * @generated
	 * @ordered
	 */
	protected double delay_Max_Interval = DELAY_MAX_INTERVAL_EDEFAULT;

	/**
	 * The default value of the '{@link #getDelay_Min_Interval() <em>Delay Min Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDelay_Min_Interval()
	 * @generated
	 * @ordered
	 */
	protected static final double DELAY_MIN_INTERVAL_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getDelay_Min_Interval() <em>Delay Min Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDelay_Min_Interval()
	 * @generated
	 * @ordered
	 */
	protected double delay_Min_Interval = DELAY_MIN_INTERVAL_EDEFAULT;

	/**
	 * The default value of the '{@link #getInput_Event() <em>Input Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInput_Event()
	 * @generated
	 * @ordered
	 */
	protected static final String INPUT_EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInput_Event() <em>Input Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInput_Event()
	 * @generated
	 * @ordered
	 */
	protected String input_Event = INPUT_EVENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getOutput_Event() <em>Output Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutput_Event()
	 * @generated
	 * @ordered
	 */
	protected static final String OUTPUT_EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOutput_Event() <em>Output Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutput_Event()
	 * @generated
	 * @ordered
	 */
	protected String output_Event = OUTPUT_EVENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getReferenced_Event() <em>Referenced Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenced_Event()
	 * @generated
	 * @ordered
	 */
	protected static final String REFERENCED_EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReferenced_Event() <em>Referenced Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenced_Event()
	 * @generated
	 * @ordered
	 */
	protected String referenced_Event = REFERENCED_EVENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OffsetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.OFFSET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getDelay_Max_Interval() {
		return delay_Max_Interval;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDelay_Max_Interval(double newDelay_Max_Interval) {
		double oldDelay_Max_Interval = delay_Max_Interval;
		delay_Max_Interval = newDelay_Max_Interval;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.OFFSET__DELAY_MAX_INTERVAL, oldDelay_Max_Interval, delay_Max_Interval));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getDelay_Min_Interval() {
		return delay_Min_Interval;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDelay_Min_Interval(double newDelay_Min_Interval) {
		double oldDelay_Min_Interval = delay_Min_Interval;
		delay_Min_Interval = newDelay_Min_Interval;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.OFFSET__DELAY_MIN_INTERVAL, oldDelay_Min_Interval, delay_Min_Interval));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInput_Event() {
		return input_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInput_Event(String newInput_Event) {
		String oldInput_Event = input_Event;
		input_Event = newInput_Event;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.OFFSET__INPUT_EVENT, oldInput_Event, input_Event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOutput_Event() {
		return output_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutput_Event(String newOutput_Event) {
		String oldOutput_Event = output_Event;
		output_Event = newOutput_Event;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.OFFSET__OUTPUT_EVENT, oldOutput_Event, output_Event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getReferenced_Event() {
		return referenced_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferenced_Event(String newReferenced_Event) {
		String oldReferenced_Event = referenced_Event;
		referenced_Event = newReferenced_Event;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.OFFSET__REFERENCED_EVENT, oldReferenced_Event, referenced_Event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.OFFSET__DELAY_MAX_INTERVAL:
				return getDelay_Max_Interval();
			case ModelPackage.OFFSET__DELAY_MIN_INTERVAL:
				return getDelay_Min_Interval();
			case ModelPackage.OFFSET__INPUT_EVENT:
				return getInput_Event();
			case ModelPackage.OFFSET__OUTPUT_EVENT:
				return getOutput_Event();
			case ModelPackage.OFFSET__REFERENCED_EVENT:
				return getReferenced_Event();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.OFFSET__DELAY_MAX_INTERVAL:
				setDelay_Max_Interval((Double)newValue);
				return;
			case ModelPackage.OFFSET__DELAY_MIN_INTERVAL:
				setDelay_Min_Interval((Double)newValue);
				return;
			case ModelPackage.OFFSET__INPUT_EVENT:
				setInput_Event((String)newValue);
				return;
			case ModelPackage.OFFSET__OUTPUT_EVENT:
				setOutput_Event((String)newValue);
				return;
			case ModelPackage.OFFSET__REFERENCED_EVENT:
				setReferenced_Event((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.OFFSET__DELAY_MAX_INTERVAL:
				setDelay_Max_Interval(DELAY_MAX_INTERVAL_EDEFAULT);
				return;
			case ModelPackage.OFFSET__DELAY_MIN_INTERVAL:
				setDelay_Min_Interval(DELAY_MIN_INTERVAL_EDEFAULT);
				return;
			case ModelPackage.OFFSET__INPUT_EVENT:
				setInput_Event(INPUT_EVENT_EDEFAULT);
				return;
			case ModelPackage.OFFSET__OUTPUT_EVENT:
				setOutput_Event(OUTPUT_EVENT_EDEFAULT);
				return;
			case ModelPackage.OFFSET__REFERENCED_EVENT:
				setReferenced_Event(REFERENCED_EVENT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.OFFSET__DELAY_MAX_INTERVAL:
				return delay_Max_Interval != DELAY_MAX_INTERVAL_EDEFAULT;
			case ModelPackage.OFFSET__DELAY_MIN_INTERVAL:
				return delay_Min_Interval != DELAY_MIN_INTERVAL_EDEFAULT;
			case ModelPackage.OFFSET__INPUT_EVENT:
				return INPUT_EVENT_EDEFAULT == null ? input_Event != null : !INPUT_EVENT_EDEFAULT.equals(input_Event);
			case ModelPackage.OFFSET__OUTPUT_EVENT:
				return OUTPUT_EVENT_EDEFAULT == null ? output_Event != null : !OUTPUT_EVENT_EDEFAULT.equals(output_Event);
			case ModelPackage.OFFSET__REFERENCED_EVENT:
				return REFERENCED_EVENT_EDEFAULT == null ? referenced_Event != null : !REFERENCED_EVENT_EDEFAULT.equals(referenced_Event);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Delay_Max_Interval: ");
		result.append(delay_Max_Interval);
		result.append(", Delay_Min_Interval: ");
		result.append(delay_Min_Interval);
		result.append(", Input_Event: ");
		result.append(input_Event);
		result.append(", Output_Event: ");
		result.append(output_Event);
		result.append(", Referenced_Event: ");
		result.append(referenced_Event);
		result.append(')');
		return result.toString();
	}

} //OffsetImpl
