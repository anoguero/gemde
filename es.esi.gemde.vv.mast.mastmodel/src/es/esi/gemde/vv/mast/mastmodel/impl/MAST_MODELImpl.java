/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Composite_Operation;
import es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation;
import es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource;
import es.esi.gemde.vv.mast.mastmodel.MAST_MODEL;
import es.esi.gemde.vv.mast.mastmodel.Message_Transmission;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network;
import es.esi.gemde.vv.mast.mastmodel.Primary_Scheduler;
import es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Protocol;
import es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Resource;
import es.esi.gemde.vv.mast.mastmodel.Regular_Processor;
import es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server;
import es.esi.gemde.vv.mast.mastmodel.Regular_Transaction;
import es.esi.gemde.vv.mast.mastmodel.SRP_Resource;
import es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler;
import es.esi.gemde.vv.mast.mastmodel.Simple_Operation;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MAST MODEL</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl#getRegular_Processor <em>Regular Processor</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl#getPacket_Based_Network <em>Packet Based Network</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl#getRegular_Scheduling_Server <em>Regular Scheduling Server</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl#getImmediate_Ceiling_Resource <em>Immediate Ceiling Resource</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl#getPriority_Inheritance_Resource <em>Priority Inheritance Resource</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl#getSRP_Resource <em>SRP Resource</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl#getSimple_Operation <em>Simple Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl#getMessage_Transmission <em>Message Transmission</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl#getComposite_Operation <em>Composite Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl#getEnclosing_Operation <em>Enclosing Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl#getRegular_Transaction <em>Regular Transaction</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl#getPrimary_Scheduler <em>Primary Scheduler</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl#getSecondary_Scheduler <em>Secondary Scheduler</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl#getModel_Date <em>Model Date</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl#getModel_Name <em>Model Name</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl#getSystem_PiP_Behaviour <em>System Pi PBehaviour</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MAST_MODELImpl extends EObjectImpl implements MAST_MODEL {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * The default value of the '{@link #getModel_Date() <em>Model Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModel_Date()
	 * @generated
	 * @ordered
	 */
	protected static final String MODEL_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getModel_Date() <em>Model Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModel_Date()
	 * @generated
	 * @ordered
	 */
	protected String model_Date = MODEL_DATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getModel_Name() <em>Model Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModel_Name()
	 * @generated
	 * @ordered
	 */
	protected static final String MODEL_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getModel_Name() <em>Model Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModel_Name()
	 * @generated
	 * @ordered
	 */
	protected String model_Name = MODEL_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSystem_PiP_Behaviour() <em>System Pi PBehaviour</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystem_PiP_Behaviour()
	 * @generated
	 * @ordered
	 */
	protected static final Priority_Inheritance_Protocol SYSTEM_PI_PBEHAVIOUR_EDEFAULT = Priority_Inheritance_Protocol.STRICT;

	/**
	 * The cached value of the '{@link #getSystem_PiP_Behaviour() <em>System Pi PBehaviour</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSystem_PiP_Behaviour()
	 * @generated
	 * @ordered
	 */
	protected Priority_Inheritance_Protocol system_PiP_Behaviour = SYSTEM_PI_PBEHAVIOUR_EDEFAULT;

	/**
	 * This is true if the System Pi PBehaviour attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean system_PiP_BehaviourESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MAST_MODELImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.MAST_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, ModelPackage.MAST_MODEL__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Regular_Processor> getRegular_Processor() {
		return getGroup().list(ModelPackage.Literals.MAST_MODEL__REGULAR_PROCESSOR);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Packet_Based_Network> getPacket_Based_Network() {
		return getGroup().list(ModelPackage.Literals.MAST_MODEL__PACKET_BASED_NETWORK);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Regular_Scheduling_Server> getRegular_Scheduling_Server() {
		return getGroup().list(ModelPackage.Literals.MAST_MODEL__REGULAR_SCHEDULING_SERVER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Immediate_Ceiling_Resource> getImmediate_Ceiling_Resource() {
		return getGroup().list(ModelPackage.Literals.MAST_MODEL__IMMEDIATE_CEILING_RESOURCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Priority_Inheritance_Resource> getPriority_Inheritance_Resource() {
		return getGroup().list(ModelPackage.Literals.MAST_MODEL__PRIORITY_INHERITANCE_RESOURCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<SRP_Resource> getSRP_Resource() {
		return getGroup().list(ModelPackage.Literals.MAST_MODEL__SRP_RESOURCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Simple_Operation> getSimple_Operation() {
		return getGroup().list(ModelPackage.Literals.MAST_MODEL__SIMPLE_OPERATION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Message_Transmission> getMessage_Transmission() {
		return getGroup().list(ModelPackage.Literals.MAST_MODEL__MESSAGE_TRANSMISSION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Composite_Operation> getComposite_Operation() {
		return getGroup().list(ModelPackage.Literals.MAST_MODEL__COMPOSITE_OPERATION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Enclosing_Operation> getEnclosing_Operation() {
		return getGroup().list(ModelPackage.Literals.MAST_MODEL__ENCLOSING_OPERATION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Regular_Transaction> getRegular_Transaction() {
		return getGroup().list(ModelPackage.Literals.MAST_MODEL__REGULAR_TRANSACTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Primary_Scheduler> getPrimary_Scheduler() {
		return getGroup().list(ModelPackage.Literals.MAST_MODEL__PRIMARY_SCHEDULER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Secondary_Scheduler> getSecondary_Scheduler() {
		return getGroup().list(ModelPackage.Literals.MAST_MODEL__SECONDARY_SCHEDULER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getModel_Date() {
		return model_Date;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModel_Date(String newModel_Date) {
		String oldModel_Date = model_Date;
		model_Date = newModel_Date;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.MAST_MODEL__MODEL_DATE, oldModel_Date, model_Date));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getModel_Name() {
		return model_Name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModel_Name(String newModel_Name) {
		String oldModel_Name = model_Name;
		model_Name = newModel_Name;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.MAST_MODEL__MODEL_NAME, oldModel_Name, model_Name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Priority_Inheritance_Protocol getSystem_PiP_Behaviour() {
		return system_PiP_Behaviour;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSystem_PiP_Behaviour(Priority_Inheritance_Protocol newSystem_PiP_Behaviour) {
		Priority_Inheritance_Protocol oldSystem_PiP_Behaviour = system_PiP_Behaviour;
		system_PiP_Behaviour = newSystem_PiP_Behaviour == null ? SYSTEM_PI_PBEHAVIOUR_EDEFAULT : newSystem_PiP_Behaviour;
		boolean oldSystem_PiP_BehaviourESet = system_PiP_BehaviourESet;
		system_PiP_BehaviourESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.MAST_MODEL__SYSTEM_PI_PBEHAVIOUR, oldSystem_PiP_Behaviour, system_PiP_Behaviour, !oldSystem_PiP_BehaviourESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSystem_PiP_Behaviour() {
		Priority_Inheritance_Protocol oldSystem_PiP_Behaviour = system_PiP_Behaviour;
		boolean oldSystem_PiP_BehaviourESet = system_PiP_BehaviourESet;
		system_PiP_Behaviour = SYSTEM_PI_PBEHAVIOUR_EDEFAULT;
		system_PiP_BehaviourESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ModelPackage.MAST_MODEL__SYSTEM_PI_PBEHAVIOUR, oldSystem_PiP_Behaviour, SYSTEM_PI_PBEHAVIOUR_EDEFAULT, oldSystem_PiP_BehaviourESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSystem_PiP_Behaviour() {
		return system_PiP_BehaviourESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelPackage.MAST_MODEL__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case ModelPackage.MAST_MODEL__REGULAR_PROCESSOR:
				return ((InternalEList<?>)getRegular_Processor()).basicRemove(otherEnd, msgs);
			case ModelPackage.MAST_MODEL__PACKET_BASED_NETWORK:
				return ((InternalEList<?>)getPacket_Based_Network()).basicRemove(otherEnd, msgs);
			case ModelPackage.MAST_MODEL__REGULAR_SCHEDULING_SERVER:
				return ((InternalEList<?>)getRegular_Scheduling_Server()).basicRemove(otherEnd, msgs);
			case ModelPackage.MAST_MODEL__IMMEDIATE_CEILING_RESOURCE:
				return ((InternalEList<?>)getImmediate_Ceiling_Resource()).basicRemove(otherEnd, msgs);
			case ModelPackage.MAST_MODEL__PRIORITY_INHERITANCE_RESOURCE:
				return ((InternalEList<?>)getPriority_Inheritance_Resource()).basicRemove(otherEnd, msgs);
			case ModelPackage.MAST_MODEL__SRP_RESOURCE:
				return ((InternalEList<?>)getSRP_Resource()).basicRemove(otherEnd, msgs);
			case ModelPackage.MAST_MODEL__SIMPLE_OPERATION:
				return ((InternalEList<?>)getSimple_Operation()).basicRemove(otherEnd, msgs);
			case ModelPackage.MAST_MODEL__MESSAGE_TRANSMISSION:
				return ((InternalEList<?>)getMessage_Transmission()).basicRemove(otherEnd, msgs);
			case ModelPackage.MAST_MODEL__COMPOSITE_OPERATION:
				return ((InternalEList<?>)getComposite_Operation()).basicRemove(otherEnd, msgs);
			case ModelPackage.MAST_MODEL__ENCLOSING_OPERATION:
				return ((InternalEList<?>)getEnclosing_Operation()).basicRemove(otherEnd, msgs);
			case ModelPackage.MAST_MODEL__REGULAR_TRANSACTION:
				return ((InternalEList<?>)getRegular_Transaction()).basicRemove(otherEnd, msgs);
			case ModelPackage.MAST_MODEL__PRIMARY_SCHEDULER:
				return ((InternalEList<?>)getPrimary_Scheduler()).basicRemove(otherEnd, msgs);
			case ModelPackage.MAST_MODEL__SECONDARY_SCHEDULER:
				return ((InternalEList<?>)getSecondary_Scheduler()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.MAST_MODEL__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case ModelPackage.MAST_MODEL__REGULAR_PROCESSOR:
				return getRegular_Processor();
			case ModelPackage.MAST_MODEL__PACKET_BASED_NETWORK:
				return getPacket_Based_Network();
			case ModelPackage.MAST_MODEL__REGULAR_SCHEDULING_SERVER:
				return getRegular_Scheduling_Server();
			case ModelPackage.MAST_MODEL__IMMEDIATE_CEILING_RESOURCE:
				return getImmediate_Ceiling_Resource();
			case ModelPackage.MAST_MODEL__PRIORITY_INHERITANCE_RESOURCE:
				return getPriority_Inheritance_Resource();
			case ModelPackage.MAST_MODEL__SRP_RESOURCE:
				return getSRP_Resource();
			case ModelPackage.MAST_MODEL__SIMPLE_OPERATION:
				return getSimple_Operation();
			case ModelPackage.MAST_MODEL__MESSAGE_TRANSMISSION:
				return getMessage_Transmission();
			case ModelPackage.MAST_MODEL__COMPOSITE_OPERATION:
				return getComposite_Operation();
			case ModelPackage.MAST_MODEL__ENCLOSING_OPERATION:
				return getEnclosing_Operation();
			case ModelPackage.MAST_MODEL__REGULAR_TRANSACTION:
				return getRegular_Transaction();
			case ModelPackage.MAST_MODEL__PRIMARY_SCHEDULER:
				return getPrimary_Scheduler();
			case ModelPackage.MAST_MODEL__SECONDARY_SCHEDULER:
				return getSecondary_Scheduler();
			case ModelPackage.MAST_MODEL__MODEL_DATE:
				return getModel_Date();
			case ModelPackage.MAST_MODEL__MODEL_NAME:
				return getModel_Name();
			case ModelPackage.MAST_MODEL__SYSTEM_PI_PBEHAVIOUR:
				return getSystem_PiP_Behaviour();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.MAST_MODEL__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case ModelPackage.MAST_MODEL__REGULAR_PROCESSOR:
				getRegular_Processor().clear();
				getRegular_Processor().addAll((Collection<? extends Regular_Processor>)newValue);
				return;
			case ModelPackage.MAST_MODEL__PACKET_BASED_NETWORK:
				getPacket_Based_Network().clear();
				getPacket_Based_Network().addAll((Collection<? extends Packet_Based_Network>)newValue);
				return;
			case ModelPackage.MAST_MODEL__REGULAR_SCHEDULING_SERVER:
				getRegular_Scheduling_Server().clear();
				getRegular_Scheduling_Server().addAll((Collection<? extends Regular_Scheduling_Server>)newValue);
				return;
			case ModelPackage.MAST_MODEL__IMMEDIATE_CEILING_RESOURCE:
				getImmediate_Ceiling_Resource().clear();
				getImmediate_Ceiling_Resource().addAll((Collection<? extends Immediate_Ceiling_Resource>)newValue);
				return;
			case ModelPackage.MAST_MODEL__PRIORITY_INHERITANCE_RESOURCE:
				getPriority_Inheritance_Resource().clear();
				getPriority_Inheritance_Resource().addAll((Collection<? extends Priority_Inheritance_Resource>)newValue);
				return;
			case ModelPackage.MAST_MODEL__SRP_RESOURCE:
				getSRP_Resource().clear();
				getSRP_Resource().addAll((Collection<? extends SRP_Resource>)newValue);
				return;
			case ModelPackage.MAST_MODEL__SIMPLE_OPERATION:
				getSimple_Operation().clear();
				getSimple_Operation().addAll((Collection<? extends Simple_Operation>)newValue);
				return;
			case ModelPackage.MAST_MODEL__MESSAGE_TRANSMISSION:
				getMessage_Transmission().clear();
				getMessage_Transmission().addAll((Collection<? extends Message_Transmission>)newValue);
				return;
			case ModelPackage.MAST_MODEL__COMPOSITE_OPERATION:
				getComposite_Operation().clear();
				getComposite_Operation().addAll((Collection<? extends Composite_Operation>)newValue);
				return;
			case ModelPackage.MAST_MODEL__ENCLOSING_OPERATION:
				getEnclosing_Operation().clear();
				getEnclosing_Operation().addAll((Collection<? extends Enclosing_Operation>)newValue);
				return;
			case ModelPackage.MAST_MODEL__REGULAR_TRANSACTION:
				getRegular_Transaction().clear();
				getRegular_Transaction().addAll((Collection<? extends Regular_Transaction>)newValue);
				return;
			case ModelPackage.MAST_MODEL__PRIMARY_SCHEDULER:
				getPrimary_Scheduler().clear();
				getPrimary_Scheduler().addAll((Collection<? extends Primary_Scheduler>)newValue);
				return;
			case ModelPackage.MAST_MODEL__SECONDARY_SCHEDULER:
				getSecondary_Scheduler().clear();
				getSecondary_Scheduler().addAll((Collection<? extends Secondary_Scheduler>)newValue);
				return;
			case ModelPackage.MAST_MODEL__MODEL_DATE:
				setModel_Date((String)newValue);
				return;
			case ModelPackage.MAST_MODEL__MODEL_NAME:
				setModel_Name((String)newValue);
				return;
			case ModelPackage.MAST_MODEL__SYSTEM_PI_PBEHAVIOUR:
				setSystem_PiP_Behaviour((Priority_Inheritance_Protocol)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.MAST_MODEL__GROUP:
				getGroup().clear();
				return;
			case ModelPackage.MAST_MODEL__REGULAR_PROCESSOR:
				getRegular_Processor().clear();
				return;
			case ModelPackage.MAST_MODEL__PACKET_BASED_NETWORK:
				getPacket_Based_Network().clear();
				return;
			case ModelPackage.MAST_MODEL__REGULAR_SCHEDULING_SERVER:
				getRegular_Scheduling_Server().clear();
				return;
			case ModelPackage.MAST_MODEL__IMMEDIATE_CEILING_RESOURCE:
				getImmediate_Ceiling_Resource().clear();
				return;
			case ModelPackage.MAST_MODEL__PRIORITY_INHERITANCE_RESOURCE:
				getPriority_Inheritance_Resource().clear();
				return;
			case ModelPackage.MAST_MODEL__SRP_RESOURCE:
				getSRP_Resource().clear();
				return;
			case ModelPackage.MAST_MODEL__SIMPLE_OPERATION:
				getSimple_Operation().clear();
				return;
			case ModelPackage.MAST_MODEL__MESSAGE_TRANSMISSION:
				getMessage_Transmission().clear();
				return;
			case ModelPackage.MAST_MODEL__COMPOSITE_OPERATION:
				getComposite_Operation().clear();
				return;
			case ModelPackage.MAST_MODEL__ENCLOSING_OPERATION:
				getEnclosing_Operation().clear();
				return;
			case ModelPackage.MAST_MODEL__REGULAR_TRANSACTION:
				getRegular_Transaction().clear();
				return;
			case ModelPackage.MAST_MODEL__PRIMARY_SCHEDULER:
				getPrimary_Scheduler().clear();
				return;
			case ModelPackage.MAST_MODEL__SECONDARY_SCHEDULER:
				getSecondary_Scheduler().clear();
				return;
			case ModelPackage.MAST_MODEL__MODEL_DATE:
				setModel_Date(MODEL_DATE_EDEFAULT);
				return;
			case ModelPackage.MAST_MODEL__MODEL_NAME:
				setModel_Name(MODEL_NAME_EDEFAULT);
				return;
			case ModelPackage.MAST_MODEL__SYSTEM_PI_PBEHAVIOUR:
				unsetSystem_PiP_Behaviour();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.MAST_MODEL__GROUP:
				return group != null && !group.isEmpty();
			case ModelPackage.MAST_MODEL__REGULAR_PROCESSOR:
				return !getRegular_Processor().isEmpty();
			case ModelPackage.MAST_MODEL__PACKET_BASED_NETWORK:
				return !getPacket_Based_Network().isEmpty();
			case ModelPackage.MAST_MODEL__REGULAR_SCHEDULING_SERVER:
				return !getRegular_Scheduling_Server().isEmpty();
			case ModelPackage.MAST_MODEL__IMMEDIATE_CEILING_RESOURCE:
				return !getImmediate_Ceiling_Resource().isEmpty();
			case ModelPackage.MAST_MODEL__PRIORITY_INHERITANCE_RESOURCE:
				return !getPriority_Inheritance_Resource().isEmpty();
			case ModelPackage.MAST_MODEL__SRP_RESOURCE:
				return !getSRP_Resource().isEmpty();
			case ModelPackage.MAST_MODEL__SIMPLE_OPERATION:
				return !getSimple_Operation().isEmpty();
			case ModelPackage.MAST_MODEL__MESSAGE_TRANSMISSION:
				return !getMessage_Transmission().isEmpty();
			case ModelPackage.MAST_MODEL__COMPOSITE_OPERATION:
				return !getComposite_Operation().isEmpty();
			case ModelPackage.MAST_MODEL__ENCLOSING_OPERATION:
				return !getEnclosing_Operation().isEmpty();
			case ModelPackage.MAST_MODEL__REGULAR_TRANSACTION:
				return !getRegular_Transaction().isEmpty();
			case ModelPackage.MAST_MODEL__PRIMARY_SCHEDULER:
				return !getPrimary_Scheduler().isEmpty();
			case ModelPackage.MAST_MODEL__SECONDARY_SCHEDULER:
				return !getSecondary_Scheduler().isEmpty();
			case ModelPackage.MAST_MODEL__MODEL_DATE:
				return MODEL_DATE_EDEFAULT == null ? model_Date != null : !MODEL_DATE_EDEFAULT.equals(model_Date);
			case ModelPackage.MAST_MODEL__MODEL_NAME:
				return MODEL_NAME_EDEFAULT == null ? model_Name != null : !MODEL_NAME_EDEFAULT.equals(model_Name);
			case ModelPackage.MAST_MODEL__SYSTEM_PI_PBEHAVIOUR:
				return isSetSystem_PiP_Behaviour();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(", Model_Date: ");
		result.append(model_Date);
		result.append(", Model_Name: ");
		result.append(model_Name);
		result.append(", System_PiP_Behaviour: ");
		if (system_PiP_BehaviourESet) result.append(system_PiP_Behaviour); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //MAST_MODELImpl
