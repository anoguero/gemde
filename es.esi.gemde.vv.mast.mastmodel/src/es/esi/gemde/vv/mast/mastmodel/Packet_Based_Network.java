/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Packet Based Network</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getList_of_Drivers <em>List of Drivers</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getMax_Blocking <em>Max Blocking</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getMax_Packet_Size <em>Max Packet Size</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getMin_Packet_Size <em>Min Packet Size</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getName <em>Name</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getSpeed_Factor <em>Speed Factor</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getThroughput <em>Throughput</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getTransmission <em>Transmission</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPacket_Based_Network()
 * @model extendedMetaData="name='Packet_Based_Network' kind='elementOnly'"
 * @generated
 */
public interface Packet_Based_Network extends EObject {
	/**
	 * Returns the value of the '<em><b>List of Drivers</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>List of Drivers</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>List of Drivers</em>' containment reference.
	 * @see #setList_of_Drivers(List_of_Drivers)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPacket_Based_Network_List_of_Drivers()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='List_of_Drivers' namespace='##targetNamespace'"
	 * @generated
	 */
	List_of_Drivers getList_of_Drivers();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getList_of_Drivers <em>List of Drivers</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>List of Drivers</em>' containment reference.
	 * @see #getList_of_Drivers()
	 * @generated
	 */
	void setList_of_Drivers(List_of_Drivers value);

	/**
	 * Returns the value of the '<em><b>Max Blocking</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Blocking</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Blocking</em>' attribute.
	 * @see #setMax_Blocking(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPacket_Based_Network_Max_Blocking()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Max_Blocking'"
	 * @generated
	 */
	double getMax_Blocking();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getMax_Blocking <em>Max Blocking</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Blocking</em>' attribute.
	 * @see #getMax_Blocking()
	 * @generated
	 */
	void setMax_Blocking(double value);

	/**
	 * Returns the value of the '<em><b>Max Packet Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Packet Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Packet Size</em>' attribute.
	 * @see #setMax_Packet_Size(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPacket_Based_Network_Max_Packet_Size()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Bit_Count"
	 *        extendedMetaData="kind='attribute' name='Max_Packet_Size'"
	 * @generated
	 */
	double getMax_Packet_Size();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getMax_Packet_Size <em>Max Packet Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Packet Size</em>' attribute.
	 * @see #getMax_Packet_Size()
	 * @generated
	 */
	void setMax_Packet_Size(double value);

	/**
	 * Returns the value of the '<em><b>Min Packet Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Packet Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Packet Size</em>' attribute.
	 * @see #setMin_Packet_Size(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPacket_Based_Network_Min_Packet_Size()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Bit_Count"
	 *        extendedMetaData="kind='attribute' name='Min_Packet_Size'"
	 * @generated
	 */
	double getMin_Packet_Size();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getMin_Packet_Size <em>Min Packet Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Packet Size</em>' attribute.
	 * @see #getMin_Packet_Size()
	 * @generated
	 */
	void setMin_Packet_Size(double value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPacket_Based_Network_Name()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier" required="true"
	 *        extendedMetaData="kind='attribute' name='Name'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Speed Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Speed Factor</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Speed Factor</em>' attribute.
	 * @see #setSpeed_Factor(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPacket_Based_Network_Speed_Factor()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Float"
	 *        extendedMetaData="kind='attribute' name='Speed_Factor'"
	 * @generated
	 */
	double getSpeed_Factor();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getSpeed_Factor <em>Speed Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Speed Factor</em>' attribute.
	 * @see #getSpeed_Factor()
	 * @generated
	 */
	void setSpeed_Factor(double value);

	/**
	 * Returns the value of the '<em><b>Throughput</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Throughput</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Throughput</em>' attribute.
	 * @see #setThroughput(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPacket_Based_Network_Throughput()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Throughput"
	 *        extendedMetaData="kind='attribute' name='Throughput'"
	 * @generated
	 */
	double getThroughput();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getThroughput <em>Throughput</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Throughput</em>' attribute.
	 * @see #getThroughput()
	 * @generated
	 */
	void setThroughput(double value);

	/**
	 * Returns the value of the '<em><b>Transmission</b></em>' attribute.
	 * The literals are from the enumeration {@link es.esi.gemde.vv.mast.mastmodel.Transmission_Type}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transmission</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transmission</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Transmission_Type
	 * @see #isSetTransmission()
	 * @see #unsetTransmission()
	 * @see #setTransmission(Transmission_Type)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPacket_Based_Network_Transmission()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='Transmission'"
	 * @generated
	 */
	Transmission_Type getTransmission();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getTransmission <em>Transmission</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transmission</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Transmission_Type
	 * @see #isSetTransmission()
	 * @see #unsetTransmission()
	 * @see #getTransmission()
	 * @generated
	 */
	void setTransmission(Transmission_Type value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getTransmission <em>Transmission</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTransmission()
	 * @see #getTransmission()
	 * @see #setTransmission(Transmission_Type)
	 * @generated
	 */
	void unsetTransmission();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getTransmission <em>Transmission</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Transmission</em>' attribute is set.
	 * @see #unsetTransmission()
	 * @see #getTransmission()
	 * @see #setTransmission(Transmission_Type)
	 * @generated
	 */
	boolean isSetTransmission();

} // Packet_Based_Network
