/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Regular Transaction</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getGroup <em>Group</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getPeriodic_External_Event <em>Periodic External Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getSporadic_External_Event <em>Sporadic External Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getUnbounded_External_Event <em>Unbounded External Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getBursty_External_Event <em>Bursty External Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getSingular_External_Event <em>Singular External Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getRegular_Event <em>Regular Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getActivity <em>Activity</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getSystem_Timed_Activity <em>System Timed Activity</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getConcentrator <em>Concentrator</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getBarrier <em>Barrier</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getDelivery_Server <em>Delivery Server</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getQuery_Server <em>Query Server</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getMulticast <em>Multicast</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getRate_Divisor <em>Rate Divisor</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getDelay <em>Delay</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getOffset <em>Offset</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Transaction()
 * @model extendedMetaData="name='Regular_Transaction' kind='elementOnly'"
 * @generated
 */
public interface Regular_Transaction extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Transaction_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Periodic External Event</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Periodic External Event</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Periodic External Event</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Transaction_Periodic_External_Event()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Periodic_External_Event' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Periodic_External_Event> getPeriodic_External_Event();

	/**
	 * Returns the value of the '<em><b>Sporadic External Event</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sporadic External Event</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sporadic External Event</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Transaction_Sporadic_External_Event()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Sporadic_External_Event' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Sporadic_External_Event> getSporadic_External_Event();

	/**
	 * Returns the value of the '<em><b>Unbounded External Event</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Unbounded_External_Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unbounded External Event</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unbounded External Event</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Transaction_Unbounded_External_Event()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Unbounded_External_Event' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Unbounded_External_Event> getUnbounded_External_Event();

	/**
	 * Returns the value of the '<em><b>Bursty External Event</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bursty External Event</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bursty External Event</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Transaction_Bursty_External_Event()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Bursty_External_Event' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Bursty_External_Event> getBursty_External_Event();

	/**
	 * Returns the value of the '<em><b>Singular External Event</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Singular_External_Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Singular External Event</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Singular External Event</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Transaction_Singular_External_Event()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Singular_External_Event' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Singular_External_Event> getSingular_External_Event();

	/**
	 * Returns the value of the '<em><b>Regular Event</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Regular_Event}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Regular Event</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Regular Event</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Transaction_Regular_Event()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Regular_Event' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Regular_Event> getRegular_Event();

	/**
	 * Returns the value of the '<em><b>Activity</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Activity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activity</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Transaction_Activity()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Activity' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Activity> getActivity();

	/**
	 * Returns the value of the '<em><b>System Timed Activity</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System Timed Activity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Timed Activity</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Transaction_System_Timed_Activity()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='System_Timed_Activity' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<System_Timed_Activity> getSystem_Timed_Activity();

	/**
	 * Returns the value of the '<em><b>Concentrator</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Concentrator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Concentrator</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Concentrator</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Transaction_Concentrator()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Concentrator' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Concentrator> getConcentrator();

	/**
	 * Returns the value of the '<em><b>Barrier</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Barrier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Barrier</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Barrier</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Transaction_Barrier()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Barrier' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Barrier> getBarrier();

	/**
	 * Returns the value of the '<em><b>Delivery Server</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Delivery_Server}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delivery Server</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delivery Server</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Transaction_Delivery_Server()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Delivery_Server' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Delivery_Server> getDelivery_Server();

	/**
	 * Returns the value of the '<em><b>Query Server</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Query_Server}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Query Server</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Query Server</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Transaction_Query_Server()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Query_Server' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Query_Server> getQuery_Server();

	/**
	 * Returns the value of the '<em><b>Multicast</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Multicast}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multicast</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multicast</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Transaction_Multicast()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Multicast' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Multicast> getMulticast();

	/**
	 * Returns the value of the '<em><b>Rate Divisor</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Rate_Divisor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rate Divisor</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rate Divisor</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Transaction_Rate_Divisor()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Rate_Divisor' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Rate_Divisor> getRate_Divisor();

	/**
	 * Returns the value of the '<em><b>Delay</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Delay}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delay</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delay</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Transaction_Delay()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Delay' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Delay> getDelay();

	/**
	 * Returns the value of the '<em><b>Offset</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Offset}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Offset</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Offset</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Transaction_Offset()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Offset' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Offset> getOffset();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Transaction_Name()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier" required="true"
	 *        extendedMetaData="kind='attribute' name='Name'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Regular_Transaction
