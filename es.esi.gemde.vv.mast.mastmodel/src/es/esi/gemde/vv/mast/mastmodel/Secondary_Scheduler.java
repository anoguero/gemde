/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Secondary Scheduler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler#getFixed_Priority_Scheduler <em>Fixed Priority Scheduler</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler#getEDF_Scheduler <em>EDF Scheduler</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler#getFP_Packet_Based_Scheduler <em>FP Packet Based Scheduler</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler#getHost <em>Host</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSecondary_Scheduler()
 * @model extendedMetaData="name='Secondary_Scheduler' kind='elementOnly'"
 * @generated
 */
public interface Secondary_Scheduler extends EObject {
	/**
	 * Returns the value of the '<em><b>Fixed Priority Scheduler</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fixed Priority Scheduler</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fixed Priority Scheduler</em>' containment reference.
	 * @see #setFixed_Priority_Scheduler(Fixed_Priority_Scheduler)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSecondary_Scheduler_Fixed_Priority_Scheduler()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Fixed_Priority_Scheduler' namespace='##targetNamespace'"
	 * @generated
	 */
	Fixed_Priority_Scheduler getFixed_Priority_Scheduler();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler#getFixed_Priority_Scheduler <em>Fixed Priority Scheduler</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fixed Priority Scheduler</em>' containment reference.
	 * @see #getFixed_Priority_Scheduler()
	 * @generated
	 */
	void setFixed_Priority_Scheduler(Fixed_Priority_Scheduler value);

	/**
	 * Returns the value of the '<em><b>EDF Scheduler</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EDF Scheduler</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EDF Scheduler</em>' containment reference.
	 * @see #setEDF_Scheduler(EDF_Scheduler)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSecondary_Scheduler_EDF_Scheduler()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EDF_Scheduler' namespace='##targetNamespace'"
	 * @generated
	 */
	EDF_Scheduler getEDF_Scheduler();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler#getEDF_Scheduler <em>EDF Scheduler</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EDF Scheduler</em>' containment reference.
	 * @see #getEDF_Scheduler()
	 * @generated
	 */
	void setEDF_Scheduler(EDF_Scheduler value);

	/**
	 * Returns the value of the '<em><b>FP Packet Based Scheduler</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>FP Packet Based Scheduler</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>FP Packet Based Scheduler</em>' containment reference.
	 * @see #setFP_Packet_Based_Scheduler(FP_Packet_Based_Scheduler)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSecondary_Scheduler_FP_Packet_Based_Scheduler()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='FP_Packet_Based_Scheduler' namespace='##targetNamespace'"
	 * @generated
	 */
	FP_Packet_Based_Scheduler getFP_Packet_Based_Scheduler();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler#getFP_Packet_Based_Scheduler <em>FP Packet Based Scheduler</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>FP Packet Based Scheduler</em>' containment reference.
	 * @see #getFP_Packet_Based_Scheduler()
	 * @generated
	 */
	void setFP_Packet_Based_Scheduler(FP_Packet_Based_Scheduler value);

	/**
	 * Returns the value of the '<em><b>Host</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Host</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Host</em>' attribute.
	 * @see #setHost(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSecondary_Scheduler_Host()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref" required="true"
	 *        extendedMetaData="kind='attribute' name='Host'"
	 * @generated
	 */
	String getHost();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler#getHost <em>Host</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Host</em>' attribute.
	 * @see #getHost()
	 * @generated
	 */
	void setHost(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSecondary_Scheduler_Name()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier" required="true"
	 *        extendedMetaData="kind='attribute' name='Name'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Secondary_Scheduler
