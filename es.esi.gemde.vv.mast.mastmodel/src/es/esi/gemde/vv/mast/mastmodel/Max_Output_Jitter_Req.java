/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Max Output Jitter Req</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Max_Output_Jitter_Req#getMax_Output_Jitter <em>Max Output Jitter</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Max_Output_Jitter_Req#getReferenced_Event <em>Referenced Event</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMax_Output_Jitter_Req()
 * @model extendedMetaData="name='Max_Output_Jitter_Req' kind='empty'"
 * @generated
 */
public interface Max_Output_Jitter_Req extends EObject {
	/**
	 * Returns the value of the '<em><b>Max Output Jitter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Output Jitter</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Output Jitter</em>' attribute.
	 * @see #setMax_Output_Jitter(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMax_Output_Jitter_Req_Max_Output_Jitter()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Time"
	 *        extendedMetaData="kind='attribute' name='Max_Output_Jitter'"
	 * @generated
	 */
	double getMax_Output_Jitter();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Max_Output_Jitter_Req#getMax_Output_Jitter <em>Max Output Jitter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Output Jitter</em>' attribute.
	 * @see #getMax_Output_Jitter()
	 * @generated
	 */
	void setMax_Output_Jitter(double value);

	/**
	 * Returns the value of the '<em><b>Referenced Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Event</em>' attribute.
	 * @see #setReferenced_Event(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMax_Output_Jitter_Req_Referenced_Event()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref" required="true"
	 *        extendedMetaData="kind='attribute' name='Referenced_Event'"
	 * @generated
	 */
	String getReferenced_Event();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Max_Output_Jitter_Req#getReferenced_Event <em>Referenced Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referenced Event</em>' attribute.
	 * @see #getReferenced_Event()
	 * @generated
	 */
	void setReferenced_Event(String value);

} // Max_Output_Jitter_Req
