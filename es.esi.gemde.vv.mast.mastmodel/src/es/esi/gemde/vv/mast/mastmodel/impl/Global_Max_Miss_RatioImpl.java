/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Global Max Miss Ratio</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Global_Max_Miss_RatioImpl#getDeadline <em>Deadline</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Global_Max_Miss_RatioImpl#getRatio <em>Ratio</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Global_Max_Miss_RatioImpl#getReferenced_Event <em>Referenced Event</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Global_Max_Miss_RatioImpl extends EObjectImpl implements Global_Max_Miss_Ratio {
	/**
	 * The default value of the '{@link #getDeadline() <em>Deadline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeadline()
	 * @generated
	 * @ordered
	 */
	protected static final double DEADLINE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getDeadline() <em>Deadline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeadline()
	 * @generated
	 * @ordered
	 */
	protected double deadline = DEADLINE_EDEFAULT;

	/**
	 * The default value of the '{@link #getRatio() <em>Ratio</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRatio()
	 * @generated
	 * @ordered
	 */
	protected static final double RATIO_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getRatio() <em>Ratio</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRatio()
	 * @generated
	 * @ordered
	 */
	protected double ratio = RATIO_EDEFAULT;

	/**
	 * The default value of the '{@link #getReferenced_Event() <em>Referenced Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenced_Event()
	 * @generated
	 * @ordered
	 */
	protected static final String REFERENCED_EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReferenced_Event() <em>Referenced Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenced_Event()
	 * @generated
	 * @ordered
	 */
	protected String referenced_Event = REFERENCED_EVENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Global_Max_Miss_RatioImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.GLOBAL_MAX_MISS_RATIO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getDeadline() {
		return deadline;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeadline(double newDeadline) {
		double oldDeadline = deadline;
		deadline = newDeadline;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.GLOBAL_MAX_MISS_RATIO__DEADLINE, oldDeadline, deadline));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getRatio() {
		return ratio;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRatio(double newRatio) {
		double oldRatio = ratio;
		ratio = newRatio;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.GLOBAL_MAX_MISS_RATIO__RATIO, oldRatio, ratio));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getReferenced_Event() {
		return referenced_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferenced_Event(String newReferenced_Event) {
		String oldReferenced_Event = referenced_Event;
		referenced_Event = newReferenced_Event;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.GLOBAL_MAX_MISS_RATIO__REFERENCED_EVENT, oldReferenced_Event, referenced_Event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.GLOBAL_MAX_MISS_RATIO__DEADLINE:
				return getDeadline();
			case ModelPackage.GLOBAL_MAX_MISS_RATIO__RATIO:
				return getRatio();
			case ModelPackage.GLOBAL_MAX_MISS_RATIO__REFERENCED_EVENT:
				return getReferenced_Event();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.GLOBAL_MAX_MISS_RATIO__DEADLINE:
				setDeadline((Double)newValue);
				return;
			case ModelPackage.GLOBAL_MAX_MISS_RATIO__RATIO:
				setRatio((Double)newValue);
				return;
			case ModelPackage.GLOBAL_MAX_MISS_RATIO__REFERENCED_EVENT:
				setReferenced_Event((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.GLOBAL_MAX_MISS_RATIO__DEADLINE:
				setDeadline(DEADLINE_EDEFAULT);
				return;
			case ModelPackage.GLOBAL_MAX_MISS_RATIO__RATIO:
				setRatio(RATIO_EDEFAULT);
				return;
			case ModelPackage.GLOBAL_MAX_MISS_RATIO__REFERENCED_EVENT:
				setReferenced_Event(REFERENCED_EVENT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.GLOBAL_MAX_MISS_RATIO__DEADLINE:
				return deadline != DEADLINE_EDEFAULT;
			case ModelPackage.GLOBAL_MAX_MISS_RATIO__RATIO:
				return ratio != RATIO_EDEFAULT;
			case ModelPackage.GLOBAL_MAX_MISS_RATIO__REFERENCED_EVENT:
				return REFERENCED_EVENT_EDEFAULT == null ? referenced_Event != null : !REFERENCED_EVENT_EDEFAULT.equals(referenced_Event);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Deadline: ");
		result.append(deadline);
		result.append(", Ratio: ");
		result.append(ratio);
		result.append(", Referenced_Event: ");
		result.append(referenced_Event);
		result.append(')');
		return result.toString();
	}

} //Global_Max_Miss_RatioImpl
