/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Alarm_Clock_System_Timer;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Alarm Clock System Timer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Alarm_Clock_System_TimerImpl#getAvg_Overhead <em>Avg Overhead</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Alarm_Clock_System_TimerImpl#getBest_Overhead <em>Best Overhead</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Alarm_Clock_System_TimerImpl#getWorst_Overhead <em>Worst Overhead</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Alarm_Clock_System_TimerImpl extends EObjectImpl implements Alarm_Clock_System_Timer {
	/**
	 * The default value of the '{@link #getAvg_Overhead() <em>Avg Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvg_Overhead()
	 * @generated
	 * @ordered
	 */
	protected static final double AVG_OVERHEAD_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getAvg_Overhead() <em>Avg Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvg_Overhead()
	 * @generated
	 * @ordered
	 */
	protected double avg_Overhead = AVG_OVERHEAD_EDEFAULT;

	/**
	 * The default value of the '{@link #getBest_Overhead() <em>Best Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBest_Overhead()
	 * @generated
	 * @ordered
	 */
	protected static final double BEST_OVERHEAD_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getBest_Overhead() <em>Best Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBest_Overhead()
	 * @generated
	 * @ordered
	 */
	protected double best_Overhead = BEST_OVERHEAD_EDEFAULT;

	/**
	 * The default value of the '{@link #getWorst_Overhead() <em>Worst Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorst_Overhead()
	 * @generated
	 * @ordered
	 */
	protected static final double WORST_OVERHEAD_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getWorst_Overhead() <em>Worst Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorst_Overhead()
	 * @generated
	 * @ordered
	 */
	protected double worst_Overhead = WORST_OVERHEAD_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Alarm_Clock_System_TimerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.ALARM_CLOCK_SYSTEM_TIMER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getAvg_Overhead() {
		return avg_Overhead;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAvg_Overhead(double newAvg_Overhead) {
		double oldAvg_Overhead = avg_Overhead;
		avg_Overhead = newAvg_Overhead;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.ALARM_CLOCK_SYSTEM_TIMER__AVG_OVERHEAD, oldAvg_Overhead, avg_Overhead));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getBest_Overhead() {
		return best_Overhead;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBest_Overhead(double newBest_Overhead) {
		double oldBest_Overhead = best_Overhead;
		best_Overhead = newBest_Overhead;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.ALARM_CLOCK_SYSTEM_TIMER__BEST_OVERHEAD, oldBest_Overhead, best_Overhead));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getWorst_Overhead() {
		return worst_Overhead;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWorst_Overhead(double newWorst_Overhead) {
		double oldWorst_Overhead = worst_Overhead;
		worst_Overhead = newWorst_Overhead;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.ALARM_CLOCK_SYSTEM_TIMER__WORST_OVERHEAD, oldWorst_Overhead, worst_Overhead));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.ALARM_CLOCK_SYSTEM_TIMER__AVG_OVERHEAD:
				return getAvg_Overhead();
			case ModelPackage.ALARM_CLOCK_SYSTEM_TIMER__BEST_OVERHEAD:
				return getBest_Overhead();
			case ModelPackage.ALARM_CLOCK_SYSTEM_TIMER__WORST_OVERHEAD:
				return getWorst_Overhead();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.ALARM_CLOCK_SYSTEM_TIMER__AVG_OVERHEAD:
				setAvg_Overhead((Double)newValue);
				return;
			case ModelPackage.ALARM_CLOCK_SYSTEM_TIMER__BEST_OVERHEAD:
				setBest_Overhead((Double)newValue);
				return;
			case ModelPackage.ALARM_CLOCK_SYSTEM_TIMER__WORST_OVERHEAD:
				setWorst_Overhead((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.ALARM_CLOCK_SYSTEM_TIMER__AVG_OVERHEAD:
				setAvg_Overhead(AVG_OVERHEAD_EDEFAULT);
				return;
			case ModelPackage.ALARM_CLOCK_SYSTEM_TIMER__BEST_OVERHEAD:
				setBest_Overhead(BEST_OVERHEAD_EDEFAULT);
				return;
			case ModelPackage.ALARM_CLOCK_SYSTEM_TIMER__WORST_OVERHEAD:
				setWorst_Overhead(WORST_OVERHEAD_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.ALARM_CLOCK_SYSTEM_TIMER__AVG_OVERHEAD:
				return avg_Overhead != AVG_OVERHEAD_EDEFAULT;
			case ModelPackage.ALARM_CLOCK_SYSTEM_TIMER__BEST_OVERHEAD:
				return best_Overhead != BEST_OVERHEAD_EDEFAULT;
			case ModelPackage.ALARM_CLOCK_SYSTEM_TIMER__WORST_OVERHEAD:
				return worst_Overhead != WORST_OVERHEAD_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Avg_Overhead: ");
		result.append(avg_Overhead);
		result.append(", Best_Overhead: ");
		result.append(best_Overhead);
		result.append(", Worst_Overhead: ");
		result.append(worst_Overhead);
		result.append(')');
		return result.toString();
	}

} //Alarm_Clock_System_TimerImpl
