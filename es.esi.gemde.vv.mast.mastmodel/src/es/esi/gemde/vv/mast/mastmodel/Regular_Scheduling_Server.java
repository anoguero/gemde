/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Regular Scheduling Server</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getNon_Preemptible_FP_Policy <em>Non Preemptible FP Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getFixed_Priority_Policy <em>Fixed Priority Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getInterrupt_FP_Policy <em>Interrupt FP Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getPolling_Policy <em>Polling Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getSporadic_Server_Policy <em>Sporadic Server Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getEDF_Policy <em>EDF Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getSRP_Parameters <em>SRP Parameters</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getName <em>Name</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getScheduler <em>Scheduler</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Scheduling_Server()
 * @model extendedMetaData="name='Regular_Scheduling_Server' kind='elementOnly'"
 * @generated
 */
public interface Regular_Scheduling_Server extends EObject {
	/**
	 * Returns the value of the '<em><b>Non Preemptible FP Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Non Preemptible FP Policy</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Non Preemptible FP Policy</em>' containment reference.
	 * @see #setNon_Preemptible_FP_Policy(Non_Preemptible_FP_Policy)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Scheduling_Server_Non_Preemptible_FP_Policy()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Non_Preemptible_FP_Policy' namespace='##targetNamespace'"
	 * @generated
	 */
	Non_Preemptible_FP_Policy getNon_Preemptible_FP_Policy();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getNon_Preemptible_FP_Policy <em>Non Preemptible FP Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Non Preemptible FP Policy</em>' containment reference.
	 * @see #getNon_Preemptible_FP_Policy()
	 * @generated
	 */
	void setNon_Preemptible_FP_Policy(Non_Preemptible_FP_Policy value);

	/**
	 * Returns the value of the '<em><b>Fixed Priority Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fixed Priority Policy</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fixed Priority Policy</em>' containment reference.
	 * @see #setFixed_Priority_Policy(Fixed_Priority_Policy)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Scheduling_Server_Fixed_Priority_Policy()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Fixed_Priority_Policy' namespace='##targetNamespace'"
	 * @generated
	 */
	Fixed_Priority_Policy getFixed_Priority_Policy();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getFixed_Priority_Policy <em>Fixed Priority Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fixed Priority Policy</em>' containment reference.
	 * @see #getFixed_Priority_Policy()
	 * @generated
	 */
	void setFixed_Priority_Policy(Fixed_Priority_Policy value);

	/**
	 * Returns the value of the '<em><b>Interrupt FP Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt FP Policy</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt FP Policy</em>' containment reference.
	 * @see #setInterrupt_FP_Policy(Interrupt_FP_Policy)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Scheduling_Server_Interrupt_FP_Policy()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Interrupt_FP_Policy' namespace='##targetNamespace'"
	 * @generated
	 */
	Interrupt_FP_Policy getInterrupt_FP_Policy();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getInterrupt_FP_Policy <em>Interrupt FP Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interrupt FP Policy</em>' containment reference.
	 * @see #getInterrupt_FP_Policy()
	 * @generated
	 */
	void setInterrupt_FP_Policy(Interrupt_FP_Policy value);

	/**
	 * Returns the value of the '<em><b>Polling Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Polling Policy</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Polling Policy</em>' containment reference.
	 * @see #setPolling_Policy(Polling_Policy)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Scheduling_Server_Polling_Policy()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Polling_Policy' namespace='##targetNamespace'"
	 * @generated
	 */
	Polling_Policy getPolling_Policy();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getPolling_Policy <em>Polling Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Polling Policy</em>' containment reference.
	 * @see #getPolling_Policy()
	 * @generated
	 */
	void setPolling_Policy(Polling_Policy value);

	/**
	 * Returns the value of the '<em><b>Sporadic Server Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sporadic Server Policy</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sporadic Server Policy</em>' containment reference.
	 * @see #setSporadic_Server_Policy(Sporadic_Server_Policy)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Scheduling_Server_Sporadic_Server_Policy()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Sporadic_Server_Policy' namespace='##targetNamespace'"
	 * @generated
	 */
	Sporadic_Server_Policy getSporadic_Server_Policy();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getSporadic_Server_Policy <em>Sporadic Server Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sporadic Server Policy</em>' containment reference.
	 * @see #getSporadic_Server_Policy()
	 * @generated
	 */
	void setSporadic_Server_Policy(Sporadic_Server_Policy value);

	/**
	 * Returns the value of the '<em><b>EDF Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EDF Policy</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EDF Policy</em>' containment reference.
	 * @see #setEDF_Policy(EDF_Policy)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Scheduling_Server_EDF_Policy()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='EDF_Policy' namespace='##targetNamespace'"
	 * @generated
	 */
	EDF_Policy getEDF_Policy();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getEDF_Policy <em>EDF Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>EDF Policy</em>' containment reference.
	 * @see #getEDF_Policy()
	 * @generated
	 */
	void setEDF_Policy(EDF_Policy value);

	/**
	 * Returns the value of the '<em><b>SRP Parameters</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SRP Parameters</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SRP Parameters</em>' containment reference.
	 * @see #setSRP_Parameters(SRP_Parameters)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Scheduling_Server_SRP_Parameters()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='SRP_Parameters' namespace='##targetNamespace'"
	 * @generated
	 */
	SRP_Parameters getSRP_Parameters();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getSRP_Parameters <em>SRP Parameters</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>SRP Parameters</em>' containment reference.
	 * @see #getSRP_Parameters()
	 * @generated
	 */
	void setSRP_Parameters(SRP_Parameters value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Scheduling_Server_Name()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier" required="true"
	 *        extendedMetaData="kind='attribute' name='Name'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Scheduler</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scheduler</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scheduler</em>' attribute.
	 * @see #setScheduler(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Scheduling_Server_Scheduler()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref" required="true"
	 *        extendedMetaData="kind='attribute' name='Scheduler'"
	 * @generated
	 */
	String getScheduler();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getScheduler <em>Scheduler</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scheduler</em>' attribute.
	 * @see #getScheduler()
	 * @generated
	 */
	void setScheduler(String value);

} // Regular_Scheduling_Server
