/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.validation;


/**
 * A sample validator interface for {@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface Fixed_Priority_SchedulerValidator {
	boolean validate();

	boolean validateAvg_Context_Switch(double value);
	boolean validateBest_Context_Switch(double value);
	boolean validateMax_Priority(int value);
	boolean validateMin_Priority(int value);
	boolean validateWorst_Context_Switch(double value);
}
