/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sporadic Server Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getBackground_Priority <em>Background Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getInitial_Capacity <em>Initial Capacity</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getMax_Pending_Replenishments <em>Max Pending Replenishments</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getNormal_Priority <em>Normal Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getPreassigned <em>Preassigned</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getReplenishment_Period <em>Replenishment Period</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSporadic_Server_Policy()
 * @model extendedMetaData="name='Sporadic_Server_Policy' kind='empty'"
 * @generated
 */
public interface Sporadic_Server_Policy extends EObject {
	/**
	 * Returns the value of the '<em><b>Background Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Background Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Background Priority</em>' attribute.
	 * @see #setBackground_Priority(int)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSporadic_Server_Policy_Background_Priority()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Priority"
	 *        extendedMetaData="kind='attribute' name='Background_Priority'"
	 * @generated
	 */
	int getBackground_Priority();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getBackground_Priority <em>Background Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Background Priority</em>' attribute.
	 * @see #getBackground_Priority()
	 * @generated
	 */
	void setBackground_Priority(int value);

	/**
	 * Returns the value of the '<em><b>Initial Capacity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Capacity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Capacity</em>' attribute.
	 * @see #setInitial_Capacity(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSporadic_Server_Policy_Initial_Capacity()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Time"
	 *        extendedMetaData="kind='attribute' name='Initial_Capacity'"
	 * @generated
	 */
	double getInitial_Capacity();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getInitial_Capacity <em>Initial Capacity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Capacity</em>' attribute.
	 * @see #getInitial_Capacity()
	 * @generated
	 */
	void setInitial_Capacity(double value);

	/**
	 * Returns the value of the '<em><b>Max Pending Replenishments</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Pending Replenishments</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Pending Replenishments</em>' attribute.
	 * @see #setMax_Pending_Replenishments(int)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSporadic_Server_Policy_Max_Pending_Replenishments()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Positive"
	 *        extendedMetaData="kind='attribute' name='Max_Pending_Replenishments'"
	 * @generated
	 */
	int getMax_Pending_Replenishments();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getMax_Pending_Replenishments <em>Max Pending Replenishments</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Pending Replenishments</em>' attribute.
	 * @see #getMax_Pending_Replenishments()
	 * @generated
	 */
	void setMax_Pending_Replenishments(int value);

	/**
	 * Returns the value of the '<em><b>Normal Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Normal Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Normal Priority</em>' attribute.
	 * @see #setNormal_Priority(int)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSporadic_Server_Policy_Normal_Priority()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Priority"
	 *        extendedMetaData="kind='attribute' name='Normal_Priority'"
	 * @generated
	 */
	int getNormal_Priority();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getNormal_Priority <em>Normal Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Normal Priority</em>' attribute.
	 * @see #getNormal_Priority()
	 * @generated
	 */
	void setNormal_Priority(int value);

	/**
	 * Returns the value of the '<em><b>Preassigned</b></em>' attribute.
	 * The literals are from the enumeration {@link es.esi.gemde.vv.mast.mastmodel.Assertion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preassigned</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preassigned</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Assertion
	 * @see #isSetPreassigned()
	 * @see #unsetPreassigned()
	 * @see #setPreassigned(Assertion)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSporadic_Server_Policy_Preassigned()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='Preassigned'"
	 * @generated
	 */
	Assertion getPreassigned();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getPreassigned <em>Preassigned</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preassigned</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Assertion
	 * @see #isSetPreassigned()
	 * @see #unsetPreassigned()
	 * @see #getPreassigned()
	 * @generated
	 */
	void setPreassigned(Assertion value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getPreassigned <em>Preassigned</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPreassigned()
	 * @see #getPreassigned()
	 * @see #setPreassigned(Assertion)
	 * @generated
	 */
	void unsetPreassigned();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getPreassigned <em>Preassigned</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Preassigned</em>' attribute is set.
	 * @see #unsetPreassigned()
	 * @see #getPreassigned()
	 * @see #setPreassigned(Assertion)
	 * @generated
	 */
	boolean isSetPreassigned();

	/**
	 * Returns the value of the '<em><b>Replenishment Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Replenishment Period</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Replenishment Period</em>' attribute.
	 * @see #setReplenishment_Period(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSporadic_Server_Policy_Replenishment_Period()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Time"
	 *        extendedMetaData="kind='attribute' name='Replenishment_Period'"
	 * @generated
	 */
	double getReplenishment_Period();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getReplenishment_Period <em>Replenishment Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Replenishment Period</em>' attribute.
	 * @see #getReplenishment_Period()
	 * @generated
	 */
	void setReplenishment_Period(double value);

} // Sporadic_Server_Policy
