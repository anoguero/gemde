/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Overridden Fixed Priority</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Overridden_Fixed_Priority#getThe_Priority <em>The Priority</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getOverridden_Fixed_Priority()
 * @model extendedMetaData="name='Overridden_Fixed_Priority' kind='empty'"
 * @generated
 */
public interface Overridden_Fixed_Priority extends EObject {
	/**
	 * Returns the value of the '<em><b>The Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>The Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>The Priority</em>' attribute.
	 * @see #setThe_Priority(int)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getOverridden_Fixed_Priority_The_Priority()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Priority" required="true"
	 *        extendedMetaData="kind='attribute' name='The_Priority'"
	 * @generated
	 */
	int getThe_Priority();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Overridden_Fixed_Priority#getThe_Priority <em>The Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>The Priority</em>' attribute.
	 * @see #getThe_Priority()
	 * @generated
	 */
	void setThe_Priority(int value);

} // Overridden_Fixed_Priority
