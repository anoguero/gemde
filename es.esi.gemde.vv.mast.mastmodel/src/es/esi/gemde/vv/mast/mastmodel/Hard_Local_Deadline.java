/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hard Local Deadline</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Hard_Local_Deadline#getDeadline <em>Deadline</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getHard_Local_Deadline()
 * @model extendedMetaData="name='Hard_Local_Deadline' kind='empty'"
 * @generated
 */
public interface Hard_Local_Deadline extends EObject {
	/**
	 * Returns the value of the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deadline</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deadline</em>' attribute.
	 * @see #setDeadline(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getHard_Local_Deadline_Deadline()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Time"
	 *        extendedMetaData="kind='attribute' name='Deadline'"
	 * @generated
	 */
	double getDeadline();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Hard_Local_Deadline#getDeadline <em>Deadline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deadline</em>' attribute.
	 * @see #getDeadline()
	 * @generated
	 */
	void setDeadline(double value);

} // Hard_Local_Deadline
