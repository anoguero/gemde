/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage
 * @generated
 */
public interface ModelFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ModelFactory eINSTANCE = es.esi.gemde.vv.mast.mastmodel.impl.ModelFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Activity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Activity</em>'.
	 * @generated
	 */
	Activity createActivity();

	/**
	 * Returns a new object of class '<em>Alarm Clock System Timer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Alarm Clock System Timer</em>'.
	 * @generated
	 */
	Alarm_Clock_System_Timer createAlarm_Clock_System_Timer();

	/**
	 * Returns a new object of class '<em>Barrier</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Barrier</em>'.
	 * @generated
	 */
	Barrier createBarrier();

	/**
	 * Returns a new object of class '<em>Bursty External Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Bursty External Event</em>'.
	 * @generated
	 */
	Bursty_External_Event createBursty_External_Event();

	/**
	 * Returns a new object of class '<em>Character Packet Driver</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Character Packet Driver</em>'.
	 * @generated
	 */
	Character_Packet_Driver createCharacter_Packet_Driver();

	/**
	 * Returns a new object of class '<em>Composite Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Composite Operation</em>'.
	 * @generated
	 */
	Composite_Operation createComposite_Operation();

	/**
	 * Returns a new object of class '<em>Composite Timing Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Composite Timing Requirement</em>'.
	 * @generated
	 */
	Composite_Timing_Requirement createComposite_Timing_Requirement();

	/**
	 * Returns a new object of class '<em>Concentrator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Concentrator</em>'.
	 * @generated
	 */
	Concentrator createConcentrator();

	/**
	 * Returns a new object of class '<em>Delay</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Delay</em>'.
	 * @generated
	 */
	Delay createDelay();

	/**
	 * Returns a new object of class '<em>Delivery Server</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Delivery Server</em>'.
	 * @generated
	 */
	Delivery_Server createDelivery_Server();

	/**
	 * Returns a new object of class '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Document Root</em>'.
	 * @generated
	 */
	Document_Root createDocument_Root();

	/**
	 * Returns a new object of class '<em>EDF Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EDF Policy</em>'.
	 * @generated
	 */
	EDF_Policy createEDF_Policy();

	/**
	 * Returns a new object of class '<em>EDF Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>EDF Scheduler</em>'.
	 * @generated
	 */
	EDF_Scheduler createEDF_Scheduler();

	/**
	 * Returns a new object of class '<em>Enclosing Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Enclosing Operation</em>'.
	 * @generated
	 */
	Enclosing_Operation createEnclosing_Operation();

	/**
	 * Returns a new object of class '<em>Fixed Priority Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fixed Priority Policy</em>'.
	 * @generated
	 */
	Fixed_Priority_Policy createFixed_Priority_Policy();

	/**
	 * Returns a new object of class '<em>Fixed Priority Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fixed Priority Scheduler</em>'.
	 * @generated
	 */
	Fixed_Priority_Scheduler createFixed_Priority_Scheduler();

	/**
	 * Returns a new object of class '<em>FP Packet Based Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>FP Packet Based Scheduler</em>'.
	 * @generated
	 */
	FP_Packet_Based_Scheduler createFP_Packet_Based_Scheduler();

	/**
	 * Returns a new object of class '<em>Global Max Miss Ratio</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Global Max Miss Ratio</em>'.
	 * @generated
	 */
	Global_Max_Miss_Ratio createGlobal_Max_Miss_Ratio();

	/**
	 * Returns a new object of class '<em>Hard Global Deadline</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hard Global Deadline</em>'.
	 * @generated
	 */
	Hard_Global_Deadline createHard_Global_Deadline();

	/**
	 * Returns a new object of class '<em>Hard Local Deadline</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hard Local Deadline</em>'.
	 * @generated
	 */
	Hard_Local_Deadline createHard_Local_Deadline();

	/**
	 * Returns a new object of class '<em>Immediate Ceiling Resource</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Immediate Ceiling Resource</em>'.
	 * @generated
	 */
	Immediate_Ceiling_Resource createImmediate_Ceiling_Resource();

	/**
	 * Returns a new object of class '<em>Interrupt FP Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Interrupt FP Policy</em>'.
	 * @generated
	 */
	Interrupt_FP_Policy createInterrupt_FP_Policy();

	/**
	 * Returns a new object of class '<em>List of Drivers</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>List of Drivers</em>'.
	 * @generated
	 */
	List_of_Drivers createList_of_Drivers();

	/**
	 * Returns a new object of class '<em>Local Max Miss Ratio</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Local Max Miss Ratio</em>'.
	 * @generated
	 */
	Local_Max_Miss_Ratio createLocal_Max_Miss_Ratio();

	/**
	 * Returns a new object of class '<em>MAST MODEL</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MAST MODEL</em>'.
	 * @generated
	 */
	MAST_MODEL createMAST_MODEL();

	/**
	 * Returns a new object of class '<em>Max Output Jitter Req</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Max Output Jitter Req</em>'.
	 * @generated
	 */
	Max_Output_Jitter_Req createMax_Output_Jitter_Req();

	/**
	 * Returns a new object of class '<em>Message Transmission</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Message Transmission</em>'.
	 * @generated
	 */
	Message_Transmission createMessage_Transmission();

	/**
	 * Returns a new object of class '<em>Multicast</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Multicast</em>'.
	 * @generated
	 */
	Multicast createMulticast();

	/**
	 * Returns a new object of class '<em>Non Preemptible FP Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Non Preemptible FP Policy</em>'.
	 * @generated
	 */
	Non_Preemptible_FP_Policy createNon_Preemptible_FP_Policy();

	/**
	 * Returns a new object of class '<em>Offset</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Offset</em>'.
	 * @generated
	 */
	Offset createOffset();

	/**
	 * Returns a new object of class '<em>Overridden Fixed Priority</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Overridden Fixed Priority</em>'.
	 * @generated
	 */
	Overridden_Fixed_Priority createOverridden_Fixed_Priority();

	/**
	 * Returns a new object of class '<em>Overridden Permanent FP</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Overridden Permanent FP</em>'.
	 * @generated
	 */
	Overridden_Permanent_FP createOverridden_Permanent_FP();

	/**
	 * Returns a new object of class '<em>Packet Based Network</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Packet Based Network</em>'.
	 * @generated
	 */
	Packet_Based_Network createPacket_Based_Network();

	/**
	 * Returns a new object of class '<em>Packet Driver</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Packet Driver</em>'.
	 * @generated
	 */
	Packet_Driver createPacket_Driver();

	/**
	 * Returns a new object of class '<em>Periodic External Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Periodic External Event</em>'.
	 * @generated
	 */
	Periodic_External_Event createPeriodic_External_Event();

	/**
	 * Returns a new object of class '<em>Polling Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Polling Policy</em>'.
	 * @generated
	 */
	Polling_Policy createPolling_Policy();

	/**
	 * Returns a new object of class '<em>Primary Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Primary Scheduler</em>'.
	 * @generated
	 */
	Primary_Scheduler createPrimary_Scheduler();

	/**
	 * Returns a new object of class '<em>Priority Inheritance Resource</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Priority Inheritance Resource</em>'.
	 * @generated
	 */
	Priority_Inheritance_Resource createPriority_Inheritance_Resource();

	/**
	 * Returns a new object of class '<em>Query Server</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Query Server</em>'.
	 * @generated
	 */
	Query_Server createQuery_Server();

	/**
	 * Returns a new object of class '<em>Rate Divisor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rate Divisor</em>'.
	 * @generated
	 */
	Rate_Divisor createRate_Divisor();

	/**
	 * Returns a new object of class '<em>Regular Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Regular Event</em>'.
	 * @generated
	 */
	Regular_Event createRegular_Event();

	/**
	 * Returns a new object of class '<em>Regular Processor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Regular Processor</em>'.
	 * @generated
	 */
	Regular_Processor createRegular_Processor();

	/**
	 * Returns a new object of class '<em>Regular Scheduling Server</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Regular Scheduling Server</em>'.
	 * @generated
	 */
	Regular_Scheduling_Server createRegular_Scheduling_Server();

	/**
	 * Returns a new object of class '<em>Regular Transaction</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Regular Transaction</em>'.
	 * @generated
	 */
	Regular_Transaction createRegular_Transaction();

	/**
	 * Returns a new object of class '<em>RTEP Packet Driver</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>RTEP Packet Driver</em>'.
	 * @generated
	 */
	RTEP_PacketDriver createRTEP_PacketDriver();

	/**
	 * Returns a new object of class '<em>Secondary Scheduler</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Secondary Scheduler</em>'.
	 * @generated
	 */
	Secondary_Scheduler createSecondary_Scheduler();

	/**
	 * Returns a new object of class '<em>Simple Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simple Operation</em>'.
	 * @generated
	 */
	Simple_Operation createSimple_Operation();

	/**
	 * Returns a new object of class '<em>Singular External Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Singular External Event</em>'.
	 * @generated
	 */
	Singular_External_Event createSingular_External_Event();

	/**
	 * Returns a new object of class '<em>Soft Global Deadline</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Soft Global Deadline</em>'.
	 * @generated
	 */
	Soft_Global_Deadline createSoft_Global_Deadline();

	/**
	 * Returns a new object of class '<em>Soft Local Deadline</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Soft Local Deadline</em>'.
	 * @generated
	 */
	Soft_Local_Deadline createSoft_Local_Deadline();

	/**
	 * Returns a new object of class '<em>Sporadic External Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sporadic External Event</em>'.
	 * @generated
	 */
	Sporadic_External_Event createSporadic_External_Event();

	/**
	 * Returns a new object of class '<em>Sporadic Server Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sporadic Server Policy</em>'.
	 * @generated
	 */
	Sporadic_Server_Policy createSporadic_Server_Policy();

	/**
	 * Returns a new object of class '<em>SRP Parameters</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SRP Parameters</em>'.
	 * @generated
	 */
	SRP_Parameters createSRP_Parameters();

	/**
	 * Returns a new object of class '<em>SRP Resource</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SRP Resource</em>'.
	 * @generated
	 */
	SRP_Resource createSRP_Resource();

	/**
	 * Returns a new object of class '<em>System Timed Activity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>System Timed Activity</em>'.
	 * @generated
	 */
	System_Timed_Activity createSystem_Timed_Activity();

	/**
	 * Returns a new object of class '<em>Ticker System Timer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ticker System Timer</em>'.
	 * @generated
	 */
	Ticker_System_Timer createTicker_System_Timer();

	/**
	 * Returns a new object of class '<em>Unbounded External Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Unbounded External Event</em>'.
	 * @generated
	 */
	Unbounded_External_Event createUnbounded_External_Event();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ModelPackage getModelPackage();

} //ModelFactory
