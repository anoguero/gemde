/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>System Timed Activity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity#getActivity_Operation <em>Activity Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity#getActivity_Server <em>Activity Server</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity#getInput_Event <em>Input Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity#getOutput_Event <em>Output Event</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSystem_Timed_Activity()
 * @model extendedMetaData="name='System_Timed_Activity' kind='empty'"
 * @generated
 */
public interface System_Timed_Activity extends EObject {
	/**
	 * Returns the value of the '<em><b>Activity Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activity Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activity Operation</em>' attribute.
	 * @see #setActivity_Operation(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSystem_Timed_Activity_Activity_Operation()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref" required="true"
	 *        extendedMetaData="kind='attribute' name='Activity_Operation'"
	 * @generated
	 */
	String getActivity_Operation();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity#getActivity_Operation <em>Activity Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Activity Operation</em>' attribute.
	 * @see #getActivity_Operation()
	 * @generated
	 */
	void setActivity_Operation(String value);

	/**
	 * Returns the value of the '<em><b>Activity Server</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activity Server</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activity Server</em>' attribute.
	 * @see #setActivity_Server(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSystem_Timed_Activity_Activity_Server()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref" required="true"
	 *        extendedMetaData="kind='attribute' name='Activity_Server'"
	 * @generated
	 */
	String getActivity_Server();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity#getActivity_Server <em>Activity Server</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Activity Server</em>' attribute.
	 * @see #getActivity_Server()
	 * @generated
	 */
	void setActivity_Server(String value);

	/**
	 * Returns the value of the '<em><b>Input Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Event</em>' attribute.
	 * @see #setInput_Event(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSystem_Timed_Activity_Input_Event()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref" required="true"
	 *        extendedMetaData="kind='attribute' name='Input_Event'"
	 * @generated
	 */
	String getInput_Event();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity#getInput_Event <em>Input Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Event</em>' attribute.
	 * @see #getInput_Event()
	 * @generated
	 */
	void setInput_Event(String value);

	/**
	 * Returns the value of the '<em><b>Output Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Event</em>' attribute.
	 * @see #setOutput_Event(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSystem_Timed_Activity_Output_Event()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref" required="true"
	 *        extendedMetaData="kind='attribute' name='Output_Event'"
	 * @generated
	 */
	String getOutput_Event();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity#getOutput_Event <em>Output Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Event</em>' attribute.
	 * @see #getOutput_Event()
	 * @generated
	 */
	void setOutput_Event(String value);

} // System_Timed_Activity
