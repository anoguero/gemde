/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>FP Packet Based Scheduler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler#getMax_Priority <em>Max Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler#getMin_Priority <em>Min Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler#getPacket_Overhead_Avg_Size <em>Packet Overhead Avg Size</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler#getPacket_Overhead_Max_Size <em>Packet Overhead Max Size</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler#getPacket_Overhead_Min_Size <em>Packet Overhead Min Size</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getFP_Packet_Based_Scheduler()
 * @model extendedMetaData="name='FP_Packet_Based_Scheduler' kind='empty'"
 * @generated
 */
public interface FP_Packet_Based_Scheduler extends EObject {
	/**
	 * Returns the value of the '<em><b>Max Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Priority</em>' attribute.
	 * @see #setMax_Priority(int)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getFP_Packet_Based_Scheduler_Max_Priority()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Priority"
	 *        extendedMetaData="kind='attribute' name='Max_Priority'"
	 * @generated
	 */
	int getMax_Priority();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler#getMax_Priority <em>Max Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Priority</em>' attribute.
	 * @see #getMax_Priority()
	 * @generated
	 */
	void setMax_Priority(int value);

	/**
	 * Returns the value of the '<em><b>Min Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Priority</em>' attribute.
	 * @see #setMin_Priority(int)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getFP_Packet_Based_Scheduler_Min_Priority()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Priority"
	 *        extendedMetaData="kind='attribute' name='Min_Priority'"
	 * @generated
	 */
	int getMin_Priority();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler#getMin_Priority <em>Min Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Priority</em>' attribute.
	 * @see #getMin_Priority()
	 * @generated
	 */
	void setMin_Priority(int value);

	/**
	 * Returns the value of the '<em><b>Packet Overhead Avg Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Packet Overhead Avg Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packet Overhead Avg Size</em>' attribute.
	 * @see #setPacket_Overhead_Avg_Size(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getFP_Packet_Based_Scheduler_Packet_Overhead_Avg_Size()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Bit_Count"
	 *        extendedMetaData="kind='attribute' name='Packet_Overhead_Avg_Size'"
	 * @generated
	 */
	double getPacket_Overhead_Avg_Size();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler#getPacket_Overhead_Avg_Size <em>Packet Overhead Avg Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Packet Overhead Avg Size</em>' attribute.
	 * @see #getPacket_Overhead_Avg_Size()
	 * @generated
	 */
	void setPacket_Overhead_Avg_Size(double value);

	/**
	 * Returns the value of the '<em><b>Packet Overhead Max Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Packet Overhead Max Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packet Overhead Max Size</em>' attribute.
	 * @see #setPacket_Overhead_Max_Size(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getFP_Packet_Based_Scheduler_Packet_Overhead_Max_Size()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Bit_Count"
	 *        extendedMetaData="kind='attribute' name='Packet_Overhead_Max_Size'"
	 * @generated
	 */
	double getPacket_Overhead_Max_Size();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler#getPacket_Overhead_Max_Size <em>Packet Overhead Max Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Packet Overhead Max Size</em>' attribute.
	 * @see #getPacket_Overhead_Max_Size()
	 * @generated
	 */
	void setPacket_Overhead_Max_Size(double value);

	/**
	 * Returns the value of the '<em><b>Packet Overhead Min Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Packet Overhead Min Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packet Overhead Min Size</em>' attribute.
	 * @see #setPacket_Overhead_Min_Size(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getFP_Packet_Based_Scheduler_Packet_Overhead_Min_Size()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Bit_Count"
	 *        extendedMetaData="kind='attribute' name='Packet_Overhead_Min_Size'"
	 * @generated
	 */
	double getPacket_Overhead_Min_Size();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler#getPacket_Overhead_Min_Size <em>Packet Overhead Min Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Packet Overhead Min Size</em>' attribute.
	 * @see #getPacket_Overhead_Min_Size()
	 * @generated
	 */
	void setPacket_Overhead_Min_Size(double value);

} // FP_Packet_Based_Scheduler
