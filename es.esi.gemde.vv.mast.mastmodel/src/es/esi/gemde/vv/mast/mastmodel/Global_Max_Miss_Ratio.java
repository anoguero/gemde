/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Global Max Miss Ratio</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio#getDeadline <em>Deadline</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio#getRatio <em>Ratio</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio#getReferenced_Event <em>Referenced Event</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getGlobal_Max_Miss_Ratio()
 * @model extendedMetaData="name='Global_Max_Miss_Ratio' kind='empty'"
 * @generated
 */
public interface Global_Max_Miss_Ratio extends EObject {
	/**
	 * Returns the value of the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deadline</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deadline</em>' attribute.
	 * @see #setDeadline(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getGlobal_Max_Miss_Ratio_Deadline()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Time"
	 *        extendedMetaData="kind='attribute' name='Deadline'"
	 * @generated
	 */
	double getDeadline();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio#getDeadline <em>Deadline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deadline</em>' attribute.
	 * @see #getDeadline()
	 * @generated
	 */
	void setDeadline(double value);

	/**
	 * Returns the value of the '<em><b>Ratio</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ratio</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ratio</em>' attribute.
	 * @see #setRatio(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getGlobal_Max_Miss_Ratio_Ratio()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Percentage"
	 *        extendedMetaData="kind='attribute' name='Ratio'"
	 * @generated
	 */
	double getRatio();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio#getRatio <em>Ratio</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ratio</em>' attribute.
	 * @see #getRatio()
	 * @generated
	 */
	void setRatio(double value);

	/**
	 * Returns the value of the '<em><b>Referenced Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Event</em>' attribute.
	 * @see #setReferenced_Event(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getGlobal_Max_Miss_Ratio_Referenced_Event()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref" required="true"
	 *        extendedMetaData="kind='attribute' name='Referenced_Event'"
	 * @generated
	 */
	String getReferenced_Event();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio#getReferenced_Event <em>Referenced Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referenced Event</em>' attribute.
	 * @see #getReferenced_Event()
	 * @generated
	 */
	void setReferenced_Event(String value);

} // Global_Max_Miss_Ratio
