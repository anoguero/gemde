/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>FP Packet Based Scheduler</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.FP_Packet_Based_SchedulerImpl#getMax_Priority <em>Max Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.FP_Packet_Based_SchedulerImpl#getMin_Priority <em>Min Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.FP_Packet_Based_SchedulerImpl#getPacket_Overhead_Avg_Size <em>Packet Overhead Avg Size</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.FP_Packet_Based_SchedulerImpl#getPacket_Overhead_Max_Size <em>Packet Overhead Max Size</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.FP_Packet_Based_SchedulerImpl#getPacket_Overhead_Min_Size <em>Packet Overhead Min Size</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class FP_Packet_Based_SchedulerImpl extends EObjectImpl implements FP_Packet_Based_Scheduler {
	/**
	 * The default value of the '{@link #getMax_Priority() <em>Max Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_Priority()
	 * @generated
	 * @ordered
	 */
	protected static final int MAX_PRIORITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMax_Priority() <em>Max Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_Priority()
	 * @generated
	 * @ordered
	 */
	protected int max_Priority = MAX_PRIORITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getMin_Priority() <em>Min Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin_Priority()
	 * @generated
	 * @ordered
	 */
	protected static final int MIN_PRIORITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMin_Priority() <em>Min Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin_Priority()
	 * @generated
	 * @ordered
	 */
	protected int min_Priority = MIN_PRIORITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getPacket_Overhead_Avg_Size() <em>Packet Overhead Avg Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Overhead_Avg_Size()
	 * @generated
	 * @ordered
	 */
	protected static final double PACKET_OVERHEAD_AVG_SIZE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getPacket_Overhead_Avg_Size() <em>Packet Overhead Avg Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Overhead_Avg_Size()
	 * @generated
	 * @ordered
	 */
	protected double packet_Overhead_Avg_Size = PACKET_OVERHEAD_AVG_SIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getPacket_Overhead_Max_Size() <em>Packet Overhead Max Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Overhead_Max_Size()
	 * @generated
	 * @ordered
	 */
	protected static final double PACKET_OVERHEAD_MAX_SIZE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getPacket_Overhead_Max_Size() <em>Packet Overhead Max Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Overhead_Max_Size()
	 * @generated
	 * @ordered
	 */
	protected double packet_Overhead_Max_Size = PACKET_OVERHEAD_MAX_SIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getPacket_Overhead_Min_Size() <em>Packet Overhead Min Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Overhead_Min_Size()
	 * @generated
	 * @ordered
	 */
	protected static final double PACKET_OVERHEAD_MIN_SIZE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getPacket_Overhead_Min_Size() <em>Packet Overhead Min Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Overhead_Min_Size()
	 * @generated
	 * @ordered
	 */
	protected double packet_Overhead_Min_Size = PACKET_OVERHEAD_MIN_SIZE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FP_Packet_Based_SchedulerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.FP_PACKET_BASED_SCHEDULER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMax_Priority() {
		return max_Priority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMax_Priority(int newMax_Priority) {
		int oldMax_Priority = max_Priority;
		max_Priority = newMax_Priority;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.FP_PACKET_BASED_SCHEDULER__MAX_PRIORITY, oldMax_Priority, max_Priority));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMin_Priority() {
		return min_Priority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMin_Priority(int newMin_Priority) {
		int oldMin_Priority = min_Priority;
		min_Priority = newMin_Priority;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.FP_PACKET_BASED_SCHEDULER__MIN_PRIORITY, oldMin_Priority, min_Priority));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPacket_Overhead_Avg_Size() {
		return packet_Overhead_Avg_Size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPacket_Overhead_Avg_Size(double newPacket_Overhead_Avg_Size) {
		double oldPacket_Overhead_Avg_Size = packet_Overhead_Avg_Size;
		packet_Overhead_Avg_Size = newPacket_Overhead_Avg_Size;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_AVG_SIZE, oldPacket_Overhead_Avg_Size, packet_Overhead_Avg_Size));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPacket_Overhead_Max_Size() {
		return packet_Overhead_Max_Size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPacket_Overhead_Max_Size(double newPacket_Overhead_Max_Size) {
		double oldPacket_Overhead_Max_Size = packet_Overhead_Max_Size;
		packet_Overhead_Max_Size = newPacket_Overhead_Max_Size;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_MAX_SIZE, oldPacket_Overhead_Max_Size, packet_Overhead_Max_Size));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPacket_Overhead_Min_Size() {
		return packet_Overhead_Min_Size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPacket_Overhead_Min_Size(double newPacket_Overhead_Min_Size) {
		double oldPacket_Overhead_Min_Size = packet_Overhead_Min_Size;
		packet_Overhead_Min_Size = newPacket_Overhead_Min_Size;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_MIN_SIZE, oldPacket_Overhead_Min_Size, packet_Overhead_Min_Size));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__MAX_PRIORITY:
				return getMax_Priority();
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__MIN_PRIORITY:
				return getMin_Priority();
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_AVG_SIZE:
				return getPacket_Overhead_Avg_Size();
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_MAX_SIZE:
				return getPacket_Overhead_Max_Size();
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_MIN_SIZE:
				return getPacket_Overhead_Min_Size();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__MAX_PRIORITY:
				setMax_Priority((Integer)newValue);
				return;
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__MIN_PRIORITY:
				setMin_Priority((Integer)newValue);
				return;
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_AVG_SIZE:
				setPacket_Overhead_Avg_Size((Double)newValue);
				return;
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_MAX_SIZE:
				setPacket_Overhead_Max_Size((Double)newValue);
				return;
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_MIN_SIZE:
				setPacket_Overhead_Min_Size((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__MAX_PRIORITY:
				setMax_Priority(MAX_PRIORITY_EDEFAULT);
				return;
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__MIN_PRIORITY:
				setMin_Priority(MIN_PRIORITY_EDEFAULT);
				return;
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_AVG_SIZE:
				setPacket_Overhead_Avg_Size(PACKET_OVERHEAD_AVG_SIZE_EDEFAULT);
				return;
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_MAX_SIZE:
				setPacket_Overhead_Max_Size(PACKET_OVERHEAD_MAX_SIZE_EDEFAULT);
				return;
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_MIN_SIZE:
				setPacket_Overhead_Min_Size(PACKET_OVERHEAD_MIN_SIZE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__MAX_PRIORITY:
				return max_Priority != MAX_PRIORITY_EDEFAULT;
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__MIN_PRIORITY:
				return min_Priority != MIN_PRIORITY_EDEFAULT;
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_AVG_SIZE:
				return packet_Overhead_Avg_Size != PACKET_OVERHEAD_AVG_SIZE_EDEFAULT;
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_MAX_SIZE:
				return packet_Overhead_Max_Size != PACKET_OVERHEAD_MAX_SIZE_EDEFAULT;
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_MIN_SIZE:
				return packet_Overhead_Min_Size != PACKET_OVERHEAD_MIN_SIZE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Max_Priority: ");
		result.append(max_Priority);
		result.append(", Min_Priority: ");
		result.append(min_Priority);
		result.append(", Packet_Overhead_Avg_Size: ");
		result.append(packet_Overhead_Avg_Size);
		result.append(", Packet_Overhead_Max_Size: ");
		result.append(packet_Overhead_Max_Size);
		result.append(", Packet_Overhead_Min_Size: ");
		result.append(packet_Overhead_Min_Size);
		result.append(')');
		return result.toString();
	}

} //FP_Packet_Based_SchedulerImpl
