/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Delivery Server</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Delivery_Server#getDelivery_Policy <em>Delivery Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Delivery_Server#getInput_Event <em>Input Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Delivery_Server#getOutput_Events_List <em>Output Events List</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getDelivery_Server()
 * @model extendedMetaData="name='Delivery_Server' kind='empty'"
 * @generated
 */
public interface Delivery_Server extends EObject {
	/**
	 * Returns the value of the '<em><b>Delivery Policy</b></em>' attribute.
	 * The literals are from the enumeration {@link es.esi.gemde.vv.mast.mastmodel.Delivery_Policy}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delivery Policy</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delivery Policy</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Delivery_Policy
	 * @see #isSetDelivery_Policy()
	 * @see #unsetDelivery_Policy()
	 * @see #setDelivery_Policy(Delivery_Policy)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getDelivery_Server_Delivery_Policy()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='Delivery_Policy'"
	 * @generated
	 */
	Delivery_Policy getDelivery_Policy();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Delivery_Server#getDelivery_Policy <em>Delivery Policy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delivery Policy</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Delivery_Policy
	 * @see #isSetDelivery_Policy()
	 * @see #unsetDelivery_Policy()
	 * @see #getDelivery_Policy()
	 * @generated
	 */
	void setDelivery_Policy(Delivery_Policy value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Delivery_Server#getDelivery_Policy <em>Delivery Policy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDelivery_Policy()
	 * @see #getDelivery_Policy()
	 * @see #setDelivery_Policy(Delivery_Policy)
	 * @generated
	 */
	void unsetDelivery_Policy();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Delivery_Server#getDelivery_Policy <em>Delivery Policy</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Delivery Policy</em>' attribute is set.
	 * @see #unsetDelivery_Policy()
	 * @see #getDelivery_Policy()
	 * @see #setDelivery_Policy(Delivery_Policy)
	 * @generated
	 */
	boolean isSetDelivery_Policy();

	/**
	 * Returns the value of the '<em><b>Input Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Event</em>' attribute.
	 * @see #setInput_Event(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getDelivery_Server_Input_Event()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref" required="true"
	 *        extendedMetaData="kind='attribute' name='Input_Event'"
	 * @generated
	 */
	String getInput_Event();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Delivery_Server#getInput_Event <em>Input Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Event</em>' attribute.
	 * @see #getInput_Event()
	 * @generated
	 */
	void setInput_Event(String value);

	/**
	 * Returns the value of the '<em><b>Output Events List</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Events List</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Events List</em>' attribute.
	 * @see #setOutput_Events_List(List)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getDelivery_Server_Output_Events_List()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref_List" required="true" many="false"
	 *        extendedMetaData="kind='attribute' name='Output_Events_List'"
	 * @generated
	 */
	List<String> getOutput_Events_List();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Delivery_Server#getOutput_Events_List <em>Output Events List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Events List</em>' attribute.
	 * @see #getOutput_Events_List()
	 * @generated
	 */
	void setOutput_Events_List(List<String> value);

} // Delivery_Server
