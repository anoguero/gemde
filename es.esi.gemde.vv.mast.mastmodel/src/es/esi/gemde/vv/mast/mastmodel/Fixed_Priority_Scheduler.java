/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Fixed Priority Scheduler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler#getAvg_Context_Switch <em>Avg Context Switch</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler#getBest_Context_Switch <em>Best Context Switch</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler#getMax_Priority <em>Max Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler#getMin_Priority <em>Min Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler#getWorst_Context_Switch <em>Worst Context Switch</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getFixed_Priority_Scheduler()
 * @model extendedMetaData="name='Fixed_Priority_Scheduler' kind='empty'"
 * @generated
 */
public interface Fixed_Priority_Scheduler extends EObject {
	/**
	 * Returns the value of the '<em><b>Avg Context Switch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Avg Context Switch</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Avg Context Switch</em>' attribute.
	 * @see #setAvg_Context_Switch(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getFixed_Priority_Scheduler_Avg_Context_Switch()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Avg_Context_Switch'"
	 * @generated
	 */
	double getAvg_Context_Switch();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler#getAvg_Context_Switch <em>Avg Context Switch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Avg Context Switch</em>' attribute.
	 * @see #getAvg_Context_Switch()
	 * @generated
	 */
	void setAvg_Context_Switch(double value);

	/**
	 * Returns the value of the '<em><b>Best Context Switch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Best Context Switch</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Best Context Switch</em>' attribute.
	 * @see #setBest_Context_Switch(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getFixed_Priority_Scheduler_Best_Context_Switch()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Best_Context_Switch'"
	 * @generated
	 */
	double getBest_Context_Switch();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler#getBest_Context_Switch <em>Best Context Switch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Best Context Switch</em>' attribute.
	 * @see #getBest_Context_Switch()
	 * @generated
	 */
	void setBest_Context_Switch(double value);

	/**
	 * Returns the value of the '<em><b>Max Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Priority</em>' attribute.
	 * @see #setMax_Priority(int)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getFixed_Priority_Scheduler_Max_Priority()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Priority"
	 *        extendedMetaData="kind='attribute' name='Max_Priority'"
	 * @generated
	 */
	int getMax_Priority();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler#getMax_Priority <em>Max Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Priority</em>' attribute.
	 * @see #getMax_Priority()
	 * @generated
	 */
	void setMax_Priority(int value);

	/**
	 * Returns the value of the '<em><b>Min Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Priority</em>' attribute.
	 * @see #setMin_Priority(int)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getFixed_Priority_Scheduler_Min_Priority()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Priority"
	 *        extendedMetaData="kind='attribute' name='Min_Priority'"
	 * @generated
	 */
	int getMin_Priority();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler#getMin_Priority <em>Min Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Priority</em>' attribute.
	 * @see #getMin_Priority()
	 * @generated
	 */
	void setMin_Priority(int value);

	/**
	 * Returns the value of the '<em><b>Worst Context Switch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Worst Context Switch</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Worst Context Switch</em>' attribute.
	 * @see #setWorst_Context_Switch(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getFixed_Priority_Scheduler_Worst_Context_Switch()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Worst_Context_Switch'"
	 * @generated
	 */
	double getWorst_Context_Switch();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler#getWorst_Context_Switch <em>Worst Context Switch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Worst Context Switch</em>' attribute.
	 * @see #getWorst_Context_Switch()
	 * @generated
	 */
	void setWorst_Context_Switch(double value);

} // Fixed_Priority_Scheduler
