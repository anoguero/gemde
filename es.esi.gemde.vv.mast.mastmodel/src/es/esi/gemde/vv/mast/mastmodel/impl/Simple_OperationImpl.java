/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Overridden_Fixed_Priority;
import es.esi.gemde.vv.mast.mastmodel.Overridden_Permanent_FP;
import es.esi.gemde.vv.mast.mastmodel.Simple_Operation;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simple Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Simple_OperationImpl#getOverridden_Fixed_Priority <em>Overridden Fixed Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Simple_OperationImpl#getOverridden_Permanent_FP <em>Overridden Permanent FP</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Simple_OperationImpl#getShared_Resources_List <em>Shared Resources List</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Simple_OperationImpl#getShared_Resources_To_Lock <em>Shared Resources To Lock</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Simple_OperationImpl#getShared_Resources_To_Unlock <em>Shared Resources To Unlock</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Simple_OperationImpl#getAverage_Case_Execution_Time <em>Average Case Execution Time</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Simple_OperationImpl#getBest_Case_Execution_Time <em>Best Case Execution Time</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Simple_OperationImpl#getName <em>Name</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Simple_OperationImpl#getWorst_Case_Execution_Time <em>Worst Case Execution Time</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Simple_OperationImpl extends EObjectImpl implements Simple_Operation {
	/**
	 * The cached value of the '{@link #getOverridden_Fixed_Priority() <em>Overridden Fixed Priority</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverridden_Fixed_Priority()
	 * @generated
	 * @ordered
	 */
	protected Overridden_Fixed_Priority overridden_Fixed_Priority;

	/**
	 * The cached value of the '{@link #getOverridden_Permanent_FP() <em>Overridden Permanent FP</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverridden_Permanent_FP()
	 * @generated
	 * @ordered
	 */
	protected Overridden_Permanent_FP overridden_Permanent_FP;

	/**
	 * The default value of the '{@link #getShared_Resources_List() <em>Shared Resources List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShared_Resources_List()
	 * @generated
	 * @ordered
	 */
	protected static final List<String> SHARED_RESOURCES_LIST_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getShared_Resources_List() <em>Shared Resources List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShared_Resources_List()
	 * @generated
	 * @ordered
	 */
	protected List<String> shared_Resources_List = SHARED_RESOURCES_LIST_EDEFAULT;

	/**
	 * The default value of the '{@link #getShared_Resources_To_Lock() <em>Shared Resources To Lock</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShared_Resources_To_Lock()
	 * @generated
	 * @ordered
	 */
	protected static final List<String> SHARED_RESOURCES_TO_LOCK_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getShared_Resources_To_Lock() <em>Shared Resources To Lock</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShared_Resources_To_Lock()
	 * @generated
	 * @ordered
	 */
	protected List<String> shared_Resources_To_Lock = SHARED_RESOURCES_TO_LOCK_EDEFAULT;

	/**
	 * The default value of the '{@link #getShared_Resources_To_Unlock() <em>Shared Resources To Unlock</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShared_Resources_To_Unlock()
	 * @generated
	 * @ordered
	 */
	protected static final List<String> SHARED_RESOURCES_TO_UNLOCK_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getShared_Resources_To_Unlock() <em>Shared Resources To Unlock</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShared_Resources_To_Unlock()
	 * @generated
	 * @ordered
	 */
	protected List<String> shared_Resources_To_Unlock = SHARED_RESOURCES_TO_UNLOCK_EDEFAULT;

	/**
	 * The default value of the '{@link #getAverage_Case_Execution_Time() <em>Average Case Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAverage_Case_Execution_Time()
	 * @generated
	 * @ordered
	 */
	protected static final double AVERAGE_CASE_EXECUTION_TIME_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getAverage_Case_Execution_Time() <em>Average Case Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAverage_Case_Execution_Time()
	 * @generated
	 * @ordered
	 */
	protected double average_Case_Execution_Time = AVERAGE_CASE_EXECUTION_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getBest_Case_Execution_Time() <em>Best Case Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBest_Case_Execution_Time()
	 * @generated
	 * @ordered
	 */
	protected static final double BEST_CASE_EXECUTION_TIME_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getBest_Case_Execution_Time() <em>Best Case Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBest_Case_Execution_Time()
	 * @generated
	 * @ordered
	 */
	protected double best_Case_Execution_Time = BEST_CASE_EXECUTION_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getWorst_Case_Execution_Time() <em>Worst Case Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorst_Case_Execution_Time()
	 * @generated
	 * @ordered
	 */
	protected static final double WORST_CASE_EXECUTION_TIME_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getWorst_Case_Execution_Time() <em>Worst Case Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorst_Case_Execution_Time()
	 * @generated
	 * @ordered
	 */
	protected double worst_Case_Execution_Time = WORST_CASE_EXECUTION_TIME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Simple_OperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.SIMPLE_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Overridden_Fixed_Priority getOverridden_Fixed_Priority() {
		return overridden_Fixed_Priority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOverridden_Fixed_Priority(Overridden_Fixed_Priority newOverridden_Fixed_Priority, NotificationChain msgs) {
		Overridden_Fixed_Priority oldOverridden_Fixed_Priority = overridden_Fixed_Priority;
		overridden_Fixed_Priority = newOverridden_Fixed_Priority;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.SIMPLE_OPERATION__OVERRIDDEN_FIXED_PRIORITY, oldOverridden_Fixed_Priority, newOverridden_Fixed_Priority);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOverridden_Fixed_Priority(Overridden_Fixed_Priority newOverridden_Fixed_Priority) {
		if (newOverridden_Fixed_Priority != overridden_Fixed_Priority) {
			NotificationChain msgs = null;
			if (overridden_Fixed_Priority != null)
				msgs = ((InternalEObject)overridden_Fixed_Priority).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.SIMPLE_OPERATION__OVERRIDDEN_FIXED_PRIORITY, null, msgs);
			if (newOverridden_Fixed_Priority != null)
				msgs = ((InternalEObject)newOverridden_Fixed_Priority).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.SIMPLE_OPERATION__OVERRIDDEN_FIXED_PRIORITY, null, msgs);
			msgs = basicSetOverridden_Fixed_Priority(newOverridden_Fixed_Priority, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SIMPLE_OPERATION__OVERRIDDEN_FIXED_PRIORITY, newOverridden_Fixed_Priority, newOverridden_Fixed_Priority));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Overridden_Permanent_FP getOverridden_Permanent_FP() {
		return overridden_Permanent_FP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOverridden_Permanent_FP(Overridden_Permanent_FP newOverridden_Permanent_FP, NotificationChain msgs) {
		Overridden_Permanent_FP oldOverridden_Permanent_FP = overridden_Permanent_FP;
		overridden_Permanent_FP = newOverridden_Permanent_FP;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.SIMPLE_OPERATION__OVERRIDDEN_PERMANENT_FP, oldOverridden_Permanent_FP, newOverridden_Permanent_FP);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOverridden_Permanent_FP(Overridden_Permanent_FP newOverridden_Permanent_FP) {
		if (newOverridden_Permanent_FP != overridden_Permanent_FP) {
			NotificationChain msgs = null;
			if (overridden_Permanent_FP != null)
				msgs = ((InternalEObject)overridden_Permanent_FP).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.SIMPLE_OPERATION__OVERRIDDEN_PERMANENT_FP, null, msgs);
			if (newOverridden_Permanent_FP != null)
				msgs = ((InternalEObject)newOverridden_Permanent_FP).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.SIMPLE_OPERATION__OVERRIDDEN_PERMANENT_FP, null, msgs);
			msgs = basicSetOverridden_Permanent_FP(newOverridden_Permanent_FP, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SIMPLE_OPERATION__OVERRIDDEN_PERMANENT_FP, newOverridden_Permanent_FP, newOverridden_Permanent_FP));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<String> getShared_Resources_List() {
		return shared_Resources_List;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShared_Resources_List(List<String> newShared_Resources_List) {
		List<String> oldShared_Resources_List = shared_Resources_List;
		shared_Resources_List = newShared_Resources_List;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SIMPLE_OPERATION__SHARED_RESOURCES_LIST, oldShared_Resources_List, shared_Resources_List));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<String> getShared_Resources_To_Lock() {
		return shared_Resources_To_Lock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShared_Resources_To_Lock(List<String> newShared_Resources_To_Lock) {
		List<String> oldShared_Resources_To_Lock = shared_Resources_To_Lock;
		shared_Resources_To_Lock = newShared_Resources_To_Lock;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SIMPLE_OPERATION__SHARED_RESOURCES_TO_LOCK, oldShared_Resources_To_Lock, shared_Resources_To_Lock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<String> getShared_Resources_To_Unlock() {
		return shared_Resources_To_Unlock;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setShared_Resources_To_Unlock(List<String> newShared_Resources_To_Unlock) {
		List<String> oldShared_Resources_To_Unlock = shared_Resources_To_Unlock;
		shared_Resources_To_Unlock = newShared_Resources_To_Unlock;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SIMPLE_OPERATION__SHARED_RESOURCES_TO_UNLOCK, oldShared_Resources_To_Unlock, shared_Resources_To_Unlock));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getAverage_Case_Execution_Time() {
		return average_Case_Execution_Time;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAverage_Case_Execution_Time(double newAverage_Case_Execution_Time) {
		double oldAverage_Case_Execution_Time = average_Case_Execution_Time;
		average_Case_Execution_Time = newAverage_Case_Execution_Time;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SIMPLE_OPERATION__AVERAGE_CASE_EXECUTION_TIME, oldAverage_Case_Execution_Time, average_Case_Execution_Time));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getBest_Case_Execution_Time() {
		return best_Case_Execution_Time;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBest_Case_Execution_Time(double newBest_Case_Execution_Time) {
		double oldBest_Case_Execution_Time = best_Case_Execution_Time;
		best_Case_Execution_Time = newBest_Case_Execution_Time;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SIMPLE_OPERATION__BEST_CASE_EXECUTION_TIME, oldBest_Case_Execution_Time, best_Case_Execution_Time));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SIMPLE_OPERATION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getWorst_Case_Execution_Time() {
		return worst_Case_Execution_Time;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWorst_Case_Execution_Time(double newWorst_Case_Execution_Time) {
		double oldWorst_Case_Execution_Time = worst_Case_Execution_Time;
		worst_Case_Execution_Time = newWorst_Case_Execution_Time;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SIMPLE_OPERATION__WORST_CASE_EXECUTION_TIME, oldWorst_Case_Execution_Time, worst_Case_Execution_Time));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelPackage.SIMPLE_OPERATION__OVERRIDDEN_FIXED_PRIORITY:
				return basicSetOverridden_Fixed_Priority(null, msgs);
			case ModelPackage.SIMPLE_OPERATION__OVERRIDDEN_PERMANENT_FP:
				return basicSetOverridden_Permanent_FP(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.SIMPLE_OPERATION__OVERRIDDEN_FIXED_PRIORITY:
				return getOverridden_Fixed_Priority();
			case ModelPackage.SIMPLE_OPERATION__OVERRIDDEN_PERMANENT_FP:
				return getOverridden_Permanent_FP();
			case ModelPackage.SIMPLE_OPERATION__SHARED_RESOURCES_LIST:
				return getShared_Resources_List();
			case ModelPackage.SIMPLE_OPERATION__SHARED_RESOURCES_TO_LOCK:
				return getShared_Resources_To_Lock();
			case ModelPackage.SIMPLE_OPERATION__SHARED_RESOURCES_TO_UNLOCK:
				return getShared_Resources_To_Unlock();
			case ModelPackage.SIMPLE_OPERATION__AVERAGE_CASE_EXECUTION_TIME:
				return getAverage_Case_Execution_Time();
			case ModelPackage.SIMPLE_OPERATION__BEST_CASE_EXECUTION_TIME:
				return getBest_Case_Execution_Time();
			case ModelPackage.SIMPLE_OPERATION__NAME:
				return getName();
			case ModelPackage.SIMPLE_OPERATION__WORST_CASE_EXECUTION_TIME:
				return getWorst_Case_Execution_Time();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.SIMPLE_OPERATION__OVERRIDDEN_FIXED_PRIORITY:
				setOverridden_Fixed_Priority((Overridden_Fixed_Priority)newValue);
				return;
			case ModelPackage.SIMPLE_OPERATION__OVERRIDDEN_PERMANENT_FP:
				setOverridden_Permanent_FP((Overridden_Permanent_FP)newValue);
				return;
			case ModelPackage.SIMPLE_OPERATION__SHARED_RESOURCES_LIST:
				setShared_Resources_List((List<String>)newValue);
				return;
			case ModelPackage.SIMPLE_OPERATION__SHARED_RESOURCES_TO_LOCK:
				setShared_Resources_To_Lock((List<String>)newValue);
				return;
			case ModelPackage.SIMPLE_OPERATION__SHARED_RESOURCES_TO_UNLOCK:
				setShared_Resources_To_Unlock((List<String>)newValue);
				return;
			case ModelPackage.SIMPLE_OPERATION__AVERAGE_CASE_EXECUTION_TIME:
				setAverage_Case_Execution_Time((Double)newValue);
				return;
			case ModelPackage.SIMPLE_OPERATION__BEST_CASE_EXECUTION_TIME:
				setBest_Case_Execution_Time((Double)newValue);
				return;
			case ModelPackage.SIMPLE_OPERATION__NAME:
				setName((String)newValue);
				return;
			case ModelPackage.SIMPLE_OPERATION__WORST_CASE_EXECUTION_TIME:
				setWorst_Case_Execution_Time((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.SIMPLE_OPERATION__OVERRIDDEN_FIXED_PRIORITY:
				setOverridden_Fixed_Priority((Overridden_Fixed_Priority)null);
				return;
			case ModelPackage.SIMPLE_OPERATION__OVERRIDDEN_PERMANENT_FP:
				setOverridden_Permanent_FP((Overridden_Permanent_FP)null);
				return;
			case ModelPackage.SIMPLE_OPERATION__SHARED_RESOURCES_LIST:
				setShared_Resources_List(SHARED_RESOURCES_LIST_EDEFAULT);
				return;
			case ModelPackage.SIMPLE_OPERATION__SHARED_RESOURCES_TO_LOCK:
				setShared_Resources_To_Lock(SHARED_RESOURCES_TO_LOCK_EDEFAULT);
				return;
			case ModelPackage.SIMPLE_OPERATION__SHARED_RESOURCES_TO_UNLOCK:
				setShared_Resources_To_Unlock(SHARED_RESOURCES_TO_UNLOCK_EDEFAULT);
				return;
			case ModelPackage.SIMPLE_OPERATION__AVERAGE_CASE_EXECUTION_TIME:
				setAverage_Case_Execution_Time(AVERAGE_CASE_EXECUTION_TIME_EDEFAULT);
				return;
			case ModelPackage.SIMPLE_OPERATION__BEST_CASE_EXECUTION_TIME:
				setBest_Case_Execution_Time(BEST_CASE_EXECUTION_TIME_EDEFAULT);
				return;
			case ModelPackage.SIMPLE_OPERATION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ModelPackage.SIMPLE_OPERATION__WORST_CASE_EXECUTION_TIME:
				setWorst_Case_Execution_Time(WORST_CASE_EXECUTION_TIME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.SIMPLE_OPERATION__OVERRIDDEN_FIXED_PRIORITY:
				return overridden_Fixed_Priority != null;
			case ModelPackage.SIMPLE_OPERATION__OVERRIDDEN_PERMANENT_FP:
				return overridden_Permanent_FP != null;
			case ModelPackage.SIMPLE_OPERATION__SHARED_RESOURCES_LIST:
				return SHARED_RESOURCES_LIST_EDEFAULT == null ? shared_Resources_List != null : !SHARED_RESOURCES_LIST_EDEFAULT.equals(shared_Resources_List);
			case ModelPackage.SIMPLE_OPERATION__SHARED_RESOURCES_TO_LOCK:
				return SHARED_RESOURCES_TO_LOCK_EDEFAULT == null ? shared_Resources_To_Lock != null : !SHARED_RESOURCES_TO_LOCK_EDEFAULT.equals(shared_Resources_To_Lock);
			case ModelPackage.SIMPLE_OPERATION__SHARED_RESOURCES_TO_UNLOCK:
				return SHARED_RESOURCES_TO_UNLOCK_EDEFAULT == null ? shared_Resources_To_Unlock != null : !SHARED_RESOURCES_TO_UNLOCK_EDEFAULT.equals(shared_Resources_To_Unlock);
			case ModelPackage.SIMPLE_OPERATION__AVERAGE_CASE_EXECUTION_TIME:
				return average_Case_Execution_Time != AVERAGE_CASE_EXECUTION_TIME_EDEFAULT;
			case ModelPackage.SIMPLE_OPERATION__BEST_CASE_EXECUTION_TIME:
				return best_Case_Execution_Time != BEST_CASE_EXECUTION_TIME_EDEFAULT;
			case ModelPackage.SIMPLE_OPERATION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ModelPackage.SIMPLE_OPERATION__WORST_CASE_EXECUTION_TIME:
				return worst_Case_Execution_Time != WORST_CASE_EXECUTION_TIME_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Shared_Resources_List: ");
		result.append(shared_Resources_List);
		result.append(", Shared_Resources_To_Lock: ");
		result.append(shared_Resources_To_Lock);
		result.append(", Shared_Resources_To_Unlock: ");
		result.append(shared_Resources_To_Unlock);
		result.append(", Average_Case_Execution_Time: ");
		result.append(average_Case_Execution_Time);
		result.append(", Best_Case_Execution_Time: ");
		result.append(best_Case_Execution_Time);
		result.append(", Name: ");
		result.append(name);
		result.append(", Worst_Case_Execution_Time: ");
		result.append(worst_Case_Execution_Time);
		result.append(')');
		return result.toString();
	}

} //Simple_OperationImpl
