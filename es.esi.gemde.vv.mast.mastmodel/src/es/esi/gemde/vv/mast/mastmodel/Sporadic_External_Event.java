/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sporadic External Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event#getAvg_Interarrival <em>Avg Interarrival</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event#getDistribution <em>Distribution</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event#getMin_Interarrival <em>Min Interarrival</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSporadic_External_Event()
 * @model extendedMetaData="name='Sporadic_External_Event' kind='empty'"
 * @generated
 */
public interface Sporadic_External_Event extends EObject {
	/**
	 * Returns the value of the '<em><b>Avg Interarrival</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Avg Interarrival</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Avg Interarrival</em>' attribute.
	 * @see #setAvg_Interarrival(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSporadic_External_Event_Avg_Interarrival()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Time"
	 *        extendedMetaData="kind='attribute' name='Avg_Interarrival'"
	 * @generated
	 */
	double getAvg_Interarrival();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event#getAvg_Interarrival <em>Avg Interarrival</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Avg Interarrival</em>' attribute.
	 * @see #getAvg_Interarrival()
	 * @generated
	 */
	void setAvg_Interarrival(double value);

	/**
	 * Returns the value of the '<em><b>Distribution</b></em>' attribute.
	 * The literals are from the enumeration {@link es.esi.gemde.vv.mast.mastmodel.Distribution_Type}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Distribution</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Distribution</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Distribution_Type
	 * @see #isSetDistribution()
	 * @see #unsetDistribution()
	 * @see #setDistribution(Distribution_Type)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSporadic_External_Event_Distribution()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='Distribution'"
	 * @generated
	 */
	Distribution_Type getDistribution();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event#getDistribution <em>Distribution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Distribution</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Distribution_Type
	 * @see #isSetDistribution()
	 * @see #unsetDistribution()
	 * @see #getDistribution()
	 * @generated
	 */
	void setDistribution(Distribution_Type value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event#getDistribution <em>Distribution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDistribution()
	 * @see #getDistribution()
	 * @see #setDistribution(Distribution_Type)
	 * @generated
	 */
	void unsetDistribution();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event#getDistribution <em>Distribution</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Distribution</em>' attribute is set.
	 * @see #unsetDistribution()
	 * @see #getDistribution()
	 * @see #setDistribution(Distribution_Type)
	 * @generated
	 */
	boolean isSetDistribution();

	/**
	 * Returns the value of the '<em><b>Min Interarrival</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Interarrival</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Interarrival</em>' attribute.
	 * @see #setMin_Interarrival(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSporadic_External_Event_Min_Interarrival()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Time"
	 *        extendedMetaData="kind='attribute' name='Min_Interarrival'"
	 * @generated
	 */
	double getMin_Interarrival();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event#getMin_Interarrival <em>Min Interarrival</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Interarrival</em>' attribute.
	 * @see #getMin_Interarrival()
	 * @generated
	 */
	void setMin_Interarrival(double value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSporadic_External_Event_Name()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier" required="true"
	 *        extendedMetaData="kind='attribute' name='Name'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Sporadic_External_Event
