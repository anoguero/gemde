/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.util;

import es.esi.gemde.vv.mast.mastmodel.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage
 * @generated
 */
public class ModelAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ModelPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ModelPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModelSwitch<Adapter> modelSwitch =
		new ModelSwitch<Adapter>() {
			@Override
			public Adapter caseActivity(Activity object) {
				return createActivityAdapter();
			}
			@Override
			public Adapter caseAlarm_Clock_System_Timer(Alarm_Clock_System_Timer object) {
				return createAlarm_Clock_System_TimerAdapter();
			}
			@Override
			public Adapter caseBarrier(Barrier object) {
				return createBarrierAdapter();
			}
			@Override
			public Adapter caseBursty_External_Event(Bursty_External_Event object) {
				return createBursty_External_EventAdapter();
			}
			@Override
			public Adapter caseCharacter_Packet_Driver(Character_Packet_Driver object) {
				return createCharacter_Packet_DriverAdapter();
			}
			@Override
			public Adapter caseComposite_Operation(Composite_Operation object) {
				return createComposite_OperationAdapter();
			}
			@Override
			public Adapter caseComposite_Timing_Requirement(Composite_Timing_Requirement object) {
				return createComposite_Timing_RequirementAdapter();
			}
			@Override
			public Adapter caseConcentrator(Concentrator object) {
				return createConcentratorAdapter();
			}
			@Override
			public Adapter caseDelay(Delay object) {
				return createDelayAdapter();
			}
			@Override
			public Adapter caseDelivery_Server(Delivery_Server object) {
				return createDelivery_ServerAdapter();
			}
			@Override
			public Adapter caseDocument_Root(Document_Root object) {
				return createDocument_RootAdapter();
			}
			@Override
			public Adapter caseEDF_Policy(EDF_Policy object) {
				return createEDF_PolicyAdapter();
			}
			@Override
			public Adapter caseEDF_Scheduler(EDF_Scheduler object) {
				return createEDF_SchedulerAdapter();
			}
			@Override
			public Adapter caseEnclosing_Operation(Enclosing_Operation object) {
				return createEnclosing_OperationAdapter();
			}
			@Override
			public Adapter caseFixed_Priority_Policy(Fixed_Priority_Policy object) {
				return createFixed_Priority_PolicyAdapter();
			}
			@Override
			public Adapter caseFixed_Priority_Scheduler(Fixed_Priority_Scheduler object) {
				return createFixed_Priority_SchedulerAdapter();
			}
			@Override
			public Adapter caseFP_Packet_Based_Scheduler(FP_Packet_Based_Scheduler object) {
				return createFP_Packet_Based_SchedulerAdapter();
			}
			@Override
			public Adapter caseGlobal_Max_Miss_Ratio(Global_Max_Miss_Ratio object) {
				return createGlobal_Max_Miss_RatioAdapter();
			}
			@Override
			public Adapter caseHard_Global_Deadline(Hard_Global_Deadline object) {
				return createHard_Global_DeadlineAdapter();
			}
			@Override
			public Adapter caseHard_Local_Deadline(Hard_Local_Deadline object) {
				return createHard_Local_DeadlineAdapter();
			}
			@Override
			public Adapter caseImmediate_Ceiling_Resource(Immediate_Ceiling_Resource object) {
				return createImmediate_Ceiling_ResourceAdapter();
			}
			@Override
			public Adapter caseInterrupt_FP_Policy(Interrupt_FP_Policy object) {
				return createInterrupt_FP_PolicyAdapter();
			}
			@Override
			public Adapter caseList_of_Drivers(List_of_Drivers object) {
				return createList_of_DriversAdapter();
			}
			@Override
			public Adapter caseLocal_Max_Miss_Ratio(Local_Max_Miss_Ratio object) {
				return createLocal_Max_Miss_RatioAdapter();
			}
			@Override
			public Adapter caseMAST_MODEL(MAST_MODEL object) {
				return createMAST_MODELAdapter();
			}
			@Override
			public Adapter caseMax_Output_Jitter_Req(Max_Output_Jitter_Req object) {
				return createMax_Output_Jitter_ReqAdapter();
			}
			@Override
			public Adapter caseMessage_Transmission(Message_Transmission object) {
				return createMessage_TransmissionAdapter();
			}
			@Override
			public Adapter caseMulticast(Multicast object) {
				return createMulticastAdapter();
			}
			@Override
			public Adapter caseNon_Preemptible_FP_Policy(Non_Preemptible_FP_Policy object) {
				return createNon_Preemptible_FP_PolicyAdapter();
			}
			@Override
			public Adapter caseOffset(Offset object) {
				return createOffsetAdapter();
			}
			@Override
			public Adapter caseOverridden_Fixed_Priority(Overridden_Fixed_Priority object) {
				return createOverridden_Fixed_PriorityAdapter();
			}
			@Override
			public Adapter caseOverridden_Permanent_FP(Overridden_Permanent_FP object) {
				return createOverridden_Permanent_FPAdapter();
			}
			@Override
			public Adapter casePacket_Based_Network(Packet_Based_Network object) {
				return createPacket_Based_NetworkAdapter();
			}
			@Override
			public Adapter casePacket_Driver(Packet_Driver object) {
				return createPacket_DriverAdapter();
			}
			@Override
			public Adapter casePeriodic_External_Event(Periodic_External_Event object) {
				return createPeriodic_External_EventAdapter();
			}
			@Override
			public Adapter casePolling_Policy(Polling_Policy object) {
				return createPolling_PolicyAdapter();
			}
			@Override
			public Adapter casePrimary_Scheduler(Primary_Scheduler object) {
				return createPrimary_SchedulerAdapter();
			}
			@Override
			public Adapter casePriority_Inheritance_Resource(Priority_Inheritance_Resource object) {
				return createPriority_Inheritance_ResourceAdapter();
			}
			@Override
			public Adapter caseQuery_Server(Query_Server object) {
				return createQuery_ServerAdapter();
			}
			@Override
			public Adapter caseRate_Divisor(Rate_Divisor object) {
				return createRate_DivisorAdapter();
			}
			@Override
			public Adapter caseRegular_Event(Regular_Event object) {
				return createRegular_EventAdapter();
			}
			@Override
			public Adapter caseRegular_Processor(Regular_Processor object) {
				return createRegular_ProcessorAdapter();
			}
			@Override
			public Adapter caseRegular_Scheduling_Server(Regular_Scheduling_Server object) {
				return createRegular_Scheduling_ServerAdapter();
			}
			@Override
			public Adapter caseRegular_Transaction(Regular_Transaction object) {
				return createRegular_TransactionAdapter();
			}
			@Override
			public Adapter caseRTEP_PacketDriver(RTEP_PacketDriver object) {
				return createRTEP_PacketDriverAdapter();
			}
			@Override
			public Adapter caseSecondary_Scheduler(Secondary_Scheduler object) {
				return createSecondary_SchedulerAdapter();
			}
			@Override
			public Adapter caseSimple_Operation(Simple_Operation object) {
				return createSimple_OperationAdapter();
			}
			@Override
			public Adapter caseSingular_External_Event(Singular_External_Event object) {
				return createSingular_External_EventAdapter();
			}
			@Override
			public Adapter caseSoft_Global_Deadline(Soft_Global_Deadline object) {
				return createSoft_Global_DeadlineAdapter();
			}
			@Override
			public Adapter caseSoft_Local_Deadline(Soft_Local_Deadline object) {
				return createSoft_Local_DeadlineAdapter();
			}
			@Override
			public Adapter caseSporadic_External_Event(Sporadic_External_Event object) {
				return createSporadic_External_EventAdapter();
			}
			@Override
			public Adapter caseSporadic_Server_Policy(Sporadic_Server_Policy object) {
				return createSporadic_Server_PolicyAdapter();
			}
			@Override
			public Adapter caseSRP_Parameters(SRP_Parameters object) {
				return createSRP_ParametersAdapter();
			}
			@Override
			public Adapter caseSRP_Resource(SRP_Resource object) {
				return createSRP_ResourceAdapter();
			}
			@Override
			public Adapter caseSystem_Timed_Activity(System_Timed_Activity object) {
				return createSystem_Timed_ActivityAdapter();
			}
			@Override
			public Adapter caseTicker_System_Timer(Ticker_System_Timer object) {
				return createTicker_System_TimerAdapter();
			}
			@Override
			public Adapter caseUnbounded_External_Event(Unbounded_External_Event object) {
				return createUnbounded_External_EventAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Activity <em>Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Activity
	 * @generated
	 */
	public Adapter createActivityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Alarm_Clock_System_Timer <em>Alarm Clock System Timer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Alarm_Clock_System_Timer
	 * @generated
	 */
	public Adapter createAlarm_Clock_System_TimerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Barrier <em>Barrier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Barrier
	 * @generated
	 */
	public Adapter createBarrierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event <em>Bursty External Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event
	 * @generated
	 */
	public Adapter createBursty_External_EventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver <em>Character Packet Driver</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver
	 * @generated
	 */
	public Adapter createCharacter_Packet_DriverAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Composite_Operation <em>Composite Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Composite_Operation
	 * @generated
	 */
	public Adapter createComposite_OperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement <em>Composite Timing Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement
	 * @generated
	 */
	public Adapter createComposite_Timing_RequirementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Concentrator <em>Concentrator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Concentrator
	 * @generated
	 */
	public Adapter createConcentratorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Delay <em>Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Delay
	 * @generated
	 */
	public Adapter createDelayAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Delivery_Server <em>Delivery Server</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Delivery_Server
	 * @generated
	 */
	public Adapter createDelivery_ServerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Document_Root <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Document_Root
	 * @generated
	 */
	public Adapter createDocument_RootAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.EDF_Policy <em>EDF Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.EDF_Policy
	 * @generated
	 */
	public Adapter createEDF_PolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.EDF_Scheduler <em>EDF Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.EDF_Scheduler
	 * @generated
	 */
	public Adapter createEDF_SchedulerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation <em>Enclosing Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation
	 * @generated
	 */
	public Adapter createEnclosing_OperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Policy <em>Fixed Priority Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Policy
	 * @generated
	 */
	public Adapter createFixed_Priority_PolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler <em>Fixed Priority Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler
	 * @generated
	 */
	public Adapter createFixed_Priority_SchedulerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler <em>FP Packet Based Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler
	 * @generated
	 */
	public Adapter createFP_Packet_Based_SchedulerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio <em>Global Max Miss Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio
	 * @generated
	 */
	public Adapter createGlobal_Max_Miss_RatioAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Hard_Global_Deadline <em>Hard Global Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Hard_Global_Deadline
	 * @generated
	 */
	public Adapter createHard_Global_DeadlineAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Hard_Local_Deadline <em>Hard Local Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Hard_Local_Deadline
	 * @generated
	 */
	public Adapter createHard_Local_DeadlineAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource <em>Immediate Ceiling Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource
	 * @generated
	 */
	public Adapter createImmediate_Ceiling_ResourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Interrupt_FP_Policy <em>Interrupt FP Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Interrupt_FP_Policy
	 * @generated
	 */
	public Adapter createInterrupt_FP_PolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.List_of_Drivers <em>List of Drivers</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.List_of_Drivers
	 * @generated
	 */
	public Adapter createList_of_DriversAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Local_Max_Miss_Ratio <em>Local Max Miss Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Local_Max_Miss_Ratio
	 * @generated
	 */
	public Adapter createLocal_Max_Miss_RatioAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL <em>MAST MODEL</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.MAST_MODEL
	 * @generated
	 */
	public Adapter createMAST_MODELAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Max_Output_Jitter_Req <em>Max Output Jitter Req</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Max_Output_Jitter_Req
	 * @generated
	 */
	public Adapter createMax_Output_Jitter_ReqAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission <em>Message Transmission</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Message_Transmission
	 * @generated
	 */
	public Adapter createMessage_TransmissionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Multicast <em>Multicast</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Multicast
	 * @generated
	 */
	public Adapter createMulticastAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Non_Preemptible_FP_Policy <em>Non Preemptible FP Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Non_Preemptible_FP_Policy
	 * @generated
	 */
	public Adapter createNon_Preemptible_FP_PolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Offset <em>Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Offset
	 * @generated
	 */
	public Adapter createOffsetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Overridden_Fixed_Priority <em>Overridden Fixed Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Overridden_Fixed_Priority
	 * @generated
	 */
	public Adapter createOverridden_Fixed_PriorityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Overridden_Permanent_FP <em>Overridden Permanent FP</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Overridden_Permanent_FP
	 * @generated
	 */
	public Adapter createOverridden_Permanent_FPAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network <em>Packet Based Network</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network
	 * @generated
	 */
	public Adapter createPacket_Based_NetworkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver <em>Packet Driver</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Packet_Driver
	 * @generated
	 */
	public Adapter createPacket_DriverAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event <em>Periodic External Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event
	 * @generated
	 */
	public Adapter createPeriodic_External_EventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy <em>Polling Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Polling_Policy
	 * @generated
	 */
	public Adapter createPolling_PolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Primary_Scheduler <em>Primary Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Primary_Scheduler
	 * @generated
	 */
	public Adapter createPrimary_SchedulerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Resource <em>Priority Inheritance Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Resource
	 * @generated
	 */
	public Adapter createPriority_Inheritance_ResourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Query_Server <em>Query Server</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Query_Server
	 * @generated
	 */
	public Adapter createQuery_ServerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Rate_Divisor <em>Rate Divisor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Rate_Divisor
	 * @generated
	 */
	public Adapter createRate_DivisorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event <em>Regular Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Event
	 * @generated
	 */
	public Adapter createRegular_EventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor <em>Regular Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Processor
	 * @generated
	 */
	public Adapter createRegular_ProcessorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server <em>Regular Scheduling Server</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server
	 * @generated
	 */
	public Adapter createRegular_Scheduling_ServerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction <em>Regular Transaction</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Transaction
	 * @generated
	 */
	public Adapter createRegular_TransactionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver <em>RTEP Packet Driver</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver
	 * @generated
	 */
	public Adapter createRTEP_PacketDriverAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler <em>Secondary Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler
	 * @generated
	 */
	public Adapter createSecondary_SchedulerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation <em>Simple Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Simple_Operation
	 * @generated
	 */
	public Adapter createSimple_OperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Singular_External_Event <em>Singular External Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Singular_External_Event
	 * @generated
	 */
	public Adapter createSingular_External_EventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Soft_Global_Deadline <em>Soft Global Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Soft_Global_Deadline
	 * @generated
	 */
	public Adapter createSoft_Global_DeadlineAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Soft_Local_Deadline <em>Soft Local Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Soft_Local_Deadline
	 * @generated
	 */
	public Adapter createSoft_Local_DeadlineAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event <em>Sporadic External Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event
	 * @generated
	 */
	public Adapter createSporadic_External_EventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy <em>Sporadic Server Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy
	 * @generated
	 */
	public Adapter createSporadic_Server_PolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.SRP_Parameters <em>SRP Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.SRP_Parameters
	 * @generated
	 */
	public Adapter createSRP_ParametersAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.SRP_Resource <em>SRP Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.SRP_Resource
	 * @generated
	 */
	public Adapter createSRP_ResourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity <em>System Timed Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity
	 * @generated
	 */
	public Adapter createSystem_Timed_ActivityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer <em>Ticker System Timer</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer
	 * @generated
	 */
	public Adapter createTicker_System_TimerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastmodel.Unbounded_External_Event <em>Unbounded External Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastmodel.Unbounded_External_Event
	 * @generated
	 */
	public Adapter createUnbounded_External_EventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ModelAdapterFactory
