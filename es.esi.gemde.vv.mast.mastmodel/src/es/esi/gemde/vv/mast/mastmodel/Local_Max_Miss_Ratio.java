/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Local Max Miss Ratio</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Local_Max_Miss_Ratio#getDeadline <em>Deadline</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Local_Max_Miss_Ratio#getRatio <em>Ratio</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getLocal_Max_Miss_Ratio()
 * @model extendedMetaData="name='Local_Max_Miss_Ratio' kind='empty'"
 * @generated
 */
public interface Local_Max_Miss_Ratio extends EObject {
	/**
	 * Returns the value of the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deadline</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deadline</em>' attribute.
	 * @see #setDeadline(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getLocal_Max_Miss_Ratio_Deadline()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Time"
	 *        extendedMetaData="kind='attribute' name='Deadline'"
	 * @generated
	 */
	double getDeadline();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Local_Max_Miss_Ratio#getDeadline <em>Deadline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deadline</em>' attribute.
	 * @see #getDeadline()
	 * @generated
	 */
	void setDeadline(double value);

	/**
	 * Returns the value of the '<em><b>Ratio</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ratio</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ratio</em>' attribute.
	 * @see #setRatio(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getLocal_Max_Miss_Ratio_Ratio()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Percentage"
	 *        extendedMetaData="kind='attribute' name='Ratio'"
	 * @generated
	 */
	double getRatio();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Local_Max_Miss_Ratio#getRatio <em>Ratio</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ratio</em>' attribute.
	 * @see #getRatio()
	 * @generated
	 */
	void setRatio(double value);

} // Local_Max_Miss_Ratio
