/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Query Server</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Query_Server#getInput_Event <em>Input Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Query_Server#getOutput_Events_List <em>Output Events List</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Query_Server#getRequest_Policy <em>Request Policy</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getQuery_Server()
 * @model extendedMetaData="name='Query_Server' kind='empty'"
 * @generated
 */
public interface Query_Server extends EObject {
	/**
	 * Returns the value of the '<em><b>Input Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Event</em>' attribute.
	 * @see #setInput_Event(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getQuery_Server_Input_Event()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref" required="true"
	 *        extendedMetaData="kind='attribute' name='Input_Event'"
	 * @generated
	 */
	String getInput_Event();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Query_Server#getInput_Event <em>Input Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Event</em>' attribute.
	 * @see #getInput_Event()
	 * @generated
	 */
	void setInput_Event(String value);

	/**
	 * Returns the value of the '<em><b>Output Events List</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Events List</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Events List</em>' attribute.
	 * @see #setOutput_Events_List(List)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getQuery_Server_Output_Events_List()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref_List" required="true" many="false"
	 *        extendedMetaData="kind='attribute' name='Output_Events_List'"
	 * @generated
	 */
	List<String> getOutput_Events_List();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Query_Server#getOutput_Events_List <em>Output Events List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Events List</em>' attribute.
	 * @see #getOutput_Events_List()
	 * @generated
	 */
	void setOutput_Events_List(List<String> value);

	/**
	 * Returns the value of the '<em><b>Request Policy</b></em>' attribute.
	 * The literals are from the enumeration {@link es.esi.gemde.vv.mast.mastmodel.Request_Policy}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Request Policy</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Request Policy</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Request_Policy
	 * @see #isSetRequest_Policy()
	 * @see #unsetRequest_Policy()
	 * @see #setRequest_Policy(Request_Policy)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getQuery_Server_Request_Policy()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='Request_Policy'"
	 * @generated
	 */
	Request_Policy getRequest_Policy();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Query_Server#getRequest_Policy <em>Request Policy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Request Policy</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Request_Policy
	 * @see #isSetRequest_Policy()
	 * @see #unsetRequest_Policy()
	 * @see #getRequest_Policy()
	 * @generated
	 */
	void setRequest_Policy(Request_Policy value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Query_Server#getRequest_Policy <em>Request Policy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetRequest_Policy()
	 * @see #getRequest_Policy()
	 * @see #setRequest_Policy(Request_Policy)
	 * @generated
	 */
	void unsetRequest_Policy();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Query_Server#getRequest_Policy <em>Request Policy</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Request Policy</em>' attribute is set.
	 * @see #unsetRequest_Policy()
	 * @see #getRequest_Policy()
	 * @see #setRequest_Policy(Request_Policy)
	 * @generated
	 */
	boolean isSetRequest_Policy();

} // Query_Server
