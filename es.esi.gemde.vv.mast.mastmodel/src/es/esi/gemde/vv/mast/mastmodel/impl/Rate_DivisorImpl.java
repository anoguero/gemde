/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Rate_Divisor;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Rate Divisor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Rate_DivisorImpl#getInput_Event <em>Input Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Rate_DivisorImpl#getOutput_Event <em>Output Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Rate_DivisorImpl#getRate_Factor <em>Rate Factor</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Rate_DivisorImpl extends EObjectImpl implements Rate_Divisor {
	/**
	 * The default value of the '{@link #getInput_Event() <em>Input Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInput_Event()
	 * @generated
	 * @ordered
	 */
	protected static final String INPUT_EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInput_Event() <em>Input Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInput_Event()
	 * @generated
	 * @ordered
	 */
	protected String input_Event = INPUT_EVENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getOutput_Event() <em>Output Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutput_Event()
	 * @generated
	 * @ordered
	 */
	protected static final String OUTPUT_EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOutput_Event() <em>Output Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutput_Event()
	 * @generated
	 * @ordered
	 */
	protected String output_Event = OUTPUT_EVENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getRate_Factor() <em>Rate Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRate_Factor()
	 * @generated
	 * @ordered
	 */
	protected static final int RATE_FACTOR_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getRate_Factor() <em>Rate Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRate_Factor()
	 * @generated
	 * @ordered
	 */
	protected int rate_Factor = RATE_FACTOR_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Rate_DivisorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.RATE_DIVISOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInput_Event() {
		return input_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInput_Event(String newInput_Event) {
		String oldInput_Event = input_Event;
		input_Event = newInput_Event;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RATE_DIVISOR__INPUT_EVENT, oldInput_Event, input_Event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOutput_Event() {
		return output_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutput_Event(String newOutput_Event) {
		String oldOutput_Event = output_Event;
		output_Event = newOutput_Event;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RATE_DIVISOR__OUTPUT_EVENT, oldOutput_Event, output_Event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getRate_Factor() {
		return rate_Factor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRate_Factor(int newRate_Factor) {
		int oldRate_Factor = rate_Factor;
		rate_Factor = newRate_Factor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RATE_DIVISOR__RATE_FACTOR, oldRate_Factor, rate_Factor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.RATE_DIVISOR__INPUT_EVENT:
				return getInput_Event();
			case ModelPackage.RATE_DIVISOR__OUTPUT_EVENT:
				return getOutput_Event();
			case ModelPackage.RATE_DIVISOR__RATE_FACTOR:
				return getRate_Factor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.RATE_DIVISOR__INPUT_EVENT:
				setInput_Event((String)newValue);
				return;
			case ModelPackage.RATE_DIVISOR__OUTPUT_EVENT:
				setOutput_Event((String)newValue);
				return;
			case ModelPackage.RATE_DIVISOR__RATE_FACTOR:
				setRate_Factor((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.RATE_DIVISOR__INPUT_EVENT:
				setInput_Event(INPUT_EVENT_EDEFAULT);
				return;
			case ModelPackage.RATE_DIVISOR__OUTPUT_EVENT:
				setOutput_Event(OUTPUT_EVENT_EDEFAULT);
				return;
			case ModelPackage.RATE_DIVISOR__RATE_FACTOR:
				setRate_Factor(RATE_FACTOR_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.RATE_DIVISOR__INPUT_EVENT:
				return INPUT_EVENT_EDEFAULT == null ? input_Event != null : !INPUT_EVENT_EDEFAULT.equals(input_Event);
			case ModelPackage.RATE_DIVISOR__OUTPUT_EVENT:
				return OUTPUT_EVENT_EDEFAULT == null ? output_Event != null : !OUTPUT_EVENT_EDEFAULT.equals(output_Event);
			case ModelPackage.RATE_DIVISOR__RATE_FACTOR:
				return rate_Factor != RATE_FACTOR_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Input_Event: ");
		result.append(input_Event);
		result.append(", Output_Event: ");
		result.append(output_Event);
		result.append(", Rate_Factor: ");
		result.append(rate_Factor);
		result.append(')');
		return result.toString();
	}

} //Rate_DivisorImpl
