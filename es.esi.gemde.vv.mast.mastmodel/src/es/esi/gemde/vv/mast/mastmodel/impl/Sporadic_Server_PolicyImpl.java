/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Assertion;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sporadic Server Policy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Sporadic_Server_PolicyImpl#getBackground_Priority <em>Background Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Sporadic_Server_PolicyImpl#getInitial_Capacity <em>Initial Capacity</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Sporadic_Server_PolicyImpl#getMax_Pending_Replenishments <em>Max Pending Replenishments</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Sporadic_Server_PolicyImpl#getNormal_Priority <em>Normal Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Sporadic_Server_PolicyImpl#getPreassigned <em>Preassigned</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Sporadic_Server_PolicyImpl#getReplenishment_Period <em>Replenishment Period</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Sporadic_Server_PolicyImpl extends EObjectImpl implements Sporadic_Server_Policy {
	/**
	 * The default value of the '{@link #getBackground_Priority() <em>Background Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBackground_Priority()
	 * @generated
	 * @ordered
	 */
	protected static final int BACKGROUND_PRIORITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getBackground_Priority() <em>Background Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBackground_Priority()
	 * @generated
	 * @ordered
	 */
	protected int background_Priority = BACKGROUND_PRIORITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getInitial_Capacity() <em>Initial Capacity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitial_Capacity()
	 * @generated
	 * @ordered
	 */
	protected static final double INITIAL_CAPACITY_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getInitial_Capacity() <em>Initial Capacity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitial_Capacity()
	 * @generated
	 * @ordered
	 */
	protected double initial_Capacity = INITIAL_CAPACITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getMax_Pending_Replenishments() <em>Max Pending Replenishments</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_Pending_Replenishments()
	 * @generated
	 * @ordered
	 */
	protected static final int MAX_PENDING_REPLENISHMENTS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMax_Pending_Replenishments() <em>Max Pending Replenishments</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_Pending_Replenishments()
	 * @generated
	 * @ordered
	 */
	protected int max_Pending_Replenishments = MAX_PENDING_REPLENISHMENTS_EDEFAULT;

	/**
	 * The default value of the '{@link #getNormal_Priority() <em>Normal Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNormal_Priority()
	 * @generated
	 * @ordered
	 */
	protected static final int NORMAL_PRIORITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNormal_Priority() <em>Normal Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNormal_Priority()
	 * @generated
	 * @ordered
	 */
	protected int normal_Priority = NORMAL_PRIORITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getPreassigned() <em>Preassigned</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreassigned()
	 * @generated
	 * @ordered
	 */
	protected static final Assertion PREASSIGNED_EDEFAULT = Assertion.YES;

	/**
	 * The cached value of the '{@link #getPreassigned() <em>Preassigned</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreassigned()
	 * @generated
	 * @ordered
	 */
	protected Assertion preassigned = PREASSIGNED_EDEFAULT;

	/**
	 * This is true if the Preassigned attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean preassignedESet;

	/**
	 * The default value of the '{@link #getReplenishment_Period() <em>Replenishment Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReplenishment_Period()
	 * @generated
	 * @ordered
	 */
	protected static final double REPLENISHMENT_PERIOD_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getReplenishment_Period() <em>Replenishment Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReplenishment_Period()
	 * @generated
	 * @ordered
	 */
	protected double replenishment_Period = REPLENISHMENT_PERIOD_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Sporadic_Server_PolicyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.SPORADIC_SERVER_POLICY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBackground_Priority() {
		return background_Priority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBackground_Priority(int newBackground_Priority) {
		int oldBackground_Priority = background_Priority;
		background_Priority = newBackground_Priority;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SPORADIC_SERVER_POLICY__BACKGROUND_PRIORITY, oldBackground_Priority, background_Priority));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getInitial_Capacity() {
		return initial_Capacity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitial_Capacity(double newInitial_Capacity) {
		double oldInitial_Capacity = initial_Capacity;
		initial_Capacity = newInitial_Capacity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SPORADIC_SERVER_POLICY__INITIAL_CAPACITY, oldInitial_Capacity, initial_Capacity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMax_Pending_Replenishments() {
		return max_Pending_Replenishments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMax_Pending_Replenishments(int newMax_Pending_Replenishments) {
		int oldMax_Pending_Replenishments = max_Pending_Replenishments;
		max_Pending_Replenishments = newMax_Pending_Replenishments;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SPORADIC_SERVER_POLICY__MAX_PENDING_REPLENISHMENTS, oldMax_Pending_Replenishments, max_Pending_Replenishments));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNormal_Priority() {
		return normal_Priority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNormal_Priority(int newNormal_Priority) {
		int oldNormal_Priority = normal_Priority;
		normal_Priority = newNormal_Priority;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SPORADIC_SERVER_POLICY__NORMAL_PRIORITY, oldNormal_Priority, normal_Priority));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Assertion getPreassigned() {
		return preassigned;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPreassigned(Assertion newPreassigned) {
		Assertion oldPreassigned = preassigned;
		preassigned = newPreassigned == null ? PREASSIGNED_EDEFAULT : newPreassigned;
		boolean oldPreassignedESet = preassignedESet;
		preassignedESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SPORADIC_SERVER_POLICY__PREASSIGNED, oldPreassigned, preassigned, !oldPreassignedESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetPreassigned() {
		Assertion oldPreassigned = preassigned;
		boolean oldPreassignedESet = preassignedESet;
		preassigned = PREASSIGNED_EDEFAULT;
		preassignedESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ModelPackage.SPORADIC_SERVER_POLICY__PREASSIGNED, oldPreassigned, PREASSIGNED_EDEFAULT, oldPreassignedESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetPreassigned() {
		return preassignedESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getReplenishment_Period() {
		return replenishment_Period;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReplenishment_Period(double newReplenishment_Period) {
		double oldReplenishment_Period = replenishment_Period;
		replenishment_Period = newReplenishment_Period;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SPORADIC_SERVER_POLICY__REPLENISHMENT_PERIOD, oldReplenishment_Period, replenishment_Period));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.SPORADIC_SERVER_POLICY__BACKGROUND_PRIORITY:
				return getBackground_Priority();
			case ModelPackage.SPORADIC_SERVER_POLICY__INITIAL_CAPACITY:
				return getInitial_Capacity();
			case ModelPackage.SPORADIC_SERVER_POLICY__MAX_PENDING_REPLENISHMENTS:
				return getMax_Pending_Replenishments();
			case ModelPackage.SPORADIC_SERVER_POLICY__NORMAL_PRIORITY:
				return getNormal_Priority();
			case ModelPackage.SPORADIC_SERVER_POLICY__PREASSIGNED:
				return getPreassigned();
			case ModelPackage.SPORADIC_SERVER_POLICY__REPLENISHMENT_PERIOD:
				return getReplenishment_Period();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.SPORADIC_SERVER_POLICY__BACKGROUND_PRIORITY:
				setBackground_Priority((Integer)newValue);
				return;
			case ModelPackage.SPORADIC_SERVER_POLICY__INITIAL_CAPACITY:
				setInitial_Capacity((Double)newValue);
				return;
			case ModelPackage.SPORADIC_SERVER_POLICY__MAX_PENDING_REPLENISHMENTS:
				setMax_Pending_Replenishments((Integer)newValue);
				return;
			case ModelPackage.SPORADIC_SERVER_POLICY__NORMAL_PRIORITY:
				setNormal_Priority((Integer)newValue);
				return;
			case ModelPackage.SPORADIC_SERVER_POLICY__PREASSIGNED:
				setPreassigned((Assertion)newValue);
				return;
			case ModelPackage.SPORADIC_SERVER_POLICY__REPLENISHMENT_PERIOD:
				setReplenishment_Period((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.SPORADIC_SERVER_POLICY__BACKGROUND_PRIORITY:
				setBackground_Priority(BACKGROUND_PRIORITY_EDEFAULT);
				return;
			case ModelPackage.SPORADIC_SERVER_POLICY__INITIAL_CAPACITY:
				setInitial_Capacity(INITIAL_CAPACITY_EDEFAULT);
				return;
			case ModelPackage.SPORADIC_SERVER_POLICY__MAX_PENDING_REPLENISHMENTS:
				setMax_Pending_Replenishments(MAX_PENDING_REPLENISHMENTS_EDEFAULT);
				return;
			case ModelPackage.SPORADIC_SERVER_POLICY__NORMAL_PRIORITY:
				setNormal_Priority(NORMAL_PRIORITY_EDEFAULT);
				return;
			case ModelPackage.SPORADIC_SERVER_POLICY__PREASSIGNED:
				unsetPreassigned();
				return;
			case ModelPackage.SPORADIC_SERVER_POLICY__REPLENISHMENT_PERIOD:
				setReplenishment_Period(REPLENISHMENT_PERIOD_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.SPORADIC_SERVER_POLICY__BACKGROUND_PRIORITY:
				return background_Priority != BACKGROUND_PRIORITY_EDEFAULT;
			case ModelPackage.SPORADIC_SERVER_POLICY__INITIAL_CAPACITY:
				return initial_Capacity != INITIAL_CAPACITY_EDEFAULT;
			case ModelPackage.SPORADIC_SERVER_POLICY__MAX_PENDING_REPLENISHMENTS:
				return max_Pending_Replenishments != MAX_PENDING_REPLENISHMENTS_EDEFAULT;
			case ModelPackage.SPORADIC_SERVER_POLICY__NORMAL_PRIORITY:
				return normal_Priority != NORMAL_PRIORITY_EDEFAULT;
			case ModelPackage.SPORADIC_SERVER_POLICY__PREASSIGNED:
				return isSetPreassigned();
			case ModelPackage.SPORADIC_SERVER_POLICY__REPLENISHMENT_PERIOD:
				return replenishment_Period != REPLENISHMENT_PERIOD_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Background_Priority: ");
		result.append(background_Priority);
		result.append(", Initial_Capacity: ");
		result.append(initial_Capacity);
		result.append(", Max_Pending_Replenishments: ");
		result.append(max_Pending_Replenishments);
		result.append(", Normal_Priority: ");
		result.append(normal_Priority);
		result.append(", Preassigned: ");
		if (preassignedESet) result.append(preassigned); else result.append("<unset>");
		result.append(", Replenishment_Period: ");
		result.append(replenishment_Period);
		result.append(')');
		return result.toString();
	}

} //Sporadic_Server_PolicyImpl
