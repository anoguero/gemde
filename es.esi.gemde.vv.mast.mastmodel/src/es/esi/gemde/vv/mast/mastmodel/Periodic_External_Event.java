/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Periodic External Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event#getMax_Jitter <em>Max Jitter</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event#getName <em>Name</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event#getPeriod <em>Period</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event#getPhase <em>Phase</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPeriodic_External_Event()
 * @model extendedMetaData="name='Periodic_External_Event' kind='empty'"
 * @generated
 */
public interface Periodic_External_Event extends EObject {
	/**
	 * Returns the value of the '<em><b>Max Jitter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Jitter</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Jitter</em>' attribute.
	 * @see #setMax_Jitter(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPeriodic_External_Event_Max_Jitter()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Time"
	 *        extendedMetaData="kind='attribute' name='Max_Jitter'"
	 * @generated
	 */
	double getMax_Jitter();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event#getMax_Jitter <em>Max Jitter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Jitter</em>' attribute.
	 * @see #getMax_Jitter()
	 * @generated
	 */
	void setMax_Jitter(double value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPeriodic_External_Event_Name()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier" required="true"
	 *        extendedMetaData="kind='attribute' name='Name'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Period</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Period</em>' attribute.
	 * @see #setPeriod(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPeriodic_External_Event_Period()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Time"
	 *        extendedMetaData="kind='attribute' name='Period'"
	 * @generated
	 */
	double getPeriod();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event#getPeriod <em>Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Period</em>' attribute.
	 * @see #getPeriod()
	 * @generated
	 */
	void setPeriod(double value);

	/**
	 * Returns the value of the '<em><b>Phase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Phase</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Phase</em>' attribute.
	 * @see #setPhase(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPeriodic_External_Event_Phase()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Absolute_Time"
	 *        extendedMetaData="kind='attribute' name='Phase'"
	 * @generated
	 */
	double getPhase();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event#getPhase <em>Phase</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Phase</em>' attribute.
	 * @see #getPhase()
	 * @generated
	 */
	void setPhase(double value);

} // Periodic_External_Event
