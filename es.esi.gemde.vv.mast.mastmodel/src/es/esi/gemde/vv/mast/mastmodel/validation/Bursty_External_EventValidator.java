/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.validation;

import es.esi.gemde.vv.mast.mastmodel.Distribution_Type;

import java.math.BigInteger;

/**
 * A sample validator interface for {@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface Bursty_External_EventValidator {
	boolean validate();

	boolean validateAvg_Interarrival(double value);
	boolean validateBound_Interval(double value);
	boolean validateDistribution(Distribution_Type value);
	boolean validateMax_Arrivals(BigInteger value);
	boolean validateName(String value);
}
