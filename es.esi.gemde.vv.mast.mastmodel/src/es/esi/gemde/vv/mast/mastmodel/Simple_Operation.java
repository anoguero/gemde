/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getOverridden_Fixed_Priority <em>Overridden Fixed Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getOverridden_Permanent_FP <em>Overridden Permanent FP</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getShared_Resources_List <em>Shared Resources List</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getShared_Resources_To_Lock <em>Shared Resources To Lock</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getShared_Resources_To_Unlock <em>Shared Resources To Unlock</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getAverage_Case_Execution_Time <em>Average Case Execution Time</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getBest_Case_Execution_Time <em>Best Case Execution Time</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getName <em>Name</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getWorst_Case_Execution_Time <em>Worst Case Execution Time</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSimple_Operation()
 * @model extendedMetaData="name='Simple_Operation' kind='elementOnly'"
 * @generated
 */
public interface Simple_Operation extends EObject {
	/**
	 * Returns the value of the '<em><b>Overridden Fixed Priority</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overridden Fixed Priority</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overridden Fixed Priority</em>' containment reference.
	 * @see #setOverridden_Fixed_Priority(Overridden_Fixed_Priority)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSimple_Operation_Overridden_Fixed_Priority()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Overridden_Fixed_Priority' namespace='##targetNamespace'"
	 * @generated
	 */
	Overridden_Fixed_Priority getOverridden_Fixed_Priority();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getOverridden_Fixed_Priority <em>Overridden Fixed Priority</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Overridden Fixed Priority</em>' containment reference.
	 * @see #getOverridden_Fixed_Priority()
	 * @generated
	 */
	void setOverridden_Fixed_Priority(Overridden_Fixed_Priority value);

	/**
	 * Returns the value of the '<em><b>Overridden Permanent FP</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overridden Permanent FP</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overridden Permanent FP</em>' containment reference.
	 * @see #setOverridden_Permanent_FP(Overridden_Permanent_FP)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSimple_Operation_Overridden_Permanent_FP()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Overridden_Permanent_FP' namespace='##targetNamespace'"
	 * @generated
	 */
	Overridden_Permanent_FP getOverridden_Permanent_FP();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getOverridden_Permanent_FP <em>Overridden Permanent FP</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Overridden Permanent FP</em>' containment reference.
	 * @see #getOverridden_Permanent_FP()
	 * @generated
	 */
	void setOverridden_Permanent_FP(Overridden_Permanent_FP value);

	/**
	 * Returns the value of the '<em><b>Shared Resources List</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared Resources List</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared Resources List</em>' attribute.
	 * @see #setShared_Resources_List(List)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSimple_Operation_Shared_Resources_List()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref_List" many="false"
	 *        extendedMetaData="kind='element' name='Shared_Resources_List' namespace='##targetNamespace'"
	 * @generated
	 */
	List<String> getShared_Resources_List();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getShared_Resources_List <em>Shared Resources List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Shared Resources List</em>' attribute.
	 * @see #getShared_Resources_List()
	 * @generated
	 */
	void setShared_Resources_List(List<String> value);

	/**
	 * Returns the value of the '<em><b>Shared Resources To Lock</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared Resources To Lock</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared Resources To Lock</em>' attribute.
	 * @see #setShared_Resources_To_Lock(List)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSimple_Operation_Shared_Resources_To_Lock()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref_List" many="false"
	 *        extendedMetaData="kind='element' name='Shared_Resources_To_Lock' namespace='##targetNamespace'"
	 * @generated
	 */
	List<String> getShared_Resources_To_Lock();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getShared_Resources_To_Lock <em>Shared Resources To Lock</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Shared Resources To Lock</em>' attribute.
	 * @see #getShared_Resources_To_Lock()
	 * @generated
	 */
	void setShared_Resources_To_Lock(List<String> value);

	/**
	 * Returns the value of the '<em><b>Shared Resources To Unlock</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared Resources To Unlock</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared Resources To Unlock</em>' attribute.
	 * @see #setShared_Resources_To_Unlock(List)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSimple_Operation_Shared_Resources_To_Unlock()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref_List" many="false"
	 *        extendedMetaData="kind='element' name='Shared_Resources_To_Unlock' namespace='##targetNamespace'"
	 * @generated
	 */
	List<String> getShared_Resources_To_Unlock();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getShared_Resources_To_Unlock <em>Shared Resources To Unlock</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Shared Resources To Unlock</em>' attribute.
	 * @see #getShared_Resources_To_Unlock()
	 * @generated
	 */
	void setShared_Resources_To_Unlock(List<String> value);

	/**
	 * Returns the value of the '<em><b>Average Case Execution Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Average Case Execution Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Average Case Execution Time</em>' attribute.
	 * @see #setAverage_Case_Execution_Time(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSimple_Operation_Average_Case_Execution_Time()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Average_Case_Execution_Time'"
	 * @generated
	 */
	double getAverage_Case_Execution_Time();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getAverage_Case_Execution_Time <em>Average Case Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Average Case Execution Time</em>' attribute.
	 * @see #getAverage_Case_Execution_Time()
	 * @generated
	 */
	void setAverage_Case_Execution_Time(double value);

	/**
	 * Returns the value of the '<em><b>Best Case Execution Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Best Case Execution Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Best Case Execution Time</em>' attribute.
	 * @see #setBest_Case_Execution_Time(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSimple_Operation_Best_Case_Execution_Time()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Best_Case_Execution_Time'"
	 * @generated
	 */
	double getBest_Case_Execution_Time();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getBest_Case_Execution_Time <em>Best Case Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Best Case Execution Time</em>' attribute.
	 * @see #getBest_Case_Execution_Time()
	 * @generated
	 */
	void setBest_Case_Execution_Time(double value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSimple_Operation_Name()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier" required="true"
	 *        extendedMetaData="kind='attribute' name='Name'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Worst Case Execution Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Worst Case Execution Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Worst Case Execution Time</em>' attribute.
	 * @see #setWorst_Case_Execution_Time(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getSimple_Operation_Worst_Case_Execution_Time()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Worst_Case_Execution_Time'"
	 * @generated
	 */
	double getWorst_Case_Execution_Time();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getWorst_Case_Execution_Time <em>Worst Case Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Worst Case Execution Time</em>' attribute.
	 * @see #getWorst_Case_Execution_Time()
	 * @generated
	 */
	void setWorst_Case_Execution_Time(double value);

} // Simple_Operation
