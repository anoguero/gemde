/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>RTEP Packet Driver</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getFailure_Timeout <em>Failure Timeout</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getMessage_Partitioning <em>Message Partitioning</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getNumber_Of_Stations <em>Number Of Stations</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Discard_Operation <em>Packet Discard Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Interrupt_Server <em>Packet Interrupt Server</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_ISR_Operation <em>Packet ISR Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Receive_Operation <em>Packet Receive Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Retransmission_Operation <em>Packet Retransmission Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Send_Operation <em>Packet Send Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Server <em>Packet Server</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Transmission_Retries <em>Packet Transmission Retries</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getRTA_Overhead_Model <em>RTA Overhead Model</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getToken_Check_Operation <em>Token Check Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getToken_Delay <em>Token Delay</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getToken_Manage_Operation <em>Token Manage Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getToken_Retransmission_Operation <em>Token Retransmission Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getToken_Transmission_Retries <em>Token Transmission Retries</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRTEP_PacketDriver()
 * @model extendedMetaData="name='RTEP_Packet_Driver' kind='empty'"
 * @generated
 */
public interface RTEP_PacketDriver extends EObject {
	/**
	 * Returns the value of the '<em><b>Failure Timeout</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Failure Timeout</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Failure Timeout</em>' attribute.
	 * @see #setFailure_Timeout(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRTEP_PacketDriver_Failure_Timeout()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Time"
	 *        extendedMetaData="kind='attribute' name='Failure_Timeout'"
	 * @generated
	 */
	double getFailure_Timeout();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getFailure_Timeout <em>Failure Timeout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Failure Timeout</em>' attribute.
	 * @see #getFailure_Timeout()
	 * @generated
	 */
	void setFailure_Timeout(double value);

	/**
	 * Returns the value of the '<em><b>Message Partitioning</b></em>' attribute.
	 * The literals are from the enumeration {@link es.esi.gemde.vv.mast.mastmodel.Assertion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Partitioning</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Partitioning</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Assertion
	 * @see #isSetMessage_Partitioning()
	 * @see #unsetMessage_Partitioning()
	 * @see #setMessage_Partitioning(Assertion)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRTEP_PacketDriver_Message_Partitioning()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='Message_Partitioning'"
	 * @generated
	 */
	Assertion getMessage_Partitioning();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getMessage_Partitioning <em>Message Partitioning</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Partitioning</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Assertion
	 * @see #isSetMessage_Partitioning()
	 * @see #unsetMessage_Partitioning()
	 * @see #getMessage_Partitioning()
	 * @generated
	 */
	void setMessage_Partitioning(Assertion value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getMessage_Partitioning <em>Message Partitioning</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMessage_Partitioning()
	 * @see #getMessage_Partitioning()
	 * @see #setMessage_Partitioning(Assertion)
	 * @generated
	 */
	void unsetMessage_Partitioning();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getMessage_Partitioning <em>Message Partitioning</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Message Partitioning</em>' attribute is set.
	 * @see #unsetMessage_Partitioning()
	 * @see #getMessage_Partitioning()
	 * @see #setMessage_Partitioning(Assertion)
	 * @generated
	 */
	boolean isSetMessage_Partitioning();

	/**
	 * Returns the value of the '<em><b>Number Of Stations</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Number Of Stations</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Number Of Stations</em>' attribute.
	 * @see #setNumber_Of_Stations(int)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRTEP_PacketDriver_Number_Of_Stations()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Positive" required="true"
	 *        extendedMetaData="kind='attribute' name='Number_Of_Stations'"
	 * @generated
	 */
	int getNumber_Of_Stations();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getNumber_Of_Stations <em>Number Of Stations</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Number Of Stations</em>' attribute.
	 * @see #getNumber_Of_Stations()
	 * @generated
	 */
	void setNumber_Of_Stations(int value);

	/**
	 * Returns the value of the '<em><b>Packet Discard Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Packet Discard Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packet Discard Operation</em>' attribute.
	 * @see #setPacket_Discard_Operation(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRTEP_PacketDriver_Packet_Discard_Operation()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref"
	 *        extendedMetaData="kind='attribute' name='Packet_Discard_Operation'"
	 * @generated
	 */
	String getPacket_Discard_Operation();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Discard_Operation <em>Packet Discard Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Packet Discard Operation</em>' attribute.
	 * @see #getPacket_Discard_Operation()
	 * @generated
	 */
	void setPacket_Discard_Operation(String value);

	/**
	 * Returns the value of the '<em><b>Packet Interrupt Server</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Packet Interrupt Server</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packet Interrupt Server</em>' attribute.
	 * @see #setPacket_Interrupt_Server(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRTEP_PacketDriver_Packet_Interrupt_Server()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref"
	 *        extendedMetaData="kind='attribute' name='Packet_Interrupt_Server'"
	 * @generated
	 */
	String getPacket_Interrupt_Server();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Interrupt_Server <em>Packet Interrupt Server</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Packet Interrupt Server</em>' attribute.
	 * @see #getPacket_Interrupt_Server()
	 * @generated
	 */
	void setPacket_Interrupt_Server(String value);

	/**
	 * Returns the value of the '<em><b>Packet ISR Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Packet ISR Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packet ISR Operation</em>' attribute.
	 * @see #setPacket_ISR_Operation(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRTEP_PacketDriver_Packet_ISR_Operation()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref"
	 *        extendedMetaData="kind='attribute' name='Packet_ISR_Operation'"
	 * @generated
	 */
	String getPacket_ISR_Operation();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_ISR_Operation <em>Packet ISR Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Packet ISR Operation</em>' attribute.
	 * @see #getPacket_ISR_Operation()
	 * @generated
	 */
	void setPacket_ISR_Operation(String value);

	/**
	 * Returns the value of the '<em><b>Packet Receive Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Packet Receive Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packet Receive Operation</em>' attribute.
	 * @see #setPacket_Receive_Operation(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRTEP_PacketDriver_Packet_Receive_Operation()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref"
	 *        extendedMetaData="kind='attribute' name='Packet_Receive_Operation'"
	 * @generated
	 */
	String getPacket_Receive_Operation();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Receive_Operation <em>Packet Receive Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Packet Receive Operation</em>' attribute.
	 * @see #getPacket_Receive_Operation()
	 * @generated
	 */
	void setPacket_Receive_Operation(String value);

	/**
	 * Returns the value of the '<em><b>Packet Retransmission Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Packet Retransmission Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packet Retransmission Operation</em>' attribute.
	 * @see #setPacket_Retransmission_Operation(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRTEP_PacketDriver_Packet_Retransmission_Operation()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref"
	 *        extendedMetaData="kind='attribute' name='Packet_Retransmission_Operation'"
	 * @generated
	 */
	String getPacket_Retransmission_Operation();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Retransmission_Operation <em>Packet Retransmission Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Packet Retransmission Operation</em>' attribute.
	 * @see #getPacket_Retransmission_Operation()
	 * @generated
	 */
	void setPacket_Retransmission_Operation(String value);

	/**
	 * Returns the value of the '<em><b>Packet Send Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Packet Send Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packet Send Operation</em>' attribute.
	 * @see #setPacket_Send_Operation(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRTEP_PacketDriver_Packet_Send_Operation()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref"
	 *        extendedMetaData="kind='attribute' name='Packet_Send_Operation'"
	 * @generated
	 */
	String getPacket_Send_Operation();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Send_Operation <em>Packet Send Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Packet Send Operation</em>' attribute.
	 * @see #getPacket_Send_Operation()
	 * @generated
	 */
	void setPacket_Send_Operation(String value);

	/**
	 * Returns the value of the '<em><b>Packet Server</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Packet Server</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packet Server</em>' attribute.
	 * @see #setPacket_Server(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRTEP_PacketDriver_Packet_Server()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref" required="true"
	 *        extendedMetaData="kind='attribute' name='Packet_Server'"
	 * @generated
	 */
	String getPacket_Server();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Server <em>Packet Server</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Packet Server</em>' attribute.
	 * @see #getPacket_Server()
	 * @generated
	 */
	void setPacket_Server(String value);

	/**
	 * Returns the value of the '<em><b>Packet Transmission Retries</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Packet Transmission Retries</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packet Transmission Retries</em>' attribute.
	 * @see #setPacket_Transmission_Retries(int)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRTEP_PacketDriver_Packet_Transmission_Retries()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Natural"
	 *        extendedMetaData="kind='attribute' name='Packet_Transmission_Retries'"
	 * @generated
	 */
	int getPacket_Transmission_Retries();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Transmission_Retries <em>Packet Transmission Retries</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Packet Transmission Retries</em>' attribute.
	 * @see #getPacket_Transmission_Retries()
	 * @generated
	 */
	void setPacket_Transmission_Retries(int value);

	/**
	 * Returns the value of the '<em><b>RTA Overhead Model</b></em>' attribute.
	 * The literals are from the enumeration {@link es.esi.gemde.vv.mast.mastmodel.Overhead_Type}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>RTA Overhead Model</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>RTA Overhead Model</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Overhead_Type
	 * @see #isSetRTA_Overhead_Model()
	 * @see #unsetRTA_Overhead_Model()
	 * @see #setRTA_Overhead_Model(Overhead_Type)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRTEP_PacketDriver_RTA_Overhead_Model()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='RTA_Overhead_Model'"
	 * @generated
	 */
	Overhead_Type getRTA_Overhead_Model();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getRTA_Overhead_Model <em>RTA Overhead Model</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>RTA Overhead Model</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Overhead_Type
	 * @see #isSetRTA_Overhead_Model()
	 * @see #unsetRTA_Overhead_Model()
	 * @see #getRTA_Overhead_Model()
	 * @generated
	 */
	void setRTA_Overhead_Model(Overhead_Type value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getRTA_Overhead_Model <em>RTA Overhead Model</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetRTA_Overhead_Model()
	 * @see #getRTA_Overhead_Model()
	 * @see #setRTA_Overhead_Model(Overhead_Type)
	 * @generated
	 */
	void unsetRTA_Overhead_Model();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getRTA_Overhead_Model <em>RTA Overhead Model</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>RTA Overhead Model</em>' attribute is set.
	 * @see #unsetRTA_Overhead_Model()
	 * @see #getRTA_Overhead_Model()
	 * @see #setRTA_Overhead_Model(Overhead_Type)
	 * @generated
	 */
	boolean isSetRTA_Overhead_Model();

	/**
	 * Returns the value of the '<em><b>Token Check Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Token Check Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Token Check Operation</em>' attribute.
	 * @see #setToken_Check_Operation(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRTEP_PacketDriver_Token_Check_Operation()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref"
	 *        extendedMetaData="kind='attribute' name='Token_Check_Operation'"
	 * @generated
	 */
	String getToken_Check_Operation();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getToken_Check_Operation <em>Token Check Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Token Check Operation</em>' attribute.
	 * @see #getToken_Check_Operation()
	 * @generated
	 */
	void setToken_Check_Operation(String value);

	/**
	 * Returns the value of the '<em><b>Token Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Token Delay</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Token Delay</em>' attribute.
	 * @see #setToken_Delay(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRTEP_PacketDriver_Token_Delay()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Time"
	 *        extendedMetaData="kind='attribute' name='Token_Delay'"
	 * @generated
	 */
	double getToken_Delay();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getToken_Delay <em>Token Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Token Delay</em>' attribute.
	 * @see #getToken_Delay()
	 * @generated
	 */
	void setToken_Delay(double value);

	/**
	 * Returns the value of the '<em><b>Token Manage Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Token Manage Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Token Manage Operation</em>' attribute.
	 * @see #setToken_Manage_Operation(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRTEP_PacketDriver_Token_Manage_Operation()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref"
	 *        extendedMetaData="kind='attribute' name='Token_Manage_Operation'"
	 * @generated
	 */
	String getToken_Manage_Operation();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getToken_Manage_Operation <em>Token Manage Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Token Manage Operation</em>' attribute.
	 * @see #getToken_Manage_Operation()
	 * @generated
	 */
	void setToken_Manage_Operation(String value);

	/**
	 * Returns the value of the '<em><b>Token Retransmission Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Token Retransmission Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Token Retransmission Operation</em>' attribute.
	 * @see #setToken_Retransmission_Operation(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRTEP_PacketDriver_Token_Retransmission_Operation()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref"
	 *        extendedMetaData="kind='attribute' name='Token_Retransmission_Operation'"
	 * @generated
	 */
	String getToken_Retransmission_Operation();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getToken_Retransmission_Operation <em>Token Retransmission Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Token Retransmission Operation</em>' attribute.
	 * @see #getToken_Retransmission_Operation()
	 * @generated
	 */
	void setToken_Retransmission_Operation(String value);

	/**
	 * Returns the value of the '<em><b>Token Transmission Retries</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Token Transmission Retries</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Token Transmission Retries</em>' attribute.
	 * @see #setToken_Transmission_Retries(int)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRTEP_PacketDriver_Token_Transmission_Retries()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Natural"
	 *        extendedMetaData="kind='attribute' name='Token_Transmission_Retries'"
	 * @generated
	 */
	int getToken_Transmission_Retries();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getToken_Transmission_Retries <em>Token Transmission Retries</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Token Transmission Retries</em>' attribute.
	 * @see #getToken_Transmission_Retries()
	 * @generated
	 */
	void setToken_Transmission_Retries(int value);

} // RTEP_PacketDriver
