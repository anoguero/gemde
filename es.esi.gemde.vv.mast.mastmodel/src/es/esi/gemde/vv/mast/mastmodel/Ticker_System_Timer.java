/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ticker System Timer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer#getAvg_Overhead <em>Avg Overhead</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer#getBest_Overhead <em>Best Overhead</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer#getPeriod <em>Period</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer#getWorst_Overhead <em>Worst Overhead</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getTicker_System_Timer()
 * @model extendedMetaData="name='Ticker_System_Timer' kind='empty'"
 * @generated
 */
public interface Ticker_System_Timer extends EObject {
	/**
	 * Returns the value of the '<em><b>Avg Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Avg Overhead</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Avg Overhead</em>' attribute.
	 * @see #setAvg_Overhead(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getTicker_System_Timer_Avg_Overhead()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Avg_Overhead'"
	 * @generated
	 */
	double getAvg_Overhead();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer#getAvg_Overhead <em>Avg Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Avg Overhead</em>' attribute.
	 * @see #getAvg_Overhead()
	 * @generated
	 */
	void setAvg_Overhead(double value);

	/**
	 * Returns the value of the '<em><b>Best Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Best Overhead</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Best Overhead</em>' attribute.
	 * @see #setBest_Overhead(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getTicker_System_Timer_Best_Overhead()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Best_Overhead'"
	 * @generated
	 */
	double getBest_Overhead();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer#getBest_Overhead <em>Best Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Best Overhead</em>' attribute.
	 * @see #getBest_Overhead()
	 * @generated
	 */
	void setBest_Overhead(double value);

	/**
	 * Returns the value of the '<em><b>Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Period</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Period</em>' attribute.
	 * @see #setPeriod(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getTicker_System_Timer_Period()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Time"
	 *        extendedMetaData="kind='attribute' name='Period'"
	 * @generated
	 */
	double getPeriod();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer#getPeriod <em>Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Period</em>' attribute.
	 * @see #getPeriod()
	 * @generated
	 */
	void setPeriod(double value);

	/**
	 * Returns the value of the '<em><b>Worst Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Worst Overhead</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Worst Overhead</em>' attribute.
	 * @see #setWorst_Overhead(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getTicker_System_Timer_Worst_Overhead()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Worst_Overhead'"
	 * @generated
	 */
	double getWorst_Overhead();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer#getWorst_Overhead <em>Worst Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Worst Overhead</em>' attribute.
	 * @see #getWorst_Overhead()
	 * @generated
	 */
	void setWorst_Overhead(double value);

} // Ticker_System_Timer
