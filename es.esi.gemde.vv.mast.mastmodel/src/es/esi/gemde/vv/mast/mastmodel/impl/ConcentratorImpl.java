/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Concentrator;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Concentrator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.ConcentratorImpl#getInput_Events_List <em>Input Events List</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.ConcentratorImpl#getOutput_Event <em>Output Event</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConcentratorImpl extends EObjectImpl implements Concentrator {
	/**
	 * The default value of the '{@link #getInput_Events_List() <em>Input Events List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInput_Events_List()
	 * @generated
	 * @ordered
	 */
	protected static final List<String> INPUT_EVENTS_LIST_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInput_Events_List() <em>Input Events List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInput_Events_List()
	 * @generated
	 * @ordered
	 */
	protected List<String> input_Events_List = INPUT_EVENTS_LIST_EDEFAULT;

	/**
	 * The default value of the '{@link #getOutput_Event() <em>Output Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutput_Event()
	 * @generated
	 * @ordered
	 */
	protected static final String OUTPUT_EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOutput_Event() <em>Output Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutput_Event()
	 * @generated
	 * @ordered
	 */
	protected String output_Event = OUTPUT_EVENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConcentratorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.CONCENTRATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<String> getInput_Events_List() {
		return input_Events_List;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInput_Events_List(List<String> newInput_Events_List) {
		List<String> oldInput_Events_List = input_Events_List;
		input_Events_List = newInput_Events_List;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.CONCENTRATOR__INPUT_EVENTS_LIST, oldInput_Events_List, input_Events_List));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOutput_Event() {
		return output_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutput_Event(String newOutput_Event) {
		String oldOutput_Event = output_Event;
		output_Event = newOutput_Event;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.CONCENTRATOR__OUTPUT_EVENT, oldOutput_Event, output_Event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.CONCENTRATOR__INPUT_EVENTS_LIST:
				return getInput_Events_List();
			case ModelPackage.CONCENTRATOR__OUTPUT_EVENT:
				return getOutput_Event();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.CONCENTRATOR__INPUT_EVENTS_LIST:
				setInput_Events_List((List<String>)newValue);
				return;
			case ModelPackage.CONCENTRATOR__OUTPUT_EVENT:
				setOutput_Event((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.CONCENTRATOR__INPUT_EVENTS_LIST:
				setInput_Events_List(INPUT_EVENTS_LIST_EDEFAULT);
				return;
			case ModelPackage.CONCENTRATOR__OUTPUT_EVENT:
				setOutput_Event(OUTPUT_EVENT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.CONCENTRATOR__INPUT_EVENTS_LIST:
				return INPUT_EVENTS_LIST_EDEFAULT == null ? input_Events_List != null : !INPUT_EVENTS_LIST_EDEFAULT.equals(input_Events_List);
			case ModelPackage.CONCENTRATOR__OUTPUT_EVENT:
				return OUTPUT_EVENT_EDEFAULT == null ? output_Event != null : !OUTPUT_EVENT_EDEFAULT.equals(output_Event);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Input_Events_List: ");
		result.append(input_Events_List);
		result.append(", Output_Event: ");
		result.append(output_Event);
		result.append(')');
		return result.toString();
	}

} //ConcentratorImpl
