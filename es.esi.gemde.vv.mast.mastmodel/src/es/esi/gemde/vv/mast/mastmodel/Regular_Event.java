/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Regular Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getGroup <em>Group</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getMax_Output_Jitter_Req <em>Max Output Jitter Req</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getHard_Global_Deadline <em>Hard Global Deadline</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getSoft_Global_Deadline <em>Soft Global Deadline</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getGlobal_Max_Miss_Ratio <em>Global Max Miss Ratio</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getHard_Local_Deadline <em>Hard Local Deadline</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getSoft_Local_Deadline <em>Soft Local Deadline</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getLocal_Max_Miss_Ratio <em>Local Max Miss Ratio</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getComposite_Timing_Requirement <em>Composite Timing Requirement</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getEvent <em>Event</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Event()
 * @model extendedMetaData="name='Regular_Event' kind='elementOnly'"
 * @generated
 */
public interface Regular_Event extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Event_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Max Output Jitter Req</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Max_Output_Jitter_Req}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Output Jitter Req</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Output Jitter Req</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Event_Max_Output_Jitter_Req()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Max_Output_Jitter_Req' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Max_Output_Jitter_Req> getMax_Output_Jitter_Req();

	/**
	 * Returns the value of the '<em><b>Hard Global Deadline</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Hard_Global_Deadline}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hard Global Deadline</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hard Global Deadline</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Event_Hard_Global_Deadline()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Hard_Global_Deadline' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Hard_Global_Deadline> getHard_Global_Deadline();

	/**
	 * Returns the value of the '<em><b>Soft Global Deadline</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Soft_Global_Deadline}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Soft Global Deadline</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Soft Global Deadline</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Event_Soft_Global_Deadline()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Soft_Global_Deadline' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Soft_Global_Deadline> getSoft_Global_Deadline();

	/**
	 * Returns the value of the '<em><b>Global Max Miss Ratio</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Global Max Miss Ratio</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Global Max Miss Ratio</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Event_Global_Max_Miss_Ratio()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Global_Max_Miss_Ratio' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Global_Max_Miss_Ratio> getGlobal_Max_Miss_Ratio();

	/**
	 * Returns the value of the '<em><b>Hard Local Deadline</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Hard_Local_Deadline}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hard Local Deadline</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hard Local Deadline</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Event_Hard_Local_Deadline()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Hard_Local_Deadline' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Hard_Local_Deadline> getHard_Local_Deadline();

	/**
	 * Returns the value of the '<em><b>Soft Local Deadline</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Soft_Local_Deadline}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Soft Local Deadline</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Soft Local Deadline</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Event_Soft_Local_Deadline()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Soft_Local_Deadline' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Soft_Local_Deadline> getSoft_Local_Deadline();

	/**
	 * Returns the value of the '<em><b>Local Max Miss Ratio</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Local_Max_Miss_Ratio}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Max Miss Ratio</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Max Miss Ratio</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Event_Local_Max_Miss_Ratio()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Local_Max_Miss_Ratio' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Local_Max_Miss_Ratio> getLocal_Max_Miss_Ratio();

	/**
	 * Returns the value of the '<em><b>Composite Timing Requirement</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Composite Timing Requirement</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Composite Timing Requirement</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Event_Composite_Timing_Requirement()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Composite_Timing_Requirement' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Composite_Timing_Requirement> getComposite_Timing_Requirement();

	/**
	 * Returns the value of the '<em><b>Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event</em>' attribute.
	 * @see #setEvent(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Event_Event()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier" required="true"
	 *        extendedMetaData="kind='attribute' name='Event'"
	 * @generated
	 */
	String getEvent();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getEvent <em>Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event</em>' attribute.
	 * @see #getEvent()
	 * @generated
	 */
	void setEvent(String value);

} // Regular_Event
