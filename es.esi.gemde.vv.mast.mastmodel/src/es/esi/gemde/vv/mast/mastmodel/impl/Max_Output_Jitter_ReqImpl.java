/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Max_Output_Jitter_Req;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Max Output Jitter Req</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Max_Output_Jitter_ReqImpl#getMax_Output_Jitter <em>Max Output Jitter</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Max_Output_Jitter_ReqImpl#getReferenced_Event <em>Referenced Event</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Max_Output_Jitter_ReqImpl extends EObjectImpl implements Max_Output_Jitter_Req {
	/**
	 * The default value of the '{@link #getMax_Output_Jitter() <em>Max Output Jitter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_Output_Jitter()
	 * @generated
	 * @ordered
	 */
	protected static final double MAX_OUTPUT_JITTER_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getMax_Output_Jitter() <em>Max Output Jitter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_Output_Jitter()
	 * @generated
	 * @ordered
	 */
	protected double max_Output_Jitter = MAX_OUTPUT_JITTER_EDEFAULT;

	/**
	 * The default value of the '{@link #getReferenced_Event() <em>Referenced Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenced_Event()
	 * @generated
	 * @ordered
	 */
	protected static final String REFERENCED_EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReferenced_Event() <em>Referenced Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenced_Event()
	 * @generated
	 * @ordered
	 */
	protected String referenced_Event = REFERENCED_EVENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Max_Output_Jitter_ReqImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.MAX_OUTPUT_JITTER_REQ;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getMax_Output_Jitter() {
		return max_Output_Jitter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMax_Output_Jitter(double newMax_Output_Jitter) {
		double oldMax_Output_Jitter = max_Output_Jitter;
		max_Output_Jitter = newMax_Output_Jitter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.MAX_OUTPUT_JITTER_REQ__MAX_OUTPUT_JITTER, oldMax_Output_Jitter, max_Output_Jitter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getReferenced_Event() {
		return referenced_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferenced_Event(String newReferenced_Event) {
		String oldReferenced_Event = referenced_Event;
		referenced_Event = newReferenced_Event;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.MAX_OUTPUT_JITTER_REQ__REFERENCED_EVENT, oldReferenced_Event, referenced_Event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.MAX_OUTPUT_JITTER_REQ__MAX_OUTPUT_JITTER:
				return getMax_Output_Jitter();
			case ModelPackage.MAX_OUTPUT_JITTER_REQ__REFERENCED_EVENT:
				return getReferenced_Event();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.MAX_OUTPUT_JITTER_REQ__MAX_OUTPUT_JITTER:
				setMax_Output_Jitter((Double)newValue);
				return;
			case ModelPackage.MAX_OUTPUT_JITTER_REQ__REFERENCED_EVENT:
				setReferenced_Event((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.MAX_OUTPUT_JITTER_REQ__MAX_OUTPUT_JITTER:
				setMax_Output_Jitter(MAX_OUTPUT_JITTER_EDEFAULT);
				return;
			case ModelPackage.MAX_OUTPUT_JITTER_REQ__REFERENCED_EVENT:
				setReferenced_Event(REFERENCED_EVENT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.MAX_OUTPUT_JITTER_REQ__MAX_OUTPUT_JITTER:
				return max_Output_Jitter != MAX_OUTPUT_JITTER_EDEFAULT;
			case ModelPackage.MAX_OUTPUT_JITTER_REQ__REFERENCED_EVENT:
				return REFERENCED_EVENT_EDEFAULT == null ? referenced_Event != null : !REFERENCED_EVENT_EDEFAULT.equals(referenced_Event);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Max_Output_Jitter: ");
		result.append(max_Output_Jitter);
		result.append(", Referenced_Event: ");
		result.append(referenced_Event);
		result.append(')');
		return result.toString();
	}

} //Max_Output_Jitter_ReqImpl
