/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event;
import es.esi.gemde.vv.mast.mastmodel.Distribution_Type;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;

import java.math.BigInteger;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Bursty External Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Bursty_External_EventImpl#getAvg_Interarrival <em>Avg Interarrival</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Bursty_External_EventImpl#getBound_Interval <em>Bound Interval</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Bursty_External_EventImpl#getDistribution <em>Distribution</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Bursty_External_EventImpl#getMax_Arrivals <em>Max Arrivals</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Bursty_External_EventImpl#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Bursty_External_EventImpl extends EObjectImpl implements Bursty_External_Event {
	/**
	 * The default value of the '{@link #getAvg_Interarrival() <em>Avg Interarrival</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvg_Interarrival()
	 * @generated
	 * @ordered
	 */
	protected static final double AVG_INTERARRIVAL_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getAvg_Interarrival() <em>Avg Interarrival</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvg_Interarrival()
	 * @generated
	 * @ordered
	 */
	protected double avg_Interarrival = AVG_INTERARRIVAL_EDEFAULT;

	/**
	 * The default value of the '{@link #getBound_Interval() <em>Bound Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBound_Interval()
	 * @generated
	 * @ordered
	 */
	protected static final double BOUND_INTERVAL_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getBound_Interval() <em>Bound Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBound_Interval()
	 * @generated
	 * @ordered
	 */
	protected double bound_Interval = BOUND_INTERVAL_EDEFAULT;

	/**
	 * The default value of the '{@link #getDistribution() <em>Distribution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDistribution()
	 * @generated
	 * @ordered
	 */
	protected static final Distribution_Type DISTRIBUTION_EDEFAULT = Distribution_Type.UNIFORM;

	/**
	 * The cached value of the '{@link #getDistribution() <em>Distribution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDistribution()
	 * @generated
	 * @ordered
	 */
	protected Distribution_Type distribution = DISTRIBUTION_EDEFAULT;

	/**
	 * This is true if the Distribution attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean distributionESet;

	/**
	 * The default value of the '{@link #getMax_Arrivals() <em>Max Arrivals</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_Arrivals()
	 * @generated
	 * @ordered
	 */
	protected static final BigInteger MAX_ARRIVALS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMax_Arrivals() <em>Max Arrivals</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_Arrivals()
	 * @generated
	 * @ordered
	 */
	protected BigInteger max_Arrivals = MAX_ARRIVALS_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Bursty_External_EventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.BURSTY_EXTERNAL_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getAvg_Interarrival() {
		return avg_Interarrival;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAvg_Interarrival(double newAvg_Interarrival) {
		double oldAvg_Interarrival = avg_Interarrival;
		avg_Interarrival = newAvg_Interarrival;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.BURSTY_EXTERNAL_EVENT__AVG_INTERARRIVAL, oldAvg_Interarrival, avg_Interarrival));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getBound_Interval() {
		return bound_Interval;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBound_Interval(double newBound_Interval) {
		double oldBound_Interval = bound_Interval;
		bound_Interval = newBound_Interval;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.BURSTY_EXTERNAL_EVENT__BOUND_INTERVAL, oldBound_Interval, bound_Interval));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Distribution_Type getDistribution() {
		return distribution;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDistribution(Distribution_Type newDistribution) {
		Distribution_Type oldDistribution = distribution;
		distribution = newDistribution == null ? DISTRIBUTION_EDEFAULT : newDistribution;
		boolean oldDistributionESet = distributionESet;
		distributionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.BURSTY_EXTERNAL_EVENT__DISTRIBUTION, oldDistribution, distribution, !oldDistributionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDistribution() {
		Distribution_Type oldDistribution = distribution;
		boolean oldDistributionESet = distributionESet;
		distribution = DISTRIBUTION_EDEFAULT;
		distributionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ModelPackage.BURSTY_EXTERNAL_EVENT__DISTRIBUTION, oldDistribution, DISTRIBUTION_EDEFAULT, oldDistributionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDistribution() {
		return distributionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BigInteger getMax_Arrivals() {
		return max_Arrivals;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMax_Arrivals(BigInteger newMax_Arrivals) {
		BigInteger oldMax_Arrivals = max_Arrivals;
		max_Arrivals = newMax_Arrivals;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.BURSTY_EXTERNAL_EVENT__MAX_ARRIVALS, oldMax_Arrivals, max_Arrivals));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.BURSTY_EXTERNAL_EVENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.BURSTY_EXTERNAL_EVENT__AVG_INTERARRIVAL:
				return getAvg_Interarrival();
			case ModelPackage.BURSTY_EXTERNAL_EVENT__BOUND_INTERVAL:
				return getBound_Interval();
			case ModelPackage.BURSTY_EXTERNAL_EVENT__DISTRIBUTION:
				return getDistribution();
			case ModelPackage.BURSTY_EXTERNAL_EVENT__MAX_ARRIVALS:
				return getMax_Arrivals();
			case ModelPackage.BURSTY_EXTERNAL_EVENT__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.BURSTY_EXTERNAL_EVENT__AVG_INTERARRIVAL:
				setAvg_Interarrival((Double)newValue);
				return;
			case ModelPackage.BURSTY_EXTERNAL_EVENT__BOUND_INTERVAL:
				setBound_Interval((Double)newValue);
				return;
			case ModelPackage.BURSTY_EXTERNAL_EVENT__DISTRIBUTION:
				setDistribution((Distribution_Type)newValue);
				return;
			case ModelPackage.BURSTY_EXTERNAL_EVENT__MAX_ARRIVALS:
				setMax_Arrivals((BigInteger)newValue);
				return;
			case ModelPackage.BURSTY_EXTERNAL_EVENT__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.BURSTY_EXTERNAL_EVENT__AVG_INTERARRIVAL:
				setAvg_Interarrival(AVG_INTERARRIVAL_EDEFAULT);
				return;
			case ModelPackage.BURSTY_EXTERNAL_EVENT__BOUND_INTERVAL:
				setBound_Interval(BOUND_INTERVAL_EDEFAULT);
				return;
			case ModelPackage.BURSTY_EXTERNAL_EVENT__DISTRIBUTION:
				unsetDistribution();
				return;
			case ModelPackage.BURSTY_EXTERNAL_EVENT__MAX_ARRIVALS:
				setMax_Arrivals(MAX_ARRIVALS_EDEFAULT);
				return;
			case ModelPackage.BURSTY_EXTERNAL_EVENT__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.BURSTY_EXTERNAL_EVENT__AVG_INTERARRIVAL:
				return avg_Interarrival != AVG_INTERARRIVAL_EDEFAULT;
			case ModelPackage.BURSTY_EXTERNAL_EVENT__BOUND_INTERVAL:
				return bound_Interval != BOUND_INTERVAL_EDEFAULT;
			case ModelPackage.BURSTY_EXTERNAL_EVENT__DISTRIBUTION:
				return isSetDistribution();
			case ModelPackage.BURSTY_EXTERNAL_EVENT__MAX_ARRIVALS:
				return MAX_ARRIVALS_EDEFAULT == null ? max_Arrivals != null : !MAX_ARRIVALS_EDEFAULT.equals(max_Arrivals);
			case ModelPackage.BURSTY_EXTERNAL_EVENT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Avg_Interarrival: ");
		result.append(avg_Interarrival);
		result.append(", Bound_Interval: ");
		result.append(bound_Interval);
		result.append(", Distribution: ");
		if (distributionESet) result.append(distribution); else result.append("<unset>");
		result.append(", Max_Arrivals: ");
		result.append(max_Arrivals);
		result.append(", Name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //Bursty_External_EventImpl
