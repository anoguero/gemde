/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Assertion;
import es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Overhead_Type;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Character Packet Driver</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Character_Packet_DriverImpl#getCharacter_Receive_Operation <em>Character Receive Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Character_Packet_DriverImpl#getCharacter_Send_Operation <em>Character Send Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Character_Packet_DriverImpl#getCharacter_Server <em>Character Server</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Character_Packet_DriverImpl#getCharacter_Transmission_Time <em>Character Transmission Time</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Character_Packet_DriverImpl#getMessage_Partitioning <em>Message Partitioning</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Character_Packet_DriverImpl#getPacket_Receive_Operation <em>Packet Receive Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Character_Packet_DriverImpl#getPacket_Send_Operation <em>Packet Send Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Character_Packet_DriverImpl#getPacket_Server <em>Packet Server</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Character_Packet_DriverImpl#getRTA_Overhead_Model <em>RTA Overhead Model</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Character_Packet_DriverImpl extends EObjectImpl implements Character_Packet_Driver {
	/**
	 * The default value of the '{@link #getCharacter_Receive_Operation() <em>Character Receive Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCharacter_Receive_Operation()
	 * @generated
	 * @ordered
	 */
	protected static final String CHARACTER_RECEIVE_OPERATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCharacter_Receive_Operation() <em>Character Receive Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCharacter_Receive_Operation()
	 * @generated
	 * @ordered
	 */
	protected String character_Receive_Operation = CHARACTER_RECEIVE_OPERATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getCharacter_Send_Operation() <em>Character Send Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCharacter_Send_Operation()
	 * @generated
	 * @ordered
	 */
	protected static final String CHARACTER_SEND_OPERATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCharacter_Send_Operation() <em>Character Send Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCharacter_Send_Operation()
	 * @generated
	 * @ordered
	 */
	protected String character_Send_Operation = CHARACTER_SEND_OPERATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getCharacter_Server() <em>Character Server</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCharacter_Server()
	 * @generated
	 * @ordered
	 */
	protected static final String CHARACTER_SERVER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCharacter_Server() <em>Character Server</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCharacter_Server()
	 * @generated
	 * @ordered
	 */
	protected String character_Server = CHARACTER_SERVER_EDEFAULT;

	/**
	 * The default value of the '{@link #getCharacter_Transmission_Time() <em>Character Transmission Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCharacter_Transmission_Time()
	 * @generated
	 * @ordered
	 */
	protected static final double CHARACTER_TRANSMISSION_TIME_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getCharacter_Transmission_Time() <em>Character Transmission Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCharacter_Transmission_Time()
	 * @generated
	 * @ordered
	 */
	protected double character_Transmission_Time = CHARACTER_TRANSMISSION_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getMessage_Partitioning() <em>Message Partitioning</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage_Partitioning()
	 * @generated
	 * @ordered
	 */
	protected static final Assertion MESSAGE_PARTITIONING_EDEFAULT = Assertion.YES;

	/**
	 * The cached value of the '{@link #getMessage_Partitioning() <em>Message Partitioning</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage_Partitioning()
	 * @generated
	 * @ordered
	 */
	protected Assertion message_Partitioning = MESSAGE_PARTITIONING_EDEFAULT;

	/**
	 * This is true if the Message Partitioning attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean message_PartitioningESet;

	/**
	 * The default value of the '{@link #getPacket_Receive_Operation() <em>Packet Receive Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Receive_Operation()
	 * @generated
	 * @ordered
	 */
	protected static final String PACKET_RECEIVE_OPERATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPacket_Receive_Operation() <em>Packet Receive Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Receive_Operation()
	 * @generated
	 * @ordered
	 */
	protected String packet_Receive_Operation = PACKET_RECEIVE_OPERATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getPacket_Send_Operation() <em>Packet Send Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Send_Operation()
	 * @generated
	 * @ordered
	 */
	protected static final String PACKET_SEND_OPERATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPacket_Send_Operation() <em>Packet Send Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Send_Operation()
	 * @generated
	 * @ordered
	 */
	protected String packet_Send_Operation = PACKET_SEND_OPERATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getPacket_Server() <em>Packet Server</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Server()
	 * @generated
	 * @ordered
	 */
	protected static final String PACKET_SERVER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPacket_Server() <em>Packet Server</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Server()
	 * @generated
	 * @ordered
	 */
	protected String packet_Server = PACKET_SERVER_EDEFAULT;

	/**
	 * The default value of the '{@link #getRTA_Overhead_Model() <em>RTA Overhead Model</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRTA_Overhead_Model()
	 * @generated
	 * @ordered
	 */
	protected static final Overhead_Type RTA_OVERHEAD_MODEL_EDEFAULT = Overhead_Type.COUPLED;

	/**
	 * The cached value of the '{@link #getRTA_Overhead_Model() <em>RTA Overhead Model</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRTA_Overhead_Model()
	 * @generated
	 * @ordered
	 */
	protected Overhead_Type rtA_Overhead_Model = RTA_OVERHEAD_MODEL_EDEFAULT;

	/**
	 * This is true if the RTA Overhead Model attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean rtA_Overhead_ModelESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Character_Packet_DriverImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.CHARACTER_PACKET_DRIVER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCharacter_Receive_Operation() {
		return character_Receive_Operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCharacter_Receive_Operation(String newCharacter_Receive_Operation) {
		String oldCharacter_Receive_Operation = character_Receive_Operation;
		character_Receive_Operation = newCharacter_Receive_Operation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.CHARACTER_PACKET_DRIVER__CHARACTER_RECEIVE_OPERATION, oldCharacter_Receive_Operation, character_Receive_Operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCharacter_Send_Operation() {
		return character_Send_Operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCharacter_Send_Operation(String newCharacter_Send_Operation) {
		String oldCharacter_Send_Operation = character_Send_Operation;
		character_Send_Operation = newCharacter_Send_Operation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.CHARACTER_PACKET_DRIVER__CHARACTER_SEND_OPERATION, oldCharacter_Send_Operation, character_Send_Operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCharacter_Server() {
		return character_Server;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCharacter_Server(String newCharacter_Server) {
		String oldCharacter_Server = character_Server;
		character_Server = newCharacter_Server;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.CHARACTER_PACKET_DRIVER__CHARACTER_SERVER, oldCharacter_Server, character_Server));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getCharacter_Transmission_Time() {
		return character_Transmission_Time;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCharacter_Transmission_Time(double newCharacter_Transmission_Time) {
		double oldCharacter_Transmission_Time = character_Transmission_Time;
		character_Transmission_Time = newCharacter_Transmission_Time;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.CHARACTER_PACKET_DRIVER__CHARACTER_TRANSMISSION_TIME, oldCharacter_Transmission_Time, character_Transmission_Time));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Assertion getMessage_Partitioning() {
		return message_Partitioning;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMessage_Partitioning(Assertion newMessage_Partitioning) {
		Assertion oldMessage_Partitioning = message_Partitioning;
		message_Partitioning = newMessage_Partitioning == null ? MESSAGE_PARTITIONING_EDEFAULT : newMessage_Partitioning;
		boolean oldMessage_PartitioningESet = message_PartitioningESet;
		message_PartitioningESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.CHARACTER_PACKET_DRIVER__MESSAGE_PARTITIONING, oldMessage_Partitioning, message_Partitioning, !oldMessage_PartitioningESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMessage_Partitioning() {
		Assertion oldMessage_Partitioning = message_Partitioning;
		boolean oldMessage_PartitioningESet = message_PartitioningESet;
		message_Partitioning = MESSAGE_PARTITIONING_EDEFAULT;
		message_PartitioningESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ModelPackage.CHARACTER_PACKET_DRIVER__MESSAGE_PARTITIONING, oldMessage_Partitioning, MESSAGE_PARTITIONING_EDEFAULT, oldMessage_PartitioningESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMessage_Partitioning() {
		return message_PartitioningESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPacket_Receive_Operation() {
		return packet_Receive_Operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPacket_Receive_Operation(String newPacket_Receive_Operation) {
		String oldPacket_Receive_Operation = packet_Receive_Operation;
		packet_Receive_Operation = newPacket_Receive_Operation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.CHARACTER_PACKET_DRIVER__PACKET_RECEIVE_OPERATION, oldPacket_Receive_Operation, packet_Receive_Operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPacket_Send_Operation() {
		return packet_Send_Operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPacket_Send_Operation(String newPacket_Send_Operation) {
		String oldPacket_Send_Operation = packet_Send_Operation;
		packet_Send_Operation = newPacket_Send_Operation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.CHARACTER_PACKET_DRIVER__PACKET_SEND_OPERATION, oldPacket_Send_Operation, packet_Send_Operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPacket_Server() {
		return packet_Server;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPacket_Server(String newPacket_Server) {
		String oldPacket_Server = packet_Server;
		packet_Server = newPacket_Server;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.CHARACTER_PACKET_DRIVER__PACKET_SERVER, oldPacket_Server, packet_Server));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Overhead_Type getRTA_Overhead_Model() {
		return rtA_Overhead_Model;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRTA_Overhead_Model(Overhead_Type newRTA_Overhead_Model) {
		Overhead_Type oldRTA_Overhead_Model = rtA_Overhead_Model;
		rtA_Overhead_Model = newRTA_Overhead_Model == null ? RTA_OVERHEAD_MODEL_EDEFAULT : newRTA_Overhead_Model;
		boolean oldRTA_Overhead_ModelESet = rtA_Overhead_ModelESet;
		rtA_Overhead_ModelESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.CHARACTER_PACKET_DRIVER__RTA_OVERHEAD_MODEL, oldRTA_Overhead_Model, rtA_Overhead_Model, !oldRTA_Overhead_ModelESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetRTA_Overhead_Model() {
		Overhead_Type oldRTA_Overhead_Model = rtA_Overhead_Model;
		boolean oldRTA_Overhead_ModelESet = rtA_Overhead_ModelESet;
		rtA_Overhead_Model = RTA_OVERHEAD_MODEL_EDEFAULT;
		rtA_Overhead_ModelESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ModelPackage.CHARACTER_PACKET_DRIVER__RTA_OVERHEAD_MODEL, oldRTA_Overhead_Model, RTA_OVERHEAD_MODEL_EDEFAULT, oldRTA_Overhead_ModelESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetRTA_Overhead_Model() {
		return rtA_Overhead_ModelESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.CHARACTER_PACKET_DRIVER__CHARACTER_RECEIVE_OPERATION:
				return getCharacter_Receive_Operation();
			case ModelPackage.CHARACTER_PACKET_DRIVER__CHARACTER_SEND_OPERATION:
				return getCharacter_Send_Operation();
			case ModelPackage.CHARACTER_PACKET_DRIVER__CHARACTER_SERVER:
				return getCharacter_Server();
			case ModelPackage.CHARACTER_PACKET_DRIVER__CHARACTER_TRANSMISSION_TIME:
				return getCharacter_Transmission_Time();
			case ModelPackage.CHARACTER_PACKET_DRIVER__MESSAGE_PARTITIONING:
				return getMessage_Partitioning();
			case ModelPackage.CHARACTER_PACKET_DRIVER__PACKET_RECEIVE_OPERATION:
				return getPacket_Receive_Operation();
			case ModelPackage.CHARACTER_PACKET_DRIVER__PACKET_SEND_OPERATION:
				return getPacket_Send_Operation();
			case ModelPackage.CHARACTER_PACKET_DRIVER__PACKET_SERVER:
				return getPacket_Server();
			case ModelPackage.CHARACTER_PACKET_DRIVER__RTA_OVERHEAD_MODEL:
				return getRTA_Overhead_Model();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.CHARACTER_PACKET_DRIVER__CHARACTER_RECEIVE_OPERATION:
				setCharacter_Receive_Operation((String)newValue);
				return;
			case ModelPackage.CHARACTER_PACKET_DRIVER__CHARACTER_SEND_OPERATION:
				setCharacter_Send_Operation((String)newValue);
				return;
			case ModelPackage.CHARACTER_PACKET_DRIVER__CHARACTER_SERVER:
				setCharacter_Server((String)newValue);
				return;
			case ModelPackage.CHARACTER_PACKET_DRIVER__CHARACTER_TRANSMISSION_TIME:
				setCharacter_Transmission_Time((Double)newValue);
				return;
			case ModelPackage.CHARACTER_PACKET_DRIVER__MESSAGE_PARTITIONING:
				setMessage_Partitioning((Assertion)newValue);
				return;
			case ModelPackage.CHARACTER_PACKET_DRIVER__PACKET_RECEIVE_OPERATION:
				setPacket_Receive_Operation((String)newValue);
				return;
			case ModelPackage.CHARACTER_PACKET_DRIVER__PACKET_SEND_OPERATION:
				setPacket_Send_Operation((String)newValue);
				return;
			case ModelPackage.CHARACTER_PACKET_DRIVER__PACKET_SERVER:
				setPacket_Server((String)newValue);
				return;
			case ModelPackage.CHARACTER_PACKET_DRIVER__RTA_OVERHEAD_MODEL:
				setRTA_Overhead_Model((Overhead_Type)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.CHARACTER_PACKET_DRIVER__CHARACTER_RECEIVE_OPERATION:
				setCharacter_Receive_Operation(CHARACTER_RECEIVE_OPERATION_EDEFAULT);
				return;
			case ModelPackage.CHARACTER_PACKET_DRIVER__CHARACTER_SEND_OPERATION:
				setCharacter_Send_Operation(CHARACTER_SEND_OPERATION_EDEFAULT);
				return;
			case ModelPackage.CHARACTER_PACKET_DRIVER__CHARACTER_SERVER:
				setCharacter_Server(CHARACTER_SERVER_EDEFAULT);
				return;
			case ModelPackage.CHARACTER_PACKET_DRIVER__CHARACTER_TRANSMISSION_TIME:
				setCharacter_Transmission_Time(CHARACTER_TRANSMISSION_TIME_EDEFAULT);
				return;
			case ModelPackage.CHARACTER_PACKET_DRIVER__MESSAGE_PARTITIONING:
				unsetMessage_Partitioning();
				return;
			case ModelPackage.CHARACTER_PACKET_DRIVER__PACKET_RECEIVE_OPERATION:
				setPacket_Receive_Operation(PACKET_RECEIVE_OPERATION_EDEFAULT);
				return;
			case ModelPackage.CHARACTER_PACKET_DRIVER__PACKET_SEND_OPERATION:
				setPacket_Send_Operation(PACKET_SEND_OPERATION_EDEFAULT);
				return;
			case ModelPackage.CHARACTER_PACKET_DRIVER__PACKET_SERVER:
				setPacket_Server(PACKET_SERVER_EDEFAULT);
				return;
			case ModelPackage.CHARACTER_PACKET_DRIVER__RTA_OVERHEAD_MODEL:
				unsetRTA_Overhead_Model();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.CHARACTER_PACKET_DRIVER__CHARACTER_RECEIVE_OPERATION:
				return CHARACTER_RECEIVE_OPERATION_EDEFAULT == null ? character_Receive_Operation != null : !CHARACTER_RECEIVE_OPERATION_EDEFAULT.equals(character_Receive_Operation);
			case ModelPackage.CHARACTER_PACKET_DRIVER__CHARACTER_SEND_OPERATION:
				return CHARACTER_SEND_OPERATION_EDEFAULT == null ? character_Send_Operation != null : !CHARACTER_SEND_OPERATION_EDEFAULT.equals(character_Send_Operation);
			case ModelPackage.CHARACTER_PACKET_DRIVER__CHARACTER_SERVER:
				return CHARACTER_SERVER_EDEFAULT == null ? character_Server != null : !CHARACTER_SERVER_EDEFAULT.equals(character_Server);
			case ModelPackage.CHARACTER_PACKET_DRIVER__CHARACTER_TRANSMISSION_TIME:
				return character_Transmission_Time != CHARACTER_TRANSMISSION_TIME_EDEFAULT;
			case ModelPackage.CHARACTER_PACKET_DRIVER__MESSAGE_PARTITIONING:
				return isSetMessage_Partitioning();
			case ModelPackage.CHARACTER_PACKET_DRIVER__PACKET_RECEIVE_OPERATION:
				return PACKET_RECEIVE_OPERATION_EDEFAULT == null ? packet_Receive_Operation != null : !PACKET_RECEIVE_OPERATION_EDEFAULT.equals(packet_Receive_Operation);
			case ModelPackage.CHARACTER_PACKET_DRIVER__PACKET_SEND_OPERATION:
				return PACKET_SEND_OPERATION_EDEFAULT == null ? packet_Send_Operation != null : !PACKET_SEND_OPERATION_EDEFAULT.equals(packet_Send_Operation);
			case ModelPackage.CHARACTER_PACKET_DRIVER__PACKET_SERVER:
				return PACKET_SERVER_EDEFAULT == null ? packet_Server != null : !PACKET_SERVER_EDEFAULT.equals(packet_Server);
			case ModelPackage.CHARACTER_PACKET_DRIVER__RTA_OVERHEAD_MODEL:
				return isSetRTA_Overhead_Model();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Character_Receive_Operation: ");
		result.append(character_Receive_Operation);
		result.append(", Character_Send_Operation: ");
		result.append(character_Send_Operation);
		result.append(", Character_Server: ");
		result.append(character_Server);
		result.append(", Character_Transmission_Time: ");
		result.append(character_Transmission_Time);
		result.append(", Message_Partitioning: ");
		if (message_PartitioningESet) result.append(message_Partitioning); else result.append("<unset>");
		result.append(", Packet_Receive_Operation: ");
		result.append(packet_Receive_Operation);
		result.append(", Packet_Send_Operation: ");
		result.append(packet_Send_Operation);
		result.append(", Packet_Server: ");
		result.append(packet_Server);
		result.append(", RTA_Overhead_Model: ");
		if (rtA_Overhead_ModelESet) result.append(rtA_Overhead_Model); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //Character_Packet_DriverImpl
