/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement;
import es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio;
import es.esi.gemde.vv.mast.mastmodel.Hard_Global_Deadline;
import es.esi.gemde.vv.mast.mastmodel.Hard_Local_Deadline;
import es.esi.gemde.vv.mast.mastmodel.Local_Max_Miss_Ratio;
import es.esi.gemde.vv.mast.mastmodel.Max_Output_Jitter_Req;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Regular_Event;
import es.esi.gemde.vv.mast.mastmodel.Soft_Global_Deadline;
import es.esi.gemde.vv.mast.mastmodel.Soft_Local_Deadline;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Regular Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_EventImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_EventImpl#getMax_Output_Jitter_Req <em>Max Output Jitter Req</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_EventImpl#getHard_Global_Deadline <em>Hard Global Deadline</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_EventImpl#getSoft_Global_Deadline <em>Soft Global Deadline</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_EventImpl#getGlobal_Max_Miss_Ratio <em>Global Max Miss Ratio</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_EventImpl#getHard_Local_Deadline <em>Hard Local Deadline</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_EventImpl#getSoft_Local_Deadline <em>Soft Local Deadline</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_EventImpl#getLocal_Max_Miss_Ratio <em>Local Max Miss Ratio</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_EventImpl#getComposite_Timing_Requirement <em>Composite Timing Requirement</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_EventImpl#getEvent <em>Event</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Regular_EventImpl extends EObjectImpl implements Regular_Event {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * The default value of the '{@link #getEvent() <em>Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvent()
	 * @generated
	 * @ordered
	 */
	protected static final String EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEvent() <em>Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvent()
	 * @generated
	 * @ordered
	 */
	protected String event = EVENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Regular_EventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.REGULAR_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, ModelPackage.REGULAR_EVENT__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Max_Output_Jitter_Req> getMax_Output_Jitter_Req() {
		return getGroup().list(ModelPackage.Literals.REGULAR_EVENT__MAX_OUTPUT_JITTER_REQ);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Hard_Global_Deadline> getHard_Global_Deadline() {
		return getGroup().list(ModelPackage.Literals.REGULAR_EVENT__HARD_GLOBAL_DEADLINE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Soft_Global_Deadline> getSoft_Global_Deadline() {
		return getGroup().list(ModelPackage.Literals.REGULAR_EVENT__SOFT_GLOBAL_DEADLINE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Global_Max_Miss_Ratio> getGlobal_Max_Miss_Ratio() {
		return getGroup().list(ModelPackage.Literals.REGULAR_EVENT__GLOBAL_MAX_MISS_RATIO);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Hard_Local_Deadline> getHard_Local_Deadline() {
		return getGroup().list(ModelPackage.Literals.REGULAR_EVENT__HARD_LOCAL_DEADLINE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Soft_Local_Deadline> getSoft_Local_Deadline() {
		return getGroup().list(ModelPackage.Literals.REGULAR_EVENT__SOFT_LOCAL_DEADLINE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Local_Max_Miss_Ratio> getLocal_Max_Miss_Ratio() {
		return getGroup().list(ModelPackage.Literals.REGULAR_EVENT__LOCAL_MAX_MISS_RATIO);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Composite_Timing_Requirement> getComposite_Timing_Requirement() {
		return getGroup().list(ModelPackage.Literals.REGULAR_EVENT__COMPOSITE_TIMING_REQUIREMENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEvent() {
		return event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvent(String newEvent) {
		String oldEvent = event;
		event = newEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_EVENT__EVENT, oldEvent, event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelPackage.REGULAR_EVENT__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_EVENT__MAX_OUTPUT_JITTER_REQ:
				return ((InternalEList<?>)getMax_Output_Jitter_Req()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_EVENT__HARD_GLOBAL_DEADLINE:
				return ((InternalEList<?>)getHard_Global_Deadline()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_EVENT__SOFT_GLOBAL_DEADLINE:
				return ((InternalEList<?>)getSoft_Global_Deadline()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_EVENT__GLOBAL_MAX_MISS_RATIO:
				return ((InternalEList<?>)getGlobal_Max_Miss_Ratio()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_EVENT__HARD_LOCAL_DEADLINE:
				return ((InternalEList<?>)getHard_Local_Deadline()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_EVENT__SOFT_LOCAL_DEADLINE:
				return ((InternalEList<?>)getSoft_Local_Deadline()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_EVENT__LOCAL_MAX_MISS_RATIO:
				return ((InternalEList<?>)getLocal_Max_Miss_Ratio()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_EVENT__COMPOSITE_TIMING_REQUIREMENT:
				return ((InternalEList<?>)getComposite_Timing_Requirement()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.REGULAR_EVENT__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case ModelPackage.REGULAR_EVENT__MAX_OUTPUT_JITTER_REQ:
				return getMax_Output_Jitter_Req();
			case ModelPackage.REGULAR_EVENT__HARD_GLOBAL_DEADLINE:
				return getHard_Global_Deadline();
			case ModelPackage.REGULAR_EVENT__SOFT_GLOBAL_DEADLINE:
				return getSoft_Global_Deadline();
			case ModelPackage.REGULAR_EVENT__GLOBAL_MAX_MISS_RATIO:
				return getGlobal_Max_Miss_Ratio();
			case ModelPackage.REGULAR_EVENT__HARD_LOCAL_DEADLINE:
				return getHard_Local_Deadline();
			case ModelPackage.REGULAR_EVENT__SOFT_LOCAL_DEADLINE:
				return getSoft_Local_Deadline();
			case ModelPackage.REGULAR_EVENT__LOCAL_MAX_MISS_RATIO:
				return getLocal_Max_Miss_Ratio();
			case ModelPackage.REGULAR_EVENT__COMPOSITE_TIMING_REQUIREMENT:
				return getComposite_Timing_Requirement();
			case ModelPackage.REGULAR_EVENT__EVENT:
				return getEvent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.REGULAR_EVENT__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case ModelPackage.REGULAR_EVENT__MAX_OUTPUT_JITTER_REQ:
				getMax_Output_Jitter_Req().clear();
				getMax_Output_Jitter_Req().addAll((Collection<? extends Max_Output_Jitter_Req>)newValue);
				return;
			case ModelPackage.REGULAR_EVENT__HARD_GLOBAL_DEADLINE:
				getHard_Global_Deadline().clear();
				getHard_Global_Deadline().addAll((Collection<? extends Hard_Global_Deadline>)newValue);
				return;
			case ModelPackage.REGULAR_EVENT__SOFT_GLOBAL_DEADLINE:
				getSoft_Global_Deadline().clear();
				getSoft_Global_Deadline().addAll((Collection<? extends Soft_Global_Deadline>)newValue);
				return;
			case ModelPackage.REGULAR_EVENT__GLOBAL_MAX_MISS_RATIO:
				getGlobal_Max_Miss_Ratio().clear();
				getGlobal_Max_Miss_Ratio().addAll((Collection<? extends Global_Max_Miss_Ratio>)newValue);
				return;
			case ModelPackage.REGULAR_EVENT__HARD_LOCAL_DEADLINE:
				getHard_Local_Deadline().clear();
				getHard_Local_Deadline().addAll((Collection<? extends Hard_Local_Deadline>)newValue);
				return;
			case ModelPackage.REGULAR_EVENT__SOFT_LOCAL_DEADLINE:
				getSoft_Local_Deadline().clear();
				getSoft_Local_Deadline().addAll((Collection<? extends Soft_Local_Deadline>)newValue);
				return;
			case ModelPackage.REGULAR_EVENT__LOCAL_MAX_MISS_RATIO:
				getLocal_Max_Miss_Ratio().clear();
				getLocal_Max_Miss_Ratio().addAll((Collection<? extends Local_Max_Miss_Ratio>)newValue);
				return;
			case ModelPackage.REGULAR_EVENT__COMPOSITE_TIMING_REQUIREMENT:
				getComposite_Timing_Requirement().clear();
				getComposite_Timing_Requirement().addAll((Collection<? extends Composite_Timing_Requirement>)newValue);
				return;
			case ModelPackage.REGULAR_EVENT__EVENT:
				setEvent((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.REGULAR_EVENT__GROUP:
				getGroup().clear();
				return;
			case ModelPackage.REGULAR_EVENT__MAX_OUTPUT_JITTER_REQ:
				getMax_Output_Jitter_Req().clear();
				return;
			case ModelPackage.REGULAR_EVENT__HARD_GLOBAL_DEADLINE:
				getHard_Global_Deadline().clear();
				return;
			case ModelPackage.REGULAR_EVENT__SOFT_GLOBAL_DEADLINE:
				getSoft_Global_Deadline().clear();
				return;
			case ModelPackage.REGULAR_EVENT__GLOBAL_MAX_MISS_RATIO:
				getGlobal_Max_Miss_Ratio().clear();
				return;
			case ModelPackage.REGULAR_EVENT__HARD_LOCAL_DEADLINE:
				getHard_Local_Deadline().clear();
				return;
			case ModelPackage.REGULAR_EVENT__SOFT_LOCAL_DEADLINE:
				getSoft_Local_Deadline().clear();
				return;
			case ModelPackage.REGULAR_EVENT__LOCAL_MAX_MISS_RATIO:
				getLocal_Max_Miss_Ratio().clear();
				return;
			case ModelPackage.REGULAR_EVENT__COMPOSITE_TIMING_REQUIREMENT:
				getComposite_Timing_Requirement().clear();
				return;
			case ModelPackage.REGULAR_EVENT__EVENT:
				setEvent(EVENT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.REGULAR_EVENT__GROUP:
				return group != null && !group.isEmpty();
			case ModelPackage.REGULAR_EVENT__MAX_OUTPUT_JITTER_REQ:
				return !getMax_Output_Jitter_Req().isEmpty();
			case ModelPackage.REGULAR_EVENT__HARD_GLOBAL_DEADLINE:
				return !getHard_Global_Deadline().isEmpty();
			case ModelPackage.REGULAR_EVENT__SOFT_GLOBAL_DEADLINE:
				return !getSoft_Global_Deadline().isEmpty();
			case ModelPackage.REGULAR_EVENT__GLOBAL_MAX_MISS_RATIO:
				return !getGlobal_Max_Miss_Ratio().isEmpty();
			case ModelPackage.REGULAR_EVENT__HARD_LOCAL_DEADLINE:
				return !getHard_Local_Deadline().isEmpty();
			case ModelPackage.REGULAR_EVENT__SOFT_LOCAL_DEADLINE:
				return !getSoft_Local_Deadline().isEmpty();
			case ModelPackage.REGULAR_EVENT__LOCAL_MAX_MISS_RATIO:
				return !getLocal_Max_Miss_Ratio().isEmpty();
			case ModelPackage.REGULAR_EVENT__COMPOSITE_TIMING_REQUIREMENT:
				return !getComposite_Timing_Requirement().isEmpty();
			case ModelPackage.REGULAR_EVENT__EVENT:
				return EVENT_EDEFAULT == null ? event != null : !EVENT_EDEFAULT.equals(event);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(", Event: ");
		result.append(event);
		result.append(')');
		return result.toString();
	}

} //Regular_EventImpl
