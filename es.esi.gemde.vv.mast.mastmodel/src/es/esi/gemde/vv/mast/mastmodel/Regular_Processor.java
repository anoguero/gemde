/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Regular Processor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getTicker_System_Timer <em>Ticker System Timer</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getAlarm_Clock_System_Timer <em>Alarm Clock System Timer</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getAvg_ISR_Switch <em>Avg ISR Switch</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getBest_ISR_Switch <em>Best ISR Switch</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getMax_Interrupt_Priority <em>Max Interrupt Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getMin_Interrupt_Priority <em>Min Interrupt Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getName <em>Name</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getSpeed_Factor <em>Speed Factor</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getWorst_ISR_Switch <em>Worst ISR Switch</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Processor()
 * @model extendedMetaData="name='Regular_Processor' kind='elementOnly'"
 * @generated
 */
public interface Regular_Processor extends EObject {
	/**
	 * Returns the value of the '<em><b>Ticker System Timer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ticker System Timer</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ticker System Timer</em>' containment reference.
	 * @see #setTicker_System_Timer(Ticker_System_Timer)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Processor_Ticker_System_Timer()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Ticker_System_Timer' namespace='##targetNamespace'"
	 * @generated
	 */
	Ticker_System_Timer getTicker_System_Timer();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getTicker_System_Timer <em>Ticker System Timer</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ticker System Timer</em>' containment reference.
	 * @see #getTicker_System_Timer()
	 * @generated
	 */
	void setTicker_System_Timer(Ticker_System_Timer value);

	/**
	 * Returns the value of the '<em><b>Alarm Clock System Timer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alarm Clock System Timer</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alarm Clock System Timer</em>' containment reference.
	 * @see #setAlarm_Clock_System_Timer(Alarm_Clock_System_Timer)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Processor_Alarm_Clock_System_Timer()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Alarm_Clock_System_Timer' namespace='##targetNamespace'"
	 * @generated
	 */
	Alarm_Clock_System_Timer getAlarm_Clock_System_Timer();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getAlarm_Clock_System_Timer <em>Alarm Clock System Timer</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alarm Clock System Timer</em>' containment reference.
	 * @see #getAlarm_Clock_System_Timer()
	 * @generated
	 */
	void setAlarm_Clock_System_Timer(Alarm_Clock_System_Timer value);

	/**
	 * Returns the value of the '<em><b>Avg ISR Switch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Avg ISR Switch</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Avg ISR Switch</em>' attribute.
	 * @see #setAvg_ISR_Switch(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Processor_Avg_ISR_Switch()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Avg_ISR_Switch'"
	 * @generated
	 */
	double getAvg_ISR_Switch();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getAvg_ISR_Switch <em>Avg ISR Switch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Avg ISR Switch</em>' attribute.
	 * @see #getAvg_ISR_Switch()
	 * @generated
	 */
	void setAvg_ISR_Switch(double value);

	/**
	 * Returns the value of the '<em><b>Best ISR Switch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Best ISR Switch</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Best ISR Switch</em>' attribute.
	 * @see #setBest_ISR_Switch(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Processor_Best_ISR_Switch()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Best_ISR_Switch'"
	 * @generated
	 */
	double getBest_ISR_Switch();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getBest_ISR_Switch <em>Best ISR Switch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Best ISR Switch</em>' attribute.
	 * @see #getBest_ISR_Switch()
	 * @generated
	 */
	void setBest_ISR_Switch(double value);

	/**
	 * Returns the value of the '<em><b>Max Interrupt Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Interrupt Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Interrupt Priority</em>' attribute.
	 * @see #setMax_Interrupt_Priority(int)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Processor_Max_Interrupt_Priority()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Interrupt_Priority"
	 *        extendedMetaData="kind='attribute' name='Max_Interrupt_Priority'"
	 * @generated
	 */
	int getMax_Interrupt_Priority();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getMax_Interrupt_Priority <em>Max Interrupt Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Interrupt Priority</em>' attribute.
	 * @see #getMax_Interrupt_Priority()
	 * @generated
	 */
	void setMax_Interrupt_Priority(int value);

	/**
	 * Returns the value of the '<em><b>Min Interrupt Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Interrupt Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Interrupt Priority</em>' attribute.
	 * @see #setMin_Interrupt_Priority(int)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Processor_Min_Interrupt_Priority()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Interrupt_Priority"
	 *        extendedMetaData="kind='attribute' name='Min_Interrupt_Priority'"
	 * @generated
	 */
	int getMin_Interrupt_Priority();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getMin_Interrupt_Priority <em>Min Interrupt Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Interrupt Priority</em>' attribute.
	 * @see #getMin_Interrupt_Priority()
	 * @generated
	 */
	void setMin_Interrupt_Priority(int value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Processor_Name()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier" required="true"
	 *        extendedMetaData="kind='attribute' name='Name'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Speed Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Speed Factor</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Speed Factor</em>' attribute.
	 * @see #setSpeed_Factor(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Processor_Speed_Factor()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Float"
	 *        extendedMetaData="kind='attribute' name='Speed_Factor'"
	 * @generated
	 */
	double getSpeed_Factor();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getSpeed_Factor <em>Speed Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Speed Factor</em>' attribute.
	 * @see #getSpeed_Factor()
	 * @generated
	 */
	void setSpeed_Factor(double value);

	/**
	 * Returns the value of the '<em><b>Worst ISR Switch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Worst ISR Switch</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Worst ISR Switch</em>' attribute.
	 * @see #setWorst_ISR_Switch(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getRegular_Processor_Worst_ISR_Switch()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Worst_ISR_Switch'"
	 * @generated
	 */
	double getWorst_ISR_Switch();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getWorst_ISR_Switch <em>Worst ISR Switch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Worst ISR Switch</em>' attribute.
	 * @see #getWorst_ISR_Switch()
	 * @generated
	 */
	void setWorst_ISR_Switch(double value);

} // Regular_Processor
