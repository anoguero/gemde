/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import java.math.BigInteger;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Bursty External Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getAvg_Interarrival <em>Avg Interarrival</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getBound_Interval <em>Bound Interval</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getDistribution <em>Distribution</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getMax_Arrivals <em>Max Arrivals</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getBursty_External_Event()
 * @model extendedMetaData="name='Bursty_External_Event' kind='empty'"
 * @generated
 */
public interface Bursty_External_Event extends EObject {
	/**
	 * Returns the value of the '<em><b>Avg Interarrival</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Avg Interarrival</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Avg Interarrival</em>' attribute.
	 * @see #setAvg_Interarrival(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getBursty_External_Event_Avg_Interarrival()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Time"
	 *        extendedMetaData="kind='attribute' name='Avg_Interarrival'"
	 * @generated
	 */
	double getAvg_Interarrival();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getAvg_Interarrival <em>Avg Interarrival</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Avg Interarrival</em>' attribute.
	 * @see #getAvg_Interarrival()
	 * @generated
	 */
	void setAvg_Interarrival(double value);

	/**
	 * Returns the value of the '<em><b>Bound Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bound Interval</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bound Interval</em>' attribute.
	 * @see #setBound_Interval(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getBursty_External_Event_Bound_Interval()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Time"
	 *        extendedMetaData="kind='attribute' name='Bound_Interval'"
	 * @generated
	 */
	double getBound_Interval();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getBound_Interval <em>Bound Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bound Interval</em>' attribute.
	 * @see #getBound_Interval()
	 * @generated
	 */
	void setBound_Interval(double value);

	/**
	 * Returns the value of the '<em><b>Distribution</b></em>' attribute.
	 * The literals are from the enumeration {@link es.esi.gemde.vv.mast.mastmodel.Distribution_Type}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Distribution</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Distribution</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Distribution_Type
	 * @see #isSetDistribution()
	 * @see #unsetDistribution()
	 * @see #setDistribution(Distribution_Type)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getBursty_External_Event_Distribution()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='Distribution'"
	 * @generated
	 */
	Distribution_Type getDistribution();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getDistribution <em>Distribution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Distribution</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Distribution_Type
	 * @see #isSetDistribution()
	 * @see #unsetDistribution()
	 * @see #getDistribution()
	 * @generated
	 */
	void setDistribution(Distribution_Type value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getDistribution <em>Distribution</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDistribution()
	 * @see #getDistribution()
	 * @see #setDistribution(Distribution_Type)
	 * @generated
	 */
	void unsetDistribution();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getDistribution <em>Distribution</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Distribution</em>' attribute is set.
	 * @see #unsetDistribution()
	 * @see #getDistribution()
	 * @see #setDistribution(Distribution_Type)
	 * @generated
	 */
	boolean isSetDistribution();

	/**
	 * Returns the value of the '<em><b>Max Arrivals</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Arrivals</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Arrivals</em>' attribute.
	 * @see #setMax_Arrivals(BigInteger)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getBursty_External_Event_Max_Arrivals()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.PositiveInteger"
	 *        extendedMetaData="kind='attribute' name='Max_Arrivals'"
	 * @generated
	 */
	BigInteger getMax_Arrivals();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getMax_Arrivals <em>Max Arrivals</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Arrivals</em>' attribute.
	 * @see #getMax_Arrivals()
	 * @generated
	 */
	void setMax_Arrivals(BigInteger value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getBursty_External_Event_Name()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier" required="true"
	 *        extendedMetaData="kind='attribute' name='Name'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Bursty_External_Event
