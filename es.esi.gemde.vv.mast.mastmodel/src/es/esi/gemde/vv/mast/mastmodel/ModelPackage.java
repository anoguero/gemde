/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see es.esi.gemde.vv.mast.mastmodel.ModelFactory
 * @model kind="package"
 * @generated
 */
public interface ModelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mastmodel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://mast.unican.es/xmlmast/model";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mast_mdl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ModelPackage eINSTANCE = es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl.init();

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.ActivityImpl <em>Activity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ActivityImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getActivity()
	 * @generated
	 */
	int ACTIVITY = 0;

	/**
	 * The feature id for the '<em><b>Activity Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__ACTIVITY_OPERATION = 0;

	/**
	 * The feature id for the '<em><b>Activity Server</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__ACTIVITY_SERVER = 1;

	/**
	 * The feature id for the '<em><b>Input Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__INPUT_EVENT = 2;

	/**
	 * The feature id for the '<em><b>Output Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY__OUTPUT_EVENT = 3;

	/**
	 * The number of structural features of the '<em>Activity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVITY_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Alarm_Clock_System_TimerImpl <em>Alarm Clock System Timer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Alarm_Clock_System_TimerImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getAlarm_Clock_System_Timer()
	 * @generated
	 */
	int ALARM_CLOCK_SYSTEM_TIMER = 1;

	/**
	 * The feature id for the '<em><b>Avg Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_CLOCK_SYSTEM_TIMER__AVG_OVERHEAD = 0;

	/**
	 * The feature id for the '<em><b>Best Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_CLOCK_SYSTEM_TIMER__BEST_OVERHEAD = 1;

	/**
	 * The feature id for the '<em><b>Worst Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_CLOCK_SYSTEM_TIMER__WORST_OVERHEAD = 2;

	/**
	 * The number of structural features of the '<em>Alarm Clock System Timer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALARM_CLOCK_SYSTEM_TIMER_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.BarrierImpl <em>Barrier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.BarrierImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getBarrier()
	 * @generated
	 */
	int BARRIER = 2;

	/**
	 * The feature id for the '<em><b>Input Events List</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BARRIER__INPUT_EVENTS_LIST = 0;

	/**
	 * The feature id for the '<em><b>Output Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BARRIER__OUTPUT_EVENT = 1;

	/**
	 * The number of structural features of the '<em>Barrier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BARRIER_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Bursty_External_EventImpl <em>Bursty External Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Bursty_External_EventImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getBursty_External_Event()
	 * @generated
	 */
	int BURSTY_EXTERNAL_EVENT = 3;

	/**
	 * The feature id for the '<em><b>Avg Interarrival</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BURSTY_EXTERNAL_EVENT__AVG_INTERARRIVAL = 0;

	/**
	 * The feature id for the '<em><b>Bound Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BURSTY_EXTERNAL_EVENT__BOUND_INTERVAL = 1;

	/**
	 * The feature id for the '<em><b>Distribution</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BURSTY_EXTERNAL_EVENT__DISTRIBUTION = 2;

	/**
	 * The feature id for the '<em><b>Max Arrivals</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BURSTY_EXTERNAL_EVENT__MAX_ARRIVALS = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BURSTY_EXTERNAL_EVENT__NAME = 4;

	/**
	 * The number of structural features of the '<em>Bursty External Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BURSTY_EXTERNAL_EVENT_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Character_Packet_DriverImpl <em>Character Packet Driver</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Character_Packet_DriverImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getCharacter_Packet_Driver()
	 * @generated
	 */
	int CHARACTER_PACKET_DRIVER = 4;

	/**
	 * The feature id for the '<em><b>Character Receive Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER_PACKET_DRIVER__CHARACTER_RECEIVE_OPERATION = 0;

	/**
	 * The feature id for the '<em><b>Character Send Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER_PACKET_DRIVER__CHARACTER_SEND_OPERATION = 1;

	/**
	 * The feature id for the '<em><b>Character Server</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER_PACKET_DRIVER__CHARACTER_SERVER = 2;

	/**
	 * The feature id for the '<em><b>Character Transmission Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER_PACKET_DRIVER__CHARACTER_TRANSMISSION_TIME = 3;

	/**
	 * The feature id for the '<em><b>Message Partitioning</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER_PACKET_DRIVER__MESSAGE_PARTITIONING = 4;

	/**
	 * The feature id for the '<em><b>Packet Receive Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER_PACKET_DRIVER__PACKET_RECEIVE_OPERATION = 5;

	/**
	 * The feature id for the '<em><b>Packet Send Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER_PACKET_DRIVER__PACKET_SEND_OPERATION = 6;

	/**
	 * The feature id for the '<em><b>Packet Server</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER_PACKET_DRIVER__PACKET_SERVER = 7;

	/**
	 * The feature id for the '<em><b>RTA Overhead Model</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER_PACKET_DRIVER__RTA_OVERHEAD_MODEL = 8;

	/**
	 * The number of structural features of the '<em>Character Packet Driver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER_PACKET_DRIVER_FEATURE_COUNT = 9;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Composite_OperationImpl <em>Composite Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Composite_OperationImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getComposite_Operation()
	 * @generated
	 */
	int COMPOSITE_OPERATION = 5;

	/**
	 * The feature id for the '<em><b>Overridden Fixed Priority</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_OPERATION__OVERRIDDEN_FIXED_PRIORITY = 0;

	/**
	 * The feature id for the '<em><b>Overridden Permanent FP</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_OPERATION__OVERRIDDEN_PERMANENT_FP = 1;

	/**
	 * The feature id for the '<em><b>Operation List</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_OPERATION__OPERATION_LIST = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_OPERATION__NAME = 3;

	/**
	 * The number of structural features of the '<em>Composite Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_OPERATION_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Composite_Timing_RequirementImpl <em>Composite Timing Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Composite_Timing_RequirementImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getComposite_Timing_Requirement()
	 * @generated
	 */
	int COMPOSITE_TIMING_REQUIREMENT = 6;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_TIMING_REQUIREMENT__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Max Output Jitter Req</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_TIMING_REQUIREMENT__MAX_OUTPUT_JITTER_REQ = 1;

	/**
	 * The feature id for the '<em><b>Hard Global Deadline</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_TIMING_REQUIREMENT__HARD_GLOBAL_DEADLINE = 2;

	/**
	 * The feature id for the '<em><b>Soft Global Deadline</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_TIMING_REQUIREMENT__SOFT_GLOBAL_DEADLINE = 3;

	/**
	 * The feature id for the '<em><b>Global Max Miss Ratio</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_TIMING_REQUIREMENT__GLOBAL_MAX_MISS_RATIO = 4;

	/**
	 * The feature id for the '<em><b>Hard Local Deadline</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_TIMING_REQUIREMENT__HARD_LOCAL_DEADLINE = 5;

	/**
	 * The feature id for the '<em><b>Soft Local Deadline</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_TIMING_REQUIREMENT__SOFT_LOCAL_DEADLINE = 6;

	/**
	 * The feature id for the '<em><b>Local Max Miss Ratio</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_TIMING_REQUIREMENT__LOCAL_MAX_MISS_RATIO = 7;

	/**
	 * The number of structural features of the '<em>Composite Timing Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_TIMING_REQUIREMENT_FEATURE_COUNT = 8;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.ConcentratorImpl <em>Concentrator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ConcentratorImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getConcentrator()
	 * @generated
	 */
	int CONCENTRATOR = 7;

	/**
	 * The feature id for the '<em><b>Input Events List</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCENTRATOR__INPUT_EVENTS_LIST = 0;

	/**
	 * The feature id for the '<em><b>Output Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCENTRATOR__OUTPUT_EVENT = 1;

	/**
	 * The number of structural features of the '<em>Concentrator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONCENTRATOR_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.DelayImpl <em>Delay</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.DelayImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getDelay()
	 * @generated
	 */
	int DELAY = 8;

	/**
	 * The feature id for the '<em><b>Delay Max Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAY__DELAY_MAX_INTERVAL = 0;

	/**
	 * The feature id for the '<em><b>Delay Min Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAY__DELAY_MIN_INTERVAL = 1;

	/**
	 * The feature id for the '<em><b>Input Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAY__INPUT_EVENT = 2;

	/**
	 * The feature id for the '<em><b>Output Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAY__OUTPUT_EVENT = 3;

	/**
	 * The number of structural features of the '<em>Delay</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELAY_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Delivery_ServerImpl <em>Delivery Server</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Delivery_ServerImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getDelivery_Server()
	 * @generated
	 */
	int DELIVERY_SERVER = 9;

	/**
	 * The feature id for the '<em><b>Delivery Policy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELIVERY_SERVER__DELIVERY_POLICY = 0;

	/**
	 * The feature id for the '<em><b>Input Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELIVERY_SERVER__INPUT_EVENT = 1;

	/**
	 * The feature id for the '<em><b>Output Events List</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELIVERY_SERVER__OUTPUT_EVENTS_LIST = 2;

	/**
	 * The number of structural features of the '<em>Delivery Server</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DELIVERY_SERVER_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Document_RootImpl <em>Document Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Document_RootImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getDocument_Root()
	 * @generated
	 */
	int DOCUMENT_ROOT = 10;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__MIXED = 0;

	/**
	 * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

	/**
	 * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

	/**
	 * The feature id for the '<em><b>MAST MODEL</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__MAST_MODEL = 3;

	/**
	 * The number of structural features of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.EDF_PolicyImpl <em>EDF Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.EDF_PolicyImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getEDF_Policy()
	 * @generated
	 */
	int EDF_POLICY = 11;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDF_POLICY__DEADLINE = 0;

	/**
	 * The feature id for the '<em><b>Preassigned</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDF_POLICY__PREASSIGNED = 1;

	/**
	 * The number of structural features of the '<em>EDF Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDF_POLICY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.EDF_SchedulerImpl <em>EDF Scheduler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.EDF_SchedulerImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getEDF_Scheduler()
	 * @generated
	 */
	int EDF_SCHEDULER = 12;

	/**
	 * The feature id for the '<em><b>Avg Context Switch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDF_SCHEDULER__AVG_CONTEXT_SWITCH = 0;

	/**
	 * The feature id for the '<em><b>Best Context Switch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDF_SCHEDULER__BEST_CONTEXT_SWITCH = 1;

	/**
	 * The feature id for the '<em><b>Worst Context Switch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDF_SCHEDULER__WORST_CONTEXT_SWITCH = 2;

	/**
	 * The number of structural features of the '<em>EDF Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDF_SCHEDULER_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Enclosing_OperationImpl <em>Enclosing Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Enclosing_OperationImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getEnclosing_Operation()
	 * @generated
	 */
	int ENCLOSING_OPERATION = 13;

	/**
	 * The feature id for the '<em><b>Overridden Fixed Priority</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENCLOSING_OPERATION__OVERRIDDEN_FIXED_PRIORITY = 0;

	/**
	 * The feature id for the '<em><b>Overridden Permanent FP</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENCLOSING_OPERATION__OVERRIDDEN_PERMANENT_FP = 1;

	/**
	 * The feature id for the '<em><b>Operation List</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENCLOSING_OPERATION__OPERATION_LIST = 2;

	/**
	 * The feature id for the '<em><b>Average Case Execution Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENCLOSING_OPERATION__AVERAGE_CASE_EXECUTION_TIME = 3;

	/**
	 * The feature id for the '<em><b>Best Case Execution Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENCLOSING_OPERATION__BEST_CASE_EXECUTION_TIME = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENCLOSING_OPERATION__NAME = 5;

	/**
	 * The feature id for the '<em><b>Worst Case Execution Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENCLOSING_OPERATION__WORST_CASE_EXECUTION_TIME = 6;

	/**
	 * The number of structural features of the '<em>Enclosing Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENCLOSING_OPERATION_FEATURE_COUNT = 7;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Fixed_Priority_PolicyImpl <em>Fixed Priority Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Fixed_Priority_PolicyImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getFixed_Priority_Policy()
	 * @generated
	 */
	int FIXED_PRIORITY_POLICY = 14;

	/**
	 * The feature id for the '<em><b>Preassigned</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_PRIORITY_POLICY__PREASSIGNED = 0;

	/**
	 * The feature id for the '<em><b>The Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_PRIORITY_POLICY__THE_PRIORITY = 1;

	/**
	 * The number of structural features of the '<em>Fixed Priority Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_PRIORITY_POLICY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Fixed_Priority_SchedulerImpl <em>Fixed Priority Scheduler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Fixed_Priority_SchedulerImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getFixed_Priority_Scheduler()
	 * @generated
	 */
	int FIXED_PRIORITY_SCHEDULER = 15;

	/**
	 * The feature id for the '<em><b>Avg Context Switch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_PRIORITY_SCHEDULER__AVG_CONTEXT_SWITCH = 0;

	/**
	 * The feature id for the '<em><b>Best Context Switch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_PRIORITY_SCHEDULER__BEST_CONTEXT_SWITCH = 1;

	/**
	 * The feature id for the '<em><b>Max Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_PRIORITY_SCHEDULER__MAX_PRIORITY = 2;

	/**
	 * The feature id for the '<em><b>Min Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_PRIORITY_SCHEDULER__MIN_PRIORITY = 3;

	/**
	 * The feature id for the '<em><b>Worst Context Switch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_PRIORITY_SCHEDULER__WORST_CONTEXT_SWITCH = 4;

	/**
	 * The number of structural features of the '<em>Fixed Priority Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_PRIORITY_SCHEDULER_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.FP_Packet_Based_SchedulerImpl <em>FP Packet Based Scheduler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.FP_Packet_Based_SchedulerImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getFP_Packet_Based_Scheduler()
	 * @generated
	 */
	int FP_PACKET_BASED_SCHEDULER = 16;

	/**
	 * The feature id for the '<em><b>Max Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FP_PACKET_BASED_SCHEDULER__MAX_PRIORITY = 0;

	/**
	 * The feature id for the '<em><b>Min Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FP_PACKET_BASED_SCHEDULER__MIN_PRIORITY = 1;

	/**
	 * The feature id for the '<em><b>Packet Overhead Avg Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_AVG_SIZE = 2;

	/**
	 * The feature id for the '<em><b>Packet Overhead Max Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_MAX_SIZE = 3;

	/**
	 * The feature id for the '<em><b>Packet Overhead Min Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_MIN_SIZE = 4;

	/**
	 * The number of structural features of the '<em>FP Packet Based Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FP_PACKET_BASED_SCHEDULER_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Global_Max_Miss_RatioImpl <em>Global Max Miss Ratio</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Global_Max_Miss_RatioImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getGlobal_Max_Miss_Ratio()
	 * @generated
	 */
	int GLOBAL_MAX_MISS_RATIO = 17;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLOBAL_MAX_MISS_RATIO__DEADLINE = 0;

	/**
	 * The feature id for the '<em><b>Ratio</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLOBAL_MAX_MISS_RATIO__RATIO = 1;

	/**
	 * The feature id for the '<em><b>Referenced Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLOBAL_MAX_MISS_RATIO__REFERENCED_EVENT = 2;

	/**
	 * The number of structural features of the '<em>Global Max Miss Ratio</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLOBAL_MAX_MISS_RATIO_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Hard_Global_DeadlineImpl <em>Hard Global Deadline</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Hard_Global_DeadlineImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getHard_Global_Deadline()
	 * @generated
	 */
	int HARD_GLOBAL_DEADLINE = 18;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HARD_GLOBAL_DEADLINE__DEADLINE = 0;

	/**
	 * The feature id for the '<em><b>Referenced Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HARD_GLOBAL_DEADLINE__REFERENCED_EVENT = 1;

	/**
	 * The number of structural features of the '<em>Hard Global Deadline</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HARD_GLOBAL_DEADLINE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Hard_Local_DeadlineImpl <em>Hard Local Deadline</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Hard_Local_DeadlineImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getHard_Local_Deadline()
	 * @generated
	 */
	int HARD_LOCAL_DEADLINE = 19;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HARD_LOCAL_DEADLINE__DEADLINE = 0;

	/**
	 * The number of structural features of the '<em>Hard Local Deadline</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HARD_LOCAL_DEADLINE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Immediate_Ceiling_ResourceImpl <em>Immediate Ceiling Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Immediate_Ceiling_ResourceImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getImmediate_Ceiling_Resource()
	 * @generated
	 */
	int IMMEDIATE_CEILING_RESOURCE = 20;

	/**
	 * The feature id for the '<em><b>Ceiling</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMMEDIATE_CEILING_RESOURCE__CEILING = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMMEDIATE_CEILING_RESOURCE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Preassigned</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMMEDIATE_CEILING_RESOURCE__PREASSIGNED = 2;

	/**
	 * The number of structural features of the '<em>Immediate Ceiling Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMMEDIATE_CEILING_RESOURCE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Interrupt_FP_PolicyImpl <em>Interrupt FP Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Interrupt_FP_PolicyImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getInterrupt_FP_Policy()
	 * @generated
	 */
	int INTERRUPT_FP_POLICY = 21;

	/**
	 * The feature id for the '<em><b>Preassigned</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERRUPT_FP_POLICY__PREASSIGNED = 0;

	/**
	 * The feature id for the '<em><b>The Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERRUPT_FP_POLICY__THE_PRIORITY = 1;

	/**
	 * The number of structural features of the '<em>Interrupt FP Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERRUPT_FP_POLICY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.List_of_DriversImpl <em>List of Drivers</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.List_of_DriversImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getList_of_Drivers()
	 * @generated
	 */
	int LIST_OF_DRIVERS = 22;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_OF_DRIVERS__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Packet Driver</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_OF_DRIVERS__PACKET_DRIVER = 1;

	/**
	 * The feature id for the '<em><b>Character Packet Driver</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_OF_DRIVERS__CHARACTER_PACKET_DRIVER = 2;

	/**
	 * The feature id for the '<em><b>RTEP Packet Driver</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_OF_DRIVERS__RTEP_PACKET_DRIVER = 3;

	/**
	 * The number of structural features of the '<em>List of Drivers</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_OF_DRIVERS_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Local_Max_Miss_RatioImpl <em>Local Max Miss Ratio</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Local_Max_Miss_RatioImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getLocal_Max_Miss_Ratio()
	 * @generated
	 */
	int LOCAL_MAX_MISS_RATIO = 23;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_MAX_MISS_RATIO__DEADLINE = 0;

	/**
	 * The feature id for the '<em><b>Ratio</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_MAX_MISS_RATIO__RATIO = 1;

	/**
	 * The number of structural features of the '<em>Local Max Miss Ratio</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_MAX_MISS_RATIO_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl <em>MAST MODEL</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getMAST_MODEL()
	 * @generated
	 */
	int MAST_MODEL = 24;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAST_MODEL__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Regular Processor</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAST_MODEL__REGULAR_PROCESSOR = 1;

	/**
	 * The feature id for the '<em><b>Packet Based Network</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAST_MODEL__PACKET_BASED_NETWORK = 2;

	/**
	 * The feature id for the '<em><b>Regular Scheduling Server</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAST_MODEL__REGULAR_SCHEDULING_SERVER = 3;

	/**
	 * The feature id for the '<em><b>Immediate Ceiling Resource</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAST_MODEL__IMMEDIATE_CEILING_RESOURCE = 4;

	/**
	 * The feature id for the '<em><b>Priority Inheritance Resource</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAST_MODEL__PRIORITY_INHERITANCE_RESOURCE = 5;

	/**
	 * The feature id for the '<em><b>SRP Resource</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAST_MODEL__SRP_RESOURCE = 6;

	/**
	 * The feature id for the '<em><b>Simple Operation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAST_MODEL__SIMPLE_OPERATION = 7;

	/**
	 * The feature id for the '<em><b>Message Transmission</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAST_MODEL__MESSAGE_TRANSMISSION = 8;

	/**
	 * The feature id for the '<em><b>Composite Operation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAST_MODEL__COMPOSITE_OPERATION = 9;

	/**
	 * The feature id for the '<em><b>Enclosing Operation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAST_MODEL__ENCLOSING_OPERATION = 10;

	/**
	 * The feature id for the '<em><b>Regular Transaction</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAST_MODEL__REGULAR_TRANSACTION = 11;

	/**
	 * The feature id for the '<em><b>Primary Scheduler</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAST_MODEL__PRIMARY_SCHEDULER = 12;

	/**
	 * The feature id for the '<em><b>Secondary Scheduler</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAST_MODEL__SECONDARY_SCHEDULER = 13;

	/**
	 * The feature id for the '<em><b>Model Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAST_MODEL__MODEL_DATE = 14;

	/**
	 * The feature id for the '<em><b>Model Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAST_MODEL__MODEL_NAME = 15;

	/**
	 * The feature id for the '<em><b>System Pi PBehaviour</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAST_MODEL__SYSTEM_PI_PBEHAVIOUR = 16;

	/**
	 * The number of structural features of the '<em>MAST MODEL</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAST_MODEL_FEATURE_COUNT = 17;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Max_Output_Jitter_ReqImpl <em>Max Output Jitter Req</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Max_Output_Jitter_ReqImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getMax_Output_Jitter_Req()
	 * @generated
	 */
	int MAX_OUTPUT_JITTER_REQ = 25;

	/**
	 * The feature id for the '<em><b>Max Output Jitter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAX_OUTPUT_JITTER_REQ__MAX_OUTPUT_JITTER = 0;

	/**
	 * The feature id for the '<em><b>Referenced Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAX_OUTPUT_JITTER_REQ__REFERENCED_EVENT = 1;

	/**
	 * The number of structural features of the '<em>Max Output Jitter Req</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAX_OUTPUT_JITTER_REQ_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Message_TransmissionImpl <em>Message Transmission</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Message_TransmissionImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getMessage_Transmission()
	 * @generated
	 */
	int MESSAGE_TRANSMISSION = 26;

	/**
	 * The feature id for the '<em><b>Overridden Fixed Priority</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_TRANSMISSION__OVERRIDDEN_FIXED_PRIORITY = 0;

	/**
	 * The feature id for the '<em><b>Overridden Permanent FP</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_TRANSMISSION__OVERRIDDEN_PERMANENT_FP = 1;

	/**
	 * The feature id for the '<em><b>Avg Message Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_TRANSMISSION__AVG_MESSAGE_SIZE = 2;

	/**
	 * The feature id for the '<em><b>Max Message Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_TRANSMISSION__MAX_MESSAGE_SIZE = 3;

	/**
	 * The feature id for the '<em><b>Min Message Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_TRANSMISSION__MIN_MESSAGE_SIZE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_TRANSMISSION__NAME = 5;

	/**
	 * The number of structural features of the '<em>Message Transmission</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MESSAGE_TRANSMISSION_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.MulticastImpl <em>Multicast</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.MulticastImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getMulticast()
	 * @generated
	 */
	int MULTICAST = 27;

	/**
	 * The feature id for the '<em><b>Input Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTICAST__INPUT_EVENT = 0;

	/**
	 * The feature id for the '<em><b>Output Events List</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTICAST__OUTPUT_EVENTS_LIST = 1;

	/**
	 * The number of structural features of the '<em>Multicast</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTICAST_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Non_Preemptible_FP_PolicyImpl <em>Non Preemptible FP Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Non_Preemptible_FP_PolicyImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getNon_Preemptible_FP_Policy()
	 * @generated
	 */
	int NON_PREEMPTIBLE_FP_POLICY = 28;

	/**
	 * The feature id for the '<em><b>Preassigned</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_PREEMPTIBLE_FP_POLICY__PREASSIGNED = 0;

	/**
	 * The feature id for the '<em><b>The Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_PREEMPTIBLE_FP_POLICY__THE_PRIORITY = 1;

	/**
	 * The number of structural features of the '<em>Non Preemptible FP Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_PREEMPTIBLE_FP_POLICY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.OffsetImpl <em>Offset</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.OffsetImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getOffset()
	 * @generated
	 */
	int OFFSET = 29;

	/**
	 * The feature id for the '<em><b>Delay Max Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OFFSET__DELAY_MAX_INTERVAL = 0;

	/**
	 * The feature id for the '<em><b>Delay Min Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OFFSET__DELAY_MIN_INTERVAL = 1;

	/**
	 * The feature id for the '<em><b>Input Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OFFSET__INPUT_EVENT = 2;

	/**
	 * The feature id for the '<em><b>Output Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OFFSET__OUTPUT_EVENT = 3;

	/**
	 * The feature id for the '<em><b>Referenced Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OFFSET__REFERENCED_EVENT = 4;

	/**
	 * The number of structural features of the '<em>Offset</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OFFSET_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Overridden_Fixed_PriorityImpl <em>Overridden Fixed Priority</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Overridden_Fixed_PriorityImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getOverridden_Fixed_Priority()
	 * @generated
	 */
	int OVERRIDDEN_FIXED_PRIORITY = 30;

	/**
	 * The feature id for the '<em><b>The Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OVERRIDDEN_FIXED_PRIORITY__THE_PRIORITY = 0;

	/**
	 * The number of structural features of the '<em>Overridden Fixed Priority</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OVERRIDDEN_FIXED_PRIORITY_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Overridden_Permanent_FPImpl <em>Overridden Permanent FP</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Overridden_Permanent_FPImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getOverridden_Permanent_FP()
	 * @generated
	 */
	int OVERRIDDEN_PERMANENT_FP = 31;

	/**
	 * The feature id for the '<em><b>The Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OVERRIDDEN_PERMANENT_FP__THE_PRIORITY = 0;

	/**
	 * The number of structural features of the '<em>Overridden Permanent FP</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OVERRIDDEN_PERMANENT_FP_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Packet_Based_NetworkImpl <em>Packet Based Network</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Packet_Based_NetworkImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPacket_Based_Network()
	 * @generated
	 */
	int PACKET_BASED_NETWORK = 32;

	/**
	 * The feature id for the '<em><b>List of Drivers</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKET_BASED_NETWORK__LIST_OF_DRIVERS = 0;

	/**
	 * The feature id for the '<em><b>Max Blocking</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKET_BASED_NETWORK__MAX_BLOCKING = 1;

	/**
	 * The feature id for the '<em><b>Max Packet Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKET_BASED_NETWORK__MAX_PACKET_SIZE = 2;

	/**
	 * The feature id for the '<em><b>Min Packet Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKET_BASED_NETWORK__MIN_PACKET_SIZE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKET_BASED_NETWORK__NAME = 4;

	/**
	 * The feature id for the '<em><b>Speed Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKET_BASED_NETWORK__SPEED_FACTOR = 5;

	/**
	 * The feature id for the '<em><b>Throughput</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKET_BASED_NETWORK__THROUGHPUT = 6;

	/**
	 * The feature id for the '<em><b>Transmission</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKET_BASED_NETWORK__TRANSMISSION = 7;

	/**
	 * The number of structural features of the '<em>Packet Based Network</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKET_BASED_NETWORK_FEATURE_COUNT = 8;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Packet_DriverImpl <em>Packet Driver</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Packet_DriverImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPacket_Driver()
	 * @generated
	 */
	int PACKET_DRIVER = 33;

	/**
	 * The feature id for the '<em><b>Message Partitioning</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKET_DRIVER__MESSAGE_PARTITIONING = 0;

	/**
	 * The feature id for the '<em><b>Packet Receive Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKET_DRIVER__PACKET_RECEIVE_OPERATION = 1;

	/**
	 * The feature id for the '<em><b>Packet Send Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKET_DRIVER__PACKET_SEND_OPERATION = 2;

	/**
	 * The feature id for the '<em><b>Packet Server</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKET_DRIVER__PACKET_SERVER = 3;

	/**
	 * The feature id for the '<em><b>RTA Overhead Model</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKET_DRIVER__RTA_OVERHEAD_MODEL = 4;

	/**
	 * The number of structural features of the '<em>Packet Driver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKET_DRIVER_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Periodic_External_EventImpl <em>Periodic External Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Periodic_External_EventImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPeriodic_External_Event()
	 * @generated
	 */
	int PERIODIC_EXTERNAL_EVENT = 34;

	/**
	 * The feature id for the '<em><b>Max Jitter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERIODIC_EXTERNAL_EVENT__MAX_JITTER = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERIODIC_EXTERNAL_EVENT__NAME = 1;

	/**
	 * The feature id for the '<em><b>Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERIODIC_EXTERNAL_EVENT__PERIOD = 2;

	/**
	 * The feature id for the '<em><b>Phase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERIODIC_EXTERNAL_EVENT__PHASE = 3;

	/**
	 * The number of structural features of the '<em>Periodic External Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERIODIC_EXTERNAL_EVENT_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Polling_PolicyImpl <em>Polling Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Polling_PolicyImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPolling_Policy()
	 * @generated
	 */
	int POLLING_POLICY = 35;

	/**
	 * The feature id for the '<em><b>Polling Avg Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLLING_POLICY__POLLING_AVG_OVERHEAD = 0;

	/**
	 * The feature id for the '<em><b>Polling Best Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLLING_POLICY__POLLING_BEST_OVERHEAD = 1;

	/**
	 * The feature id for the '<em><b>Polling Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLLING_POLICY__POLLING_PERIOD = 2;

	/**
	 * The feature id for the '<em><b>Polling Worst Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLLING_POLICY__POLLING_WORST_OVERHEAD = 3;

	/**
	 * The feature id for the '<em><b>Preassigned</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLLING_POLICY__PREASSIGNED = 4;

	/**
	 * The feature id for the '<em><b>The Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLLING_POLICY__THE_PRIORITY = 5;

	/**
	 * The number of structural features of the '<em>Polling Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLLING_POLICY_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Primary_SchedulerImpl <em>Primary Scheduler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Primary_SchedulerImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPrimary_Scheduler()
	 * @generated
	 */
	int PRIMARY_SCHEDULER = 36;

	/**
	 * The feature id for the '<em><b>Fixed Priority Scheduler</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMARY_SCHEDULER__FIXED_PRIORITY_SCHEDULER = 0;

	/**
	 * The feature id for the '<em><b>EDF Scheduler</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMARY_SCHEDULER__EDF_SCHEDULER = 1;

	/**
	 * The feature id for the '<em><b>FP Packet Based Scheduler</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMARY_SCHEDULER__FP_PACKET_BASED_SCHEDULER = 2;

	/**
	 * The feature id for the '<em><b>Host</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMARY_SCHEDULER__HOST = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMARY_SCHEDULER__NAME = 4;

	/**
	 * The number of structural features of the '<em>Primary Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIMARY_SCHEDULER_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Priority_Inheritance_ResourceImpl <em>Priority Inheritance Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Priority_Inheritance_ResourceImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPriority_Inheritance_Resource()
	 * @generated
	 */
	int PRIORITY_INHERITANCE_RESOURCE = 37;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIORITY_INHERITANCE_RESOURCE__NAME = 0;

	/**
	 * The number of structural features of the '<em>Priority Inheritance Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIORITY_INHERITANCE_RESOURCE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Query_ServerImpl <em>Query Server</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Query_ServerImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getQuery_Server()
	 * @generated
	 */
	int QUERY_SERVER = 38;

	/**
	 * The feature id for the '<em><b>Input Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_SERVER__INPUT_EVENT = 0;

	/**
	 * The feature id for the '<em><b>Output Events List</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_SERVER__OUTPUT_EVENTS_LIST = 1;

	/**
	 * The feature id for the '<em><b>Request Policy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_SERVER__REQUEST_POLICY = 2;

	/**
	 * The number of structural features of the '<em>Query Server</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUERY_SERVER_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Rate_DivisorImpl <em>Rate Divisor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Rate_DivisorImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getRate_Divisor()
	 * @generated
	 */
	int RATE_DIVISOR = 39;

	/**
	 * The feature id for the '<em><b>Input Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RATE_DIVISOR__INPUT_EVENT = 0;

	/**
	 * The feature id for the '<em><b>Output Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RATE_DIVISOR__OUTPUT_EVENT = 1;

	/**
	 * The feature id for the '<em><b>Rate Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RATE_DIVISOR__RATE_FACTOR = 2;

	/**
	 * The number of structural features of the '<em>Rate Divisor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RATE_DIVISOR_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_EventImpl <em>Regular Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Regular_EventImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getRegular_Event()
	 * @generated
	 */
	int REGULAR_EVENT = 40;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_EVENT__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Max Output Jitter Req</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_EVENT__MAX_OUTPUT_JITTER_REQ = 1;

	/**
	 * The feature id for the '<em><b>Hard Global Deadline</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_EVENT__HARD_GLOBAL_DEADLINE = 2;

	/**
	 * The feature id for the '<em><b>Soft Global Deadline</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_EVENT__SOFT_GLOBAL_DEADLINE = 3;

	/**
	 * The feature id for the '<em><b>Global Max Miss Ratio</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_EVENT__GLOBAL_MAX_MISS_RATIO = 4;

	/**
	 * The feature id for the '<em><b>Hard Local Deadline</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_EVENT__HARD_LOCAL_DEADLINE = 5;

	/**
	 * The feature id for the '<em><b>Soft Local Deadline</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_EVENT__SOFT_LOCAL_DEADLINE = 6;

	/**
	 * The feature id for the '<em><b>Local Max Miss Ratio</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_EVENT__LOCAL_MAX_MISS_RATIO = 7;

	/**
	 * The feature id for the '<em><b>Composite Timing Requirement</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_EVENT__COMPOSITE_TIMING_REQUIREMENT = 8;

	/**
	 * The feature id for the '<em><b>Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_EVENT__EVENT = 9;

	/**
	 * The number of structural features of the '<em>Regular Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_EVENT_FEATURE_COUNT = 10;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_ProcessorImpl <em>Regular Processor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Regular_ProcessorImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getRegular_Processor()
	 * @generated
	 */
	int REGULAR_PROCESSOR = 41;

	/**
	 * The feature id for the '<em><b>Ticker System Timer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_PROCESSOR__TICKER_SYSTEM_TIMER = 0;

	/**
	 * The feature id for the '<em><b>Alarm Clock System Timer</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_PROCESSOR__ALARM_CLOCK_SYSTEM_TIMER = 1;

	/**
	 * The feature id for the '<em><b>Avg ISR Switch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_PROCESSOR__AVG_ISR_SWITCH = 2;

	/**
	 * The feature id for the '<em><b>Best ISR Switch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_PROCESSOR__BEST_ISR_SWITCH = 3;

	/**
	 * The feature id for the '<em><b>Max Interrupt Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_PROCESSOR__MAX_INTERRUPT_PRIORITY = 4;

	/**
	 * The feature id for the '<em><b>Min Interrupt Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_PROCESSOR__MIN_INTERRUPT_PRIORITY = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_PROCESSOR__NAME = 6;

	/**
	 * The feature id for the '<em><b>Speed Factor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_PROCESSOR__SPEED_FACTOR = 7;

	/**
	 * The feature id for the '<em><b>Worst ISR Switch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_PROCESSOR__WORST_ISR_SWITCH = 8;

	/**
	 * The number of structural features of the '<em>Regular Processor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_PROCESSOR_FEATURE_COUNT = 9;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_Scheduling_ServerImpl <em>Regular Scheduling Server</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Regular_Scheduling_ServerImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getRegular_Scheduling_Server()
	 * @generated
	 */
	int REGULAR_SCHEDULING_SERVER = 42;

	/**
	 * The feature id for the '<em><b>Non Preemptible FP Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_SCHEDULING_SERVER__NON_PREEMPTIBLE_FP_POLICY = 0;

	/**
	 * The feature id for the '<em><b>Fixed Priority Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_SCHEDULING_SERVER__FIXED_PRIORITY_POLICY = 1;

	/**
	 * The feature id for the '<em><b>Interrupt FP Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_SCHEDULING_SERVER__INTERRUPT_FP_POLICY = 2;

	/**
	 * The feature id for the '<em><b>Polling Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_SCHEDULING_SERVER__POLLING_POLICY = 3;

	/**
	 * The feature id for the '<em><b>Sporadic Server Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_SCHEDULING_SERVER__SPORADIC_SERVER_POLICY = 4;

	/**
	 * The feature id for the '<em><b>EDF Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_SCHEDULING_SERVER__EDF_POLICY = 5;

	/**
	 * The feature id for the '<em><b>SRP Parameters</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_SCHEDULING_SERVER__SRP_PARAMETERS = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_SCHEDULING_SERVER__NAME = 7;

	/**
	 * The feature id for the '<em><b>Scheduler</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_SCHEDULING_SERVER__SCHEDULER = 8;

	/**
	 * The number of structural features of the '<em>Regular Scheduling Server</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_SCHEDULING_SERVER_FEATURE_COUNT = 9;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl <em>Regular Transaction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getRegular_Transaction()
	 * @generated
	 */
	int REGULAR_TRANSACTION = 43;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_TRANSACTION__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Periodic External Event</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_TRANSACTION__PERIODIC_EXTERNAL_EVENT = 1;

	/**
	 * The feature id for the '<em><b>Sporadic External Event</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_TRANSACTION__SPORADIC_EXTERNAL_EVENT = 2;

	/**
	 * The feature id for the '<em><b>Unbounded External Event</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_TRANSACTION__UNBOUNDED_EXTERNAL_EVENT = 3;

	/**
	 * The feature id for the '<em><b>Bursty External Event</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_TRANSACTION__BURSTY_EXTERNAL_EVENT = 4;

	/**
	 * The feature id for the '<em><b>Singular External Event</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_TRANSACTION__SINGULAR_EXTERNAL_EVENT = 5;

	/**
	 * The feature id for the '<em><b>Regular Event</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_TRANSACTION__REGULAR_EVENT = 6;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_TRANSACTION__ACTIVITY = 7;

	/**
	 * The feature id for the '<em><b>System Timed Activity</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_TRANSACTION__SYSTEM_TIMED_ACTIVITY = 8;

	/**
	 * The feature id for the '<em><b>Concentrator</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_TRANSACTION__CONCENTRATOR = 9;

	/**
	 * The feature id for the '<em><b>Barrier</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_TRANSACTION__BARRIER = 10;

	/**
	 * The feature id for the '<em><b>Delivery Server</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_TRANSACTION__DELIVERY_SERVER = 11;

	/**
	 * The feature id for the '<em><b>Query Server</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_TRANSACTION__QUERY_SERVER = 12;

	/**
	 * The feature id for the '<em><b>Multicast</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_TRANSACTION__MULTICAST = 13;

	/**
	 * The feature id for the '<em><b>Rate Divisor</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_TRANSACTION__RATE_DIVISOR = 14;

	/**
	 * The feature id for the '<em><b>Delay</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_TRANSACTION__DELAY = 15;

	/**
	 * The feature id for the '<em><b>Offset</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_TRANSACTION__OFFSET = 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_TRANSACTION__NAME = 17;

	/**
	 * The number of structural features of the '<em>Regular Transaction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REGULAR_TRANSACTION_FEATURE_COUNT = 18;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl <em>RTEP Packet Driver</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getRTEP_PacketDriver()
	 * @generated
	 */
	int RTEP_PACKET_DRIVER = 44;

	/**
	 * The feature id for the '<em><b>Failure Timeout</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTEP_PACKET_DRIVER__FAILURE_TIMEOUT = 0;

	/**
	 * The feature id for the '<em><b>Message Partitioning</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTEP_PACKET_DRIVER__MESSAGE_PARTITIONING = 1;

	/**
	 * The feature id for the '<em><b>Number Of Stations</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTEP_PACKET_DRIVER__NUMBER_OF_STATIONS = 2;

	/**
	 * The feature id for the '<em><b>Packet Discard Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTEP_PACKET_DRIVER__PACKET_DISCARD_OPERATION = 3;

	/**
	 * The feature id for the '<em><b>Packet Interrupt Server</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTEP_PACKET_DRIVER__PACKET_INTERRUPT_SERVER = 4;

	/**
	 * The feature id for the '<em><b>Packet ISR Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTEP_PACKET_DRIVER__PACKET_ISR_OPERATION = 5;

	/**
	 * The feature id for the '<em><b>Packet Receive Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTEP_PACKET_DRIVER__PACKET_RECEIVE_OPERATION = 6;

	/**
	 * The feature id for the '<em><b>Packet Retransmission Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTEP_PACKET_DRIVER__PACKET_RETRANSMISSION_OPERATION = 7;

	/**
	 * The feature id for the '<em><b>Packet Send Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTEP_PACKET_DRIVER__PACKET_SEND_OPERATION = 8;

	/**
	 * The feature id for the '<em><b>Packet Server</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTEP_PACKET_DRIVER__PACKET_SERVER = 9;

	/**
	 * The feature id for the '<em><b>Packet Transmission Retries</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTEP_PACKET_DRIVER__PACKET_TRANSMISSION_RETRIES = 10;

	/**
	 * The feature id for the '<em><b>RTA Overhead Model</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTEP_PACKET_DRIVER__RTA_OVERHEAD_MODEL = 11;

	/**
	 * The feature id for the '<em><b>Token Check Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTEP_PACKET_DRIVER__TOKEN_CHECK_OPERATION = 12;

	/**
	 * The feature id for the '<em><b>Token Delay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTEP_PACKET_DRIVER__TOKEN_DELAY = 13;

	/**
	 * The feature id for the '<em><b>Token Manage Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTEP_PACKET_DRIVER__TOKEN_MANAGE_OPERATION = 14;

	/**
	 * The feature id for the '<em><b>Token Retransmission Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTEP_PACKET_DRIVER__TOKEN_RETRANSMISSION_OPERATION = 15;

	/**
	 * The feature id for the '<em><b>Token Transmission Retries</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTEP_PACKET_DRIVER__TOKEN_TRANSMISSION_RETRIES = 16;

	/**
	 * The number of structural features of the '<em>RTEP Packet Driver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RTEP_PACKET_DRIVER_FEATURE_COUNT = 17;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Secondary_SchedulerImpl <em>Secondary Scheduler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Secondary_SchedulerImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getSecondary_Scheduler()
	 * @generated
	 */
	int SECONDARY_SCHEDULER = 45;

	/**
	 * The feature id for the '<em><b>Fixed Priority Scheduler</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECONDARY_SCHEDULER__FIXED_PRIORITY_SCHEDULER = 0;

	/**
	 * The feature id for the '<em><b>EDF Scheduler</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECONDARY_SCHEDULER__EDF_SCHEDULER = 1;

	/**
	 * The feature id for the '<em><b>FP Packet Based Scheduler</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECONDARY_SCHEDULER__FP_PACKET_BASED_SCHEDULER = 2;

	/**
	 * The feature id for the '<em><b>Host</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECONDARY_SCHEDULER__HOST = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECONDARY_SCHEDULER__NAME = 4;

	/**
	 * The number of structural features of the '<em>Secondary Scheduler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECONDARY_SCHEDULER_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Simple_OperationImpl <em>Simple Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Simple_OperationImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getSimple_Operation()
	 * @generated
	 */
	int SIMPLE_OPERATION = 46;

	/**
	 * The feature id for the '<em><b>Overridden Fixed Priority</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_OPERATION__OVERRIDDEN_FIXED_PRIORITY = 0;

	/**
	 * The feature id for the '<em><b>Overridden Permanent FP</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_OPERATION__OVERRIDDEN_PERMANENT_FP = 1;

	/**
	 * The feature id for the '<em><b>Shared Resources List</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_OPERATION__SHARED_RESOURCES_LIST = 2;

	/**
	 * The feature id for the '<em><b>Shared Resources To Lock</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_OPERATION__SHARED_RESOURCES_TO_LOCK = 3;

	/**
	 * The feature id for the '<em><b>Shared Resources To Unlock</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_OPERATION__SHARED_RESOURCES_TO_UNLOCK = 4;

	/**
	 * The feature id for the '<em><b>Average Case Execution Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_OPERATION__AVERAGE_CASE_EXECUTION_TIME = 5;

	/**
	 * The feature id for the '<em><b>Best Case Execution Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_OPERATION__BEST_CASE_EXECUTION_TIME = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_OPERATION__NAME = 7;

	/**
	 * The feature id for the '<em><b>Worst Case Execution Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_OPERATION__WORST_CASE_EXECUTION_TIME = 8;

	/**
	 * The number of structural features of the '<em>Simple Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMPLE_OPERATION_FEATURE_COUNT = 9;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Singular_External_EventImpl <em>Singular External Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Singular_External_EventImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getSingular_External_Event()
	 * @generated
	 */
	int SINGULAR_EXTERNAL_EVENT = 47;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGULAR_EXTERNAL_EVENT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Phase</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGULAR_EXTERNAL_EVENT__PHASE = 1;

	/**
	 * The number of structural features of the '<em>Singular External Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGULAR_EXTERNAL_EVENT_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Soft_Global_DeadlineImpl <em>Soft Global Deadline</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Soft_Global_DeadlineImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getSoft_Global_Deadline()
	 * @generated
	 */
	int SOFT_GLOBAL_DEADLINE = 48;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GLOBAL_DEADLINE__DEADLINE = 0;

	/**
	 * The feature id for the '<em><b>Referenced Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GLOBAL_DEADLINE__REFERENCED_EVENT = 1;

	/**
	 * The number of structural features of the '<em>Soft Global Deadline</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GLOBAL_DEADLINE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Soft_Local_DeadlineImpl <em>Soft Local Deadline</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Soft_Local_DeadlineImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getSoft_Local_Deadline()
	 * @generated
	 */
	int SOFT_LOCAL_DEADLINE = 49;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_LOCAL_DEADLINE__DEADLINE = 0;

	/**
	 * The number of structural features of the '<em>Soft Local Deadline</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_LOCAL_DEADLINE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Sporadic_External_EventImpl <em>Sporadic External Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Sporadic_External_EventImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getSporadic_External_Event()
	 * @generated
	 */
	int SPORADIC_EXTERNAL_EVENT = 50;

	/**
	 * The feature id for the '<em><b>Avg Interarrival</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPORADIC_EXTERNAL_EVENT__AVG_INTERARRIVAL = 0;

	/**
	 * The feature id for the '<em><b>Distribution</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPORADIC_EXTERNAL_EVENT__DISTRIBUTION = 1;

	/**
	 * The feature id for the '<em><b>Min Interarrival</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPORADIC_EXTERNAL_EVENT__MIN_INTERARRIVAL = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPORADIC_EXTERNAL_EVENT__NAME = 3;

	/**
	 * The number of structural features of the '<em>Sporadic External Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPORADIC_EXTERNAL_EVENT_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Sporadic_Server_PolicyImpl <em>Sporadic Server Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Sporadic_Server_PolicyImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getSporadic_Server_Policy()
	 * @generated
	 */
	int SPORADIC_SERVER_POLICY = 51;

	/**
	 * The feature id for the '<em><b>Background Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPORADIC_SERVER_POLICY__BACKGROUND_PRIORITY = 0;

	/**
	 * The feature id for the '<em><b>Initial Capacity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPORADIC_SERVER_POLICY__INITIAL_CAPACITY = 1;

	/**
	 * The feature id for the '<em><b>Max Pending Replenishments</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPORADIC_SERVER_POLICY__MAX_PENDING_REPLENISHMENTS = 2;

	/**
	 * The feature id for the '<em><b>Normal Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPORADIC_SERVER_POLICY__NORMAL_PRIORITY = 3;

	/**
	 * The feature id for the '<em><b>Preassigned</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPORADIC_SERVER_POLICY__PREASSIGNED = 4;

	/**
	 * The feature id for the '<em><b>Replenishment Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPORADIC_SERVER_POLICY__REPLENISHMENT_PERIOD = 5;

	/**
	 * The number of structural features of the '<em>Sporadic Server Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPORADIC_SERVER_POLICY_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.SRP_ParametersImpl <em>SRP Parameters</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.SRP_ParametersImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getSRP_Parameters()
	 * @generated
	 */
	int SRP_PARAMETERS = 52;

	/**
	 * The feature id for the '<em><b>Preassigned</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SRP_PARAMETERS__PREASSIGNED = 0;

	/**
	 * The feature id for the '<em><b>Preemption Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SRP_PARAMETERS__PREEMPTION_LEVEL = 1;

	/**
	 * The number of structural features of the '<em>SRP Parameters</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SRP_PARAMETERS_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.SRP_ResourceImpl <em>SRP Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.SRP_ResourceImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getSRP_Resource()
	 * @generated
	 */
	int SRP_RESOURCE = 53;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SRP_RESOURCE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Preassigned</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SRP_RESOURCE__PREASSIGNED = 1;

	/**
	 * The feature id for the '<em><b>Preemption Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SRP_RESOURCE__PREEMPTION_LEVEL = 2;

	/**
	 * The number of structural features of the '<em>SRP Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SRP_RESOURCE_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.System_Timed_ActivityImpl <em>System Timed Activity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.System_Timed_ActivityImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getSystem_Timed_Activity()
	 * @generated
	 */
	int SYSTEM_TIMED_ACTIVITY = 54;

	/**
	 * The feature id for the '<em><b>Activity Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TIMED_ACTIVITY__ACTIVITY_OPERATION = 0;

	/**
	 * The feature id for the '<em><b>Activity Server</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TIMED_ACTIVITY__ACTIVITY_SERVER = 1;

	/**
	 * The feature id for the '<em><b>Input Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TIMED_ACTIVITY__INPUT_EVENT = 2;

	/**
	 * The feature id for the '<em><b>Output Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TIMED_ACTIVITY__OUTPUT_EVENT = 3;

	/**
	 * The number of structural features of the '<em>System Timed Activity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SYSTEM_TIMED_ACTIVITY_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Ticker_System_TimerImpl <em>Ticker System Timer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Ticker_System_TimerImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getTicker_System_Timer()
	 * @generated
	 */
	int TICKER_SYSTEM_TIMER = 55;

	/**
	 * The feature id for the '<em><b>Avg Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TICKER_SYSTEM_TIMER__AVG_OVERHEAD = 0;

	/**
	 * The feature id for the '<em><b>Best Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TICKER_SYSTEM_TIMER__BEST_OVERHEAD = 1;

	/**
	 * The feature id for the '<em><b>Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TICKER_SYSTEM_TIMER__PERIOD = 2;

	/**
	 * The feature id for the '<em><b>Worst Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TICKER_SYSTEM_TIMER__WORST_OVERHEAD = 3;

	/**
	 * The number of structural features of the '<em>Ticker System Timer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TICKER_SYSTEM_TIMER_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Unbounded_External_EventImpl <em>Unbounded External Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.Unbounded_External_EventImpl
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getUnbounded_External_Event()
	 * @generated
	 */
	int UNBOUNDED_EXTERNAL_EVENT = 56;

	/**
	 * The feature id for the '<em><b>Avg Interarrival</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNBOUNDED_EXTERNAL_EVENT__AVG_INTERARRIVAL = 0;

	/**
	 * The feature id for the '<em><b>Distribution</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNBOUNDED_EXTERNAL_EVENT__DISTRIBUTION = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNBOUNDED_EXTERNAL_EVENT__NAME = 2;

	/**
	 * The number of structural features of the '<em>Unbounded External Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNBOUNDED_EXTERNAL_EVENT_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.Affirmative_Assertion <em>Affirmative Assertion</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.Affirmative_Assertion
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getAffirmative_Assertion()
	 * @generated
	 */
	int AFFIRMATIVE_ASSERTION = 57;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.Assertion <em>Assertion</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.Assertion
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getAssertion()
	 * @generated
	 */
	int ASSERTION = 58;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.Delivery_Policy <em>Delivery Policy</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.Delivery_Policy
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getDelivery_Policy()
	 * @generated
	 */
	int DELIVERY_POLICY = 59;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.Distribution_Type <em>Distribution Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.Distribution_Type
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getDistribution_Type()
	 * @generated
	 */
	int DISTRIBUTION_TYPE = 60;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.Negative_Assertion <em>Negative Assertion</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.Negative_Assertion
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getNegative_Assertion()
	 * @generated
	 */
	int NEGATIVE_ASSERTION = 61;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.Overhead_Type <em>Overhead Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.Overhead_Type
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getOverhead_Type()
	 * @generated
	 */
	int OVERHEAD_TYPE = 62;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Protocol <em>Priority Inheritance Protocol</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Protocol
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPriority_Inheritance_Protocol()
	 * @generated
	 */
	int PRIORITY_INHERITANCE_PROTOCOL = 63;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.Request_Policy <em>Request Policy</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.Request_Policy
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getRequest_Policy()
	 * @generated
	 */
	int REQUEST_POLICY = 64;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastmodel.Transmission_Type <em>Transmission Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.Transmission_Type
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getTransmission_Type()
	 * @generated
	 */
	int TRANSMISSION_TYPE = 65;

	/**
	 * The meta object id for the '<em>Absolute Time</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getAbsolute_Time()
	 * @generated
	 */
	int ABSOLUTE_TIME = 66;

	/**
	 * The meta object id for the '<em>Affirmative Assertion Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.Affirmative_Assertion
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getAffirmative_Assertion_Object()
	 * @generated
	 */
	int AFFIRMATIVE_ASSERTION_OBJECT = 67;

	/**
	 * The meta object id for the '<em>Any Priority</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getAny_Priority()
	 * @generated
	 */
	int ANY_PRIORITY = 68;

	/**
	 * The meta object id for the '<em>Assertion Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.Assertion
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getAssertion_Object()
	 * @generated
	 */
	int ASSERTION_OBJECT = 69;

	/**
	 * The meta object id for the '<em>Bit Count</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getBit_Count()
	 * @generated
	 */
	int BIT_COUNT = 70;

	/**
	 * The meta object id for the '<em>Date Time</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getDate_Time()
	 * @generated
	 */
	int DATE_TIME = 71;

	/**
	 * The meta object id for the '<em>Delivery Policy Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.Delivery_Policy
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getDelivery_Policy_Object()
	 * @generated
	 */
	int DELIVERY_POLICY_OBJECT = 72;

	/**
	 * The meta object id for the '<em>Distribution Type Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.Distribution_Type
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getDistribution_Type_Object()
	 * @generated
	 */
	int DISTRIBUTION_TYPE_OBJECT = 73;

	/**
	 * The meta object id for the '<em>Float</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getFloat()
	 * @generated
	 */
	int FLOAT = 74;

	/**
	 * The meta object id for the '<em>Identifier</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getIdentifier()
	 * @generated
	 */
	int IDENTIFIER = 75;

	/**
	 * The meta object id for the '<em>Identifier Ref</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getIdentifier_Ref()
	 * @generated
	 */
	int IDENTIFIER_REF = 76;

	/**
	 * The meta object id for the '<em>Identifier Ref List</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.List
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getIdentifier_Ref_List()
	 * @generated
	 */
	int IDENTIFIER_REF_LIST = 77;

	/**
	 * The meta object id for the '<em>Interrupt Priority</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getInterrupt_Priority()
	 * @generated
	 */
	int INTERRUPT_PRIORITY = 78;

	/**
	 * The meta object id for the '<em>Natural</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getNatural()
	 * @generated
	 */
	int NATURAL = 79;

	/**
	 * The meta object id for the '<em>Negative Assertion Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.Negative_Assertion
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getNegative_Assertion_Object()
	 * @generated
	 */
	int NEGATIVE_ASSERTION_OBJECT = 80;

	/**
	 * The meta object id for the '<em>Normalized Execution Time</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getNormalized_Execution_Time()
	 * @generated
	 */
	int NORMALIZED_EXECUTION_TIME = 81;

	/**
	 * The meta object id for the '<em>Overhead Type Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.Overhead_Type
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getOverhead_Type_Object()
	 * @generated
	 */
	int OVERHEAD_TYPE_OBJECT = 82;

	/**
	 * The meta object id for the '<em>Pathname</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPathname()
	 * @generated
	 */
	int PATHNAME = 83;

	/**
	 * The meta object id for the '<em>Percentage</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPercentage()
	 * @generated
	 */
	int PERCENTAGE = 84;

	/**
	 * The meta object id for the '<em>Positive</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPositive()
	 * @generated
	 */
	int POSITIVE = 85;

	/**
	 * The meta object id for the '<em>Preemption Level</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPreemption_Level()
	 * @generated
	 */
	int PREEMPTION_LEVEL = 86;

	/**
	 * The meta object id for the '<em>Priority</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPriority()
	 * @generated
	 */
	int PRIORITY = 87;

	/**
	 * The meta object id for the '<em>Priority Inheritance Protocol Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Protocol
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPriority_Inheritance_Protocol_Object()
	 * @generated
	 */
	int PRIORITY_INHERITANCE_PROTOCOL_OBJECT = 88;

	/**
	 * The meta object id for the '<em>Request Policy Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.Request_Policy
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getRequest_Policy_Object()
	 * @generated
	 */
	int REQUEST_POLICY_OBJECT = 89;

	/**
	 * The meta object id for the '<em>Throughput</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getThroughput()
	 * @generated
	 */
	int THROUGHPUT = 90;

	/**
	 * The meta object id for the '<em>Time</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getTime()
	 * @generated
	 */
	int TIME = 91;

	/**
	 * The meta object id for the '<em>Transmission Type Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastmodel.Transmission_Type
	 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getTransmission_Type_Object()
	 * @generated
	 */
	int TRANSMISSION_TYPE_OBJECT = 92;


	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Activity <em>Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Activity</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Activity
	 * @generated
	 */
	EClass getActivity();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Activity#getActivity_Operation <em>Activity Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Activity Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Activity#getActivity_Operation()
	 * @see #getActivity()
	 * @generated
	 */
	EAttribute getActivity_Activity_Operation();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Activity#getActivity_Server <em>Activity Server</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Activity Server</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Activity#getActivity_Server()
	 * @see #getActivity()
	 * @generated
	 */
	EAttribute getActivity_Activity_Server();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Activity#getInput_Event <em>Input Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Input Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Activity#getInput_Event()
	 * @see #getActivity()
	 * @generated
	 */
	EAttribute getActivity_Input_Event();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Activity#getOutput_Event <em>Output Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Activity#getOutput_Event()
	 * @see #getActivity()
	 * @generated
	 */
	EAttribute getActivity_Output_Event();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Alarm_Clock_System_Timer <em>Alarm Clock System Timer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Alarm Clock System Timer</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Alarm_Clock_System_Timer
	 * @generated
	 */
	EClass getAlarm_Clock_System_Timer();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Alarm_Clock_System_Timer#getAvg_Overhead <em>Avg Overhead</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Avg Overhead</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Alarm_Clock_System_Timer#getAvg_Overhead()
	 * @see #getAlarm_Clock_System_Timer()
	 * @generated
	 */
	EAttribute getAlarm_Clock_System_Timer_Avg_Overhead();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Alarm_Clock_System_Timer#getBest_Overhead <em>Best Overhead</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Best Overhead</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Alarm_Clock_System_Timer#getBest_Overhead()
	 * @see #getAlarm_Clock_System_Timer()
	 * @generated
	 */
	EAttribute getAlarm_Clock_System_Timer_Best_Overhead();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Alarm_Clock_System_Timer#getWorst_Overhead <em>Worst Overhead</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Worst Overhead</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Alarm_Clock_System_Timer#getWorst_Overhead()
	 * @see #getAlarm_Clock_System_Timer()
	 * @generated
	 */
	EAttribute getAlarm_Clock_System_Timer_Worst_Overhead();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Barrier <em>Barrier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Barrier</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Barrier
	 * @generated
	 */
	EClass getBarrier();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Barrier#getInput_Events_List <em>Input Events List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Input Events List</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Barrier#getInput_Events_List()
	 * @see #getBarrier()
	 * @generated
	 */
	EAttribute getBarrier_Input_Events_List();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Barrier#getOutput_Event <em>Output Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Barrier#getOutput_Event()
	 * @see #getBarrier()
	 * @generated
	 */
	EAttribute getBarrier_Output_Event();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event <em>Bursty External Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bursty External Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event
	 * @generated
	 */
	EClass getBursty_External_Event();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getAvg_Interarrival <em>Avg Interarrival</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Avg Interarrival</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getAvg_Interarrival()
	 * @see #getBursty_External_Event()
	 * @generated
	 */
	EAttribute getBursty_External_Event_Avg_Interarrival();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getBound_Interval <em>Bound Interval</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bound Interval</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getBound_Interval()
	 * @see #getBursty_External_Event()
	 * @generated
	 */
	EAttribute getBursty_External_Event_Bound_Interval();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getDistribution <em>Distribution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Distribution</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getDistribution()
	 * @see #getBursty_External_Event()
	 * @generated
	 */
	EAttribute getBursty_External_Event_Distribution();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getMax_Arrivals <em>Max Arrivals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Arrivals</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getMax_Arrivals()
	 * @see #getBursty_External_Event()
	 * @generated
	 */
	EAttribute getBursty_External_Event_Max_Arrivals();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event#getName()
	 * @see #getBursty_External_Event()
	 * @generated
	 */
	EAttribute getBursty_External_Event_Name();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver <em>Character Packet Driver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Character Packet Driver</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver
	 * @generated
	 */
	EClass getCharacter_Packet_Driver();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver#getCharacter_Receive_Operation <em>Character Receive Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Character Receive Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver#getCharacter_Receive_Operation()
	 * @see #getCharacter_Packet_Driver()
	 * @generated
	 */
	EAttribute getCharacter_Packet_Driver_Character_Receive_Operation();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver#getCharacter_Send_Operation <em>Character Send Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Character Send Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver#getCharacter_Send_Operation()
	 * @see #getCharacter_Packet_Driver()
	 * @generated
	 */
	EAttribute getCharacter_Packet_Driver_Character_Send_Operation();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver#getCharacter_Server <em>Character Server</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Character Server</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver#getCharacter_Server()
	 * @see #getCharacter_Packet_Driver()
	 * @generated
	 */
	EAttribute getCharacter_Packet_Driver_Character_Server();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver#getCharacter_Transmission_Time <em>Character Transmission Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Character Transmission Time</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver#getCharacter_Transmission_Time()
	 * @see #getCharacter_Packet_Driver()
	 * @generated
	 */
	EAttribute getCharacter_Packet_Driver_Character_Transmission_Time();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver#getMessage_Partitioning <em>Message Partitioning</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Partitioning</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver#getMessage_Partitioning()
	 * @see #getCharacter_Packet_Driver()
	 * @generated
	 */
	EAttribute getCharacter_Packet_Driver_Message_Partitioning();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver#getPacket_Receive_Operation <em>Packet Receive Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Packet Receive Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver#getPacket_Receive_Operation()
	 * @see #getCharacter_Packet_Driver()
	 * @generated
	 */
	EAttribute getCharacter_Packet_Driver_Packet_Receive_Operation();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver#getPacket_Send_Operation <em>Packet Send Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Packet Send Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver#getPacket_Send_Operation()
	 * @see #getCharacter_Packet_Driver()
	 * @generated
	 */
	EAttribute getCharacter_Packet_Driver_Packet_Send_Operation();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver#getPacket_Server <em>Packet Server</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Packet Server</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver#getPacket_Server()
	 * @see #getCharacter_Packet_Driver()
	 * @generated
	 */
	EAttribute getCharacter_Packet_Driver_Packet_Server();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver#getRTA_Overhead_Model <em>RTA Overhead Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>RTA Overhead Model</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver#getRTA_Overhead_Model()
	 * @see #getCharacter_Packet_Driver()
	 * @generated
	 */
	EAttribute getCharacter_Packet_Driver_RTA_Overhead_Model();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Composite_Operation <em>Composite Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Composite_Operation
	 * @generated
	 */
	EClass getComposite_Operation();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Composite_Operation#getOverridden_Fixed_Priority <em>Overridden Fixed Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Overridden Fixed Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Composite_Operation#getOverridden_Fixed_Priority()
	 * @see #getComposite_Operation()
	 * @generated
	 */
	EReference getComposite_Operation_Overridden_Fixed_Priority();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Composite_Operation#getOverridden_Permanent_FP <em>Overridden Permanent FP</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Overridden Permanent FP</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Composite_Operation#getOverridden_Permanent_FP()
	 * @see #getComposite_Operation()
	 * @generated
	 */
	EReference getComposite_Operation_Overridden_Permanent_FP();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Composite_Operation#getOperation_List <em>Operation List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operation List</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Composite_Operation#getOperation_List()
	 * @see #getComposite_Operation()
	 * @generated
	 */
	EAttribute getComposite_Operation_Operation_List();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Composite_Operation#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Composite_Operation#getName()
	 * @see #getComposite_Operation()
	 * @generated
	 */
	EAttribute getComposite_Operation_Name();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement <em>Composite Timing Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite Timing Requirement</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement
	 * @generated
	 */
	EClass getComposite_Timing_Requirement();

	/**
	 * Returns the meta object for the attribute list '{@link es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getGroup()
	 * @see #getComposite_Timing_Requirement()
	 * @generated
	 */
	EAttribute getComposite_Timing_Requirement_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getMax_Output_Jitter_Req <em>Max Output Jitter Req</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Max Output Jitter Req</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getMax_Output_Jitter_Req()
	 * @see #getComposite_Timing_Requirement()
	 * @generated
	 */
	EReference getComposite_Timing_Requirement_Max_Output_Jitter_Req();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getHard_Global_Deadline <em>Hard Global Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Hard Global Deadline</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getHard_Global_Deadline()
	 * @see #getComposite_Timing_Requirement()
	 * @generated
	 */
	EReference getComposite_Timing_Requirement_Hard_Global_Deadline();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getSoft_Global_Deadline <em>Soft Global Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Soft Global Deadline</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getSoft_Global_Deadline()
	 * @see #getComposite_Timing_Requirement()
	 * @generated
	 */
	EReference getComposite_Timing_Requirement_Soft_Global_Deadline();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getGlobal_Max_Miss_Ratio <em>Global Max Miss Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Global Max Miss Ratio</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getGlobal_Max_Miss_Ratio()
	 * @see #getComposite_Timing_Requirement()
	 * @generated
	 */
	EReference getComposite_Timing_Requirement_Global_Max_Miss_Ratio();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getHard_Local_Deadline <em>Hard Local Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Hard Local Deadline</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getHard_Local_Deadline()
	 * @see #getComposite_Timing_Requirement()
	 * @generated
	 */
	EReference getComposite_Timing_Requirement_Hard_Local_Deadline();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getSoft_Local_Deadline <em>Soft Local Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Soft Local Deadline</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getSoft_Local_Deadline()
	 * @see #getComposite_Timing_Requirement()
	 * @generated
	 */
	EReference getComposite_Timing_Requirement_Soft_Local_Deadline();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getLocal_Max_Miss_Ratio <em>Local Max Miss Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Local Max Miss Ratio</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getLocal_Max_Miss_Ratio()
	 * @see #getComposite_Timing_Requirement()
	 * @generated
	 */
	EReference getComposite_Timing_Requirement_Local_Max_Miss_Ratio();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Concentrator <em>Concentrator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Concentrator</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Concentrator
	 * @generated
	 */
	EClass getConcentrator();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Concentrator#getInput_Events_List <em>Input Events List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Input Events List</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Concentrator#getInput_Events_List()
	 * @see #getConcentrator()
	 * @generated
	 */
	EAttribute getConcentrator_Input_Events_List();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Concentrator#getOutput_Event <em>Output Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Concentrator#getOutput_Event()
	 * @see #getConcentrator()
	 * @generated
	 */
	EAttribute getConcentrator_Output_Event();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Delay <em>Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Delay</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Delay
	 * @generated
	 */
	EClass getDelay();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Delay#getDelay_Max_Interval <em>Delay Max Interval</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Delay Max Interval</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Delay#getDelay_Max_Interval()
	 * @see #getDelay()
	 * @generated
	 */
	EAttribute getDelay_Delay_Max_Interval();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Delay#getDelay_Min_Interval <em>Delay Min Interval</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Delay Min Interval</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Delay#getDelay_Min_Interval()
	 * @see #getDelay()
	 * @generated
	 */
	EAttribute getDelay_Delay_Min_Interval();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Delay#getInput_Event <em>Input Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Input Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Delay#getInput_Event()
	 * @see #getDelay()
	 * @generated
	 */
	EAttribute getDelay_Input_Event();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Delay#getOutput_Event <em>Output Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Delay#getOutput_Event()
	 * @see #getDelay()
	 * @generated
	 */
	EAttribute getDelay_Output_Event();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Delivery_Server <em>Delivery Server</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Delivery Server</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Delivery_Server
	 * @generated
	 */
	EClass getDelivery_Server();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Delivery_Server#getDelivery_Policy <em>Delivery Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Delivery Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Delivery_Server#getDelivery_Policy()
	 * @see #getDelivery_Server()
	 * @generated
	 */
	EAttribute getDelivery_Server_Delivery_Policy();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Delivery_Server#getInput_Event <em>Input Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Input Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Delivery_Server#getInput_Event()
	 * @see #getDelivery_Server()
	 * @generated
	 */
	EAttribute getDelivery_Server_Input_Event();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Delivery_Server#getOutput_Events_List <em>Output Events List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Events List</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Delivery_Server#getOutput_Events_List()
	 * @see #getDelivery_Server()
	 * @generated
	 */
	EAttribute getDelivery_Server_Output_Events_List();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Document_Root <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Root</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Document_Root
	 * @generated
	 */
	EClass getDocument_Root();

	/**
	 * Returns the meta object for the attribute list '{@link es.esi.gemde.vv.mast.mastmodel.Document_Root#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Document_Root#getMixed()
	 * @see #getDocument_Root()
	 * @generated
	 */
	EAttribute getDocument_Root_Mixed();

	/**
	 * Returns the meta object for the map '{@link es.esi.gemde.vv.mast.mastmodel.Document_Root#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Document_Root#getXMLNSPrefixMap()
	 * @see #getDocument_Root()
	 * @generated
	 */
	EReference getDocument_Root_XMLNSPrefixMap();

	/**
	 * Returns the meta object for the map '{@link es.esi.gemde.vv.mast.mastmodel.Document_Root#getXSISchemaLocation <em>XSI Schema Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XSI Schema Location</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Document_Root#getXSISchemaLocation()
	 * @see #getDocument_Root()
	 * @generated
	 */
	EReference getDocument_Root_XSISchemaLocation();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Document_Root#getMAST_MODEL <em>MAST MODEL</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>MAST MODEL</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Document_Root#getMAST_MODEL()
	 * @see #getDocument_Root()
	 * @generated
	 */
	EReference getDocument_Root_MAST_MODEL();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.EDF_Policy <em>EDF Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EDF Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.EDF_Policy
	 * @generated
	 */
	EClass getEDF_Policy();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.EDF_Policy#getDeadline <em>Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deadline</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.EDF_Policy#getDeadline()
	 * @see #getEDF_Policy()
	 * @generated
	 */
	EAttribute getEDF_Policy_Deadline();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.EDF_Policy#getPreassigned <em>Preassigned</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Preassigned</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.EDF_Policy#getPreassigned()
	 * @see #getEDF_Policy()
	 * @generated
	 */
	EAttribute getEDF_Policy_Preassigned();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.EDF_Scheduler <em>EDF Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>EDF Scheduler</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.EDF_Scheduler
	 * @generated
	 */
	EClass getEDF_Scheduler();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.EDF_Scheduler#getAvg_Context_Switch <em>Avg Context Switch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Avg Context Switch</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.EDF_Scheduler#getAvg_Context_Switch()
	 * @see #getEDF_Scheduler()
	 * @generated
	 */
	EAttribute getEDF_Scheduler_Avg_Context_Switch();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.EDF_Scheduler#getBest_Context_Switch <em>Best Context Switch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Best Context Switch</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.EDF_Scheduler#getBest_Context_Switch()
	 * @see #getEDF_Scheduler()
	 * @generated
	 */
	EAttribute getEDF_Scheduler_Best_Context_Switch();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.EDF_Scheduler#getWorst_Context_Switch <em>Worst Context Switch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Worst Context Switch</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.EDF_Scheduler#getWorst_Context_Switch()
	 * @see #getEDF_Scheduler()
	 * @generated
	 */
	EAttribute getEDF_Scheduler_Worst_Context_Switch();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation <em>Enclosing Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Enclosing Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation
	 * @generated
	 */
	EClass getEnclosing_Operation();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation#getOverridden_Fixed_Priority <em>Overridden Fixed Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Overridden Fixed Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation#getOverridden_Fixed_Priority()
	 * @see #getEnclosing_Operation()
	 * @generated
	 */
	EReference getEnclosing_Operation_Overridden_Fixed_Priority();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation#getOverridden_Permanent_FP <em>Overridden Permanent FP</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Overridden Permanent FP</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation#getOverridden_Permanent_FP()
	 * @see #getEnclosing_Operation()
	 * @generated
	 */
	EReference getEnclosing_Operation_Overridden_Permanent_FP();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation#getOperation_List <em>Operation List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operation List</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation#getOperation_List()
	 * @see #getEnclosing_Operation()
	 * @generated
	 */
	EAttribute getEnclosing_Operation_Operation_List();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation#getAverage_Case_Execution_Time <em>Average Case Execution Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Average Case Execution Time</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation#getAverage_Case_Execution_Time()
	 * @see #getEnclosing_Operation()
	 * @generated
	 */
	EAttribute getEnclosing_Operation_Average_Case_Execution_Time();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation#getBest_Case_Execution_Time <em>Best Case Execution Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Best Case Execution Time</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation#getBest_Case_Execution_Time()
	 * @see #getEnclosing_Operation()
	 * @generated
	 */
	EAttribute getEnclosing_Operation_Best_Case_Execution_Time();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation#getName()
	 * @see #getEnclosing_Operation()
	 * @generated
	 */
	EAttribute getEnclosing_Operation_Name();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation#getWorst_Case_Execution_Time <em>Worst Case Execution Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Worst Case Execution Time</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation#getWorst_Case_Execution_Time()
	 * @see #getEnclosing_Operation()
	 * @generated
	 */
	EAttribute getEnclosing_Operation_Worst_Case_Execution_Time();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Policy <em>Fixed Priority Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fixed Priority Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Policy
	 * @generated
	 */
	EClass getFixed_Priority_Policy();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Policy#getPreassigned <em>Preassigned</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Preassigned</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Policy#getPreassigned()
	 * @see #getFixed_Priority_Policy()
	 * @generated
	 */
	EAttribute getFixed_Priority_Policy_Preassigned();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Policy#getThe_Priority <em>The Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>The Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Policy#getThe_Priority()
	 * @see #getFixed_Priority_Policy()
	 * @generated
	 */
	EAttribute getFixed_Priority_Policy_The_Priority();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler <em>Fixed Priority Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fixed Priority Scheduler</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler
	 * @generated
	 */
	EClass getFixed_Priority_Scheduler();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler#getAvg_Context_Switch <em>Avg Context Switch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Avg Context Switch</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler#getAvg_Context_Switch()
	 * @see #getFixed_Priority_Scheduler()
	 * @generated
	 */
	EAttribute getFixed_Priority_Scheduler_Avg_Context_Switch();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler#getBest_Context_Switch <em>Best Context Switch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Best Context Switch</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler#getBest_Context_Switch()
	 * @see #getFixed_Priority_Scheduler()
	 * @generated
	 */
	EAttribute getFixed_Priority_Scheduler_Best_Context_Switch();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler#getMax_Priority <em>Max Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler#getMax_Priority()
	 * @see #getFixed_Priority_Scheduler()
	 * @generated
	 */
	EAttribute getFixed_Priority_Scheduler_Max_Priority();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler#getMin_Priority <em>Min Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler#getMin_Priority()
	 * @see #getFixed_Priority_Scheduler()
	 * @generated
	 */
	EAttribute getFixed_Priority_Scheduler_Min_Priority();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler#getWorst_Context_Switch <em>Worst Context Switch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Worst Context Switch</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler#getWorst_Context_Switch()
	 * @see #getFixed_Priority_Scheduler()
	 * @generated
	 */
	EAttribute getFixed_Priority_Scheduler_Worst_Context_Switch();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler <em>FP Packet Based Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>FP Packet Based Scheduler</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler
	 * @generated
	 */
	EClass getFP_Packet_Based_Scheduler();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler#getMax_Priority <em>Max Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler#getMax_Priority()
	 * @see #getFP_Packet_Based_Scheduler()
	 * @generated
	 */
	EAttribute getFP_Packet_Based_Scheduler_Max_Priority();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler#getMin_Priority <em>Min Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler#getMin_Priority()
	 * @see #getFP_Packet_Based_Scheduler()
	 * @generated
	 */
	EAttribute getFP_Packet_Based_Scheduler_Min_Priority();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler#getPacket_Overhead_Avg_Size <em>Packet Overhead Avg Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Packet Overhead Avg Size</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler#getPacket_Overhead_Avg_Size()
	 * @see #getFP_Packet_Based_Scheduler()
	 * @generated
	 */
	EAttribute getFP_Packet_Based_Scheduler_Packet_Overhead_Avg_Size();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler#getPacket_Overhead_Max_Size <em>Packet Overhead Max Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Packet Overhead Max Size</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler#getPacket_Overhead_Max_Size()
	 * @see #getFP_Packet_Based_Scheduler()
	 * @generated
	 */
	EAttribute getFP_Packet_Based_Scheduler_Packet_Overhead_Max_Size();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler#getPacket_Overhead_Min_Size <em>Packet Overhead Min Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Packet Overhead Min Size</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler#getPacket_Overhead_Min_Size()
	 * @see #getFP_Packet_Based_Scheduler()
	 * @generated
	 */
	EAttribute getFP_Packet_Based_Scheduler_Packet_Overhead_Min_Size();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio <em>Global Max Miss Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Global Max Miss Ratio</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio
	 * @generated
	 */
	EClass getGlobal_Max_Miss_Ratio();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio#getDeadline <em>Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deadline</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio#getDeadline()
	 * @see #getGlobal_Max_Miss_Ratio()
	 * @generated
	 */
	EAttribute getGlobal_Max_Miss_Ratio_Deadline();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio#getRatio <em>Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ratio</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio#getRatio()
	 * @see #getGlobal_Max_Miss_Ratio()
	 * @generated
	 */
	EAttribute getGlobal_Max_Miss_Ratio_Ratio();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio#getReferenced_Event <em>Referenced Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Referenced Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio#getReferenced_Event()
	 * @see #getGlobal_Max_Miss_Ratio()
	 * @generated
	 */
	EAttribute getGlobal_Max_Miss_Ratio_Referenced_Event();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Hard_Global_Deadline <em>Hard Global Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hard Global Deadline</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Hard_Global_Deadline
	 * @generated
	 */
	EClass getHard_Global_Deadline();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Hard_Global_Deadline#getDeadline <em>Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deadline</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Hard_Global_Deadline#getDeadline()
	 * @see #getHard_Global_Deadline()
	 * @generated
	 */
	EAttribute getHard_Global_Deadline_Deadline();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Hard_Global_Deadline#getReferenced_Event <em>Referenced Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Referenced Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Hard_Global_Deadline#getReferenced_Event()
	 * @see #getHard_Global_Deadline()
	 * @generated
	 */
	EAttribute getHard_Global_Deadline_Referenced_Event();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Hard_Local_Deadline <em>Hard Local Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hard Local Deadline</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Hard_Local_Deadline
	 * @generated
	 */
	EClass getHard_Local_Deadline();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Hard_Local_Deadline#getDeadline <em>Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deadline</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Hard_Local_Deadline#getDeadline()
	 * @see #getHard_Local_Deadline()
	 * @generated
	 */
	EAttribute getHard_Local_Deadline_Deadline();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource <em>Immediate Ceiling Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Immediate Ceiling Resource</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource
	 * @generated
	 */
	EClass getImmediate_Ceiling_Resource();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource#getCeiling <em>Ceiling</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ceiling</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource#getCeiling()
	 * @see #getImmediate_Ceiling_Resource()
	 * @generated
	 */
	EAttribute getImmediate_Ceiling_Resource_Ceiling();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource#getName()
	 * @see #getImmediate_Ceiling_Resource()
	 * @generated
	 */
	EAttribute getImmediate_Ceiling_Resource_Name();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource#getPreassigned <em>Preassigned</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Preassigned</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource#getPreassigned()
	 * @see #getImmediate_Ceiling_Resource()
	 * @generated
	 */
	EAttribute getImmediate_Ceiling_Resource_Preassigned();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Interrupt_FP_Policy <em>Interrupt FP Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interrupt FP Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Interrupt_FP_Policy
	 * @generated
	 */
	EClass getInterrupt_FP_Policy();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Interrupt_FP_Policy#getPreassigned <em>Preassigned</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Preassigned</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Interrupt_FP_Policy#getPreassigned()
	 * @see #getInterrupt_FP_Policy()
	 * @generated
	 */
	EAttribute getInterrupt_FP_Policy_Preassigned();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Interrupt_FP_Policy#getThe_Priority <em>The Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>The Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Interrupt_FP_Policy#getThe_Priority()
	 * @see #getInterrupt_FP_Policy()
	 * @generated
	 */
	EAttribute getInterrupt_FP_Policy_The_Priority();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.List_of_Drivers <em>List of Drivers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>List of Drivers</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.List_of_Drivers
	 * @generated
	 */
	EClass getList_of_Drivers();

	/**
	 * Returns the meta object for the attribute list '{@link es.esi.gemde.vv.mast.mastmodel.List_of_Drivers#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.List_of_Drivers#getGroup()
	 * @see #getList_of_Drivers()
	 * @generated
	 */
	EAttribute getList_of_Drivers_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.List_of_Drivers#getPacket_Driver <em>Packet Driver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Packet Driver</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.List_of_Drivers#getPacket_Driver()
	 * @see #getList_of_Drivers()
	 * @generated
	 */
	EReference getList_of_Drivers_Packet_Driver();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.List_of_Drivers#getCharacter_Packet_Driver <em>Character Packet Driver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Character Packet Driver</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.List_of_Drivers#getCharacter_Packet_Driver()
	 * @see #getList_of_Drivers()
	 * @generated
	 */
	EReference getList_of_Drivers_Character_Packet_Driver();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.List_of_Drivers#getRTEP_Packet_Driver <em>RTEP Packet Driver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>RTEP Packet Driver</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.List_of_Drivers#getRTEP_Packet_Driver()
	 * @see #getList_of_Drivers()
	 * @generated
	 */
	EReference getList_of_Drivers_RTEP_Packet_Driver();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Local_Max_Miss_Ratio <em>Local Max Miss Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Local Max Miss Ratio</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Local_Max_Miss_Ratio
	 * @generated
	 */
	EClass getLocal_Max_Miss_Ratio();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Local_Max_Miss_Ratio#getDeadline <em>Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deadline</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Local_Max_Miss_Ratio#getDeadline()
	 * @see #getLocal_Max_Miss_Ratio()
	 * @generated
	 */
	EAttribute getLocal_Max_Miss_Ratio_Deadline();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Local_Max_Miss_Ratio#getRatio <em>Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ratio</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Local_Max_Miss_Ratio#getRatio()
	 * @see #getLocal_Max_Miss_Ratio()
	 * @generated
	 */
	EAttribute getLocal_Max_Miss_Ratio_Ratio();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL <em>MAST MODEL</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MAST MODEL</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.MAST_MODEL
	 * @generated
	 */
	EClass getMAST_MODEL();

	/**
	 * Returns the meta object for the attribute list '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getGroup()
	 * @see #getMAST_MODEL()
	 * @generated
	 */
	EAttribute getMAST_MODEL_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getRegular_Processor <em>Regular Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Regular Processor</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getRegular_Processor()
	 * @see #getMAST_MODEL()
	 * @generated
	 */
	EReference getMAST_MODEL_Regular_Processor();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getPacket_Based_Network <em>Packet Based Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Packet Based Network</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getPacket_Based_Network()
	 * @see #getMAST_MODEL()
	 * @generated
	 */
	EReference getMAST_MODEL_Packet_Based_Network();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getRegular_Scheduling_Server <em>Regular Scheduling Server</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Regular Scheduling Server</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getRegular_Scheduling_Server()
	 * @see #getMAST_MODEL()
	 * @generated
	 */
	EReference getMAST_MODEL_Regular_Scheduling_Server();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getImmediate_Ceiling_Resource <em>Immediate Ceiling Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Immediate Ceiling Resource</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getImmediate_Ceiling_Resource()
	 * @see #getMAST_MODEL()
	 * @generated
	 */
	EReference getMAST_MODEL_Immediate_Ceiling_Resource();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getPriority_Inheritance_Resource <em>Priority Inheritance Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Priority Inheritance Resource</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getPriority_Inheritance_Resource()
	 * @see #getMAST_MODEL()
	 * @generated
	 */
	EReference getMAST_MODEL_Priority_Inheritance_Resource();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getSRP_Resource <em>SRP Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>SRP Resource</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getSRP_Resource()
	 * @see #getMAST_MODEL()
	 * @generated
	 */
	EReference getMAST_MODEL_SRP_Resource();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getSimple_Operation <em>Simple Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Simple Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getSimple_Operation()
	 * @see #getMAST_MODEL()
	 * @generated
	 */
	EReference getMAST_MODEL_Simple_Operation();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getMessage_Transmission <em>Message Transmission</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Message Transmission</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getMessage_Transmission()
	 * @see #getMAST_MODEL()
	 * @generated
	 */
	EReference getMAST_MODEL_Message_Transmission();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getComposite_Operation <em>Composite Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Composite Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getComposite_Operation()
	 * @see #getMAST_MODEL()
	 * @generated
	 */
	EReference getMAST_MODEL_Composite_Operation();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getEnclosing_Operation <em>Enclosing Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Enclosing Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getEnclosing_Operation()
	 * @see #getMAST_MODEL()
	 * @generated
	 */
	EReference getMAST_MODEL_Enclosing_Operation();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getRegular_Transaction <em>Regular Transaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Regular Transaction</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getRegular_Transaction()
	 * @see #getMAST_MODEL()
	 * @generated
	 */
	EReference getMAST_MODEL_Regular_Transaction();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getPrimary_Scheduler <em>Primary Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Primary Scheduler</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getPrimary_Scheduler()
	 * @see #getMAST_MODEL()
	 * @generated
	 */
	EReference getMAST_MODEL_Primary_Scheduler();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getSecondary_Scheduler <em>Secondary Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Secondary Scheduler</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getSecondary_Scheduler()
	 * @see #getMAST_MODEL()
	 * @generated
	 */
	EReference getMAST_MODEL_Secondary_Scheduler();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getModel_Date <em>Model Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model Date</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getModel_Date()
	 * @see #getMAST_MODEL()
	 * @generated
	 */
	EAttribute getMAST_MODEL_Model_Date();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getModel_Name <em>Model Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getModel_Name()
	 * @see #getMAST_MODEL()
	 * @generated
	 */
	EAttribute getMAST_MODEL_Model_Name();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getSystem_PiP_Behaviour <em>System Pi PBehaviour</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>System Pi PBehaviour</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getSystem_PiP_Behaviour()
	 * @see #getMAST_MODEL()
	 * @generated
	 */
	EAttribute getMAST_MODEL_System_PiP_Behaviour();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Max_Output_Jitter_Req <em>Max Output Jitter Req</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Max Output Jitter Req</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Max_Output_Jitter_Req
	 * @generated
	 */
	EClass getMax_Output_Jitter_Req();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Max_Output_Jitter_Req#getMax_Output_Jitter <em>Max Output Jitter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Output Jitter</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Max_Output_Jitter_Req#getMax_Output_Jitter()
	 * @see #getMax_Output_Jitter_Req()
	 * @generated
	 */
	EAttribute getMax_Output_Jitter_Req_Max_Output_Jitter();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Max_Output_Jitter_Req#getReferenced_Event <em>Referenced Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Referenced Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Max_Output_Jitter_Req#getReferenced_Event()
	 * @see #getMax_Output_Jitter_Req()
	 * @generated
	 */
	EAttribute getMax_Output_Jitter_Req_Referenced_Event();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission <em>Message Transmission</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Message Transmission</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Message_Transmission
	 * @generated
	 */
	EClass getMessage_Transmission();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getOverridden_Fixed_Priority <em>Overridden Fixed Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Overridden Fixed Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getOverridden_Fixed_Priority()
	 * @see #getMessage_Transmission()
	 * @generated
	 */
	EReference getMessage_Transmission_Overridden_Fixed_Priority();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getOverridden_Permanent_FP <em>Overridden Permanent FP</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Overridden Permanent FP</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getOverridden_Permanent_FP()
	 * @see #getMessage_Transmission()
	 * @generated
	 */
	EReference getMessage_Transmission_Overridden_Permanent_FP();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getAvg_Message_Size <em>Avg Message Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Avg Message Size</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getAvg_Message_Size()
	 * @see #getMessage_Transmission()
	 * @generated
	 */
	EAttribute getMessage_Transmission_Avg_Message_Size();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getMax_Message_Size <em>Max Message Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Message Size</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getMax_Message_Size()
	 * @see #getMessage_Transmission()
	 * @generated
	 */
	EAttribute getMessage_Transmission_Max_Message_Size();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getMin_Message_Size <em>Min Message Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Message Size</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getMin_Message_Size()
	 * @see #getMessage_Transmission()
	 * @generated
	 */
	EAttribute getMessage_Transmission_Min_Message_Size();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getName()
	 * @see #getMessage_Transmission()
	 * @generated
	 */
	EAttribute getMessage_Transmission_Name();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Multicast <em>Multicast</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multicast</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Multicast
	 * @generated
	 */
	EClass getMulticast();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Multicast#getInput_Event <em>Input Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Input Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Multicast#getInput_Event()
	 * @see #getMulticast()
	 * @generated
	 */
	EAttribute getMulticast_Input_Event();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Multicast#getOutput_Events_List <em>Output Events List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Events List</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Multicast#getOutput_Events_List()
	 * @see #getMulticast()
	 * @generated
	 */
	EAttribute getMulticast_Output_Events_List();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Non_Preemptible_FP_Policy <em>Non Preemptible FP Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Non Preemptible FP Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Non_Preemptible_FP_Policy
	 * @generated
	 */
	EClass getNon_Preemptible_FP_Policy();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Non_Preemptible_FP_Policy#getPreassigned <em>Preassigned</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Preassigned</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Non_Preemptible_FP_Policy#getPreassigned()
	 * @see #getNon_Preemptible_FP_Policy()
	 * @generated
	 */
	EAttribute getNon_Preemptible_FP_Policy_Preassigned();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Non_Preemptible_FP_Policy#getThe_Priority <em>The Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>The Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Non_Preemptible_FP_Policy#getThe_Priority()
	 * @see #getNon_Preemptible_FP_Policy()
	 * @generated
	 */
	EAttribute getNon_Preemptible_FP_Policy_The_Priority();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Offset <em>Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Offset</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Offset
	 * @generated
	 */
	EClass getOffset();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Offset#getDelay_Max_Interval <em>Delay Max Interval</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Delay Max Interval</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Offset#getDelay_Max_Interval()
	 * @see #getOffset()
	 * @generated
	 */
	EAttribute getOffset_Delay_Max_Interval();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Offset#getDelay_Min_Interval <em>Delay Min Interval</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Delay Min Interval</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Offset#getDelay_Min_Interval()
	 * @see #getOffset()
	 * @generated
	 */
	EAttribute getOffset_Delay_Min_Interval();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Offset#getInput_Event <em>Input Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Input Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Offset#getInput_Event()
	 * @see #getOffset()
	 * @generated
	 */
	EAttribute getOffset_Input_Event();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Offset#getOutput_Event <em>Output Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Offset#getOutput_Event()
	 * @see #getOffset()
	 * @generated
	 */
	EAttribute getOffset_Output_Event();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Offset#getReferenced_Event <em>Referenced Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Referenced Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Offset#getReferenced_Event()
	 * @see #getOffset()
	 * @generated
	 */
	EAttribute getOffset_Referenced_Event();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Overridden_Fixed_Priority <em>Overridden Fixed Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Overridden Fixed Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Overridden_Fixed_Priority
	 * @generated
	 */
	EClass getOverridden_Fixed_Priority();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Overridden_Fixed_Priority#getThe_Priority <em>The Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>The Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Overridden_Fixed_Priority#getThe_Priority()
	 * @see #getOverridden_Fixed_Priority()
	 * @generated
	 */
	EAttribute getOverridden_Fixed_Priority_The_Priority();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Overridden_Permanent_FP <em>Overridden Permanent FP</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Overridden Permanent FP</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Overridden_Permanent_FP
	 * @generated
	 */
	EClass getOverridden_Permanent_FP();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Overridden_Permanent_FP#getThe_Priority <em>The Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>The Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Overridden_Permanent_FP#getThe_Priority()
	 * @see #getOverridden_Permanent_FP()
	 * @generated
	 */
	EAttribute getOverridden_Permanent_FP_The_Priority();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network <em>Packet Based Network</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Packet Based Network</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network
	 * @generated
	 */
	EClass getPacket_Based_Network();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getList_of_Drivers <em>List of Drivers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>List of Drivers</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getList_of_Drivers()
	 * @see #getPacket_Based_Network()
	 * @generated
	 */
	EReference getPacket_Based_Network_List_of_Drivers();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getMax_Blocking <em>Max Blocking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Blocking</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getMax_Blocking()
	 * @see #getPacket_Based_Network()
	 * @generated
	 */
	EAttribute getPacket_Based_Network_Max_Blocking();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getMax_Packet_Size <em>Max Packet Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Packet Size</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getMax_Packet_Size()
	 * @see #getPacket_Based_Network()
	 * @generated
	 */
	EAttribute getPacket_Based_Network_Max_Packet_Size();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getMin_Packet_Size <em>Min Packet Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Packet Size</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getMin_Packet_Size()
	 * @see #getPacket_Based_Network()
	 * @generated
	 */
	EAttribute getPacket_Based_Network_Min_Packet_Size();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getName()
	 * @see #getPacket_Based_Network()
	 * @generated
	 */
	EAttribute getPacket_Based_Network_Name();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getSpeed_Factor <em>Speed Factor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Speed Factor</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getSpeed_Factor()
	 * @see #getPacket_Based_Network()
	 * @generated
	 */
	EAttribute getPacket_Based_Network_Speed_Factor();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getThroughput <em>Throughput</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Throughput</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getThroughput()
	 * @see #getPacket_Based_Network()
	 * @generated
	 */
	EAttribute getPacket_Based_Network_Throughput();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getTransmission <em>Transmission</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Transmission</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network#getTransmission()
	 * @see #getPacket_Based_Network()
	 * @generated
	 */
	EAttribute getPacket_Based_Network_Transmission();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver <em>Packet Driver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Packet Driver</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Packet_Driver
	 * @generated
	 */
	EClass getPacket_Driver();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getMessage_Partitioning <em>Message Partitioning</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Partitioning</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getMessage_Partitioning()
	 * @see #getPacket_Driver()
	 * @generated
	 */
	EAttribute getPacket_Driver_Message_Partitioning();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getPacket_Receive_Operation <em>Packet Receive Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Packet Receive Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getPacket_Receive_Operation()
	 * @see #getPacket_Driver()
	 * @generated
	 */
	EAttribute getPacket_Driver_Packet_Receive_Operation();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getPacket_Send_Operation <em>Packet Send Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Packet Send Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getPacket_Send_Operation()
	 * @see #getPacket_Driver()
	 * @generated
	 */
	EAttribute getPacket_Driver_Packet_Send_Operation();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getPacket_Server <em>Packet Server</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Packet Server</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getPacket_Server()
	 * @see #getPacket_Driver()
	 * @generated
	 */
	EAttribute getPacket_Driver_Packet_Server();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getRTA_Overhead_Model <em>RTA Overhead Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>RTA Overhead Model</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getRTA_Overhead_Model()
	 * @see #getPacket_Driver()
	 * @generated
	 */
	EAttribute getPacket_Driver_RTA_Overhead_Model();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event <em>Periodic External Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Periodic External Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event
	 * @generated
	 */
	EClass getPeriodic_External_Event();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event#getMax_Jitter <em>Max Jitter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Jitter</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event#getMax_Jitter()
	 * @see #getPeriodic_External_Event()
	 * @generated
	 */
	EAttribute getPeriodic_External_Event_Max_Jitter();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event#getName()
	 * @see #getPeriodic_External_Event()
	 * @generated
	 */
	EAttribute getPeriodic_External_Event_Name();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event#getPeriod <em>Period</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Period</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event#getPeriod()
	 * @see #getPeriodic_External_Event()
	 * @generated
	 */
	EAttribute getPeriodic_External_Event_Period();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event#getPhase <em>Phase</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Phase</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event#getPhase()
	 * @see #getPeriodic_External_Event()
	 * @generated
	 */
	EAttribute getPeriodic_External_Event_Phase();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy <em>Polling Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Polling Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Polling_Policy
	 * @generated
	 */
	EClass getPolling_Policy();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPolling_Avg_Overhead <em>Polling Avg Overhead</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Polling Avg Overhead</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPolling_Avg_Overhead()
	 * @see #getPolling_Policy()
	 * @generated
	 */
	EAttribute getPolling_Policy_Polling_Avg_Overhead();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPolling_Best_Overhead <em>Polling Best Overhead</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Polling Best Overhead</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPolling_Best_Overhead()
	 * @see #getPolling_Policy()
	 * @generated
	 */
	EAttribute getPolling_Policy_Polling_Best_Overhead();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPolling_Period <em>Polling Period</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Polling Period</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPolling_Period()
	 * @see #getPolling_Policy()
	 * @generated
	 */
	EAttribute getPolling_Policy_Polling_Period();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPolling_Worst_Overhead <em>Polling Worst Overhead</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Polling Worst Overhead</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPolling_Worst_Overhead()
	 * @see #getPolling_Policy()
	 * @generated
	 */
	EAttribute getPolling_Policy_Polling_Worst_Overhead();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPreassigned <em>Preassigned</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Preassigned</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPreassigned()
	 * @see #getPolling_Policy()
	 * @generated
	 */
	EAttribute getPolling_Policy_Preassigned();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getThe_Priority <em>The Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>The Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getThe_Priority()
	 * @see #getPolling_Policy()
	 * @generated
	 */
	EAttribute getPolling_Policy_The_Priority();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Primary_Scheduler <em>Primary Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Primary Scheduler</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Primary_Scheduler
	 * @generated
	 */
	EClass getPrimary_Scheduler();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Primary_Scheduler#getFixed_Priority_Scheduler <em>Fixed Priority Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fixed Priority Scheduler</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Primary_Scheduler#getFixed_Priority_Scheduler()
	 * @see #getPrimary_Scheduler()
	 * @generated
	 */
	EReference getPrimary_Scheduler_Fixed_Priority_Scheduler();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Primary_Scheduler#getEDF_Scheduler <em>EDF Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>EDF Scheduler</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Primary_Scheduler#getEDF_Scheduler()
	 * @see #getPrimary_Scheduler()
	 * @generated
	 */
	EReference getPrimary_Scheduler_EDF_Scheduler();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Primary_Scheduler#getFP_Packet_Based_Scheduler <em>FP Packet Based Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>FP Packet Based Scheduler</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Primary_Scheduler#getFP_Packet_Based_Scheduler()
	 * @see #getPrimary_Scheduler()
	 * @generated
	 */
	EReference getPrimary_Scheduler_FP_Packet_Based_Scheduler();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Primary_Scheduler#getHost <em>Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Host</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Primary_Scheduler#getHost()
	 * @see #getPrimary_Scheduler()
	 * @generated
	 */
	EAttribute getPrimary_Scheduler_Host();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Primary_Scheduler#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Primary_Scheduler#getName()
	 * @see #getPrimary_Scheduler()
	 * @generated
	 */
	EAttribute getPrimary_Scheduler_Name();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Resource <em>Priority Inheritance Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Priority Inheritance Resource</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Resource
	 * @generated
	 */
	EClass getPriority_Inheritance_Resource();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Resource#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Resource#getName()
	 * @see #getPriority_Inheritance_Resource()
	 * @generated
	 */
	EAttribute getPriority_Inheritance_Resource_Name();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Query_Server <em>Query Server</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Query Server</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Query_Server
	 * @generated
	 */
	EClass getQuery_Server();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Query_Server#getInput_Event <em>Input Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Input Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Query_Server#getInput_Event()
	 * @see #getQuery_Server()
	 * @generated
	 */
	EAttribute getQuery_Server_Input_Event();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Query_Server#getOutput_Events_List <em>Output Events List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Events List</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Query_Server#getOutput_Events_List()
	 * @see #getQuery_Server()
	 * @generated
	 */
	EAttribute getQuery_Server_Output_Events_List();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Query_Server#getRequest_Policy <em>Request Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Request Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Query_Server#getRequest_Policy()
	 * @see #getQuery_Server()
	 * @generated
	 */
	EAttribute getQuery_Server_Request_Policy();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Rate_Divisor <em>Rate Divisor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rate Divisor</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Rate_Divisor
	 * @generated
	 */
	EClass getRate_Divisor();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Rate_Divisor#getInput_Event <em>Input Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Input Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Rate_Divisor#getInput_Event()
	 * @see #getRate_Divisor()
	 * @generated
	 */
	EAttribute getRate_Divisor_Input_Event();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Rate_Divisor#getOutput_Event <em>Output Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Rate_Divisor#getOutput_Event()
	 * @see #getRate_Divisor()
	 * @generated
	 */
	EAttribute getRate_Divisor_Output_Event();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Rate_Divisor#getRate_Factor <em>Rate Factor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rate Factor</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Rate_Divisor#getRate_Factor()
	 * @see #getRate_Divisor()
	 * @generated
	 */
	EAttribute getRate_Divisor_Rate_Factor();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event <em>Regular Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Regular Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Event
	 * @generated
	 */
	EClass getRegular_Event();

	/**
	 * Returns the meta object for the attribute list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Event#getGroup()
	 * @see #getRegular_Event()
	 * @generated
	 */
	EAttribute getRegular_Event_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getMax_Output_Jitter_Req <em>Max Output Jitter Req</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Max Output Jitter Req</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Event#getMax_Output_Jitter_Req()
	 * @see #getRegular_Event()
	 * @generated
	 */
	EReference getRegular_Event_Max_Output_Jitter_Req();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getHard_Global_Deadline <em>Hard Global Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Hard Global Deadline</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Event#getHard_Global_Deadline()
	 * @see #getRegular_Event()
	 * @generated
	 */
	EReference getRegular_Event_Hard_Global_Deadline();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getSoft_Global_Deadline <em>Soft Global Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Soft Global Deadline</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Event#getSoft_Global_Deadline()
	 * @see #getRegular_Event()
	 * @generated
	 */
	EReference getRegular_Event_Soft_Global_Deadline();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getGlobal_Max_Miss_Ratio <em>Global Max Miss Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Global Max Miss Ratio</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Event#getGlobal_Max_Miss_Ratio()
	 * @see #getRegular_Event()
	 * @generated
	 */
	EReference getRegular_Event_Global_Max_Miss_Ratio();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getHard_Local_Deadline <em>Hard Local Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Hard Local Deadline</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Event#getHard_Local_Deadline()
	 * @see #getRegular_Event()
	 * @generated
	 */
	EReference getRegular_Event_Hard_Local_Deadline();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getSoft_Local_Deadline <em>Soft Local Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Soft Local Deadline</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Event#getSoft_Local_Deadline()
	 * @see #getRegular_Event()
	 * @generated
	 */
	EReference getRegular_Event_Soft_Local_Deadline();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getLocal_Max_Miss_Ratio <em>Local Max Miss Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Local Max Miss Ratio</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Event#getLocal_Max_Miss_Ratio()
	 * @see #getRegular_Event()
	 * @generated
	 */
	EReference getRegular_Event_Local_Max_Miss_Ratio();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getComposite_Timing_Requirement <em>Composite Timing Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Composite Timing Requirement</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Event#getComposite_Timing_Requirement()
	 * @see #getRegular_Event()
	 * @generated
	 */
	EReference getRegular_Event_Composite_Timing_Requirement();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Event#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Event#getEvent()
	 * @see #getRegular_Event()
	 * @generated
	 */
	EAttribute getRegular_Event_Event();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor <em>Regular Processor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Regular Processor</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Processor
	 * @generated
	 */
	EClass getRegular_Processor();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getTicker_System_Timer <em>Ticker System Timer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Ticker System Timer</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getTicker_System_Timer()
	 * @see #getRegular_Processor()
	 * @generated
	 */
	EReference getRegular_Processor_Ticker_System_Timer();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getAlarm_Clock_System_Timer <em>Alarm Clock System Timer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Alarm Clock System Timer</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getAlarm_Clock_System_Timer()
	 * @see #getRegular_Processor()
	 * @generated
	 */
	EReference getRegular_Processor_Alarm_Clock_System_Timer();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getAvg_ISR_Switch <em>Avg ISR Switch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Avg ISR Switch</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getAvg_ISR_Switch()
	 * @see #getRegular_Processor()
	 * @generated
	 */
	EAttribute getRegular_Processor_Avg_ISR_Switch();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getBest_ISR_Switch <em>Best ISR Switch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Best ISR Switch</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getBest_ISR_Switch()
	 * @see #getRegular_Processor()
	 * @generated
	 */
	EAttribute getRegular_Processor_Best_ISR_Switch();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getMax_Interrupt_Priority <em>Max Interrupt Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Interrupt Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getMax_Interrupt_Priority()
	 * @see #getRegular_Processor()
	 * @generated
	 */
	EAttribute getRegular_Processor_Max_Interrupt_Priority();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getMin_Interrupt_Priority <em>Min Interrupt Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Interrupt Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getMin_Interrupt_Priority()
	 * @see #getRegular_Processor()
	 * @generated
	 */
	EAttribute getRegular_Processor_Min_Interrupt_Priority();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getName()
	 * @see #getRegular_Processor()
	 * @generated
	 */
	EAttribute getRegular_Processor_Name();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getSpeed_Factor <em>Speed Factor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Speed Factor</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getSpeed_Factor()
	 * @see #getRegular_Processor()
	 * @generated
	 */
	EAttribute getRegular_Processor_Speed_Factor();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getWorst_ISR_Switch <em>Worst ISR Switch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Worst ISR Switch</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Processor#getWorst_ISR_Switch()
	 * @see #getRegular_Processor()
	 * @generated
	 */
	EAttribute getRegular_Processor_Worst_ISR_Switch();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server <em>Regular Scheduling Server</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Regular Scheduling Server</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server
	 * @generated
	 */
	EClass getRegular_Scheduling_Server();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getNon_Preemptible_FP_Policy <em>Non Preemptible FP Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Non Preemptible FP Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getNon_Preemptible_FP_Policy()
	 * @see #getRegular_Scheduling_Server()
	 * @generated
	 */
	EReference getRegular_Scheduling_Server_Non_Preemptible_FP_Policy();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getFixed_Priority_Policy <em>Fixed Priority Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fixed Priority Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getFixed_Priority_Policy()
	 * @see #getRegular_Scheduling_Server()
	 * @generated
	 */
	EReference getRegular_Scheduling_Server_Fixed_Priority_Policy();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getInterrupt_FP_Policy <em>Interrupt FP Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Interrupt FP Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getInterrupt_FP_Policy()
	 * @see #getRegular_Scheduling_Server()
	 * @generated
	 */
	EReference getRegular_Scheduling_Server_Interrupt_FP_Policy();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getPolling_Policy <em>Polling Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Polling Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getPolling_Policy()
	 * @see #getRegular_Scheduling_Server()
	 * @generated
	 */
	EReference getRegular_Scheduling_Server_Polling_Policy();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getSporadic_Server_Policy <em>Sporadic Server Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sporadic Server Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getSporadic_Server_Policy()
	 * @see #getRegular_Scheduling_Server()
	 * @generated
	 */
	EReference getRegular_Scheduling_Server_Sporadic_Server_Policy();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getEDF_Policy <em>EDF Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>EDF Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getEDF_Policy()
	 * @see #getRegular_Scheduling_Server()
	 * @generated
	 */
	EReference getRegular_Scheduling_Server_EDF_Policy();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getSRP_Parameters <em>SRP Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>SRP Parameters</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getSRP_Parameters()
	 * @see #getRegular_Scheduling_Server()
	 * @generated
	 */
	EReference getRegular_Scheduling_Server_SRP_Parameters();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getName()
	 * @see #getRegular_Scheduling_Server()
	 * @generated
	 */
	EAttribute getRegular_Scheduling_Server_Name();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getScheduler <em>Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Scheduler</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server#getScheduler()
	 * @see #getRegular_Scheduling_Server()
	 * @generated
	 */
	EAttribute getRegular_Scheduling_Server_Scheduler();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction <em>Regular Transaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Regular Transaction</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Transaction
	 * @generated
	 */
	EClass getRegular_Transaction();

	/**
	 * Returns the meta object for the attribute list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getGroup()
	 * @see #getRegular_Transaction()
	 * @generated
	 */
	EAttribute getRegular_Transaction_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getPeriodic_External_Event <em>Periodic External Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Periodic External Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getPeriodic_External_Event()
	 * @see #getRegular_Transaction()
	 * @generated
	 */
	EReference getRegular_Transaction_Periodic_External_Event();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getSporadic_External_Event <em>Sporadic External Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sporadic External Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getSporadic_External_Event()
	 * @see #getRegular_Transaction()
	 * @generated
	 */
	EReference getRegular_Transaction_Sporadic_External_Event();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getUnbounded_External_Event <em>Unbounded External Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Unbounded External Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getUnbounded_External_Event()
	 * @see #getRegular_Transaction()
	 * @generated
	 */
	EReference getRegular_Transaction_Unbounded_External_Event();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getBursty_External_Event <em>Bursty External Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Bursty External Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getBursty_External_Event()
	 * @see #getRegular_Transaction()
	 * @generated
	 */
	EReference getRegular_Transaction_Bursty_External_Event();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getSingular_External_Event <em>Singular External Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Singular External Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getSingular_External_Event()
	 * @see #getRegular_Transaction()
	 * @generated
	 */
	EReference getRegular_Transaction_Singular_External_Event();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getRegular_Event <em>Regular Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Regular Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getRegular_Event()
	 * @see #getRegular_Transaction()
	 * @generated
	 */
	EReference getRegular_Transaction_Regular_Event();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getActivity <em>Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Activity</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getActivity()
	 * @see #getRegular_Transaction()
	 * @generated
	 */
	EReference getRegular_Transaction_Activity();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getSystem_Timed_Activity <em>System Timed Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>System Timed Activity</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getSystem_Timed_Activity()
	 * @see #getRegular_Transaction()
	 * @generated
	 */
	EReference getRegular_Transaction_System_Timed_Activity();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getConcentrator <em>Concentrator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Concentrator</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getConcentrator()
	 * @see #getRegular_Transaction()
	 * @generated
	 */
	EReference getRegular_Transaction_Concentrator();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getBarrier <em>Barrier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Barrier</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getBarrier()
	 * @see #getRegular_Transaction()
	 * @generated
	 */
	EReference getRegular_Transaction_Barrier();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getDelivery_Server <em>Delivery Server</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Delivery Server</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getDelivery_Server()
	 * @see #getRegular_Transaction()
	 * @generated
	 */
	EReference getRegular_Transaction_Delivery_Server();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getQuery_Server <em>Query Server</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Query Server</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getQuery_Server()
	 * @see #getRegular_Transaction()
	 * @generated
	 */
	EReference getRegular_Transaction_Query_Server();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getMulticast <em>Multicast</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Multicast</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getMulticast()
	 * @see #getRegular_Transaction()
	 * @generated
	 */
	EReference getRegular_Transaction_Multicast();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getRate_Divisor <em>Rate Divisor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rate Divisor</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getRate_Divisor()
	 * @see #getRegular_Transaction()
	 * @generated
	 */
	EReference getRegular_Transaction_Rate_Divisor();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getDelay <em>Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Delay</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getDelay()
	 * @see #getRegular_Transaction()
	 * @generated
	 */
	EReference getRegular_Transaction_Delay();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getOffset <em>Offset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Offset</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getOffset()
	 * @see #getRegular_Transaction()
	 * @generated
	 */
	EReference getRegular_Transaction_Offset();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Regular_Transaction#getName()
	 * @see #getRegular_Transaction()
	 * @generated
	 */
	EAttribute getRegular_Transaction_Name();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver <em>RTEP Packet Driver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>RTEP Packet Driver</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver
	 * @generated
	 */
	EClass getRTEP_PacketDriver();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getFailure_Timeout <em>Failure Timeout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Failure Timeout</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getFailure_Timeout()
	 * @see #getRTEP_PacketDriver()
	 * @generated
	 */
	EAttribute getRTEP_PacketDriver_Failure_Timeout();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getMessage_Partitioning <em>Message Partitioning</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Partitioning</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getMessage_Partitioning()
	 * @see #getRTEP_PacketDriver()
	 * @generated
	 */
	EAttribute getRTEP_PacketDriver_Message_Partitioning();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getNumber_Of_Stations <em>Number Of Stations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Of Stations</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getNumber_Of_Stations()
	 * @see #getRTEP_PacketDriver()
	 * @generated
	 */
	EAttribute getRTEP_PacketDriver_Number_Of_Stations();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Discard_Operation <em>Packet Discard Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Packet Discard Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Discard_Operation()
	 * @see #getRTEP_PacketDriver()
	 * @generated
	 */
	EAttribute getRTEP_PacketDriver_Packet_Discard_Operation();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Interrupt_Server <em>Packet Interrupt Server</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Packet Interrupt Server</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Interrupt_Server()
	 * @see #getRTEP_PacketDriver()
	 * @generated
	 */
	EAttribute getRTEP_PacketDriver_Packet_Interrupt_Server();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_ISR_Operation <em>Packet ISR Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Packet ISR Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_ISR_Operation()
	 * @see #getRTEP_PacketDriver()
	 * @generated
	 */
	EAttribute getRTEP_PacketDriver_Packet_ISR_Operation();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Receive_Operation <em>Packet Receive Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Packet Receive Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Receive_Operation()
	 * @see #getRTEP_PacketDriver()
	 * @generated
	 */
	EAttribute getRTEP_PacketDriver_Packet_Receive_Operation();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Retransmission_Operation <em>Packet Retransmission Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Packet Retransmission Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Retransmission_Operation()
	 * @see #getRTEP_PacketDriver()
	 * @generated
	 */
	EAttribute getRTEP_PacketDriver_Packet_Retransmission_Operation();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Send_Operation <em>Packet Send Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Packet Send Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Send_Operation()
	 * @see #getRTEP_PacketDriver()
	 * @generated
	 */
	EAttribute getRTEP_PacketDriver_Packet_Send_Operation();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Server <em>Packet Server</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Packet Server</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Server()
	 * @see #getRTEP_PacketDriver()
	 * @generated
	 */
	EAttribute getRTEP_PacketDriver_Packet_Server();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Transmission_Retries <em>Packet Transmission Retries</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Packet Transmission Retries</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getPacket_Transmission_Retries()
	 * @see #getRTEP_PacketDriver()
	 * @generated
	 */
	EAttribute getRTEP_PacketDriver_Packet_Transmission_Retries();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getRTA_Overhead_Model <em>RTA Overhead Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>RTA Overhead Model</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getRTA_Overhead_Model()
	 * @see #getRTEP_PacketDriver()
	 * @generated
	 */
	EAttribute getRTEP_PacketDriver_RTA_Overhead_Model();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getToken_Check_Operation <em>Token Check Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Token Check Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getToken_Check_Operation()
	 * @see #getRTEP_PacketDriver()
	 * @generated
	 */
	EAttribute getRTEP_PacketDriver_Token_Check_Operation();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getToken_Delay <em>Token Delay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Token Delay</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getToken_Delay()
	 * @see #getRTEP_PacketDriver()
	 * @generated
	 */
	EAttribute getRTEP_PacketDriver_Token_Delay();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getToken_Manage_Operation <em>Token Manage Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Token Manage Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getToken_Manage_Operation()
	 * @see #getRTEP_PacketDriver()
	 * @generated
	 */
	EAttribute getRTEP_PacketDriver_Token_Manage_Operation();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getToken_Retransmission_Operation <em>Token Retransmission Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Token Retransmission Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getToken_Retransmission_Operation()
	 * @see #getRTEP_PacketDriver()
	 * @generated
	 */
	EAttribute getRTEP_PacketDriver_Token_Retransmission_Operation();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getToken_Transmission_Retries <em>Token Transmission Retries</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Token Transmission Retries</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver#getToken_Transmission_Retries()
	 * @see #getRTEP_PacketDriver()
	 * @generated
	 */
	EAttribute getRTEP_PacketDriver_Token_Transmission_Retries();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler <em>Secondary Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Secondary Scheduler</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler
	 * @generated
	 */
	EClass getSecondary_Scheduler();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler#getFixed_Priority_Scheduler <em>Fixed Priority Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fixed Priority Scheduler</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler#getFixed_Priority_Scheduler()
	 * @see #getSecondary_Scheduler()
	 * @generated
	 */
	EReference getSecondary_Scheduler_Fixed_Priority_Scheduler();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler#getEDF_Scheduler <em>EDF Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>EDF Scheduler</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler#getEDF_Scheduler()
	 * @see #getSecondary_Scheduler()
	 * @generated
	 */
	EReference getSecondary_Scheduler_EDF_Scheduler();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler#getFP_Packet_Based_Scheduler <em>FP Packet Based Scheduler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>FP Packet Based Scheduler</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler#getFP_Packet_Based_Scheduler()
	 * @see #getSecondary_Scheduler()
	 * @generated
	 */
	EReference getSecondary_Scheduler_FP_Packet_Based_Scheduler();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler#getHost <em>Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Host</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler#getHost()
	 * @see #getSecondary_Scheduler()
	 * @generated
	 */
	EAttribute getSecondary_Scheduler_Host();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler#getName()
	 * @see #getSecondary_Scheduler()
	 * @generated
	 */
	EAttribute getSecondary_Scheduler_Name();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation <em>Simple Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simple Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Simple_Operation
	 * @generated
	 */
	EClass getSimple_Operation();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getOverridden_Fixed_Priority <em>Overridden Fixed Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Overridden Fixed Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getOverridden_Fixed_Priority()
	 * @see #getSimple_Operation()
	 * @generated
	 */
	EReference getSimple_Operation_Overridden_Fixed_Priority();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getOverridden_Permanent_FP <em>Overridden Permanent FP</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Overridden Permanent FP</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getOverridden_Permanent_FP()
	 * @see #getSimple_Operation()
	 * @generated
	 */
	EReference getSimple_Operation_Overridden_Permanent_FP();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getShared_Resources_List <em>Shared Resources List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Shared Resources List</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getShared_Resources_List()
	 * @see #getSimple_Operation()
	 * @generated
	 */
	EAttribute getSimple_Operation_Shared_Resources_List();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getShared_Resources_To_Lock <em>Shared Resources To Lock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Shared Resources To Lock</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getShared_Resources_To_Lock()
	 * @see #getSimple_Operation()
	 * @generated
	 */
	EAttribute getSimple_Operation_Shared_Resources_To_Lock();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getShared_Resources_To_Unlock <em>Shared Resources To Unlock</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Shared Resources To Unlock</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getShared_Resources_To_Unlock()
	 * @see #getSimple_Operation()
	 * @generated
	 */
	EAttribute getSimple_Operation_Shared_Resources_To_Unlock();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getAverage_Case_Execution_Time <em>Average Case Execution Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Average Case Execution Time</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getAverage_Case_Execution_Time()
	 * @see #getSimple_Operation()
	 * @generated
	 */
	EAttribute getSimple_Operation_Average_Case_Execution_Time();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getBest_Case_Execution_Time <em>Best Case Execution Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Best Case Execution Time</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getBest_Case_Execution_Time()
	 * @see #getSimple_Operation()
	 * @generated
	 */
	EAttribute getSimple_Operation_Best_Case_Execution_Time();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getName()
	 * @see #getSimple_Operation()
	 * @generated
	 */
	EAttribute getSimple_Operation_Name();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getWorst_Case_Execution_Time <em>Worst Case Execution Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Worst Case Execution Time</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Simple_Operation#getWorst_Case_Execution_Time()
	 * @see #getSimple_Operation()
	 * @generated
	 */
	EAttribute getSimple_Operation_Worst_Case_Execution_Time();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Singular_External_Event <em>Singular External Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Singular External Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Singular_External_Event
	 * @generated
	 */
	EClass getSingular_External_Event();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Singular_External_Event#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Singular_External_Event#getName()
	 * @see #getSingular_External_Event()
	 * @generated
	 */
	EAttribute getSingular_External_Event_Name();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Singular_External_Event#getPhase <em>Phase</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Phase</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Singular_External_Event#getPhase()
	 * @see #getSingular_External_Event()
	 * @generated
	 */
	EAttribute getSingular_External_Event_Phase();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Soft_Global_Deadline <em>Soft Global Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Soft Global Deadline</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Soft_Global_Deadline
	 * @generated
	 */
	EClass getSoft_Global_Deadline();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Soft_Global_Deadline#getDeadline <em>Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deadline</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Soft_Global_Deadline#getDeadline()
	 * @see #getSoft_Global_Deadline()
	 * @generated
	 */
	EAttribute getSoft_Global_Deadline_Deadline();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Soft_Global_Deadline#getReferenced_Event <em>Referenced Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Referenced Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Soft_Global_Deadline#getReferenced_Event()
	 * @see #getSoft_Global_Deadline()
	 * @generated
	 */
	EAttribute getSoft_Global_Deadline_Referenced_Event();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Soft_Local_Deadline <em>Soft Local Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Soft Local Deadline</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Soft_Local_Deadline
	 * @generated
	 */
	EClass getSoft_Local_Deadline();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Soft_Local_Deadline#getDeadline <em>Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deadline</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Soft_Local_Deadline#getDeadline()
	 * @see #getSoft_Local_Deadline()
	 * @generated
	 */
	EAttribute getSoft_Local_Deadline_Deadline();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event <em>Sporadic External Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sporadic External Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event
	 * @generated
	 */
	EClass getSporadic_External_Event();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event#getAvg_Interarrival <em>Avg Interarrival</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Avg Interarrival</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event#getAvg_Interarrival()
	 * @see #getSporadic_External_Event()
	 * @generated
	 */
	EAttribute getSporadic_External_Event_Avg_Interarrival();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event#getDistribution <em>Distribution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Distribution</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event#getDistribution()
	 * @see #getSporadic_External_Event()
	 * @generated
	 */
	EAttribute getSporadic_External_Event_Distribution();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event#getMin_Interarrival <em>Min Interarrival</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Interarrival</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event#getMin_Interarrival()
	 * @see #getSporadic_External_Event()
	 * @generated
	 */
	EAttribute getSporadic_External_Event_Min_Interarrival();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event#getName()
	 * @see #getSporadic_External_Event()
	 * @generated
	 */
	EAttribute getSporadic_External_Event_Name();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy <em>Sporadic Server Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sporadic Server Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy
	 * @generated
	 */
	EClass getSporadic_Server_Policy();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getBackground_Priority <em>Background Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Background Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getBackground_Priority()
	 * @see #getSporadic_Server_Policy()
	 * @generated
	 */
	EAttribute getSporadic_Server_Policy_Background_Priority();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getInitial_Capacity <em>Initial Capacity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Capacity</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getInitial_Capacity()
	 * @see #getSporadic_Server_Policy()
	 * @generated
	 */
	EAttribute getSporadic_Server_Policy_Initial_Capacity();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getMax_Pending_Replenishments <em>Max Pending Replenishments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Pending Replenishments</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getMax_Pending_Replenishments()
	 * @see #getSporadic_Server_Policy()
	 * @generated
	 */
	EAttribute getSporadic_Server_Policy_Max_Pending_Replenishments();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getNormal_Priority <em>Normal Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Normal Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getNormal_Priority()
	 * @see #getSporadic_Server_Policy()
	 * @generated
	 */
	EAttribute getSporadic_Server_Policy_Normal_Priority();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getPreassigned <em>Preassigned</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Preassigned</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getPreassigned()
	 * @see #getSporadic_Server_Policy()
	 * @generated
	 */
	EAttribute getSporadic_Server_Policy_Preassigned();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getReplenishment_Period <em>Replenishment Period</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Replenishment Period</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy#getReplenishment_Period()
	 * @see #getSporadic_Server_Policy()
	 * @generated
	 */
	EAttribute getSporadic_Server_Policy_Replenishment_Period();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.SRP_Parameters <em>SRP Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SRP Parameters</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.SRP_Parameters
	 * @generated
	 */
	EClass getSRP_Parameters();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.SRP_Parameters#getPreassigned <em>Preassigned</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Preassigned</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.SRP_Parameters#getPreassigned()
	 * @see #getSRP_Parameters()
	 * @generated
	 */
	EAttribute getSRP_Parameters_Preassigned();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.SRP_Parameters#getPreemption_Level <em>Preemption Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Preemption Level</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.SRP_Parameters#getPreemption_Level()
	 * @see #getSRP_Parameters()
	 * @generated
	 */
	EAttribute getSRP_Parameters_Preemption_Level();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.SRP_Resource <em>SRP Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SRP Resource</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.SRP_Resource
	 * @generated
	 */
	EClass getSRP_Resource();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.SRP_Resource#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.SRP_Resource#getName()
	 * @see #getSRP_Resource()
	 * @generated
	 */
	EAttribute getSRP_Resource_Name();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.SRP_Resource#getPreassigned <em>Preassigned</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Preassigned</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.SRP_Resource#getPreassigned()
	 * @see #getSRP_Resource()
	 * @generated
	 */
	EAttribute getSRP_Resource_Preassigned();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.SRP_Resource#getPreemption_Level <em>Preemption Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Preemption Level</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.SRP_Resource#getPreemption_Level()
	 * @see #getSRP_Resource()
	 * @generated
	 */
	EAttribute getSRP_Resource_Preemption_Level();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity <em>System Timed Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>System Timed Activity</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity
	 * @generated
	 */
	EClass getSystem_Timed_Activity();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity#getActivity_Operation <em>Activity Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Activity Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity#getActivity_Operation()
	 * @see #getSystem_Timed_Activity()
	 * @generated
	 */
	EAttribute getSystem_Timed_Activity_Activity_Operation();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity#getActivity_Server <em>Activity Server</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Activity Server</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity#getActivity_Server()
	 * @see #getSystem_Timed_Activity()
	 * @generated
	 */
	EAttribute getSystem_Timed_Activity_Activity_Server();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity#getInput_Event <em>Input Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Input Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity#getInput_Event()
	 * @see #getSystem_Timed_Activity()
	 * @generated
	 */
	EAttribute getSystem_Timed_Activity_Input_Event();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity#getOutput_Event <em>Output Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity#getOutput_Event()
	 * @see #getSystem_Timed_Activity()
	 * @generated
	 */
	EAttribute getSystem_Timed_Activity_Output_Event();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer <em>Ticker System Timer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ticker System Timer</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer
	 * @generated
	 */
	EClass getTicker_System_Timer();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer#getAvg_Overhead <em>Avg Overhead</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Avg Overhead</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer#getAvg_Overhead()
	 * @see #getTicker_System_Timer()
	 * @generated
	 */
	EAttribute getTicker_System_Timer_Avg_Overhead();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer#getBest_Overhead <em>Best Overhead</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Best Overhead</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer#getBest_Overhead()
	 * @see #getTicker_System_Timer()
	 * @generated
	 */
	EAttribute getTicker_System_Timer_Best_Overhead();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer#getPeriod <em>Period</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Period</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer#getPeriod()
	 * @see #getTicker_System_Timer()
	 * @generated
	 */
	EAttribute getTicker_System_Timer_Period();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer#getWorst_Overhead <em>Worst Overhead</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Worst Overhead</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer#getWorst_Overhead()
	 * @see #getTicker_System_Timer()
	 * @generated
	 */
	EAttribute getTicker_System_Timer_Worst_Overhead();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastmodel.Unbounded_External_Event <em>Unbounded External Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unbounded External Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Unbounded_External_Event
	 * @generated
	 */
	EClass getUnbounded_External_Event();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Unbounded_External_Event#getAvg_Interarrival <em>Avg Interarrival</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Avg Interarrival</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Unbounded_External_Event#getAvg_Interarrival()
	 * @see #getUnbounded_External_Event()
	 * @generated
	 */
	EAttribute getUnbounded_External_Event_Avg_Interarrival();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Unbounded_External_Event#getDistribution <em>Distribution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Distribution</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Unbounded_External_Event#getDistribution()
	 * @see #getUnbounded_External_Event()
	 * @generated
	 */
	EAttribute getUnbounded_External_Event_Distribution();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastmodel.Unbounded_External_Event#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Unbounded_External_Event#getName()
	 * @see #getUnbounded_External_Event()
	 * @generated
	 */
	EAttribute getUnbounded_External_Event_Name();

	/**
	 * Returns the meta object for enum '{@link es.esi.gemde.vv.mast.mastmodel.Affirmative_Assertion <em>Affirmative Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Affirmative Assertion</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Affirmative_Assertion
	 * @generated
	 */
	EEnum getAffirmative_Assertion();

	/**
	 * Returns the meta object for enum '{@link es.esi.gemde.vv.mast.mastmodel.Assertion <em>Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Assertion</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Assertion
	 * @generated
	 */
	EEnum getAssertion();

	/**
	 * Returns the meta object for enum '{@link es.esi.gemde.vv.mast.mastmodel.Delivery_Policy <em>Delivery Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Delivery Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Delivery_Policy
	 * @generated
	 */
	EEnum getDelivery_Policy();

	/**
	 * Returns the meta object for enum '{@link es.esi.gemde.vv.mast.mastmodel.Distribution_Type <em>Distribution Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Distribution Type</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Distribution_Type
	 * @generated
	 */
	EEnum getDistribution_Type();

	/**
	 * Returns the meta object for enum '{@link es.esi.gemde.vv.mast.mastmodel.Negative_Assertion <em>Negative Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Negative Assertion</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Negative_Assertion
	 * @generated
	 */
	EEnum getNegative_Assertion();

	/**
	 * Returns the meta object for enum '{@link es.esi.gemde.vv.mast.mastmodel.Overhead_Type <em>Overhead Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Overhead Type</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Overhead_Type
	 * @generated
	 */
	EEnum getOverhead_Type();

	/**
	 * Returns the meta object for enum '{@link es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Protocol <em>Priority Inheritance Protocol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Priority Inheritance Protocol</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Protocol
	 * @generated
	 */
	EEnum getPriority_Inheritance_Protocol();

	/**
	 * Returns the meta object for enum '{@link es.esi.gemde.vv.mast.mastmodel.Request_Policy <em>Request Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Request Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Request_Policy
	 * @generated
	 */
	EEnum getRequest_Policy();

	/**
	 * Returns the meta object for enum '{@link es.esi.gemde.vv.mast.mastmodel.Transmission_Type <em>Transmission Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Transmission Type</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Transmission_Type
	 * @generated
	 */
	EEnum getTransmission_Type();

	/**
	 * Returns the meta object for data type '<em>Absolute Time</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Absolute Time</em>'.
	 * @model instanceClass="double"
	 *        extendedMetaData="name='Absolute_Time' baseType='http://www.eclipse.org/emf/2003/XMLType#double'"
	 * @generated
	 */
	EDataType getAbsolute_Time();

	/**
	 * Returns the meta object for data type '{@link es.esi.gemde.vv.mast.mastmodel.Affirmative_Assertion <em>Affirmative Assertion Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Affirmative Assertion Object</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Affirmative_Assertion
	 * @model instanceClass="es.esi.gemde.vv.mast.mastmodel.Affirmative_Assertion"
	 *        extendedMetaData="name='Affirmative_Assertion:Object' baseType='Affirmative_Assertion'"
	 * @generated
	 */
	EDataType getAffirmative_Assertion_Object();

	/**
	 * Returns the meta object for data type '<em>Any Priority</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Any Priority</em>'.
	 * @model instanceClass="int"
	 *        extendedMetaData="name='Any_Priority' baseType='http://www.eclipse.org/emf/2003/XMLType#int'"
	 * @generated
	 */
	EDataType getAny_Priority();

	/**
	 * Returns the meta object for data type '{@link es.esi.gemde.vv.mast.mastmodel.Assertion <em>Assertion Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Assertion Object</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Assertion
	 * @model instanceClass="es.esi.gemde.vv.mast.mastmodel.Assertion"
	 *        extendedMetaData="name='Assertion:Object' baseType='Assertion'"
	 * @generated
	 */
	EDataType getAssertion_Object();

	/**
	 * Returns the meta object for data type '<em>Bit Count</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Bit Count</em>'.
	 * @model instanceClass="double"
	 *        extendedMetaData="name='Bit_Count' baseType='http://www.eclipse.org/emf/2003/XMLType#double'"
	 * @generated
	 */
	EDataType getBit_Count();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Date Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Date Time</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 *        extendedMetaData="name='Date_Time' baseType='http://www.eclipse.org/emf/2003/XMLType#string'"
	 * @generated
	 */
	EDataType getDate_Time();

	/**
	 * Returns the meta object for data type '{@link es.esi.gemde.vv.mast.mastmodel.Delivery_Policy <em>Delivery Policy Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Delivery Policy Object</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Delivery_Policy
	 * @model instanceClass="es.esi.gemde.vv.mast.mastmodel.Delivery_Policy"
	 *        extendedMetaData="name='Delivery_Policy:Object' baseType='Delivery_Policy'"
	 * @generated
	 */
	EDataType getDelivery_Policy_Object();

	/**
	 * Returns the meta object for data type '{@link es.esi.gemde.vv.mast.mastmodel.Distribution_Type <em>Distribution Type Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Distribution Type Object</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Distribution_Type
	 * @model instanceClass="es.esi.gemde.vv.mast.mastmodel.Distribution_Type"
	 *        extendedMetaData="name='Distribution_Type:Object' baseType='Distribution_Type'"
	 * @generated
	 */
	EDataType getDistribution_Type_Object();

	/**
	 * Returns the meta object for data type '<em>Float</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Float</em>'.
	 * @model instanceClass="double"
	 *        extendedMetaData="name='Float' baseType='http://www.eclipse.org/emf/2003/XMLType#double'"
	 * @generated
	 */
	EDataType getFloat();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Identifier</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 *        extendedMetaData="name='Identifier' baseType='http://www.eclipse.org/emf/2003/XMLType#NCName' pattern='([a-z]|[A-Z])([a-z]|[A-Z]|[0-9]|.|_)*'"
	 * @generated
	 */
	EDataType getIdentifier();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Identifier Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Identifier Ref</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 *        extendedMetaData="name='Identifier_Ref' baseType='Identifier'"
	 * @generated
	 */
	EDataType getIdentifier_Ref();

	/**
	 * Returns the meta object for data type '{@link java.util.List <em>Identifier Ref List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Identifier Ref List</em>'.
	 * @see java.util.List
	 * @model instanceClass="java.util.List"
	 *        extendedMetaData="name='Identifier_Ref_List' baseType='http://www.eclipse.org/emf/2003/XMLType#NMTOKENS'"
	 * @generated
	 */
	EDataType getIdentifier_Ref_List();

	/**
	 * Returns the meta object for data type '<em>Interrupt Priority</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Interrupt Priority</em>'.
	 * @model instanceClass="int"
	 *        extendedMetaData="name='Interrupt_Priority' baseType='Any_Priority'"
	 * @generated
	 */
	EDataType getInterrupt_Priority();

	/**
	 * Returns the meta object for data type '<em>Natural</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Natural</em>'.
	 * @model instanceClass="int"
	 *        extendedMetaData="name='Natural' baseType='http://www.eclipse.org/emf/2003/XMLType#int'"
	 * @generated
	 */
	EDataType getNatural();

	/**
	 * Returns the meta object for data type '{@link es.esi.gemde.vv.mast.mastmodel.Negative_Assertion <em>Negative Assertion Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Negative Assertion Object</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Negative_Assertion
	 * @model instanceClass="es.esi.gemde.vv.mast.mastmodel.Negative_Assertion"
	 *        extendedMetaData="name='Negative_Assertion:Object' baseType='Negative_Assertion'"
	 * @generated
	 */
	EDataType getNegative_Assertion_Object();

	/**
	 * Returns the meta object for data type '<em>Normalized Execution Time</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Normalized Execution Time</em>'.
	 * @model instanceClass="double"
	 *        extendedMetaData="name='Normalized_Execution_Time' baseType='http://www.eclipse.org/emf/2003/XMLType#double'"
	 * @generated
	 */
	EDataType getNormalized_Execution_Time();

	/**
	 * Returns the meta object for data type '{@link es.esi.gemde.vv.mast.mastmodel.Overhead_Type <em>Overhead Type Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Overhead Type Object</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Overhead_Type
	 * @model instanceClass="es.esi.gemde.vv.mast.mastmodel.Overhead_Type"
	 *        extendedMetaData="name='Overhead_Type:Object' baseType='Overhead_Type'"
	 * @generated
	 */
	EDataType getOverhead_Type_Object();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Pathname</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Pathname</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 *        extendedMetaData="name='Pathname' baseType='http://www.eclipse.org/emf/2003/XMLType#string'"
	 * @generated
	 */
	EDataType getPathname();

	/**
	 * Returns the meta object for data type '<em>Percentage</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Percentage</em>'.
	 * @model instanceClass="double"
	 *        extendedMetaData="name='Percentage' baseType='http://www.eclipse.org/emf/2003/XMLType#double'"
	 * @generated
	 */
	EDataType getPercentage();

	/**
	 * Returns the meta object for data type '<em>Positive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Positive</em>'.
	 * @model instanceClass="int"
	 *        extendedMetaData="name='Positive' baseType='http://www.eclipse.org/emf/2003/XMLType#int'"
	 * @generated
	 */
	EDataType getPositive();

	/**
	 * Returns the meta object for data type '<em>Preemption Level</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Preemption Level</em>'.
	 * @model instanceClass="int"
	 *        extendedMetaData="name='Preemption_Level' baseType='http://www.eclipse.org/emf/2003/XMLType#int'"
	 * @generated
	 */
	EDataType getPreemption_Level();

	/**
	 * Returns the meta object for data type '<em>Priority</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Priority</em>'.
	 * @model instanceClass="int"
	 *        extendedMetaData="name='Priority' baseType='Any_Priority'"
	 * @generated
	 */
	EDataType getPriority();

	/**
	 * Returns the meta object for data type '{@link es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Protocol <em>Priority Inheritance Protocol Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Priority Inheritance Protocol Object</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Protocol
	 * @model instanceClass="es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Protocol"
	 *        extendedMetaData="name='Priority_Inheritance_Protocol:Object' baseType='Priority_Inheritance_Protocol'"
	 * @generated
	 */
	EDataType getPriority_Inheritance_Protocol_Object();

	/**
	 * Returns the meta object for data type '{@link es.esi.gemde.vv.mast.mastmodel.Request_Policy <em>Request Policy Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Request Policy Object</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Request_Policy
	 * @model instanceClass="es.esi.gemde.vv.mast.mastmodel.Request_Policy"
	 *        extendedMetaData="name='Request_Policy:Object' baseType='Request_Policy'"
	 * @generated
	 */
	EDataType getRequest_Policy_Object();

	/**
	 * Returns the meta object for data type '<em>Throughput</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Throughput</em>'.
	 * @model instanceClass="double"
	 *        extendedMetaData="name='Throughput' baseType='http://www.eclipse.org/emf/2003/XMLType#double'"
	 * @generated
	 */
	EDataType getThroughput();

	/**
	 * Returns the meta object for data type '<em>Time</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Time</em>'.
	 * @model instanceClass="double"
	 *        extendedMetaData="name='Time' baseType='http://www.eclipse.org/emf/2003/XMLType#double'"
	 * @generated
	 */
	EDataType getTime();

	/**
	 * Returns the meta object for data type '{@link es.esi.gemde.vv.mast.mastmodel.Transmission_Type <em>Transmission Type Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Transmission Type Object</em>'.
	 * @see es.esi.gemde.vv.mast.mastmodel.Transmission_Type
	 * @model instanceClass="es.esi.gemde.vv.mast.mastmodel.Transmission_Type"
	 *        extendedMetaData="name='Transmission_Type:Object' baseType='Transmission_Type'"
	 * @generated
	 */
	EDataType getTransmission_Type_Object();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ModelFactory getModelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.ActivityImpl <em>Activity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ActivityImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getActivity()
		 * @generated
		 */
		EClass ACTIVITY = eINSTANCE.getActivity();

		/**
		 * The meta object literal for the '<em><b>Activity Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVITY__ACTIVITY_OPERATION = eINSTANCE.getActivity_Activity_Operation();

		/**
		 * The meta object literal for the '<em><b>Activity Server</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVITY__ACTIVITY_SERVER = eINSTANCE.getActivity_Activity_Server();

		/**
		 * The meta object literal for the '<em><b>Input Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVITY__INPUT_EVENT = eINSTANCE.getActivity_Input_Event();

		/**
		 * The meta object literal for the '<em><b>Output Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVITY__OUTPUT_EVENT = eINSTANCE.getActivity_Output_Event();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Alarm_Clock_System_TimerImpl <em>Alarm Clock System Timer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Alarm_Clock_System_TimerImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getAlarm_Clock_System_Timer()
		 * @generated
		 */
		EClass ALARM_CLOCK_SYSTEM_TIMER = eINSTANCE.getAlarm_Clock_System_Timer();

		/**
		 * The meta object literal for the '<em><b>Avg Overhead</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ALARM_CLOCK_SYSTEM_TIMER__AVG_OVERHEAD = eINSTANCE.getAlarm_Clock_System_Timer_Avg_Overhead();

		/**
		 * The meta object literal for the '<em><b>Best Overhead</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ALARM_CLOCK_SYSTEM_TIMER__BEST_OVERHEAD = eINSTANCE.getAlarm_Clock_System_Timer_Best_Overhead();

		/**
		 * The meta object literal for the '<em><b>Worst Overhead</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ALARM_CLOCK_SYSTEM_TIMER__WORST_OVERHEAD = eINSTANCE.getAlarm_Clock_System_Timer_Worst_Overhead();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.BarrierImpl <em>Barrier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.BarrierImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getBarrier()
		 * @generated
		 */
		EClass BARRIER = eINSTANCE.getBarrier();

		/**
		 * The meta object literal for the '<em><b>Input Events List</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BARRIER__INPUT_EVENTS_LIST = eINSTANCE.getBarrier_Input_Events_List();

		/**
		 * The meta object literal for the '<em><b>Output Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BARRIER__OUTPUT_EVENT = eINSTANCE.getBarrier_Output_Event();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Bursty_External_EventImpl <em>Bursty External Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Bursty_External_EventImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getBursty_External_Event()
		 * @generated
		 */
		EClass BURSTY_EXTERNAL_EVENT = eINSTANCE.getBursty_External_Event();

		/**
		 * The meta object literal for the '<em><b>Avg Interarrival</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BURSTY_EXTERNAL_EVENT__AVG_INTERARRIVAL = eINSTANCE.getBursty_External_Event_Avg_Interarrival();

		/**
		 * The meta object literal for the '<em><b>Bound Interval</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BURSTY_EXTERNAL_EVENT__BOUND_INTERVAL = eINSTANCE.getBursty_External_Event_Bound_Interval();

		/**
		 * The meta object literal for the '<em><b>Distribution</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BURSTY_EXTERNAL_EVENT__DISTRIBUTION = eINSTANCE.getBursty_External_Event_Distribution();

		/**
		 * The meta object literal for the '<em><b>Max Arrivals</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BURSTY_EXTERNAL_EVENT__MAX_ARRIVALS = eINSTANCE.getBursty_External_Event_Max_Arrivals();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BURSTY_EXTERNAL_EVENT__NAME = eINSTANCE.getBursty_External_Event_Name();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Character_Packet_DriverImpl <em>Character Packet Driver</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Character_Packet_DriverImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getCharacter_Packet_Driver()
		 * @generated
		 */
		EClass CHARACTER_PACKET_DRIVER = eINSTANCE.getCharacter_Packet_Driver();

		/**
		 * The meta object literal for the '<em><b>Character Receive Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHARACTER_PACKET_DRIVER__CHARACTER_RECEIVE_OPERATION = eINSTANCE.getCharacter_Packet_Driver_Character_Receive_Operation();

		/**
		 * The meta object literal for the '<em><b>Character Send Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHARACTER_PACKET_DRIVER__CHARACTER_SEND_OPERATION = eINSTANCE.getCharacter_Packet_Driver_Character_Send_Operation();

		/**
		 * The meta object literal for the '<em><b>Character Server</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHARACTER_PACKET_DRIVER__CHARACTER_SERVER = eINSTANCE.getCharacter_Packet_Driver_Character_Server();

		/**
		 * The meta object literal for the '<em><b>Character Transmission Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHARACTER_PACKET_DRIVER__CHARACTER_TRANSMISSION_TIME = eINSTANCE.getCharacter_Packet_Driver_Character_Transmission_Time();

		/**
		 * The meta object literal for the '<em><b>Message Partitioning</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHARACTER_PACKET_DRIVER__MESSAGE_PARTITIONING = eINSTANCE.getCharacter_Packet_Driver_Message_Partitioning();

		/**
		 * The meta object literal for the '<em><b>Packet Receive Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHARACTER_PACKET_DRIVER__PACKET_RECEIVE_OPERATION = eINSTANCE.getCharacter_Packet_Driver_Packet_Receive_Operation();

		/**
		 * The meta object literal for the '<em><b>Packet Send Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHARACTER_PACKET_DRIVER__PACKET_SEND_OPERATION = eINSTANCE.getCharacter_Packet_Driver_Packet_Send_Operation();

		/**
		 * The meta object literal for the '<em><b>Packet Server</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHARACTER_PACKET_DRIVER__PACKET_SERVER = eINSTANCE.getCharacter_Packet_Driver_Packet_Server();

		/**
		 * The meta object literal for the '<em><b>RTA Overhead Model</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHARACTER_PACKET_DRIVER__RTA_OVERHEAD_MODEL = eINSTANCE.getCharacter_Packet_Driver_RTA_Overhead_Model();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Composite_OperationImpl <em>Composite Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Composite_OperationImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getComposite_Operation()
		 * @generated
		 */
		EClass COMPOSITE_OPERATION = eINSTANCE.getComposite_Operation();

		/**
		 * The meta object literal for the '<em><b>Overridden Fixed Priority</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_OPERATION__OVERRIDDEN_FIXED_PRIORITY = eINSTANCE.getComposite_Operation_Overridden_Fixed_Priority();

		/**
		 * The meta object literal for the '<em><b>Overridden Permanent FP</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_OPERATION__OVERRIDDEN_PERMANENT_FP = eINSTANCE.getComposite_Operation_Overridden_Permanent_FP();

		/**
		 * The meta object literal for the '<em><b>Operation List</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPOSITE_OPERATION__OPERATION_LIST = eINSTANCE.getComposite_Operation_Operation_List();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPOSITE_OPERATION__NAME = eINSTANCE.getComposite_Operation_Name();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Composite_Timing_RequirementImpl <em>Composite Timing Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Composite_Timing_RequirementImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getComposite_Timing_Requirement()
		 * @generated
		 */
		EClass COMPOSITE_TIMING_REQUIREMENT = eINSTANCE.getComposite_Timing_Requirement();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPOSITE_TIMING_REQUIREMENT__GROUP = eINSTANCE.getComposite_Timing_Requirement_Group();

		/**
		 * The meta object literal for the '<em><b>Max Output Jitter Req</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_TIMING_REQUIREMENT__MAX_OUTPUT_JITTER_REQ = eINSTANCE.getComposite_Timing_Requirement_Max_Output_Jitter_Req();

		/**
		 * The meta object literal for the '<em><b>Hard Global Deadline</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_TIMING_REQUIREMENT__HARD_GLOBAL_DEADLINE = eINSTANCE.getComposite_Timing_Requirement_Hard_Global_Deadline();

		/**
		 * The meta object literal for the '<em><b>Soft Global Deadline</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_TIMING_REQUIREMENT__SOFT_GLOBAL_DEADLINE = eINSTANCE.getComposite_Timing_Requirement_Soft_Global_Deadline();

		/**
		 * The meta object literal for the '<em><b>Global Max Miss Ratio</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_TIMING_REQUIREMENT__GLOBAL_MAX_MISS_RATIO = eINSTANCE.getComposite_Timing_Requirement_Global_Max_Miss_Ratio();

		/**
		 * The meta object literal for the '<em><b>Hard Local Deadline</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_TIMING_REQUIREMENT__HARD_LOCAL_DEADLINE = eINSTANCE.getComposite_Timing_Requirement_Hard_Local_Deadline();

		/**
		 * The meta object literal for the '<em><b>Soft Local Deadline</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_TIMING_REQUIREMENT__SOFT_LOCAL_DEADLINE = eINSTANCE.getComposite_Timing_Requirement_Soft_Local_Deadline();

		/**
		 * The meta object literal for the '<em><b>Local Max Miss Ratio</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_TIMING_REQUIREMENT__LOCAL_MAX_MISS_RATIO = eINSTANCE.getComposite_Timing_Requirement_Local_Max_Miss_Ratio();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.ConcentratorImpl <em>Concentrator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ConcentratorImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getConcentrator()
		 * @generated
		 */
		EClass CONCENTRATOR = eINSTANCE.getConcentrator();

		/**
		 * The meta object literal for the '<em><b>Input Events List</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONCENTRATOR__INPUT_EVENTS_LIST = eINSTANCE.getConcentrator_Input_Events_List();

		/**
		 * The meta object literal for the '<em><b>Output Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONCENTRATOR__OUTPUT_EVENT = eINSTANCE.getConcentrator_Output_Event();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.DelayImpl <em>Delay</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.DelayImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getDelay()
		 * @generated
		 */
		EClass DELAY = eINSTANCE.getDelay();

		/**
		 * The meta object literal for the '<em><b>Delay Max Interval</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DELAY__DELAY_MAX_INTERVAL = eINSTANCE.getDelay_Delay_Max_Interval();

		/**
		 * The meta object literal for the '<em><b>Delay Min Interval</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DELAY__DELAY_MIN_INTERVAL = eINSTANCE.getDelay_Delay_Min_Interval();

		/**
		 * The meta object literal for the '<em><b>Input Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DELAY__INPUT_EVENT = eINSTANCE.getDelay_Input_Event();

		/**
		 * The meta object literal for the '<em><b>Output Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DELAY__OUTPUT_EVENT = eINSTANCE.getDelay_Output_Event();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Delivery_ServerImpl <em>Delivery Server</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Delivery_ServerImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getDelivery_Server()
		 * @generated
		 */
		EClass DELIVERY_SERVER = eINSTANCE.getDelivery_Server();

		/**
		 * The meta object literal for the '<em><b>Delivery Policy</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DELIVERY_SERVER__DELIVERY_POLICY = eINSTANCE.getDelivery_Server_Delivery_Policy();

		/**
		 * The meta object literal for the '<em><b>Input Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DELIVERY_SERVER__INPUT_EVENT = eINSTANCE.getDelivery_Server_Input_Event();

		/**
		 * The meta object literal for the '<em><b>Output Events List</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DELIVERY_SERVER__OUTPUT_EVENTS_LIST = eINSTANCE.getDelivery_Server_Output_Events_List();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Document_RootImpl <em>Document Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Document_RootImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getDocument_Root()
		 * @generated
		 */
		EClass DOCUMENT_ROOT = eINSTANCE.getDocument_Root();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocument_Root_Mixed();

		/**
		 * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocument_Root_XMLNSPrefixMap();

		/**
		 * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocument_Root_XSISchemaLocation();

		/**
		 * The meta object literal for the '<em><b>MAST MODEL</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__MAST_MODEL = eINSTANCE.getDocument_Root_MAST_MODEL();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.EDF_PolicyImpl <em>EDF Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.EDF_PolicyImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getEDF_Policy()
		 * @generated
		 */
		EClass EDF_POLICY = eINSTANCE.getEDF_Policy();

		/**
		 * The meta object literal for the '<em><b>Deadline</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDF_POLICY__DEADLINE = eINSTANCE.getEDF_Policy_Deadline();

		/**
		 * The meta object literal for the '<em><b>Preassigned</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDF_POLICY__PREASSIGNED = eINSTANCE.getEDF_Policy_Preassigned();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.EDF_SchedulerImpl <em>EDF Scheduler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.EDF_SchedulerImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getEDF_Scheduler()
		 * @generated
		 */
		EClass EDF_SCHEDULER = eINSTANCE.getEDF_Scheduler();

		/**
		 * The meta object literal for the '<em><b>Avg Context Switch</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDF_SCHEDULER__AVG_CONTEXT_SWITCH = eINSTANCE.getEDF_Scheduler_Avg_Context_Switch();

		/**
		 * The meta object literal for the '<em><b>Best Context Switch</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDF_SCHEDULER__BEST_CONTEXT_SWITCH = eINSTANCE.getEDF_Scheduler_Best_Context_Switch();

		/**
		 * The meta object literal for the '<em><b>Worst Context Switch</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDF_SCHEDULER__WORST_CONTEXT_SWITCH = eINSTANCE.getEDF_Scheduler_Worst_Context_Switch();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Enclosing_OperationImpl <em>Enclosing Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Enclosing_OperationImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getEnclosing_Operation()
		 * @generated
		 */
		EClass ENCLOSING_OPERATION = eINSTANCE.getEnclosing_Operation();

		/**
		 * The meta object literal for the '<em><b>Overridden Fixed Priority</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENCLOSING_OPERATION__OVERRIDDEN_FIXED_PRIORITY = eINSTANCE.getEnclosing_Operation_Overridden_Fixed_Priority();

		/**
		 * The meta object literal for the '<em><b>Overridden Permanent FP</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENCLOSING_OPERATION__OVERRIDDEN_PERMANENT_FP = eINSTANCE.getEnclosing_Operation_Overridden_Permanent_FP();

		/**
		 * The meta object literal for the '<em><b>Operation List</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENCLOSING_OPERATION__OPERATION_LIST = eINSTANCE.getEnclosing_Operation_Operation_List();

		/**
		 * The meta object literal for the '<em><b>Average Case Execution Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENCLOSING_OPERATION__AVERAGE_CASE_EXECUTION_TIME = eINSTANCE.getEnclosing_Operation_Average_Case_Execution_Time();

		/**
		 * The meta object literal for the '<em><b>Best Case Execution Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENCLOSING_OPERATION__BEST_CASE_EXECUTION_TIME = eINSTANCE.getEnclosing_Operation_Best_Case_Execution_Time();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENCLOSING_OPERATION__NAME = eINSTANCE.getEnclosing_Operation_Name();

		/**
		 * The meta object literal for the '<em><b>Worst Case Execution Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENCLOSING_OPERATION__WORST_CASE_EXECUTION_TIME = eINSTANCE.getEnclosing_Operation_Worst_Case_Execution_Time();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Fixed_Priority_PolicyImpl <em>Fixed Priority Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Fixed_Priority_PolicyImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getFixed_Priority_Policy()
		 * @generated
		 */
		EClass FIXED_PRIORITY_POLICY = eINSTANCE.getFixed_Priority_Policy();

		/**
		 * The meta object literal for the '<em><b>Preassigned</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_PRIORITY_POLICY__PREASSIGNED = eINSTANCE.getFixed_Priority_Policy_Preassigned();

		/**
		 * The meta object literal for the '<em><b>The Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_PRIORITY_POLICY__THE_PRIORITY = eINSTANCE.getFixed_Priority_Policy_The_Priority();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Fixed_Priority_SchedulerImpl <em>Fixed Priority Scheduler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Fixed_Priority_SchedulerImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getFixed_Priority_Scheduler()
		 * @generated
		 */
		EClass FIXED_PRIORITY_SCHEDULER = eINSTANCE.getFixed_Priority_Scheduler();

		/**
		 * The meta object literal for the '<em><b>Avg Context Switch</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_PRIORITY_SCHEDULER__AVG_CONTEXT_SWITCH = eINSTANCE.getFixed_Priority_Scheduler_Avg_Context_Switch();

		/**
		 * The meta object literal for the '<em><b>Best Context Switch</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_PRIORITY_SCHEDULER__BEST_CONTEXT_SWITCH = eINSTANCE.getFixed_Priority_Scheduler_Best_Context_Switch();

		/**
		 * The meta object literal for the '<em><b>Max Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_PRIORITY_SCHEDULER__MAX_PRIORITY = eINSTANCE.getFixed_Priority_Scheduler_Max_Priority();

		/**
		 * The meta object literal for the '<em><b>Min Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_PRIORITY_SCHEDULER__MIN_PRIORITY = eINSTANCE.getFixed_Priority_Scheduler_Min_Priority();

		/**
		 * The meta object literal for the '<em><b>Worst Context Switch</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_PRIORITY_SCHEDULER__WORST_CONTEXT_SWITCH = eINSTANCE.getFixed_Priority_Scheduler_Worst_Context_Switch();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.FP_Packet_Based_SchedulerImpl <em>FP Packet Based Scheduler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.FP_Packet_Based_SchedulerImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getFP_Packet_Based_Scheduler()
		 * @generated
		 */
		EClass FP_PACKET_BASED_SCHEDULER = eINSTANCE.getFP_Packet_Based_Scheduler();

		/**
		 * The meta object literal for the '<em><b>Max Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FP_PACKET_BASED_SCHEDULER__MAX_PRIORITY = eINSTANCE.getFP_Packet_Based_Scheduler_Max_Priority();

		/**
		 * The meta object literal for the '<em><b>Min Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FP_PACKET_BASED_SCHEDULER__MIN_PRIORITY = eINSTANCE.getFP_Packet_Based_Scheduler_Min_Priority();

		/**
		 * The meta object literal for the '<em><b>Packet Overhead Avg Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_AVG_SIZE = eINSTANCE.getFP_Packet_Based_Scheduler_Packet_Overhead_Avg_Size();

		/**
		 * The meta object literal for the '<em><b>Packet Overhead Max Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_MAX_SIZE = eINSTANCE.getFP_Packet_Based_Scheduler_Packet_Overhead_Max_Size();

		/**
		 * The meta object literal for the '<em><b>Packet Overhead Min Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_MIN_SIZE = eINSTANCE.getFP_Packet_Based_Scheduler_Packet_Overhead_Min_Size();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Global_Max_Miss_RatioImpl <em>Global Max Miss Ratio</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Global_Max_Miss_RatioImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getGlobal_Max_Miss_Ratio()
		 * @generated
		 */
		EClass GLOBAL_MAX_MISS_RATIO = eINSTANCE.getGlobal_Max_Miss_Ratio();

		/**
		 * The meta object literal for the '<em><b>Deadline</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GLOBAL_MAX_MISS_RATIO__DEADLINE = eINSTANCE.getGlobal_Max_Miss_Ratio_Deadline();

		/**
		 * The meta object literal for the '<em><b>Ratio</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GLOBAL_MAX_MISS_RATIO__RATIO = eINSTANCE.getGlobal_Max_Miss_Ratio_Ratio();

		/**
		 * The meta object literal for the '<em><b>Referenced Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GLOBAL_MAX_MISS_RATIO__REFERENCED_EVENT = eINSTANCE.getGlobal_Max_Miss_Ratio_Referenced_Event();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Hard_Global_DeadlineImpl <em>Hard Global Deadline</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Hard_Global_DeadlineImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getHard_Global_Deadline()
		 * @generated
		 */
		EClass HARD_GLOBAL_DEADLINE = eINSTANCE.getHard_Global_Deadline();

		/**
		 * The meta object literal for the '<em><b>Deadline</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HARD_GLOBAL_DEADLINE__DEADLINE = eINSTANCE.getHard_Global_Deadline_Deadline();

		/**
		 * The meta object literal for the '<em><b>Referenced Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HARD_GLOBAL_DEADLINE__REFERENCED_EVENT = eINSTANCE.getHard_Global_Deadline_Referenced_Event();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Hard_Local_DeadlineImpl <em>Hard Local Deadline</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Hard_Local_DeadlineImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getHard_Local_Deadline()
		 * @generated
		 */
		EClass HARD_LOCAL_DEADLINE = eINSTANCE.getHard_Local_Deadline();

		/**
		 * The meta object literal for the '<em><b>Deadline</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HARD_LOCAL_DEADLINE__DEADLINE = eINSTANCE.getHard_Local_Deadline_Deadline();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Immediate_Ceiling_ResourceImpl <em>Immediate Ceiling Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Immediate_Ceiling_ResourceImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getImmediate_Ceiling_Resource()
		 * @generated
		 */
		EClass IMMEDIATE_CEILING_RESOURCE = eINSTANCE.getImmediate_Ceiling_Resource();

		/**
		 * The meta object literal for the '<em><b>Ceiling</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMMEDIATE_CEILING_RESOURCE__CEILING = eINSTANCE.getImmediate_Ceiling_Resource_Ceiling();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMMEDIATE_CEILING_RESOURCE__NAME = eINSTANCE.getImmediate_Ceiling_Resource_Name();

		/**
		 * The meta object literal for the '<em><b>Preassigned</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMMEDIATE_CEILING_RESOURCE__PREASSIGNED = eINSTANCE.getImmediate_Ceiling_Resource_Preassigned();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Interrupt_FP_PolicyImpl <em>Interrupt FP Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Interrupt_FP_PolicyImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getInterrupt_FP_Policy()
		 * @generated
		 */
		EClass INTERRUPT_FP_POLICY = eINSTANCE.getInterrupt_FP_Policy();

		/**
		 * The meta object literal for the '<em><b>Preassigned</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERRUPT_FP_POLICY__PREASSIGNED = eINSTANCE.getInterrupt_FP_Policy_Preassigned();

		/**
		 * The meta object literal for the '<em><b>The Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERRUPT_FP_POLICY__THE_PRIORITY = eINSTANCE.getInterrupt_FP_Policy_The_Priority();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.List_of_DriversImpl <em>List of Drivers</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.List_of_DriversImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getList_of_Drivers()
		 * @generated
		 */
		EClass LIST_OF_DRIVERS = eINSTANCE.getList_of_Drivers();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LIST_OF_DRIVERS__GROUP = eINSTANCE.getList_of_Drivers_Group();

		/**
		 * The meta object literal for the '<em><b>Packet Driver</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIST_OF_DRIVERS__PACKET_DRIVER = eINSTANCE.getList_of_Drivers_Packet_Driver();

		/**
		 * The meta object literal for the '<em><b>Character Packet Driver</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIST_OF_DRIVERS__CHARACTER_PACKET_DRIVER = eINSTANCE.getList_of_Drivers_Character_Packet_Driver();

		/**
		 * The meta object literal for the '<em><b>RTEP Packet Driver</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LIST_OF_DRIVERS__RTEP_PACKET_DRIVER = eINSTANCE.getList_of_Drivers_RTEP_Packet_Driver();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Local_Max_Miss_RatioImpl <em>Local Max Miss Ratio</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Local_Max_Miss_RatioImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getLocal_Max_Miss_Ratio()
		 * @generated
		 */
		EClass LOCAL_MAX_MISS_RATIO = eINSTANCE.getLocal_Max_Miss_Ratio();

		/**
		 * The meta object literal for the '<em><b>Deadline</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCAL_MAX_MISS_RATIO__DEADLINE = eINSTANCE.getLocal_Max_Miss_Ratio_Deadline();

		/**
		 * The meta object literal for the '<em><b>Ratio</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCAL_MAX_MISS_RATIO__RATIO = eINSTANCE.getLocal_Max_Miss_Ratio_Ratio();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl <em>MAST MODEL</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.MAST_MODELImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getMAST_MODEL()
		 * @generated
		 */
		EClass MAST_MODEL = eINSTANCE.getMAST_MODEL();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAST_MODEL__GROUP = eINSTANCE.getMAST_MODEL_Group();

		/**
		 * The meta object literal for the '<em><b>Regular Processor</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAST_MODEL__REGULAR_PROCESSOR = eINSTANCE.getMAST_MODEL_Regular_Processor();

		/**
		 * The meta object literal for the '<em><b>Packet Based Network</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAST_MODEL__PACKET_BASED_NETWORK = eINSTANCE.getMAST_MODEL_Packet_Based_Network();

		/**
		 * The meta object literal for the '<em><b>Regular Scheduling Server</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAST_MODEL__REGULAR_SCHEDULING_SERVER = eINSTANCE.getMAST_MODEL_Regular_Scheduling_Server();

		/**
		 * The meta object literal for the '<em><b>Immediate Ceiling Resource</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAST_MODEL__IMMEDIATE_CEILING_RESOURCE = eINSTANCE.getMAST_MODEL_Immediate_Ceiling_Resource();

		/**
		 * The meta object literal for the '<em><b>Priority Inheritance Resource</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAST_MODEL__PRIORITY_INHERITANCE_RESOURCE = eINSTANCE.getMAST_MODEL_Priority_Inheritance_Resource();

		/**
		 * The meta object literal for the '<em><b>SRP Resource</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAST_MODEL__SRP_RESOURCE = eINSTANCE.getMAST_MODEL_SRP_Resource();

		/**
		 * The meta object literal for the '<em><b>Simple Operation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAST_MODEL__SIMPLE_OPERATION = eINSTANCE.getMAST_MODEL_Simple_Operation();

		/**
		 * The meta object literal for the '<em><b>Message Transmission</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAST_MODEL__MESSAGE_TRANSMISSION = eINSTANCE.getMAST_MODEL_Message_Transmission();

		/**
		 * The meta object literal for the '<em><b>Composite Operation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAST_MODEL__COMPOSITE_OPERATION = eINSTANCE.getMAST_MODEL_Composite_Operation();

		/**
		 * The meta object literal for the '<em><b>Enclosing Operation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAST_MODEL__ENCLOSING_OPERATION = eINSTANCE.getMAST_MODEL_Enclosing_Operation();

		/**
		 * The meta object literal for the '<em><b>Regular Transaction</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAST_MODEL__REGULAR_TRANSACTION = eINSTANCE.getMAST_MODEL_Regular_Transaction();

		/**
		 * The meta object literal for the '<em><b>Primary Scheduler</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAST_MODEL__PRIMARY_SCHEDULER = eINSTANCE.getMAST_MODEL_Primary_Scheduler();

		/**
		 * The meta object literal for the '<em><b>Secondary Scheduler</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAST_MODEL__SECONDARY_SCHEDULER = eINSTANCE.getMAST_MODEL_Secondary_Scheduler();

		/**
		 * The meta object literal for the '<em><b>Model Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAST_MODEL__MODEL_DATE = eINSTANCE.getMAST_MODEL_Model_Date();

		/**
		 * The meta object literal for the '<em><b>Model Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAST_MODEL__MODEL_NAME = eINSTANCE.getMAST_MODEL_Model_Name();

		/**
		 * The meta object literal for the '<em><b>System Pi PBehaviour</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAST_MODEL__SYSTEM_PI_PBEHAVIOUR = eINSTANCE.getMAST_MODEL_System_PiP_Behaviour();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Max_Output_Jitter_ReqImpl <em>Max Output Jitter Req</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Max_Output_Jitter_ReqImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getMax_Output_Jitter_Req()
		 * @generated
		 */
		EClass MAX_OUTPUT_JITTER_REQ = eINSTANCE.getMax_Output_Jitter_Req();

		/**
		 * The meta object literal for the '<em><b>Max Output Jitter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAX_OUTPUT_JITTER_REQ__MAX_OUTPUT_JITTER = eINSTANCE.getMax_Output_Jitter_Req_Max_Output_Jitter();

		/**
		 * The meta object literal for the '<em><b>Referenced Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAX_OUTPUT_JITTER_REQ__REFERENCED_EVENT = eINSTANCE.getMax_Output_Jitter_Req_Referenced_Event();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Message_TransmissionImpl <em>Message Transmission</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Message_TransmissionImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getMessage_Transmission()
		 * @generated
		 */
		EClass MESSAGE_TRANSMISSION = eINSTANCE.getMessage_Transmission();

		/**
		 * The meta object literal for the '<em><b>Overridden Fixed Priority</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_TRANSMISSION__OVERRIDDEN_FIXED_PRIORITY = eINSTANCE.getMessage_Transmission_Overridden_Fixed_Priority();

		/**
		 * The meta object literal for the '<em><b>Overridden Permanent FP</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MESSAGE_TRANSMISSION__OVERRIDDEN_PERMANENT_FP = eINSTANCE.getMessage_Transmission_Overridden_Permanent_FP();

		/**
		 * The meta object literal for the '<em><b>Avg Message Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_TRANSMISSION__AVG_MESSAGE_SIZE = eINSTANCE.getMessage_Transmission_Avg_Message_Size();

		/**
		 * The meta object literal for the '<em><b>Max Message Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_TRANSMISSION__MAX_MESSAGE_SIZE = eINSTANCE.getMessage_Transmission_Max_Message_Size();

		/**
		 * The meta object literal for the '<em><b>Min Message Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_TRANSMISSION__MIN_MESSAGE_SIZE = eINSTANCE.getMessage_Transmission_Min_Message_Size();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MESSAGE_TRANSMISSION__NAME = eINSTANCE.getMessage_Transmission_Name();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.MulticastImpl <em>Multicast</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.MulticastImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getMulticast()
		 * @generated
		 */
		EClass MULTICAST = eINSTANCE.getMulticast();

		/**
		 * The meta object literal for the '<em><b>Input Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MULTICAST__INPUT_EVENT = eINSTANCE.getMulticast_Input_Event();

		/**
		 * The meta object literal for the '<em><b>Output Events List</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MULTICAST__OUTPUT_EVENTS_LIST = eINSTANCE.getMulticast_Output_Events_List();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Non_Preemptible_FP_PolicyImpl <em>Non Preemptible FP Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Non_Preemptible_FP_PolicyImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getNon_Preemptible_FP_Policy()
		 * @generated
		 */
		EClass NON_PREEMPTIBLE_FP_POLICY = eINSTANCE.getNon_Preemptible_FP_Policy();

		/**
		 * The meta object literal for the '<em><b>Preassigned</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NON_PREEMPTIBLE_FP_POLICY__PREASSIGNED = eINSTANCE.getNon_Preemptible_FP_Policy_Preassigned();

		/**
		 * The meta object literal for the '<em><b>The Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NON_PREEMPTIBLE_FP_POLICY__THE_PRIORITY = eINSTANCE.getNon_Preemptible_FP_Policy_The_Priority();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.OffsetImpl <em>Offset</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.OffsetImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getOffset()
		 * @generated
		 */
		EClass OFFSET = eINSTANCE.getOffset();

		/**
		 * The meta object literal for the '<em><b>Delay Max Interval</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OFFSET__DELAY_MAX_INTERVAL = eINSTANCE.getOffset_Delay_Max_Interval();

		/**
		 * The meta object literal for the '<em><b>Delay Min Interval</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OFFSET__DELAY_MIN_INTERVAL = eINSTANCE.getOffset_Delay_Min_Interval();

		/**
		 * The meta object literal for the '<em><b>Input Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OFFSET__INPUT_EVENT = eINSTANCE.getOffset_Input_Event();

		/**
		 * The meta object literal for the '<em><b>Output Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OFFSET__OUTPUT_EVENT = eINSTANCE.getOffset_Output_Event();

		/**
		 * The meta object literal for the '<em><b>Referenced Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OFFSET__REFERENCED_EVENT = eINSTANCE.getOffset_Referenced_Event();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Overridden_Fixed_PriorityImpl <em>Overridden Fixed Priority</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Overridden_Fixed_PriorityImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getOverridden_Fixed_Priority()
		 * @generated
		 */
		EClass OVERRIDDEN_FIXED_PRIORITY = eINSTANCE.getOverridden_Fixed_Priority();

		/**
		 * The meta object literal for the '<em><b>The Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OVERRIDDEN_FIXED_PRIORITY__THE_PRIORITY = eINSTANCE.getOverridden_Fixed_Priority_The_Priority();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Overridden_Permanent_FPImpl <em>Overridden Permanent FP</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Overridden_Permanent_FPImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getOverridden_Permanent_FP()
		 * @generated
		 */
		EClass OVERRIDDEN_PERMANENT_FP = eINSTANCE.getOverridden_Permanent_FP();

		/**
		 * The meta object literal for the '<em><b>The Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OVERRIDDEN_PERMANENT_FP__THE_PRIORITY = eINSTANCE.getOverridden_Permanent_FP_The_Priority();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Packet_Based_NetworkImpl <em>Packet Based Network</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Packet_Based_NetworkImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPacket_Based_Network()
		 * @generated
		 */
		EClass PACKET_BASED_NETWORK = eINSTANCE.getPacket_Based_Network();

		/**
		 * The meta object literal for the '<em><b>List of Drivers</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PACKET_BASED_NETWORK__LIST_OF_DRIVERS = eINSTANCE.getPacket_Based_Network_List_of_Drivers();

		/**
		 * The meta object literal for the '<em><b>Max Blocking</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PACKET_BASED_NETWORK__MAX_BLOCKING = eINSTANCE.getPacket_Based_Network_Max_Blocking();

		/**
		 * The meta object literal for the '<em><b>Max Packet Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PACKET_BASED_NETWORK__MAX_PACKET_SIZE = eINSTANCE.getPacket_Based_Network_Max_Packet_Size();

		/**
		 * The meta object literal for the '<em><b>Min Packet Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PACKET_BASED_NETWORK__MIN_PACKET_SIZE = eINSTANCE.getPacket_Based_Network_Min_Packet_Size();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PACKET_BASED_NETWORK__NAME = eINSTANCE.getPacket_Based_Network_Name();

		/**
		 * The meta object literal for the '<em><b>Speed Factor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PACKET_BASED_NETWORK__SPEED_FACTOR = eINSTANCE.getPacket_Based_Network_Speed_Factor();

		/**
		 * The meta object literal for the '<em><b>Throughput</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PACKET_BASED_NETWORK__THROUGHPUT = eINSTANCE.getPacket_Based_Network_Throughput();

		/**
		 * The meta object literal for the '<em><b>Transmission</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PACKET_BASED_NETWORK__TRANSMISSION = eINSTANCE.getPacket_Based_Network_Transmission();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Packet_DriverImpl <em>Packet Driver</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Packet_DriverImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPacket_Driver()
		 * @generated
		 */
		EClass PACKET_DRIVER = eINSTANCE.getPacket_Driver();

		/**
		 * The meta object literal for the '<em><b>Message Partitioning</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PACKET_DRIVER__MESSAGE_PARTITIONING = eINSTANCE.getPacket_Driver_Message_Partitioning();

		/**
		 * The meta object literal for the '<em><b>Packet Receive Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PACKET_DRIVER__PACKET_RECEIVE_OPERATION = eINSTANCE.getPacket_Driver_Packet_Receive_Operation();

		/**
		 * The meta object literal for the '<em><b>Packet Send Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PACKET_DRIVER__PACKET_SEND_OPERATION = eINSTANCE.getPacket_Driver_Packet_Send_Operation();

		/**
		 * The meta object literal for the '<em><b>Packet Server</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PACKET_DRIVER__PACKET_SERVER = eINSTANCE.getPacket_Driver_Packet_Server();

		/**
		 * The meta object literal for the '<em><b>RTA Overhead Model</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PACKET_DRIVER__RTA_OVERHEAD_MODEL = eINSTANCE.getPacket_Driver_RTA_Overhead_Model();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Periodic_External_EventImpl <em>Periodic External Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Periodic_External_EventImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPeriodic_External_Event()
		 * @generated
		 */
		EClass PERIODIC_EXTERNAL_EVENT = eINSTANCE.getPeriodic_External_Event();

		/**
		 * The meta object literal for the '<em><b>Max Jitter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERIODIC_EXTERNAL_EVENT__MAX_JITTER = eINSTANCE.getPeriodic_External_Event_Max_Jitter();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERIODIC_EXTERNAL_EVENT__NAME = eINSTANCE.getPeriodic_External_Event_Name();

		/**
		 * The meta object literal for the '<em><b>Period</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERIODIC_EXTERNAL_EVENT__PERIOD = eINSTANCE.getPeriodic_External_Event_Period();

		/**
		 * The meta object literal for the '<em><b>Phase</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERIODIC_EXTERNAL_EVENT__PHASE = eINSTANCE.getPeriodic_External_Event_Phase();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Polling_PolicyImpl <em>Polling Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Polling_PolicyImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPolling_Policy()
		 * @generated
		 */
		EClass POLLING_POLICY = eINSTANCE.getPolling_Policy();

		/**
		 * The meta object literal for the '<em><b>Polling Avg Overhead</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POLLING_POLICY__POLLING_AVG_OVERHEAD = eINSTANCE.getPolling_Policy_Polling_Avg_Overhead();

		/**
		 * The meta object literal for the '<em><b>Polling Best Overhead</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POLLING_POLICY__POLLING_BEST_OVERHEAD = eINSTANCE.getPolling_Policy_Polling_Best_Overhead();

		/**
		 * The meta object literal for the '<em><b>Polling Period</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POLLING_POLICY__POLLING_PERIOD = eINSTANCE.getPolling_Policy_Polling_Period();

		/**
		 * The meta object literal for the '<em><b>Polling Worst Overhead</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POLLING_POLICY__POLLING_WORST_OVERHEAD = eINSTANCE.getPolling_Policy_Polling_Worst_Overhead();

		/**
		 * The meta object literal for the '<em><b>Preassigned</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POLLING_POLICY__PREASSIGNED = eINSTANCE.getPolling_Policy_Preassigned();

		/**
		 * The meta object literal for the '<em><b>The Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POLLING_POLICY__THE_PRIORITY = eINSTANCE.getPolling_Policy_The_Priority();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Primary_SchedulerImpl <em>Primary Scheduler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Primary_SchedulerImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPrimary_Scheduler()
		 * @generated
		 */
		EClass PRIMARY_SCHEDULER = eINSTANCE.getPrimary_Scheduler();

		/**
		 * The meta object literal for the '<em><b>Fixed Priority Scheduler</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIMARY_SCHEDULER__FIXED_PRIORITY_SCHEDULER = eINSTANCE.getPrimary_Scheduler_Fixed_Priority_Scheduler();

		/**
		 * The meta object literal for the '<em><b>EDF Scheduler</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIMARY_SCHEDULER__EDF_SCHEDULER = eINSTANCE.getPrimary_Scheduler_EDF_Scheduler();

		/**
		 * The meta object literal for the '<em><b>FP Packet Based Scheduler</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PRIMARY_SCHEDULER__FP_PACKET_BASED_SCHEDULER = eINSTANCE.getPrimary_Scheduler_FP_Packet_Based_Scheduler();

		/**
		 * The meta object literal for the '<em><b>Host</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRIMARY_SCHEDULER__HOST = eINSTANCE.getPrimary_Scheduler_Host();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRIMARY_SCHEDULER__NAME = eINSTANCE.getPrimary_Scheduler_Name();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Priority_Inheritance_ResourceImpl <em>Priority Inheritance Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Priority_Inheritance_ResourceImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPriority_Inheritance_Resource()
		 * @generated
		 */
		EClass PRIORITY_INHERITANCE_RESOURCE = eINSTANCE.getPriority_Inheritance_Resource();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRIORITY_INHERITANCE_RESOURCE__NAME = eINSTANCE.getPriority_Inheritance_Resource_Name();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Query_ServerImpl <em>Query Server</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Query_ServerImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getQuery_Server()
		 * @generated
		 */
		EClass QUERY_SERVER = eINSTANCE.getQuery_Server();

		/**
		 * The meta object literal for the '<em><b>Input Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUERY_SERVER__INPUT_EVENT = eINSTANCE.getQuery_Server_Input_Event();

		/**
		 * The meta object literal for the '<em><b>Output Events List</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUERY_SERVER__OUTPUT_EVENTS_LIST = eINSTANCE.getQuery_Server_Output_Events_List();

		/**
		 * The meta object literal for the '<em><b>Request Policy</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUERY_SERVER__REQUEST_POLICY = eINSTANCE.getQuery_Server_Request_Policy();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Rate_DivisorImpl <em>Rate Divisor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Rate_DivisorImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getRate_Divisor()
		 * @generated
		 */
		EClass RATE_DIVISOR = eINSTANCE.getRate_Divisor();

		/**
		 * The meta object literal for the '<em><b>Input Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RATE_DIVISOR__INPUT_EVENT = eINSTANCE.getRate_Divisor_Input_Event();

		/**
		 * The meta object literal for the '<em><b>Output Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RATE_DIVISOR__OUTPUT_EVENT = eINSTANCE.getRate_Divisor_Output_Event();

		/**
		 * The meta object literal for the '<em><b>Rate Factor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RATE_DIVISOR__RATE_FACTOR = eINSTANCE.getRate_Divisor_Rate_Factor();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_EventImpl <em>Regular Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Regular_EventImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getRegular_Event()
		 * @generated
		 */
		EClass REGULAR_EVENT = eINSTANCE.getRegular_Event();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGULAR_EVENT__GROUP = eINSTANCE.getRegular_Event_Group();

		/**
		 * The meta object literal for the '<em><b>Max Output Jitter Req</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_EVENT__MAX_OUTPUT_JITTER_REQ = eINSTANCE.getRegular_Event_Max_Output_Jitter_Req();

		/**
		 * The meta object literal for the '<em><b>Hard Global Deadline</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_EVENT__HARD_GLOBAL_DEADLINE = eINSTANCE.getRegular_Event_Hard_Global_Deadline();

		/**
		 * The meta object literal for the '<em><b>Soft Global Deadline</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_EVENT__SOFT_GLOBAL_DEADLINE = eINSTANCE.getRegular_Event_Soft_Global_Deadline();

		/**
		 * The meta object literal for the '<em><b>Global Max Miss Ratio</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_EVENT__GLOBAL_MAX_MISS_RATIO = eINSTANCE.getRegular_Event_Global_Max_Miss_Ratio();

		/**
		 * The meta object literal for the '<em><b>Hard Local Deadline</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_EVENT__HARD_LOCAL_DEADLINE = eINSTANCE.getRegular_Event_Hard_Local_Deadline();

		/**
		 * The meta object literal for the '<em><b>Soft Local Deadline</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_EVENT__SOFT_LOCAL_DEADLINE = eINSTANCE.getRegular_Event_Soft_Local_Deadline();

		/**
		 * The meta object literal for the '<em><b>Local Max Miss Ratio</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_EVENT__LOCAL_MAX_MISS_RATIO = eINSTANCE.getRegular_Event_Local_Max_Miss_Ratio();

		/**
		 * The meta object literal for the '<em><b>Composite Timing Requirement</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_EVENT__COMPOSITE_TIMING_REQUIREMENT = eINSTANCE.getRegular_Event_Composite_Timing_Requirement();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGULAR_EVENT__EVENT = eINSTANCE.getRegular_Event_Event();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_ProcessorImpl <em>Regular Processor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Regular_ProcessorImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getRegular_Processor()
		 * @generated
		 */
		EClass REGULAR_PROCESSOR = eINSTANCE.getRegular_Processor();

		/**
		 * The meta object literal for the '<em><b>Ticker System Timer</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_PROCESSOR__TICKER_SYSTEM_TIMER = eINSTANCE.getRegular_Processor_Ticker_System_Timer();

		/**
		 * The meta object literal for the '<em><b>Alarm Clock System Timer</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_PROCESSOR__ALARM_CLOCK_SYSTEM_TIMER = eINSTANCE.getRegular_Processor_Alarm_Clock_System_Timer();

		/**
		 * The meta object literal for the '<em><b>Avg ISR Switch</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGULAR_PROCESSOR__AVG_ISR_SWITCH = eINSTANCE.getRegular_Processor_Avg_ISR_Switch();

		/**
		 * The meta object literal for the '<em><b>Best ISR Switch</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGULAR_PROCESSOR__BEST_ISR_SWITCH = eINSTANCE.getRegular_Processor_Best_ISR_Switch();

		/**
		 * The meta object literal for the '<em><b>Max Interrupt Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGULAR_PROCESSOR__MAX_INTERRUPT_PRIORITY = eINSTANCE.getRegular_Processor_Max_Interrupt_Priority();

		/**
		 * The meta object literal for the '<em><b>Min Interrupt Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGULAR_PROCESSOR__MIN_INTERRUPT_PRIORITY = eINSTANCE.getRegular_Processor_Min_Interrupt_Priority();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGULAR_PROCESSOR__NAME = eINSTANCE.getRegular_Processor_Name();

		/**
		 * The meta object literal for the '<em><b>Speed Factor</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGULAR_PROCESSOR__SPEED_FACTOR = eINSTANCE.getRegular_Processor_Speed_Factor();

		/**
		 * The meta object literal for the '<em><b>Worst ISR Switch</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGULAR_PROCESSOR__WORST_ISR_SWITCH = eINSTANCE.getRegular_Processor_Worst_ISR_Switch();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_Scheduling_ServerImpl <em>Regular Scheduling Server</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Regular_Scheduling_ServerImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getRegular_Scheduling_Server()
		 * @generated
		 */
		EClass REGULAR_SCHEDULING_SERVER = eINSTANCE.getRegular_Scheduling_Server();

		/**
		 * The meta object literal for the '<em><b>Non Preemptible FP Policy</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_SCHEDULING_SERVER__NON_PREEMPTIBLE_FP_POLICY = eINSTANCE.getRegular_Scheduling_Server_Non_Preemptible_FP_Policy();

		/**
		 * The meta object literal for the '<em><b>Fixed Priority Policy</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_SCHEDULING_SERVER__FIXED_PRIORITY_POLICY = eINSTANCE.getRegular_Scheduling_Server_Fixed_Priority_Policy();

		/**
		 * The meta object literal for the '<em><b>Interrupt FP Policy</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_SCHEDULING_SERVER__INTERRUPT_FP_POLICY = eINSTANCE.getRegular_Scheduling_Server_Interrupt_FP_Policy();

		/**
		 * The meta object literal for the '<em><b>Polling Policy</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_SCHEDULING_SERVER__POLLING_POLICY = eINSTANCE.getRegular_Scheduling_Server_Polling_Policy();

		/**
		 * The meta object literal for the '<em><b>Sporadic Server Policy</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_SCHEDULING_SERVER__SPORADIC_SERVER_POLICY = eINSTANCE.getRegular_Scheduling_Server_Sporadic_Server_Policy();

		/**
		 * The meta object literal for the '<em><b>EDF Policy</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_SCHEDULING_SERVER__EDF_POLICY = eINSTANCE.getRegular_Scheduling_Server_EDF_Policy();

		/**
		 * The meta object literal for the '<em><b>SRP Parameters</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_SCHEDULING_SERVER__SRP_PARAMETERS = eINSTANCE.getRegular_Scheduling_Server_SRP_Parameters();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGULAR_SCHEDULING_SERVER__NAME = eINSTANCE.getRegular_Scheduling_Server_Name();

		/**
		 * The meta object literal for the '<em><b>Scheduler</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGULAR_SCHEDULING_SERVER__SCHEDULER = eINSTANCE.getRegular_Scheduling_Server_Scheduler();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl <em>Regular Transaction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getRegular_Transaction()
		 * @generated
		 */
		EClass REGULAR_TRANSACTION = eINSTANCE.getRegular_Transaction();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGULAR_TRANSACTION__GROUP = eINSTANCE.getRegular_Transaction_Group();

		/**
		 * The meta object literal for the '<em><b>Periodic External Event</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_TRANSACTION__PERIODIC_EXTERNAL_EVENT = eINSTANCE.getRegular_Transaction_Periodic_External_Event();

		/**
		 * The meta object literal for the '<em><b>Sporadic External Event</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_TRANSACTION__SPORADIC_EXTERNAL_EVENT = eINSTANCE.getRegular_Transaction_Sporadic_External_Event();

		/**
		 * The meta object literal for the '<em><b>Unbounded External Event</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_TRANSACTION__UNBOUNDED_EXTERNAL_EVENT = eINSTANCE.getRegular_Transaction_Unbounded_External_Event();

		/**
		 * The meta object literal for the '<em><b>Bursty External Event</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_TRANSACTION__BURSTY_EXTERNAL_EVENT = eINSTANCE.getRegular_Transaction_Bursty_External_Event();

		/**
		 * The meta object literal for the '<em><b>Singular External Event</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_TRANSACTION__SINGULAR_EXTERNAL_EVENT = eINSTANCE.getRegular_Transaction_Singular_External_Event();

		/**
		 * The meta object literal for the '<em><b>Regular Event</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_TRANSACTION__REGULAR_EVENT = eINSTANCE.getRegular_Transaction_Regular_Event();

		/**
		 * The meta object literal for the '<em><b>Activity</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_TRANSACTION__ACTIVITY = eINSTANCE.getRegular_Transaction_Activity();

		/**
		 * The meta object literal for the '<em><b>System Timed Activity</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_TRANSACTION__SYSTEM_TIMED_ACTIVITY = eINSTANCE.getRegular_Transaction_System_Timed_Activity();

		/**
		 * The meta object literal for the '<em><b>Concentrator</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_TRANSACTION__CONCENTRATOR = eINSTANCE.getRegular_Transaction_Concentrator();

		/**
		 * The meta object literal for the '<em><b>Barrier</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_TRANSACTION__BARRIER = eINSTANCE.getRegular_Transaction_Barrier();

		/**
		 * The meta object literal for the '<em><b>Delivery Server</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_TRANSACTION__DELIVERY_SERVER = eINSTANCE.getRegular_Transaction_Delivery_Server();

		/**
		 * The meta object literal for the '<em><b>Query Server</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_TRANSACTION__QUERY_SERVER = eINSTANCE.getRegular_Transaction_Query_Server();

		/**
		 * The meta object literal for the '<em><b>Multicast</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_TRANSACTION__MULTICAST = eINSTANCE.getRegular_Transaction_Multicast();

		/**
		 * The meta object literal for the '<em><b>Rate Divisor</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_TRANSACTION__RATE_DIVISOR = eINSTANCE.getRegular_Transaction_Rate_Divisor();

		/**
		 * The meta object literal for the '<em><b>Delay</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_TRANSACTION__DELAY = eINSTANCE.getRegular_Transaction_Delay();

		/**
		 * The meta object literal for the '<em><b>Offset</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REGULAR_TRANSACTION__OFFSET = eINSTANCE.getRegular_Transaction_Offset();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REGULAR_TRANSACTION__NAME = eINSTANCE.getRegular_Transaction_Name();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl <em>RTEP Packet Driver</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getRTEP_PacketDriver()
		 * @generated
		 */
		EClass RTEP_PACKET_DRIVER = eINSTANCE.getRTEP_PacketDriver();

		/**
		 * The meta object literal for the '<em><b>Failure Timeout</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RTEP_PACKET_DRIVER__FAILURE_TIMEOUT = eINSTANCE.getRTEP_PacketDriver_Failure_Timeout();

		/**
		 * The meta object literal for the '<em><b>Message Partitioning</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RTEP_PACKET_DRIVER__MESSAGE_PARTITIONING = eINSTANCE.getRTEP_PacketDriver_Message_Partitioning();

		/**
		 * The meta object literal for the '<em><b>Number Of Stations</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RTEP_PACKET_DRIVER__NUMBER_OF_STATIONS = eINSTANCE.getRTEP_PacketDriver_Number_Of_Stations();

		/**
		 * The meta object literal for the '<em><b>Packet Discard Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RTEP_PACKET_DRIVER__PACKET_DISCARD_OPERATION = eINSTANCE.getRTEP_PacketDriver_Packet_Discard_Operation();

		/**
		 * The meta object literal for the '<em><b>Packet Interrupt Server</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RTEP_PACKET_DRIVER__PACKET_INTERRUPT_SERVER = eINSTANCE.getRTEP_PacketDriver_Packet_Interrupt_Server();

		/**
		 * The meta object literal for the '<em><b>Packet ISR Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RTEP_PACKET_DRIVER__PACKET_ISR_OPERATION = eINSTANCE.getRTEP_PacketDriver_Packet_ISR_Operation();

		/**
		 * The meta object literal for the '<em><b>Packet Receive Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RTEP_PACKET_DRIVER__PACKET_RECEIVE_OPERATION = eINSTANCE.getRTEP_PacketDriver_Packet_Receive_Operation();

		/**
		 * The meta object literal for the '<em><b>Packet Retransmission Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RTEP_PACKET_DRIVER__PACKET_RETRANSMISSION_OPERATION = eINSTANCE.getRTEP_PacketDriver_Packet_Retransmission_Operation();

		/**
		 * The meta object literal for the '<em><b>Packet Send Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RTEP_PACKET_DRIVER__PACKET_SEND_OPERATION = eINSTANCE.getRTEP_PacketDriver_Packet_Send_Operation();

		/**
		 * The meta object literal for the '<em><b>Packet Server</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RTEP_PACKET_DRIVER__PACKET_SERVER = eINSTANCE.getRTEP_PacketDriver_Packet_Server();

		/**
		 * The meta object literal for the '<em><b>Packet Transmission Retries</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RTEP_PACKET_DRIVER__PACKET_TRANSMISSION_RETRIES = eINSTANCE.getRTEP_PacketDriver_Packet_Transmission_Retries();

		/**
		 * The meta object literal for the '<em><b>RTA Overhead Model</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RTEP_PACKET_DRIVER__RTA_OVERHEAD_MODEL = eINSTANCE.getRTEP_PacketDriver_RTA_Overhead_Model();

		/**
		 * The meta object literal for the '<em><b>Token Check Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RTEP_PACKET_DRIVER__TOKEN_CHECK_OPERATION = eINSTANCE.getRTEP_PacketDriver_Token_Check_Operation();

		/**
		 * The meta object literal for the '<em><b>Token Delay</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RTEP_PACKET_DRIVER__TOKEN_DELAY = eINSTANCE.getRTEP_PacketDriver_Token_Delay();

		/**
		 * The meta object literal for the '<em><b>Token Manage Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RTEP_PACKET_DRIVER__TOKEN_MANAGE_OPERATION = eINSTANCE.getRTEP_PacketDriver_Token_Manage_Operation();

		/**
		 * The meta object literal for the '<em><b>Token Retransmission Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RTEP_PACKET_DRIVER__TOKEN_RETRANSMISSION_OPERATION = eINSTANCE.getRTEP_PacketDriver_Token_Retransmission_Operation();

		/**
		 * The meta object literal for the '<em><b>Token Transmission Retries</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RTEP_PACKET_DRIVER__TOKEN_TRANSMISSION_RETRIES = eINSTANCE.getRTEP_PacketDriver_Token_Transmission_Retries();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Secondary_SchedulerImpl <em>Secondary Scheduler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Secondary_SchedulerImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getSecondary_Scheduler()
		 * @generated
		 */
		EClass SECONDARY_SCHEDULER = eINSTANCE.getSecondary_Scheduler();

		/**
		 * The meta object literal for the '<em><b>Fixed Priority Scheduler</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECONDARY_SCHEDULER__FIXED_PRIORITY_SCHEDULER = eINSTANCE.getSecondary_Scheduler_Fixed_Priority_Scheduler();

		/**
		 * The meta object literal for the '<em><b>EDF Scheduler</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECONDARY_SCHEDULER__EDF_SCHEDULER = eINSTANCE.getSecondary_Scheduler_EDF_Scheduler();

		/**
		 * The meta object literal for the '<em><b>FP Packet Based Scheduler</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECONDARY_SCHEDULER__FP_PACKET_BASED_SCHEDULER = eINSTANCE.getSecondary_Scheduler_FP_Packet_Based_Scheduler();

		/**
		 * The meta object literal for the '<em><b>Host</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SECONDARY_SCHEDULER__HOST = eINSTANCE.getSecondary_Scheduler_Host();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SECONDARY_SCHEDULER__NAME = eINSTANCE.getSecondary_Scheduler_Name();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Simple_OperationImpl <em>Simple Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Simple_OperationImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getSimple_Operation()
		 * @generated
		 */
		EClass SIMPLE_OPERATION = eINSTANCE.getSimple_Operation();

		/**
		 * The meta object literal for the '<em><b>Overridden Fixed Priority</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMPLE_OPERATION__OVERRIDDEN_FIXED_PRIORITY = eINSTANCE.getSimple_Operation_Overridden_Fixed_Priority();

		/**
		 * The meta object literal for the '<em><b>Overridden Permanent FP</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMPLE_OPERATION__OVERRIDDEN_PERMANENT_FP = eINSTANCE.getSimple_Operation_Overridden_Permanent_FP();

		/**
		 * The meta object literal for the '<em><b>Shared Resources List</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_OPERATION__SHARED_RESOURCES_LIST = eINSTANCE.getSimple_Operation_Shared_Resources_List();

		/**
		 * The meta object literal for the '<em><b>Shared Resources To Lock</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_OPERATION__SHARED_RESOURCES_TO_LOCK = eINSTANCE.getSimple_Operation_Shared_Resources_To_Lock();

		/**
		 * The meta object literal for the '<em><b>Shared Resources To Unlock</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_OPERATION__SHARED_RESOURCES_TO_UNLOCK = eINSTANCE.getSimple_Operation_Shared_Resources_To_Unlock();

		/**
		 * The meta object literal for the '<em><b>Average Case Execution Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_OPERATION__AVERAGE_CASE_EXECUTION_TIME = eINSTANCE.getSimple_Operation_Average_Case_Execution_Time();

		/**
		 * The meta object literal for the '<em><b>Best Case Execution Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_OPERATION__BEST_CASE_EXECUTION_TIME = eINSTANCE.getSimple_Operation_Best_Case_Execution_Time();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_OPERATION__NAME = eINSTANCE.getSimple_Operation_Name();

		/**
		 * The meta object literal for the '<em><b>Worst Case Execution Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMPLE_OPERATION__WORST_CASE_EXECUTION_TIME = eINSTANCE.getSimple_Operation_Worst_Case_Execution_Time();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Singular_External_EventImpl <em>Singular External Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Singular_External_EventImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getSingular_External_Event()
		 * @generated
		 */
		EClass SINGULAR_EXTERNAL_EVENT = eINSTANCE.getSingular_External_Event();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SINGULAR_EXTERNAL_EVENT__NAME = eINSTANCE.getSingular_External_Event_Name();

		/**
		 * The meta object literal for the '<em><b>Phase</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SINGULAR_EXTERNAL_EVENT__PHASE = eINSTANCE.getSingular_External_Event_Phase();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Soft_Global_DeadlineImpl <em>Soft Global Deadline</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Soft_Global_DeadlineImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getSoft_Global_Deadline()
		 * @generated
		 */
		EClass SOFT_GLOBAL_DEADLINE = eINSTANCE.getSoft_Global_Deadline();

		/**
		 * The meta object literal for the '<em><b>Deadline</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOFT_GLOBAL_DEADLINE__DEADLINE = eINSTANCE.getSoft_Global_Deadline_Deadline();

		/**
		 * The meta object literal for the '<em><b>Referenced Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOFT_GLOBAL_DEADLINE__REFERENCED_EVENT = eINSTANCE.getSoft_Global_Deadline_Referenced_Event();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Soft_Local_DeadlineImpl <em>Soft Local Deadline</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Soft_Local_DeadlineImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getSoft_Local_Deadline()
		 * @generated
		 */
		EClass SOFT_LOCAL_DEADLINE = eINSTANCE.getSoft_Local_Deadline();

		/**
		 * The meta object literal for the '<em><b>Deadline</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOFT_LOCAL_DEADLINE__DEADLINE = eINSTANCE.getSoft_Local_Deadline_Deadline();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Sporadic_External_EventImpl <em>Sporadic External Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Sporadic_External_EventImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getSporadic_External_Event()
		 * @generated
		 */
		EClass SPORADIC_EXTERNAL_EVENT = eINSTANCE.getSporadic_External_Event();

		/**
		 * The meta object literal for the '<em><b>Avg Interarrival</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPORADIC_EXTERNAL_EVENT__AVG_INTERARRIVAL = eINSTANCE.getSporadic_External_Event_Avg_Interarrival();

		/**
		 * The meta object literal for the '<em><b>Distribution</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPORADIC_EXTERNAL_EVENT__DISTRIBUTION = eINSTANCE.getSporadic_External_Event_Distribution();

		/**
		 * The meta object literal for the '<em><b>Min Interarrival</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPORADIC_EXTERNAL_EVENT__MIN_INTERARRIVAL = eINSTANCE.getSporadic_External_Event_Min_Interarrival();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPORADIC_EXTERNAL_EVENT__NAME = eINSTANCE.getSporadic_External_Event_Name();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Sporadic_Server_PolicyImpl <em>Sporadic Server Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Sporadic_Server_PolicyImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getSporadic_Server_Policy()
		 * @generated
		 */
		EClass SPORADIC_SERVER_POLICY = eINSTANCE.getSporadic_Server_Policy();

		/**
		 * The meta object literal for the '<em><b>Background Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPORADIC_SERVER_POLICY__BACKGROUND_PRIORITY = eINSTANCE.getSporadic_Server_Policy_Background_Priority();

		/**
		 * The meta object literal for the '<em><b>Initial Capacity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPORADIC_SERVER_POLICY__INITIAL_CAPACITY = eINSTANCE.getSporadic_Server_Policy_Initial_Capacity();

		/**
		 * The meta object literal for the '<em><b>Max Pending Replenishments</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPORADIC_SERVER_POLICY__MAX_PENDING_REPLENISHMENTS = eINSTANCE.getSporadic_Server_Policy_Max_Pending_Replenishments();

		/**
		 * The meta object literal for the '<em><b>Normal Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPORADIC_SERVER_POLICY__NORMAL_PRIORITY = eINSTANCE.getSporadic_Server_Policy_Normal_Priority();

		/**
		 * The meta object literal for the '<em><b>Preassigned</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPORADIC_SERVER_POLICY__PREASSIGNED = eINSTANCE.getSporadic_Server_Policy_Preassigned();

		/**
		 * The meta object literal for the '<em><b>Replenishment Period</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPORADIC_SERVER_POLICY__REPLENISHMENT_PERIOD = eINSTANCE.getSporadic_Server_Policy_Replenishment_Period();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.SRP_ParametersImpl <em>SRP Parameters</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.SRP_ParametersImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getSRP_Parameters()
		 * @generated
		 */
		EClass SRP_PARAMETERS = eINSTANCE.getSRP_Parameters();

		/**
		 * The meta object literal for the '<em><b>Preassigned</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SRP_PARAMETERS__PREASSIGNED = eINSTANCE.getSRP_Parameters_Preassigned();

		/**
		 * The meta object literal for the '<em><b>Preemption Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SRP_PARAMETERS__PREEMPTION_LEVEL = eINSTANCE.getSRP_Parameters_Preemption_Level();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.SRP_ResourceImpl <em>SRP Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.SRP_ResourceImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getSRP_Resource()
		 * @generated
		 */
		EClass SRP_RESOURCE = eINSTANCE.getSRP_Resource();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SRP_RESOURCE__NAME = eINSTANCE.getSRP_Resource_Name();

		/**
		 * The meta object literal for the '<em><b>Preassigned</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SRP_RESOURCE__PREASSIGNED = eINSTANCE.getSRP_Resource_Preassigned();

		/**
		 * The meta object literal for the '<em><b>Preemption Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SRP_RESOURCE__PREEMPTION_LEVEL = eINSTANCE.getSRP_Resource_Preemption_Level();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.System_Timed_ActivityImpl <em>System Timed Activity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.System_Timed_ActivityImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getSystem_Timed_Activity()
		 * @generated
		 */
		EClass SYSTEM_TIMED_ACTIVITY = eINSTANCE.getSystem_Timed_Activity();

		/**
		 * The meta object literal for the '<em><b>Activity Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYSTEM_TIMED_ACTIVITY__ACTIVITY_OPERATION = eINSTANCE.getSystem_Timed_Activity_Activity_Operation();

		/**
		 * The meta object literal for the '<em><b>Activity Server</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYSTEM_TIMED_ACTIVITY__ACTIVITY_SERVER = eINSTANCE.getSystem_Timed_Activity_Activity_Server();

		/**
		 * The meta object literal for the '<em><b>Input Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYSTEM_TIMED_ACTIVITY__INPUT_EVENT = eINSTANCE.getSystem_Timed_Activity_Input_Event();

		/**
		 * The meta object literal for the '<em><b>Output Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SYSTEM_TIMED_ACTIVITY__OUTPUT_EVENT = eINSTANCE.getSystem_Timed_Activity_Output_Event();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Ticker_System_TimerImpl <em>Ticker System Timer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Ticker_System_TimerImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getTicker_System_Timer()
		 * @generated
		 */
		EClass TICKER_SYSTEM_TIMER = eINSTANCE.getTicker_System_Timer();

		/**
		 * The meta object literal for the '<em><b>Avg Overhead</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TICKER_SYSTEM_TIMER__AVG_OVERHEAD = eINSTANCE.getTicker_System_Timer_Avg_Overhead();

		/**
		 * The meta object literal for the '<em><b>Best Overhead</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TICKER_SYSTEM_TIMER__BEST_OVERHEAD = eINSTANCE.getTicker_System_Timer_Best_Overhead();

		/**
		 * The meta object literal for the '<em><b>Period</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TICKER_SYSTEM_TIMER__PERIOD = eINSTANCE.getTicker_System_Timer_Period();

		/**
		 * The meta object literal for the '<em><b>Worst Overhead</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TICKER_SYSTEM_TIMER__WORST_OVERHEAD = eINSTANCE.getTicker_System_Timer_Worst_Overhead();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.impl.Unbounded_External_EventImpl <em>Unbounded External Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.Unbounded_External_EventImpl
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getUnbounded_External_Event()
		 * @generated
		 */
		EClass UNBOUNDED_EXTERNAL_EVENT = eINSTANCE.getUnbounded_External_Event();

		/**
		 * The meta object literal for the '<em><b>Avg Interarrival</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNBOUNDED_EXTERNAL_EVENT__AVG_INTERARRIVAL = eINSTANCE.getUnbounded_External_Event_Avg_Interarrival();

		/**
		 * The meta object literal for the '<em><b>Distribution</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNBOUNDED_EXTERNAL_EVENT__DISTRIBUTION = eINSTANCE.getUnbounded_External_Event_Distribution();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNBOUNDED_EXTERNAL_EVENT__NAME = eINSTANCE.getUnbounded_External_Event_Name();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.Affirmative_Assertion <em>Affirmative Assertion</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.Affirmative_Assertion
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getAffirmative_Assertion()
		 * @generated
		 */
		EEnum AFFIRMATIVE_ASSERTION = eINSTANCE.getAffirmative_Assertion();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.Assertion <em>Assertion</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.Assertion
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getAssertion()
		 * @generated
		 */
		EEnum ASSERTION = eINSTANCE.getAssertion();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.Delivery_Policy <em>Delivery Policy</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.Delivery_Policy
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getDelivery_Policy()
		 * @generated
		 */
		EEnum DELIVERY_POLICY = eINSTANCE.getDelivery_Policy();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.Distribution_Type <em>Distribution Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.Distribution_Type
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getDistribution_Type()
		 * @generated
		 */
		EEnum DISTRIBUTION_TYPE = eINSTANCE.getDistribution_Type();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.Negative_Assertion <em>Negative Assertion</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.Negative_Assertion
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getNegative_Assertion()
		 * @generated
		 */
		EEnum NEGATIVE_ASSERTION = eINSTANCE.getNegative_Assertion();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.Overhead_Type <em>Overhead Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.Overhead_Type
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getOverhead_Type()
		 * @generated
		 */
		EEnum OVERHEAD_TYPE = eINSTANCE.getOverhead_Type();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Protocol <em>Priority Inheritance Protocol</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Protocol
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPriority_Inheritance_Protocol()
		 * @generated
		 */
		EEnum PRIORITY_INHERITANCE_PROTOCOL = eINSTANCE.getPriority_Inheritance_Protocol();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.Request_Policy <em>Request Policy</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.Request_Policy
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getRequest_Policy()
		 * @generated
		 */
		EEnum REQUEST_POLICY = eINSTANCE.getRequest_Policy();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastmodel.Transmission_Type <em>Transmission Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.Transmission_Type
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getTransmission_Type()
		 * @generated
		 */
		EEnum TRANSMISSION_TYPE = eINSTANCE.getTransmission_Type();

		/**
		 * The meta object literal for the '<em>Absolute Time</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getAbsolute_Time()
		 * @generated
		 */
		EDataType ABSOLUTE_TIME = eINSTANCE.getAbsolute_Time();

		/**
		 * The meta object literal for the '<em>Affirmative Assertion Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.Affirmative_Assertion
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getAffirmative_Assertion_Object()
		 * @generated
		 */
		EDataType AFFIRMATIVE_ASSERTION_OBJECT = eINSTANCE.getAffirmative_Assertion_Object();

		/**
		 * The meta object literal for the '<em>Any Priority</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getAny_Priority()
		 * @generated
		 */
		EDataType ANY_PRIORITY = eINSTANCE.getAny_Priority();

		/**
		 * The meta object literal for the '<em>Assertion Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.Assertion
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getAssertion_Object()
		 * @generated
		 */
		EDataType ASSERTION_OBJECT = eINSTANCE.getAssertion_Object();

		/**
		 * The meta object literal for the '<em>Bit Count</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getBit_Count()
		 * @generated
		 */
		EDataType BIT_COUNT = eINSTANCE.getBit_Count();

		/**
		 * The meta object literal for the '<em>Date Time</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getDate_Time()
		 * @generated
		 */
		EDataType DATE_TIME = eINSTANCE.getDate_Time();

		/**
		 * The meta object literal for the '<em>Delivery Policy Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.Delivery_Policy
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getDelivery_Policy_Object()
		 * @generated
		 */
		EDataType DELIVERY_POLICY_OBJECT = eINSTANCE.getDelivery_Policy_Object();

		/**
		 * The meta object literal for the '<em>Distribution Type Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.Distribution_Type
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getDistribution_Type_Object()
		 * @generated
		 */
		EDataType DISTRIBUTION_TYPE_OBJECT = eINSTANCE.getDistribution_Type_Object();

		/**
		 * The meta object literal for the '<em>Float</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getFloat()
		 * @generated
		 */
		EDataType FLOAT = eINSTANCE.getFloat();

		/**
		 * The meta object literal for the '<em>Identifier</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getIdentifier()
		 * @generated
		 */
		EDataType IDENTIFIER = eINSTANCE.getIdentifier();

		/**
		 * The meta object literal for the '<em>Identifier Ref</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getIdentifier_Ref()
		 * @generated
		 */
		EDataType IDENTIFIER_REF = eINSTANCE.getIdentifier_Ref();

		/**
		 * The meta object literal for the '<em>Identifier Ref List</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.List
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getIdentifier_Ref_List()
		 * @generated
		 */
		EDataType IDENTIFIER_REF_LIST = eINSTANCE.getIdentifier_Ref_List();

		/**
		 * The meta object literal for the '<em>Interrupt Priority</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getInterrupt_Priority()
		 * @generated
		 */
		EDataType INTERRUPT_PRIORITY = eINSTANCE.getInterrupt_Priority();

		/**
		 * The meta object literal for the '<em>Natural</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getNatural()
		 * @generated
		 */
		EDataType NATURAL = eINSTANCE.getNatural();

		/**
		 * The meta object literal for the '<em>Negative Assertion Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.Negative_Assertion
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getNegative_Assertion_Object()
		 * @generated
		 */
		EDataType NEGATIVE_ASSERTION_OBJECT = eINSTANCE.getNegative_Assertion_Object();

		/**
		 * The meta object literal for the '<em>Normalized Execution Time</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getNormalized_Execution_Time()
		 * @generated
		 */
		EDataType NORMALIZED_EXECUTION_TIME = eINSTANCE.getNormalized_Execution_Time();

		/**
		 * The meta object literal for the '<em>Overhead Type Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.Overhead_Type
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getOverhead_Type_Object()
		 * @generated
		 */
		EDataType OVERHEAD_TYPE_OBJECT = eINSTANCE.getOverhead_Type_Object();

		/**
		 * The meta object literal for the '<em>Pathname</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPathname()
		 * @generated
		 */
		EDataType PATHNAME = eINSTANCE.getPathname();

		/**
		 * The meta object literal for the '<em>Percentage</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPercentage()
		 * @generated
		 */
		EDataType PERCENTAGE = eINSTANCE.getPercentage();

		/**
		 * The meta object literal for the '<em>Positive</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPositive()
		 * @generated
		 */
		EDataType POSITIVE = eINSTANCE.getPositive();

		/**
		 * The meta object literal for the '<em>Preemption Level</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPreemption_Level()
		 * @generated
		 */
		EDataType PREEMPTION_LEVEL = eINSTANCE.getPreemption_Level();

		/**
		 * The meta object literal for the '<em>Priority</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPriority()
		 * @generated
		 */
		EDataType PRIORITY = eINSTANCE.getPriority();

		/**
		 * The meta object literal for the '<em>Priority Inheritance Protocol Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Protocol
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getPriority_Inheritance_Protocol_Object()
		 * @generated
		 */
		EDataType PRIORITY_INHERITANCE_PROTOCOL_OBJECT = eINSTANCE.getPriority_Inheritance_Protocol_Object();

		/**
		 * The meta object literal for the '<em>Request Policy Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.Request_Policy
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getRequest_Policy_Object()
		 * @generated
		 */
		EDataType REQUEST_POLICY_OBJECT = eINSTANCE.getRequest_Policy_Object();

		/**
		 * The meta object literal for the '<em>Throughput</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getThroughput()
		 * @generated
		 */
		EDataType THROUGHPUT = eINSTANCE.getThroughput();

		/**
		 * The meta object literal for the '<em>Time</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getTime()
		 * @generated
		 */
		EDataType TIME = eINSTANCE.getTime();

		/**
		 * The meta object literal for the '<em>Transmission Type Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastmodel.Transmission_Type
		 * @see es.esi.gemde.vv.mast.mastmodel.impl.ModelPackageImpl#getTransmission_Type_Object()
		 * @generated
		 */
		EDataType TRANSMISSION_TYPE_OBJECT = eINSTANCE.getTransmission_Type_Object();

	}

} //ModelPackage
