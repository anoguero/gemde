/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Assertion;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Overhead_Type;
import es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>RTEP Packet Driver</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl#getFailure_Timeout <em>Failure Timeout</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl#getMessage_Partitioning <em>Message Partitioning</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl#getNumber_Of_Stations <em>Number Of Stations</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl#getPacket_Discard_Operation <em>Packet Discard Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl#getPacket_Interrupt_Server <em>Packet Interrupt Server</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl#getPacket_ISR_Operation <em>Packet ISR Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl#getPacket_Receive_Operation <em>Packet Receive Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl#getPacket_Retransmission_Operation <em>Packet Retransmission Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl#getPacket_Send_Operation <em>Packet Send Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl#getPacket_Server <em>Packet Server</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl#getPacket_Transmission_Retries <em>Packet Transmission Retries</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl#getRTA_Overhead_Model <em>RTA Overhead Model</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl#getToken_Check_Operation <em>Token Check Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl#getToken_Delay <em>Token Delay</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl#getToken_Manage_Operation <em>Token Manage Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl#getToken_Retransmission_Operation <em>Token Retransmission Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.RTEP_PacketDriverImpl#getToken_Transmission_Retries <em>Token Transmission Retries</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RTEP_PacketDriverImpl extends EObjectImpl implements RTEP_PacketDriver {
	/**
	 * The default value of the '{@link #getFailure_Timeout() <em>Failure Timeout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFailure_Timeout()
	 * @generated
	 * @ordered
	 */
	protected static final double FAILURE_TIMEOUT_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getFailure_Timeout() <em>Failure Timeout</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFailure_Timeout()
	 * @generated
	 * @ordered
	 */
	protected double failure_Timeout = FAILURE_TIMEOUT_EDEFAULT;

	/**
	 * The default value of the '{@link #getMessage_Partitioning() <em>Message Partitioning</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage_Partitioning()
	 * @generated
	 * @ordered
	 */
	protected static final Assertion MESSAGE_PARTITIONING_EDEFAULT = Assertion.YES;

	/**
	 * The cached value of the '{@link #getMessage_Partitioning() <em>Message Partitioning</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessage_Partitioning()
	 * @generated
	 * @ordered
	 */
	protected Assertion message_Partitioning = MESSAGE_PARTITIONING_EDEFAULT;

	/**
	 * This is true if the Message Partitioning attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean message_PartitioningESet;

	/**
	 * The default value of the '{@link #getNumber_Of_Stations() <em>Number Of Stations</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber_Of_Stations()
	 * @generated
	 * @ordered
	 */
	protected static final int NUMBER_OF_STATIONS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumber_Of_Stations() <em>Number Of Stations</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumber_Of_Stations()
	 * @generated
	 * @ordered
	 */
	protected int number_Of_Stations = NUMBER_OF_STATIONS_EDEFAULT;

	/**
	 * The default value of the '{@link #getPacket_Discard_Operation() <em>Packet Discard Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Discard_Operation()
	 * @generated
	 * @ordered
	 */
	protected static final String PACKET_DISCARD_OPERATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPacket_Discard_Operation() <em>Packet Discard Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Discard_Operation()
	 * @generated
	 * @ordered
	 */
	protected String packet_Discard_Operation = PACKET_DISCARD_OPERATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getPacket_Interrupt_Server() <em>Packet Interrupt Server</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Interrupt_Server()
	 * @generated
	 * @ordered
	 */
	protected static final String PACKET_INTERRUPT_SERVER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPacket_Interrupt_Server() <em>Packet Interrupt Server</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Interrupt_Server()
	 * @generated
	 * @ordered
	 */
	protected String packet_Interrupt_Server = PACKET_INTERRUPT_SERVER_EDEFAULT;

	/**
	 * The default value of the '{@link #getPacket_ISR_Operation() <em>Packet ISR Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_ISR_Operation()
	 * @generated
	 * @ordered
	 */
	protected static final String PACKET_ISR_OPERATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPacket_ISR_Operation() <em>Packet ISR Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_ISR_Operation()
	 * @generated
	 * @ordered
	 */
	protected String packet_ISR_Operation = PACKET_ISR_OPERATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getPacket_Receive_Operation() <em>Packet Receive Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Receive_Operation()
	 * @generated
	 * @ordered
	 */
	protected static final String PACKET_RECEIVE_OPERATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPacket_Receive_Operation() <em>Packet Receive Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Receive_Operation()
	 * @generated
	 * @ordered
	 */
	protected String packet_Receive_Operation = PACKET_RECEIVE_OPERATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getPacket_Retransmission_Operation() <em>Packet Retransmission Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Retransmission_Operation()
	 * @generated
	 * @ordered
	 */
	protected static final String PACKET_RETRANSMISSION_OPERATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPacket_Retransmission_Operation() <em>Packet Retransmission Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Retransmission_Operation()
	 * @generated
	 * @ordered
	 */
	protected String packet_Retransmission_Operation = PACKET_RETRANSMISSION_OPERATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getPacket_Send_Operation() <em>Packet Send Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Send_Operation()
	 * @generated
	 * @ordered
	 */
	protected static final String PACKET_SEND_OPERATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPacket_Send_Operation() <em>Packet Send Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Send_Operation()
	 * @generated
	 * @ordered
	 */
	protected String packet_Send_Operation = PACKET_SEND_OPERATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getPacket_Server() <em>Packet Server</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Server()
	 * @generated
	 * @ordered
	 */
	protected static final String PACKET_SERVER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPacket_Server() <em>Packet Server</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Server()
	 * @generated
	 * @ordered
	 */
	protected String packet_Server = PACKET_SERVER_EDEFAULT;

	/**
	 * The default value of the '{@link #getPacket_Transmission_Retries() <em>Packet Transmission Retries</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Transmission_Retries()
	 * @generated
	 * @ordered
	 */
	protected static final int PACKET_TRANSMISSION_RETRIES_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getPacket_Transmission_Retries() <em>Packet Transmission Retries</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPacket_Transmission_Retries()
	 * @generated
	 * @ordered
	 */
	protected int packet_Transmission_Retries = PACKET_TRANSMISSION_RETRIES_EDEFAULT;

	/**
	 * The default value of the '{@link #getRTA_Overhead_Model() <em>RTA Overhead Model</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRTA_Overhead_Model()
	 * @generated
	 * @ordered
	 */
	protected static final Overhead_Type RTA_OVERHEAD_MODEL_EDEFAULT = Overhead_Type.COUPLED;

	/**
	 * The cached value of the '{@link #getRTA_Overhead_Model() <em>RTA Overhead Model</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRTA_Overhead_Model()
	 * @generated
	 * @ordered
	 */
	protected Overhead_Type rtA_Overhead_Model = RTA_OVERHEAD_MODEL_EDEFAULT;

	/**
	 * This is true if the RTA Overhead Model attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean rtA_Overhead_ModelESet;

	/**
	 * The default value of the '{@link #getToken_Check_Operation() <em>Token Check Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToken_Check_Operation()
	 * @generated
	 * @ordered
	 */
	protected static final String TOKEN_CHECK_OPERATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getToken_Check_Operation() <em>Token Check Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToken_Check_Operation()
	 * @generated
	 * @ordered
	 */
	protected String token_Check_Operation = TOKEN_CHECK_OPERATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getToken_Delay() <em>Token Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToken_Delay()
	 * @generated
	 * @ordered
	 */
	protected static final double TOKEN_DELAY_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getToken_Delay() <em>Token Delay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToken_Delay()
	 * @generated
	 * @ordered
	 */
	protected double token_Delay = TOKEN_DELAY_EDEFAULT;

	/**
	 * The default value of the '{@link #getToken_Manage_Operation() <em>Token Manage Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToken_Manage_Operation()
	 * @generated
	 * @ordered
	 */
	protected static final String TOKEN_MANAGE_OPERATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getToken_Manage_Operation() <em>Token Manage Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToken_Manage_Operation()
	 * @generated
	 * @ordered
	 */
	protected String token_Manage_Operation = TOKEN_MANAGE_OPERATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getToken_Retransmission_Operation() <em>Token Retransmission Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToken_Retransmission_Operation()
	 * @generated
	 * @ordered
	 */
	protected static final String TOKEN_RETRANSMISSION_OPERATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getToken_Retransmission_Operation() <em>Token Retransmission Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToken_Retransmission_Operation()
	 * @generated
	 * @ordered
	 */
	protected String token_Retransmission_Operation = TOKEN_RETRANSMISSION_OPERATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getToken_Transmission_Retries() <em>Token Transmission Retries</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToken_Transmission_Retries()
	 * @generated
	 * @ordered
	 */
	protected static final int TOKEN_TRANSMISSION_RETRIES_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getToken_Transmission_Retries() <em>Token Transmission Retries</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToken_Transmission_Retries()
	 * @generated
	 * @ordered
	 */
	protected int token_Transmission_Retries = TOKEN_TRANSMISSION_RETRIES_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RTEP_PacketDriverImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.RTEP_PACKET_DRIVER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getFailure_Timeout() {
		return failure_Timeout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFailure_Timeout(double newFailure_Timeout) {
		double oldFailure_Timeout = failure_Timeout;
		failure_Timeout = newFailure_Timeout;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RTEP_PACKET_DRIVER__FAILURE_TIMEOUT, oldFailure_Timeout, failure_Timeout));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Assertion getMessage_Partitioning() {
		return message_Partitioning;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMessage_Partitioning(Assertion newMessage_Partitioning) {
		Assertion oldMessage_Partitioning = message_Partitioning;
		message_Partitioning = newMessage_Partitioning == null ? MESSAGE_PARTITIONING_EDEFAULT : newMessage_Partitioning;
		boolean oldMessage_PartitioningESet = message_PartitioningESet;
		message_PartitioningESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RTEP_PACKET_DRIVER__MESSAGE_PARTITIONING, oldMessage_Partitioning, message_Partitioning, !oldMessage_PartitioningESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMessage_Partitioning() {
		Assertion oldMessage_Partitioning = message_Partitioning;
		boolean oldMessage_PartitioningESet = message_PartitioningESet;
		message_Partitioning = MESSAGE_PARTITIONING_EDEFAULT;
		message_PartitioningESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ModelPackage.RTEP_PACKET_DRIVER__MESSAGE_PARTITIONING, oldMessage_Partitioning, MESSAGE_PARTITIONING_EDEFAULT, oldMessage_PartitioningESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMessage_Partitioning() {
		return message_PartitioningESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumber_Of_Stations() {
		return number_Of_Stations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumber_Of_Stations(int newNumber_Of_Stations) {
		int oldNumber_Of_Stations = number_Of_Stations;
		number_Of_Stations = newNumber_Of_Stations;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RTEP_PACKET_DRIVER__NUMBER_OF_STATIONS, oldNumber_Of_Stations, number_Of_Stations));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPacket_Discard_Operation() {
		return packet_Discard_Operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPacket_Discard_Operation(String newPacket_Discard_Operation) {
		String oldPacket_Discard_Operation = packet_Discard_Operation;
		packet_Discard_Operation = newPacket_Discard_Operation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RTEP_PACKET_DRIVER__PACKET_DISCARD_OPERATION, oldPacket_Discard_Operation, packet_Discard_Operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPacket_Interrupt_Server() {
		return packet_Interrupt_Server;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPacket_Interrupt_Server(String newPacket_Interrupt_Server) {
		String oldPacket_Interrupt_Server = packet_Interrupt_Server;
		packet_Interrupt_Server = newPacket_Interrupt_Server;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RTEP_PACKET_DRIVER__PACKET_INTERRUPT_SERVER, oldPacket_Interrupt_Server, packet_Interrupt_Server));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPacket_ISR_Operation() {
		return packet_ISR_Operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPacket_ISR_Operation(String newPacket_ISR_Operation) {
		String oldPacket_ISR_Operation = packet_ISR_Operation;
		packet_ISR_Operation = newPacket_ISR_Operation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RTEP_PACKET_DRIVER__PACKET_ISR_OPERATION, oldPacket_ISR_Operation, packet_ISR_Operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPacket_Receive_Operation() {
		return packet_Receive_Operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPacket_Receive_Operation(String newPacket_Receive_Operation) {
		String oldPacket_Receive_Operation = packet_Receive_Operation;
		packet_Receive_Operation = newPacket_Receive_Operation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RTEP_PACKET_DRIVER__PACKET_RECEIVE_OPERATION, oldPacket_Receive_Operation, packet_Receive_Operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPacket_Retransmission_Operation() {
		return packet_Retransmission_Operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPacket_Retransmission_Operation(String newPacket_Retransmission_Operation) {
		String oldPacket_Retransmission_Operation = packet_Retransmission_Operation;
		packet_Retransmission_Operation = newPacket_Retransmission_Operation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RTEP_PACKET_DRIVER__PACKET_RETRANSMISSION_OPERATION, oldPacket_Retransmission_Operation, packet_Retransmission_Operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPacket_Send_Operation() {
		return packet_Send_Operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPacket_Send_Operation(String newPacket_Send_Operation) {
		String oldPacket_Send_Operation = packet_Send_Operation;
		packet_Send_Operation = newPacket_Send_Operation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RTEP_PACKET_DRIVER__PACKET_SEND_OPERATION, oldPacket_Send_Operation, packet_Send_Operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPacket_Server() {
		return packet_Server;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPacket_Server(String newPacket_Server) {
		String oldPacket_Server = packet_Server;
		packet_Server = newPacket_Server;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RTEP_PACKET_DRIVER__PACKET_SERVER, oldPacket_Server, packet_Server));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPacket_Transmission_Retries() {
		return packet_Transmission_Retries;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPacket_Transmission_Retries(int newPacket_Transmission_Retries) {
		int oldPacket_Transmission_Retries = packet_Transmission_Retries;
		packet_Transmission_Retries = newPacket_Transmission_Retries;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RTEP_PACKET_DRIVER__PACKET_TRANSMISSION_RETRIES, oldPacket_Transmission_Retries, packet_Transmission_Retries));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Overhead_Type getRTA_Overhead_Model() {
		return rtA_Overhead_Model;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRTA_Overhead_Model(Overhead_Type newRTA_Overhead_Model) {
		Overhead_Type oldRTA_Overhead_Model = rtA_Overhead_Model;
		rtA_Overhead_Model = newRTA_Overhead_Model == null ? RTA_OVERHEAD_MODEL_EDEFAULT : newRTA_Overhead_Model;
		boolean oldRTA_Overhead_ModelESet = rtA_Overhead_ModelESet;
		rtA_Overhead_ModelESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RTEP_PACKET_DRIVER__RTA_OVERHEAD_MODEL, oldRTA_Overhead_Model, rtA_Overhead_Model, !oldRTA_Overhead_ModelESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetRTA_Overhead_Model() {
		Overhead_Type oldRTA_Overhead_Model = rtA_Overhead_Model;
		boolean oldRTA_Overhead_ModelESet = rtA_Overhead_ModelESet;
		rtA_Overhead_Model = RTA_OVERHEAD_MODEL_EDEFAULT;
		rtA_Overhead_ModelESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ModelPackage.RTEP_PACKET_DRIVER__RTA_OVERHEAD_MODEL, oldRTA_Overhead_Model, RTA_OVERHEAD_MODEL_EDEFAULT, oldRTA_Overhead_ModelESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetRTA_Overhead_Model() {
		return rtA_Overhead_ModelESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getToken_Check_Operation() {
		return token_Check_Operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToken_Check_Operation(String newToken_Check_Operation) {
		String oldToken_Check_Operation = token_Check_Operation;
		token_Check_Operation = newToken_Check_Operation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RTEP_PACKET_DRIVER__TOKEN_CHECK_OPERATION, oldToken_Check_Operation, token_Check_Operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getToken_Delay() {
		return token_Delay;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToken_Delay(double newToken_Delay) {
		double oldToken_Delay = token_Delay;
		token_Delay = newToken_Delay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RTEP_PACKET_DRIVER__TOKEN_DELAY, oldToken_Delay, token_Delay));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getToken_Manage_Operation() {
		return token_Manage_Operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToken_Manage_Operation(String newToken_Manage_Operation) {
		String oldToken_Manage_Operation = token_Manage_Operation;
		token_Manage_Operation = newToken_Manage_Operation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RTEP_PACKET_DRIVER__TOKEN_MANAGE_OPERATION, oldToken_Manage_Operation, token_Manage_Operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getToken_Retransmission_Operation() {
		return token_Retransmission_Operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToken_Retransmission_Operation(String newToken_Retransmission_Operation) {
		String oldToken_Retransmission_Operation = token_Retransmission_Operation;
		token_Retransmission_Operation = newToken_Retransmission_Operation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RTEP_PACKET_DRIVER__TOKEN_RETRANSMISSION_OPERATION, oldToken_Retransmission_Operation, token_Retransmission_Operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getToken_Transmission_Retries() {
		return token_Transmission_Retries;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToken_Transmission_Retries(int newToken_Transmission_Retries) {
		int oldToken_Transmission_Retries = token_Transmission_Retries;
		token_Transmission_Retries = newToken_Transmission_Retries;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.RTEP_PACKET_DRIVER__TOKEN_TRANSMISSION_RETRIES, oldToken_Transmission_Retries, token_Transmission_Retries));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.RTEP_PACKET_DRIVER__FAILURE_TIMEOUT:
				return getFailure_Timeout();
			case ModelPackage.RTEP_PACKET_DRIVER__MESSAGE_PARTITIONING:
				return getMessage_Partitioning();
			case ModelPackage.RTEP_PACKET_DRIVER__NUMBER_OF_STATIONS:
				return getNumber_Of_Stations();
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_DISCARD_OPERATION:
				return getPacket_Discard_Operation();
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_INTERRUPT_SERVER:
				return getPacket_Interrupt_Server();
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_ISR_OPERATION:
				return getPacket_ISR_Operation();
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_RECEIVE_OPERATION:
				return getPacket_Receive_Operation();
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_RETRANSMISSION_OPERATION:
				return getPacket_Retransmission_Operation();
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_SEND_OPERATION:
				return getPacket_Send_Operation();
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_SERVER:
				return getPacket_Server();
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_TRANSMISSION_RETRIES:
				return getPacket_Transmission_Retries();
			case ModelPackage.RTEP_PACKET_DRIVER__RTA_OVERHEAD_MODEL:
				return getRTA_Overhead_Model();
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_CHECK_OPERATION:
				return getToken_Check_Operation();
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_DELAY:
				return getToken_Delay();
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_MANAGE_OPERATION:
				return getToken_Manage_Operation();
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_RETRANSMISSION_OPERATION:
				return getToken_Retransmission_Operation();
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_TRANSMISSION_RETRIES:
				return getToken_Transmission_Retries();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.RTEP_PACKET_DRIVER__FAILURE_TIMEOUT:
				setFailure_Timeout((Double)newValue);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__MESSAGE_PARTITIONING:
				setMessage_Partitioning((Assertion)newValue);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__NUMBER_OF_STATIONS:
				setNumber_Of_Stations((Integer)newValue);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_DISCARD_OPERATION:
				setPacket_Discard_Operation((String)newValue);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_INTERRUPT_SERVER:
				setPacket_Interrupt_Server((String)newValue);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_ISR_OPERATION:
				setPacket_ISR_Operation((String)newValue);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_RECEIVE_OPERATION:
				setPacket_Receive_Operation((String)newValue);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_RETRANSMISSION_OPERATION:
				setPacket_Retransmission_Operation((String)newValue);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_SEND_OPERATION:
				setPacket_Send_Operation((String)newValue);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_SERVER:
				setPacket_Server((String)newValue);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_TRANSMISSION_RETRIES:
				setPacket_Transmission_Retries((Integer)newValue);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__RTA_OVERHEAD_MODEL:
				setRTA_Overhead_Model((Overhead_Type)newValue);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_CHECK_OPERATION:
				setToken_Check_Operation((String)newValue);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_DELAY:
				setToken_Delay((Double)newValue);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_MANAGE_OPERATION:
				setToken_Manage_Operation((String)newValue);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_RETRANSMISSION_OPERATION:
				setToken_Retransmission_Operation((String)newValue);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_TRANSMISSION_RETRIES:
				setToken_Transmission_Retries((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.RTEP_PACKET_DRIVER__FAILURE_TIMEOUT:
				setFailure_Timeout(FAILURE_TIMEOUT_EDEFAULT);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__MESSAGE_PARTITIONING:
				unsetMessage_Partitioning();
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__NUMBER_OF_STATIONS:
				setNumber_Of_Stations(NUMBER_OF_STATIONS_EDEFAULT);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_DISCARD_OPERATION:
				setPacket_Discard_Operation(PACKET_DISCARD_OPERATION_EDEFAULT);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_INTERRUPT_SERVER:
				setPacket_Interrupt_Server(PACKET_INTERRUPT_SERVER_EDEFAULT);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_ISR_OPERATION:
				setPacket_ISR_Operation(PACKET_ISR_OPERATION_EDEFAULT);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_RECEIVE_OPERATION:
				setPacket_Receive_Operation(PACKET_RECEIVE_OPERATION_EDEFAULT);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_RETRANSMISSION_OPERATION:
				setPacket_Retransmission_Operation(PACKET_RETRANSMISSION_OPERATION_EDEFAULT);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_SEND_OPERATION:
				setPacket_Send_Operation(PACKET_SEND_OPERATION_EDEFAULT);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_SERVER:
				setPacket_Server(PACKET_SERVER_EDEFAULT);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_TRANSMISSION_RETRIES:
				setPacket_Transmission_Retries(PACKET_TRANSMISSION_RETRIES_EDEFAULT);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__RTA_OVERHEAD_MODEL:
				unsetRTA_Overhead_Model();
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_CHECK_OPERATION:
				setToken_Check_Operation(TOKEN_CHECK_OPERATION_EDEFAULT);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_DELAY:
				setToken_Delay(TOKEN_DELAY_EDEFAULT);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_MANAGE_OPERATION:
				setToken_Manage_Operation(TOKEN_MANAGE_OPERATION_EDEFAULT);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_RETRANSMISSION_OPERATION:
				setToken_Retransmission_Operation(TOKEN_RETRANSMISSION_OPERATION_EDEFAULT);
				return;
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_TRANSMISSION_RETRIES:
				setToken_Transmission_Retries(TOKEN_TRANSMISSION_RETRIES_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.RTEP_PACKET_DRIVER__FAILURE_TIMEOUT:
				return failure_Timeout != FAILURE_TIMEOUT_EDEFAULT;
			case ModelPackage.RTEP_PACKET_DRIVER__MESSAGE_PARTITIONING:
				return isSetMessage_Partitioning();
			case ModelPackage.RTEP_PACKET_DRIVER__NUMBER_OF_STATIONS:
				return number_Of_Stations != NUMBER_OF_STATIONS_EDEFAULT;
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_DISCARD_OPERATION:
				return PACKET_DISCARD_OPERATION_EDEFAULT == null ? packet_Discard_Operation != null : !PACKET_DISCARD_OPERATION_EDEFAULT.equals(packet_Discard_Operation);
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_INTERRUPT_SERVER:
				return PACKET_INTERRUPT_SERVER_EDEFAULT == null ? packet_Interrupt_Server != null : !PACKET_INTERRUPT_SERVER_EDEFAULT.equals(packet_Interrupt_Server);
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_ISR_OPERATION:
				return PACKET_ISR_OPERATION_EDEFAULT == null ? packet_ISR_Operation != null : !PACKET_ISR_OPERATION_EDEFAULT.equals(packet_ISR_Operation);
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_RECEIVE_OPERATION:
				return PACKET_RECEIVE_OPERATION_EDEFAULT == null ? packet_Receive_Operation != null : !PACKET_RECEIVE_OPERATION_EDEFAULT.equals(packet_Receive_Operation);
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_RETRANSMISSION_OPERATION:
				return PACKET_RETRANSMISSION_OPERATION_EDEFAULT == null ? packet_Retransmission_Operation != null : !PACKET_RETRANSMISSION_OPERATION_EDEFAULT.equals(packet_Retransmission_Operation);
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_SEND_OPERATION:
				return PACKET_SEND_OPERATION_EDEFAULT == null ? packet_Send_Operation != null : !PACKET_SEND_OPERATION_EDEFAULT.equals(packet_Send_Operation);
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_SERVER:
				return PACKET_SERVER_EDEFAULT == null ? packet_Server != null : !PACKET_SERVER_EDEFAULT.equals(packet_Server);
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_TRANSMISSION_RETRIES:
				return packet_Transmission_Retries != PACKET_TRANSMISSION_RETRIES_EDEFAULT;
			case ModelPackage.RTEP_PACKET_DRIVER__RTA_OVERHEAD_MODEL:
				return isSetRTA_Overhead_Model();
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_CHECK_OPERATION:
				return TOKEN_CHECK_OPERATION_EDEFAULT == null ? token_Check_Operation != null : !TOKEN_CHECK_OPERATION_EDEFAULT.equals(token_Check_Operation);
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_DELAY:
				return token_Delay != TOKEN_DELAY_EDEFAULT;
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_MANAGE_OPERATION:
				return TOKEN_MANAGE_OPERATION_EDEFAULT == null ? token_Manage_Operation != null : !TOKEN_MANAGE_OPERATION_EDEFAULT.equals(token_Manage_Operation);
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_RETRANSMISSION_OPERATION:
				return TOKEN_RETRANSMISSION_OPERATION_EDEFAULT == null ? token_Retransmission_Operation != null : !TOKEN_RETRANSMISSION_OPERATION_EDEFAULT.equals(token_Retransmission_Operation);
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_TRANSMISSION_RETRIES:
				return token_Transmission_Retries != TOKEN_TRANSMISSION_RETRIES_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Failure_Timeout: ");
		result.append(failure_Timeout);
		result.append(", Message_Partitioning: ");
		if (message_PartitioningESet) result.append(message_Partitioning); else result.append("<unset>");
		result.append(", Number_Of_Stations: ");
		result.append(number_Of_Stations);
		result.append(", Packet_Discard_Operation: ");
		result.append(packet_Discard_Operation);
		result.append(", Packet_Interrupt_Server: ");
		result.append(packet_Interrupt_Server);
		result.append(", Packet_ISR_Operation: ");
		result.append(packet_ISR_Operation);
		result.append(", Packet_Receive_Operation: ");
		result.append(packet_Receive_Operation);
		result.append(", Packet_Retransmission_Operation: ");
		result.append(packet_Retransmission_Operation);
		result.append(", Packet_Send_Operation: ");
		result.append(packet_Send_Operation);
		result.append(", Packet_Server: ");
		result.append(packet_Server);
		result.append(", Packet_Transmission_Retries: ");
		result.append(packet_Transmission_Retries);
		result.append(", RTA_Overhead_Model: ");
		if (rtA_Overhead_ModelESet) result.append(rtA_Overhead_Model); else result.append("<unset>");
		result.append(", Token_Check_Operation: ");
		result.append(token_Check_Operation);
		result.append(", Token_Delay: ");
		result.append(token_Delay);
		result.append(", Token_Manage_Operation: ");
		result.append(token_Manage_Operation);
		result.append(", Token_Retransmission_Operation: ");
		result.append(token_Retransmission_Operation);
		result.append(", Token_Transmission_Retries: ");
		result.append(token_Transmission_Retries);
		result.append(')');
		return result.toString();
	}

} //RTEP_PacketDriverImpl
