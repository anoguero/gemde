/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.EDF_Policy;
import es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Policy;
import es.esi.gemde.vv.mast.mastmodel.Interrupt_FP_Policy;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Non_Preemptible_FP_Policy;
import es.esi.gemde.vv.mast.mastmodel.Polling_Policy;
import es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server;
import es.esi.gemde.vv.mast.mastmodel.SRP_Parameters;
import es.esi.gemde.vv.mast.mastmodel.Sporadic_Server_Policy;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Regular Scheduling Server</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_Scheduling_ServerImpl#getNon_Preemptible_FP_Policy <em>Non Preemptible FP Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_Scheduling_ServerImpl#getFixed_Priority_Policy <em>Fixed Priority Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_Scheduling_ServerImpl#getInterrupt_FP_Policy <em>Interrupt FP Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_Scheduling_ServerImpl#getPolling_Policy <em>Polling Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_Scheduling_ServerImpl#getSporadic_Server_Policy <em>Sporadic Server Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_Scheduling_ServerImpl#getEDF_Policy <em>EDF Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_Scheduling_ServerImpl#getSRP_Parameters <em>SRP Parameters</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_Scheduling_ServerImpl#getName <em>Name</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_Scheduling_ServerImpl#getScheduler <em>Scheduler</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Regular_Scheduling_ServerImpl extends EObjectImpl implements Regular_Scheduling_Server {
	/**
	 * The cached value of the '{@link #getNon_Preemptible_FP_Policy() <em>Non Preemptible FP Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNon_Preemptible_FP_Policy()
	 * @generated
	 * @ordered
	 */
	protected Non_Preemptible_FP_Policy non_Preemptible_FP_Policy;

	/**
	 * The cached value of the '{@link #getFixed_Priority_Policy() <em>Fixed Priority Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFixed_Priority_Policy()
	 * @generated
	 * @ordered
	 */
	protected Fixed_Priority_Policy fixed_Priority_Policy;

	/**
	 * The cached value of the '{@link #getInterrupt_FP_Policy() <em>Interrupt FP Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterrupt_FP_Policy()
	 * @generated
	 * @ordered
	 */
	protected Interrupt_FP_Policy interrupt_FP_Policy;

	/**
	 * The cached value of the '{@link #getPolling_Policy() <em>Polling Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPolling_Policy()
	 * @generated
	 * @ordered
	 */
	protected Polling_Policy polling_Policy;

	/**
	 * The cached value of the '{@link #getSporadic_Server_Policy() <em>Sporadic Server Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSporadic_Server_Policy()
	 * @generated
	 * @ordered
	 */
	protected Sporadic_Server_Policy sporadic_Server_Policy;

	/**
	 * The cached value of the '{@link #getEDF_Policy() <em>EDF Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEDF_Policy()
	 * @generated
	 * @ordered
	 */
	protected EDF_Policy edF_Policy;

	/**
	 * The cached value of the '{@link #getSRP_Parameters() <em>SRP Parameters</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSRP_Parameters()
	 * @generated
	 * @ordered
	 */
	protected SRP_Parameters srP_Parameters;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getScheduler() <em>Scheduler</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScheduler()
	 * @generated
	 * @ordered
	 */
	protected static final String SCHEDULER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getScheduler() <em>Scheduler</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScheduler()
	 * @generated
	 * @ordered
	 */
	protected String scheduler = SCHEDULER_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Regular_Scheduling_ServerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.REGULAR_SCHEDULING_SERVER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Non_Preemptible_FP_Policy getNon_Preemptible_FP_Policy() {
		return non_Preemptible_FP_Policy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNon_Preemptible_FP_Policy(Non_Preemptible_FP_Policy newNon_Preemptible_FP_Policy, NotificationChain msgs) {
		Non_Preemptible_FP_Policy oldNon_Preemptible_FP_Policy = non_Preemptible_FP_Policy;
		non_Preemptible_FP_Policy = newNon_Preemptible_FP_Policy;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_SCHEDULING_SERVER__NON_PREEMPTIBLE_FP_POLICY, oldNon_Preemptible_FP_Policy, newNon_Preemptible_FP_Policy);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNon_Preemptible_FP_Policy(Non_Preemptible_FP_Policy newNon_Preemptible_FP_Policy) {
		if (newNon_Preemptible_FP_Policy != non_Preemptible_FP_Policy) {
			NotificationChain msgs = null;
			if (non_Preemptible_FP_Policy != null)
				msgs = ((InternalEObject)non_Preemptible_FP_Policy).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.REGULAR_SCHEDULING_SERVER__NON_PREEMPTIBLE_FP_POLICY, null, msgs);
			if (newNon_Preemptible_FP_Policy != null)
				msgs = ((InternalEObject)newNon_Preemptible_FP_Policy).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.REGULAR_SCHEDULING_SERVER__NON_PREEMPTIBLE_FP_POLICY, null, msgs);
			msgs = basicSetNon_Preemptible_FP_Policy(newNon_Preemptible_FP_Policy, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_SCHEDULING_SERVER__NON_PREEMPTIBLE_FP_POLICY, newNon_Preemptible_FP_Policy, newNon_Preemptible_FP_Policy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Fixed_Priority_Policy getFixed_Priority_Policy() {
		return fixed_Priority_Policy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFixed_Priority_Policy(Fixed_Priority_Policy newFixed_Priority_Policy, NotificationChain msgs) {
		Fixed_Priority_Policy oldFixed_Priority_Policy = fixed_Priority_Policy;
		fixed_Priority_Policy = newFixed_Priority_Policy;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_SCHEDULING_SERVER__FIXED_PRIORITY_POLICY, oldFixed_Priority_Policy, newFixed_Priority_Policy);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFixed_Priority_Policy(Fixed_Priority_Policy newFixed_Priority_Policy) {
		if (newFixed_Priority_Policy != fixed_Priority_Policy) {
			NotificationChain msgs = null;
			if (fixed_Priority_Policy != null)
				msgs = ((InternalEObject)fixed_Priority_Policy).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.REGULAR_SCHEDULING_SERVER__FIXED_PRIORITY_POLICY, null, msgs);
			if (newFixed_Priority_Policy != null)
				msgs = ((InternalEObject)newFixed_Priority_Policy).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.REGULAR_SCHEDULING_SERVER__FIXED_PRIORITY_POLICY, null, msgs);
			msgs = basicSetFixed_Priority_Policy(newFixed_Priority_Policy, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_SCHEDULING_SERVER__FIXED_PRIORITY_POLICY, newFixed_Priority_Policy, newFixed_Priority_Policy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interrupt_FP_Policy getInterrupt_FP_Policy() {
		return interrupt_FP_Policy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInterrupt_FP_Policy(Interrupt_FP_Policy newInterrupt_FP_Policy, NotificationChain msgs) {
		Interrupt_FP_Policy oldInterrupt_FP_Policy = interrupt_FP_Policy;
		interrupt_FP_Policy = newInterrupt_FP_Policy;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_SCHEDULING_SERVER__INTERRUPT_FP_POLICY, oldInterrupt_FP_Policy, newInterrupt_FP_Policy);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterrupt_FP_Policy(Interrupt_FP_Policy newInterrupt_FP_Policy) {
		if (newInterrupt_FP_Policy != interrupt_FP_Policy) {
			NotificationChain msgs = null;
			if (interrupt_FP_Policy != null)
				msgs = ((InternalEObject)interrupt_FP_Policy).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.REGULAR_SCHEDULING_SERVER__INTERRUPT_FP_POLICY, null, msgs);
			if (newInterrupt_FP_Policy != null)
				msgs = ((InternalEObject)newInterrupt_FP_Policy).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.REGULAR_SCHEDULING_SERVER__INTERRUPT_FP_POLICY, null, msgs);
			msgs = basicSetInterrupt_FP_Policy(newInterrupt_FP_Policy, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_SCHEDULING_SERVER__INTERRUPT_FP_POLICY, newInterrupt_FP_Policy, newInterrupt_FP_Policy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Polling_Policy getPolling_Policy() {
		return polling_Policy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPolling_Policy(Polling_Policy newPolling_Policy, NotificationChain msgs) {
		Polling_Policy oldPolling_Policy = polling_Policy;
		polling_Policy = newPolling_Policy;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_SCHEDULING_SERVER__POLLING_POLICY, oldPolling_Policy, newPolling_Policy);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPolling_Policy(Polling_Policy newPolling_Policy) {
		if (newPolling_Policy != polling_Policy) {
			NotificationChain msgs = null;
			if (polling_Policy != null)
				msgs = ((InternalEObject)polling_Policy).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.REGULAR_SCHEDULING_SERVER__POLLING_POLICY, null, msgs);
			if (newPolling_Policy != null)
				msgs = ((InternalEObject)newPolling_Policy).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.REGULAR_SCHEDULING_SERVER__POLLING_POLICY, null, msgs);
			msgs = basicSetPolling_Policy(newPolling_Policy, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_SCHEDULING_SERVER__POLLING_POLICY, newPolling_Policy, newPolling_Policy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Sporadic_Server_Policy getSporadic_Server_Policy() {
		return sporadic_Server_Policy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSporadic_Server_Policy(Sporadic_Server_Policy newSporadic_Server_Policy, NotificationChain msgs) {
		Sporadic_Server_Policy oldSporadic_Server_Policy = sporadic_Server_Policy;
		sporadic_Server_Policy = newSporadic_Server_Policy;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_SCHEDULING_SERVER__SPORADIC_SERVER_POLICY, oldSporadic_Server_Policy, newSporadic_Server_Policy);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSporadic_Server_Policy(Sporadic_Server_Policy newSporadic_Server_Policy) {
		if (newSporadic_Server_Policy != sporadic_Server_Policy) {
			NotificationChain msgs = null;
			if (sporadic_Server_Policy != null)
				msgs = ((InternalEObject)sporadic_Server_Policy).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.REGULAR_SCHEDULING_SERVER__SPORADIC_SERVER_POLICY, null, msgs);
			if (newSporadic_Server_Policy != null)
				msgs = ((InternalEObject)newSporadic_Server_Policy).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.REGULAR_SCHEDULING_SERVER__SPORADIC_SERVER_POLICY, null, msgs);
			msgs = basicSetSporadic_Server_Policy(newSporadic_Server_Policy, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_SCHEDULING_SERVER__SPORADIC_SERVER_POLICY, newSporadic_Server_Policy, newSporadic_Server_Policy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDF_Policy getEDF_Policy() {
		return edF_Policy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEDF_Policy(EDF_Policy newEDF_Policy, NotificationChain msgs) {
		EDF_Policy oldEDF_Policy = edF_Policy;
		edF_Policy = newEDF_Policy;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_SCHEDULING_SERVER__EDF_POLICY, oldEDF_Policy, newEDF_Policy);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEDF_Policy(EDF_Policy newEDF_Policy) {
		if (newEDF_Policy != edF_Policy) {
			NotificationChain msgs = null;
			if (edF_Policy != null)
				msgs = ((InternalEObject)edF_Policy).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.REGULAR_SCHEDULING_SERVER__EDF_POLICY, null, msgs);
			if (newEDF_Policy != null)
				msgs = ((InternalEObject)newEDF_Policy).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.REGULAR_SCHEDULING_SERVER__EDF_POLICY, null, msgs);
			msgs = basicSetEDF_Policy(newEDF_Policy, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_SCHEDULING_SERVER__EDF_POLICY, newEDF_Policy, newEDF_Policy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SRP_Parameters getSRP_Parameters() {
		return srP_Parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSRP_Parameters(SRP_Parameters newSRP_Parameters, NotificationChain msgs) {
		SRP_Parameters oldSRP_Parameters = srP_Parameters;
		srP_Parameters = newSRP_Parameters;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_SCHEDULING_SERVER__SRP_PARAMETERS, oldSRP_Parameters, newSRP_Parameters);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSRP_Parameters(SRP_Parameters newSRP_Parameters) {
		if (newSRP_Parameters != srP_Parameters) {
			NotificationChain msgs = null;
			if (srP_Parameters != null)
				msgs = ((InternalEObject)srP_Parameters).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.REGULAR_SCHEDULING_SERVER__SRP_PARAMETERS, null, msgs);
			if (newSRP_Parameters != null)
				msgs = ((InternalEObject)newSRP_Parameters).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.REGULAR_SCHEDULING_SERVER__SRP_PARAMETERS, null, msgs);
			msgs = basicSetSRP_Parameters(newSRP_Parameters, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_SCHEDULING_SERVER__SRP_PARAMETERS, newSRP_Parameters, newSRP_Parameters));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_SCHEDULING_SERVER__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getScheduler() {
		return scheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScheduler(String newScheduler) {
		String oldScheduler = scheduler;
		scheduler = newScheduler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_SCHEDULING_SERVER__SCHEDULER, oldScheduler, scheduler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelPackage.REGULAR_SCHEDULING_SERVER__NON_PREEMPTIBLE_FP_POLICY:
				return basicSetNon_Preemptible_FP_Policy(null, msgs);
			case ModelPackage.REGULAR_SCHEDULING_SERVER__FIXED_PRIORITY_POLICY:
				return basicSetFixed_Priority_Policy(null, msgs);
			case ModelPackage.REGULAR_SCHEDULING_SERVER__INTERRUPT_FP_POLICY:
				return basicSetInterrupt_FP_Policy(null, msgs);
			case ModelPackage.REGULAR_SCHEDULING_SERVER__POLLING_POLICY:
				return basicSetPolling_Policy(null, msgs);
			case ModelPackage.REGULAR_SCHEDULING_SERVER__SPORADIC_SERVER_POLICY:
				return basicSetSporadic_Server_Policy(null, msgs);
			case ModelPackage.REGULAR_SCHEDULING_SERVER__EDF_POLICY:
				return basicSetEDF_Policy(null, msgs);
			case ModelPackage.REGULAR_SCHEDULING_SERVER__SRP_PARAMETERS:
				return basicSetSRP_Parameters(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.REGULAR_SCHEDULING_SERVER__NON_PREEMPTIBLE_FP_POLICY:
				return getNon_Preemptible_FP_Policy();
			case ModelPackage.REGULAR_SCHEDULING_SERVER__FIXED_PRIORITY_POLICY:
				return getFixed_Priority_Policy();
			case ModelPackage.REGULAR_SCHEDULING_SERVER__INTERRUPT_FP_POLICY:
				return getInterrupt_FP_Policy();
			case ModelPackage.REGULAR_SCHEDULING_SERVER__POLLING_POLICY:
				return getPolling_Policy();
			case ModelPackage.REGULAR_SCHEDULING_SERVER__SPORADIC_SERVER_POLICY:
				return getSporadic_Server_Policy();
			case ModelPackage.REGULAR_SCHEDULING_SERVER__EDF_POLICY:
				return getEDF_Policy();
			case ModelPackage.REGULAR_SCHEDULING_SERVER__SRP_PARAMETERS:
				return getSRP_Parameters();
			case ModelPackage.REGULAR_SCHEDULING_SERVER__NAME:
				return getName();
			case ModelPackage.REGULAR_SCHEDULING_SERVER__SCHEDULER:
				return getScheduler();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.REGULAR_SCHEDULING_SERVER__NON_PREEMPTIBLE_FP_POLICY:
				setNon_Preemptible_FP_Policy((Non_Preemptible_FP_Policy)newValue);
				return;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__FIXED_PRIORITY_POLICY:
				setFixed_Priority_Policy((Fixed_Priority_Policy)newValue);
				return;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__INTERRUPT_FP_POLICY:
				setInterrupt_FP_Policy((Interrupt_FP_Policy)newValue);
				return;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__POLLING_POLICY:
				setPolling_Policy((Polling_Policy)newValue);
				return;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__SPORADIC_SERVER_POLICY:
				setSporadic_Server_Policy((Sporadic_Server_Policy)newValue);
				return;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__EDF_POLICY:
				setEDF_Policy((EDF_Policy)newValue);
				return;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__SRP_PARAMETERS:
				setSRP_Parameters((SRP_Parameters)newValue);
				return;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__NAME:
				setName((String)newValue);
				return;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__SCHEDULER:
				setScheduler((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.REGULAR_SCHEDULING_SERVER__NON_PREEMPTIBLE_FP_POLICY:
				setNon_Preemptible_FP_Policy((Non_Preemptible_FP_Policy)null);
				return;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__FIXED_PRIORITY_POLICY:
				setFixed_Priority_Policy((Fixed_Priority_Policy)null);
				return;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__INTERRUPT_FP_POLICY:
				setInterrupt_FP_Policy((Interrupt_FP_Policy)null);
				return;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__POLLING_POLICY:
				setPolling_Policy((Polling_Policy)null);
				return;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__SPORADIC_SERVER_POLICY:
				setSporadic_Server_Policy((Sporadic_Server_Policy)null);
				return;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__EDF_POLICY:
				setEDF_Policy((EDF_Policy)null);
				return;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__SRP_PARAMETERS:
				setSRP_Parameters((SRP_Parameters)null);
				return;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__SCHEDULER:
				setScheduler(SCHEDULER_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.REGULAR_SCHEDULING_SERVER__NON_PREEMPTIBLE_FP_POLICY:
				return non_Preemptible_FP_Policy != null;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__FIXED_PRIORITY_POLICY:
				return fixed_Priority_Policy != null;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__INTERRUPT_FP_POLICY:
				return interrupt_FP_Policy != null;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__POLLING_POLICY:
				return polling_Policy != null;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__SPORADIC_SERVER_POLICY:
				return sporadic_Server_Policy != null;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__EDF_POLICY:
				return edF_Policy != null;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__SRP_PARAMETERS:
				return srP_Parameters != null;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ModelPackage.REGULAR_SCHEDULING_SERVER__SCHEDULER:
				return SCHEDULER_EDEFAULT == null ? scheduler != null : !SCHEDULER_EDEFAULT.equals(scheduler);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Name: ");
		result.append(name);
		result.append(", Scheduler: ");
		result.append(scheduler);
		result.append(')');
		return result.toString();
	}

} //Regular_Scheduling_ServerImpl
