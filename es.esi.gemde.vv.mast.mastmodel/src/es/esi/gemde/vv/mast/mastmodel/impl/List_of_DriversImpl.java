/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver;
import es.esi.gemde.vv.mast.mastmodel.List_of_Drivers;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Packet_Driver;
import es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>List of Drivers</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.List_of_DriversImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.List_of_DriversImpl#getPacket_Driver <em>Packet Driver</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.List_of_DriversImpl#getCharacter_Packet_Driver <em>Character Packet Driver</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.List_of_DriversImpl#getRTEP_Packet_Driver <em>RTEP Packet Driver</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class List_of_DriversImpl extends EObjectImpl implements List_of_Drivers {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected List_of_DriversImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.LIST_OF_DRIVERS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, ModelPackage.LIST_OF_DRIVERS__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Packet_Driver> getPacket_Driver() {
		return getGroup().list(ModelPackage.Literals.LIST_OF_DRIVERS__PACKET_DRIVER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Character_Packet_Driver> getCharacter_Packet_Driver() {
		return getGroup().list(ModelPackage.Literals.LIST_OF_DRIVERS__CHARACTER_PACKET_DRIVER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RTEP_PacketDriver> getRTEP_Packet_Driver() {
		return getGroup().list(ModelPackage.Literals.LIST_OF_DRIVERS__RTEP_PACKET_DRIVER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelPackage.LIST_OF_DRIVERS__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case ModelPackage.LIST_OF_DRIVERS__PACKET_DRIVER:
				return ((InternalEList<?>)getPacket_Driver()).basicRemove(otherEnd, msgs);
			case ModelPackage.LIST_OF_DRIVERS__CHARACTER_PACKET_DRIVER:
				return ((InternalEList<?>)getCharacter_Packet_Driver()).basicRemove(otherEnd, msgs);
			case ModelPackage.LIST_OF_DRIVERS__RTEP_PACKET_DRIVER:
				return ((InternalEList<?>)getRTEP_Packet_Driver()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.LIST_OF_DRIVERS__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case ModelPackage.LIST_OF_DRIVERS__PACKET_DRIVER:
				return getPacket_Driver();
			case ModelPackage.LIST_OF_DRIVERS__CHARACTER_PACKET_DRIVER:
				return getCharacter_Packet_Driver();
			case ModelPackage.LIST_OF_DRIVERS__RTEP_PACKET_DRIVER:
				return getRTEP_Packet_Driver();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.LIST_OF_DRIVERS__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case ModelPackage.LIST_OF_DRIVERS__PACKET_DRIVER:
				getPacket_Driver().clear();
				getPacket_Driver().addAll((Collection<? extends Packet_Driver>)newValue);
				return;
			case ModelPackage.LIST_OF_DRIVERS__CHARACTER_PACKET_DRIVER:
				getCharacter_Packet_Driver().clear();
				getCharacter_Packet_Driver().addAll((Collection<? extends Character_Packet_Driver>)newValue);
				return;
			case ModelPackage.LIST_OF_DRIVERS__RTEP_PACKET_DRIVER:
				getRTEP_Packet_Driver().clear();
				getRTEP_Packet_Driver().addAll((Collection<? extends RTEP_PacketDriver>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.LIST_OF_DRIVERS__GROUP:
				getGroup().clear();
				return;
			case ModelPackage.LIST_OF_DRIVERS__PACKET_DRIVER:
				getPacket_Driver().clear();
				return;
			case ModelPackage.LIST_OF_DRIVERS__CHARACTER_PACKET_DRIVER:
				getCharacter_Packet_Driver().clear();
				return;
			case ModelPackage.LIST_OF_DRIVERS__RTEP_PACKET_DRIVER:
				getRTEP_Packet_Driver().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.LIST_OF_DRIVERS__GROUP:
				return group != null && !group.isEmpty();
			case ModelPackage.LIST_OF_DRIVERS__PACKET_DRIVER:
				return !getPacket_Driver().isEmpty();
			case ModelPackage.LIST_OF_DRIVERS__CHARACTER_PACKET_DRIVER:
				return !getCharacter_Packet_Driver().isEmpty();
			case ModelPackage.LIST_OF_DRIVERS__RTEP_PACKET_DRIVER:
				return !getRTEP_Packet_Driver().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(')');
		return result.toString();
	}

} //List_of_DriversImpl
