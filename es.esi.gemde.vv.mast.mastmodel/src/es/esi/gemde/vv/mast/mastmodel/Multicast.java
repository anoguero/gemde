/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multicast</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Multicast#getInput_Event <em>Input Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Multicast#getOutput_Events_List <em>Output Events List</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMulticast()
 * @model extendedMetaData="name='Multicast' kind='empty'"
 * @generated
 */
public interface Multicast extends EObject {
	/**
	 * Returns the value of the '<em><b>Input Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Event</em>' attribute.
	 * @see #setInput_Event(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMulticast_Input_Event()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref" required="true"
	 *        extendedMetaData="kind='attribute' name='Input_Event'"
	 * @generated
	 */
	String getInput_Event();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Multicast#getInput_Event <em>Input Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Event</em>' attribute.
	 * @see #getInput_Event()
	 * @generated
	 */
	void setInput_Event(String value);

	/**
	 * Returns the value of the '<em><b>Output Events List</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Events List</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Events List</em>' attribute.
	 * @see #setOutput_Events_List(List)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMulticast_Output_Events_List()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref_List" required="true" many="false"
	 *        extendedMetaData="kind='attribute' name='Output_Events_List'"
	 * @generated
	 */
	List<String> getOutput_Events_List();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Multicast#getOutput_Events_List <em>Output Events List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Events List</em>' attribute.
	 * @see #getOutput_Events_List()
	 * @generated
	 */
	void setOutput_Events_List(List<String> value);

} // Multicast
