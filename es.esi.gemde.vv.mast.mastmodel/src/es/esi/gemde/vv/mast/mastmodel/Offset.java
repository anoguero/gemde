/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Offset</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Offset#getDelay_Max_Interval <em>Delay Max Interval</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Offset#getDelay_Min_Interval <em>Delay Min Interval</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Offset#getInput_Event <em>Input Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Offset#getOutput_Event <em>Output Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Offset#getReferenced_Event <em>Referenced Event</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getOffset()
 * @model extendedMetaData="name='Offset' kind='empty'"
 * @generated
 */
public interface Offset extends EObject {
	/**
	 * Returns the value of the '<em><b>Delay Max Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delay Max Interval</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delay Max Interval</em>' attribute.
	 * @see #setDelay_Max_Interval(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getOffset_Delay_Max_Interval()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Time"
	 *        extendedMetaData="kind='attribute' name='Delay_Max_Interval'"
	 * @generated
	 */
	double getDelay_Max_Interval();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Offset#getDelay_Max_Interval <em>Delay Max Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delay Max Interval</em>' attribute.
	 * @see #getDelay_Max_Interval()
	 * @generated
	 */
	void setDelay_Max_Interval(double value);

	/**
	 * Returns the value of the '<em><b>Delay Min Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delay Min Interval</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delay Min Interval</em>' attribute.
	 * @see #setDelay_Min_Interval(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getOffset_Delay_Min_Interval()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Time"
	 *        extendedMetaData="kind='attribute' name='Delay_Min_Interval'"
	 * @generated
	 */
	double getDelay_Min_Interval();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Offset#getDelay_Min_Interval <em>Delay Min Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delay Min Interval</em>' attribute.
	 * @see #getDelay_Min_Interval()
	 * @generated
	 */
	void setDelay_Min_Interval(double value);

	/**
	 * Returns the value of the '<em><b>Input Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Event</em>' attribute.
	 * @see #setInput_Event(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getOffset_Input_Event()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref" required="true"
	 *        extendedMetaData="kind='attribute' name='Input_Event'"
	 * @generated
	 */
	String getInput_Event();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Offset#getInput_Event <em>Input Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Event</em>' attribute.
	 * @see #getInput_Event()
	 * @generated
	 */
	void setInput_Event(String value);

	/**
	 * Returns the value of the '<em><b>Output Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Event</em>' attribute.
	 * @see #setOutput_Event(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getOffset_Output_Event()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref" required="true"
	 *        extendedMetaData="kind='attribute' name='Output_Event'"
	 * @generated
	 */
	String getOutput_Event();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Offset#getOutput_Event <em>Output Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Event</em>' attribute.
	 * @see #getOutput_Event()
	 * @generated
	 */
	void setOutput_Event(String value);

	/**
	 * Returns the value of the '<em><b>Referenced Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Event</em>' attribute.
	 * @see #setReferenced_Event(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getOffset_Referenced_Event()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref" required="true"
	 *        extendedMetaData="kind='attribute' name='Referenced_Event'"
	 * @generated
	 */
	String getReferenced_Event();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Offset#getReferenced_Event <em>Referenced Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referenced Event</em>' attribute.
	 * @see #getReferenced_Event()
	 * @generated
	 */
	void setReferenced_Event(String value);

} // Offset
