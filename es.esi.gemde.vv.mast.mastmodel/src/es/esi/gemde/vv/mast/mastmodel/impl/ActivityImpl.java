/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Activity;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Activity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.ActivityImpl#getActivity_Operation <em>Activity Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.ActivityImpl#getActivity_Server <em>Activity Server</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.ActivityImpl#getInput_Event <em>Input Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.ActivityImpl#getOutput_Event <em>Output Event</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ActivityImpl extends EObjectImpl implements Activity {
	/**
	 * The default value of the '{@link #getActivity_Operation() <em>Activity Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivity_Operation()
	 * @generated
	 * @ordered
	 */
	protected static final String ACTIVITY_OPERATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getActivity_Operation() <em>Activity Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivity_Operation()
	 * @generated
	 * @ordered
	 */
	protected String activity_Operation = ACTIVITY_OPERATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getActivity_Server() <em>Activity Server</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivity_Server()
	 * @generated
	 * @ordered
	 */
	protected static final String ACTIVITY_SERVER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getActivity_Server() <em>Activity Server</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivity_Server()
	 * @generated
	 * @ordered
	 */
	protected String activity_Server = ACTIVITY_SERVER_EDEFAULT;

	/**
	 * The default value of the '{@link #getInput_Event() <em>Input Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInput_Event()
	 * @generated
	 * @ordered
	 */
	protected static final String INPUT_EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInput_Event() <em>Input Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInput_Event()
	 * @generated
	 * @ordered
	 */
	protected String input_Event = INPUT_EVENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getOutput_Event() <em>Output Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutput_Event()
	 * @generated
	 * @ordered
	 */
	protected static final String OUTPUT_EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOutput_Event() <em>Output Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutput_Event()
	 * @generated
	 * @ordered
	 */
	protected String output_Event = OUTPUT_EVENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActivityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.ACTIVITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getActivity_Operation() {
		return activity_Operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActivity_Operation(String newActivity_Operation) {
		String oldActivity_Operation = activity_Operation;
		activity_Operation = newActivity_Operation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.ACTIVITY__ACTIVITY_OPERATION, oldActivity_Operation, activity_Operation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getActivity_Server() {
		return activity_Server;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActivity_Server(String newActivity_Server) {
		String oldActivity_Server = activity_Server;
		activity_Server = newActivity_Server;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.ACTIVITY__ACTIVITY_SERVER, oldActivity_Server, activity_Server));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInput_Event() {
		return input_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInput_Event(String newInput_Event) {
		String oldInput_Event = input_Event;
		input_Event = newInput_Event;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.ACTIVITY__INPUT_EVENT, oldInput_Event, input_Event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOutput_Event() {
		return output_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutput_Event(String newOutput_Event) {
		String oldOutput_Event = output_Event;
		output_Event = newOutput_Event;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.ACTIVITY__OUTPUT_EVENT, oldOutput_Event, output_Event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.ACTIVITY__ACTIVITY_OPERATION:
				return getActivity_Operation();
			case ModelPackage.ACTIVITY__ACTIVITY_SERVER:
				return getActivity_Server();
			case ModelPackage.ACTIVITY__INPUT_EVENT:
				return getInput_Event();
			case ModelPackage.ACTIVITY__OUTPUT_EVENT:
				return getOutput_Event();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.ACTIVITY__ACTIVITY_OPERATION:
				setActivity_Operation((String)newValue);
				return;
			case ModelPackage.ACTIVITY__ACTIVITY_SERVER:
				setActivity_Server((String)newValue);
				return;
			case ModelPackage.ACTIVITY__INPUT_EVENT:
				setInput_Event((String)newValue);
				return;
			case ModelPackage.ACTIVITY__OUTPUT_EVENT:
				setOutput_Event((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.ACTIVITY__ACTIVITY_OPERATION:
				setActivity_Operation(ACTIVITY_OPERATION_EDEFAULT);
				return;
			case ModelPackage.ACTIVITY__ACTIVITY_SERVER:
				setActivity_Server(ACTIVITY_SERVER_EDEFAULT);
				return;
			case ModelPackage.ACTIVITY__INPUT_EVENT:
				setInput_Event(INPUT_EVENT_EDEFAULT);
				return;
			case ModelPackage.ACTIVITY__OUTPUT_EVENT:
				setOutput_Event(OUTPUT_EVENT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.ACTIVITY__ACTIVITY_OPERATION:
				return ACTIVITY_OPERATION_EDEFAULT == null ? activity_Operation != null : !ACTIVITY_OPERATION_EDEFAULT.equals(activity_Operation);
			case ModelPackage.ACTIVITY__ACTIVITY_SERVER:
				return ACTIVITY_SERVER_EDEFAULT == null ? activity_Server != null : !ACTIVITY_SERVER_EDEFAULT.equals(activity_Server);
			case ModelPackage.ACTIVITY__INPUT_EVENT:
				return INPUT_EVENT_EDEFAULT == null ? input_Event != null : !INPUT_EVENT_EDEFAULT.equals(input_Event);
			case ModelPackage.ACTIVITY__OUTPUT_EVENT:
				return OUTPUT_EVENT_EDEFAULT == null ? output_Event != null : !OUTPUT_EVENT_EDEFAULT.equals(output_Event);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Activity_Operation: ");
		result.append(activity_Operation);
		result.append(", Activity_Server: ");
		result.append(activity_Server);
		result.append(", Input_Event: ");
		result.append(input_Event);
		result.append(", Output_Event: ");
		result.append(output_Event);
		result.append(')');
		return result.toString();
	}

} //ActivityImpl
