/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAST MODEL</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getGroup <em>Group</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getRegular_Processor <em>Regular Processor</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getPacket_Based_Network <em>Packet Based Network</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getRegular_Scheduling_Server <em>Regular Scheduling Server</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getImmediate_Ceiling_Resource <em>Immediate Ceiling Resource</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getPriority_Inheritance_Resource <em>Priority Inheritance Resource</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getSRP_Resource <em>SRP Resource</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getSimple_Operation <em>Simple Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getMessage_Transmission <em>Message Transmission</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getComposite_Operation <em>Composite Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getEnclosing_Operation <em>Enclosing Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getRegular_Transaction <em>Regular Transaction</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getPrimary_Scheduler <em>Primary Scheduler</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getSecondary_Scheduler <em>Secondary Scheduler</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getModel_Date <em>Model Date</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getModel_Name <em>Model Name</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getSystem_PiP_Behaviour <em>System Pi PBehaviour</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMAST_MODEL()
 * @model extendedMetaData="name='MAST_MODEL' kind='elementOnly'"
 * @generated
 */
public interface MAST_MODEL extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMAST_MODEL_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Regular Processor</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Regular_Processor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Regular Processor</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Regular Processor</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMAST_MODEL_Regular_Processor()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Regular_Processor' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Regular_Processor> getRegular_Processor();

	/**
	 * Returns the value of the '<em><b>Packet Based Network</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Packet Based Network</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packet Based Network</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMAST_MODEL_Packet_Based_Network()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Packet_Based_Network' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Packet_Based_Network> getPacket_Based_Network();

	/**
	 * Returns the value of the '<em><b>Regular Scheduling Server</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Regular Scheduling Server</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Regular Scheduling Server</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMAST_MODEL_Regular_Scheduling_Server()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Regular_Scheduling_Server' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Regular_Scheduling_Server> getRegular_Scheduling_Server();

	/**
	 * Returns the value of the '<em><b>Immediate Ceiling Resource</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Immediate Ceiling Resource</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Immediate Ceiling Resource</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMAST_MODEL_Immediate_Ceiling_Resource()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Immediate_Ceiling_Resource' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Immediate_Ceiling_Resource> getImmediate_Ceiling_Resource();

	/**
	 * Returns the value of the '<em><b>Priority Inheritance Resource</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Resource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Inheritance Resource</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Inheritance Resource</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMAST_MODEL_Priority_Inheritance_Resource()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Priority_Inheritance_Resource' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Priority_Inheritance_Resource> getPriority_Inheritance_Resource();

	/**
	 * Returns the value of the '<em><b>SRP Resource</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.SRP_Resource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>SRP Resource</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>SRP Resource</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMAST_MODEL_SRP_Resource()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='SRP_Resource' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<SRP_Resource> getSRP_Resource();

	/**
	 * Returns the value of the '<em><b>Simple Operation</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simple Operation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simple Operation</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMAST_MODEL_Simple_Operation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Simple_Operation' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Simple_Operation> getSimple_Operation();

	/**
	 * Returns the value of the '<em><b>Message Transmission</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Transmission</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Transmission</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMAST_MODEL_Message_Transmission()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Message_Transmission' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Message_Transmission> getMessage_Transmission();

	/**
	 * Returns the value of the '<em><b>Composite Operation</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Composite_Operation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Composite Operation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Composite Operation</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMAST_MODEL_Composite_Operation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Composite_Operation' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Composite_Operation> getComposite_Operation();

	/**
	 * Returns the value of the '<em><b>Enclosing Operation</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enclosing Operation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enclosing Operation</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMAST_MODEL_Enclosing_Operation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Enclosing_Operation' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Enclosing_Operation> getEnclosing_Operation();

	/**
	 * Returns the value of the '<em><b>Regular Transaction</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Regular_Transaction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Regular Transaction</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Regular Transaction</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMAST_MODEL_Regular_Transaction()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Regular_Transaction' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Regular_Transaction> getRegular_Transaction();

	/**
	 * Returns the value of the '<em><b>Primary Scheduler</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Primary_Scheduler}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Primary Scheduler</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Primary Scheduler</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMAST_MODEL_Primary_Scheduler()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Primary_Scheduler' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Primary_Scheduler> getPrimary_Scheduler();

	/**
	 * Returns the value of the '<em><b>Secondary Scheduler</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Secondary Scheduler</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Secondary Scheduler</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMAST_MODEL_Secondary_Scheduler()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Secondary_Scheduler' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Secondary_Scheduler> getSecondary_Scheduler();

	/**
	 * Returns the value of the '<em><b>Model Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Date</em>' attribute.
	 * @see #setModel_Date(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMAST_MODEL_Model_Date()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Date_Time" required="true"
	 *        extendedMetaData="kind='attribute' name='Model_Date'"
	 * @generated
	 */
	String getModel_Date();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getModel_Date <em>Model Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Date</em>' attribute.
	 * @see #getModel_Date()
	 * @generated
	 */
	void setModel_Date(String value);

	/**
	 * Returns the value of the '<em><b>Model Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Name</em>' attribute.
	 * @see #setModel_Name(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMAST_MODEL_Model_Name()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier" required="true"
	 *        extendedMetaData="kind='attribute' name='Model_Name'"
	 * @generated
	 */
	String getModel_Name();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getModel_Name <em>Model Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Name</em>' attribute.
	 * @see #getModel_Name()
	 * @generated
	 */
	void setModel_Name(String value);

	/**
	 * Returns the value of the '<em><b>System Pi PBehaviour</b></em>' attribute.
	 * The literals are from the enumeration {@link es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Protocol}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System Pi PBehaviour</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Pi PBehaviour</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Protocol
	 * @see #isSetSystem_PiP_Behaviour()
	 * @see #unsetSystem_PiP_Behaviour()
	 * @see #setSystem_PiP_Behaviour(Priority_Inheritance_Protocol)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMAST_MODEL_System_PiP_Behaviour()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='System_PiP_Behaviour'"
	 * @generated
	 */
	Priority_Inheritance_Protocol getSystem_PiP_Behaviour();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getSystem_PiP_Behaviour <em>System Pi PBehaviour</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>System Pi PBehaviour</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Protocol
	 * @see #isSetSystem_PiP_Behaviour()
	 * @see #unsetSystem_PiP_Behaviour()
	 * @see #getSystem_PiP_Behaviour()
	 * @generated
	 */
	void setSystem_PiP_Behaviour(Priority_Inheritance_Protocol value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getSystem_PiP_Behaviour <em>System Pi PBehaviour</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSystem_PiP_Behaviour()
	 * @see #getSystem_PiP_Behaviour()
	 * @see #setSystem_PiP_Behaviour(Priority_Inheritance_Protocol)
	 * @generated
	 */
	void unsetSystem_PiP_Behaviour();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL#getSystem_PiP_Behaviour <em>System Pi PBehaviour</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>System Pi PBehaviour</em>' attribute is set.
	 * @see #unsetSystem_PiP_Behaviour()
	 * @see #getSystem_PiP_Behaviour()
	 * @see #setSystem_PiP_Behaviour(Priority_Inheritance_Protocol)
	 * @generated
	 */
	boolean isSetSystem_PiP_Behaviour();

} // MAST_MODEL
