/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Composite_Operation;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Overridden_Fixed_Priority;
import es.esi.gemde.vv.mast.mastmodel.Overridden_Permanent_FP;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Composite Operation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Composite_OperationImpl#getOverridden_Fixed_Priority <em>Overridden Fixed Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Composite_OperationImpl#getOverridden_Permanent_FP <em>Overridden Permanent FP</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Composite_OperationImpl#getOperation_List <em>Operation List</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Composite_OperationImpl#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Composite_OperationImpl extends EObjectImpl implements Composite_Operation {
	/**
	 * The cached value of the '{@link #getOverridden_Fixed_Priority() <em>Overridden Fixed Priority</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverridden_Fixed_Priority()
	 * @generated
	 * @ordered
	 */
	protected Overridden_Fixed_Priority overridden_Fixed_Priority;

	/**
	 * The cached value of the '{@link #getOverridden_Permanent_FP() <em>Overridden Permanent FP</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverridden_Permanent_FP()
	 * @generated
	 * @ordered
	 */
	protected Overridden_Permanent_FP overridden_Permanent_FP;

	/**
	 * The default value of the '{@link #getOperation_List() <em>Operation List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation_List()
	 * @generated
	 * @ordered
	 */
	protected static final List<String> OPERATION_LIST_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOperation_List() <em>Operation List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation_List()
	 * @generated
	 * @ordered
	 */
	protected List<String> operation_List = OPERATION_LIST_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Composite_OperationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.COMPOSITE_OPERATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Overridden_Fixed_Priority getOverridden_Fixed_Priority() {
		return overridden_Fixed_Priority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOverridden_Fixed_Priority(Overridden_Fixed_Priority newOverridden_Fixed_Priority, NotificationChain msgs) {
		Overridden_Fixed_Priority oldOverridden_Fixed_Priority = overridden_Fixed_Priority;
		overridden_Fixed_Priority = newOverridden_Fixed_Priority;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.COMPOSITE_OPERATION__OVERRIDDEN_FIXED_PRIORITY, oldOverridden_Fixed_Priority, newOverridden_Fixed_Priority);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOverridden_Fixed_Priority(Overridden_Fixed_Priority newOverridden_Fixed_Priority) {
		if (newOverridden_Fixed_Priority != overridden_Fixed_Priority) {
			NotificationChain msgs = null;
			if (overridden_Fixed_Priority != null)
				msgs = ((InternalEObject)overridden_Fixed_Priority).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.COMPOSITE_OPERATION__OVERRIDDEN_FIXED_PRIORITY, null, msgs);
			if (newOverridden_Fixed_Priority != null)
				msgs = ((InternalEObject)newOverridden_Fixed_Priority).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.COMPOSITE_OPERATION__OVERRIDDEN_FIXED_PRIORITY, null, msgs);
			msgs = basicSetOverridden_Fixed_Priority(newOverridden_Fixed_Priority, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.COMPOSITE_OPERATION__OVERRIDDEN_FIXED_PRIORITY, newOverridden_Fixed_Priority, newOverridden_Fixed_Priority));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Overridden_Permanent_FP getOverridden_Permanent_FP() {
		return overridden_Permanent_FP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOverridden_Permanent_FP(Overridden_Permanent_FP newOverridden_Permanent_FP, NotificationChain msgs) {
		Overridden_Permanent_FP oldOverridden_Permanent_FP = overridden_Permanent_FP;
		overridden_Permanent_FP = newOverridden_Permanent_FP;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.COMPOSITE_OPERATION__OVERRIDDEN_PERMANENT_FP, oldOverridden_Permanent_FP, newOverridden_Permanent_FP);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOverridden_Permanent_FP(Overridden_Permanent_FP newOverridden_Permanent_FP) {
		if (newOverridden_Permanent_FP != overridden_Permanent_FP) {
			NotificationChain msgs = null;
			if (overridden_Permanent_FP != null)
				msgs = ((InternalEObject)overridden_Permanent_FP).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.COMPOSITE_OPERATION__OVERRIDDEN_PERMANENT_FP, null, msgs);
			if (newOverridden_Permanent_FP != null)
				msgs = ((InternalEObject)newOverridden_Permanent_FP).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.COMPOSITE_OPERATION__OVERRIDDEN_PERMANENT_FP, null, msgs);
			msgs = basicSetOverridden_Permanent_FP(newOverridden_Permanent_FP, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.COMPOSITE_OPERATION__OVERRIDDEN_PERMANENT_FP, newOverridden_Permanent_FP, newOverridden_Permanent_FP));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<String> getOperation_List() {
		return operation_List;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperation_List(List<String> newOperation_List) {
		List<String> oldOperation_List = operation_List;
		operation_List = newOperation_List;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.COMPOSITE_OPERATION__OPERATION_LIST, oldOperation_List, operation_List));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.COMPOSITE_OPERATION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelPackage.COMPOSITE_OPERATION__OVERRIDDEN_FIXED_PRIORITY:
				return basicSetOverridden_Fixed_Priority(null, msgs);
			case ModelPackage.COMPOSITE_OPERATION__OVERRIDDEN_PERMANENT_FP:
				return basicSetOverridden_Permanent_FP(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.COMPOSITE_OPERATION__OVERRIDDEN_FIXED_PRIORITY:
				return getOverridden_Fixed_Priority();
			case ModelPackage.COMPOSITE_OPERATION__OVERRIDDEN_PERMANENT_FP:
				return getOverridden_Permanent_FP();
			case ModelPackage.COMPOSITE_OPERATION__OPERATION_LIST:
				return getOperation_List();
			case ModelPackage.COMPOSITE_OPERATION__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.COMPOSITE_OPERATION__OVERRIDDEN_FIXED_PRIORITY:
				setOverridden_Fixed_Priority((Overridden_Fixed_Priority)newValue);
				return;
			case ModelPackage.COMPOSITE_OPERATION__OVERRIDDEN_PERMANENT_FP:
				setOverridden_Permanent_FP((Overridden_Permanent_FP)newValue);
				return;
			case ModelPackage.COMPOSITE_OPERATION__OPERATION_LIST:
				setOperation_List((List<String>)newValue);
				return;
			case ModelPackage.COMPOSITE_OPERATION__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.COMPOSITE_OPERATION__OVERRIDDEN_FIXED_PRIORITY:
				setOverridden_Fixed_Priority((Overridden_Fixed_Priority)null);
				return;
			case ModelPackage.COMPOSITE_OPERATION__OVERRIDDEN_PERMANENT_FP:
				setOverridden_Permanent_FP((Overridden_Permanent_FP)null);
				return;
			case ModelPackage.COMPOSITE_OPERATION__OPERATION_LIST:
				setOperation_List(OPERATION_LIST_EDEFAULT);
				return;
			case ModelPackage.COMPOSITE_OPERATION__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.COMPOSITE_OPERATION__OVERRIDDEN_FIXED_PRIORITY:
				return overridden_Fixed_Priority != null;
			case ModelPackage.COMPOSITE_OPERATION__OVERRIDDEN_PERMANENT_FP:
				return overridden_Permanent_FP != null;
			case ModelPackage.COMPOSITE_OPERATION__OPERATION_LIST:
				return OPERATION_LIST_EDEFAULT == null ? operation_List != null : !OPERATION_LIST_EDEFAULT.equals(operation_List);
			case ModelPackage.COMPOSITE_OPERATION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Operation_List: ");
		result.append(operation_List);
		result.append(", Name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //Composite_OperationImpl
