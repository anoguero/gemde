/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Delivery_Policy;
import es.esi.gemde.vv.mast.mastmodel.Delivery_Server;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;

import java.util.List;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Delivery Server</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Delivery_ServerImpl#getDelivery_Policy <em>Delivery Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Delivery_ServerImpl#getInput_Event <em>Input Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Delivery_ServerImpl#getOutput_Events_List <em>Output Events List</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Delivery_ServerImpl extends EObjectImpl implements Delivery_Server {
	/**
	 * The default value of the '{@link #getDelivery_Policy() <em>Delivery Policy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDelivery_Policy()
	 * @generated
	 * @ordered
	 */
	protected static final Delivery_Policy DELIVERY_POLICY_EDEFAULT = Delivery_Policy.SCAN;

	/**
	 * The cached value of the '{@link #getDelivery_Policy() <em>Delivery Policy</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDelivery_Policy()
	 * @generated
	 * @ordered
	 */
	protected Delivery_Policy delivery_Policy = DELIVERY_POLICY_EDEFAULT;

	/**
	 * This is true if the Delivery Policy attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean delivery_PolicyESet;

	/**
	 * The default value of the '{@link #getInput_Event() <em>Input Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInput_Event()
	 * @generated
	 * @ordered
	 */
	protected static final String INPUT_EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInput_Event() <em>Input Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInput_Event()
	 * @generated
	 * @ordered
	 */
	protected String input_Event = INPUT_EVENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getOutput_Events_List() <em>Output Events List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutput_Events_List()
	 * @generated
	 * @ordered
	 */
	protected static final List<String> OUTPUT_EVENTS_LIST_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOutput_Events_List() <em>Output Events List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOutput_Events_List()
	 * @generated
	 * @ordered
	 */
	protected List<String> output_Events_List = OUTPUT_EVENTS_LIST_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Delivery_ServerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.DELIVERY_SERVER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Delivery_Policy getDelivery_Policy() {
		return delivery_Policy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDelivery_Policy(Delivery_Policy newDelivery_Policy) {
		Delivery_Policy oldDelivery_Policy = delivery_Policy;
		delivery_Policy = newDelivery_Policy == null ? DELIVERY_POLICY_EDEFAULT : newDelivery_Policy;
		boolean oldDelivery_PolicyESet = delivery_PolicyESet;
		delivery_PolicyESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.DELIVERY_SERVER__DELIVERY_POLICY, oldDelivery_Policy, delivery_Policy, !oldDelivery_PolicyESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDelivery_Policy() {
		Delivery_Policy oldDelivery_Policy = delivery_Policy;
		boolean oldDelivery_PolicyESet = delivery_PolicyESet;
		delivery_Policy = DELIVERY_POLICY_EDEFAULT;
		delivery_PolicyESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ModelPackage.DELIVERY_SERVER__DELIVERY_POLICY, oldDelivery_Policy, DELIVERY_POLICY_EDEFAULT, oldDelivery_PolicyESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDelivery_Policy() {
		return delivery_PolicyESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInput_Event() {
		return input_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInput_Event(String newInput_Event) {
		String oldInput_Event = input_Event;
		input_Event = newInput_Event;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.DELIVERY_SERVER__INPUT_EVENT, oldInput_Event, input_Event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<String> getOutput_Events_List() {
		return output_Events_List;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOutput_Events_List(List<String> newOutput_Events_List) {
		List<String> oldOutput_Events_List = output_Events_List;
		output_Events_List = newOutput_Events_List;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.DELIVERY_SERVER__OUTPUT_EVENTS_LIST, oldOutput_Events_List, output_Events_List));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.DELIVERY_SERVER__DELIVERY_POLICY:
				return getDelivery_Policy();
			case ModelPackage.DELIVERY_SERVER__INPUT_EVENT:
				return getInput_Event();
			case ModelPackage.DELIVERY_SERVER__OUTPUT_EVENTS_LIST:
				return getOutput_Events_List();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.DELIVERY_SERVER__DELIVERY_POLICY:
				setDelivery_Policy((Delivery_Policy)newValue);
				return;
			case ModelPackage.DELIVERY_SERVER__INPUT_EVENT:
				setInput_Event((String)newValue);
				return;
			case ModelPackage.DELIVERY_SERVER__OUTPUT_EVENTS_LIST:
				setOutput_Events_List((List<String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.DELIVERY_SERVER__DELIVERY_POLICY:
				unsetDelivery_Policy();
				return;
			case ModelPackage.DELIVERY_SERVER__INPUT_EVENT:
				setInput_Event(INPUT_EVENT_EDEFAULT);
				return;
			case ModelPackage.DELIVERY_SERVER__OUTPUT_EVENTS_LIST:
				setOutput_Events_List(OUTPUT_EVENTS_LIST_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.DELIVERY_SERVER__DELIVERY_POLICY:
				return isSetDelivery_Policy();
			case ModelPackage.DELIVERY_SERVER__INPUT_EVENT:
				return INPUT_EVENT_EDEFAULT == null ? input_Event != null : !INPUT_EVENT_EDEFAULT.equals(input_Event);
			case ModelPackage.DELIVERY_SERVER__OUTPUT_EVENTS_LIST:
				return OUTPUT_EVENTS_LIST_EDEFAULT == null ? output_Events_List != null : !OUTPUT_EVENTS_LIST_EDEFAULT.equals(output_Events_List);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Delivery_Policy: ");
		if (delivery_PolicyESet) result.append(delivery_Policy); else result.append("<unset>");
		result.append(", Input_Event: ");
		result.append(input_Event);
		result.append(", Output_Events_List: ");
		result.append(output_Events_List);
		result.append(')');
		return result.toString();
	}

} //Delivery_ServerImpl
