/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Packet Driver</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getMessage_Partitioning <em>Message Partitioning</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getPacket_Receive_Operation <em>Packet Receive Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getPacket_Send_Operation <em>Packet Send Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getPacket_Server <em>Packet Server</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getRTA_Overhead_Model <em>RTA Overhead Model</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPacket_Driver()
 * @model extendedMetaData="name='Packet_Driver' kind='empty'"
 * @generated
 */
public interface Packet_Driver extends EObject {
	/**
	 * Returns the value of the '<em><b>Message Partitioning</b></em>' attribute.
	 * The literals are from the enumeration {@link es.esi.gemde.vv.mast.mastmodel.Assertion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Partitioning</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Partitioning</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Assertion
	 * @see #isSetMessage_Partitioning()
	 * @see #unsetMessage_Partitioning()
	 * @see #setMessage_Partitioning(Assertion)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPacket_Driver_Message_Partitioning()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='Message_Partitioning'"
	 * @generated
	 */
	Assertion getMessage_Partitioning();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getMessage_Partitioning <em>Message Partitioning</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Partitioning</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Assertion
	 * @see #isSetMessage_Partitioning()
	 * @see #unsetMessage_Partitioning()
	 * @see #getMessage_Partitioning()
	 * @generated
	 */
	void setMessage_Partitioning(Assertion value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getMessage_Partitioning <em>Message Partitioning</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMessage_Partitioning()
	 * @see #getMessage_Partitioning()
	 * @see #setMessage_Partitioning(Assertion)
	 * @generated
	 */
	void unsetMessage_Partitioning();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getMessage_Partitioning <em>Message Partitioning</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Message Partitioning</em>' attribute is set.
	 * @see #unsetMessage_Partitioning()
	 * @see #getMessage_Partitioning()
	 * @see #setMessage_Partitioning(Assertion)
	 * @generated
	 */
	boolean isSetMessage_Partitioning();

	/**
	 * Returns the value of the '<em><b>Packet Receive Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Packet Receive Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packet Receive Operation</em>' attribute.
	 * @see #setPacket_Receive_Operation(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPacket_Driver_Packet_Receive_Operation()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref"
	 *        extendedMetaData="kind='attribute' name='Packet_Receive_Operation'"
	 * @generated
	 */
	String getPacket_Receive_Operation();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getPacket_Receive_Operation <em>Packet Receive Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Packet Receive Operation</em>' attribute.
	 * @see #getPacket_Receive_Operation()
	 * @generated
	 */
	void setPacket_Receive_Operation(String value);

	/**
	 * Returns the value of the '<em><b>Packet Send Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Packet Send Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packet Send Operation</em>' attribute.
	 * @see #setPacket_Send_Operation(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPacket_Driver_Packet_Send_Operation()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref"
	 *        extendedMetaData="kind='attribute' name='Packet_Send_Operation'"
	 * @generated
	 */
	String getPacket_Send_Operation();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getPacket_Send_Operation <em>Packet Send Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Packet Send Operation</em>' attribute.
	 * @see #getPacket_Send_Operation()
	 * @generated
	 */
	void setPacket_Send_Operation(String value);

	/**
	 * Returns the value of the '<em><b>Packet Server</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Packet Server</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packet Server</em>' attribute.
	 * @see #setPacket_Server(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPacket_Driver_Packet_Server()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref" required="true"
	 *        extendedMetaData="kind='attribute' name='Packet_Server'"
	 * @generated
	 */
	String getPacket_Server();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getPacket_Server <em>Packet Server</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Packet Server</em>' attribute.
	 * @see #getPacket_Server()
	 * @generated
	 */
	void setPacket_Server(String value);

	/**
	 * Returns the value of the '<em><b>RTA Overhead Model</b></em>' attribute.
	 * The literals are from the enumeration {@link es.esi.gemde.vv.mast.mastmodel.Overhead_Type}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>RTA Overhead Model</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>RTA Overhead Model</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Overhead_Type
	 * @see #isSetRTA_Overhead_Model()
	 * @see #unsetRTA_Overhead_Model()
	 * @see #setRTA_Overhead_Model(Overhead_Type)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPacket_Driver_RTA_Overhead_Model()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='RTA_Overhead_Model'"
	 * @generated
	 */
	Overhead_Type getRTA_Overhead_Model();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getRTA_Overhead_Model <em>RTA Overhead Model</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>RTA Overhead Model</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Overhead_Type
	 * @see #isSetRTA_Overhead_Model()
	 * @see #unsetRTA_Overhead_Model()
	 * @see #getRTA_Overhead_Model()
	 * @generated
	 */
	void setRTA_Overhead_Model(Overhead_Type value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getRTA_Overhead_Model <em>RTA Overhead Model</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetRTA_Overhead_Model()
	 * @see #getRTA_Overhead_Model()
	 * @see #setRTA_Overhead_Model(Overhead_Type)
	 * @generated
	 */
	void unsetRTA_Overhead_Model();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver#getRTA_Overhead_Model <em>RTA Overhead Model</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>RTA Overhead Model</em>' attribute is set.
	 * @see #unsetRTA_Overhead_Model()
	 * @see #getRTA_Overhead_Model()
	 * @see #setRTA_Overhead_Model(Overhead_Type)
	 * @generated
	 */
	boolean isSetRTA_Overhead_Model();

} // Packet_Driver
