/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Alarm_Clock_System_Timer;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Regular_Processor;
import es.esi.gemde.vv.mast.mastmodel.Ticker_System_Timer;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Regular Processor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_ProcessorImpl#getTicker_System_Timer <em>Ticker System Timer</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_ProcessorImpl#getAlarm_Clock_System_Timer <em>Alarm Clock System Timer</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_ProcessorImpl#getAvg_ISR_Switch <em>Avg ISR Switch</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_ProcessorImpl#getBest_ISR_Switch <em>Best ISR Switch</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_ProcessorImpl#getMax_Interrupt_Priority <em>Max Interrupt Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_ProcessorImpl#getMin_Interrupt_Priority <em>Min Interrupt Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_ProcessorImpl#getName <em>Name</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_ProcessorImpl#getSpeed_Factor <em>Speed Factor</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_ProcessorImpl#getWorst_ISR_Switch <em>Worst ISR Switch</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Regular_ProcessorImpl extends EObjectImpl implements Regular_Processor {
	/**
	 * The cached value of the '{@link #getTicker_System_Timer() <em>Ticker System Timer</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTicker_System_Timer()
	 * @generated
	 * @ordered
	 */
	protected Ticker_System_Timer ticker_System_Timer;

	/**
	 * The cached value of the '{@link #getAlarm_Clock_System_Timer() <em>Alarm Clock System Timer</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAlarm_Clock_System_Timer()
	 * @generated
	 * @ordered
	 */
	protected Alarm_Clock_System_Timer alarm_Clock_System_Timer;

	/**
	 * The default value of the '{@link #getAvg_ISR_Switch() <em>Avg ISR Switch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvg_ISR_Switch()
	 * @generated
	 * @ordered
	 */
	protected static final double AVG_ISR_SWITCH_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getAvg_ISR_Switch() <em>Avg ISR Switch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvg_ISR_Switch()
	 * @generated
	 * @ordered
	 */
	protected double avg_ISR_Switch = AVG_ISR_SWITCH_EDEFAULT;

	/**
	 * The default value of the '{@link #getBest_ISR_Switch() <em>Best ISR Switch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBest_ISR_Switch()
	 * @generated
	 * @ordered
	 */
	protected static final double BEST_ISR_SWITCH_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getBest_ISR_Switch() <em>Best ISR Switch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBest_ISR_Switch()
	 * @generated
	 * @ordered
	 */
	protected double best_ISR_Switch = BEST_ISR_SWITCH_EDEFAULT;

	/**
	 * The default value of the '{@link #getMax_Interrupt_Priority() <em>Max Interrupt Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_Interrupt_Priority()
	 * @generated
	 * @ordered
	 */
	protected static final int MAX_INTERRUPT_PRIORITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMax_Interrupt_Priority() <em>Max Interrupt Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_Interrupt_Priority()
	 * @generated
	 * @ordered
	 */
	protected int max_Interrupt_Priority = MAX_INTERRUPT_PRIORITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getMin_Interrupt_Priority() <em>Min Interrupt Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin_Interrupt_Priority()
	 * @generated
	 * @ordered
	 */
	protected static final int MIN_INTERRUPT_PRIORITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMin_Interrupt_Priority() <em>Min Interrupt Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin_Interrupt_Priority()
	 * @generated
	 * @ordered
	 */
	protected int min_Interrupt_Priority = MIN_INTERRUPT_PRIORITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSpeed_Factor() <em>Speed Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpeed_Factor()
	 * @generated
	 * @ordered
	 */
	protected static final double SPEED_FACTOR_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getSpeed_Factor() <em>Speed Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpeed_Factor()
	 * @generated
	 * @ordered
	 */
	protected double speed_Factor = SPEED_FACTOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getWorst_ISR_Switch() <em>Worst ISR Switch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorst_ISR_Switch()
	 * @generated
	 * @ordered
	 */
	protected static final double WORST_ISR_SWITCH_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getWorst_ISR_Switch() <em>Worst ISR Switch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWorst_ISR_Switch()
	 * @generated
	 * @ordered
	 */
	protected double worst_ISR_Switch = WORST_ISR_SWITCH_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Regular_ProcessorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.REGULAR_PROCESSOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ticker_System_Timer getTicker_System_Timer() {
		return ticker_System_Timer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTicker_System_Timer(Ticker_System_Timer newTicker_System_Timer, NotificationChain msgs) {
		Ticker_System_Timer oldTicker_System_Timer = ticker_System_Timer;
		ticker_System_Timer = newTicker_System_Timer;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_PROCESSOR__TICKER_SYSTEM_TIMER, oldTicker_System_Timer, newTicker_System_Timer);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTicker_System_Timer(Ticker_System_Timer newTicker_System_Timer) {
		if (newTicker_System_Timer != ticker_System_Timer) {
			NotificationChain msgs = null;
			if (ticker_System_Timer != null)
				msgs = ((InternalEObject)ticker_System_Timer).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.REGULAR_PROCESSOR__TICKER_SYSTEM_TIMER, null, msgs);
			if (newTicker_System_Timer != null)
				msgs = ((InternalEObject)newTicker_System_Timer).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.REGULAR_PROCESSOR__TICKER_SYSTEM_TIMER, null, msgs);
			msgs = basicSetTicker_System_Timer(newTicker_System_Timer, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_PROCESSOR__TICKER_SYSTEM_TIMER, newTicker_System_Timer, newTicker_System_Timer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Alarm_Clock_System_Timer getAlarm_Clock_System_Timer() {
		return alarm_Clock_System_Timer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAlarm_Clock_System_Timer(Alarm_Clock_System_Timer newAlarm_Clock_System_Timer, NotificationChain msgs) {
		Alarm_Clock_System_Timer oldAlarm_Clock_System_Timer = alarm_Clock_System_Timer;
		alarm_Clock_System_Timer = newAlarm_Clock_System_Timer;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_PROCESSOR__ALARM_CLOCK_SYSTEM_TIMER, oldAlarm_Clock_System_Timer, newAlarm_Clock_System_Timer);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAlarm_Clock_System_Timer(Alarm_Clock_System_Timer newAlarm_Clock_System_Timer) {
		if (newAlarm_Clock_System_Timer != alarm_Clock_System_Timer) {
			NotificationChain msgs = null;
			if (alarm_Clock_System_Timer != null)
				msgs = ((InternalEObject)alarm_Clock_System_Timer).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.REGULAR_PROCESSOR__ALARM_CLOCK_SYSTEM_TIMER, null, msgs);
			if (newAlarm_Clock_System_Timer != null)
				msgs = ((InternalEObject)newAlarm_Clock_System_Timer).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.REGULAR_PROCESSOR__ALARM_CLOCK_SYSTEM_TIMER, null, msgs);
			msgs = basicSetAlarm_Clock_System_Timer(newAlarm_Clock_System_Timer, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_PROCESSOR__ALARM_CLOCK_SYSTEM_TIMER, newAlarm_Clock_System_Timer, newAlarm_Clock_System_Timer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getAvg_ISR_Switch() {
		return avg_ISR_Switch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAvg_ISR_Switch(double newAvg_ISR_Switch) {
		double oldAvg_ISR_Switch = avg_ISR_Switch;
		avg_ISR_Switch = newAvg_ISR_Switch;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_PROCESSOR__AVG_ISR_SWITCH, oldAvg_ISR_Switch, avg_ISR_Switch));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getBest_ISR_Switch() {
		return best_ISR_Switch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBest_ISR_Switch(double newBest_ISR_Switch) {
		double oldBest_ISR_Switch = best_ISR_Switch;
		best_ISR_Switch = newBest_ISR_Switch;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_PROCESSOR__BEST_ISR_SWITCH, oldBest_ISR_Switch, best_ISR_Switch));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMax_Interrupt_Priority() {
		return max_Interrupt_Priority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMax_Interrupt_Priority(int newMax_Interrupt_Priority) {
		int oldMax_Interrupt_Priority = max_Interrupt_Priority;
		max_Interrupt_Priority = newMax_Interrupt_Priority;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_PROCESSOR__MAX_INTERRUPT_PRIORITY, oldMax_Interrupt_Priority, max_Interrupt_Priority));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMin_Interrupt_Priority() {
		return min_Interrupt_Priority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMin_Interrupt_Priority(int newMin_Interrupt_Priority) {
		int oldMin_Interrupt_Priority = min_Interrupt_Priority;
		min_Interrupt_Priority = newMin_Interrupt_Priority;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_PROCESSOR__MIN_INTERRUPT_PRIORITY, oldMin_Interrupt_Priority, min_Interrupt_Priority));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_PROCESSOR__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getSpeed_Factor() {
		return speed_Factor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpeed_Factor(double newSpeed_Factor) {
		double oldSpeed_Factor = speed_Factor;
		speed_Factor = newSpeed_Factor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_PROCESSOR__SPEED_FACTOR, oldSpeed_Factor, speed_Factor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getWorst_ISR_Switch() {
		return worst_ISR_Switch;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWorst_ISR_Switch(double newWorst_ISR_Switch) {
		double oldWorst_ISR_Switch = worst_ISR_Switch;
		worst_ISR_Switch = newWorst_ISR_Switch;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_PROCESSOR__WORST_ISR_SWITCH, oldWorst_ISR_Switch, worst_ISR_Switch));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelPackage.REGULAR_PROCESSOR__TICKER_SYSTEM_TIMER:
				return basicSetTicker_System_Timer(null, msgs);
			case ModelPackage.REGULAR_PROCESSOR__ALARM_CLOCK_SYSTEM_TIMER:
				return basicSetAlarm_Clock_System_Timer(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.REGULAR_PROCESSOR__TICKER_SYSTEM_TIMER:
				return getTicker_System_Timer();
			case ModelPackage.REGULAR_PROCESSOR__ALARM_CLOCK_SYSTEM_TIMER:
				return getAlarm_Clock_System_Timer();
			case ModelPackage.REGULAR_PROCESSOR__AVG_ISR_SWITCH:
				return getAvg_ISR_Switch();
			case ModelPackage.REGULAR_PROCESSOR__BEST_ISR_SWITCH:
				return getBest_ISR_Switch();
			case ModelPackage.REGULAR_PROCESSOR__MAX_INTERRUPT_PRIORITY:
				return getMax_Interrupt_Priority();
			case ModelPackage.REGULAR_PROCESSOR__MIN_INTERRUPT_PRIORITY:
				return getMin_Interrupt_Priority();
			case ModelPackage.REGULAR_PROCESSOR__NAME:
				return getName();
			case ModelPackage.REGULAR_PROCESSOR__SPEED_FACTOR:
				return getSpeed_Factor();
			case ModelPackage.REGULAR_PROCESSOR__WORST_ISR_SWITCH:
				return getWorst_ISR_Switch();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.REGULAR_PROCESSOR__TICKER_SYSTEM_TIMER:
				setTicker_System_Timer((Ticker_System_Timer)newValue);
				return;
			case ModelPackage.REGULAR_PROCESSOR__ALARM_CLOCK_SYSTEM_TIMER:
				setAlarm_Clock_System_Timer((Alarm_Clock_System_Timer)newValue);
				return;
			case ModelPackage.REGULAR_PROCESSOR__AVG_ISR_SWITCH:
				setAvg_ISR_Switch((Double)newValue);
				return;
			case ModelPackage.REGULAR_PROCESSOR__BEST_ISR_SWITCH:
				setBest_ISR_Switch((Double)newValue);
				return;
			case ModelPackage.REGULAR_PROCESSOR__MAX_INTERRUPT_PRIORITY:
				setMax_Interrupt_Priority((Integer)newValue);
				return;
			case ModelPackage.REGULAR_PROCESSOR__MIN_INTERRUPT_PRIORITY:
				setMin_Interrupt_Priority((Integer)newValue);
				return;
			case ModelPackage.REGULAR_PROCESSOR__NAME:
				setName((String)newValue);
				return;
			case ModelPackage.REGULAR_PROCESSOR__SPEED_FACTOR:
				setSpeed_Factor((Double)newValue);
				return;
			case ModelPackage.REGULAR_PROCESSOR__WORST_ISR_SWITCH:
				setWorst_ISR_Switch((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.REGULAR_PROCESSOR__TICKER_SYSTEM_TIMER:
				setTicker_System_Timer((Ticker_System_Timer)null);
				return;
			case ModelPackage.REGULAR_PROCESSOR__ALARM_CLOCK_SYSTEM_TIMER:
				setAlarm_Clock_System_Timer((Alarm_Clock_System_Timer)null);
				return;
			case ModelPackage.REGULAR_PROCESSOR__AVG_ISR_SWITCH:
				setAvg_ISR_Switch(AVG_ISR_SWITCH_EDEFAULT);
				return;
			case ModelPackage.REGULAR_PROCESSOR__BEST_ISR_SWITCH:
				setBest_ISR_Switch(BEST_ISR_SWITCH_EDEFAULT);
				return;
			case ModelPackage.REGULAR_PROCESSOR__MAX_INTERRUPT_PRIORITY:
				setMax_Interrupt_Priority(MAX_INTERRUPT_PRIORITY_EDEFAULT);
				return;
			case ModelPackage.REGULAR_PROCESSOR__MIN_INTERRUPT_PRIORITY:
				setMin_Interrupt_Priority(MIN_INTERRUPT_PRIORITY_EDEFAULT);
				return;
			case ModelPackage.REGULAR_PROCESSOR__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ModelPackage.REGULAR_PROCESSOR__SPEED_FACTOR:
				setSpeed_Factor(SPEED_FACTOR_EDEFAULT);
				return;
			case ModelPackage.REGULAR_PROCESSOR__WORST_ISR_SWITCH:
				setWorst_ISR_Switch(WORST_ISR_SWITCH_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.REGULAR_PROCESSOR__TICKER_SYSTEM_TIMER:
				return ticker_System_Timer != null;
			case ModelPackage.REGULAR_PROCESSOR__ALARM_CLOCK_SYSTEM_TIMER:
				return alarm_Clock_System_Timer != null;
			case ModelPackage.REGULAR_PROCESSOR__AVG_ISR_SWITCH:
				return avg_ISR_Switch != AVG_ISR_SWITCH_EDEFAULT;
			case ModelPackage.REGULAR_PROCESSOR__BEST_ISR_SWITCH:
				return best_ISR_Switch != BEST_ISR_SWITCH_EDEFAULT;
			case ModelPackage.REGULAR_PROCESSOR__MAX_INTERRUPT_PRIORITY:
				return max_Interrupt_Priority != MAX_INTERRUPT_PRIORITY_EDEFAULT;
			case ModelPackage.REGULAR_PROCESSOR__MIN_INTERRUPT_PRIORITY:
				return min_Interrupt_Priority != MIN_INTERRUPT_PRIORITY_EDEFAULT;
			case ModelPackage.REGULAR_PROCESSOR__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ModelPackage.REGULAR_PROCESSOR__SPEED_FACTOR:
				return speed_Factor != SPEED_FACTOR_EDEFAULT;
			case ModelPackage.REGULAR_PROCESSOR__WORST_ISR_SWITCH:
				return worst_ISR_Switch != WORST_ISR_SWITCH_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Avg_ISR_Switch: ");
		result.append(avg_ISR_Switch);
		result.append(", Best_ISR_Switch: ");
		result.append(best_ISR_Switch);
		result.append(", Max_Interrupt_Priority: ");
		result.append(max_Interrupt_Priority);
		result.append(", Min_Interrupt_Priority: ");
		result.append(min_Interrupt_Priority);
		result.append(", Name: ");
		result.append(name);
		result.append(", Speed_Factor: ");
		result.append(speed_Factor);
		result.append(", Worst_ISR_Switch: ");
		result.append(worst_ISR_Switch);
		result.append(')');
		return result.toString();
	}

} //Regular_ProcessorImpl
