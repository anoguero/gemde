/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alarm Clock System Timer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Alarm_Clock_System_Timer#getAvg_Overhead <em>Avg Overhead</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Alarm_Clock_System_Timer#getBest_Overhead <em>Best Overhead</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Alarm_Clock_System_Timer#getWorst_Overhead <em>Worst Overhead</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getAlarm_Clock_System_Timer()
 * @model extendedMetaData="name='Alarm_Clock_System_Timer' kind='empty'"
 * @generated
 */
public interface Alarm_Clock_System_Timer extends EObject {
	/**
	 * Returns the value of the '<em><b>Avg Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Avg Overhead</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Avg Overhead</em>' attribute.
	 * @see #setAvg_Overhead(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getAlarm_Clock_System_Timer_Avg_Overhead()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Avg_Overhead'"
	 * @generated
	 */
	double getAvg_Overhead();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Alarm_Clock_System_Timer#getAvg_Overhead <em>Avg Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Avg Overhead</em>' attribute.
	 * @see #getAvg_Overhead()
	 * @generated
	 */
	void setAvg_Overhead(double value);

	/**
	 * Returns the value of the '<em><b>Best Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Best Overhead</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Best Overhead</em>' attribute.
	 * @see #setBest_Overhead(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getAlarm_Clock_System_Timer_Best_Overhead()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Best_Overhead'"
	 * @generated
	 */
	double getBest_Overhead();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Alarm_Clock_System_Timer#getBest_Overhead <em>Best Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Best Overhead</em>' attribute.
	 * @see #getBest_Overhead()
	 * @generated
	 */
	void setBest_Overhead(double value);

	/**
	 * Returns the value of the '<em><b>Worst Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Worst Overhead</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Worst Overhead</em>' attribute.
	 * @see #setWorst_Overhead(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getAlarm_Clock_System_Timer_Worst_Overhead()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Worst_Overhead'"
	 * @generated
	 */
	double getWorst_Overhead();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Alarm_Clock_System_Timer#getWorst_Overhead <em>Worst Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Worst Overhead</em>' attribute.
	 * @see #getWorst_Overhead()
	 * @generated
	 */
	void setWorst_Overhead(double value);

} // Alarm_Clock_System_Timer
