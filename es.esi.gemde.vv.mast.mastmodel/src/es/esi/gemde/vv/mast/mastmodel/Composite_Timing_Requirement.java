/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Timing Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getGroup <em>Group</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getMax_Output_Jitter_Req <em>Max Output Jitter Req</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getHard_Global_Deadline <em>Hard Global Deadline</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getSoft_Global_Deadline <em>Soft Global Deadline</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getGlobal_Max_Miss_Ratio <em>Global Max Miss Ratio</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getHard_Local_Deadline <em>Hard Local Deadline</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getSoft_Local_Deadline <em>Soft Local Deadline</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement#getLocal_Max_Miss_Ratio <em>Local Max Miss Ratio</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getComposite_Timing_Requirement()
 * @model extendedMetaData="name='Composite_Timing_Requirement' kind='elementOnly'"
 * @generated
 */
public interface Composite_Timing_Requirement extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getComposite_Timing_Requirement_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Max Output Jitter Req</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Max_Output_Jitter_Req}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Output Jitter Req</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Output Jitter Req</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getComposite_Timing_Requirement_Max_Output_Jitter_Req()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Max_Output_Jitter_Req' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Max_Output_Jitter_Req> getMax_Output_Jitter_Req();

	/**
	 * Returns the value of the '<em><b>Hard Global Deadline</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Hard_Global_Deadline}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hard Global Deadline</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hard Global Deadline</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getComposite_Timing_Requirement_Hard_Global_Deadline()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Hard_Global_Deadline' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Hard_Global_Deadline> getHard_Global_Deadline();

	/**
	 * Returns the value of the '<em><b>Soft Global Deadline</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Soft_Global_Deadline}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Soft Global Deadline</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Soft Global Deadline</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getComposite_Timing_Requirement_Soft_Global_Deadline()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Soft_Global_Deadline' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Soft_Global_Deadline> getSoft_Global_Deadline();

	/**
	 * Returns the value of the '<em><b>Global Max Miss Ratio</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Global_Max_Miss_Ratio}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Global Max Miss Ratio</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Global Max Miss Ratio</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getComposite_Timing_Requirement_Global_Max_Miss_Ratio()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Global_Max_Miss_Ratio' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Global_Max_Miss_Ratio> getGlobal_Max_Miss_Ratio();

	/**
	 * Returns the value of the '<em><b>Hard Local Deadline</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Hard_Local_Deadline}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hard Local Deadline</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hard Local Deadline</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getComposite_Timing_Requirement_Hard_Local_Deadline()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Hard_Local_Deadline' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Hard_Local_Deadline> getHard_Local_Deadline();

	/**
	 * Returns the value of the '<em><b>Soft Local Deadline</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Soft_Local_Deadline}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Soft Local Deadline</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Soft Local Deadline</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getComposite_Timing_Requirement_Soft_Local_Deadline()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Soft_Local_Deadline' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Soft_Local_Deadline> getSoft_Local_Deadline();

	/**
	 * Returns the value of the '<em><b>Local Max Miss Ratio</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Local_Max_Miss_Ratio}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Max Miss Ratio</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Max Miss Ratio</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getComposite_Timing_Requirement_Local_Max_Miss_Ratio()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Local_Max_Miss_Ratio' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Local_Max_Miss_Ratio> getLocal_Max_Miss_Ratio();

} // Composite_Timing_Requirement
