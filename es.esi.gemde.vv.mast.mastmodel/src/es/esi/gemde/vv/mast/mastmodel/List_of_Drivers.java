/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>List of Drivers</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.List_of_Drivers#getGroup <em>Group</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.List_of_Drivers#getPacket_Driver <em>Packet Driver</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.List_of_Drivers#getCharacter_Packet_Driver <em>Character Packet Driver</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.List_of_Drivers#getRTEP_Packet_Driver <em>RTEP Packet Driver</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getList_of_Drivers()
 * @model extendedMetaData="name='List_of_Drivers' kind='elementOnly'"
 * @generated
 */
public interface List_of_Drivers extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getList_of_Drivers_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Packet Driver</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Packet_Driver}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Packet Driver</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Packet Driver</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getList_of_Drivers_Packet_Driver()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Packet_Driver' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Packet_Driver> getPacket_Driver();

	/**
	 * Returns the value of the '<em><b>Character Packet Driver</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.Character_Packet_Driver}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Character Packet Driver</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Character Packet Driver</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getList_of_Drivers_Character_Packet_Driver()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Character_Packet_Driver' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Character_Packet_Driver> getCharacter_Packet_Driver();

	/**
	 * Returns the value of the '<em><b>RTEP Packet Driver</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>RTEP Packet Driver</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>RTEP Packet Driver</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getList_of_Drivers_RTEP_Packet_Driver()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='RTEP_Packet_Driver' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<RTEP_PacketDriver> getRTEP_Packet_Driver();

} // List_of_Drivers
