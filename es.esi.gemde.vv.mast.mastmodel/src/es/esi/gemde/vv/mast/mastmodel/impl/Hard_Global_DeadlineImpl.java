/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Hard_Global_Deadline;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hard Global Deadline</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Hard_Global_DeadlineImpl#getDeadline <em>Deadline</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Hard_Global_DeadlineImpl#getReferenced_Event <em>Referenced Event</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Hard_Global_DeadlineImpl extends EObjectImpl implements Hard_Global_Deadline {
	/**
	 * The default value of the '{@link #getDeadline() <em>Deadline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeadline()
	 * @generated
	 * @ordered
	 */
	protected static final double DEADLINE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getDeadline() <em>Deadline</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeadline()
	 * @generated
	 * @ordered
	 */
	protected double deadline = DEADLINE_EDEFAULT;

	/**
	 * The default value of the '{@link #getReferenced_Event() <em>Referenced Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenced_Event()
	 * @generated
	 * @ordered
	 */
	protected static final String REFERENCED_EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReferenced_Event() <em>Referenced Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenced_Event()
	 * @generated
	 * @ordered
	 */
	protected String referenced_Event = REFERENCED_EVENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Hard_Global_DeadlineImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.HARD_GLOBAL_DEADLINE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getDeadline() {
		return deadline;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeadline(double newDeadline) {
		double oldDeadline = deadline;
		deadline = newDeadline;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.HARD_GLOBAL_DEADLINE__DEADLINE, oldDeadline, deadline));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getReferenced_Event() {
		return referenced_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferenced_Event(String newReferenced_Event) {
		String oldReferenced_Event = referenced_Event;
		referenced_Event = newReferenced_Event;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.HARD_GLOBAL_DEADLINE__REFERENCED_EVENT, oldReferenced_Event, referenced_Event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.HARD_GLOBAL_DEADLINE__DEADLINE:
				return getDeadline();
			case ModelPackage.HARD_GLOBAL_DEADLINE__REFERENCED_EVENT:
				return getReferenced_Event();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.HARD_GLOBAL_DEADLINE__DEADLINE:
				setDeadline((Double)newValue);
				return;
			case ModelPackage.HARD_GLOBAL_DEADLINE__REFERENCED_EVENT:
				setReferenced_Event((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.HARD_GLOBAL_DEADLINE__DEADLINE:
				setDeadline(DEADLINE_EDEFAULT);
				return;
			case ModelPackage.HARD_GLOBAL_DEADLINE__REFERENCED_EVENT:
				setReferenced_Event(REFERENCED_EVENT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.HARD_GLOBAL_DEADLINE__DEADLINE:
				return deadline != DEADLINE_EDEFAULT;
			case ModelPackage.HARD_GLOBAL_DEADLINE__REFERENCED_EVENT:
				return REFERENCED_EVENT_EDEFAULT == null ? referenced_Event != null : !REFERENCED_EVENT_EDEFAULT.equals(referenced_Event);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Deadline: ");
		result.append(deadline);
		result.append(", Referenced_Event: ");
		result.append(referenced_Event);
		result.append(')');
		return result.toString();
	}

} //Hard_Global_DeadlineImpl
