/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Polling Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPolling_Avg_Overhead <em>Polling Avg Overhead</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPolling_Best_Overhead <em>Polling Best Overhead</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPolling_Period <em>Polling Period</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPolling_Worst_Overhead <em>Polling Worst Overhead</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPreassigned <em>Preassigned</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getThe_Priority <em>The Priority</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPolling_Policy()
 * @model extendedMetaData="name='Polling_Policy' kind='empty'"
 * @generated
 */
public interface Polling_Policy extends EObject {
	/**
	 * Returns the value of the '<em><b>Polling Avg Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Polling Avg Overhead</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Polling Avg Overhead</em>' attribute.
	 * @see #setPolling_Avg_Overhead(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPolling_Policy_Polling_Avg_Overhead()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Polling_Avg_Overhead'"
	 * @generated
	 */
	double getPolling_Avg_Overhead();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPolling_Avg_Overhead <em>Polling Avg Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Polling Avg Overhead</em>' attribute.
	 * @see #getPolling_Avg_Overhead()
	 * @generated
	 */
	void setPolling_Avg_Overhead(double value);

	/**
	 * Returns the value of the '<em><b>Polling Best Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Polling Best Overhead</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Polling Best Overhead</em>' attribute.
	 * @see #setPolling_Best_Overhead(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPolling_Policy_Polling_Best_Overhead()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Polling_Best_Overhead'"
	 * @generated
	 */
	double getPolling_Best_Overhead();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPolling_Best_Overhead <em>Polling Best Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Polling Best Overhead</em>' attribute.
	 * @see #getPolling_Best_Overhead()
	 * @generated
	 */
	void setPolling_Best_Overhead(double value);

	/**
	 * Returns the value of the '<em><b>Polling Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Polling Period</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Polling Period</em>' attribute.
	 * @see #setPolling_Period(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPolling_Policy_Polling_Period()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Time"
	 *        extendedMetaData="kind='attribute' name='Polling_Period'"
	 * @generated
	 */
	double getPolling_Period();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPolling_Period <em>Polling Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Polling Period</em>' attribute.
	 * @see #getPolling_Period()
	 * @generated
	 */
	void setPolling_Period(double value);

	/**
	 * Returns the value of the '<em><b>Polling Worst Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Polling Worst Overhead</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Polling Worst Overhead</em>' attribute.
	 * @see #setPolling_Worst_Overhead(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPolling_Policy_Polling_Worst_Overhead()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Polling_Worst_Overhead'"
	 * @generated
	 */
	double getPolling_Worst_Overhead();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPolling_Worst_Overhead <em>Polling Worst Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Polling Worst Overhead</em>' attribute.
	 * @see #getPolling_Worst_Overhead()
	 * @generated
	 */
	void setPolling_Worst_Overhead(double value);

	/**
	 * Returns the value of the '<em><b>Preassigned</b></em>' attribute.
	 * The literals are from the enumeration {@link es.esi.gemde.vv.mast.mastmodel.Assertion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preassigned</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preassigned</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Assertion
	 * @see #isSetPreassigned()
	 * @see #unsetPreassigned()
	 * @see #setPreassigned(Assertion)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPolling_Policy_Preassigned()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='Preassigned'"
	 * @generated
	 */
	Assertion getPreassigned();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPreassigned <em>Preassigned</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preassigned</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastmodel.Assertion
	 * @see #isSetPreassigned()
	 * @see #unsetPreassigned()
	 * @see #getPreassigned()
	 * @generated
	 */
	void setPreassigned(Assertion value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPreassigned <em>Preassigned</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPreassigned()
	 * @see #getPreassigned()
	 * @see #setPreassigned(Assertion)
	 * @generated
	 */
	void unsetPreassigned();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getPreassigned <em>Preassigned</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Preassigned</em>' attribute is set.
	 * @see #unsetPreassigned()
	 * @see #getPreassigned()
	 * @see #setPreassigned(Assertion)
	 * @generated
	 */
	boolean isSetPreassigned();

	/**
	 * Returns the value of the '<em><b>The Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>The Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>The Priority</em>' attribute.
	 * @see #setThe_Priority(int)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getPolling_Policy_The_Priority()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Priority"
	 *        extendedMetaData="kind='attribute' name='The_Priority'"
	 * @generated
	 */
	int getThe_Priority();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Polling_Policy#getThe_Priority <em>The Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>The Priority</em>' attribute.
	 * @see #getThe_Priority()
	 * @generated
	 */
	void setThe_Priority(int value);

} // Polling_Policy
