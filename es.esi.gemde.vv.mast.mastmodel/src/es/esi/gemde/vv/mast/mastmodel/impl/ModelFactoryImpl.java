/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.*;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.xml.type.XMLTypeFactory;
import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ModelFactoryImpl extends EFactoryImpl implements ModelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ModelFactory init() {
		try {
			ModelFactory theModelFactory = (ModelFactory)EPackage.Registry.INSTANCE.getEFactory("http://mast.unican.es/xmlmast/model"); 
			if (theModelFactory != null) {
				return theModelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ModelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ModelPackage.ACTIVITY: return createActivity();
			case ModelPackage.ALARM_CLOCK_SYSTEM_TIMER: return createAlarm_Clock_System_Timer();
			case ModelPackage.BARRIER: return createBarrier();
			case ModelPackage.BURSTY_EXTERNAL_EVENT: return createBursty_External_Event();
			case ModelPackage.CHARACTER_PACKET_DRIVER: return createCharacter_Packet_Driver();
			case ModelPackage.COMPOSITE_OPERATION: return createComposite_Operation();
			case ModelPackage.COMPOSITE_TIMING_REQUIREMENT: return createComposite_Timing_Requirement();
			case ModelPackage.CONCENTRATOR: return createConcentrator();
			case ModelPackage.DELAY: return createDelay();
			case ModelPackage.DELIVERY_SERVER: return createDelivery_Server();
			case ModelPackage.DOCUMENT_ROOT: return createDocument_Root();
			case ModelPackage.EDF_POLICY: return createEDF_Policy();
			case ModelPackage.EDF_SCHEDULER: return createEDF_Scheduler();
			case ModelPackage.ENCLOSING_OPERATION: return createEnclosing_Operation();
			case ModelPackage.FIXED_PRIORITY_POLICY: return createFixed_Priority_Policy();
			case ModelPackage.FIXED_PRIORITY_SCHEDULER: return createFixed_Priority_Scheduler();
			case ModelPackage.FP_PACKET_BASED_SCHEDULER: return createFP_Packet_Based_Scheduler();
			case ModelPackage.GLOBAL_MAX_MISS_RATIO: return createGlobal_Max_Miss_Ratio();
			case ModelPackage.HARD_GLOBAL_DEADLINE: return createHard_Global_Deadline();
			case ModelPackage.HARD_LOCAL_DEADLINE: return createHard_Local_Deadline();
			case ModelPackage.IMMEDIATE_CEILING_RESOURCE: return createImmediate_Ceiling_Resource();
			case ModelPackage.INTERRUPT_FP_POLICY: return createInterrupt_FP_Policy();
			case ModelPackage.LIST_OF_DRIVERS: return createList_of_Drivers();
			case ModelPackage.LOCAL_MAX_MISS_RATIO: return createLocal_Max_Miss_Ratio();
			case ModelPackage.MAST_MODEL: return createMAST_MODEL();
			case ModelPackage.MAX_OUTPUT_JITTER_REQ: return createMax_Output_Jitter_Req();
			case ModelPackage.MESSAGE_TRANSMISSION: return createMessage_Transmission();
			case ModelPackage.MULTICAST: return createMulticast();
			case ModelPackage.NON_PREEMPTIBLE_FP_POLICY: return createNon_Preemptible_FP_Policy();
			case ModelPackage.OFFSET: return createOffset();
			case ModelPackage.OVERRIDDEN_FIXED_PRIORITY: return createOverridden_Fixed_Priority();
			case ModelPackage.OVERRIDDEN_PERMANENT_FP: return createOverridden_Permanent_FP();
			case ModelPackage.PACKET_BASED_NETWORK: return createPacket_Based_Network();
			case ModelPackage.PACKET_DRIVER: return createPacket_Driver();
			case ModelPackage.PERIODIC_EXTERNAL_EVENT: return createPeriodic_External_Event();
			case ModelPackage.POLLING_POLICY: return createPolling_Policy();
			case ModelPackage.PRIMARY_SCHEDULER: return createPrimary_Scheduler();
			case ModelPackage.PRIORITY_INHERITANCE_RESOURCE: return createPriority_Inheritance_Resource();
			case ModelPackage.QUERY_SERVER: return createQuery_Server();
			case ModelPackage.RATE_DIVISOR: return createRate_Divisor();
			case ModelPackage.REGULAR_EVENT: return createRegular_Event();
			case ModelPackage.REGULAR_PROCESSOR: return createRegular_Processor();
			case ModelPackage.REGULAR_SCHEDULING_SERVER: return createRegular_Scheduling_Server();
			case ModelPackage.REGULAR_TRANSACTION: return createRegular_Transaction();
			case ModelPackage.RTEP_PACKET_DRIVER: return createRTEP_PacketDriver();
			case ModelPackage.SECONDARY_SCHEDULER: return createSecondary_Scheduler();
			case ModelPackage.SIMPLE_OPERATION: return createSimple_Operation();
			case ModelPackage.SINGULAR_EXTERNAL_EVENT: return createSingular_External_Event();
			case ModelPackage.SOFT_GLOBAL_DEADLINE: return createSoft_Global_Deadline();
			case ModelPackage.SOFT_LOCAL_DEADLINE: return createSoft_Local_Deadline();
			case ModelPackage.SPORADIC_EXTERNAL_EVENT: return createSporadic_External_Event();
			case ModelPackage.SPORADIC_SERVER_POLICY: return createSporadic_Server_Policy();
			case ModelPackage.SRP_PARAMETERS: return createSRP_Parameters();
			case ModelPackage.SRP_RESOURCE: return createSRP_Resource();
			case ModelPackage.SYSTEM_TIMED_ACTIVITY: return createSystem_Timed_Activity();
			case ModelPackage.TICKER_SYSTEM_TIMER: return createTicker_System_Timer();
			case ModelPackage.UNBOUNDED_EXTERNAL_EVENT: return createUnbounded_External_Event();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ModelPackage.AFFIRMATIVE_ASSERTION:
				return createAffirmative_AssertionFromString(eDataType, initialValue);
			case ModelPackage.ASSERTION:
				return createAssertionFromString(eDataType, initialValue);
			case ModelPackage.DELIVERY_POLICY:
				return createDelivery_PolicyFromString(eDataType, initialValue);
			case ModelPackage.DISTRIBUTION_TYPE:
				return createDistribution_TypeFromString(eDataType, initialValue);
			case ModelPackage.NEGATIVE_ASSERTION:
				return createNegative_AssertionFromString(eDataType, initialValue);
			case ModelPackage.OVERHEAD_TYPE:
				return createOverhead_TypeFromString(eDataType, initialValue);
			case ModelPackage.PRIORITY_INHERITANCE_PROTOCOL:
				return createPriority_Inheritance_ProtocolFromString(eDataType, initialValue);
			case ModelPackage.REQUEST_POLICY:
				return createRequest_PolicyFromString(eDataType, initialValue);
			case ModelPackage.TRANSMISSION_TYPE:
				return createTransmission_TypeFromString(eDataType, initialValue);
			case ModelPackage.ABSOLUTE_TIME:
				return createAbsolute_TimeFromString(eDataType, initialValue);
			case ModelPackage.AFFIRMATIVE_ASSERTION_OBJECT:
				return createAffirmative_Assertion_ObjectFromString(eDataType, initialValue);
			case ModelPackage.ANY_PRIORITY:
				return createAny_PriorityFromString(eDataType, initialValue);
			case ModelPackage.ASSERTION_OBJECT:
				return createAssertion_ObjectFromString(eDataType, initialValue);
			case ModelPackage.BIT_COUNT:
				return createBit_CountFromString(eDataType, initialValue);
			case ModelPackage.DATE_TIME:
				return createDate_TimeFromString(eDataType, initialValue);
			case ModelPackage.DELIVERY_POLICY_OBJECT:
				return createDelivery_Policy_ObjectFromString(eDataType, initialValue);
			case ModelPackage.DISTRIBUTION_TYPE_OBJECT:
				return createDistribution_Type_ObjectFromString(eDataType, initialValue);
			case ModelPackage.FLOAT:
				return createFloatFromString(eDataType, initialValue);
			case ModelPackage.IDENTIFIER:
				return createIdentifierFromString(eDataType, initialValue);
			case ModelPackage.IDENTIFIER_REF:
				return createIdentifier_RefFromString(eDataType, initialValue);
			case ModelPackage.IDENTIFIER_REF_LIST:
				return createIdentifier_Ref_ListFromString(eDataType, initialValue);
			case ModelPackage.INTERRUPT_PRIORITY:
				return createInterrupt_PriorityFromString(eDataType, initialValue);
			case ModelPackage.NATURAL:
				return createNaturalFromString(eDataType, initialValue);
			case ModelPackage.NEGATIVE_ASSERTION_OBJECT:
				return createNegative_Assertion_ObjectFromString(eDataType, initialValue);
			case ModelPackage.NORMALIZED_EXECUTION_TIME:
				return createNormalized_Execution_TimeFromString(eDataType, initialValue);
			case ModelPackage.OVERHEAD_TYPE_OBJECT:
				return createOverhead_Type_ObjectFromString(eDataType, initialValue);
			case ModelPackage.PATHNAME:
				return createPathnameFromString(eDataType, initialValue);
			case ModelPackage.PERCENTAGE:
				return createPercentageFromString(eDataType, initialValue);
			case ModelPackage.POSITIVE:
				return createPositiveFromString(eDataType, initialValue);
			case ModelPackage.PREEMPTION_LEVEL:
				return createPreemption_LevelFromString(eDataType, initialValue);
			case ModelPackage.PRIORITY:
				return createPriorityFromString(eDataType, initialValue);
			case ModelPackage.PRIORITY_INHERITANCE_PROTOCOL_OBJECT:
				return createPriority_Inheritance_Protocol_ObjectFromString(eDataType, initialValue);
			case ModelPackage.REQUEST_POLICY_OBJECT:
				return createRequest_Policy_ObjectFromString(eDataType, initialValue);
			case ModelPackage.THROUGHPUT:
				return createThroughputFromString(eDataType, initialValue);
			case ModelPackage.TIME:
				return createTimeFromString(eDataType, initialValue);
			case ModelPackage.TRANSMISSION_TYPE_OBJECT:
				return createTransmission_Type_ObjectFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ModelPackage.AFFIRMATIVE_ASSERTION:
				return convertAffirmative_AssertionToString(eDataType, instanceValue);
			case ModelPackage.ASSERTION:
				return convertAssertionToString(eDataType, instanceValue);
			case ModelPackage.DELIVERY_POLICY:
				return convertDelivery_PolicyToString(eDataType, instanceValue);
			case ModelPackage.DISTRIBUTION_TYPE:
				return convertDistribution_TypeToString(eDataType, instanceValue);
			case ModelPackage.NEGATIVE_ASSERTION:
				return convertNegative_AssertionToString(eDataType, instanceValue);
			case ModelPackage.OVERHEAD_TYPE:
				return convertOverhead_TypeToString(eDataType, instanceValue);
			case ModelPackage.PRIORITY_INHERITANCE_PROTOCOL:
				return convertPriority_Inheritance_ProtocolToString(eDataType, instanceValue);
			case ModelPackage.REQUEST_POLICY:
				return convertRequest_PolicyToString(eDataType, instanceValue);
			case ModelPackage.TRANSMISSION_TYPE:
				return convertTransmission_TypeToString(eDataType, instanceValue);
			case ModelPackage.ABSOLUTE_TIME:
				return convertAbsolute_TimeToString(eDataType, instanceValue);
			case ModelPackage.AFFIRMATIVE_ASSERTION_OBJECT:
				return convertAffirmative_Assertion_ObjectToString(eDataType, instanceValue);
			case ModelPackage.ANY_PRIORITY:
				return convertAny_PriorityToString(eDataType, instanceValue);
			case ModelPackage.ASSERTION_OBJECT:
				return convertAssertion_ObjectToString(eDataType, instanceValue);
			case ModelPackage.BIT_COUNT:
				return convertBit_CountToString(eDataType, instanceValue);
			case ModelPackage.DATE_TIME:
				return convertDate_TimeToString(eDataType, instanceValue);
			case ModelPackage.DELIVERY_POLICY_OBJECT:
				return convertDelivery_Policy_ObjectToString(eDataType, instanceValue);
			case ModelPackage.DISTRIBUTION_TYPE_OBJECT:
				return convertDistribution_Type_ObjectToString(eDataType, instanceValue);
			case ModelPackage.FLOAT:
				return convertFloatToString(eDataType, instanceValue);
			case ModelPackage.IDENTIFIER:
				return convertIdentifierToString(eDataType, instanceValue);
			case ModelPackage.IDENTIFIER_REF:
				return convertIdentifier_RefToString(eDataType, instanceValue);
			case ModelPackage.IDENTIFIER_REF_LIST:
				return convertIdentifier_Ref_ListToString(eDataType, instanceValue);
			case ModelPackage.INTERRUPT_PRIORITY:
				return convertInterrupt_PriorityToString(eDataType, instanceValue);
			case ModelPackage.NATURAL:
				return convertNaturalToString(eDataType, instanceValue);
			case ModelPackage.NEGATIVE_ASSERTION_OBJECT:
				return convertNegative_Assertion_ObjectToString(eDataType, instanceValue);
			case ModelPackage.NORMALIZED_EXECUTION_TIME:
				return convertNormalized_Execution_TimeToString(eDataType, instanceValue);
			case ModelPackage.OVERHEAD_TYPE_OBJECT:
				return convertOverhead_Type_ObjectToString(eDataType, instanceValue);
			case ModelPackage.PATHNAME:
				return convertPathnameToString(eDataType, instanceValue);
			case ModelPackage.PERCENTAGE:
				return convertPercentageToString(eDataType, instanceValue);
			case ModelPackage.POSITIVE:
				return convertPositiveToString(eDataType, instanceValue);
			case ModelPackage.PREEMPTION_LEVEL:
				return convertPreemption_LevelToString(eDataType, instanceValue);
			case ModelPackage.PRIORITY:
				return convertPriorityToString(eDataType, instanceValue);
			case ModelPackage.PRIORITY_INHERITANCE_PROTOCOL_OBJECT:
				return convertPriority_Inheritance_Protocol_ObjectToString(eDataType, instanceValue);
			case ModelPackage.REQUEST_POLICY_OBJECT:
				return convertRequest_Policy_ObjectToString(eDataType, instanceValue);
			case ModelPackage.THROUGHPUT:
				return convertThroughputToString(eDataType, instanceValue);
			case ModelPackage.TIME:
				return convertTimeToString(eDataType, instanceValue);
			case ModelPackage.TRANSMISSION_TYPE_OBJECT:
				return convertTransmission_Type_ObjectToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Activity createActivity() {
		ActivityImpl activity = new ActivityImpl();
		return activity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Alarm_Clock_System_Timer createAlarm_Clock_System_Timer() {
		Alarm_Clock_System_TimerImpl alarm_Clock_System_Timer = new Alarm_Clock_System_TimerImpl();
		return alarm_Clock_System_Timer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Barrier createBarrier() {
		BarrierImpl barrier = new BarrierImpl();
		return barrier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Bursty_External_Event createBursty_External_Event() {
		Bursty_External_EventImpl bursty_External_Event = new Bursty_External_EventImpl();
		return bursty_External_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Character_Packet_Driver createCharacter_Packet_Driver() {
		Character_Packet_DriverImpl character_Packet_Driver = new Character_Packet_DriverImpl();
		return character_Packet_Driver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Composite_Operation createComposite_Operation() {
		Composite_OperationImpl composite_Operation = new Composite_OperationImpl();
		return composite_Operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Composite_Timing_Requirement createComposite_Timing_Requirement() {
		Composite_Timing_RequirementImpl composite_Timing_Requirement = new Composite_Timing_RequirementImpl();
		return composite_Timing_Requirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Concentrator createConcentrator() {
		ConcentratorImpl concentrator = new ConcentratorImpl();
		return concentrator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Delay createDelay() {
		DelayImpl delay = new DelayImpl();
		return delay;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Delivery_Server createDelivery_Server() {
		Delivery_ServerImpl delivery_Server = new Delivery_ServerImpl();
		return delivery_Server;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Document_Root createDocument_Root() {
		Document_RootImpl document_Root = new Document_RootImpl();
		return document_Root;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDF_Policy createEDF_Policy() {
		EDF_PolicyImpl edF_Policy = new EDF_PolicyImpl();
		return edF_Policy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDF_Scheduler createEDF_Scheduler() {
		EDF_SchedulerImpl edF_Scheduler = new EDF_SchedulerImpl();
		return edF_Scheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Enclosing_Operation createEnclosing_Operation() {
		Enclosing_OperationImpl enclosing_Operation = new Enclosing_OperationImpl();
		return enclosing_Operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Fixed_Priority_Policy createFixed_Priority_Policy() {
		Fixed_Priority_PolicyImpl fixed_Priority_Policy = new Fixed_Priority_PolicyImpl();
		return fixed_Priority_Policy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Fixed_Priority_Scheduler createFixed_Priority_Scheduler() {
		Fixed_Priority_SchedulerImpl fixed_Priority_Scheduler = new Fixed_Priority_SchedulerImpl();
		return fixed_Priority_Scheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FP_Packet_Based_Scheduler createFP_Packet_Based_Scheduler() {
		FP_Packet_Based_SchedulerImpl fP_Packet_Based_Scheduler = new FP_Packet_Based_SchedulerImpl();
		return fP_Packet_Based_Scheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Global_Max_Miss_Ratio createGlobal_Max_Miss_Ratio() {
		Global_Max_Miss_RatioImpl global_Max_Miss_Ratio = new Global_Max_Miss_RatioImpl();
		return global_Max_Miss_Ratio;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Hard_Global_Deadline createHard_Global_Deadline() {
		Hard_Global_DeadlineImpl hard_Global_Deadline = new Hard_Global_DeadlineImpl();
		return hard_Global_Deadline;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Hard_Local_Deadline createHard_Local_Deadline() {
		Hard_Local_DeadlineImpl hard_Local_Deadline = new Hard_Local_DeadlineImpl();
		return hard_Local_Deadline;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Immediate_Ceiling_Resource createImmediate_Ceiling_Resource() {
		Immediate_Ceiling_ResourceImpl immediate_Ceiling_Resource = new Immediate_Ceiling_ResourceImpl();
		return immediate_Ceiling_Resource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interrupt_FP_Policy createInterrupt_FP_Policy() {
		Interrupt_FP_PolicyImpl interrupt_FP_Policy = new Interrupt_FP_PolicyImpl();
		return interrupt_FP_Policy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List_of_Drivers createList_of_Drivers() {
		List_of_DriversImpl list_of_Drivers = new List_of_DriversImpl();
		return list_of_Drivers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Local_Max_Miss_Ratio createLocal_Max_Miss_Ratio() {
		Local_Max_Miss_RatioImpl local_Max_Miss_Ratio = new Local_Max_Miss_RatioImpl();
		return local_Max_Miss_Ratio;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAST_MODEL createMAST_MODEL() {
		MAST_MODELImpl masT_MODEL = new MAST_MODELImpl();
		return masT_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Max_Output_Jitter_Req createMax_Output_Jitter_Req() {
		Max_Output_Jitter_ReqImpl max_Output_Jitter_Req = new Max_Output_Jitter_ReqImpl();
		return max_Output_Jitter_Req;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Message_Transmission createMessage_Transmission() {
		Message_TransmissionImpl message_Transmission = new Message_TransmissionImpl();
		return message_Transmission;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Multicast createMulticast() {
		MulticastImpl multicast = new MulticastImpl();
		return multicast;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Non_Preemptible_FP_Policy createNon_Preemptible_FP_Policy() {
		Non_Preemptible_FP_PolicyImpl non_Preemptible_FP_Policy = new Non_Preemptible_FP_PolicyImpl();
		return non_Preemptible_FP_Policy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Offset createOffset() {
		OffsetImpl offset = new OffsetImpl();
		return offset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Overridden_Fixed_Priority createOverridden_Fixed_Priority() {
		Overridden_Fixed_PriorityImpl overridden_Fixed_Priority = new Overridden_Fixed_PriorityImpl();
		return overridden_Fixed_Priority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Overridden_Permanent_FP createOverridden_Permanent_FP() {
		Overridden_Permanent_FPImpl overridden_Permanent_FP = new Overridden_Permanent_FPImpl();
		return overridden_Permanent_FP;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Packet_Based_Network createPacket_Based_Network() {
		Packet_Based_NetworkImpl packet_Based_Network = new Packet_Based_NetworkImpl();
		return packet_Based_Network;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Packet_Driver createPacket_Driver() {
		Packet_DriverImpl packet_Driver = new Packet_DriverImpl();
		return packet_Driver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Periodic_External_Event createPeriodic_External_Event() {
		Periodic_External_EventImpl periodic_External_Event = new Periodic_External_EventImpl();
		return periodic_External_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Polling_Policy createPolling_Policy() {
		Polling_PolicyImpl polling_Policy = new Polling_PolicyImpl();
		return polling_Policy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Primary_Scheduler createPrimary_Scheduler() {
		Primary_SchedulerImpl primary_Scheduler = new Primary_SchedulerImpl();
		return primary_Scheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Priority_Inheritance_Resource createPriority_Inheritance_Resource() {
		Priority_Inheritance_ResourceImpl priority_Inheritance_Resource = new Priority_Inheritance_ResourceImpl();
		return priority_Inheritance_Resource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Query_Server createQuery_Server() {
		Query_ServerImpl query_Server = new Query_ServerImpl();
		return query_Server;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Rate_Divisor createRate_Divisor() {
		Rate_DivisorImpl rate_Divisor = new Rate_DivisorImpl();
		return rate_Divisor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Regular_Event createRegular_Event() {
		Regular_EventImpl regular_Event = new Regular_EventImpl();
		return regular_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Regular_Processor createRegular_Processor() {
		Regular_ProcessorImpl regular_Processor = new Regular_ProcessorImpl();
		return regular_Processor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Regular_Scheduling_Server createRegular_Scheduling_Server() {
		Regular_Scheduling_ServerImpl regular_Scheduling_Server = new Regular_Scheduling_ServerImpl();
		return regular_Scheduling_Server;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Regular_Transaction createRegular_Transaction() {
		Regular_TransactionImpl regular_Transaction = new Regular_TransactionImpl();
		return regular_Transaction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RTEP_PacketDriver createRTEP_PacketDriver() {
		RTEP_PacketDriverImpl rteP_PacketDriver = new RTEP_PacketDriverImpl();
		return rteP_PacketDriver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Secondary_Scheduler createSecondary_Scheduler() {
		Secondary_SchedulerImpl secondary_Scheduler = new Secondary_SchedulerImpl();
		return secondary_Scheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Simple_Operation createSimple_Operation() {
		Simple_OperationImpl simple_Operation = new Simple_OperationImpl();
		return simple_Operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Singular_External_Event createSingular_External_Event() {
		Singular_External_EventImpl singular_External_Event = new Singular_External_EventImpl();
		return singular_External_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Soft_Global_Deadline createSoft_Global_Deadline() {
		Soft_Global_DeadlineImpl soft_Global_Deadline = new Soft_Global_DeadlineImpl();
		return soft_Global_Deadline;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Soft_Local_Deadline createSoft_Local_Deadline() {
		Soft_Local_DeadlineImpl soft_Local_Deadline = new Soft_Local_DeadlineImpl();
		return soft_Local_Deadline;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Sporadic_External_Event createSporadic_External_Event() {
		Sporadic_External_EventImpl sporadic_External_Event = new Sporadic_External_EventImpl();
		return sporadic_External_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Sporadic_Server_Policy createSporadic_Server_Policy() {
		Sporadic_Server_PolicyImpl sporadic_Server_Policy = new Sporadic_Server_PolicyImpl();
		return sporadic_Server_Policy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SRP_Parameters createSRP_Parameters() {
		SRP_ParametersImpl srP_Parameters = new SRP_ParametersImpl();
		return srP_Parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SRP_Resource createSRP_Resource() {
		SRP_ResourceImpl srP_Resource = new SRP_ResourceImpl();
		return srP_Resource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public System_Timed_Activity createSystem_Timed_Activity() {
		System_Timed_ActivityImpl system_Timed_Activity = new System_Timed_ActivityImpl();
		return system_Timed_Activity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ticker_System_Timer createTicker_System_Timer() {
		Ticker_System_TimerImpl ticker_System_Timer = new Ticker_System_TimerImpl();
		return ticker_System_Timer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Unbounded_External_Event createUnbounded_External_Event() {
		Unbounded_External_EventImpl unbounded_External_Event = new Unbounded_External_EventImpl();
		return unbounded_External_Event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Affirmative_Assertion createAffirmative_AssertionFromString(EDataType eDataType, String initialValue) {
		Affirmative_Assertion result = Affirmative_Assertion.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAffirmative_AssertionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Assertion createAssertionFromString(EDataType eDataType, String initialValue) {
		Assertion result = Assertion.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAssertionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Delivery_Policy createDelivery_PolicyFromString(EDataType eDataType, String initialValue) {
		Delivery_Policy result = Delivery_Policy.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDelivery_PolicyToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Distribution_Type createDistribution_TypeFromString(EDataType eDataType, String initialValue) {
		Distribution_Type result = Distribution_Type.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDistribution_TypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Negative_Assertion createNegative_AssertionFromString(EDataType eDataType, String initialValue) {
		Negative_Assertion result = Negative_Assertion.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertNegative_AssertionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Overhead_Type createOverhead_TypeFromString(EDataType eDataType, String initialValue) {
		Overhead_Type result = Overhead_Type.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOverhead_TypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Priority_Inheritance_Protocol createPriority_Inheritance_ProtocolFromString(EDataType eDataType, String initialValue) {
		Priority_Inheritance_Protocol result = Priority_Inheritance_Protocol.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPriority_Inheritance_ProtocolToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Request_Policy createRequest_PolicyFromString(EDataType eDataType, String initialValue) {
		Request_Policy result = Request_Policy.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRequest_PolicyToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transmission_Type createTransmission_TypeFromString(EDataType eDataType, String initialValue) {
		Transmission_Type result = Transmission_Type.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTransmission_TypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double createAbsolute_TimeFromString(EDataType eDataType, String initialValue) {
		return (Double)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.DOUBLE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAbsolute_TimeToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.DOUBLE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Affirmative_Assertion createAffirmative_Assertion_ObjectFromString(EDataType eDataType, String initialValue) {
		return createAffirmative_AssertionFromString(ModelPackage.Literals.AFFIRMATIVE_ASSERTION, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAffirmative_Assertion_ObjectToString(EDataType eDataType, Object instanceValue) {
		return convertAffirmative_AssertionToString(ModelPackage.Literals.AFFIRMATIVE_ASSERTION, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer createAny_PriorityFromString(EDataType eDataType, String initialValue) {
		return (Integer)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.INT, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAny_PriorityToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.INT, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Assertion createAssertion_ObjectFromString(EDataType eDataType, String initialValue) {
		return createAssertionFromString(ModelPackage.Literals.ASSERTION, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAssertion_ObjectToString(EDataType eDataType, Object instanceValue) {
		return convertAssertionToString(ModelPackage.Literals.ASSERTION, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double createBit_CountFromString(EDataType eDataType, String initialValue) {
		return (Double)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.DOUBLE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBit_CountToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.DOUBLE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createDate_TimeFromString(EDataType eDataType, String initialValue) {
		return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.STRING, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDate_TimeToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.STRING, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Delivery_Policy createDelivery_Policy_ObjectFromString(EDataType eDataType, String initialValue) {
		return createDelivery_PolicyFromString(ModelPackage.Literals.DELIVERY_POLICY, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDelivery_Policy_ObjectToString(EDataType eDataType, Object instanceValue) {
		return convertDelivery_PolicyToString(ModelPackage.Literals.DELIVERY_POLICY, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Distribution_Type createDistribution_Type_ObjectFromString(EDataType eDataType, String initialValue) {
		return createDistribution_TypeFromString(ModelPackage.Literals.DISTRIBUTION_TYPE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDistribution_Type_ObjectToString(EDataType eDataType, Object instanceValue) {
		return convertDistribution_TypeToString(ModelPackage.Literals.DISTRIBUTION_TYPE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double createFloatFromString(EDataType eDataType, String initialValue) {
		return (Double)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.DOUBLE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFloatToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.DOUBLE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createIdentifierFromString(EDataType eDataType, String initialValue) {
		return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NC_NAME, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIdentifierToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NC_NAME, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createIdentifier_RefFromString(EDataType eDataType, String initialValue) {
		return createIdentifierFromString(ModelPackage.Literals.IDENTIFIER, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIdentifier_RefToString(EDataType eDataType, Object instanceValue) {
		return convertIdentifierToString(ModelPackage.Literals.IDENTIFIER, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<String> createIdentifier_Ref_ListFromString(EDataType eDataType, String initialValue) {
		return (List)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NMTOKENS, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIdentifier_Ref_ListToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NMTOKENS, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer createInterrupt_PriorityFromString(EDataType eDataType, String initialValue) {
		return createAny_PriorityFromString(ModelPackage.Literals.ANY_PRIORITY, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertInterrupt_PriorityToString(EDataType eDataType, Object instanceValue) {
		return convertAny_PriorityToString(ModelPackage.Literals.ANY_PRIORITY, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer createNaturalFromString(EDataType eDataType, String initialValue) {
		return (Integer)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.INT, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertNaturalToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.INT, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Negative_Assertion createNegative_Assertion_ObjectFromString(EDataType eDataType, String initialValue) {
		return createNegative_AssertionFromString(ModelPackage.Literals.NEGATIVE_ASSERTION, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertNegative_Assertion_ObjectToString(EDataType eDataType, Object instanceValue) {
		return convertNegative_AssertionToString(ModelPackage.Literals.NEGATIVE_ASSERTION, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double createNormalized_Execution_TimeFromString(EDataType eDataType, String initialValue) {
		return (Double)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.DOUBLE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertNormalized_Execution_TimeToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.DOUBLE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Overhead_Type createOverhead_Type_ObjectFromString(EDataType eDataType, String initialValue) {
		return createOverhead_TypeFromString(ModelPackage.Literals.OVERHEAD_TYPE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOverhead_Type_ObjectToString(EDataType eDataType, Object instanceValue) {
		return convertOverhead_TypeToString(ModelPackage.Literals.OVERHEAD_TYPE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createPathnameFromString(EDataType eDataType, String initialValue) {
		return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.STRING, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPathnameToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.STRING, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double createPercentageFromString(EDataType eDataType, String initialValue) {
		return (Double)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.DOUBLE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPercentageToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.DOUBLE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer createPositiveFromString(EDataType eDataType, String initialValue) {
		return (Integer)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.INT, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPositiveToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.INT, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer createPreemption_LevelFromString(EDataType eDataType, String initialValue) {
		return (Integer)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.INT, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPreemption_LevelToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.INT, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer createPriorityFromString(EDataType eDataType, String initialValue) {
		return createAny_PriorityFromString(ModelPackage.Literals.ANY_PRIORITY, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPriorityToString(EDataType eDataType, Object instanceValue) {
		return convertAny_PriorityToString(ModelPackage.Literals.ANY_PRIORITY, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Priority_Inheritance_Protocol createPriority_Inheritance_Protocol_ObjectFromString(EDataType eDataType, String initialValue) {
		return createPriority_Inheritance_ProtocolFromString(ModelPackage.Literals.PRIORITY_INHERITANCE_PROTOCOL, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPriority_Inheritance_Protocol_ObjectToString(EDataType eDataType, Object instanceValue) {
		return convertPriority_Inheritance_ProtocolToString(ModelPackage.Literals.PRIORITY_INHERITANCE_PROTOCOL, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Request_Policy createRequest_Policy_ObjectFromString(EDataType eDataType, String initialValue) {
		return createRequest_PolicyFromString(ModelPackage.Literals.REQUEST_POLICY, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertRequest_Policy_ObjectToString(EDataType eDataType, Object instanceValue) {
		return convertRequest_PolicyToString(ModelPackage.Literals.REQUEST_POLICY, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double createThroughputFromString(EDataType eDataType, String initialValue) {
		return (Double)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.DOUBLE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertThroughputToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.DOUBLE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double createTimeFromString(EDataType eDataType, String initialValue) {
		return (Double)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.DOUBLE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTimeToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.DOUBLE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transmission_Type createTransmission_Type_ObjectFromString(EDataType eDataType, String initialValue) {
		return createTransmission_TypeFromString(ModelPackage.Literals.TRANSMISSION_TYPE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTransmission_Type_ObjectToString(EDataType eDataType, Object instanceValue) {
		return convertTransmission_TypeToString(ModelPackage.Literals.TRANSMISSION_TYPE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelPackage getModelPackage() {
		return (ModelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ModelPackage getPackage() {
		return ModelPackage.eINSTANCE;
	}

} //ModelFactoryImpl
