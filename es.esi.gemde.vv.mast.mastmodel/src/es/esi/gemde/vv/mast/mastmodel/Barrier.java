/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Barrier</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Barrier#getInput_Events_List <em>Input Events List</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Barrier#getOutput_Event <em>Output Event</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getBarrier()
 * @model extendedMetaData="name='Barrier' kind='empty'"
 * @generated
 */
public interface Barrier extends EObject {
	/**
	 * Returns the value of the '<em><b>Input Events List</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Events List</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Events List</em>' attribute.
	 * @see #setInput_Events_List(List)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getBarrier_Input_Events_List()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref_List" required="true" many="false"
	 *        extendedMetaData="kind='attribute' name='Input_Events_List'"
	 * @generated
	 */
	List<String> getInput_Events_List();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Barrier#getInput_Events_List <em>Input Events List</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Events List</em>' attribute.
	 * @see #getInput_Events_List()
	 * @generated
	 */
	void setInput_Events_List(List<String> value);

	/**
	 * Returns the value of the '<em><b>Output Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Event</em>' attribute.
	 * @see #setOutput_Event(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getBarrier_Output_Event()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier_Ref" required="true"
	 *        extendedMetaData="kind='attribute' name='Output_Event'"
	 * @generated
	 */
	String getOutput_Event();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Barrier#getOutput_Event <em>Output Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Event</em>' attribute.
	 * @see #getOutput_Event()
	 * @generated
	 */
	void setOutput_Event(String value);

} // Barrier
