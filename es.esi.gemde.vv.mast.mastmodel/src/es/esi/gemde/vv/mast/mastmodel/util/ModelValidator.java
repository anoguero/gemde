/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.util;

import es.esi.gemde.vv.mast.mastmodel.*;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import org.eclipse.emf.ecore.xml.type.util.XMLTypeUtil;
import org.eclipse.emf.ecore.xml.type.util.XMLTypeValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage
 * @generated
 */
public class ModelValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final ModelValidator INSTANCE = new ModelValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "es.esi.gemde.vv.mast.mastmodel";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * The cached base package validator.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XMLTypeValidator xmlTypeValidator;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModelValidator() {
		super();
		xmlTypeValidator = XMLTypeValidator.INSTANCE;
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return ModelPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case ModelPackage.ACTIVITY:
				return validateActivity((Activity)value, diagnostics, context);
			case ModelPackage.ALARM_CLOCK_SYSTEM_TIMER:
				return validateAlarm_Clock_System_Timer((Alarm_Clock_System_Timer)value, diagnostics, context);
			case ModelPackage.BARRIER:
				return validateBarrier((Barrier)value, diagnostics, context);
			case ModelPackage.BURSTY_EXTERNAL_EVENT:
				return validateBursty_External_Event((Bursty_External_Event)value, diagnostics, context);
			case ModelPackage.CHARACTER_PACKET_DRIVER:
				return validateCharacter_Packet_Driver((Character_Packet_Driver)value, diagnostics, context);
			case ModelPackage.COMPOSITE_OPERATION:
				return validateComposite_Operation((Composite_Operation)value, diagnostics, context);
			case ModelPackage.COMPOSITE_TIMING_REQUIREMENT:
				return validateComposite_Timing_Requirement((Composite_Timing_Requirement)value, diagnostics, context);
			case ModelPackage.CONCENTRATOR:
				return validateConcentrator((Concentrator)value, diagnostics, context);
			case ModelPackage.DELAY:
				return validateDelay((Delay)value, diagnostics, context);
			case ModelPackage.DELIVERY_SERVER:
				return validateDelivery_Server((Delivery_Server)value, diagnostics, context);
			case ModelPackage.DOCUMENT_ROOT:
				return validateDocument_Root((Document_Root)value, diagnostics, context);
			case ModelPackage.EDF_POLICY:
				return validateEDF_Policy((EDF_Policy)value, diagnostics, context);
			case ModelPackage.EDF_SCHEDULER:
				return validateEDF_Scheduler((EDF_Scheduler)value, diagnostics, context);
			case ModelPackage.ENCLOSING_OPERATION:
				return validateEnclosing_Operation((Enclosing_Operation)value, diagnostics, context);
			case ModelPackage.FIXED_PRIORITY_POLICY:
				return validateFixed_Priority_Policy((Fixed_Priority_Policy)value, diagnostics, context);
			case ModelPackage.FIXED_PRIORITY_SCHEDULER:
				return validateFixed_Priority_Scheduler((Fixed_Priority_Scheduler)value, diagnostics, context);
			case ModelPackage.FP_PACKET_BASED_SCHEDULER:
				return validateFP_Packet_Based_Scheduler((FP_Packet_Based_Scheduler)value, diagnostics, context);
			case ModelPackage.GLOBAL_MAX_MISS_RATIO:
				return validateGlobal_Max_Miss_Ratio((Global_Max_Miss_Ratio)value, diagnostics, context);
			case ModelPackage.HARD_GLOBAL_DEADLINE:
				return validateHard_Global_Deadline((Hard_Global_Deadline)value, diagnostics, context);
			case ModelPackage.HARD_LOCAL_DEADLINE:
				return validateHard_Local_Deadline((Hard_Local_Deadline)value, diagnostics, context);
			case ModelPackage.IMMEDIATE_CEILING_RESOURCE:
				return validateImmediate_Ceiling_Resource((Immediate_Ceiling_Resource)value, diagnostics, context);
			case ModelPackage.INTERRUPT_FP_POLICY:
				return validateInterrupt_FP_Policy((Interrupt_FP_Policy)value, diagnostics, context);
			case ModelPackage.LIST_OF_DRIVERS:
				return validateList_of_Drivers((List_of_Drivers)value, diagnostics, context);
			case ModelPackage.LOCAL_MAX_MISS_RATIO:
				return validateLocal_Max_Miss_Ratio((Local_Max_Miss_Ratio)value, diagnostics, context);
			case ModelPackage.MAST_MODEL:
				return validateMAST_MODEL((MAST_MODEL)value, diagnostics, context);
			case ModelPackage.MAX_OUTPUT_JITTER_REQ:
				return validateMax_Output_Jitter_Req((Max_Output_Jitter_Req)value, diagnostics, context);
			case ModelPackage.MESSAGE_TRANSMISSION:
				return validateMessage_Transmission((Message_Transmission)value, diagnostics, context);
			case ModelPackage.MULTICAST:
				return validateMulticast((Multicast)value, diagnostics, context);
			case ModelPackage.NON_PREEMPTIBLE_FP_POLICY:
				return validateNon_Preemptible_FP_Policy((Non_Preemptible_FP_Policy)value, diagnostics, context);
			case ModelPackage.OFFSET:
				return validateOffset((Offset)value, diagnostics, context);
			case ModelPackage.OVERRIDDEN_FIXED_PRIORITY:
				return validateOverridden_Fixed_Priority((Overridden_Fixed_Priority)value, diagnostics, context);
			case ModelPackage.OVERRIDDEN_PERMANENT_FP:
				return validateOverridden_Permanent_FP((Overridden_Permanent_FP)value, diagnostics, context);
			case ModelPackage.PACKET_BASED_NETWORK:
				return validatePacket_Based_Network((Packet_Based_Network)value, diagnostics, context);
			case ModelPackage.PACKET_DRIVER:
				return validatePacket_Driver((Packet_Driver)value, diagnostics, context);
			case ModelPackage.PERIODIC_EXTERNAL_EVENT:
				return validatePeriodic_External_Event((Periodic_External_Event)value, diagnostics, context);
			case ModelPackage.POLLING_POLICY:
				return validatePolling_Policy((Polling_Policy)value, diagnostics, context);
			case ModelPackage.PRIMARY_SCHEDULER:
				return validatePrimary_Scheduler((Primary_Scheduler)value, diagnostics, context);
			case ModelPackage.PRIORITY_INHERITANCE_RESOURCE:
				return validatePriority_Inheritance_Resource((Priority_Inheritance_Resource)value, diagnostics, context);
			case ModelPackage.QUERY_SERVER:
				return validateQuery_Server((Query_Server)value, diagnostics, context);
			case ModelPackage.RATE_DIVISOR:
				return validateRate_Divisor((Rate_Divisor)value, diagnostics, context);
			case ModelPackage.REGULAR_EVENT:
				return validateRegular_Event((Regular_Event)value, diagnostics, context);
			case ModelPackage.REGULAR_PROCESSOR:
				return validateRegular_Processor((Regular_Processor)value, diagnostics, context);
			case ModelPackage.REGULAR_SCHEDULING_SERVER:
				return validateRegular_Scheduling_Server((Regular_Scheduling_Server)value, diagnostics, context);
			case ModelPackage.REGULAR_TRANSACTION:
				return validateRegular_Transaction((Regular_Transaction)value, diagnostics, context);
			case ModelPackage.RTEP_PACKET_DRIVER:
				return validateRTEP_PacketDriver((RTEP_PacketDriver)value, diagnostics, context);
			case ModelPackage.SECONDARY_SCHEDULER:
				return validateSecondary_Scheduler((Secondary_Scheduler)value, diagnostics, context);
			case ModelPackage.SIMPLE_OPERATION:
				return validateSimple_Operation((Simple_Operation)value, diagnostics, context);
			case ModelPackage.SINGULAR_EXTERNAL_EVENT:
				return validateSingular_External_Event((Singular_External_Event)value, diagnostics, context);
			case ModelPackage.SOFT_GLOBAL_DEADLINE:
				return validateSoft_Global_Deadline((Soft_Global_Deadline)value, diagnostics, context);
			case ModelPackage.SOFT_LOCAL_DEADLINE:
				return validateSoft_Local_Deadline((Soft_Local_Deadline)value, diagnostics, context);
			case ModelPackage.SPORADIC_EXTERNAL_EVENT:
				return validateSporadic_External_Event((Sporadic_External_Event)value, diagnostics, context);
			case ModelPackage.SPORADIC_SERVER_POLICY:
				return validateSporadic_Server_Policy((Sporadic_Server_Policy)value, diagnostics, context);
			case ModelPackage.SRP_PARAMETERS:
				return validateSRP_Parameters((SRP_Parameters)value, diagnostics, context);
			case ModelPackage.SRP_RESOURCE:
				return validateSRP_Resource((SRP_Resource)value, diagnostics, context);
			case ModelPackage.SYSTEM_TIMED_ACTIVITY:
				return validateSystem_Timed_Activity((System_Timed_Activity)value, diagnostics, context);
			case ModelPackage.TICKER_SYSTEM_TIMER:
				return validateTicker_System_Timer((Ticker_System_Timer)value, diagnostics, context);
			case ModelPackage.UNBOUNDED_EXTERNAL_EVENT:
				return validateUnbounded_External_Event((Unbounded_External_Event)value, diagnostics, context);
			case ModelPackage.AFFIRMATIVE_ASSERTION:
				return validateAffirmative_Assertion((Affirmative_Assertion)value, diagnostics, context);
			case ModelPackage.ASSERTION:
				return validateAssertion((Assertion)value, diagnostics, context);
			case ModelPackage.DELIVERY_POLICY:
				return validateDelivery_Policy((Delivery_Policy)value, diagnostics, context);
			case ModelPackage.DISTRIBUTION_TYPE:
				return validateDistribution_Type((Distribution_Type)value, diagnostics, context);
			case ModelPackage.NEGATIVE_ASSERTION:
				return validateNegative_Assertion((Negative_Assertion)value, diagnostics, context);
			case ModelPackage.OVERHEAD_TYPE:
				return validateOverhead_Type((Overhead_Type)value, diagnostics, context);
			case ModelPackage.PRIORITY_INHERITANCE_PROTOCOL:
				return validatePriority_Inheritance_Protocol((Priority_Inheritance_Protocol)value, diagnostics, context);
			case ModelPackage.REQUEST_POLICY:
				return validateRequest_Policy((Request_Policy)value, diagnostics, context);
			case ModelPackage.TRANSMISSION_TYPE:
				return validateTransmission_Type((Transmission_Type)value, diagnostics, context);
			case ModelPackage.ABSOLUTE_TIME:
				return validateAbsolute_Time((Double)value, diagnostics, context);
			case ModelPackage.AFFIRMATIVE_ASSERTION_OBJECT:
				return validateAffirmative_Assertion_Object((Affirmative_Assertion)value, diagnostics, context);
			case ModelPackage.ANY_PRIORITY:
				return validateAny_Priority((Integer)value, diagnostics, context);
			case ModelPackage.ASSERTION_OBJECT:
				return validateAssertion_Object((Assertion)value, diagnostics, context);
			case ModelPackage.BIT_COUNT:
				return validateBit_Count((Double)value, diagnostics, context);
			case ModelPackage.DATE_TIME:
				return validateDate_Time((String)value, diagnostics, context);
			case ModelPackage.DELIVERY_POLICY_OBJECT:
				return validateDelivery_Policy_Object((Delivery_Policy)value, diagnostics, context);
			case ModelPackage.DISTRIBUTION_TYPE_OBJECT:
				return validateDistribution_Type_Object((Distribution_Type)value, diagnostics, context);
			case ModelPackage.FLOAT:
				return validateFloat((Double)value, diagnostics, context);
			case ModelPackage.IDENTIFIER:
				return validateIdentifier((String)value, diagnostics, context);
			case ModelPackage.IDENTIFIER_REF:
				return validateIdentifier_Ref((String)value, diagnostics, context);
			case ModelPackage.IDENTIFIER_REF_LIST:
				return validateIdentifier_Ref_List((List<?>)value, diagnostics, context);
			case ModelPackage.INTERRUPT_PRIORITY:
				return validateInterrupt_Priority((Integer)value, diagnostics, context);
			case ModelPackage.NATURAL:
				return validateNatural((Integer)value, diagnostics, context);
			case ModelPackage.NEGATIVE_ASSERTION_OBJECT:
				return validateNegative_Assertion_Object((Negative_Assertion)value, diagnostics, context);
			case ModelPackage.NORMALIZED_EXECUTION_TIME:
				return validateNormalized_Execution_Time((Double)value, diagnostics, context);
			case ModelPackage.OVERHEAD_TYPE_OBJECT:
				return validateOverhead_Type_Object((Overhead_Type)value, diagnostics, context);
			case ModelPackage.PATHNAME:
				return validatePathname((String)value, diagnostics, context);
			case ModelPackage.PERCENTAGE:
				return validatePercentage((Double)value, diagnostics, context);
			case ModelPackage.POSITIVE:
				return validatePositive((Integer)value, diagnostics, context);
			case ModelPackage.PREEMPTION_LEVEL:
				return validatePreemption_Level((Integer)value, diagnostics, context);
			case ModelPackage.PRIORITY:
				return validatePriority((Integer)value, diagnostics, context);
			case ModelPackage.PRIORITY_INHERITANCE_PROTOCOL_OBJECT:
				return validatePriority_Inheritance_Protocol_Object((Priority_Inheritance_Protocol)value, diagnostics, context);
			case ModelPackage.REQUEST_POLICY_OBJECT:
				return validateRequest_Policy_Object((Request_Policy)value, diagnostics, context);
			case ModelPackage.THROUGHPUT:
				return validateThroughput((Double)value, diagnostics, context);
			case ModelPackage.TIME:
				return validateTime((Double)value, diagnostics, context);
			case ModelPackage.TRANSMISSION_TYPE_OBJECT:
				return validateTransmission_Type_Object((Transmission_Type)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateActivity(Activity activity, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(activity, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAlarm_Clock_System_Timer(Alarm_Clock_System_Timer alarm_Clock_System_Timer, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(alarm_Clock_System_Timer, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBarrier(Barrier barrier, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(barrier, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBursty_External_Event(Bursty_External_Event bursty_External_Event, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(bursty_External_Event, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCharacter_Packet_Driver(Character_Packet_Driver character_Packet_Driver, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(character_Packet_Driver, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComposite_Operation(Composite_Operation composite_Operation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(composite_Operation, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComposite_Timing_Requirement(Composite_Timing_Requirement composite_Timing_Requirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(composite_Timing_Requirement, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConcentrator(Concentrator concentrator, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(concentrator, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDelay(Delay delay, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(delay, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDelivery_Server(Delivery_Server delivery_Server, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(delivery_Server, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDocument_Root(Document_Root document_Root, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(document_Root, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEDF_Policy(EDF_Policy edF_Policy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(edF_Policy, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEDF_Scheduler(EDF_Scheduler edF_Scheduler, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(edF_Scheduler, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEnclosing_Operation(Enclosing_Operation enclosing_Operation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(enclosing_Operation, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFixed_Priority_Policy(Fixed_Priority_Policy fixed_Priority_Policy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(fixed_Priority_Policy, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFixed_Priority_Scheduler(Fixed_Priority_Scheduler fixed_Priority_Scheduler, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(fixed_Priority_Scheduler, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFP_Packet_Based_Scheduler(FP_Packet_Based_Scheduler fP_Packet_Based_Scheduler, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(fP_Packet_Based_Scheduler, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGlobal_Max_Miss_Ratio(Global_Max_Miss_Ratio global_Max_Miss_Ratio, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(global_Max_Miss_Ratio, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHard_Global_Deadline(Hard_Global_Deadline hard_Global_Deadline, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(hard_Global_Deadline, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHard_Local_Deadline(Hard_Local_Deadline hard_Local_Deadline, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(hard_Local_Deadline, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateImmediate_Ceiling_Resource(Immediate_Ceiling_Resource immediate_Ceiling_Resource, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(immediate_Ceiling_Resource, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateInterrupt_FP_Policy(Interrupt_FP_Policy interrupt_FP_Policy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(interrupt_FP_Policy, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateList_of_Drivers(List_of_Drivers list_of_Drivers, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(list_of_Drivers, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLocal_Max_Miss_Ratio(Local_Max_Miss_Ratio local_Max_Miss_Ratio, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(local_Max_Miss_Ratio, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMAST_MODEL(MAST_MODEL masT_MODEL, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(masT_MODEL, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMax_Output_Jitter_Req(Max_Output_Jitter_Req max_Output_Jitter_Req, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(max_Output_Jitter_Req, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMessage_Transmission(Message_Transmission message_Transmission, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(message_Transmission, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMulticast(Multicast multicast, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(multicast, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNon_Preemptible_FP_Policy(Non_Preemptible_FP_Policy non_Preemptible_FP_Policy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(non_Preemptible_FP_Policy, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOffset(Offset offset, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(offset, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOverridden_Fixed_Priority(Overridden_Fixed_Priority overridden_Fixed_Priority, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(overridden_Fixed_Priority, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOverridden_Permanent_FP(Overridden_Permanent_FP overridden_Permanent_FP, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(overridden_Permanent_FP, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePacket_Based_Network(Packet_Based_Network packet_Based_Network, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(packet_Based_Network, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePacket_Driver(Packet_Driver packet_Driver, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(packet_Driver, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePeriodic_External_Event(Periodic_External_Event periodic_External_Event, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(periodic_External_Event, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePolling_Policy(Polling_Policy polling_Policy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(polling_Policy, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePrimary_Scheduler(Primary_Scheduler primary_Scheduler, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(primary_Scheduler, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePriority_Inheritance_Resource(Priority_Inheritance_Resource priority_Inheritance_Resource, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(priority_Inheritance_Resource, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateQuery_Server(Query_Server query_Server, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(query_Server, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRate_Divisor(Rate_Divisor rate_Divisor, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(rate_Divisor, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRegular_Event(Regular_Event regular_Event, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(regular_Event, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRegular_Processor(Regular_Processor regular_Processor, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(regular_Processor, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRegular_Scheduling_Server(Regular_Scheduling_Server regular_Scheduling_Server, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(regular_Scheduling_Server, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRegular_Transaction(Regular_Transaction regular_Transaction, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(regular_Transaction, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRTEP_PacketDriver(RTEP_PacketDriver rteP_PacketDriver, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(rteP_PacketDriver, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSecondary_Scheduler(Secondary_Scheduler secondary_Scheduler, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(secondary_Scheduler, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSimple_Operation(Simple_Operation simple_Operation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(simple_Operation, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSingular_External_Event(Singular_External_Event singular_External_Event, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(singular_External_Event, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSoft_Global_Deadline(Soft_Global_Deadline soft_Global_Deadline, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(soft_Global_Deadline, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSoft_Local_Deadline(Soft_Local_Deadline soft_Local_Deadline, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(soft_Local_Deadline, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSporadic_External_Event(Sporadic_External_Event sporadic_External_Event, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(sporadic_External_Event, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSporadic_Server_Policy(Sporadic_Server_Policy sporadic_Server_Policy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(sporadic_Server_Policy, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSRP_Parameters(SRP_Parameters srP_Parameters, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(srP_Parameters, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSRP_Resource(SRP_Resource srP_Resource, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(srP_Resource, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSystem_Timed_Activity(System_Timed_Activity system_Timed_Activity, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(system_Timed_Activity, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTicker_System_Timer(Ticker_System_Timer ticker_System_Timer, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(ticker_System_Timer, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUnbounded_External_Event(Unbounded_External_Event unbounded_External_Event, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(unbounded_External_Event, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAffirmative_Assertion(Affirmative_Assertion affirmative_Assertion, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssertion(Assertion assertion, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDelivery_Policy(Delivery_Policy delivery_Policy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDistribution_Type(Distribution_Type distribution_Type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNegative_Assertion(Negative_Assertion negative_Assertion, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOverhead_Type(Overhead_Type overhead_Type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePriority_Inheritance_Protocol(Priority_Inheritance_Protocol priority_Inheritance_Protocol, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequest_Policy(Request_Policy request_Policy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransmission_Type(Transmission_Type transmission_Type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAbsolute_Time(double absolute_Time, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAffirmative_Assertion_Object(Affirmative_Assertion affirmative_Assertion_Object, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAny_Priority(int any_Priority, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssertion_Object(Assertion assertion_Object, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBit_Count(double bit_Count, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDate_Time(String date_Time, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDelivery_Policy_Object(Delivery_Policy delivery_Policy_Object, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDistribution_Type_Object(Distribution_Type distribution_Type_Object, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFloat(double float_, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIdentifier(String identifier, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateIdentifier_Pattern(identifier, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @see #validateIdentifier_Pattern
	 */
	public static final  PatternMatcher [][] IDENTIFIER__PATTERN__VALUES =
		new PatternMatcher [][] {
			new PatternMatcher [] {
				XMLTypeUtil.createPatternMatcher("([a-z]|[A-Z])([a-z]|[A-Z]|[0-9]|.|_)*")
			},
			new PatternMatcher [] {
				XMLTypeUtil.createPatternMatcher("[\\i-[:]][\\c-[:]]*")
			},
			new PatternMatcher [] {
				XMLTypeUtil.createPatternMatcher("\\i\\c*")
			}
		};

	/**
	 * Validates the Pattern constraint of '<em>Identifier</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIdentifier_Pattern(String identifier, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validatePattern(ModelPackage.Literals.IDENTIFIER, identifier, IDENTIFIER__PATTERN__VALUES, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIdentifier_Ref(String identifier_Ref, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateIdentifier_Pattern(identifier_Ref, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIdentifier_Ref_List(List<?> identifier_Ref_List, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = xmlTypeValidator.validateNMTOKENS_MinLength(identifier_Ref_List, diagnostics, context);
		if (result || diagnostics != null) result &= xmlTypeValidator.validateNMTOKENSBase_ItemType(identifier_Ref_List, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateInterrupt_Priority(int interrupt_Priority, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNatural(int natural, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNegative_Assertion_Object(Negative_Assertion negative_Assertion_Object, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNormalized_Execution_Time(double normalized_Execution_Time, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOverhead_Type_Object(Overhead_Type overhead_Type_Object, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePathname(String pathname, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePercentage(double percentage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePositive(int positive, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePreemption_Level(int preemption_Level, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePriority(int priority, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePriority_Inheritance_Protocol_Object(Priority_Inheritance_Protocol priority_Inheritance_Protocol_Object, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequest_Policy_Object(Request_Policy request_Policy_Object, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateThroughput(double throughput, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTime(double time, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransmission_Type_Object(Transmission_Type transmission_Type_Object, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //ModelValidator
