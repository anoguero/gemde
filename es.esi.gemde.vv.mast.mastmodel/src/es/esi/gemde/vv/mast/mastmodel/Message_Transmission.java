/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Message Transmission</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getOverridden_Fixed_Priority <em>Overridden Fixed Priority</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getOverridden_Permanent_FP <em>Overridden Permanent FP</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getAvg_Message_Size <em>Avg Message Size</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getMax_Message_Size <em>Max Message Size</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getMin_Message_Size <em>Min Message Size</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMessage_Transmission()
 * @model extendedMetaData="name='Message_Transmission' kind='elementOnly'"
 * @generated
 */
public interface Message_Transmission extends EObject {
	/**
	 * Returns the value of the '<em><b>Overridden Fixed Priority</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overridden Fixed Priority</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overridden Fixed Priority</em>' containment reference.
	 * @see #setOverridden_Fixed_Priority(Overridden_Fixed_Priority)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMessage_Transmission_Overridden_Fixed_Priority()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Overridden_Fixed_Priority' namespace='##targetNamespace'"
	 * @generated
	 */
	Overridden_Fixed_Priority getOverridden_Fixed_Priority();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getOverridden_Fixed_Priority <em>Overridden Fixed Priority</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Overridden Fixed Priority</em>' containment reference.
	 * @see #getOverridden_Fixed_Priority()
	 * @generated
	 */
	void setOverridden_Fixed_Priority(Overridden_Fixed_Priority value);

	/**
	 * Returns the value of the '<em><b>Overridden Permanent FP</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overridden Permanent FP</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overridden Permanent FP</em>' containment reference.
	 * @see #setOverridden_Permanent_FP(Overridden_Permanent_FP)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMessage_Transmission_Overridden_Permanent_FP()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Overridden_Permanent_FP' namespace='##targetNamespace'"
	 * @generated
	 */
	Overridden_Permanent_FP getOverridden_Permanent_FP();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getOverridden_Permanent_FP <em>Overridden Permanent FP</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Overridden Permanent FP</em>' containment reference.
	 * @see #getOverridden_Permanent_FP()
	 * @generated
	 */
	void setOverridden_Permanent_FP(Overridden_Permanent_FP value);

	/**
	 * Returns the value of the '<em><b>Avg Message Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Avg Message Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Avg Message Size</em>' attribute.
	 * @see #setAvg_Message_Size(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMessage_Transmission_Avg_Message_Size()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Bit_Count"
	 *        extendedMetaData="kind='attribute' name='Avg_Message_Size'"
	 * @generated
	 */
	double getAvg_Message_Size();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getAvg_Message_Size <em>Avg Message Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Avg Message Size</em>' attribute.
	 * @see #getAvg_Message_Size()
	 * @generated
	 */
	void setAvg_Message_Size(double value);

	/**
	 * Returns the value of the '<em><b>Max Message Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Message Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Message Size</em>' attribute.
	 * @see #setMax_Message_Size(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMessage_Transmission_Max_Message_Size()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Bit_Count"
	 *        extendedMetaData="kind='attribute' name='Max_Message_Size'"
	 * @generated
	 */
	double getMax_Message_Size();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getMax_Message_Size <em>Max Message Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Message Size</em>' attribute.
	 * @see #getMax_Message_Size()
	 * @generated
	 */
	void setMax_Message_Size(double value);

	/**
	 * Returns the value of the '<em><b>Min Message Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Message Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Message Size</em>' attribute.
	 * @see #setMin_Message_Size(double)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMessage_Transmission_Min_Message_Size()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Bit_Count"
	 *        extendedMetaData="kind='attribute' name='Min_Message_Size'"
	 * @generated
	 */
	double getMin_Message_Size();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getMin_Message_Size <em>Min Message Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Message Size</em>' attribute.
	 * @see #getMin_Message_Size()
	 * @generated
	 */
	void setMin_Message_Size(double value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see es.esi.gemde.vv.mast.mastmodel.ModelPackage#getMessage_Transmission_Name()
	 * @model dataType="es.esi.gemde.vv.mast.mastmodel.Identifier" required="true"
	 *        extendedMetaData="kind='attribute' name='Name'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastmodel.Message_Transmission#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Message_Transmission
