/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.EDF_Scheduler;
import es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler;
import es.esi.gemde.vv.mast.mastmodel.Fixed_Priority_Scheduler;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Secondary_Scheduler;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Secondary Scheduler</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Secondary_SchedulerImpl#getFixed_Priority_Scheduler <em>Fixed Priority Scheduler</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Secondary_SchedulerImpl#getEDF_Scheduler <em>EDF Scheduler</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Secondary_SchedulerImpl#getFP_Packet_Based_Scheduler <em>FP Packet Based Scheduler</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Secondary_SchedulerImpl#getHost <em>Host</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Secondary_SchedulerImpl#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Secondary_SchedulerImpl extends EObjectImpl implements Secondary_Scheduler {
	/**
	 * The cached value of the '{@link #getFixed_Priority_Scheduler() <em>Fixed Priority Scheduler</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFixed_Priority_Scheduler()
	 * @generated
	 * @ordered
	 */
	protected Fixed_Priority_Scheduler fixed_Priority_Scheduler;

	/**
	 * The cached value of the '{@link #getEDF_Scheduler() <em>EDF Scheduler</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEDF_Scheduler()
	 * @generated
	 * @ordered
	 */
	protected EDF_Scheduler edF_Scheduler;

	/**
	 * The cached value of the '{@link #getFP_Packet_Based_Scheduler() <em>FP Packet Based Scheduler</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFP_Packet_Based_Scheduler()
	 * @generated
	 * @ordered
	 */
	protected FP_Packet_Based_Scheduler fP_Packet_Based_Scheduler;

	/**
	 * The default value of the '{@link #getHost() <em>Host</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHost()
	 * @generated
	 * @ordered
	 */
	protected static final String HOST_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHost() <em>Host</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHost()
	 * @generated
	 * @ordered
	 */
	protected String host = HOST_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Secondary_SchedulerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.SECONDARY_SCHEDULER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Fixed_Priority_Scheduler getFixed_Priority_Scheduler() {
		return fixed_Priority_Scheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFixed_Priority_Scheduler(Fixed_Priority_Scheduler newFixed_Priority_Scheduler, NotificationChain msgs) {
		Fixed_Priority_Scheduler oldFixed_Priority_Scheduler = fixed_Priority_Scheduler;
		fixed_Priority_Scheduler = newFixed_Priority_Scheduler;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.SECONDARY_SCHEDULER__FIXED_PRIORITY_SCHEDULER, oldFixed_Priority_Scheduler, newFixed_Priority_Scheduler);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFixed_Priority_Scheduler(Fixed_Priority_Scheduler newFixed_Priority_Scheduler) {
		if (newFixed_Priority_Scheduler != fixed_Priority_Scheduler) {
			NotificationChain msgs = null;
			if (fixed_Priority_Scheduler != null)
				msgs = ((InternalEObject)fixed_Priority_Scheduler).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.SECONDARY_SCHEDULER__FIXED_PRIORITY_SCHEDULER, null, msgs);
			if (newFixed_Priority_Scheduler != null)
				msgs = ((InternalEObject)newFixed_Priority_Scheduler).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.SECONDARY_SCHEDULER__FIXED_PRIORITY_SCHEDULER, null, msgs);
			msgs = basicSetFixed_Priority_Scheduler(newFixed_Priority_Scheduler, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SECONDARY_SCHEDULER__FIXED_PRIORITY_SCHEDULER, newFixed_Priority_Scheduler, newFixed_Priority_Scheduler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDF_Scheduler getEDF_Scheduler() {
		return edF_Scheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEDF_Scheduler(EDF_Scheduler newEDF_Scheduler, NotificationChain msgs) {
		EDF_Scheduler oldEDF_Scheduler = edF_Scheduler;
		edF_Scheduler = newEDF_Scheduler;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.SECONDARY_SCHEDULER__EDF_SCHEDULER, oldEDF_Scheduler, newEDF_Scheduler);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEDF_Scheduler(EDF_Scheduler newEDF_Scheduler) {
		if (newEDF_Scheduler != edF_Scheduler) {
			NotificationChain msgs = null;
			if (edF_Scheduler != null)
				msgs = ((InternalEObject)edF_Scheduler).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.SECONDARY_SCHEDULER__EDF_SCHEDULER, null, msgs);
			if (newEDF_Scheduler != null)
				msgs = ((InternalEObject)newEDF_Scheduler).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.SECONDARY_SCHEDULER__EDF_SCHEDULER, null, msgs);
			msgs = basicSetEDF_Scheduler(newEDF_Scheduler, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SECONDARY_SCHEDULER__EDF_SCHEDULER, newEDF_Scheduler, newEDF_Scheduler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FP_Packet_Based_Scheduler getFP_Packet_Based_Scheduler() {
		return fP_Packet_Based_Scheduler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFP_Packet_Based_Scheduler(FP_Packet_Based_Scheduler newFP_Packet_Based_Scheduler, NotificationChain msgs) {
		FP_Packet_Based_Scheduler oldFP_Packet_Based_Scheduler = fP_Packet_Based_Scheduler;
		fP_Packet_Based_Scheduler = newFP_Packet_Based_Scheduler;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.SECONDARY_SCHEDULER__FP_PACKET_BASED_SCHEDULER, oldFP_Packet_Based_Scheduler, newFP_Packet_Based_Scheduler);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFP_Packet_Based_Scheduler(FP_Packet_Based_Scheduler newFP_Packet_Based_Scheduler) {
		if (newFP_Packet_Based_Scheduler != fP_Packet_Based_Scheduler) {
			NotificationChain msgs = null;
			if (fP_Packet_Based_Scheduler != null)
				msgs = ((InternalEObject)fP_Packet_Based_Scheduler).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.SECONDARY_SCHEDULER__FP_PACKET_BASED_SCHEDULER, null, msgs);
			if (newFP_Packet_Based_Scheduler != null)
				msgs = ((InternalEObject)newFP_Packet_Based_Scheduler).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.SECONDARY_SCHEDULER__FP_PACKET_BASED_SCHEDULER, null, msgs);
			msgs = basicSetFP_Packet_Based_Scheduler(newFP_Packet_Based_Scheduler, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SECONDARY_SCHEDULER__FP_PACKET_BASED_SCHEDULER, newFP_Packet_Based_Scheduler, newFP_Packet_Based_Scheduler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHost() {
		return host;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHost(String newHost) {
		String oldHost = host;
		host = newHost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SECONDARY_SCHEDULER__HOST, oldHost, host));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.SECONDARY_SCHEDULER__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelPackage.SECONDARY_SCHEDULER__FIXED_PRIORITY_SCHEDULER:
				return basicSetFixed_Priority_Scheduler(null, msgs);
			case ModelPackage.SECONDARY_SCHEDULER__EDF_SCHEDULER:
				return basicSetEDF_Scheduler(null, msgs);
			case ModelPackage.SECONDARY_SCHEDULER__FP_PACKET_BASED_SCHEDULER:
				return basicSetFP_Packet_Based_Scheduler(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.SECONDARY_SCHEDULER__FIXED_PRIORITY_SCHEDULER:
				return getFixed_Priority_Scheduler();
			case ModelPackage.SECONDARY_SCHEDULER__EDF_SCHEDULER:
				return getEDF_Scheduler();
			case ModelPackage.SECONDARY_SCHEDULER__FP_PACKET_BASED_SCHEDULER:
				return getFP_Packet_Based_Scheduler();
			case ModelPackage.SECONDARY_SCHEDULER__HOST:
				return getHost();
			case ModelPackage.SECONDARY_SCHEDULER__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.SECONDARY_SCHEDULER__FIXED_PRIORITY_SCHEDULER:
				setFixed_Priority_Scheduler((Fixed_Priority_Scheduler)newValue);
				return;
			case ModelPackage.SECONDARY_SCHEDULER__EDF_SCHEDULER:
				setEDF_Scheduler((EDF_Scheduler)newValue);
				return;
			case ModelPackage.SECONDARY_SCHEDULER__FP_PACKET_BASED_SCHEDULER:
				setFP_Packet_Based_Scheduler((FP_Packet_Based_Scheduler)newValue);
				return;
			case ModelPackage.SECONDARY_SCHEDULER__HOST:
				setHost((String)newValue);
				return;
			case ModelPackage.SECONDARY_SCHEDULER__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.SECONDARY_SCHEDULER__FIXED_PRIORITY_SCHEDULER:
				setFixed_Priority_Scheduler((Fixed_Priority_Scheduler)null);
				return;
			case ModelPackage.SECONDARY_SCHEDULER__EDF_SCHEDULER:
				setEDF_Scheduler((EDF_Scheduler)null);
				return;
			case ModelPackage.SECONDARY_SCHEDULER__FP_PACKET_BASED_SCHEDULER:
				setFP_Packet_Based_Scheduler((FP_Packet_Based_Scheduler)null);
				return;
			case ModelPackage.SECONDARY_SCHEDULER__HOST:
				setHost(HOST_EDEFAULT);
				return;
			case ModelPackage.SECONDARY_SCHEDULER__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.SECONDARY_SCHEDULER__FIXED_PRIORITY_SCHEDULER:
				return fixed_Priority_Scheduler != null;
			case ModelPackage.SECONDARY_SCHEDULER__EDF_SCHEDULER:
				return edF_Scheduler != null;
			case ModelPackage.SECONDARY_SCHEDULER__FP_PACKET_BASED_SCHEDULER:
				return fP_Packet_Based_Scheduler != null;
			case ModelPackage.SECONDARY_SCHEDULER__HOST:
				return HOST_EDEFAULT == null ? host != null : !HOST_EDEFAULT.equals(host);
			case ModelPackage.SECONDARY_SCHEDULER__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Host: ");
		result.append(host);
		result.append(", Name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //Secondary_SchedulerImpl
