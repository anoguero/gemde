/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.List_of_Drivers;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Packet_Based_Network;
import es.esi.gemde.vv.mast.mastmodel.Transmission_Type;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Packet Based Network</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Packet_Based_NetworkImpl#getList_of_Drivers <em>List of Drivers</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Packet_Based_NetworkImpl#getMax_Blocking <em>Max Blocking</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Packet_Based_NetworkImpl#getMax_Packet_Size <em>Max Packet Size</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Packet_Based_NetworkImpl#getMin_Packet_Size <em>Min Packet Size</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Packet_Based_NetworkImpl#getName <em>Name</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Packet_Based_NetworkImpl#getSpeed_Factor <em>Speed Factor</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Packet_Based_NetworkImpl#getThroughput <em>Throughput</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Packet_Based_NetworkImpl#getTransmission <em>Transmission</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Packet_Based_NetworkImpl extends EObjectImpl implements Packet_Based_Network {
	/**
	 * The cached value of the '{@link #getList_of_Drivers() <em>List of Drivers</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getList_of_Drivers()
	 * @generated
	 * @ordered
	 */
	protected List_of_Drivers list_of_Drivers;

	/**
	 * The default value of the '{@link #getMax_Blocking() <em>Max Blocking</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_Blocking()
	 * @generated
	 * @ordered
	 */
	protected static final double MAX_BLOCKING_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getMax_Blocking() <em>Max Blocking</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_Blocking()
	 * @generated
	 * @ordered
	 */
	protected double max_Blocking = MAX_BLOCKING_EDEFAULT;

	/**
	 * The default value of the '{@link #getMax_Packet_Size() <em>Max Packet Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_Packet_Size()
	 * @generated
	 * @ordered
	 */
	protected static final double MAX_PACKET_SIZE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getMax_Packet_Size() <em>Max Packet Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMax_Packet_Size()
	 * @generated
	 * @ordered
	 */
	protected double max_Packet_Size = MAX_PACKET_SIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMin_Packet_Size() <em>Min Packet Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin_Packet_Size()
	 * @generated
	 * @ordered
	 */
	protected static final double MIN_PACKET_SIZE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getMin_Packet_Size() <em>Min Packet Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMin_Packet_Size()
	 * @generated
	 * @ordered
	 */
	protected double min_Packet_Size = MIN_PACKET_SIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getSpeed_Factor() <em>Speed Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpeed_Factor()
	 * @generated
	 * @ordered
	 */
	protected static final double SPEED_FACTOR_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getSpeed_Factor() <em>Speed Factor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpeed_Factor()
	 * @generated
	 * @ordered
	 */
	protected double speed_Factor = SPEED_FACTOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getThroughput() <em>Throughput</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThroughput()
	 * @generated
	 * @ordered
	 */
	protected static final double THROUGHPUT_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getThroughput() <em>Throughput</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThroughput()
	 * @generated
	 * @ordered
	 */
	protected double throughput = THROUGHPUT_EDEFAULT;

	/**
	 * The default value of the '{@link #getTransmission() <em>Transmission</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransmission()
	 * @generated
	 * @ordered
	 */
	protected static final Transmission_Type TRANSMISSION_EDEFAULT = Transmission_Type.SIMPLEX;

	/**
	 * The cached value of the '{@link #getTransmission() <em>Transmission</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransmission()
	 * @generated
	 * @ordered
	 */
	protected Transmission_Type transmission = TRANSMISSION_EDEFAULT;

	/**
	 * This is true if the Transmission attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean transmissionESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Packet_Based_NetworkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.PACKET_BASED_NETWORK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List_of_Drivers getList_of_Drivers() {
		return list_of_Drivers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetList_of_Drivers(List_of_Drivers newList_of_Drivers, NotificationChain msgs) {
		List_of_Drivers oldList_of_Drivers = list_of_Drivers;
		list_of_Drivers = newList_of_Drivers;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModelPackage.PACKET_BASED_NETWORK__LIST_OF_DRIVERS, oldList_of_Drivers, newList_of_Drivers);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setList_of_Drivers(List_of_Drivers newList_of_Drivers) {
		if (newList_of_Drivers != list_of_Drivers) {
			NotificationChain msgs = null;
			if (list_of_Drivers != null)
				msgs = ((InternalEObject)list_of_Drivers).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ModelPackage.PACKET_BASED_NETWORK__LIST_OF_DRIVERS, null, msgs);
			if (newList_of_Drivers != null)
				msgs = ((InternalEObject)newList_of_Drivers).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ModelPackage.PACKET_BASED_NETWORK__LIST_OF_DRIVERS, null, msgs);
			msgs = basicSetList_of_Drivers(newList_of_Drivers, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.PACKET_BASED_NETWORK__LIST_OF_DRIVERS, newList_of_Drivers, newList_of_Drivers));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getMax_Blocking() {
		return max_Blocking;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMax_Blocking(double newMax_Blocking) {
		double oldMax_Blocking = max_Blocking;
		max_Blocking = newMax_Blocking;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.PACKET_BASED_NETWORK__MAX_BLOCKING, oldMax_Blocking, max_Blocking));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getMax_Packet_Size() {
		return max_Packet_Size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMax_Packet_Size(double newMax_Packet_Size) {
		double oldMax_Packet_Size = max_Packet_Size;
		max_Packet_Size = newMax_Packet_Size;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.PACKET_BASED_NETWORK__MAX_PACKET_SIZE, oldMax_Packet_Size, max_Packet_Size));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getMin_Packet_Size() {
		return min_Packet_Size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMin_Packet_Size(double newMin_Packet_Size) {
		double oldMin_Packet_Size = min_Packet_Size;
		min_Packet_Size = newMin_Packet_Size;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.PACKET_BASED_NETWORK__MIN_PACKET_SIZE, oldMin_Packet_Size, min_Packet_Size));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.PACKET_BASED_NETWORK__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getSpeed_Factor() {
		return speed_Factor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpeed_Factor(double newSpeed_Factor) {
		double oldSpeed_Factor = speed_Factor;
		speed_Factor = newSpeed_Factor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.PACKET_BASED_NETWORK__SPEED_FACTOR, oldSpeed_Factor, speed_Factor));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getThroughput() {
		return throughput;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setThroughput(double newThroughput) {
		double oldThroughput = throughput;
		throughput = newThroughput;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.PACKET_BASED_NETWORK__THROUGHPUT, oldThroughput, throughput));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transmission_Type getTransmission() {
		return transmission;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTransmission(Transmission_Type newTransmission) {
		Transmission_Type oldTransmission = transmission;
		transmission = newTransmission == null ? TRANSMISSION_EDEFAULT : newTransmission;
		boolean oldTransmissionESet = transmissionESet;
		transmissionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.PACKET_BASED_NETWORK__TRANSMISSION, oldTransmission, transmission, !oldTransmissionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTransmission() {
		Transmission_Type oldTransmission = transmission;
		boolean oldTransmissionESet = transmissionESet;
		transmission = TRANSMISSION_EDEFAULT;
		transmissionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ModelPackage.PACKET_BASED_NETWORK__TRANSMISSION, oldTransmission, TRANSMISSION_EDEFAULT, oldTransmissionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTransmission() {
		return transmissionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelPackage.PACKET_BASED_NETWORK__LIST_OF_DRIVERS:
				return basicSetList_of_Drivers(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.PACKET_BASED_NETWORK__LIST_OF_DRIVERS:
				return getList_of_Drivers();
			case ModelPackage.PACKET_BASED_NETWORK__MAX_BLOCKING:
				return getMax_Blocking();
			case ModelPackage.PACKET_BASED_NETWORK__MAX_PACKET_SIZE:
				return getMax_Packet_Size();
			case ModelPackage.PACKET_BASED_NETWORK__MIN_PACKET_SIZE:
				return getMin_Packet_Size();
			case ModelPackage.PACKET_BASED_NETWORK__NAME:
				return getName();
			case ModelPackage.PACKET_BASED_NETWORK__SPEED_FACTOR:
				return getSpeed_Factor();
			case ModelPackage.PACKET_BASED_NETWORK__THROUGHPUT:
				return getThroughput();
			case ModelPackage.PACKET_BASED_NETWORK__TRANSMISSION:
				return getTransmission();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.PACKET_BASED_NETWORK__LIST_OF_DRIVERS:
				setList_of_Drivers((List_of_Drivers)newValue);
				return;
			case ModelPackage.PACKET_BASED_NETWORK__MAX_BLOCKING:
				setMax_Blocking((Double)newValue);
				return;
			case ModelPackage.PACKET_BASED_NETWORK__MAX_PACKET_SIZE:
				setMax_Packet_Size((Double)newValue);
				return;
			case ModelPackage.PACKET_BASED_NETWORK__MIN_PACKET_SIZE:
				setMin_Packet_Size((Double)newValue);
				return;
			case ModelPackage.PACKET_BASED_NETWORK__NAME:
				setName((String)newValue);
				return;
			case ModelPackage.PACKET_BASED_NETWORK__SPEED_FACTOR:
				setSpeed_Factor((Double)newValue);
				return;
			case ModelPackage.PACKET_BASED_NETWORK__THROUGHPUT:
				setThroughput((Double)newValue);
				return;
			case ModelPackage.PACKET_BASED_NETWORK__TRANSMISSION:
				setTransmission((Transmission_Type)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.PACKET_BASED_NETWORK__LIST_OF_DRIVERS:
				setList_of_Drivers((List_of_Drivers)null);
				return;
			case ModelPackage.PACKET_BASED_NETWORK__MAX_BLOCKING:
				setMax_Blocking(MAX_BLOCKING_EDEFAULT);
				return;
			case ModelPackage.PACKET_BASED_NETWORK__MAX_PACKET_SIZE:
				setMax_Packet_Size(MAX_PACKET_SIZE_EDEFAULT);
				return;
			case ModelPackage.PACKET_BASED_NETWORK__MIN_PACKET_SIZE:
				setMin_Packet_Size(MIN_PACKET_SIZE_EDEFAULT);
				return;
			case ModelPackage.PACKET_BASED_NETWORK__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ModelPackage.PACKET_BASED_NETWORK__SPEED_FACTOR:
				setSpeed_Factor(SPEED_FACTOR_EDEFAULT);
				return;
			case ModelPackage.PACKET_BASED_NETWORK__THROUGHPUT:
				setThroughput(THROUGHPUT_EDEFAULT);
				return;
			case ModelPackage.PACKET_BASED_NETWORK__TRANSMISSION:
				unsetTransmission();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.PACKET_BASED_NETWORK__LIST_OF_DRIVERS:
				return list_of_Drivers != null;
			case ModelPackage.PACKET_BASED_NETWORK__MAX_BLOCKING:
				return max_Blocking != MAX_BLOCKING_EDEFAULT;
			case ModelPackage.PACKET_BASED_NETWORK__MAX_PACKET_SIZE:
				return max_Packet_Size != MAX_PACKET_SIZE_EDEFAULT;
			case ModelPackage.PACKET_BASED_NETWORK__MIN_PACKET_SIZE:
				return min_Packet_Size != MIN_PACKET_SIZE_EDEFAULT;
			case ModelPackage.PACKET_BASED_NETWORK__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ModelPackage.PACKET_BASED_NETWORK__SPEED_FACTOR:
				return speed_Factor != SPEED_FACTOR_EDEFAULT;
			case ModelPackage.PACKET_BASED_NETWORK__THROUGHPUT:
				return throughput != THROUGHPUT_EDEFAULT;
			case ModelPackage.PACKET_BASED_NETWORK__TRANSMISSION:
				return isSetTransmission();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (Max_Blocking: ");
		result.append(max_Blocking);
		result.append(", Max_Packet_Size: ");
		result.append(max_Packet_Size);
		result.append(", Min_Packet_Size: ");
		result.append(min_Packet_Size);
		result.append(", Name: ");
		result.append(name);
		result.append(", Speed_Factor: ");
		result.append(speed_Factor);
		result.append(", Throughput: ");
		result.append(throughput);
		result.append(", Transmission: ");
		if (transmissionESet) result.append(transmission); else result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //Packet_Based_NetworkImpl
