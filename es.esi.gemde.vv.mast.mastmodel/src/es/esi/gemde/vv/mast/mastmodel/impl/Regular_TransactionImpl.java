/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.impl;

import es.esi.gemde.vv.mast.mastmodel.Activity;
import es.esi.gemde.vv.mast.mastmodel.Barrier;
import es.esi.gemde.vv.mast.mastmodel.Bursty_External_Event;
import es.esi.gemde.vv.mast.mastmodel.Concentrator;
import es.esi.gemde.vv.mast.mastmodel.Delay;
import es.esi.gemde.vv.mast.mastmodel.Delivery_Server;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Multicast;
import es.esi.gemde.vv.mast.mastmodel.Offset;
import es.esi.gemde.vv.mast.mastmodel.Periodic_External_Event;
import es.esi.gemde.vv.mast.mastmodel.Query_Server;
import es.esi.gemde.vv.mast.mastmodel.Rate_Divisor;
import es.esi.gemde.vv.mast.mastmodel.Regular_Event;
import es.esi.gemde.vv.mast.mastmodel.Regular_Transaction;
import es.esi.gemde.vv.mast.mastmodel.Singular_External_Event;
import es.esi.gemde.vv.mast.mastmodel.Sporadic_External_Event;
import es.esi.gemde.vv.mast.mastmodel.System_Timed_Activity;
import es.esi.gemde.vv.mast.mastmodel.Unbounded_External_Event;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Regular Transaction</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl#getPeriodic_External_Event <em>Periodic External Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl#getSporadic_External_Event <em>Sporadic External Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl#getUnbounded_External_Event <em>Unbounded External Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl#getBursty_External_Event <em>Bursty External Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl#getSingular_External_Event <em>Singular External Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl#getRegular_Event <em>Regular Event</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl#getActivity <em>Activity</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl#getSystem_Timed_Activity <em>System Timed Activity</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl#getConcentrator <em>Concentrator</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl#getBarrier <em>Barrier</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl#getDelivery_Server <em>Delivery Server</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl#getQuery_Server <em>Query Server</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl#getMulticast <em>Multicast</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl#getRate_Divisor <em>Rate Divisor</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl#getDelay <em>Delay</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl#getOffset <em>Offset</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastmodel.impl.Regular_TransactionImpl#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Regular_TransactionImpl extends EObjectImpl implements Regular_Transaction {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Regular_TransactionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModelPackage.Literals.REGULAR_TRANSACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, ModelPackage.REGULAR_TRANSACTION__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Periodic_External_Event> getPeriodic_External_Event() {
		return getGroup().list(ModelPackage.Literals.REGULAR_TRANSACTION__PERIODIC_EXTERNAL_EVENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Sporadic_External_Event> getSporadic_External_Event() {
		return getGroup().list(ModelPackage.Literals.REGULAR_TRANSACTION__SPORADIC_EXTERNAL_EVENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Unbounded_External_Event> getUnbounded_External_Event() {
		return getGroup().list(ModelPackage.Literals.REGULAR_TRANSACTION__UNBOUNDED_EXTERNAL_EVENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Bursty_External_Event> getBursty_External_Event() {
		return getGroup().list(ModelPackage.Literals.REGULAR_TRANSACTION__BURSTY_EXTERNAL_EVENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Singular_External_Event> getSingular_External_Event() {
		return getGroup().list(ModelPackage.Literals.REGULAR_TRANSACTION__SINGULAR_EXTERNAL_EVENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Regular_Event> getRegular_Event() {
		return getGroup().list(ModelPackage.Literals.REGULAR_TRANSACTION__REGULAR_EVENT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Activity> getActivity() {
		return getGroup().list(ModelPackage.Literals.REGULAR_TRANSACTION__ACTIVITY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<System_Timed_Activity> getSystem_Timed_Activity() {
		return getGroup().list(ModelPackage.Literals.REGULAR_TRANSACTION__SYSTEM_TIMED_ACTIVITY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Concentrator> getConcentrator() {
		return getGroup().list(ModelPackage.Literals.REGULAR_TRANSACTION__CONCENTRATOR);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Barrier> getBarrier() {
		return getGroup().list(ModelPackage.Literals.REGULAR_TRANSACTION__BARRIER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Delivery_Server> getDelivery_Server() {
		return getGroup().list(ModelPackage.Literals.REGULAR_TRANSACTION__DELIVERY_SERVER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Query_Server> getQuery_Server() {
		return getGroup().list(ModelPackage.Literals.REGULAR_TRANSACTION__QUERY_SERVER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Multicast> getMulticast() {
		return getGroup().list(ModelPackage.Literals.REGULAR_TRANSACTION__MULTICAST);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Rate_Divisor> getRate_Divisor() {
		return getGroup().list(ModelPackage.Literals.REGULAR_TRANSACTION__RATE_DIVISOR);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Delay> getDelay() {
		return getGroup().list(ModelPackage.Literals.REGULAR_TRANSACTION__DELAY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Offset> getOffset() {
		return getGroup().list(ModelPackage.Literals.REGULAR_TRANSACTION__OFFSET);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModelPackage.REGULAR_TRANSACTION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModelPackage.REGULAR_TRANSACTION__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_TRANSACTION__PERIODIC_EXTERNAL_EVENT:
				return ((InternalEList<?>)getPeriodic_External_Event()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_TRANSACTION__SPORADIC_EXTERNAL_EVENT:
				return ((InternalEList<?>)getSporadic_External_Event()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_TRANSACTION__UNBOUNDED_EXTERNAL_EVENT:
				return ((InternalEList<?>)getUnbounded_External_Event()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_TRANSACTION__BURSTY_EXTERNAL_EVENT:
				return ((InternalEList<?>)getBursty_External_Event()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_TRANSACTION__SINGULAR_EXTERNAL_EVENT:
				return ((InternalEList<?>)getSingular_External_Event()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_TRANSACTION__REGULAR_EVENT:
				return ((InternalEList<?>)getRegular_Event()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_TRANSACTION__ACTIVITY:
				return ((InternalEList<?>)getActivity()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_TRANSACTION__SYSTEM_TIMED_ACTIVITY:
				return ((InternalEList<?>)getSystem_Timed_Activity()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_TRANSACTION__CONCENTRATOR:
				return ((InternalEList<?>)getConcentrator()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_TRANSACTION__BARRIER:
				return ((InternalEList<?>)getBarrier()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_TRANSACTION__DELIVERY_SERVER:
				return ((InternalEList<?>)getDelivery_Server()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_TRANSACTION__QUERY_SERVER:
				return ((InternalEList<?>)getQuery_Server()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_TRANSACTION__MULTICAST:
				return ((InternalEList<?>)getMulticast()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_TRANSACTION__RATE_DIVISOR:
				return ((InternalEList<?>)getRate_Divisor()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_TRANSACTION__DELAY:
				return ((InternalEList<?>)getDelay()).basicRemove(otherEnd, msgs);
			case ModelPackage.REGULAR_TRANSACTION__OFFSET:
				return ((InternalEList<?>)getOffset()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModelPackage.REGULAR_TRANSACTION__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case ModelPackage.REGULAR_TRANSACTION__PERIODIC_EXTERNAL_EVENT:
				return getPeriodic_External_Event();
			case ModelPackage.REGULAR_TRANSACTION__SPORADIC_EXTERNAL_EVENT:
				return getSporadic_External_Event();
			case ModelPackage.REGULAR_TRANSACTION__UNBOUNDED_EXTERNAL_EVENT:
				return getUnbounded_External_Event();
			case ModelPackage.REGULAR_TRANSACTION__BURSTY_EXTERNAL_EVENT:
				return getBursty_External_Event();
			case ModelPackage.REGULAR_TRANSACTION__SINGULAR_EXTERNAL_EVENT:
				return getSingular_External_Event();
			case ModelPackage.REGULAR_TRANSACTION__REGULAR_EVENT:
				return getRegular_Event();
			case ModelPackage.REGULAR_TRANSACTION__ACTIVITY:
				return getActivity();
			case ModelPackage.REGULAR_TRANSACTION__SYSTEM_TIMED_ACTIVITY:
				return getSystem_Timed_Activity();
			case ModelPackage.REGULAR_TRANSACTION__CONCENTRATOR:
				return getConcentrator();
			case ModelPackage.REGULAR_TRANSACTION__BARRIER:
				return getBarrier();
			case ModelPackage.REGULAR_TRANSACTION__DELIVERY_SERVER:
				return getDelivery_Server();
			case ModelPackage.REGULAR_TRANSACTION__QUERY_SERVER:
				return getQuery_Server();
			case ModelPackage.REGULAR_TRANSACTION__MULTICAST:
				return getMulticast();
			case ModelPackage.REGULAR_TRANSACTION__RATE_DIVISOR:
				return getRate_Divisor();
			case ModelPackage.REGULAR_TRANSACTION__DELAY:
				return getDelay();
			case ModelPackage.REGULAR_TRANSACTION__OFFSET:
				return getOffset();
			case ModelPackage.REGULAR_TRANSACTION__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModelPackage.REGULAR_TRANSACTION__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case ModelPackage.REGULAR_TRANSACTION__PERIODIC_EXTERNAL_EVENT:
				getPeriodic_External_Event().clear();
				getPeriodic_External_Event().addAll((Collection<? extends Periodic_External_Event>)newValue);
				return;
			case ModelPackage.REGULAR_TRANSACTION__SPORADIC_EXTERNAL_EVENT:
				getSporadic_External_Event().clear();
				getSporadic_External_Event().addAll((Collection<? extends Sporadic_External_Event>)newValue);
				return;
			case ModelPackage.REGULAR_TRANSACTION__UNBOUNDED_EXTERNAL_EVENT:
				getUnbounded_External_Event().clear();
				getUnbounded_External_Event().addAll((Collection<? extends Unbounded_External_Event>)newValue);
				return;
			case ModelPackage.REGULAR_TRANSACTION__BURSTY_EXTERNAL_EVENT:
				getBursty_External_Event().clear();
				getBursty_External_Event().addAll((Collection<? extends Bursty_External_Event>)newValue);
				return;
			case ModelPackage.REGULAR_TRANSACTION__SINGULAR_EXTERNAL_EVENT:
				getSingular_External_Event().clear();
				getSingular_External_Event().addAll((Collection<? extends Singular_External_Event>)newValue);
				return;
			case ModelPackage.REGULAR_TRANSACTION__REGULAR_EVENT:
				getRegular_Event().clear();
				getRegular_Event().addAll((Collection<? extends Regular_Event>)newValue);
				return;
			case ModelPackage.REGULAR_TRANSACTION__ACTIVITY:
				getActivity().clear();
				getActivity().addAll((Collection<? extends Activity>)newValue);
				return;
			case ModelPackage.REGULAR_TRANSACTION__SYSTEM_TIMED_ACTIVITY:
				getSystem_Timed_Activity().clear();
				getSystem_Timed_Activity().addAll((Collection<? extends System_Timed_Activity>)newValue);
				return;
			case ModelPackage.REGULAR_TRANSACTION__CONCENTRATOR:
				getConcentrator().clear();
				getConcentrator().addAll((Collection<? extends Concentrator>)newValue);
				return;
			case ModelPackage.REGULAR_TRANSACTION__BARRIER:
				getBarrier().clear();
				getBarrier().addAll((Collection<? extends Barrier>)newValue);
				return;
			case ModelPackage.REGULAR_TRANSACTION__DELIVERY_SERVER:
				getDelivery_Server().clear();
				getDelivery_Server().addAll((Collection<? extends Delivery_Server>)newValue);
				return;
			case ModelPackage.REGULAR_TRANSACTION__QUERY_SERVER:
				getQuery_Server().clear();
				getQuery_Server().addAll((Collection<? extends Query_Server>)newValue);
				return;
			case ModelPackage.REGULAR_TRANSACTION__MULTICAST:
				getMulticast().clear();
				getMulticast().addAll((Collection<? extends Multicast>)newValue);
				return;
			case ModelPackage.REGULAR_TRANSACTION__RATE_DIVISOR:
				getRate_Divisor().clear();
				getRate_Divisor().addAll((Collection<? extends Rate_Divisor>)newValue);
				return;
			case ModelPackage.REGULAR_TRANSACTION__DELAY:
				getDelay().clear();
				getDelay().addAll((Collection<? extends Delay>)newValue);
				return;
			case ModelPackage.REGULAR_TRANSACTION__OFFSET:
				getOffset().clear();
				getOffset().addAll((Collection<? extends Offset>)newValue);
				return;
			case ModelPackage.REGULAR_TRANSACTION__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModelPackage.REGULAR_TRANSACTION__GROUP:
				getGroup().clear();
				return;
			case ModelPackage.REGULAR_TRANSACTION__PERIODIC_EXTERNAL_EVENT:
				getPeriodic_External_Event().clear();
				return;
			case ModelPackage.REGULAR_TRANSACTION__SPORADIC_EXTERNAL_EVENT:
				getSporadic_External_Event().clear();
				return;
			case ModelPackage.REGULAR_TRANSACTION__UNBOUNDED_EXTERNAL_EVENT:
				getUnbounded_External_Event().clear();
				return;
			case ModelPackage.REGULAR_TRANSACTION__BURSTY_EXTERNAL_EVENT:
				getBursty_External_Event().clear();
				return;
			case ModelPackage.REGULAR_TRANSACTION__SINGULAR_EXTERNAL_EVENT:
				getSingular_External_Event().clear();
				return;
			case ModelPackage.REGULAR_TRANSACTION__REGULAR_EVENT:
				getRegular_Event().clear();
				return;
			case ModelPackage.REGULAR_TRANSACTION__ACTIVITY:
				getActivity().clear();
				return;
			case ModelPackage.REGULAR_TRANSACTION__SYSTEM_TIMED_ACTIVITY:
				getSystem_Timed_Activity().clear();
				return;
			case ModelPackage.REGULAR_TRANSACTION__CONCENTRATOR:
				getConcentrator().clear();
				return;
			case ModelPackage.REGULAR_TRANSACTION__BARRIER:
				getBarrier().clear();
				return;
			case ModelPackage.REGULAR_TRANSACTION__DELIVERY_SERVER:
				getDelivery_Server().clear();
				return;
			case ModelPackage.REGULAR_TRANSACTION__QUERY_SERVER:
				getQuery_Server().clear();
				return;
			case ModelPackage.REGULAR_TRANSACTION__MULTICAST:
				getMulticast().clear();
				return;
			case ModelPackage.REGULAR_TRANSACTION__RATE_DIVISOR:
				getRate_Divisor().clear();
				return;
			case ModelPackage.REGULAR_TRANSACTION__DELAY:
				getDelay().clear();
				return;
			case ModelPackage.REGULAR_TRANSACTION__OFFSET:
				getOffset().clear();
				return;
			case ModelPackage.REGULAR_TRANSACTION__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModelPackage.REGULAR_TRANSACTION__GROUP:
				return group != null && !group.isEmpty();
			case ModelPackage.REGULAR_TRANSACTION__PERIODIC_EXTERNAL_EVENT:
				return !getPeriodic_External_Event().isEmpty();
			case ModelPackage.REGULAR_TRANSACTION__SPORADIC_EXTERNAL_EVENT:
				return !getSporadic_External_Event().isEmpty();
			case ModelPackage.REGULAR_TRANSACTION__UNBOUNDED_EXTERNAL_EVENT:
				return !getUnbounded_External_Event().isEmpty();
			case ModelPackage.REGULAR_TRANSACTION__BURSTY_EXTERNAL_EVENT:
				return !getBursty_External_Event().isEmpty();
			case ModelPackage.REGULAR_TRANSACTION__SINGULAR_EXTERNAL_EVENT:
				return !getSingular_External_Event().isEmpty();
			case ModelPackage.REGULAR_TRANSACTION__REGULAR_EVENT:
				return !getRegular_Event().isEmpty();
			case ModelPackage.REGULAR_TRANSACTION__ACTIVITY:
				return !getActivity().isEmpty();
			case ModelPackage.REGULAR_TRANSACTION__SYSTEM_TIMED_ACTIVITY:
				return !getSystem_Timed_Activity().isEmpty();
			case ModelPackage.REGULAR_TRANSACTION__CONCENTRATOR:
				return !getConcentrator().isEmpty();
			case ModelPackage.REGULAR_TRANSACTION__BARRIER:
				return !getBarrier().isEmpty();
			case ModelPackage.REGULAR_TRANSACTION__DELIVERY_SERVER:
				return !getDelivery_Server().isEmpty();
			case ModelPackage.REGULAR_TRANSACTION__QUERY_SERVER:
				return !getQuery_Server().isEmpty();
			case ModelPackage.REGULAR_TRANSACTION__MULTICAST:
				return !getMulticast().isEmpty();
			case ModelPackage.REGULAR_TRANSACTION__RATE_DIVISOR:
				return !getRate_Divisor().isEmpty();
			case ModelPackage.REGULAR_TRANSACTION__DELAY:
				return !getDelay().isEmpty();
			case ModelPackage.REGULAR_TRANSACTION__OFFSET:
				return !getOffset().isEmpty();
			case ModelPackage.REGULAR_TRANSACTION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(", Name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //Regular_TransactionImpl
