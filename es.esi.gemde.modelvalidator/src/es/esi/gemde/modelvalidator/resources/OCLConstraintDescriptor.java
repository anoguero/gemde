/**
 * <copyright>
 *
 * Copyright (c) 2007 IBM Corporation and others.
 * All rights reserved.   This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   IBM - Initial API and implementation
 *
 * </copyright>
 *
 * $Id: OCLConstraintDescriptor.java,v 1.2 2007/11/14 18:03:39 cdamus Exp $
 */
package es.esi.gemde.modelvalidator.resources;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.model.ConstraintSeverity;
import org.eclipse.emf.validation.model.EvaluationMode;
import org.eclipse.emf.validation.service.AbstractConstraintDescriptor;
import org.eclipse.ocl.ecore.Constraint;

/**
 * Implementation of a constraint descriptor for our provider.
 * Taken from the OCL example of the EMF Validation Framework
 *
 * @author Christian W. Damus (with modifications from Adri�n Noguero (adrian.noguero@esi.es))
 * @version 1.0
 * @since 1.0
 *
 */
public class OCLConstraintDescriptor extends AbstractConstraintDescriptor {
	private final Constraint constraint;
	private final String id;
	private final String name;
	private final String namespace;
	private final int severityCode;
	private final String body;
	
	/**
	 * A constructor for the descriptor.
	 *
	 *@param namespace the namespace of the constraint.
	 *@param constraint the name of the constraint.
	 *@param severityCode the severity definition according to the constants defined in IStatus.
	 *@param plainBody the plain OCL definition of the constraint in text.
	 */
	public OCLConstraintDescriptor(String namespace, Constraint constraint, int severityCode, String plainBody) {
		this.constraint = constraint;
		
		String name = constraint.getName();
		if (name == null) {
			name = Long.toHexString(System.identityHashCode(constraint));
		}
		
		id = namespace + '.' + name;
		this.name = name;
		this.namespace = namespace;
		this.severityCode = severityCode;
		this.body = plainBody;
	}
	
	/**
	 * A method to access the Constraint object defined by this descriptor
	 * @return the OCL constraint object
	 */
	public final Constraint getConstraint() {
		return constraint;
	}
	
	/** 
	 * This method implementation was modified to provide the plain OCL body 
	 * of the constraint.
	 * 
	 * @return the plain (unparsed) OCL body of this constraint 
	 * @see org.eclipse.emf.validation.service.IConstraintDescriptor#getBody()
	 * 
	 */
	public String getBody() {
		return body;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.validation.service.IConstraintDescriptor#getDescription()
	 */
	public String getDescription() {
		return constraint.getSpecification().getBodyExpression().toString();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.validation.service.IConstraintDescriptor#getEvaluationMode()
	 */
	public EvaluationMode<?> getEvaluationMode() {
		return EvaluationMode.BATCH;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.validation.service.IConstraintDescriptor#getId()
	 */
	public String getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.validation.service.IConstraintDescriptor#getMessagePattern()
	 */
	public String getMessagePattern() {
		return String.format("Constraint %s violated on {0}", getName()); //$NON-NLS-1$
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.validation.service.IConstraintDescriptor#getName()
	 */
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.validation.service.IConstraintDescriptor#getPluginId()
	 */
	public String getPluginId() {
		return namespace;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.validation.service.IConstraintDescriptor#getSeverity()
	 */
	public ConstraintSeverity getSeverity() {
		switch (severityCode) {
		case IStatus.INFO:
			return ConstraintSeverity.INFO;
		case IStatus.ERROR:
			return ConstraintSeverity.ERROR;
		case IStatus.WARNING:
			return ConstraintSeverity.WARNING;
		default:
			return ConstraintSeverity.NULL;
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.validation.service.IConstraintDescriptor#getStatusCode()
	 */
	public int getStatusCode() {
		return severityCode;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.validation.service.IConstraintDescriptor#targetsEvent(org.eclipse.emf.common.notify.Notification)
	 */
	public boolean targetsEvent(Notification notification) {
		return false;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.validation.service.IConstraintDescriptor#targetsTypeOf(org.eclipse.emf.ecore.EObject)
	 */
	public boolean targetsTypeOf(EObject eObject) {
		return constraint.getSpecification().getContextVariable().getType()
				.isInstance(eObject);
	}

}
