/**
 * <copyright>
 *
 * Copyright (c) 2007 IBM Corporation and others.
 * All rights reserved.   This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   IBM - Initial API and implementation
 *
 * </copyright>
 *
 * $Id: OCLConstraint.java,v 1.1 2007/05/07 16:12:12 cdamus Exp $
 */
package es.esi.gemde.modelvalidator.resources;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.validation.ocl.AbstractOCLModelConstraint;
import org.eclipse.ocl.EnvironmentFactory;
import org.eclipse.ocl.Query;
import org.eclipse.ocl.ecore.Constraint;
import org.eclipse.ocl.ecore.OCL;


/**
 * A simple implementation of the {@link AbstractOCLModelConstraint}.
 * Taken from the OCL example of the EMF Validation Framework.
 *
 * @author Christian W. Damus
 * @version 1.0
 * @since 1.0
 *
 */
public class OCLConstraint extends AbstractOCLModelConstraint<EClassifier, Constraint, EClass, EObject> {
	private final OCL.Query query;
	
	/**
	 * A constructor that creates a constraint from a descriptor and an OCL instance.
	 *
	 *@param desc the OCLConstraintDescriptor that defines this constraint
	 *@param ocl the OCL instance that contains the constraints.
	 */
	public OCLConstraint(OCLConstraintDescriptor desc, OCL ocl) {
		super(desc);
		
		this.query = ocl.createQuery(desc.getConstraint());
	}
	
	// override this method to indicate that we are doing new-style OCL
	@Override
	protected EnvironmentFactory<?, EClassifier, ?, ?, ?, ?, ?, ?, ?, Constraint, EClass, EObject>
	createOCLEnvironmentFactory() {
		return query.getOCL().getEnvironment().getFactory();
	}
	
	@Override
	public Query<EClassifier, EClass, EObject> getConstraintCondition(EObject target) {
		return query;
	}
}
