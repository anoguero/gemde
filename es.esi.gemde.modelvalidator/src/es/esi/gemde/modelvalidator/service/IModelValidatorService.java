/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/

package es.esi.gemde.modelvalidator.service;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import es.esi.gemde.modelvalidator.exceptions.IllegalTargetException;
import es.esi.gemde.modelvalidator.exceptions.NoConstraintsValidatedException;
import es.esi.gemde.modelvalidator.exceptions.ValidationEngineException;



/**
 * Definition of the interface that will be provided by the Model Validation Service in GEMDE.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public interface IModelValidatorService {


	/**
	 * This method adds a constraint to the constraint repository.
	 * 
	 * @param constraint the constraint to be added.
	 * @throws IllegalArgumentException if the provided constraint is null or if the constraint already exists in the repository.
	 */
	public void addConstraint(IModelValidatorConstraint constraint) throws IllegalArgumentException;
	
	
	/**
	 * This method adds a constraint to the repository for just one session. Constraints added using this method cannot be removed from the repository until the service is disposed.
	 * It is intended to be used by other plug-ins that require to load constraints and protect them.
	 * 
	 * @param constraint the constraint to be added.
	 * @throws IllegalArgumentException if the provided constraint is null or if the constraint already exists in the repository.
	 */
	public void addSessionConstraint(IModelValidatorConstraint constraint) throws IllegalArgumentException;


	/**
	 * This method adds a constraint to the constraint repository.
	 * 
	 * @param name the name of the constraint to be removed.
	 * @param category the category of the constraint to be removed.
	 * @throws IllegalArgumentException if any of the parameters is null or if the selected constraint is being used by an active validation.
	 */
	public void removeConstraint(String name, String category) throws IllegalArgumentException;


	/**
	 * This method changes a constraint in the repository with the same name and category 
	 * as the provided constraint with the new implementation.
	 * 
	 * @param constraint updated implementation of the constraint to be changed.
	 * @throws IllegalArgumentException if the provided constraint is null, if it doesn't exist in the repository or if it is currently in use by an active validation.
	 */
	public void changeConstraint(IModelValidatorConstraint constraint) throws IllegalArgumentException;

	
	/**
	 * This method adds a new validation to the batch validation repository.
	 * 
	 * @param validation the new validation to be added to the repository.
	 * @throws IllegalArgumentException if the new validation is null or if it already exists in the repository.
	 */
	public void addBatchValidation(IBatchValidation validation) throws IllegalArgumentException;
	
	
	/**
	 * This method adds a validation to the repository for just one session. Validations added using this method will not be removed from the repository until the service is disposed.
	 * It is intended to be used by other plug-ins that require to load validations and protect them.
	 * 
	 * @param validation the new validation to be added to the repository.
	 * @throws IllegalArgumentException if the new validation is null or if it already exists in the repository.
	 */
	public void addSessionBatchValidation(IBatchValidation validation) throws IllegalArgumentException;


	/**
	 * This method removes a batch validation from the repository.
	 * 
	 * @param validationName the name of the validation that will be removed.
	 * @throws IllegalArgumentException if a null validation name is provided.
	 */
	public void removeBatchValidation(String validationName) throws IllegalArgumentException;


	/**
	 * This method updates a batch validation in the repository with the same name 
	 * as the one provided.
	 * 
	 * @param validation the updated batch validation.
	 * @throws IllegalArgumentException if the batch validation provided is null or if it doesn't exist in the repository.
	 */
	public void changeBatchValidation(IBatchValidation validation) throws IllegalArgumentException;


	/**
	 * This methods returns a copy of the constraint repository.
	 * 
	 * @return the cloned copy of the constraint repository. 
	 */
	public List<IModelValidatorConstraint> getConstraintRepository();
	

	/**
	 * This method returns a string with all the constraint categories defined in the constraint repository.
	 * 
	 * @return the categories
	 */
	public List<String> getConstraintCategories();

	
	/**
	 * This methods returns a copy of the batch validation with the selected name.
	 * @param name the name of the validation
	 * @return the batch validation object or null if it was not found
	 */
	public IBatchValidation getBatchValidation(String name);

	/**
	 * This methods returns a copy of the batch validation repository.
	 * @return the cloned copy of the batch validation repository
	 */
	public List<IBatchValidation> getBatchValidationRepository();


	/**
	 * This method runs a batch validation on a target and returns the result of the validation.
	 * 
	 * @param validationName the name of the validation to be run.
	 * @param targets the targeted {@link EObject}s in a list.
	 * @return the validation result.
	 * @throws ValidationEngineException if any exception should occur during validation.
	 * @throws IllegalTargetException if the validation cannot be run on the selected target.
	 * @throws IllegalArgumentException if a null argument is provided.
	 * @throws NoConstraintsValidatedException if the validation couldn't validate any constraints against the given targets.
	 */
	public IValidationResult runValidation(String validationName, List<EObject> targets) throws ValidationEngineException, IllegalTargetException, IllegalArgumentException, NoConstraintsValidatedException;


	/**
	 * This method returns the result of the last validation that was run by the service.
	 * 
	 * @return the result of the last validation
	 */
	public IValidationResult getLastValidationResult();
	
	/**
	 * Method that forces saving the repositories
	 * to the repository file
	 */
	public void saveRepositories();
}
