/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/

package es.esi.gemde.modelvalidator.service;


import es.esi.gemde.modelvalidator.exceptions.ConstraintCreationException;


/**
 * Interface that defines a constraint factory in GEMDE.
 * Implementations should be able to create {@link IModelValidatorConstraint}s from a series of strings defining the constraint.
 *
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 * @version 1.0
 * @since 1.0
 *
 */
public interface IConstraintFactory {
	
	/**
	 * This method creates a constraint from a textual specification.
	 * 
	 * @param name the name of the constraint.
	 * @param category the category of the constraint.
	 * @param severity the severity code of the constraint as defined in {@link org.eclipse.core.runtime.IStatus}
	 * @param body the textual specification of the constraint
	 * @param isProtected whether the created constraint will be session protected
	 * @return the implementation of the created constraint
	 * @throws IllegalArgumentException if the provided category or body parameters are null or if the provided severity value is invalid.
	 * @throws ConstraintCreationException if a problems occurs while creating the constraint.
	 * 
	 * @deprecated Use {@link IConstraintFactory#createConstraint(String, String, int, String)}, {@link IConstraintFactory#createProtectedConstraint(String, String, int, String)} and {@link IConstraintFactory#createNetworkedConstraint(String, String, int, String)} instead
	 */
	public IModelValidatorConstraint createConstraint(String name, String category, int severity, String body, boolean isProtected) throws IllegalArgumentException, ConstraintCreationException;
	
	/**
	 * This method creates a constraint from a textual specification.
	 * 
	 * @param name the name of the constraint.
	 * @param description the description of this constraint.
	 * @param category the category of the constraint.
	 * @param severity the severity code of the constraint as defined in {@link org.eclipse.core.runtime.IStatus}
	 * @param body the textual specification of the constraint
	 * @param isProtected whether the created constraint will be session protected
	 * @return the implementation of the created constraint
	 * @throws IllegalArgumentException if the provided category or body parameters are null or if the provided severity value is invalid.
	 * @throws ConstraintCreationException if a problems occurs while creating the constraint.
	 * @since 1.4.0
	 * 
	 * @deprecated Use {@link IConstraintFactory#createConstraint(String, String, int, String)}, {@link IConstraintFactory#createProtectedConstraint(String, String, int, String)} and {@link IConstraintFactory#createNetworkedConstraint(String, String, int, String)} instead
	 */
	public IModelValidatorConstraint createConstraint(String name, String description, String category, int severity, String body, boolean isProtected) throws IllegalArgumentException, ConstraintCreationException;
	
	/**
	 * This method creates a constraint from a textual specification.
	 * 
	 * @param name the name of the constraint.
	 * @param category the category of the constraint.
	 * @param severity the severity code of the constraint as defined in {@link org.eclipse.core.runtime.IStatus}
	 * @param body the textual specification of the constraint
	 * @return the implementation of the created constraint
	 * @throws IllegalArgumentException if the provided category or body parameters are null or if the provided severity value is invalid.
	 * @throws ConstraintCreationException if a problems occurs while creating the constraint.
	 */
	public IModelValidatorConstraint createConstraint(String name, String category, int severity, String body) throws IllegalArgumentException, ConstraintCreationException;
	
	/**
	 * This method creates a protected constraint from a textual specification.
	 * 
	 * @param name the name of the constraint.
	 * @param category the category of the constraint.
	 * @param severity the severity code of the constraint as defined in {@link org.eclipse.core.runtime.IStatus}
	 * @param body the textual specification of the constraint
	 * @return the implementation of the created constraint
	 * @throws IllegalArgumentException if the provided category or body parameters are null or if the provided severity value is invalid.
	 * @throws ConstraintCreationException if a problems occurs while creating the constraint.
	 * 
	 */
	public IModelValidatorConstraint createProtectedConstraint(String name, String category, int severity, String body) throws IllegalArgumentException, ConstraintCreationException;
	
	/**
	 * This method creates a networked constraint from a textual specification.
	 * 
	 * @param name the name of the constraint.
	 * @param category the category of the constraint.
	 * @param severity the severity code of the constraint as defined in {@link org.eclipse.core.runtime.IStatus}
	 * @param body the textual specification of the constraint
	 * @return the implementation of the created constraint
	 * @throws IllegalArgumentException if the provided category or body parameters are null or if the provided severity value is invalid.
	 * @throws ConstraintCreationException if a problems occurs while creating the constraint.
	 */
	public IModelValidatorConstraint createNetworkedConstraint(String name, String category, int severity, String body) throws IllegalArgumentException, ConstraintCreationException;

}
