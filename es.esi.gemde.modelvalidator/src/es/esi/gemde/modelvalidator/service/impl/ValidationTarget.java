/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.modelvalidator.service.impl;

import org.eclipse.emf.ecore.EObject;

import es.esi.gemde.modelvalidator.service.IModelValidatorConstraint;
import es.esi.gemde.modelvalidator.service.IValidationTarget;

/**
 * Implementation of the {@link IValidationTarget} interface.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class ValidationTarget implements IValidationTarget {

	private IModelValidatorConstraint constraint;
	private EObject target;
	
	/**
	 * Constructor of the class.
	 *
	 *@param constraint the constraint implementation of this validation pair.
	 *@param target the target of this validation pair.
	 */
	public ValidationTarget (IModelValidatorConstraint constraint, EObject target) {
		this.constraint = constraint;
		this.target = target;
	}
	
	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IValidationTarget#getConstraint()
	 */
	@Override
	public IModelValidatorConstraint getConstraint() {
		return constraint;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IValidationTarget#getTarget()
	 */
	@Override
	public EObject getTarget() {
		return target;
	}

}
