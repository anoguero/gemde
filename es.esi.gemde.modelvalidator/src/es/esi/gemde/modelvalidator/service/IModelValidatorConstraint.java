/*******************************************************************************
 * Copyright (c) 2010, 2012 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/

package es.esi.gemde.modelvalidator.service;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import es.esi.gemde.core.resources.IGemdeResource;


/**
 * The interface that defines a constraint in GEMDE.
 *
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 * @version 1.0
 * @since 1.0.0
 *
 */
public interface IModelValidatorConstraint extends IGemdeResource {
	
	/**
	 * This method validates this constraint against a single target
	 * 
	 * @param onTarget the target {@link EObject} on which the constraint will be validated.
	 * @return the result of the validation.
	 */
	public IStatus validate(EObject onTarget);
	
	/**
	 * Returns the name of this constraint.
	 * 
	 * @return the name of the constraint.
	 */
	public String getName();
	
	/**
	 * Returns the category of this constraint.
	 * 
	 * @return the category of the constraint.
	 */
	public String getCategory();
	
	/**
	 * Returns the severity of this constraint as defined in {@link IStatus}.
	 * 
	 * @return the severity of the constraint.
	 */
	public int getSeverity();
	
	/**
	 * Returns the text string that defines this constraint.
	 * 
	 * @return the text definition of the constraint body.
	 */
	public String getBody();
	
	/**
	 * Checks if this constraint can target an element. 
	 * @param target the {@link EObject} element we want to check
	 * @return true if the constraint can be applied on the provided target or false otherwise.
	 */
	public boolean targetsTypeOf(EObject target);
	
	/**
	 * Returns the instance, as an {@link EPackage}, of the metamodel to which this constraint is applied.
	 *  
	 * @return the metamodel instance.
	 */
	public EPackage getApplicationMetamodel();
	
	/**
	 * Returns the instance, as an {@link EClass}, of the object to which this constraint is applied.
	 * 
	 * @return the EClass instance.
	 */
	public EClass getApplicationContext();
	
	/**
	 * Checks whether this constraint is session protected.
	 * 
	 * @return true is this constraint is protected. False otherwise.
	 * @deprecated Use {@link IGemdeResource#getResourceType()} instead.
	 */
	public boolean isProtected();
	
	/**
	 * Returns the Description of the current constraint
	 * 
	 * @return the description String.
	 * @since 1.4.0
	 */
	public String getDescription();
	
	/**
	 * Returns the unique identifier of the current constraint.
	 * 
	 * @return an String containing the identifier.
	 * @since 1.4.0
	 */
	public String getIdentifier();
	
}
