/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/

package es.esi.gemde.modelvalidator.service;

import java.util.List;

import org.eclipse.emf.ecore.EPackage;

import es.esi.gemde.core.resources.IGemdeResource.ResourceType;
import es.esi.gemde.modelvalidator.service.impl.BatchValidation;


/**
 * A factory of IBatchValidation instances.
 *
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 * @version 1.0
 * @since 1.0
 *
 */
public class BatchValidationFactory {
	
	/**
	 * A static method to create an instance of an IBatchValidation. The creation process asserts that the validation is runnable.
	 * 
	 * @param name the name of the IBatchValidation instance.
	 * @param selectedConstraints the selected constraint list that will be referenced by the created validation instance.
	 * @param metamodel the metamodel to which the constraints in this validation refer.
	 * @return the created instance.
	 * @throws IllegalArgumentException see {@link IBatchValidation}
	 */
	public static IBatchValidation createBatchValidation (String name, List<IModelValidatorConstraint> selectedConstraints, EPackage metamodel) throws IllegalArgumentException {
		for (IModelValidatorConstraint constraint : selectedConstraints) {
			if (!constraint.getApplicationMetamodel().equals(metamodel)) {
				throw new IllegalArgumentException("Constraint " + constraint.getName() + " does not refer to metamodel " + metamodel.getName());
			}
		}
		return new BatchValidation(name, selectedConstraints, ResourceType.NORMAL);
	}
	
	/**
	 * A static method to create a session protected instance of an IBatchValidation. The creation process asserts that the validation is runnable.
	 * 
	 * @param name the name of the IBatchValidation instance.
	 * @param selectedConstraints the selected constraint list that will be referenced by the created validation instance.
	 * @param metamodel the metamodel to which the constraints in this validation refer.
	 * @return the created instance.
	 * @throws IllegalArgumentException see {@link IBatchValidation}
	 */
	public static IBatchValidation createProtectedBatchValidation (String name, List<IModelValidatorConstraint> selectedConstraints, EPackage metamodel) throws IllegalArgumentException {
		for (IModelValidatorConstraint constraint : selectedConstraints) {
			if (!constraint.getApplicationMetamodel().equals(metamodel)) {
				throw new IllegalArgumentException("Constraint " + constraint.getName() + " does not refer to metamodel " + metamodel.getName());
			}
		}
		return new BatchValidation(name, selectedConstraints, ResourceType.PROTECTED);
	}
	
	/**
	 * A static method to create an instance of an IBatchValidation. The creation process asserts that the validation is runnable.
	 * 
	 * @param id the identifier of the IBatchValidation instance.
	 * @param name the name of the IBatchValidation instance.
	 * @param description a meaningful instance of the IBatchValidation instance.
	 * @param selectedConstraints the selected constraint list that will be referenced by the created validation instance.
	 * @param metamodel the metamodel to which the constraints in this validation refer.
	 * @return the created instance.
	 * @throws IllegalArgumentException see {@link IBatchValidation}
	 */
	public static IBatchValidation createBatchValidation (String id, String name, String description, List<IModelValidatorConstraint> selectedConstraints, EPackage metamodel) throws IllegalArgumentException {
		for (IModelValidatorConstraint constraint : selectedConstraints) {
			if (!constraint.getApplicationMetamodel().equals(metamodel)) {
				throw new IllegalArgumentException("Constraint " + constraint.getName() + " does not refer to metamodel " + metamodel.getName());
			}
		}
		return new BatchValidation(id, name, description, selectedConstraints, ResourceType.NORMAL);
	}
	
	/**
	 * A static method to create a session protected instance of an IBatchValidation. The creation process asserts that the validation is runnable.
	 * 
	 * @param id the identifier of the IBatchValidation instance.
	 * @param name the name of the IBatchValidation instance.
	 * @param description a meaningful instance of the IBatchValidation instance.
	 * @param selectedConstraints the selected constraint list that will be referenced by the created validation instance.
	 * @param metamodel the metamodel to which the constraints in this validation refer.
	 * @return the created instance.
	 * @throws IllegalArgumentException see {@link IBatchValidation}
	 */
	public static IBatchValidation createProtectedBatchValidation (String id, String name, String description, List<IModelValidatorConstraint> selectedConstraints, EPackage metamodel) throws IllegalArgumentException {
		for (IModelValidatorConstraint constraint : selectedConstraints) {
			if (!constraint.getApplicationMetamodel().equals(metamodel)) {
				throw new IllegalArgumentException("Constraint " + constraint.getName() + " does not refer to metamodel " + metamodel.getName());
			}
		}
		return new BatchValidation(id, name, description, selectedConstraints, ResourceType.PROTECTED);
	}

	/**
	 * A static method to create an instance of a networked IBatchValidation. The creation process asserts that the validation is runnable.
	 * 
	 * @param name the name of the IBatchValidation instance.
	 * @param selectedConstraints the selected constraint list that will be referenced by the created validation instance.
	 * @param metamodel the metamodel to which the constraints in this validation refer.
	 * @return the created instance.
	 * @throws IllegalArgumentException see {@link IBatchValidation}
	 */
	public static IBatchValidation createNetworkedBatchValidation (String name, List<IModelValidatorConstraint> selectedConstraints, EPackage metamodel) throws IllegalArgumentException {
		for (IModelValidatorConstraint constraint : selectedConstraints) {
			if (!constraint.getApplicationMetamodel().equals(metamodel)) {
				throw new IllegalArgumentException("Constraint " + constraint.getName() + " does not refer to metamodel " + metamodel.getName());
			}
		}
		return new BatchValidation(name, selectedConstraints, ResourceType.NETWORKED);
	}

	/**
	 * A static method to create an instance of a networked IBatchValidation. The creation process asserts that the validation is runnable.
	 * 
	 * @param id the identifier of the IBatchValidation instance.
	 * @param name the name of the IBatchValidation instance.
	 * @param description a meaningful instance of the IBatchValidation instance.
	 * @param selectedConstraints the selected constraint list that will be referenced by the created validation instance.
	 * @param metamodel the metamodel to which the constraints in this validation refer.
	 * @return the created instance.
	 * @throws IllegalArgumentException see {@link IBatchValidation}
	 */
	public static IBatchValidation createNetworkedBatchValidation (String id, String name, String description, List<IModelValidatorConstraint> selectedConstraints, EPackage metamodel) throws IllegalArgumentException {
		for (IModelValidatorConstraint constraint : selectedConstraints) {
			if (!constraint.getApplicationMetamodel().equals(metamodel)) {
				throw new IllegalArgumentException("Constraint " + constraint.getName() + " does not refer to metamodel " + metamodel.getName());
			}
		}
		return new BatchValidation(id, name, description, selectedConstraints, ResourceType.NETWORKED);
	}
	
}
