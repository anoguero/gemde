/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.modelvalidator.service;

import org.eclipse.emf.ecore.EObject;

/**
 * A wrapper interface for validation constraint/target pairs.
 * It is used by IValidationResult to provide successful and failed pairs.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public interface IValidationTarget {

	/**
	 * Returns the constraint of this pair.
	 * 
	 * @return the constraint implementation.
	 */
	public IModelValidatorConstraint getConstraint();
	
	/**
	 * Returns the target of this pair.
	 * 
	 * @return the targeted EObject element.
	 */
	public EObject getTarget();
	
}
