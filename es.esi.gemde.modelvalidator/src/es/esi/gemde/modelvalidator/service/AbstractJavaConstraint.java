/*******************************************************************************
 * Copyright (c) 2010, 2012 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adrian Noguero (adrian.noguero@tecnalia.com)
 *     
 *******************************************************************************/
package es.esi.gemde.modelvalidator.service;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

/**
 * Partial implementation of the {@link IModelValidatorConstraint} interface.
 * Intended for users that want to contribute Java constraints to GEMDE.
 * 
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 *
 */
public abstract class AbstractJavaConstraint implements
		IModelValidatorConstraint {
	
	private String name;
	private String description;
	private String category;
	private EPackage metamodel;
	private EClass context;
	private String id;
	private int severity;
	
	private static int counter = 0;
	
	/**
	 * 
	 * Constructor of the class with all the parameters that define a constraint in GEMDE.
	 * 
	 * @param id. The String that identifies this constraint.
	 * @param name. The name of the constraint.
	 * @param category. The category of this constraint as a String.
	 * @param severity. The severity value of this constraint.
	 * @param metamodel. The metamodel of the EClass the constraint affects to.
	 * @param context. The metaclass to which the constraint will be applied.
	 * @param description. A description for the constraint.
	 * @throws IllegalArgumentException if a non-valid severity value is provided.
	 */
	public AbstractJavaConstraint(String id, String name, String category, int severity, EPackage metamodel, EClass context, String description) throws IllegalArgumentException {
		if (severity != IStatus.ERROR && severity != IStatus.WARNING && severity != IStatus.INFO) {
			throw new IllegalArgumentException("Invalid severity was provided");
		}
		this.id = id;
		this.name = name;
		this.description = description;
		this.category = category;
		this.severity = severity;
		this.metamodel = metamodel;
		this.context = context;
	}
	
	/**
	 * Simplified constructor of the class.
	 * 
	 * @param name. The name of the constraint.
	 * @param category. The category of this constraint as a String.
	 * @param severity. The severity value of this constraint.
	 * @param metamodel. The metamodel of the EClass the constraint affects to.
	 * @param context. The metaclass to which the constraint will be applied.
	 * @throws IllegalArgumentException if a non-valid severity value is provided.
	 */
	public AbstractJavaConstraint(String name, String category, int severity, EPackage metamodel, EClass context) throws IllegalArgumentException {
		this("Java_" + counter, name, category, severity, metamodel, context, new String());
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IModelValidatorConstraint#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IModelValidatorConstraint#getCategory()
	 */
	@Override
	public String getCategory() {
		return category;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IModelValidatorConstraint#getSeverity()
	 */
	@Override
	public int getSeverity() {
		return severity;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IModelValidatorConstraint#getBody()
	 */
	@Override
	public String getBody() {
		return "Java constraint";
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IModelValidatorConstraint#targetsTypeOf(org.eclipse.emf.ecore.EObject)
	 */
	@Override
	public boolean targetsTypeOf(EObject target) {
		return context.isInstance(target);
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IModelValidatorConstraint#getApplicationMetamodel()
	 */
	@Override
	public EPackage getApplicationMetamodel() {
		return metamodel;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IModelValidatorConstraint#getApplicationContext()
	 */
	@Override
	public EClass getApplicationContext() {
		return context;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IModelValidatorConstraint#isProtected()
	 */
	@Override
	public boolean isProtected() {
		return true;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IModelValidatorConstraint#getDescription()
	 */
	@Override
	public String getDescription() {
		return description;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IModelValidatorConstraint#getIdentifier()
	 */
	@Override
	public String getIdentifier() {
		return id;
	}

}
