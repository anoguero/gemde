/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/

package es.esi.gemde.modelvalidator.service;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import es.esi.gemde.core.resources.IGemdeResource;
import es.esi.gemde.modelvalidator.exceptions.IllegalTargetException;
import es.esi.gemde.modelvalidator.exceptions.NoConstraintsValidatedException;
import es.esi.gemde.modelvalidator.exceptions.ValidationEngineException;


/**
 * Interface defining a Batch Validation in GEMDE
 *
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 * @version 1.0
 * @since 1.0
 *
 */
public interface IBatchValidation extends IGemdeResource {
	
	/**
	 * Returns the textual name if this validation.
	 * 
	 * @return the name of this validation
	 */
	public String getName();
	
	/**
	 * Returns a list with the constraints selected for this validation.
	 * 
	 * @return the selected constraints list
	 */
	public List<IModelValidatorConstraint> getSelectedConstraints();
	
	/**
	 * This method executes the current validation on the provided targets.
	 * 
	 * @param onTargets the targeted {@link EObject}s.
	 * @return the result of the validation.
	 * @throws IllegalArgumentException if the onTargets list is null, is empty, or if the targets refer to different metamodels.
	 * @throws IllegalTargetException if any of the constraints of the validation can't target the selected object. 
	 * @throws ValidationEngineException if any exception occurs during the validation process.
	 * @throws NoConstraintsValidatedException if no constraints could be validated against the given targets.
	 */
	public IValidationResult execute(List<EObject> onTargets) throws IllegalArgumentException, IllegalTargetException, ValidationEngineException, NoConstraintsValidatedException;
	
	/**
	 * Checks whether this validation is session protected.
	 * 
	 * @return true if the validation is protected. False otherwise.
	 * @deprecated Use {@link IGemdeResource#getResourceType()} instead.
	 */
	public boolean isProtected();
	
	/**
	 * Returns the Description of the current validation
	 * 
	 * @return the description String.
	 * @since 1.4.0
	 */
	public String getDescription();
	
	/**
	 * Returns the unique identifier of the current validation.
	 * 
	 * @return an String containing the identifier.
	 * @since 1.4.0
	 */
	public String getIdentifier();

}
