/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/

package es.esi.gemde.modelvalidator.service;

import java.util.List;

import org.eclipse.core.runtime.MultiStatus;


/**
 * The interface which defines the results of the execution of a batch 
 * validation in GEMDE
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public interface IValidationResult {
	

	/**
	 * This method returns the global status of the batch validation according to {@link org.eclipse.core.runtime.IStatus}.
	 * 
	 * @return the integer status value.
	 */
	public int getStatusCode ();
	
	/**
	 * This method returns the list of pairs constraint/target that were successfully parsed be the batch validation process.
	 * 
	 * @return the successful constraint list.
	 */
	public List<IValidationTarget> getSuccessfulConstraints();
	
	/**
	 * This method returns the list of pairs constraint/target that reported errors/warnings during the batch validation process.
	 * 
	 * @return the list of erroneous constraints
	 */
	public List<IValidationTarget> getFailedConstraints();
	
	/**
	 * This method returns the list of messages generated during the batch validation process.
	 * 
	 * @return an array of text strings containing the batch validation messages.
	 */
	public String [] getMessages();
	
	/**
	 * Returns the error information in this IValidationResult as a MultiStatus instance
	 * to be used with the ErrorDialog eclipse class.
	 * 
	 * @return the converted MultiStatus instance of this result.
	 */
	public MultiStatus asMultiStatus();
	
}
