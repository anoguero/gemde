/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/

package es.esi.gemde.modelvalidator.exceptions;

/**
 * An exception raised by the Validation Service if a validation routine couldn't validate any constraints against the given targets.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
@SuppressWarnings("serial")
public class NoConstraintsValidatedException extends Exception {

	/**
	 * Constructor of the class including a message.
	 *
	 *@param message the text message to be attached to the exception.
	 */
	public NoConstraintsValidatedException(String message) {
		super(message);
	}
	
}
