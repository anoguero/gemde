/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/

package es.esi.gemde.modelvalidator.exceptions;


/**
 * An exception thrown by {@link es.esi.gemde.modelvalidator.service.IConstraintFactory#createConstraint(String, String, int, String)} if a constraint is not created correctly.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
@SuppressWarnings("serial")
public class ConstraintCreationException extends Exception {

	
	/**
	 * A constructor including an explanation text message.
	 *
	 *@param message the explanation text.
	 */
	public ConstraintCreationException(String message) {
		super(message);
	}
	
}
