/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/

package es.esi.gemde.modelvalidator.exceptions;


/**
 * An exception thrown by {@link es.esi.gemde.modelvalidator.service.IModelValidatorService#runValidation(String, org.eclipse.emf.ecore.EObject)}
 * This exception is a wrapper for any kind of exception that could be caught
 * during a validation.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
@SuppressWarnings("serial")
public class ValidationEngineException extends Exception {

	/**
	 * A constructor with a parameter describing the Exception that raised
	 * this ValidationEngineException.
	 *
	 *@param e the exception that raised this ValidationEngineException
	 */
	public ValidationEngineException(Exception e) {
		super(e);
	}
}
