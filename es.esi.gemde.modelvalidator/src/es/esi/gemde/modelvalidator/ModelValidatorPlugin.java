/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/

package es.esi.gemde.modelvalidator;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import es.esi.gemde.modelvalidator.service.IModelValidatorService;
import es.esi.gemde.modelvalidator.service.impl.ModelValidatorService;



/**
 * This class implements the {@link AbstractUIPlugin} class and holds a copy
 * of the validation service instance.
 * The class includes some constants and a the static method {@link #getImage(String, String, int)}
 * used by UI classes in the plug-in to access icons.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class ModelValidatorPlugin extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "es.esi.gemde.modelvalidator";
	

	// The shared instance
	private static ModelValidatorPlugin plugin;
	
	private static ModelValidatorPlugin instance = new ModelValidatorPlugin();
	
	// The shared instance of the service
	private static ModelValidatorService service;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = instance;
		service = (ModelValidatorService) ModelValidatorService.getInstance();
		
		service.loadExtensions();
		service.initializeRepositories();
		
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		service.saveRepositories();
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static ModelValidatorPlugin getDefault() { 
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	/**
	 * Returns the instance of the Model Validation Service currently running.
	 * 
	 * @return the server instance
	 */
	public static IModelValidatorService getServer () {
		return service;
	}
	
	
	
}
