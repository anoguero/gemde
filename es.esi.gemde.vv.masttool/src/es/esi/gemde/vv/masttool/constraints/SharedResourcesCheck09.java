/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.masttool.constraints;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import es.esi.gemde.modelvalidator.service.IModelValidatorConstraint;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Regular_Transaction;

/**
 * Java implementation of the MAST Shared Resource Check 9
 *
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 * @version 1.0
 * @since 1.0
 *
 */
public class SharedResourcesCheck09 implements IModelValidatorConstraint {

	// Constants
	private static final String CATEGORY = "MAST Shared Resources Checks";
	private static final String NAME = "09 - Distributed systems scheduled under EDF may not use shared resources";
	
	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IModelValidatorConstraint#getApplicationContext()
	 */
	@Override
	public EClass getApplicationContext() {
		return ModelPackage.eINSTANCE.getRegular_Transaction();
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IModelValidatorConstraint#getApplicationMetamodel()
	 */
	@Override
	public EPackage getApplicationMetamodel() {
		return ModelPackage.eINSTANCE.getRegular_Transaction().getEPackage();
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IModelValidatorConstraint#getBody()
	 */
	@Override
	public String getBody() {
		// Not used
		return "Constraint implemented in Java";
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IModelValidatorConstraint#getCategory()
	 */
	@Override
	public String getCategory() {
		return CATEGORY;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IModelValidatorConstraint#getName()
	 */
	@Override
	public String getName() {
		return NAME;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IModelValidatorConstraint#getSeverity()
	 */
	@Override
	public int getSeverity() {
		return IStatus.ERROR;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IModelValidatorConstraint#isProtected()
	 */
	@Override
	public boolean isProtected() {
		return true;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IModelValidatorConstraint#targetsTypeOf(org.eclipse.emf.ecore.EObject)
	 */
	@Override
	public boolean targetsTypeOf(EObject target) {
		return (target instanceof Regular_Transaction);
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modelvalidator.service.IModelValidatorConstraint#validate(org.eclipse.emf.ecore.EObject)
	 */
	@Override
	public IStatus validate(EObject onTarget) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getIdentifier() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResourceType getResourceType() {
		return ResourceType.PROTECTED;
	}

}
