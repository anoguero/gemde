/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.masttool.operations;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Vector;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import es.esi.gemde.vv.mast.mastmodel.MAST_MODEL;
import es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION;
import es.esi.gemde.vv.masttool.MastToolPlugin;
import es.esi.gemde.vv.masttool.exceptions.MASTNotFoundException;
import es.esi.gemde.vv.masttool.resources.MASTRunner;
import es.esi.gemde.vv.masttool.resources.utils.MASTModelingUtils;
import es.esi.gemde.vv.modelanalyzer.exceptions.ToolException;
import es.esi.gemde.vv.modelanalyzer.service.AbstractToolOperation;
import es.esi.gemde.vv.modelanalyzer.service.IAnalysisResult;

public class HolisticFixedPriorityAnalysisOperation extends
		AbstractToolOperation {

	private static final String NAME = "Holistic Fixed Priority Analysis";
	private static final String DESCRIPTION = "";
	
	public HolisticFixedPriorityAnalysisOperation() {
		super(NAME, DESCRIPTION);
		addInput("MAST model", MAST_MODEL.class);
		addInput("Calculate Priorities?", Boolean.class);
		addInput("Calculate Ceilings?", Boolean.class);
		addInput("Calculate Slacks?", Boolean.class);
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IToolOperation#runOperation(org.eclipse.emf.ecore.EObject[])
	 */
	@Override
	public IAnalysisResult runOperation(Object[] inputs) throws ToolException {
		// Check input list
		if (inputs == null || inputs.length != 4) {
			throw new IllegalArgumentException("Wrong number of inputs was provided");
		}
		
		// Create the temporary folder if required
		MastToolPlugin.createDir();
		
		// Save the model to the temporary folder
		MAST_MODEL model = (MAST_MODEL) inputs[0];
		File input = ResourcesPlugin.getWorkspace().getRoot().getLocation().append(".metadata/.plugins/" + MastToolPlugin.PLUGIN_ID + "/"+ model.getModel_Name() +".xml").toFile();
		try {
			FileOutputStream fos = new FileOutputStream(input);
			model.eResource().save(fos, null);
			fos.close();
		}
		catch (IOException e) {
			throw new ToolException(e);
		}
		
		File outModel = null;
		
		// Parse analysis parameters
		Vector<String> opts = new Vector<String>();
		if ((Boolean)inputs[1] == true) {
			opts.add(MASTRunner.PARAMETERS);
			opts.add(MASTRunner.TECHNIQUE);
			opts.add(MASTRunner.MONOPROCESSOR);
		}
		if ((Boolean)inputs[2] == true) {
			opts.add(MASTRunner.CEILINGS);
		}
		if ((Boolean)inputs[3] == true) {
			opts.add(MASTRunner.SYSTEM_SLACK);
		}
		
		
		// Run the tool
		try {
			outModel = MASTRunner.runMAST(MASTRunner.HOLISTIC, opts.toArray(new String[0]), input);
		} catch (IllegalArgumentException e) {
			throw new ToolException(e);
		} catch (MASTNotFoundException e) {
			throw new ToolException(e);
		}
		
		// Something went wrong...
		if (outModel == null) {
			throw new ToolException(new Exception("Error occurred running MAST"));
		}
		
		// Load the model from the file
		ResourceSet rs = new ResourceSetImpl();
		URI modelURI = URI.createFileURI(outModel.getAbsolutePath());
		Resource resource = null;
		try {
			resource = rs.getResource(modelURI, true);
		}
		catch (Exception ex) {
			throw new ToolException(ex);
		}
		
		if (resource == null) {
			throw new ToolException(new Exception("Error occurred running MAST -> couldn't read results model"));
		}
		
		// Get the REAL_TIME_SITUATION instance
		REAL_TIME_SITUATION result = null;
		boolean found = false;
		TreeIterator<EObject> it = resource.getAllContents();
		while (!found && it.hasNext()) {
			EObject curr = it.next();
			if (curr instanceof REAL_TIME_SITUATION) {
				result = (REAL_TIME_SITUATION)curr;
				found = true;
			}
		}
		
		if (result == null) {
			throw new ToolException(new Exception("Error occurred running MAST -> erroneous results model was generated"));
		}
		
		// Clean-up the temporary files and generate an IAnalysisResult 
		// instance from the MAST results model.
		MastToolPlugin.cleanupDir();
		return MASTModelingUtils.createResultsFromModel(result);
	}

}
