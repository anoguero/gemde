/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.masttool.operations;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import es.esi.gemde.vv.modelanalyzer.exceptions.ToolException;
import es.esi.gemde.vv.modelanalyzer.service.AbstractToolOperation;
import es.esi.gemde.vv.modelanalyzer.service.IAnalysisResult;

public class EDFSecondaryPolicyAnalysisOperation extends AbstractToolOperation {

	private static final String NAME = "EDF as Secondary Policy Analysis";
	private static final String DESCRIPTION = "";
	
	public EDFSecondaryPolicyAnalysisOperation() {
		super(NAME, DESCRIPTION);
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IToolOperation#runOperation(org.eclipse.emf.ecore.EObject[])
	 */
	@Override
	public IAnalysisResult runOperation(Object[] inputs) throws ToolException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getRequiredInputNames() {
		// TODO Auto-generated method stub
		return null;
	}

}
