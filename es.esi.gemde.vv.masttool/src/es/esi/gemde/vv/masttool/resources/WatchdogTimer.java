/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.masttool.resources;

/**
 * A class implementing a watchdog timer. The timer controls that a
 * certain process does not execute forever.
 * 
 * This WatchdogTimer class extends {@link Thread}; and its is therefore a 
 * special kind of runnable.
 * 
 * It is used as follows:
 * 1st - A watchdog instance is created.
 * 2nd - The watchdog is started using the start() method. See {@link Thread#start()} for details.
 * 3rd - To stop the timer the interrupt() method is used. See {@link Thread#interrupt()} for details.
 * 4th - After the timer has been used we may check its state using {@link WatchdogTimer#wasTrigerred()}
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class WatchdogTimer extends Thread {

	private Process p;
	private int time;
	private boolean triggered;
	
	/**
	 * Constructor of the watchdog timer
	 *
	 *@param p the process that will be controlled by the timer.
	 *@param millis the number of milliseconds elapsed before the timer expires.
	 */
	public WatchdogTimer (Process p, int millis) {
		this.p = p;
		this.time = millis;
		triggered = false;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	@Override
	public void run() {
		triggered = false;
		try {
			sleep(time);
			triggered = true;
			p.destroy();
		} catch (InterruptedException e) {
			// The watchdog has been interrupted
		}
		
	}
	
	/**
	 * Returns whether this watchdog has expired in its last execution
	 * 
	 * @return true if the watchdog expired; false otherwise.
	 */
	public boolean wasTrigerred() {
		return triggered;
	}

	
	
}
