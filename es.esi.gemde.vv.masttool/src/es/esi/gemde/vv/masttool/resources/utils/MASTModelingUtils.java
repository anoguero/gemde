/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.masttool.resources.utils;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;

import es.esi.gemde.vv.mast.mastmodel.Activity;
import es.esi.gemde.vv.mast.mastmodel.Barrier;
import es.esi.gemde.vv.mast.mastmodel.Composite_Operation;
import es.esi.gemde.vv.mast.mastmodel.Concentrator;
import es.esi.gemde.vv.mast.mastmodel.Delay;
import es.esi.gemde.vv.mast.mastmodel.Delivery_Server;
import es.esi.gemde.vv.mast.mastmodel.Enclosing_Operation;
import es.esi.gemde.vv.mast.mastmodel.Immediate_Ceiling_Resource;
import es.esi.gemde.vv.mast.mastmodel.MAST_MODEL;
import es.esi.gemde.vv.mast.mastmodel.Multicast;
import es.esi.gemde.vv.mast.mastmodel.Offset;
import es.esi.gemde.vv.mast.mastmodel.Priority_Inheritance_Resource;
import es.esi.gemde.vv.mast.mastmodel.Query_Server;
import es.esi.gemde.vv.mast.mastmodel.Rate_Divisor;
import es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server;
import es.esi.gemde.vv.mast.mastmodel.Regular_Transaction;
import es.esi.gemde.vv.mast.mastmodel.SRP_Resource;
import es.esi.gemde.vv.mast.mastmodel.Simple_Operation;
import es.esi.gemde.vv.mast.mastresult.Global_Response_Time;
import es.esi.gemde.vv.mast.mastresult.Operation_Results;
import es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results;
import es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION;
import es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results;
import es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results;
import es.esi.gemde.vv.mast.mastresult.Timing_Result;
import es.esi.gemde.vv.mast.mastresult.Transaction_Results;
import es.esi.gemde.vv.masttool.resources.TreeDataItem;
import es.esi.gemde.vv.masttool.resources.TreeList;
import es.esi.gemde.vv.modelanalyzer.service.IAnalysisResult;
import es.esi.gemde.vv.modelanalyzer.service.ResultFactory;

/**
 * Utility class with static methods that help managing MAST models.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class MASTModelingUtils {

	/**
	 * Internal method used to create the transaction graph tree for a concrete external event.
	 * 
	 * @param event the name of the external event for which
	 * @param t the transaction object
	 * @return true if the tree is created without circular dependencies. False otherwise.
	 */
	public static TreeList<Object> createTree(String event, Regular_Transaction t) {
		TreeList<Object> tree = new TreeList<Object>();
		
		if (!createNode(event, t, tree.getTreeRoot())) {
			return null;
		}
		
		return tree;
	}

	/**
	 * Internal recursive method that creates a node in the tree given an input event, a transaction and the parent node.
	 * If no nodes follow the given input event, then no nodes are created.
	 * Additionally, if a circular dependency is detected, the method returns false. 
	 * 
	 * @param event the name of the input event that will generate this node.
	 * @param t the transaction object.
	 * @param parent the parent tree element from which we will create the node.
	 * @return true false is a circular dependency is detected while creating the node. True otherwise, even if the node couldn't be created.
	 */
	public static boolean createNode(String event, Regular_Transaction t, TreeDataItem<Object> parent) {
		Object node = getNextNode(event, t);
		if (node != null) {
			TreeDataItem<Object> child = parent.createChild(node);
			if (child.getAllParentsData().contains(node)) {
				return false;
			}
			for (String o : getOutputEvents(node)) {
				if (!createNode(o, t, child)) {
					return false;
				}
			}
		}
		
		return true;
	}

	/**
	 * Internal method that retrieves the next node for the given event name in the context of the given transaction.
	 * 
	 * @param event the name of the input event.
	 * @param t the context transaction
	 * @return the next node of the transaction graph or null if no follow-up node is found.
	 */
	public static Object getNextNode(String event, Regular_Transaction t) {
		Object nextNode = null;
		
		for (Activity a : t.getActivity()) {
			if (a.getInput_Event().equals(event)) {
				nextNode = a;
			}
		}
		
		for (Rate_Divisor a : t.getRate_Divisor()) {
			if (a.getInput_Event().equals(event)) {
				nextNode = a;
			}
		}
		
		for (Delay a : t.getDelay()) {
			if (a.getInput_Event().equals(event)) {
				nextNode = a;
			}
		}
		
		for (Offset a : t.getOffset()) {
			if (a.getInput_Event().equals(event)) {
				nextNode = a;
			}
		}
		
		for (Multicast a : t.getMulticast()) {
			if (a.getInput_Event().equals(event)) {
				nextNode = a;
			}
		}
		
		for (Delivery_Server a : t.getDelivery_Server()) {
			if (a.getInput_Event().equals(event)) {
				nextNode = a;
			}
		}
		
		for (Query_Server a : t.getQuery_Server()) {
			if (a.getInput_Event().equals(event)) {
				nextNode = a;
			}
		}
		
		for (Concentrator a : t.getConcentrator()) {
			if (a.getInput_Events_List().contains(event)) {
				nextNode = a;
			}
		}
		
		for (Barrier a : t.getBarrier()) {
			if (a.getInput_Events_List().contains(event)) {
				nextNode = a;
			}
		}
		
		return nextNode;
	}

	/**
	 * Internal method that retrieves the list of output events for a given node.
	 * 
	 * @param node the node implementation.
	 * @return an array with the names of all the output events for that node.
	 */
	public static String [] getOutputEvents(Object node) {
		
		if (node instanceof Activity) {
			String [] result = {((Activity)node).getOutput_Event()}; 
			return result;
		}
		else if (node instanceof Rate_Divisor) {
			String [] result = {((Rate_Divisor)node).getOutput_Event()}; 
			return result;
		}
		else if (node instanceof Delay) {
			String [] result = {((Delay)node).getOutput_Event()}; 
			return result;
		}
		else if (node instanceof Offset) {
			String [] result = {((Offset)node).getOutput_Event()}; 
			return result;
		}
		else if (node instanceof Concentrator) {
			String [] result = {((Concentrator)node).getOutput_Event()}; 
			return result;
		}
		else if (node instanceof Barrier) {
			String [] result = {((Barrier)node).getOutput_Event()}; 
			return result;
		}
		else if (node instanceof Query_Server) {
			String [] result = ((Query_Server)node).getOutput_Events_List().toArray(new String[0]); 
			return result;
		}
		else if (node instanceof Delivery_Server) {
			String [] result = ((Delivery_Server)node).getOutput_Events_List().toArray(new String[0]); 
			return result;
		}
		else if (node instanceof Multicast) {
			String [] result = ((Multicast)node).getOutput_Events_List().toArray(new String[0]); 
			return result;	
		}
		
		return null;
	}
	
	/**
	 * Returns the list of {@link Simple_Operation} composing an {@link Enclosing_Operation} or a {@link Composite_Operation}
	 * 
	 * @param model the model instance from which operations will be taken
	 * @param op the Composite or Enclosing operation instance.
	 * @return a list containing the simple operations referred by the provided composite operation.
	 */
	public static List<Simple_Operation> getComposingOps (MAST_MODEL model, Object op) {
		Vector<Simple_Operation> result = new Vector<Simple_Operation>();
		List<String> ops;
		if (op instanceof Enclosing_Operation) {
			ops = ((Enclosing_Operation)op).getOperation_List();
		}
		else if (op instanceof Composite_Operation) {
			ops = ((Composite_Operation)op).getOperation_List();
		}
		else {
			return null;
		}
		
		for (String name : ops) {
			Object simple = getModelOperation(name, model);
			if (simple instanceof Simple_Operation) {
				result.add((Simple_Operation)simple);
			}
			else {
				if (simple != null) {
					result.addAll(getComposingOps(model, ops));
				}
				
			}
		}
		return result;
	}

	/**
	 * Returns all the paths from a transaction graph.
	 * 
	 * @param tree the transaction graph as a TreeList object
	 * @return a bidimensional array containing the nodes present in each path.
	 */
	public static Object [] [] getPaths(TreeList<Object> tree) {
		List<TreeDataItem<Object>> leafs = tree.getLeafs();
		
		int maxCol = 0;
		for (TreeDataItem<Object> leaf : leafs) {
			if (leaf.getAllParents().size() > maxCol) {
				maxCol = leaf.getAllParents().size();
			}
		}
		
		Object [] [] result = new Object [leafs.size()] [maxCol];
		for (TreeDataItem<Object> leaf : leafs) {
			for (TreeDataItem<Object> parent : leaf.getAllParents()) {
				// Remove the root element of the tree
				if (parent.getItemData() != null) {
					result[leafs.indexOf(leaf)][leaf.getAllParents().indexOf(parent)-1] = parent.getItemData();
				}
			}
			// Add the leaf to the path
			result[leafs.indexOf(leaf)][maxCol-1] = leaf.getItemData();
		}
		
		
		
		return result;
	}
	
	/**
	 * This method returns the operation object (Simple, Enclosing or Composite) from the model that matches the provided name.
	 * 
	 * @param name the name of the operation that will be returned.
	 * @param model the MAST model from which the operation will be returned.
	 * @return the operation object that matches the given name or null if no operation was found with the given name.
	 */
	public static Object getModelOperation(String name, MAST_MODEL model) {
		TreeIterator<EObject> i = model.eAllContents();
		while (i.hasNext()) {
			EObject current = i.next();
			if (current instanceof Simple_Operation && ((Simple_Operation)current).getName() != null && ((Simple_Operation)current).getName().equals(name)) {
				return current;
			}
			else if (current instanceof Enclosing_Operation && ((Enclosing_Operation)current).getName() != null && ((Enclosing_Operation)current).getName().equals(name)) {
				return current;
			}
			else if (current instanceof Composite_Operation && ((Composite_Operation)current).getName() != null && ((Composite_Operation)current).getName().equals(name)) {
				return current;
			}
		}
		
		
		return null;
	}
	
	/**
	 * This method returns the shared resource object (Priority Inheritance, Immediate Ceiling or SRP) from the model that matches the provided name.
	 * 
	 * @param name the name of the shared resource that will be returned.
	 * @param model the MAST model from which the resource will be returned.
	 * @return the shared resource object that matches the given name or null if no resource was found with the given name.
	 */
	public static Object getModelResource(String name, MAST_MODEL model) {
		TreeIterator<EObject> i = model.eAllContents();
		while (i.hasNext()) {
			EObject current = i.next();
			if (current instanceof SRP_Resource && ((SRP_Resource)current).getName() != null && ((SRP_Resource)current).getName().equals(name)) {
				return current;
			}
			else if (current instanceof Priority_Inheritance_Resource && ((Priority_Inheritance_Resource)current).getName() != null && ((Priority_Inheritance_Resource)current).getName().equals(name)) {
				return current;
			}
			else if (current instanceof Immediate_Ceiling_Resource && ((Immediate_Ceiling_Resource)current).getName() != null && ((Immediate_Ceiling_Resource)current).getName().equals(name)) {
				return current;
			}
		}
		
		
		return null;
	}
	
	/**
	 * This method returns the scheduling server object from the model that matches the provided name.
	 * 
	 * @param name the name of the scheduling server that will be returned.
	 * @param model the MAST model from which the scheduling server will be returned.
	 * @return the scheduling server object that matches the given name or null if no scheduling server was found with the given name.
	 */
	public static Regular_Scheduling_Server getModelSchedulingServer(String name, MAST_MODEL model) {
		TreeIterator<EObject> i = model.eAllContents();
		while (i.hasNext()) {
			EObject current = i.next();
			if (current instanceof Regular_Scheduling_Server && ((Regular_Scheduling_Server)current).getName() != null && ((Regular_Scheduling_Server)current).getName().equals(name)) {
				return (Regular_Scheduling_Server)current;
			}
		}
		
		
		return null;
	}
	
	public static IAnalysisResult createResultsFromModel (REAL_TIME_SITUATION rts) {
		LinkedHashMap<String, HashMap<String, Object>> records = new LinkedHashMap<String, HashMap<String,Object>>();
		
		// Generate analysis data:
		LinkedHashMap<String, Object> analysisData = new LinkedHashMap<String, Object>();
		analysisData.put("Model Name", rts.getModelName());
		analysisData.put("Analysis Date", rts.getGenerationDate());
		analysisData.put("Model Date", rts.getModelDate());
		analysisData.put("Analysis Tool", rts.getGenerationTool());
		records.put("Analysis Description Data", analysisData);
		
		// Generate processing resources data
		LinkedHashMap<String, Object> prData = new LinkedHashMap<String, Object>();
		for (Processing_Resource_Results res : rts.getProcessingResource()) {
			if (res.getUtilization().size() >= 1) {
				prData.put("Utilization of " + res.getName(), res.getUtilization().get(0).getTotal());
			}
			if (res.getReadyQueueSize().size() >= 1) {
				prData.put("Ready Queue Size of " + res.getName(), res.getReadyQueueSize().get(0).getMaxNum());
			}
			if (res.getSlack().size() >= 1) {
				prData.put("Slack of " + res.getName(), res.getSlack().get(0).getValue());
			}
		}
		if (!prData.isEmpty()) {
			records.put("Processing Resources Results", prData);
		}
		
		// Generate scheduling server data
		LinkedHashMap<String, Object> ssData = new LinkedHashMap<String, Object>();
		for (Scheduling_Server_Results res : rts.getSchedulingServer()) {
			if (res.getFixedPriorityPolicy() != null) {
				ssData.put("Fixed Priority computed for " + res.getName(), res.getFixedPriorityPolicy().getThePriority());
			}
			if (res.getInterruptFPPolicy() != null) {
				ssData.put("Interrupt Priority computed for " + res.getName(), res.getInterruptFPPolicy().getThePriority());
			}
			if (res.getNonPreemptibleFPPolicy() != null) {
				ssData.put("Non-Preemptible FP computed for " + res.getName(), res.getNonPreemptibleFPPolicy().getThePriority());
			}
			if (res.getSporadicServerPolicy() != null) {
				ssData.put("Priority of Sporadic Server Policy computed for " + res.getName(), res.getSporadicServerPolicy().getNormalPriority());
				ssData.put("Replenishment Period Sporadic Server Policy computed for " + res.getName(), res.getSporadicServerPolicy().getReplenishmentPeriod());
			}
			if (res.getPollingPolicy() != null) {
				ssData.put("Priority of Polling Priority computed for " + res.getName(), res.getPollingPolicy().getThePriority());
				ssData.put("Polling Period computed for " + res.getName(), res.getPollingPolicy().getPollingPeriod());
			}
		}
		if (!ssData.isEmpty()) {
			records.put("Scheduling Server Results", ssData);
		}
		
		// Generate shared resources data
		LinkedHashMap<String, Object> srData = new LinkedHashMap<String, Object>();
		for (Shared_Resource_Results res : rts.getSharedResource()) {
			if (res.getUtilization() != null) {
				srData.put("Utilization of " + res.getName(), res.getUtilization().getTotal());
			}
			if (res.getQueueSize() != null) {
				srData.put("Max Queue Size computed for " + res.getName(), res.getQueueSize().getMaxNum());
			}
			if (res.getPriorityCeiling() > 0) {
				srData.put("Priority Ceiling computed for " + res.getName(), res.getPriorityCeiling());
			}
		}
		if (!srData.isEmpty()) {
			records.put("Shared Resources Results", srData);
		}
		
		// Generate operation data
		LinkedHashMap<String, Object> opData = new LinkedHashMap<String, Object>();
		for (Operation_Results res : rts.getOperation()) {
			if (res.getSlack() != null) {
				opData.put("Slack measured for " + res.getName(), res.getSlack().getValue());
			}
		}
		if (!opData.isEmpty()) {
			records.put("Operation Results", opData);
		}
		
		// Generate transaction data
		LinkedHashMap<String, Object> tData = new LinkedHashMap<String, Object>();
		for (Transaction_Results res : rts.getTransaction()) {
			if (res.getSlack().size() > 0) {
				tData.put("Slack measured for " + res.getName(), res.getSlack().get(0).getValue());
			}
			if (res.getTimingResult() != null) {
				for (Timing_Result tres : res.getTimingResult()) {
					tData.put("Number of suspensions on event " + tres.getEventName() + " in transaction " + res.getName(), tres.getNumOfSuspensions());
					tData.put("Worst Blocking Time for event " + tres.getEventName() + " in transaction " + res.getName(), tres.getWorstBlockingTime());
					tData.put("Worst Local Response Time for event " + tres.getEventName() + " in transaction " + res.getName(), tres.getWorstLocalResponseTime());
					for (Global_Response_Time grt : tres.getWorstGlobalResponseTimes().getGlobalResponseTime()) {
						tData.put("Worst Global Response Time for external event " + grt.getReferencedEvent() + " in transaction " + res.getName(), grt.getTimeValue());
					}
				}
			}
		}
		if (!tData.isEmpty()) {
			records.put("Transaction Results", tData);
		}
		
		// Generate system data
		LinkedHashMap<String, Object> systemData = new LinkedHashMap<String, Object>();
		if (rts.getSystemSlack() != null && !rts.getSystemSlack().isEmpty()) {
			systemData.put("Total system slack", rts.getSystemSlack().get(0).getValue());
		}
		if (!systemData.isEmpty()) {
			records.put("System Results", systemData);
		}
		
		return ResultFactory.createResult(records);
	}
	
}
