/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.masttool.resources;

import java.util.List;
import java.util.Vector;


/**
 * Class used to create a tree of Objects
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 * @param <E> the type of the objects that will be inserted in the tree. 
 */
public class TreeList<E> {
	
	private final TreeDataItem<E> rootElement = new TreeDataItem<E>();
	private Vector<TreeDataItem<E>> data = new Vector<TreeDataItem<E>>();
	
	/**
	 * Returns the final root item of the tree. Elements must be created as children of this element.
	 * 
	 * @return the root tree element.
	 */
	public TreeDataItem<E> getTreeRoot() {
		return rootElement;
	}
	
	/**
	 * Returns a list with all the elements of the tree that are leafs,
	 * that is, that don't have children.
	 * 
	 * @return the list of leaf elements.
	 */
	public List<TreeDataItem<E>> getLeafs() {
		Vector<TreeDataItem<E>> result = new Vector<TreeDataItem<E>>();
		fillDataVector();
		for (TreeDataItem<E> item : data) {
			if (!item.hasChildren()) {
				result.add(item);
			}
		}
		return result;
	}

	/**
	 * Internal method to fill the data vector. 
	 * This vector contains all the data objects stored in the tree as a list.
	 * 
	 */
	private void fillDataVector() {
		for (TreeDataItem<E> item : rootElement.getChildren()) {
			fill(item);
		}
	}

	/**
	 * Recursive internal method used to add a data item to the data vector and all its children. 
	 * 
	 * @param item the current item to be stored.
	 */
	private void fill(TreeDataItem<E> item) {
		data.add(item);
		for (TreeDataItem<E> child : item.getChildren()) {
			fill(child);
		}
	}
	
	

}
