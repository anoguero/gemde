/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.masttool.resources;

import java.util.List;
import java.util.Vector;

/**
 * Class that implements an element of a tree list.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 * @param <E> the type of the objects that will be stored in the item.
 */
public class TreeDataItem<E> {

	private Vector<TreeDataItem<E>> children;
	private E data = null;
	private TreeDataItem<E> parent = null;
	
	/**
	 * Constructor of the Class
	 *
	 */
	TreeDataItem () {
		children = new Vector<TreeDataItem<E>>();
	}
	
	/**
	 * Method used to set the data element of the tree item.
	 * 
	 * @param data the instance of the data object to be stored.
	 */
	public void setItemData(E data) {
		this.data = data;
	}
	
	/**
	 * Returns the data object of the current tree item.
	 * 
	 * @return the data object instance stored in this tree item.
	 */
	public E getItemData () {
		return data;
	}
	
	/**
	 * Returns the parent of this tree object. It may return null if the tree item has no parents (the root item).
	 * 
	 * @return the parent of the current tree item.
	 */
	public TreeDataItem<E> getParent() {
		return parent;
	}
	
	/**
	 * Creates a child tree item with the given data attached to the current tree item and returns the instance of the child.
	 * 
	 * @param data the data to be stored in the child item.
	 * @return the instance of the child tree item.
	 */
	public TreeDataItem<E> createChild (E data) {
		TreeDataItem<E> child = new TreeDataItem<E>();
		child.setItemData(data);
		child.parent = this;
		children.add(child);
		return child;
	}
	
	/**
	 * Returns an ordered list from the top of the tree containing all the parents of the current tree item.
	 * 
	 * @return the ordered parents list.
	 */
	public List<TreeDataItem<E>> getAllParents() {
		Vector<TreeDataItem<E>> result = new Vector<TreeDataItem<E>>();
		
		TreeDataItem<E> current = parent;
		int count = 0, size = 0;
		while (current != null) {
			count++;
			current = current.getParent();
		}
		size = count;
		
		result.setSize(size);
		
		current = parent;
		while (current != null) {
			result.insertElementAt(current, 0);
			current = current.getParent();
			count--;
		}
		
		result.setSize(size);
		
		return result;
	}
	
	/**
	 * Returns an ordered list from the top of the tree containing the data objects of all the parents of the current tree item.
	 * 
	 * @return the ordered data objects list.
	 */
	public List<E> getAllParentsData () {
		Vector<E> result = new Vector<E>();
		
		TreeDataItem<E> current = parent;
		int count = 0;
		while (current != null) {
			count++;
			current = current.getParent();
		}
		
		result.setSize(count);
		
		current = parent;
		while (current != null) {
			result.insertElementAt(current.getItemData(), count-1);
			current = current.getParent();
			count--;
		}
		
		return result;
	}
	
	/**
	 * Checks whether the current tree item has children.
	 * 
	 * @return true is the tree item has children. False otherwise.
	 */
	public boolean hasChildren () {
		return children.size() > 0;
	}
	
	/**
	 * Returns a list with the children tree items of the current one. It might be empty.
	 * 
	 * @return the list of the children tree items.
	 */
	public List<TreeDataItem<E>> getChildren() {
		return children;
	}
	
}
