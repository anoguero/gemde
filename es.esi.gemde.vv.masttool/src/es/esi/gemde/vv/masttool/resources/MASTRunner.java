/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.masttool.resources;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;

import es.esi.gemde.vv.masttool.MastToolPlugin;
import es.esi.gemde.vv.masttool.exceptions.MASTNotFoundException;
import es.esi.gemde.vv.masttool.resources.ui.MASTToolPreferencePage;
import es.esi.gemde.vv.modelanalyzer.exceptions.ToolException;

/**
 * Utility class used as an interface between Eclipse and MAST. 
 * Operations invoke this class using the {@link MASTRunner#runMAST(String, String[], File)} method
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class MASTRunner {

	// Constants for the names of the tools. Used internally only!
	private static final String MAST = "mast_analysis";
	private static final String MAST_RESULT_CONVERTER = "mast_xml_convert_results";
	
	// Constants for the names of the operations.
	public static final String CLASSIC_RM = "classic_rm";
	public static final String VARYING_PRIOS = "varying_priorities";
	public static final String EDF_MONO = "edf_monoprocessor";
	public static final String EDF_WITHIN_PRIOS = "edf_within_priorities";
	public static final String HOLISTIC = "holistic";
	public static final String HOLISTIC_LOCAL_EDF = "holistic_local_edf";
	public static final String HOLISTIC_GLOBAL_EDF = "holistic_global_edf";
	public static final String OFFSET_BASED = "offset_based";
	public static final String OFFSET_BASED_OPTIMIZED = "offset_based_optimized";
	
	// Constants for the names of the options
	public static final String CEILINGS = "-ceilings";
	public static final String PARAMETERS = "-sched_parameters";
	public static final String TECHNIQUE = "-technique";
	public static final String ASSIGNMENT = "-assignment_parameters";
	public static final String DESCRIPTION = "-description";
	public static final String SYSTEM_SLACK = "-slack";
	public static final String OP_SLACK = "-operation_slack";
	public static final String HOPA = "hopa";
	public static final String HOSDA_LOCAL = "hosda_local";
	public static final String HOSDA_GLOBAL = "hosda_global";
	public static final String PD_LOCAL = "pd_local";
	public static final String PD_GLOBAL = "pd_global";
	public static final String NPD_LOCAL = "npd_local";
	public static final String NPD_GLOBAL = "npd_global";
	public static final String ANNEALING = "annealing";
	public static final String MONOPROCESSOR = "monoprocessor";
	
	// Static variable containing the path to the MAST binaries
	private static IPath path = null;
	
	/**
	 * Method used to invoke the MAST tool with a given parameters list. 
	 * This method creates the MAST invocation from the parameters provided 
	 * by operations and handles the outputs provided by the tool.
	 * 
	 * The method returns a File containing the results of the analysis if 
	 * no errors occur during MAST invocation.
	 * 
	 * @param tool the name of the tool that will be invoked from MAST.
	 * @param options an array of String containing the options to be passed to the MAST tool. They must be properly ordered.
	 * @param input the File containing the input model to be passed to MAST.
	 * @return the output File produced by MAST. This File will be located in the temporary folder of this plug-in.
	 * @throws MASTNotFoundException if MAST is not found in the path currently configured in the plug-in.
	 * @throws IllegalArgumentException if any of the input parameters is not valid or if the options list is not correct. 
	 * @throws ToolException if any error shall occur during the execution of MAST. Errors will be wrapped inside this exception.
	 */
	public static File runMAST (String tool, String [] options, File input) throws MASTNotFoundException, IllegalArgumentException, ToolException {
		// First load and check if MAST is available in the configured path.
		loadMASTBinaryPath();
		
		File mastDir = path.toFile();
		
		if (mastDir == null) {
			throw new MASTNotFoundException("MAST couldn't be found in the given path! Please, configure the MAST Tool path in the preferences page under Window/Preferences");
		}
		
		boolean found = false;
		for (String fileName : mastDir.list()) {
			if (fileName != null && fileName.contains(MAST)) {
				found = true;
			}
		}
		
		if (!found) {
			throw new MASTNotFoundException("MAST couldn't be found in the given path! Please, configure the MAST Tool path in the preferences page under Window/Preferences");
		}
		
		// Check provided inputs
		if (tool == null || (!tool.equals(CLASSIC_RM) && !tool.equals(VARYING_PRIOS) && !tool.equals(EDF_MONO) && !tool.equals(EDF_WITHIN_PRIOS) && !tool.equals(HOLISTIC) && !tool.equals(HOLISTIC_LOCAL_EDF) && !tool.equals(HOLISTIC_GLOBAL_EDF) && !tool.equals(OFFSET_BASED) && !tool.equals(OFFSET_BASED_OPTIMIZED))) {
			throw new IllegalArgumentException("Non valid tool name was provided");
		}
		
		// Check provided options
		if (!checkOptions(options)) {
			throw new IllegalArgumentException("Non valid option list was provided");
		}
		
		// Check input file
		if (!input.isFile() || !input.canRead()) {
			throw new IllegalArgumentException("Non valid input file was provided");
		}
		
		// Set the output file in the same directory as the input
		File output = new Path(input.getParentFile().getAbsolutePath()).append(input.getName().substring(0, input.getName().lastIndexOf('.')) + ".out").toFile();
		
		// If all input arguments are correct we invoke the MAST tool
		String command = createMASTCommand(tool, options, input, output);
		
		// Execute
		Process p1 = null;
		try {
			p1 = Runtime.getRuntime().exec(command, null, mastDir);
		} catch (IOException e) {
			throw new ToolException(e);
		}
		
		// Set a watchdog timer to prevent MAST to be running forever
		WatchdogTimer watchdog = new WatchdogTimer(p1, 30000);
		watchdog.start();
		
		String message = new String();
		
		// Capture the output of the tool
		if (p1 != null) {
			InputStream is = p1.getInputStream();
			byte[] buffer = new byte [256];
			int read;
			try {
				while ((read = is.read(buffer)) != -1) {
					message += new String(buffer, 0, read);
				}
			}
			catch (IOException e) {
				throw new ToolException(e);
			}
		}
		
		// After the tool has terminated we check the returned value for correctness
		int retVal = 0;
		try {
			retVal = p1.waitFor();
		} catch (InterruptedException e) {
			// Not used catch block...
		}
		
		// Turn-off the watchdog
		watchdog.interrupt();
		
		//System.out.println("Return Value: " + retVal);
		System.out.println(message); 
		
		if (retVal != 0) {
			if (watchdog.wasTrigerred()) {
				throw new ToolException(new Exception("MAST Timed-out! This could have happened due to a bug in the tool or to a system overload."));
			}
			else {
				throw new ToolException(new Exception("MAST returned an error:\n" + message));
			}
		}
		
		// If the tool was executed correctly we transform the 
		// results to the model format and return the pointer to 
		// the file
		String conversion = createResultCoversionCommand(input, output);
		
		// Execute
		Process p2 = null;
		try {
			p2 = Runtime.getRuntime().exec(conversion, null, mastDir);
		} catch (IOException e) {
			throw new ToolException(e);
		}
		
		// Set a watchdog timer to prevent MAST to be running forever
		WatchdogTimer watchdog2 = new WatchdogTimer(p2, 30000);
		watchdog2.start();
		
		String resultsStr = new String();
		
		// Capture the output of the tool
		if (p2 != null) {
			InputStream is = p2.getInputStream();
			byte[] buffer = new byte [256];
			int read;
			try {
				while ((read = is.read(buffer)) != -1) {
					resultsStr += new String(buffer, 0, read, "UTF-8");
				}
			}
			catch (IOException e) {
				throw new ToolException(e);
			}
		}
		
		int convVal = 0;
		try {
			convVal = p2.waitFor();
		} catch (InterruptedException e) {
			// Not used catch block...
		}
		
		watchdog2.interrupt();
		
		if (convVal != 0 || !resultsStr.startsWith("<?xml")) {
			if (watchdog2.wasTrigerred()) {
				throw new ToolException(new Exception("MAST Timed-out! This could have happened due to a bug in the tool or to a system overload."));
			}
			else {
				throw new ToolException(new Exception("MAST results conversion returned an error:\n" + resultsStr));
			}
		}
		
		// Persisting the read model
		File outModel = new Path(input.getParentFile().getAbsolutePath()).append(input.getName().substring(0, input.getName().lastIndexOf('.')) + ".mastresult").toFile(); 
		try {
			if (!outModel.exists()) {
				outModel.createNewFile();
			}
			FileOutputStream os = new FileOutputStream(outModel);
			os.write(resultsStr.getBytes("UTF-8"));
			os.close();
		} catch (FileNotFoundException e) {
			throw new ToolException(e);
		} catch (IOException e) {
			throw new ToolException(e);
		}		
		
		return outModel;
	}

	/**
	 * Internal method that loads the MAST binary path to the variable.
	 */
	private static void loadMASTBinaryPath() {
		path = new Path(MastToolPlugin.getDefault().getPreferenceStore().getString(MASTToolPreferencePage.MAST_DIR));
	}

	/**
	 * Internal method that creates the MAST command to be executed by the Runtime.
	 * No checks are done at this stage!!
	 * 
	 * @param tool the name of the MAST tool to be invoked.
	 * @param options an STring array containing the options
	 * @param input the input File to be provided to the tool.
	 * @param output the output File to be provided to the tool.
	 * @return a String containing the command to be executed.
	 */
	private static String createMASTCommand(String tool, String[] options, File input, File output) {
		String command = new String();
		
		command += path.append(MAST).toOSString() + " " + tool;
		
		boolean nextIsFile = false;
		for (String option : options) {
			if (nextIsFile && option.contains(" ")) {
				command += " \"" + option + "\"";
			}
			else {
				command += " " + option;
			}
			if (option.equals(DESCRIPTION) || option.equals(ASSIGNMENT)) {
				nextIsFile = true;
			}
		}
		
		IPath inputPath = new Path(input.getAbsolutePath());
		
		if (inputPath.toOSString().contains(" ")) {
			command += " \"" + inputPath.toOSString() + "\"";
		}
		else {
			command += " " + inputPath.toOSString();
		}
		
		if (output != null) {
			if (output.exists() && output.canWrite() && output.isFile()) {
				
				IPath outputPath = new Path(output.getAbsolutePath());

				if (outputPath.toOSString().contains(" ")) {
					command += " \"" + outputPath.toOSString() + "\"";
				}
				else {
					command += " " + outputPath.toOSString();
				}
				
			}
			else if (!output.exists()) {
				try {
					output.createNewFile();
					
					IPath outputPath = new Path(output.getAbsolutePath());

					if (outputPath.toOSString().contains(" ")) {
						command += " \"" + outputPath.toOSString() + "\"";
					}
					else {
						command += " " + outputPath.toOSString();
					}
					
				}
				catch (IOException e) {
					//e.printStackTrace();
				}
			}
		}
		
		
		return command;
	}
	
	/**
	 * Internal method that creates the results file to model conversion command.
	 * This method makes no checks!
	 * 
	 * @param input the File containing the input model provided to the tool.
	 * @param output the results File to be converted to a model. 
	 * @return the conversion command to be executed by the Runtime.
	 */
	private static String createResultCoversionCommand(File input, File output) {
		String command = new String();
		
		IPath inputPath, outputPath;
		
		inputPath = new Path(input.getAbsolutePath());
		outputPath = new Path(output.getAbsolutePath());
		
		command += path.append(MAST_RESULT_CONVERTER).toOSString();
		
		if (inputPath.toOSString().contains(" ")) {
			command += " \"" + inputPath.toOSString() + "\"";
		}
		else {
			command += " " + inputPath.toOSString();
		}
		
		if (outputPath.toOSString().contains(" ")) {
			command += " \"" + outputPath.toOSString() + "\"";
		}
		else {
			command += " " + outputPath.toOSString();
		}
		
		
		return command;
	}

	/**
	 * Internal method to check that the provided option list is correctly formed.
	 * 
	 * @param options the array of String containing the options to be chacked. It may be empty, but not null!!
	 * @return true if the provided option array is correct. False otherwise.
	 */
	private static boolean checkOptions(String[] options) {
		// Check the validity of the options list
		for (int i = 0; i < options.length; i++) {
			// Check if any option is null
			if (options[i] == null) {
				return false; 
			}
			// Check ASSIGNMENT option
			if (options[i].equals(ASSIGNMENT)) {
				if (++i >= options.length || options[i] == null) {
					return false;
				}
				else {
					File f = new File(options[i]);
					if (!f.exists() || !f.canRead() || !f.isFile()) {
						return false;
					}
				}
			}
			// Check DESCRIPTION option
			if (options[i].equals(DESCRIPTION)) {
				if (++i >= options.length || options[i] == null) {
					return false;
				}
				else {
					File f = new File(options[i]);
					if (f.getParentFile() == null || !f.getParentFile().isDirectory()) {
						return false;
					}
				}
			}
			// Check OP_SLACK option
			if (options[i].equals(OP_SLACK)) {
				if (++i >= options.length || options[i] == null) {
					return false;
				}
			}
			// Check TECHNIQUE option
			if (options[i].equals(TECHNIQUE)) {
				if (i-1 < 0 || !options[i-1].equals(PARAMETERS)) {
					return false;
				}
				if (++i >= options.length || options[i] == null) {
					return false;
				}
				else {
					if (options[i].equals(HOPA) && options[i].equals(HOSDA_GLOBAL) && options[i].equals(HOSDA_LOCAL) && options[i].equals(PD_GLOBAL) && options[i].equals(PD_LOCAL) && options[i].equals(NPD_GLOBAL) && options[i].equals(NPD_LOCAL) && options[i].equals(ANNEALING) && options[i].equals(MONOPROCESSOR)) {
						return false;
					}
				}
			}
		}
		return true;
	}
}
