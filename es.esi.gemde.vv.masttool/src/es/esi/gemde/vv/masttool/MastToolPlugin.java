/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.masttool;

import java.io.File;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class MastToolPlugin extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "es.esi.gemde.vv.masttool";

	// The shared instance
	private static MastToolPlugin plugin;
	
	/**
	 * The constructor
	 */
	public MastToolPlugin() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static MastToolPlugin getDefault() {
		return plugin;
	}
	
	/**
	 * This method creates the directory of the tool is needed. This directory will be used for storing temporal data.
	 */
	public static void createDir () {
		// We store the file to a temporary folder in the workspace metadata region
		IPath wsPath = ResourcesPlugin.getWorkspace().getRoot().getLocation();
		
		// Create the directories if needed
		File dir = wsPath.append(".metadata/.plugins").toFile();
		if (!dir.exists() || !dir.isDirectory()) {
			dir.mkdir();
		}
		
		dir = wsPath.append(".metadata/.plugins/" + MastToolPlugin.PLUGIN_ID).toFile();
		if (!dir.exists() || !dir.isDirectory()) {
			dir.mkdir();
		}
	}
	
	/**
	 * This method empties the temporal directory of the tool. 
	 * It is called after an analysis operation has been completed.
	 */
	public static void cleanupDir () {
		// We store the file to a temporary folder in the workspace metadata region
		IPath wsPath = ResourcesPlugin.getWorkspace().getRoot().getLocation();
		
		// Go to the temporal MAST tool directory
		File dir = wsPath.append(".metadata/.plugins/" + MastToolPlugin.PLUGIN_ID).toFile();
		if (dir.exists() && dir.isDirectory()) {
			for (File f : dir.listFiles()) {
				f.delete();
			}
		}
	}

}
