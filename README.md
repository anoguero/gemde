# GEMDE #

GEMDE is a MDE framework tool for Eclipse that enables users to create a full MDE method, combining model validation, model transformation and connection to third party tools. Additionally GEMDE will support in the future methodology modeling and automation, based on Eclipse cheatsheets. 

## Installation Instructions ##

You may install GEMDE by compiling the source code or directly using the latest released update site. In this page we will guide you through the installation of GEMDE in a blank Eclipse for Java developers instance downloaded from the official site.

### Download the latest Update Site ###

GEMDE is packaged as a prebuilt update site. You may download the latest version from the [downloads section](http://bitbucket.org/anoguero/gemde/downloads). Once downloaded, open the Help/Install New Software... wizard.

Add the downloaded update site to the Available Software Sites list.

Select the site and all the features to perform the installation. The main Eclipse repository will be accessed during the installation process to find dependencies.

And everything is ready!! Enjoy GEMDE!!


### Download the Source Code (committers) ###

**Dependencies and Code Generation**

To build GEMDE from source you will need to follow these steps:

 * Install the [Eugenia GFM plugin](http://www.eclipse.org/epsilon/download/) from Epsilon. Also install the Emfatic plugin (see dependencies of Eugenia).
 * Install EMF, EMF Validation Framework and GMF from the official Eclipse repositories.