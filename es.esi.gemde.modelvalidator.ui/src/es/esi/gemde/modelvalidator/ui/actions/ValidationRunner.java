/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.modelvalidator.ui.actions;

import java.util.List;
import java.util.Vector;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

import es.esi.gemde.modelvalidator.ModelValidatorPlugin;
import es.esi.gemde.modelvalidator.exceptions.IllegalTargetException;
import es.esi.gemde.modelvalidator.exceptions.NoConstraintsValidatedException;
import es.esi.gemde.modelvalidator.exceptions.ValidationEngineException;
import es.esi.gemde.modelvalidator.service.IBatchValidation;
import es.esi.gemde.modelvalidator.service.IValidationResult;
import es.esi.gemde.modelvalidator.ui.ModelValidatorUI;

/**
 * A wrapper task for the code shared by RunValidationAction and RunOtherValidationAction
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class ValidationRunner {

	/**
	 * Static wrapper method for the shared code.
	 * 
	 * @param validation the validation to be run.
	 * @param rootTarget the selected object got from the workbench.
	 */
	public static void runValidation(IBatchValidation validation, EObject rootTarget) {
		// First we create the list of targets for the validation
		List<EObject> targets = new Vector<EObject>();
		
		// The targets list is created using all the contents of the root target that share metamodel with the constraints in the current validation. 
		targets.add(rootTarget);
		TreeIterator<EObject> contents = rootTarget.eAllContents();
		while(contents.hasNext()) {
			EObject current = contents.next(); 
			if (rootTarget.eClass().getEPackage().equals(current.eClass().getEPackage())) {
				targets.add(current);
			}
		}
		
		IValidationResult result = null;
		try {
			result = ModelValidatorPlugin.getServer().runValidation(validation.getName(), targets);
		} catch (IllegalArgumentException e) {
			// Should never occur
			e.printStackTrace();
		} catch (ValidationEngineException e) {
			MessageDialog.openError(new Shell(), "Exception raised during validation!", "GEMDE validation engine threw an Exception: " + e.getMessage());
		} catch (IllegalTargetException e) {
			MessageDialog.openError(new Shell(), "Illegal target!", e.getMessage());
		} catch (NoConstraintsValidatedException e) {
			MessageDialog.openInformation(new Shell(), "No constraints where validated!", "Validation '" + validation.getName() + "' couldn't validate any constraints against the selected target.");
		}
		
		ModelValidatorUI.showValidationResults(result);
	}
	
}
