/*******************************************************************************
 * Copyright (c) 2012 Tecnalia. Software Systems Engineering Unit.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@tecnalia.com)
 *     
 *******************************************************************************/
package es.esi.gemde.modelvalidator.ui.actions;

import java.lang.reflect.Method;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.internal.Workbench;

/**
 * This class contains utility methods for the actions.
 *  
 * @author Adri�n Noguero (adrian.noguero@tecnalia.com)
 * @version 1.0
 * @since 1.0
 *
 */
@SuppressWarnings("restriction")
public class SelectionUtils {

	/**
	 * This method provides access the selected EObject in the current selection if any.
	 * 
	 * @return the selected EObject instance, or null.
	 */
	public static EObject getSelectedEObject () {

		ISelection selection = Workbench.getInstance().getActiveWorkbenchWindow().getSelectionService().getSelection();
		if (selection instanceof StructuredSelection && ((StructuredSelection)selection).getFirstElement() instanceof EObject) {
			return (EObject)((StructuredSelection)selection).getFirstElement(); 

		}
		else if (selection instanceof StructuredSelection && ((StructuredSelection)selection).getFirstElement() instanceof IGraphicalEditPart) {
			return ((IGraphicalEditPart)((StructuredSelection)selection).getFirstElement()).resolveSemanticElement();
		}
		else {
			boolean canContinue = false;
			Method getUMLElementMethod = null;
			try {
				Class<?> clazz = Class.forName("com.cea.papyrus.core.editpart.IUMLElementEditPart");
				canContinue = selection instanceof StructuredSelection && clazz.isAssignableFrom(((StructuredSelection)selection).getFirstElement().getClass());
				if (canContinue) {
					for (Method m : clazz.getMethods()) {
						if (m.getName().equals("getUmlElement") && m.getParameterTypes().length == 0) {
							getUMLElementMethod = m;
						}
					}
				}
			}
			catch (Exception e) {
				// This means that the Papyrus plug-in is not present. We just ignore this exception.
				return null;
			}
			if (canContinue && getUMLElementMethod != null) {

				Object retObj = null;
				try {
					retObj = getUMLElementMethod.invoke(((StructuredSelection)selection).getFirstElement(), new Object [0]);
				} catch (Exception e) {
					// Error regarding Papyrus... not important
				}

				if (retObj != null && retObj instanceof EObject) {
					return (EObject)retObj;
				}
				else {
					return null;
				}
			}
			else {
				return null;
			}
		}
	}
}
