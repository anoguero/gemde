/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/

package es.esi.gemde.modelvalidator.ui.actions;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.action.Action;

import es.esi.gemde.modelvalidator.service.IBatchValidation;


/**
 * Action that runs a validation selected from the context menu.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class RunValidationAction extends Action {

	private IBatchValidation validation;
	
	/**
	 * Constructor including the validation to be executed.
	 *
	 *@param validation the selected batch validation.
	 */
	public RunValidationAction (IBatchValidation validation) {
		super();
		setText("Run '" + validation.getName() + "'");
		this.validation = validation;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		
		EObject rootTarget = SelectionUtils.getSelectedEObject();
		if (rootTarget != null) {
			ValidationRunner.runValidation(validation, rootTarget);
		}
			
	}

}
