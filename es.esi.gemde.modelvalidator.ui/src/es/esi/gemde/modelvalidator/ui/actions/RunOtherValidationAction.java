/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/

package es.esi.gemde.modelvalidator.ui.actions;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;

import es.esi.gemde.core.CorePlugin;
import es.esi.gemde.core.resources.ExtendedWizardDialog;
import es.esi.gemde.modelvalidator.ModelValidatorPlugin;
import es.esi.gemde.modelvalidator.ui.wizards.ValidationCreationWizard;


/**
 * Action that implements the logic to run a validation. 
 * It calls the ValidationCreationiWizard to define it beforehand.
 *
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 * @version 1.0.4
 * @since 1.0
 *
 */
public class RunOtherValidationAction extends Action {

	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		
		EObject rootTarget = SelectionUtils.getSelectedEObject();
		
		if (rootTarget != null) {

			// First we call the ValidationCreationiWizard to create a batch validation
			ValidationCreationWizard wizard = new ValidationCreationWizard();
			WizardDialog dialog = new ExtendedWizardDialog(new Shell(), wizard, CorePlugin.getImage(CorePlugin.VALIDATION_REPOSITORY_ICON));
			int status = dialog.open();

			if (status != Dialog.OK) {
				MessageDialog.openError(new Shell(), "Couldn't run the validation", "The validation was not created correctly. Validation process will be aborted.");
			}
			else {
				// Once we have created the validation we store it in the repository for further uses
				ModelValidatorPlugin.getServer().addBatchValidation(wizard.getCreatedValidation());

				ValidationRunner.runValidation(wizard.getCreatedValidation(), rootTarget);
			}
		}
		
		
	}

	/**
	 * Default constructor. It establishes "Other..." as text for this action.
	 *
	 */
	public RunOtherValidationAction() {
		super();
		setText("Other...");
	}

}
