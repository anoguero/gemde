/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.modelvalidator.ui.actions;

import java.util.Vector;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;

import es.esi.gemde.modelvalidator.service.IBatchValidation;
import es.esi.gemde.modelvalidator.service.IModelValidatorConstraint;
import es.esi.gemde.modelvalidator.ui.dialogs.ExportValidationsDialog;
import es.esi.gemde.modelvalidator.ui.resources.Exporter;

/**
 * Action that implements the logic to export a list of validations
 * from the GEMDE repository as plug-in contributions.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class ExportValidationsAction implements IWorkbenchWindowActionDelegate {

	private IWorkbenchWindow window;
	/**
	 * This method just calls two dialogs to get the required information from the user
	 * to export some constraints to a plug-in extension file. 
	 * 
	 * @see IWorkbenchWindowActionDelegate#run
	 */
	public void run(IAction action) {
		
		DirectoryDialog dialog = new DirectoryDialog(window.getShell());
		dialog.setText("Select the export path");
		dialog.setMessage("Select the directory where the exported validations and constraints will be placed");
		String path = dialog.open();
		if (path != null) {
			// Select the constraints to be exported
			ExportValidationsDialog dialog2 = new ExportValidationsDialog(window.getShell());
			if (dialog2.open() == Dialog.OK) {
				String idPrefix = dialog2.getIDPrefix();
				IBatchValidation[] validations = dialog2.getSelectedValidations().toArray(new IBatchValidation[0]);
				IModelValidatorConstraint[] constraints = getRequiredConstraints(validations);
				Exporter.export(path, idPrefix, constraints, validations);
			}
		}
		
	}

	/**
	 * Internal method used to get from the repository all the constraints
	 * referred in a given set of validations.
	 * 
	 * @param validations an array with the validations from which the constraints will be obtained.
	 * @return the array of constraints referred by the validations.
	 */
	private IModelValidatorConstraint[] getRequiredConstraints(IBatchValidation[] validations) {
		Vector<IModelValidatorConstraint> constraints = new Vector<IModelValidatorConstraint>();
		
		for (IBatchValidation val : validations) {
			for (IModelValidatorConstraint cons : val.getSelectedConstraints()) {
				if (!constraints.contains(cons)) {
					constraints.add(cons);
				}
			}
		}
		
		return constraints.toArray(new IModelValidatorConstraint[0]);
	}

	/**
	 * Selection in the workbench has been changed. We 
	 * can change the state of the 'real' action here
	 * if we want, but this can only happen after 
	 * the delegate has been created.
	 * @see IWorkbenchWindowActionDelegate#selectionChanged
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

	/**
	 * We can use this method to dispose of any system
	 * resources we previously allocated.
	 * @see IWorkbenchWindowActionDelegate#dispose
	 */
	public void dispose() {
	}

	/**
	 * We will cache window object in order to
	 * be able to provide parent shell for the message dialog.
	 * @see IWorkbenchWindowActionDelegate#init
	 */
	public void init(IWorkbenchWindow window) {
		this.window = window;
	}

}
