package es.esi.gemde.modelvalidator.ui;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import es.esi.gemde.modelvalidator.service.IValidationResult;

/**
 * The activator class controls the plug-in life cycle
 */
public class ModelValidatorUI extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "es.esi.gemde.modelvalidator.ui";

	// The shared instance
	private static ModelValidatorUI plugin;

	/**
	 * The constructor
	 */
	public ModelValidatorUI() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static ModelValidatorUI getDefault() {
		return plugin;
	}
	
	/**
	 * Shows a dialog containing the information on a {@link IValidationResult}
	 *
	 * @param res The {@link IValidationResult} instance to show.
	 */
	public static void showValidationResults(IValidationResult res) {
		if (res != null) {
			
			switch (res.getStatusCode()) {
			case IStatus.ERROR:
				//MessageDialog.openError(new Shell(), "Validation returned ERROR", message);
				ErrorDialog.openError(new Shell(), "Validation returned ERROR", null, res.asMultiStatus());
				break;
			case IStatus.WARNING:
				//MessageDialog.openWarning(new Shell(), "Validation returned WARNING", message);
				ErrorDialog.openError(new Shell(), "Validation returned WARNING", null, res.asMultiStatus());
				break;
			case IStatus.INFO:
				//MessageDialog.openInformation(new Shell(), "Validation returned INFO", message);
				ErrorDialog.openError(new Shell(), "Validation returned INFO", null, res.asMultiStatus());
				break;
			case IStatus.OK:
				MessageDialog.openInformation(new Shell(), "Validation successfully parsed", "Validation returned no errors.");
				break;
			}
		}
	}
}
