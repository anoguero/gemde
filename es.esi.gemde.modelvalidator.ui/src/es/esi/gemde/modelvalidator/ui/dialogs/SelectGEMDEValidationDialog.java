package es.esi.gemde.modelvalidator.ui.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import es.esi.gemde.core.CorePlugin;
import es.esi.gemde.core.resources.IGemdeResource;
import es.esi.gemde.modelvalidator.ModelValidatorPlugin;
import es.esi.gemde.modelvalidator.service.IBatchValidation;

public class SelectGEMDEValidationDialog extends Dialog {

	private Button okButton;
	private IBatchValidation validation;
	
	public SelectGEMDEValidationDialog(Shell parentShell) {
		super(parentShell);
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Select GEMDE Validation");
		newShell.setImage(CorePlugin.getImage(CorePlugin.VALIDATION_ICON));
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite topPanel = new Composite(parent, SWT.NONE);
		topPanel.setLayout(new GridLayout(1, true));
		
		Label label = new Label(topPanel, SWT.NONE);
		GridData labelData = new GridData(GridData.FILL_HORIZONTAL);
		labelData.horizontalSpan = 1;
		label.setLayoutData(labelData);
		label.setText("Available Validations:");
		
		TreeViewer tree = new TreeViewer(topPanel, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		GridData treeData = new GridData(GridData.FILL_BOTH);
		treeData.horizontalSpan = 1;
		tree.getTree().setLayoutData(treeData);
		
		tree.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				Object obj = ((StructuredSelection)event.getSelection()).getFirstElement();
				if (obj instanceof IBatchValidation) {
					validation = (IBatchValidation) obj;
				}
				checkCompleted();
			}
		});
		
		// Fill the tree with
		Tree t = tree.getTree();
		t.clearAll(true);
		for (IBatchValidation val : ModelValidatorPlugin.getServer().getBatchValidationRepository()) {
			TreeItem ti = new TreeItem(t, SWT.NONE);
			ti.setData(val);
			ti.setText(val.getName());
			if (val.getResourceType().equals(IGemdeResource.ResourceType.PROTECTED)) {
				ti.setImage(CorePlugin.getImage(CorePlugin.VALIDATION_ICON, CorePlugin.LOCK_OVERLAY, CorePlugin.BOTTOM_RIGHT));
			}
			else if (val.getResourceType().equals(IGemdeResource.ResourceType.NETWORKED)) {
				ti.setImage(CorePlugin.getImage(CorePlugin.VALIDATION_ICON, CorePlugin.NET_OVERLAY, CorePlugin.BOTTOM_RIGHT));
			}
			else {
				ti.setImage(CorePlugin.getImage(CorePlugin.VALIDATION_ICON));
			}
		}
		
		return parent;
	}

	
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// Only create an OK button
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		okButton = getButton(IDialogConstants.OK_ID);
		checkCompleted();
	}

	private void checkCompleted() {
		if (okButton != null) {
			okButton.setEnabled(validation != null);
		}
	}
	
	public IBatchValidation getSelectedValidation() {
		return validation;
	}
}
