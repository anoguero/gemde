/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.modelvalidator.ui.resources;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.XMLMemento;

import es.esi.gemde.modelvalidator.service.IBatchValidation;
import es.esi.gemde.modelvalidator.service.IModelValidatorConstraint;

/**
 * Utility class that generates the output XML files of an export action.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class Exporter {

	private static final String PLUGIN = "plugin";
	private static final String EXTENSION = "extension";
	private static final String POINT = "point";
	private static final String SESSIONDATA = "sessiondata";
	private static final String POINT_ID = "es.esi.gemde.modelvalidator.sessiondata";
	private static final String CATEGORY = "constraint_category";
	private static final String CONSTRAINT = "text_constraint";
	private static final String NAME = "name";
	private static final String ID = "id";
	private static final String BODY = "body";
	private static final String SEVERITY = "severity";
	private static final String VALIDATION = "validation";
	private static final String CONSTRAINT_REF = "constraint_reference";
	
	
	
	/**
	 * Static method that performs the export. This methods makes no checks, 
	 * so the previous steps in the export action must check the validity of
	 * the provided parameters!
	 * Note that if a validation is exported all its referred constraints 
	 * should be exported too.
	 * 
	 * @param outputPath the path where the XML file will be created.
	 * @param idPrefix a prefix that will be placed before the auto generated numeric ID for the constraints.
	 * @param constraints an array containing the constraints to be exported.
	 * @param validations an array containing the validations to be exported.
	 */
	public static void export (String outputPath, String idPrefix, IModelValidatorConstraint[] constraints, IBatchValidation[] validations) {
		HashMap<IModelValidatorConstraint, String> idMap = new HashMap<IModelValidatorConstraint, String>();
		XMLMemento memento = XMLMemento.createWriteRoot(PLUGIN);
		IMemento ext = memento.createChild(EXTENSION);
		ext.putString(POINT, POINT_ID);
		IMemento session = ext.createChild(SESSIONDATA);
		if (constraints != null && constraints.length > 0) {
			HashMap<String, IMemento> catMap = new HashMap<String, IMemento>();
			int i = 0;
			for (IModelValidatorConstraint constraint : constraints) {
				IMemento cat = catMap.get(constraint.getCategory()); 
				if (cat == null) {
					cat = session.createChild(CATEGORY);
					cat.putString(NAME, constraint.getCategory());
					catMap.put(constraint.getCategory(), cat);
				}
				IMemento cons = cat.createChild(CONSTRAINT);
				
				String id = idPrefix + "_" + String.format("%04d", i++);
				
				cons.putString(ID, id);
				cons.putString(NAME, constraint.getName());
				switch (constraint.getSeverity()) {
				case IStatus.ERROR:
					cons.putString(SEVERITY, "ERROR");
					break;
				case IStatus.WARNING:
					cons.putString(SEVERITY, "WARNING");
					break;
				case IStatus.INFO:
					cons.putString(SEVERITY, "INFO");
					break;
				default:
					cons.putString(SEVERITY, "ERROR");
					break;	
				}
				cons.putString(BODY, constraint.getBody());
				idMap.put(constraint, id);
			}
		}
		
		if (validations != null && validations.length > 0) {
			for (IBatchValidation validation : validations) {
				IMemento val = session.createChild(VALIDATION);
				val.putString(NAME, validation.getName());
				
				for (IModelValidatorConstraint cons : validation.getSelectedConstraints()) {
					IMemento ref = val.createChild(CONSTRAINT_REF);
					ref.putString(ID, idMap.get(cons));
				}
			}
		}
		
		try {
			memento.save(new FileWriter(new File(outputPath + "/plugin.xml")));
			MessageDialog.openInformation(null, "Export operation completed successfully!", "The required resources where correctly exported to: \n\t" + outputPath + "\\plugin.xml");
		} catch (IOException e) {
			e.printStackTrace();
			MessageDialog.openError(null, "An IO error occurred", "The export operation couldn't be completed due to an IOException: " + e.getMessage());
		}
			

	}
	
}
