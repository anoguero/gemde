/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.modelvalidator.ui.resources;

import java.util.List;
import java.util.Vector;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.actions.CompoundContributionItem;

import es.esi.gemde.modelvalidator.ModelValidatorPlugin;
import es.esi.gemde.modelvalidator.service.IBatchValidation;
import es.esi.gemde.modelvalidator.ui.actions.RunOtherValidationAction;
import es.esi.gemde.modelvalidator.ui.actions.RunValidationAction;
import es.esi.gemde.modelvalidator.ui.actions.SelectionUtils;

/**
 * A refinement of the {@link CompoundContributionItem} class to create the elements
 * of the of Run GEMDE Validation menu in the main Eclipse popup menu. 
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class RunValidationContributionItem extends CompoundContributionItem {


	/* (non-Javadoc)
	 * @see org.eclipse.ui.actions.CompoundContributionItem#getContributionItems()
	 */
	@Override
	protected IContributionItem[] getContributionItems() {
		Vector<IContributionItem> list = new Vector<IContributionItem>();
		
		List<IBatchValidation> validations = ModelValidatorPlugin.getServer().getBatchValidationRepository();
		EObject selection = SelectionUtils.getSelectedEObject();
		
		for (IBatchValidation valid : validations) {
			// Only show validations valid for the selected metamodel
			if (valid.getSelectedConstraints().get(0).getApplicationMetamodel().equals(selection.eClass().getEPackage())) {
				list.add(new ActionContributionItem(new RunValidationAction(valid)));
			}
		}
		
		if (list.size() > 0){ 
			list.add(new Separator("other"));
		}
		
		list.add(new ActionContributionItem(new RunOtherValidationAction()));
		
		return list.toArray(new IContributionItem[0]);
	}

}
