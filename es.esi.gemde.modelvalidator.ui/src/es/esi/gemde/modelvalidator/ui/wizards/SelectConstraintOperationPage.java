/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.modelvalidator.ui.wizards;

import java.sql.Date;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

/**
 * A wizard page that allows the user to define a query against a list of properties of an {@link EObject}.
 * This page may lead to itself if the selected query requires a further iteration to be fully defined.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class SelectConstraintOperationPage extends WizardPage {

	public static final String NAME = "Select Constraint Operation";
	private ListViewer queryList;
	private Label selectedInfoLabel;
	private ConstraintWizardData data;
	private OCLConstraintData currentLevelData;
	private Label sizeLabel, sizeErrorLabel;
	private Text sizeText;
	private Label valueLabel, valueErrorLabel;
	private Text valueText;
	private int level;
	private ListViewer propertiesViewer;
	private boolean isSizeOK, isValueOK;
	
	// Constructor
	/**
	 * Constructor of the page, requires the wizard data, the constraint data and the recursiveness level.
	 *
	 *@param wizData the wizard data
	 *@param data the constraint data in the current level
	 *@param level the current level
	 */
	public SelectConstraintOperationPage(ConstraintWizardData wizData, OCLConstraintData data, int level) {
		super(NAME, "Select Contraint Operation Page", null);
		this.data = wizData;
		this.currentLevelData = data;
		// Look for the data in the current level
		for (int i = 0; i < level-1; i++) {
			if (!currentLevelData.hasSubData()) {
				currentLevelData.subData = new OCLConstraintData();
			}
			currentLevelData = currentLevelData.subData;
		}
		this.level = level;
	}
	
	// Listeners
	PaintListener paintListener = new PaintListener() {

		@Override
		public void paintControl(PaintEvent e) {
			if (currentLevelData.constrainedObject != null) {
				selectedInfoLabel.setText("\tSelected EClass '" + currentLevelData.constrainedObject.getName() + "' of metamodel '" + data.constrainedMM.getName() + "'");
				if (currentLevelData.property == null) {
					queryList.getList().removeAll();
				}
				StructuredSelection currentSelection = (StructuredSelection)propertiesViewer.getSelection();
				propertiesViewer.getList().removeAll();
				propertiesViewer.add(currentLevelData.constrainedObject.getEAllStructuralFeatures().toArray());
				if (!currentSelection.isEmpty()) {
					propertiesViewer.setSelection(currentSelection, true);
				}
				
			}
		}
		
	};
	
	private ModifyListener sizeListener = new ModifyListener () {

		@Override
		public void modifyText(ModifyEvent e) {
			// Do something only if the sizeText is enabled
			if (sizeText.isVisible()) {
				try {
					int parsedValue = Integer.parseInt(sizeText.getText());
					if (parsedValue < 0) {
						sizeText.setForeground(new Color (sizeText.getFont().getDevice(), 255, 0, 0));
						sizeErrorLabel.setText("The selected value must be a positive integer value.");
						isSizeOK = false;
					}
					else {
						sizeText.setForeground(new Color (sizeText.getFont().getDevice(), 0, 0, 0));
						sizeErrorLabel.setText("");
						currentLevelData.sizeValue = parsedValue;
						isSizeOK = true;
					}
				}
				catch (NumberFormatException nfe) {
					sizeText.setForeground(new Color (sizeText.getFont().getDevice(), 255, 0, 0));
					sizeErrorLabel.setText("The selected value must be a positive integer value.");
					isSizeOK = false;
				}
			}
			else {
				sizeErrorLabel.setText("");
				isSizeOK = false;
			}
			
			// Update finish and page complete
			checkCompleteness();
		}
		
	};
	
	private ModifyListener valueListener = new ModifyListener () {

		@Override
		public void modifyText(ModifyEvent e) {
			// Do something only is the valueText is enabled
			if (valueText.isVisible()) {
				String type = currentLevelData.property.getEType().getName();
				if (type.equals("EShort") || type.equals("EShortObject")) {
					try {
						Short.parseShort(valueText.getText());
						valueText.setForeground(new Color (sizeText.getFont().getDevice(), 0, 0, 0));
						valueErrorLabel.setText("");
						currentLevelData.value = valueText.getText();
						isValueOK = true;
					}
					catch (NumberFormatException nfe) {
						valueText.setForeground(new Color (sizeText.getFont().getDevice(), 255, 0, 0));
						valueErrorLabel.setText("The selected value must be a short integer value.");
						isValueOK = false;
					}
				}
				else if (type.equals("EInt") || type.equals("EIntegerObject")) {
					try {
						Integer.parseInt(valueText.getText());
						valueText.setForeground(new Color (sizeText.getFont().getDevice(), 0, 0, 0));
						valueErrorLabel.setText("");
						currentLevelData.value = valueText.getText();
						isValueOK = true;
					}
					catch (NumberFormatException nfe) {
						valueText.setForeground(new Color (sizeText.getFont().getDevice(), 255, 0, 0));
						valueErrorLabel.setText("The selected value must be an integer value.");
						isValueOK = false;
					}
				}
				else if (type.equals("ELong") || type.equals("ELongObject") || type.equals("EBigInteger")) {
					try {
						Long.parseLong(valueText.getText());
						valueText.setForeground(new Color (sizeText.getFont().getDevice(), 0, 0, 0));
						valueErrorLabel.setText("");
						currentLevelData.value = valueText.getText();
						isValueOK = true;
					}
					catch (NumberFormatException nfe) {
						valueText.setForeground(new Color (sizeText.getFont().getDevice(), 255, 0, 0));
						valueErrorLabel.setText("The selected value must be a long integer value.");
						isValueOK = false;
					}
				}
				else if (type.equals("EByte") || type.equals("EByteObject")) {
					try {
						Byte.parseByte(valueText.getText());
						valueText.setForeground(new Color (sizeText.getFont().getDevice(), 0, 0, 0));
						valueErrorLabel.setText("");
						currentLevelData.value = valueText.getText();
						isValueOK = true;
					}
					catch (NumberFormatException nfe) {
						valueText.setForeground(new Color (sizeText.getFont().getDevice(), 255, 0, 0));
						valueErrorLabel.setText("The selected value must be a byte value.");
						isValueOK = false;
					}
				}
				else if (type.equals("EFloat") || type.equals("EFloatObject")) {
					try {
						Float.parseFloat(valueText.getText());
						valueText.setForeground(new Color (sizeText.getFont().getDevice(), 0, 0, 0));
						valueErrorLabel.setText("");
						currentLevelData.value = valueText.getText();
						isValueOK = true;
					}
					catch (NumberFormatException nfe) {
						valueText.setForeground(new Color (sizeText.getFont().getDevice(), 255, 0, 0));
						valueErrorLabel.setText("The selected value must be a float value.");
						isValueOK = false;
					}
				}
				else if (type.equals("EDouble") || type.equals("EDoubleObject") || type.equals("EBigDecimal")) {
					try {
						Double.parseDouble(valueText.getText());
						valueText.setForeground(new Color (sizeText.getFont().getDevice(), 0, 0, 0));
						valueErrorLabel.setText("");
						currentLevelData.value = valueText.getText();
						isValueOK = true;
					}
					catch (NumberFormatException nfe) {
						valueText.setForeground(new Color (sizeText.getFont().getDevice(), 255, 0, 0));
						valueErrorLabel.setText("The selected value must be a double value.");
						isValueOK = false;
					}
				}
				else if (type.equals("EChar") || type.equals("ECharacterObject")) {
					if (valueText.getText().length() == 1) {
						valueText.setForeground(new Color (sizeText.getFont().getDevice(), 0, 0, 0));
						valueErrorLabel.setText("");
						currentLevelData.value = valueText.getText();
						isValueOK = true;
					}
					else {
						valueText.setForeground(new Color (sizeText.getFont().getDevice(), 255, 0, 0));
						valueErrorLabel.setText("The selected value must be a character.");
						isValueOK = false;
					}
				}
				else if (type.equals("EDate")) {
					try {
						Date.valueOf(valueText.getText());
						valueText.setForeground(new Color (sizeText.getFont().getDevice(), 0, 0, 0));
						valueErrorLabel.setText("");
						currentLevelData.value = "'" + valueText.getText() + "'";
						isValueOK = true;
					}
					catch (IllegalArgumentException iae) {
						valueText.setForeground(new Color (sizeText.getFont().getDevice(), 255, 0, 0));
						valueErrorLabel.setText("The selected value must be a date value using the format 'yyyy-mm-dd'.");
						isValueOK = false;
					}
				}
				else if (type.equals("EBoolean") || type.equals("EBooleanObject")) {
					if (Boolean.parseBoolean(valueText.getText())) {
						valueText.setForeground(new Color (sizeText.getFont().getDevice(), 0, 0, 0));
						valueErrorLabel.setText("");
						currentLevelData.value = valueText.getText();
						isValueOK = true;
					}
					else {
						valueText.setForeground(new Color (sizeText.getFont().getDevice(), 255, 0, 0));
						valueErrorLabel.setText("The selected value must be a boolean value (true or false).");
						isValueOK = false;
					}
				}
				else {
					if (!type.equals("EString")) {
						valueErrorLabel.setText("WARNING: Value type not supported. No parsing will be done.");
					}
					currentLevelData.value = "'" + valueText.getText() + "'";
					isValueOK = true;
				}
			}
			else {
				valueErrorLabel.setText("");
				isValueOK = false;
			}
			
			// Update finish and page complete
			checkCompleteness();
			
		}
		
	};
	
	private ISelectionChangedListener selectPropertyListener = new ISelectionChangedListener() {

		@Override
		public void selectionChanged(SelectionChangedEvent event) {
			EStructuralFeature selection = (EStructuralFeature) ((StructuredSelection) propertiesViewer.getSelection()).getFirstElement();
			StructuredSelection previousSelection = null;
			
			if (currentLevelData.property != null && currentLevelData.property.equals(selection)) {
				previousSelection = (StructuredSelection)queryList.getSelection();
			}
			else {	
				currentLevelData.property = selection;
//				sizeText.setText("");
//				valueText.setText("");
			}
			queryList.getList().removeAll();
			if (selection != null) {
				
				int min = currentLevelData.property.getLowerBound();
				int max = currentLevelData.property.getUpperBound();
				
				if (max == 1) {
					if (min == 0) {
						queryList.add(new Integer(OCLConstraintData.OP_IS_NULL));
						queryList.add(new Integer(OCLConstraintData.OP_IS_NOT_NULL));
					}
					queryList.add(new Integer(OCLConstraintData.OP_HAS_VALUE));
					queryList.add(new Integer(OCLConstraintData.OP_HAS_NOT_VALUE));
				}
				else {
					if (min == 0) {
						queryList.add(new Integer(OCLConstraintData.OP_IS_EMPTY));
						queryList.add(new Integer(OCLConstraintData.OP_IS_NOT_EMPTY));
					}
					if (min != max) {
						queryList.add(new Integer(OCLConstraintData.OP_SIZE_EQUAL));
						queryList.add(new Integer(OCLConstraintData.OP_SIZE_GREATER));
						queryList.add(new Integer(OCLConstraintData.OP_SIZE_GREATER_EQUAL));
						queryList.add(new Integer(OCLConstraintData.OP_SIZE_LOWER));
						queryList.add(new Integer(OCLConstraintData.OP_SIZE_LOWER_EQUAL));
					}
					queryList.add(new Integer(OCLConstraintData.OP_ELEMENT_HAS_VALUE));
					queryList.add(new Integer(OCLConstraintData.OP_ELEMENT_HAS_NOT_VALUE));
					queryList.add(new Integer(OCLConstraintData.OP_CONTAINS_ELEMENT_WITH_VALUE));
					queryList.add(new Integer(OCLConstraintData.OP_NOT_CONTAINS_ELEMENT_WITH_VALUE));
				}
			}
			
			if (previousSelection != null && !previousSelection.isEmpty()) {
				queryList.setSelection(previousSelection, true);
			}
			else {
				queryList.setSelection(new StructuredSelection());
				data.canFinish = false;
				setPageComplete(false);
			}
		}
		
		
	};
	
	private ISelectionChangedListener selectOperationListener = new ISelectionChangedListener() {

		@Override
		public void selectionChanged(SelectionChangedEvent event) {
			if (((StructuredSelection) queryList.getSelection()).getFirstElement() instanceof Integer) {
				
				int currentSel = (Integer)((StructuredSelection) queryList.getSelection()).getFirstElement();
				boolean updateNeeded = false;
				
				if (currentSel != currentLevelData.opCode) {
					currentLevelData.opCode = currentSel;
					updateNeeded = true;
				}
				
				
				if (currentLevelData.opCode == OCLConstraintData.OP_IS_EMPTY || currentLevelData.opCode == OCLConstraintData.OP_IS_NOT_EMPTY || currentLevelData.opCode == OCLConstraintData.OP_IS_NULL || currentLevelData.opCode == OCLConstraintData.OP_IS_NOT_NULL) {
					sizeLabel.setText("");
					sizeText.setVisible(false);
					valueLabel.setText("");
					valueText.setVisible(false);
					
					data.canFinish = true;
					setPageComplete(false);
				}
				else if (currentLevelData.opCode == OCLConstraintData.OP_SIZE_EQUAL || currentLevelData.opCode == OCLConstraintData.OP_SIZE_GREATER || currentLevelData.opCode == OCLConstraintData.OP_SIZE_LOWER || currentLevelData.opCode == OCLConstraintData.OP_SIZE_GREATER_EQUAL || currentLevelData.opCode == OCLConstraintData.OP_SIZE_LOWER_EQUAL) {
					sizeLabel.setText("Size Value:");
					sizeText.setVisible(true);
					valueLabel.setText("");
					valueText.setVisible(false);
					
					data.canFinish = false;
					setPageComplete(false);
				}
				else if ((currentLevelData.opCode == OCLConstraintData.OP_CONTAINS_ELEMENT_WITH_VALUE || currentLevelData.opCode == OCLConstraintData.OP_HAS_VALUE || currentLevelData.opCode == OCLConstraintData.OP_HAS_NOT_VALUE || currentLevelData.opCode == OCLConstraintData.OP_NOT_CONTAINS_ELEMENT_WITH_VALUE) && !(currentLevelData.property.getEType() instanceof EClass)) {
					sizeLabel.setText("");
					sizeText.setVisible(false);
					valueLabel.setText("Value:");
					valueText.setVisible(true);
					
					data.canFinish = false;
					setPageComplete(false);
				}
				else if ((currentLevelData.opCode == OCLConstraintData.OP_ELEMENT_HAS_VALUE || currentLevelData.opCode == OCLConstraintData.OP_ELEMENT_HAS_NOT_VALUE) && !(currentLevelData.property.getEType() instanceof EClass)) {
					sizeLabel.setText("Position:");
					sizeText.setVisible(true);
					valueLabel.setText("Value:");
					valueText.setVisible(true);
					
					data.canFinish = false;
					setPageComplete(false);
				}
				else if (currentLevelData.opCode == OCLConstraintData.OP_ELEMENT_HAS_VALUE || currentLevelData.opCode == OCLConstraintData.OP_ELEMENT_HAS_NOT_VALUE) {
					
					if (currentLevelData.subData == null) {
						currentLevelData.subData = new OCLConstraintData();
					}
					currentLevelData.subData.constrainedObject = (EClass) currentLevelData.property.getEType();
					
					sizeLabel.setText("Position:");
					sizeText.setVisible(true);
					valueLabel.setText("");
					valueText.setVisible(false);
					
					data.canFinish = false;
					setPageComplete(false);
				}
				else {
					// Create a new level of data if needed
					if (currentLevelData.subData == null) {
						currentLevelData.subData = new OCLConstraintData();
					}
					currentLevelData.subData.constrainedObject = (EClass) currentLevelData.property.getEType();
					
					sizeLabel.setText("");
					sizeText.setVisible(false);
					valueLabel.setText("");
					valueText.setVisible(false);
					
					data.canFinish = false;
					setPageComplete(true);
				}
				
				if (updateNeeded) {
					sizeText.setText("");
					valueText.setText("");
				}
				
			}
			else {
				sizeLabel.setText("");
				sizeText.setVisible(false);
				valueLabel.setText("");
				valueText.setVisible(false);
				sizeText.setText("");
				valueText.setText("");
				
				setPageComplete(false);
			}
		}
		
	};

	// Overriden createControl() method
	@Override
	public void createControl(Composite parent) {
		// Create the controls
		Composite topPanel = new Composite(parent, SWT.NONE);
		topPanel.setLayout(new GridLayout(6, true));
		topPanel.setSize(400, 600);
		
		Label selectedTitleLabel = new Label(topPanel, SWT.NONE);
		selectedTitleLabel.setText("Selected element:");
		GridData selectedTitleLabelData = new GridData();
		selectedTitleLabelData.horizontalSpan = 6;
		selectedTitleLabel.setLayoutData(selectedTitleLabelData);
		
		selectedInfoLabel = new Label(topPanel, SWT.NONE);
		selectedInfoLabel.setText("");
		GridData selectedInfoLabelData = new GridData(GridData.FILL_HORIZONTAL);
		selectedInfoLabelData.horizontalSpan = 6;
		selectedInfoLabel.setLayoutData(selectedInfoLabelData);
		
		Label propertiesLabel = new Label(topPanel, SWT.NONE);
		GridData propertiesLabelData = new GridData();
		propertiesLabelData.horizontalSpan = 3;
		propertiesLabel.setText("Properties");
		propertiesLabel.setLayoutData(propertiesLabelData);
		
		Label operationsLabel = new Label(topPanel, SWT.NONE);
		GridData operationsLabelData = new GridData();
		operationsLabelData.horizontalSpan = 3;
		operationsLabel.setText("Operations");
		operationsLabel.setLayoutData(operationsLabelData);
		
		propertiesViewer = new ListViewer(topPanel, SWT.WRAP | SWT.SINGLE | SWT.BORDER | SWT.V_SCROLL| SWT.H_SCROLL);
		GridData propertiesViewerData = new GridData(GridData.FILL_BOTH);
		propertiesViewerData.horizontalSpan = 3;
		propertiesViewer.getList().setLayoutData(propertiesViewerData);
		propertiesViewer.setLabelProvider(propertiesLabelProvider);
		
		queryList = new ListViewer(topPanel, SWT.WRAP | SWT.SINGLE | SWT.BORDER | SWT.V_SCROLL| SWT.H_SCROLL);
		GridData queryListData = new GridData(GridData.FILL_BOTH);
		queryListData.horizontalSpan = 3;
		queryList.getList().setLayoutData(queryListData);
		queryList.setLabelProvider(opsLabelProvider);
		
		sizeLabel = new Label(topPanel, SWT.RIGHT);
		GridData sizeLabelData = new GridData(GridData.FILL_HORIZONTAL);
		sizeLabelData.horizontalSpan = 1;
		sizeLabel.setLayoutData(sizeLabelData);
		sizeLabel.setText("");
		
		sizeText = new Text (topPanel, SWT.LEFT | SWT.SINGLE | SWT.BORDER);
		GridData sizeTextData = new GridData(GridData.FILL_HORIZONTAL);
		sizeTextData.horizontalSpan = 2;
		sizeText.setLayoutData(sizeTextData);
		sizeText.setVisible(false);
		
		valueLabel = new Label(topPanel, SWT.RIGHT);
		GridData valueLabelData = new GridData(GridData.FILL_HORIZONTAL);
		valueLabelData.horizontalSpan = 1;
		valueLabel.setLayoutData(valueLabelData);
		valueLabel.setText("");
		
		valueText = new Text (topPanel, SWT.LEFT | SWT.SINGLE | SWT.BORDER);
		GridData valueTextData = new GridData(GridData.FILL_HORIZONTAL);
		valueTextData.horizontalSpan = 2;
		valueText.setLayoutData(valueTextData);
		valueText.setVisible(false);
		
		sizeErrorLabel = new Label(topPanel, SWT.RIGHT);
		GridData sizeErrorLabelData = new GridData(GridData.FILL_HORIZONTAL);
		sizeErrorLabelData.horizontalSpan = 3;
		sizeErrorLabel.setLayoutData(sizeErrorLabelData);
		sizeErrorLabel.setText("");
		
		valueErrorLabel = new Label(topPanel, SWT.RIGHT);
		GridData valueErrorLabelData = new GridData(GridData.FILL_HORIZONTAL);
		valueErrorLabelData.horizontalSpan = 3;
		valueErrorLabel.setLayoutData(valueErrorLabelData);
		valueErrorLabel.setText("");
		
		// Add listeners
		topPanel.addPaintListener(paintListener);
		propertiesViewer.addSelectionChangedListener(selectPropertyListener);
		queryList.addSelectionChangedListener(selectOperationListener);
		sizeText.addModifyListener(sizeListener);
		valueText.addModifyListener(valueListener);
		
		// Configure page title, description, ...
		setTitle("Select the constraint operation. Level " + level);
		setDescription("Select the element property and the constraint operation that will be applied to the selected model element.");
		
		// Set controls
		setControl(topPanel);
		setPageComplete(false);
		
	}
	
	// Label providers for the ListViewers
	private LabelProvider opsLabelProvider = new LabelProvider(){
		
		public Image getImage(Object element) {
			return null;
		}
		public String getText(Object element) {
			Integer opCode = (Integer) element;
			switch (opCode.intValue()) {
			case OCLConstraintData.OP_CONTAINS_ELEMENT_WITH_VALUE:
				return "Contains an element with value ...";
			case OCLConstraintData.OP_NOT_CONTAINS_ELEMENT_WITH_VALUE:
				return "Doesn't contain an element with value ...";
			case OCLConstraintData.OP_ELEMENT_HAS_VALUE:
				return "Element at position ... has value ...";
			case OCLConstraintData.OP_HAS_VALUE:
				return "Has value ...";
			case OCLConstraintData.OP_IS_EMPTY:
				return "Is empty";
			case OCLConstraintData.OP_IS_NOT_EMPTY:
				return "Is not empty";
			case OCLConstraintData.OP_IS_NOT_NULL:
				return "Is not null";
			case OCLConstraintData.OP_IS_NULL:
				return "Is null";
			case OCLConstraintData.OP_SIZE_EQUAL:
				return "Has size value equal to ...";
			case OCLConstraintData.OP_SIZE_GREATER:
				return "Has size value greater than ...";
			case OCLConstraintData.OP_SIZE_LOWER:
				return "Has size value lower than ...";
			case OCLConstraintData.OP_SIZE_GREATER_EQUAL:
				return "Has size value greater or equal to ...";
			case OCLConstraintData.OP_SIZE_LOWER_EQUAL:
				return "Has size value lower or equal to ...";
			case OCLConstraintData.OP_HAS_NOT_VALUE:
				return "Has value different from ...";
			case OCLConstraintData.OP_ELEMENT_HAS_NOT_VALUE:
				return "Element at position ... has value different from ...";
			default:
				return "Unknown operation";
			}
		}
	};
	
	private LabelProvider propertiesLabelProvider = new LabelProvider(){
		public Image getImage(Object element) {
			return null;
		}
		public String getText(Object element) {
			String result, maxStr;
			int minMult = ((EStructuralFeature)element).getLowerBound(), maxMult = ((EStructuralFeature)element).getUpperBound();
			result = ((EStructuralFeature) element).getName() + " : " + ((EStructuralFeature) element).getEType().getName();
			if (((EObject)element).eClass().getName().equals("EReference")) {
				result = ">" + result;
			}
			if (maxMult == EStructuralFeature.UNBOUNDED_MULTIPLICITY) {
				maxStr = "*";
			}
			else if (maxMult == EStructuralFeature.UNSPECIFIED_MULTIPLICITY) {
				maxStr = "?";
			}
			else {
				maxStr = String.valueOf(maxMult);
			}
			
			if (minMult != maxMult) {
				result = result.concat(" [" + minMult + ".." + maxStr + "]");
			}
			else {
				result = result.concat(" [" + minMult + "]");
			}
			
			
			return (result);
		}
	};
	
	/**
	 * Internal method called every time a listener is activated.
	 */
	private void checkCompleteness() {
		switch (currentLevelData.opCode) {
		case OCLConstraintData.OP_IS_EMPTY:
		case OCLConstraintData.OP_IS_NOT_EMPTY:
		case OCLConstraintData.OP_IS_NULL:
		case OCLConstraintData.OP_IS_NOT_NULL:
			data.canFinish = true;
			setPageComplete(false);
			break;
		case OCLConstraintData.OP_HAS_VALUE:
		case OCLConstraintData.OP_HAS_NOT_VALUE:
		case OCLConstraintData.OP_CONTAINS_ELEMENT_WITH_VALUE:
		case OCLConstraintData.OP_NOT_CONTAINS_ELEMENT_WITH_VALUE:
			if (currentLevelData.property.getEType() instanceof EClass) {
				data.canFinish = false;
				setPageComplete(true);
			}
			else {
				if (isValueOK) {
					data.canFinish = true;
				}
				else {
					data.canFinish = false;
				}
				setPageComplete(false);
			}
			break;
		case OCLConstraintData.OP_SIZE_EQUAL:
		case OCLConstraintData.OP_SIZE_GREATER:
		case OCLConstraintData.OP_SIZE_GREATER_EQUAL:
		case OCLConstraintData.OP_SIZE_LOWER:
		case OCLConstraintData.OP_SIZE_LOWER_EQUAL:
			if (isSizeOK) {
				data.canFinish = true;
			}
			else {
				data.canFinish = false;
			}
			setPageComplete(false);
			break;
		case OCLConstraintData.OP_ELEMENT_HAS_VALUE:
		case OCLConstraintData.OP_ELEMENT_HAS_NOT_VALUE:
			if (currentLevelData.property.getEType() instanceof EClass) {
				data.canFinish = false;
				if (isSizeOK) {
					setPageComplete(true);
				}
				else {
					setPageComplete(false);
				}
			}
			else {
				if (isSizeOK && isValueOK) {
					data.canFinish = true;
				}
				else {
					data.canFinish = false;
				}
				setPageComplete(false);
			}
			break;
		default:
			data.canFinish = false;
			setPageComplete(false);
		}
	}
}
