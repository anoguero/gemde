/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/

package es.esi.gemde.modelvalidator.ui.wizards;

import java.util.Vector;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

/**
 * Wizard to easily define OCL constraints.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class OCLConstraintCreationWizard extends Wizard {

	private String resultingOCL;
	private OCLConstraintData constraintData;
	private ConstraintWizardData wizData;
	private Vector<IWizardPage> pages;
	private int position;
	
	/**
	 * Initializes the wizard using the default configuration.
	 *
	 */
	public OCLConstraintCreationWizard() {
		super();
		this.resultingOCL = null;
		this.setHelpAvailable(false);
		constraintData = new OCLConstraintData();
		wizData = new ConstraintWizardData();
		position = 0;
		pages = new Vector<IWizardPage>();
		setWindowTitle("Create a new Constraint");
		
	}
	
	/**
	 * Initializes the wizard with an already selected category.
	 *
	 *@param category the selected textual category
	 */
	public OCLConstraintCreationWizard(String category) {
		this();
		wizData.category = category;
	}
	
	/**
	 * Initializes the wizard with the data of a constraint for editing.
	 *
	 *@param name the name of the constraint to be edited.
	 *@param category the category of the constraint to be edited.
	 *@param metamodel the metamodel to which the constraint is related.
	 *@param context the EClass to which this constraint is applied.
	 *@param ocl the plain OCL body of the constraint.
	 */
	public OCLConstraintCreationWizard(String name, String category, int severity, EPackage metamodel, EClass context, String ocl) {
		this();
		wizData.name = name;
		wizData.category = category;
		wizData.constrainedMM = metamodel;
		constraintData.constrainedObject = context;
		wizData.plainOCL = getPlainOCL(ocl);
		wizData.severity = severity;
	}
	
	private String getPlainOCL(String ocl) {
		return ocl.substring(ocl.indexOf("inv:")+4, ocl.indexOf("endpackage")).replaceAll("\n", "").trim();
	}

	@Override
	public int getPageCount() {
		return pages.size();
	}

	@Override
	public IWizardPage getPreviousPage(IWizardPage page) {
		position = pages.indexOf(page);
		if (position == 0) {
			return null;
		}
		else {
			return pages.elementAt(position-1);
		}
	}

	@Override
	public IWizardPage getNextPage(IWizardPage page) {
		position = pages.indexOf(page);
		// Get the next page and add another one!!
		if (pages.size() >= (position+1)) {
			
			if (pages.size() == (position+1)) {
				addPage(new SelectConstraintOperationPage(wizData, constraintData, position+1));
				
			}
			return pages.elementAt(position+1);
		}
		else {
			return null;
		}
	}
	
	@Override
	public boolean performFinish() {
		if (wizData.createdOCLString != null) {
			resultingOCL = wizData.createdOCLString;
		}
		else {
			// Create the OCL sentence from the information gathered
			resultingOCL = new String();
//			final String constraintName = "example_constraint";
			
			// Create the heading
			resultingOCL += "package " + wizData.constrainedMM.getName() + "\n";
			resultingOCL += "context " + constraintData.constrainedObject.getName() + "\n";
			resultingOCL += "inv: ";
			
			// Create the query recursively
			boolean end = false;
			while (!end) {
				resultingOCL = insertInPosition(resultingOCL, createQuery(constraintData));
				if (constraintData.hasSubData()) {
					constraintData = constraintData.subData;
				}
				else {
					end = true;
					resultingOCL += "\n";
				}
			}
			
			// Create the closing
			resultingOCL += "endpackage";
		}
		return true;
	}

	/**
	 * Internal method used to insert the next piece of the OCL string in the required position.
	 * 
	 * @param ocl target OCL string
	 * @param query the new query to be added.
	 * @return the resulting string after the insertion.
	 */
	private String insertInPosition(String ocl, String query) {
		if (ocl.indexOf("##pos##") != -1) {
			return ocl.replaceFirst("##pos##", query);
		}
		else {
			return ocl.concat(query);
		}
		
	}

	/**
	 * Internal method used to create the OCL query from the OCLConstraintData captured by the wizard.
	 * 
	 * @param data the OCLConstraintData instance.
	 * @return the created OCL query as a String.
	 */
	private String createQuery(OCLConstraintData data) {
		String query = new String();
		query += data.property.getName();
		switch (data.opCode) {
		case OCLConstraintData.OP_IS_EMPTY:
			query += "->isEmpty()";
			break;
		case OCLConstraintData.OP_IS_NOT_EMPTY:
			query += "->notEmpty()";
			break;
		case OCLConstraintData.OP_IS_NULL:
			query += " = null";
			break;
		case OCLConstraintData.OP_IS_NOT_NULL:
			query += " <> null";
			break;
		case OCLConstraintData.OP_SIZE_EQUAL:
			query += "->size() = " + data.sizeValue;
			break;
		case OCLConstraintData.OP_SIZE_GREATER:
			query += "->size() > " + data.sizeValue;
			break;
		case OCLConstraintData.OP_SIZE_GREATER_EQUAL:
			query += "->size() >= " + data.sizeValue;
			break;
		case OCLConstraintData.OP_SIZE_LOWER:
			query += "->size() < " + data.sizeValue;
			break;
		case OCLConstraintData.OP_SIZE_LOWER_EQUAL:
			query += "->size() <= " + data.sizeValue;
			break;
		case OCLConstraintData.OP_HAS_VALUE:
			if (data.property.getEType() instanceof EClass) {
				query += ".##pos##";
			}
			else {
				query += " = " + data.value;
			}
			break;
		case OCLConstraintData.OP_HAS_NOT_VALUE:
			if (data.property.getEType() instanceof EClass) {
				query += ".##pos##";
			}
			else {
				query += " <> " + data.value;
			}
			break;
		case OCLConstraintData.OP_CONTAINS_ELEMENT_WITH_VALUE:
			if (data.property.getEType() instanceof EClass) {
				query += "->select(##pos##)->notEmpty()";
			}
			else {
				query += "->select(p | p = " + data.value + ")->notEmpty()";
			}
			break;
		case OCLConstraintData.OP_NOT_CONTAINS_ELEMENT_WITH_VALUE:
			if (data.property.getEType() instanceof EClass) {
				query += "->select(##pos##)->isEmpty()";
			}
			else {
				query += "->select(p | p = " + data.value + ")->isEmpty()";
			}
			break;
		case OCLConstraintData.OP_ELEMENT_HAS_VALUE:
			if (data.property.getEType() instanceof EClass) {
				query += "->asSequence()->at("+ data.sizeValue +").##pos##";
			}
			else {
				query += "->asSequence()->at("+ data.sizeValue +") = " + data.value;
			}
			break;
		case OCLConstraintData.OP_ELEMENT_HAS_NOT_VALUE:
			if (data.property.getEType() instanceof EClass) {
				query += "->asSequence()->at("+ data.sizeValue +").##pos##";
			}
			else {
				query += "->asSequence()->at("+ data.sizeValue +") <> " + data.value;
			}
			break;
		default:
				
			break;
		}
		return query;
	}

	@Override
	public void addPages() {
		addPage(new SelectModelElementPage(wizData, constraintData));
		addPage(new SelectConstraintOperationPage(wizData, constraintData, 1));
	}

	@Override
	public boolean canFinish() {
		return wizData.canFinish;
	}
	
	/**
	 * Returns the generated OCL string.
	 * 
	 * @return the OCL string created by the wizard.
	 */
	public String getOCL () {
		return resultingOCL;
	}
	
	/**
	 * Returns the OCLConstraintData instance used by the wizard to create the OCL constraint.
	 * 
	 * @return the OCLConstraintData instance.
	 */
	public OCLConstraintData getData () {
		return constraintData;
	}
	
	/**
	 * Return the name of the created constraint.
	 * 
	 * @return the name of the constraint.
	 */
	public String getConstraintName () {
		return wizData.name;
	}
	
	/**
	 * Return the category of the created constraint.
	 * 
	 * @return the category of the constraint.
	 */
	public String getConstraintCategory () {
		return wizData.category;
	}
	
	/**
	 * Return the severity code of the created constraint as defined in {@link org.eclipse.core.runtime.IStatus}.
	 * 
	 * @return the severity of the constraint.
	 */
	public int getConstraintSeverity() {
		return wizData.severity;
	}

	@Override
	public IWizardPage[] getPages() {
		return pages.toArray(new IWizardPage[0]);
	}

	@Override
	public IWizardPage getStartingPage() {
		return super.getStartingPage();
	}

	@Override
	public void addPage(IWizardPage page) {
		pages.add(page);
		super.addPage(page);
	}

}
