/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.modelvalidator.ui.wizards;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EPackage;

/**
 * A class containing the internal data used by the {@link es.esi.gemde.modelvalidator.ui.wizards.OCLConstraintCreationWizard} wizard.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class ConstraintWizardData {

	// Wizard data values
	public boolean canFinish = false;
	public String createdOCLString = null;
	public int complexityLevel = 0;
	public EPackage constrainedMM;
	public String name;
	public String category;
	public int severity = IStatus.ERROR;
	public String plainOCL = null;
}
