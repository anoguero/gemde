/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.modelvalidator.ui.wizards;

import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.ocl.examples.interpreter.console.IOCLFactory;
import org.eclipse.emf.ocl.examples.interpreter.console.ModelingLevel;
import org.eclipse.emf.ocl.examples.interpreter.console.text.ColorManager;
import org.eclipse.emf.ocl.examples.interpreter.console.text.OCLDocument;
import org.eclipse.emf.ocl.examples.interpreter.console.text.OCLSourceViewer;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.ocl.OCL;
import org.eclipse.ocl.OCLInput;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.Constraint;
import org.eclipse.ocl.ecore.EcoreEnvironmentFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import es.esi.gemde.modelvalidator.ui.dialogs.MetamodelSelectionDialog;

/**
 * A Wizard page that allows the user to define the name and category of a new constraint, the metamodel to which the constraint is applicable and the target {@link EObject}. 
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class SelectModelElementPage extends WizardPage {

	public static final String NAME = "Select Model Element";
	
	private Text nameText;
	private Text categoryText;
	private TreeViewer modelViewer;
	private OCLConstraintData data;
	private ConstraintWizardData wizData;
	private Label mmText;
	private Button manualOCLButton;
	private boolean oclOK;
	private Combo severityCombo;
	
	// OCL Editor variables
	private OCLSourceViewer oclEditor;
	private OCLDocument document;
	private ColorManager colorManager;
	private IOCLFactory<Object> oclFactory = new EcoreOCLFactory();
	private ModelingLevel modelingLevel = ModelingLevel.M2;
	

	
	// Listener Classes
	// When an object is selected the properties list is populated
	private ISelectionChangedListener selectObjectListener = new ISelectionChangedListener() {

		@Override
		public void selectionChanged(SelectionChangedEvent event) {
			EClass selection = (EClass) ((StructuredSelection) modelViewer.getSelection()).getFirstElement();
			if (selection instanceof EClass) {
				
				if (data.constrainedObject == null || !data.constrainedObject.equals(selection) ) {
					data.constrainedObject = selection;
					manualOCLButton.setSelection(false);
					oclEditor.getTextWidget().setText("");
					oclEditor.getTextWidget().setEnabled(false);
					oclEditor.getTextWidget().setVisible(false);
				}
				
				
				data.property = null;
				
				manualOCLButton.setEnabled(true);
				
			}
			else {
				data.constrainedObject = null;
				data.property = null;
				manualOCLButton.setSelection(false);
				manualOCLButton.setEnabled(false);
				
			}
			
			document.setOCLContext(selection);
			updatePageStatus();
		}

	};
	
	private SelectionListener oclButtonListener = new SelectionListener() {

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			// Not used
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			if (manualOCLButton.isEnabled() && manualOCLButton.getSelection()) {
				oclEditor.getTextWidget().setEnabled(true);
				oclEditor.getTextWidget().setVisible(true);
			}
			else {
				oclEditor.getTextWidget().setText("");
				oclEditor.getTextWidget().setEnabled(false);
				oclEditor.getTextWidget().setVisible(false);
			}
		}
	};
	
	private SelectionListener comboListener = new SelectionListener () {

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			// Not used			
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			if (severityCombo.getSelectionIndex() == 0) {
				// ERROR is selected
				wizData.severity = IStatus.ERROR;
			}
			else if (severityCombo.getSelectionIndex() == 1) {
				// WARNING is selected
				wizData.severity = IStatus.WARNING;
			}
			else {
				// INFO is selected
				wizData.severity = IStatus.INFO;
			}
		}
		
	};
	
	private ModifyListener nameModifyListener = new ModifyListener () {

		@Override
		public void modifyText(ModifyEvent e) {
			wizData.name = nameText.getText();
			updatePageStatus();
		}
		
	};
	
	private ModifyListener catModifyListener = new ModifyListener () {

		@Override
		public void modifyText(ModifyEvent e) {
			wizData.category = categoryText.getText();
			updatePageStatus();
			
		}
		
	};
	
	private ModifyListener oclModifyListener = new ModifyListener () {

		@Override
		public void modifyText(ModifyEvent e) {
			if (oclEditor.getTextWidget().isEnabled() && oclEditor.getTextWidget().getText() != "") {
				String fullOCLQuery = "package " + wizData.constrainedMM.getName() +
									"\ncontext " + data.constrainedObject.getName() +
									"\ninv: " + oclEditor.getTextWidget().getText() + 
									"\nendpackage";
				OCLInput input = new OCLInput(fullOCLQuery);
				org.eclipse.ocl.ecore.OCL ocl = org.eclipse.ocl.ecore.OCL.newInstance();
				try {
					List<Constraint> constraints = ocl.parse(input);
					if (constraints.size() != 1) {
						oclOK = false;
						wizData.createdOCLString = null;
					}
					else {
						if (!ocl.createQuery(constraints.get(0)).resultType().getInstanceClassName().equals("java.lang.Boolean")) {
							oclOK = false;
							wizData.createdOCLString = null;
						}
						else {
							oclOK = true;
							wizData.createdOCLString = fullOCLQuery;
						}
					}
				}
				catch (ParserException pe) {
					oclOK = false;
					wizData.createdOCLString = null;
				}
				
			}
			else {
				oclOK = false;
				wizData.createdOCLString = null;
			}
			updatePageStatus();
		}
		
	};
	
	// When the select metamodel button is pressed
	private SelectionListener buttonListener = new SelectionListener() {

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			// Not needed
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			// If the button is pressed a new dialog is created to select a metamodel from the registry
			MetamodelSelectionDialog dialog = new MetamodelSelectionDialog(getShell());
			int result = dialog.open();
			if (result == Dialog.OK && dialog.getSelectedMM() != null) {
				mmText.setText(dialog.getSelectedMM().getName() + " : " + dialog.getSelectedMM().getNsURI());
				wizData.constrainedMM = dialog.getSelectedMM();
				modelViewer.setInput(null);
				modelViewer.setInput(wizData.constrainedMM);
			}
		}
		
	};
	
	
	/**
	 * Constructor of the class. Requires an instance of the two data structures used by the containing {@link es.esi.gemde.modelvalidator.ui.wizards.OCLConstraintCreationWizard}
	 *
	 *@param wizData the wizard data.
	 *@param data the constraint data.
	 */
	public SelectModelElementPage(ConstraintWizardData wizData, OCLConstraintData data) {
		super(NAME, "Select Model Element Page", null);
		this.wizData = wizData;
		this.data = data;
		oclOK = false;
	}
	
	/**
	 * Returns the selected metamodel object.
	 * 
	 * @return the selected object.
	 */
	public EObject getSelectedObject() {
		return data.constrainedObject;
	}

	/* 
	 * createControl() method
	 * (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createControl(Composite parent) {
		Composite topPanel = new Composite(parent, SWT.NONE);
		topPanel.setLayout(new GridLayout(6, true));
		
		topPanel.setSize(400, 600);
		
		// Creating controls in order!
		
		Label nameLabel = new Label(topPanel, SWT.NONE);
		GridData nameLabelData = new GridData();
		nameLabelData.horizontalSpan = 1;
		nameLabel.setText("Constraint Name:");
		nameLabel.setLayoutData(nameLabelData);
		
		nameText = new Text(topPanel, SWT.BORDER | SWT.LEFT);
		GridData nameTextData = new GridData(GridData.FILL_HORIZONTAL);
		nameTextData.horizontalSpan = 5;
		nameText.setText("");
		nameText.setLayoutData(nameTextData);
		
		// Configure the text field if the name was already selected
		if (wizData.name != null && wizData.name != "") {
			nameText.setText(wizData.name);
		}
		
		Label categoryLabel = new Label(topPanel, SWT.NONE);
		GridData categoryLabelData = new GridData();
		categoryLabelData.horizontalSpan = 1;
		categoryLabel.setText("Constraint Category:");
		categoryLabel.setLayoutData(categoryLabelData);
		
		categoryText = new Text(topPanel, SWT.BORDER | SWT.LEFT);
		GridData categoryTextData = new GridData(GridData.FILL_HORIZONTAL);
		categoryTextData.horizontalSpan = 5;
		categoryText.setText("");
		categoryText.setLayoutData(categoryTextData);
		
		// Configure the text field if the category was already selected
		if (wizData.category != null && wizData.category != "") {
			categoryText.setText(wizData.category);
		}
		
		Label severityLabel = new Label(topPanel, SWT.NONE);
		GridData severityLabelData = new GridData();
		severityLabelData.horizontalSpan = 1;
		severityLabel.setText("Constraint Severity:");
		severityLabel.setLayoutData(severityLabelData);
		
		severityCombo = new Combo(topPanel, SWT.BORDER | SWT.READ_ONLY);
		GridData severityComboData = new GridData(GridData.FILL_HORIZONTAL);
		severityComboData.horizontalSpan = 1;
		severityCombo.setLayoutData(severityComboData);
		severityCombo.add("ERROR");
		severityCombo.add("WARNING");
		severityCombo.add("INFO");
		severityCombo.select(adaptIndex(wizData.severity));
		
		Label emptyLabel2 = new Label (topPanel, SWT.NONE);
		GridData emptyLabel2Data = new GridData();
		emptyLabel2Data.horizontalSpan = 4;
		emptyLabel2.setText("");
		emptyLabel2.setLayoutData(emptyLabel2Data);
		
		
		Label mmLabel = new Label(topPanel, SWT.NONE);
		GridData expData = new GridData();
		expData.horizontalSpan = 1;
		mmLabel.setText("Select Metamodel:");
		mmLabel.setLayoutData(expData);
		
		mmText = new Label(topPanel, SWT.HORIZONTAL | SWT.SHADOW_OUT);
		GridData mmTextData = new GridData(GridData.FILL_HORIZONTAL);
		mmTextData.horizontalSpan = 4;
		mmText.setLayoutData(mmTextData);
		mmText.setText("");
		
		Button mmButton = new Button(topPanel, SWT.PUSH | SWT.CENTER);
		GridData butData = new GridData(GridData.FILL_HORIZONTAL);
		butData.horizontalSpan = 1;
		mmButton.setLayoutData(butData);
		mmButton.setText("Select");
		
		Label modelLabel = new Label(topPanel, SWT.NONE);
		GridData modelLabelData = new GridData(GridData.FILL_HORIZONTAL);
		modelLabelData.horizontalSpan = 6;
		modelLabel.setText("Meta Classes");
		modelLabel.setLayoutData(modelLabelData);
		
		// Create a tree viewer and use EMF edit features to show it!! (taken from PLUM!!)
		modelViewer = new TreeViewer(topPanel, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		GridData modelViewerData = new GridData(GridData.FILL_BOTH);
		modelViewerData.horizontalSpan = 6;
		modelViewer.getTree().setLayoutData(modelViewerData);

		ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		modelViewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
		modelViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		modelViewer.addFilter(modelFilter);
		modelViewer.setSorter(new ViewerSorter());
		
		Label emptyLabel = new Label(topPanel, SWT.NONE);
		GridData emptyLabelData = new GridData(GridData.FILL_HORIZONTAL);
		emptyLabelData.horizontalSpan = 5;
		emptyLabel.setText("");
		emptyLabel.setLayoutData(emptyLabelData);
		
		// Configure the metamodel and the selected EClass
		if (wizData.constrainedMM != null) {
			mmText.setText(wizData.constrainedMM.getNsURI());
			modelViewer.setInput(wizData.constrainedMM);
			if (data.constrainedObject != null) {
				modelViewer.setSelection(new StructuredSelection(data.constrainedObject));
			}
		}
		
		manualOCLButton = new Button(topPanel, SWT.CHECK | SWT.CENTER);
		GridData manualOCLButData = new GridData(GridData.FILL_HORIZONTAL);
		manualOCLButData.horizontalSpan = 1;
		manualOCLButton.setLayoutData(manualOCLButData);
		manualOCLButton.setText("Toggle OCL Editor");
		
		colorManager = new ColorManager();
		document = new OCLDocument();
		document.setOCLFactory(oclFactory);
		document.setModelingLevel(modelingLevel);
		
		oclEditor = new OCLSourceViewer(topPanel, colorManager, SWT.BORDER | SWT.MULTI);
		oclEditor.setDocument(document);
		GridData oclEditorData = new GridData(GridData.FILL_HORIZONTAL);
		oclEditorData.horizontalSpan = 6;
		oclEditor.getTextWidget().setLayoutData(oclEditorData);
		oclEditor.getTextWidget().addKeyListener(new InputKeyListener());
		
		
		// Initialize if needed
		if (wizData.plainOCL != null) {
			manualOCLButton.setEnabled(true);
			manualOCLButton.setSelection(true);
			oclEditor.getTextWidget().setEnabled(true);
			oclEditor.getTextWidget().setVisible(true);
			oclEditor.getTextWidget().setText(wizData.plainOCL);
		}
		else {
			manualOCLButton.setEnabled(false);
			oclEditor.getTextWidget().setEnabled(false);
			oclEditor.getTextWidget().setVisible(false);
		}
		
		// Add listeners to the composite
		modelViewer.addSelectionChangedListener(selectObjectListener);
		mmButton.addSelectionListener(buttonListener);
		manualOCLButton.addSelectionListener(oclButtonListener);
		oclEditor.getTextWidget().addModifyListener(oclModifyListener);
		nameText.addModifyListener(nameModifyListener);
		categoryText.addModifyListener(catModifyListener);
		severityCombo.addSelectionListener(comboListener);
		
		// Set page description, title, image, etc...
		setDescription("Select a name, category and severity values for the constraint. Then, select the metamodel to which the constraint shall be applied.\nLastly, select the object element on which the new constraint shall be applied.");
		setTitle("Initialize the constraint data");
		
		setControl(topPanel);
		setPageComplete(false);

	}
	
	// A Viewer Filter that shows only EClasses
	ViewerFilter modelFilter = new ViewerFilter() {

		@Override
		public boolean select(Viewer viewer, Object parentElement, Object element) {
			if (element instanceof EClass) {
				return true;
			}
			else
				return false;
		}
		
	};
	
	/**
	 * Implementation of the {@link IOCLFactory} interface. Taken from the OCL console example and slightly adapted.
	 *
	 * @author Adri�n Noguero (adrian.noguero@esi.es)
	 * @version 1.0
	 * @since 1.0
	 *
	 */
	private class EcoreOCLFactory implements IOCLFactory<Object> {

	    @SuppressWarnings("unchecked")
	    public OCL<?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?> createOCL(ModelingLevel level) {
            return OCL.newInstance(
                new EcoreEnvironmentFactory(EPackage.Registry.INSTANCE/*
                    new DelegatingPackageRegistry(
                            wizData.constrainedMM.eResource().getResourceSet().getPackageRegistry(),
                            EPackage.Registry.INSTANCE)*/));
	    }
	    
        @SuppressWarnings("unchecked")
		public OCL<?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?> createOCL(ModelingLevel level,
                Resource res) {
            
            return OCL.newInstance(
                new EcoreEnvironmentFactory(EPackage.Registry.INSTANCE/*
                        new DelegatingPackageRegistry(
                        wizData.constrainedMM.eResource().getResourceSet().getPackageRegistry(),
                        EPackage.Registry.INSTANCE)*/),
                res);
        }
        
	    public Object getContextClassifier(EObject object) {
	        return data.constrainedObject;
	    }
	    
	    public String getName(Object modelElement) {
	        return ((ENamedElement) modelElement).getName();
	    }
	}
	
	/**
	 * Implementation of the {@link KeyListener} interface. Taken from the OCL console example and slightly adapted.
	 *
	 * @author Adri�n Noguero (adrian.noguero@esi.es)
	 * @version 1.0
	 * @since 1.0
	 *
	 */
	private class InputKeyListener implements KeyListener {
		
		public void keyPressed(KeyEvent e) {
			// Do nothing
		}

		public void keyReleased(KeyEvent e) {
			switch (e.keyCode) {
			case ' ':
			    if ((e.stateMask & SWT.CTRL) == SWT.CTRL) {
			        oclEditor.getContentAssistant().showPossibleCompletions();
			    }
			    break;
			}
		}
	}
	
	/**
	 * Internal method to adapt the IStatus severity value to the combo index.
	 * 
	 * @param severity the IStatus severity value.
	 * @return adapted combo index value.
	 */
	private int adaptIndex(int severity) {
		switch(severity) {
		case IStatus.ERROR:
			return 0;
		case IStatus.WARNING:
			return 1;
		case IStatus.INFO:
			return 2;
		}
		return 0;
	}

	/**
	 * Internal method used to update the page status after a listener has been triggered
	 */
	private void updatePageStatus() {
		if (nameText.getText() != "" && categoryText.getText() != "" && nameText.getText() != null && categoryText.getText() != null) {
			
			if (manualOCLButton.getSelection()) {
				wizData.canFinish = (data.constrainedObject instanceof EClass && oclOK);
				setPageComplete(data.constrainedObject instanceof EClass && !oclOK);
			}
			else {
				wizData.canFinish = false;
				setPageComplete(data.constrainedObject instanceof EClass);
			}
		}
		else {
			wizData.canFinish = false;
			setPageComplete(false);
		}
	}
	
	
}
