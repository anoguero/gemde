/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.modelvalidator.ui.wizards;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * A structure used by the {@link es.esi.gemde.modelvalidator.ui.wizards.OCLConstraintCreationWizard} wizard to store the information about the created OCL constraint.
 * 
 * This structure is intended to be recursive using the {@link OCLConstraintData#subData} property. Each level will include information about an iteration of the constraint definition process.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class OCLConstraintData {

	// Constraint Definition
	public EClass constrainedObject;
	public EStructuralFeature property;
	public int opCode;
	public String value;
	public int sizeValue;
	
	public OCLConstraintData subData = null;	
	
	public static final int OP_IS_EMPTY = 0;
	public static final int OP_IS_NOT_EMPTY = 1;
	public static final int OP_HAS_VALUE = 2;
	public static final int OP_SIZE_GREATER = 3;
	public static final int OP_SIZE_LOWER = 4;
	public static final int OP_SIZE_EQUAL = 5;
	public static final int OP_ELEMENT_HAS_VALUE = 6;
	public static final int OP_CONTAINS_ELEMENT_WITH_VALUE = 7;
	public static final int OP_IS_NULL = 8;
	public static final int OP_IS_NOT_NULL = 9;
	public static final int OP_SIZE_GREATER_EQUAL = 10;
	public static final int OP_SIZE_LOWER_EQUAL = 11;
	public static final int OP_HAS_NOT_VALUE = 12;
	public static final int OP_ELEMENT_HAS_NOT_VALUE = 13;
	public static final int OP_NOT_CONTAINS_ELEMENT_WITH_VALUE = 14;
	
	/**
	 * Returns true if this instance has sub-data defined.
	 * 
	 * @return true if subData is not null. False otherwise.
	 */
	public boolean hasSubData () {
		return (subData != null);
	}
	
}
