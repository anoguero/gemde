/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.modeltransformator.mofscriptengine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;

import org.eclipse.mofscript.MOFScriptModel.MOFScriptSpecification;
import org.eclipse.mofscript.parser.ParserUtil;

/**
 * A class extending {@link ParserUtil} to add a new parse() method.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class ExtendedParserUtil extends ParserUtil {

	/**
	 * Parses a transformation from a {@link URI} instance.
	 * 
	 * @param transformationURI the URI containing the transformation.
	 * @return the {@link MOFScriptSpecification} instance of the transformation or null if accessing resource identified by the provided URI returned exceptions.
	 */
	public MOFScriptSpecification parse (URI transformationURI) {
		try {
			BufferedReader r = new BufferedReader(new InputStreamReader(transformationURI.toURL().openStream()));
			return doParse(r, true, true, transformationURI.toString());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
