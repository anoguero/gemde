/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Abstract Sub Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask#getPrev <em>Prev</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask#getNext <em>Next</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getAbstractSubTask()
 * @model abstract="true"
 * @generated
 */
public interface AbstractSubTask extends MethodStep
{
  /**
   * Returns the value of the '<em><b>Prev</b></em>' reference.
   * It is bidirectional and its opposite is '{@link es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask#getNext <em>Next</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Prev</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Prev</em>' reference.
   * @see #setPrev(AbstractSubTask)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getAbstractSubTask_Prev()
   * @see es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask#getNext
   * @model opposite="next"
   * @generated
   */
  AbstractSubTask getPrev();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask#getPrev <em>Prev</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Prev</em>' reference.
   * @see #getPrev()
   * @generated
   */
  void setPrev(AbstractSubTask value);

  /**
   * Returns the value of the '<em><b>Next</b></em>' reference.
   * It is bidirectional and its opposite is '{@link es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask#getPrev <em>Prev</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Next</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Next</em>' reference.
   * @see #setNext(AbstractSubTask)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getAbstractSubTask_Next()
   * @see es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask#getPrev
   * @model opposite="prev"
   *        annotation="gmf.link target.decoration='arrow' color='122,163,204' tool.name='Next SubTask' tool.small.bundle='es.esi.gemde.methodmanager.methodmodel.edit' tool.small.path='icons/full/obj16/NextSubTaskTool.gif'"
   * @generated
   */
  AbstractSubTask getNext();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask#getNext <em>Next</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Next</em>' reference.
   * @see #getNext()
   * @generated
   */
  void setNext(AbstractSubTask value);

} // AbstractSubTask
