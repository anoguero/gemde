/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Select File Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.SelectFileCommand#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.SelectFileCommand#getOnlyWorkspace <em>Only Workspace</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSelectFileCommand()
 * @model annotation="gmf.node figure='rectangle' border.color='102,76,0' color='230,204,128' label='name' tool.name='Add Select File'"
 * @generated
 */
public interface SelectFileCommand extends SimpleCommand
{
  /**
   * Returns the value of the '<em><b>Identifier</b></em>' attribute.
   * The default value is <code>"es.esi.gemde.methodmanager.commands.selectfilecommand"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Identifier</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Identifier</em>' attribute.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSelectFileCommand_Identifier()
   * @model default="es.esi.gemde.methodmanager.commands.selectfilecommand" required="true" changeable="false"
   * @generated
   */
  String getIdentifier();

  /**
   * Returns the value of the '<em><b>Only Workspace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Only Workspace</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Only Workspace</em>' reference.
   * @see #setOnlyWorkspace(CommandParameter)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSelectFileCommand_OnlyWorkspace()
   * @model required="true"
   * @generated
   */
  CommandParameter getOnlyWorkspace();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectFileCommand#getOnlyWorkspace <em>Only Workspace</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Only Workspace</em>' reference.
   * @see #getOnlyWorkspace()
   * @generated
   */
  void setOnlyWorkspace(CommandParameter value);

} // SelectFileCommand
