/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Select Model Element From File Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand#getModelFile <em>Model File</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand#getAddToList <em>Add To List</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand#getList <em>List</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand#getIdentifier <em>Identifier</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSelectModelElementFromFileCommand()
 * @model annotation="gmf.node figure='rectangle' border.color='102,76,0' color='230,204,128' label='name' tool.name='Add Select EObject from File Command'"
 * @generated
 */
public interface SelectModelElementFromFileCommand extends SimpleCommand
{
  /**
   * Returns the value of the '<em><b>Model File</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Model File</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Model File</em>' reference.
   * @see #setModelFile(CommandParameter)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSelectModelElementFromFileCommand_ModelFile()
   * @model required="true"
   * @generated
   */
  CommandParameter getModelFile();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand#getModelFile <em>Model File</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Model File</em>' reference.
   * @see #getModelFile()
   * @generated
   */
  void setModelFile(CommandParameter value);

  /**
   * Returns the value of the '<em><b>Add To List</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Add To List</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Add To List</em>' reference.
   * @see #setAddToList(CommandParameter)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSelectModelElementFromFileCommand_AddToList()
   * @model
   * @generated
   */
  CommandParameter getAddToList();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand#getAddToList <em>Add To List</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Add To List</em>' reference.
   * @see #getAddToList()
   * @generated
   */
  void setAddToList(CommandParameter value);

  /**
   * Returns the value of the '<em><b>List</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>List</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>List</em>' reference.
   * @see #setList(CommandParameter)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSelectModelElementFromFileCommand_List()
   * @model
   * @generated
   */
  CommandParameter getList();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand#getList <em>List</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>List</em>' reference.
   * @see #getList()
   * @generated
   */
  void setList(CommandParameter value);

  /**
   * Returns the value of the '<em><b>Identifier</b></em>' attribute.
   * The default value is <code>"es.esi.gemde.methodmanager.commands.selectelemfromfilecommand"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Identifier</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Identifier</em>' attribute.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSelectModelElementFromFileCommand_Identifier()
   * @model default="es.esi.gemde.methodmanager.commands.selectelemfromfilecommand" required="true" changeable="false"
   * @generated
   */
  String getIdentifier();

} // SelectModelElementFromFileCommand
