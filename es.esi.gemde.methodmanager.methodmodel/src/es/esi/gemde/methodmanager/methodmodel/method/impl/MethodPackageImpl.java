/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.impl;

import es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask;
import es.esi.gemde.methodmanager.methodmodel.method.CallWizardCommand;
import es.esi.gemde.methodmanager.methodmodel.method.CommandParameter;
import es.esi.gemde.methodmanager.methodmodel.method.ComplexCommand;
import es.esi.gemde.methodmanager.methodmodel.method.Condition;
import es.esi.gemde.methodmanager.methodmodel.method.ConditionalSubTask;
import es.esi.gemde.methodmanager.methodmodel.method.ConstantCommandParameter;
import es.esi.gemde.methodmanager.methodmodel.method.CustomCommand;
import es.esi.gemde.methodmanager.methodmodel.method.DataStorageElement;
import es.esi.gemde.methodmanager.methodmodel.method.ExecutableElement;
import es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod;
import es.esi.gemde.methodmanager.methodmodel.method.InternalVariable;
import es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand;
import es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand;
import es.esi.gemde.methodmanager.methodmodel.method.MethodCommand;
import es.esi.gemde.methodmanager.methodmodel.method.MethodFactory;
import es.esi.gemde.methodmanager.methodmodel.method.MethodPackage;
import es.esi.gemde.methodmanager.methodmodel.method.MethodPart;
import es.esi.gemde.methodmanager.methodmodel.method.MethodStep;
import es.esi.gemde.methodmanager.methodmodel.method.MethodVariable;
import es.esi.gemde.methodmanager.methodmodel.method.OpenEditorCommand;
import es.esi.gemde.methodmanager.methodmodel.method.Phase;
import es.esi.gemde.methodmanager.methodmodel.method.RepeatedSubTask;
import es.esi.gemde.methodmanager.methodmodel.method.SelectFileCommand;
import es.esi.gemde.methodmanager.methodmodel.method.SelectFolderCommand;
import es.esi.gemde.methodmanager.methodmodel.method.SelectMetamodelCommand;
import es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand;
import es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand;
import es.esi.gemde.methodmanager.methodmodel.method.SelectTransformationCommand;
import es.esi.gemde.methodmanager.methodmodel.method.SelectValidationCommand;
import es.esi.gemde.methodmanager.methodmodel.method.SimpleCommand;
import es.esi.gemde.methodmanager.methodmodel.method.SubTask;
import es.esi.gemde.methodmanager.methodmodel.method.Task;
import es.esi.gemde.methodmanager.methodmodel.method.VariableCommandParameter;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MethodPackageImpl extends EPackageImpl implements MethodPackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass gemdeMethodEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass methodStepEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass methodPartEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass executableElementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass dataStorageElementEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass methodVariableEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass internalVariableEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass phaseEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass taskEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass abstractSubTaskEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass subTaskEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass conditionEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass conditionalSubTaskEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass repeatedSubTaskEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass commandParameterEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass variableCommandParameterEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass constantCommandParameterEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass methodCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass complexCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass simpleCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass customCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass callWizardCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass selectFileCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass selectFolderCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass selectMetamodelCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass selectModelElementFromFileCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass selectModelElementFromEObjectCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass selectValidationCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass selectTransformationCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass launchValidationCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass launchTransformationCommandEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass openEditorCommandEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private MethodPackageImpl()
  {
    super(eNS_URI, MethodFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
   * 
   * <p>This method is used to initialize {@link MethodPackage#eINSTANCE} when that field is accessed.
   * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static MethodPackage init()
  {
    if (isInited) return (MethodPackage)EPackage.Registry.INSTANCE.getEPackage(MethodPackage.eNS_URI);

    // Obtain or create and register package
    MethodPackageImpl theMethodPackage = (MethodPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof MethodPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new MethodPackageImpl());

    isInited = true;

    // Create package meta-data objects
    theMethodPackage.createPackageContents();

    // Initialize created meta-data
    theMethodPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theMethodPackage.freeze();

  
    // Update the registry and return the package
    EPackage.Registry.INSTANCE.put(MethodPackage.eNS_URI, theMethodPackage);
    return theMethodPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getGEMDEMethod()
  {
    return gemdeMethodEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getGEMDEMethod_Name()
  {
    return (EAttribute)gemdeMethodEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getGEMDEMethod_Description()
  {
    return (EAttribute)gemdeMethodEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getGEMDEMethod_ProjectName()
  {
    return (EAttribute)gemdeMethodEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGEMDEMethod_Phases()
  {
    return (EReference)gemdeMethodEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGEMDEMethod_InitialPhase()
  {
    return (EReference)gemdeMethodEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGEMDEMethod_Variables()
  {
    return (EReference)gemdeMethodEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getGEMDEMethod_Commands()
  {
    return (EReference)gemdeMethodEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMethodStep()
  {
    return methodStepEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMethodStep_Title()
  {
    return (EAttribute)methodStepEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMethodStep_Description()
  {
    return (EAttribute)methodStepEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMethodPart()
  {
    return methodPartEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMethodPart_Introduction()
  {
    return (EAttribute)methodPartEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMethodPart_Conclusion()
  {
    return (EAttribute)methodPartEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMethodPart_IsOptional()
  {
    return (EAttribute)methodPartEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getExecutableElement()
  {
    return executableElementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getExecutableElement_Command()
  {
    return (EReference)executableElementEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDataStorageElement()
  {
    return dataStorageElementEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMethodVariable()
  {
    return methodVariableEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMethodVariable_Identifier()
  {
    return (EAttribute)methodVariableEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMethodVariable_Name()
  {
    return (EAttribute)methodVariableEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMethodVariable_Type()
  {
    return (EAttribute)methodVariableEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getInternalVariable()
  {
    return internalVariableEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getInternalVariable_Name()
  {
    return (EAttribute)internalVariableEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getPhase()
  {
    return phaseEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPhase_Tasks()
  {
    return (EReference)phaseEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPhase_InitialTask()
  {
    return (EReference)phaseEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPhase_Prev()
  {
    return (EReference)phaseEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getPhase_Next()
  {
    return (EReference)phaseEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getTask()
  {
    return taskEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTask_SubTasks()
  {
    return (EReference)taskEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTask_InitialSubtask()
  {
    return (EReference)taskEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTask_Prev()
  {
    return (EReference)taskEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getTask_Next()
  {
    return (EReference)taskEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAbstractSubTask()
  {
    return abstractSubTaskEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAbstractSubTask_Prev()
  {
    return (EReference)abstractSubTaskEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getAbstractSubTask_Next()
  {
    return (EReference)abstractSubTaskEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSubTask()
  {
    return subTaskEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCondition()
  {
    return conditionEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getCondition_Value()
  {
    return (EAttribute)conditionEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCondition_From()
  {
    return (EReference)conditionEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCondition_To()
  {
    return (EReference)conditionEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConditionalSubTask()
  {
    return conditionalSubTaskEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConditionalSubTask_Variable()
  {
    return (EReference)conditionalSubTaskEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getConditionalSubTask_ConditionalLinks()
  {
    return (EReference)conditionalSubTaskEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getRepeatedSubTask()
  {
    return repeatedSubTaskEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRepeatedSubTask_RepeatedElement()
  {
    return (EReference)repeatedSubTaskEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getRepeatedSubTask_RepetitionVector()
  {
    return (EAttribute)repeatedSubTaskEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getRepeatedSubTask_RepetitionVariable()
  {
    return (EReference)repeatedSubTaskEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCommandParameter()
  {
    return commandParameterEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getCommandParameter_Identifier()
  {
    return (EAttribute)commandParameterEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getVariableCommandParameter()
  {
    return variableCommandParameterEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getVariableCommandParameter_Value()
  {
    return (EReference)variableCommandParameterEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getConstantCommandParameter()
  {
    return constantCommandParameterEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getConstantCommandParameter_Value()
  {
    return (EAttribute)constantCommandParameterEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getMethodCommand()
  {
    return methodCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getMethodCommand_Name()
  {
    return (EAttribute)methodCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getComplexCommand()
  {
    return complexCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getComplexCommand_Identifier()
  {
    return (EAttribute)complexCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getComplexCommand_Commands()
  {
    return (EReference)complexCommandEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSimpleCommand()
  {
    return simpleCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSimpleCommand_ReturnValueTo()
  {
    return (EReference)simpleCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSimpleCommand_Parameters()
  {
    return (EReference)simpleCommandEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCustomCommand()
  {
    return customCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getCustomCommand_Identifier()
  {
    return (EAttribute)customCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getCallWizardCommand()
  {
    return callWizardCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getCallWizardCommand_Identifier()
  {
    return (EAttribute)callWizardCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getCallWizardCommand_Wizard()
  {
    return (EReference)callWizardCommandEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSelectFileCommand()
  {
    return selectFileCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSelectFileCommand_Identifier()
  {
    return (EAttribute)selectFileCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelectFileCommand_OnlyWorkspace()
  {
    return (EReference)selectFileCommandEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSelectFolderCommand()
  {
    return selectFolderCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelectFolderCommand_OnlyWorkspace()
  {
    return (EReference)selectFolderCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSelectFolderCommand_Identifier()
  {
    return (EAttribute)selectFolderCommandEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSelectMetamodelCommand()
  {
    return selectMetamodelCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSelectMetamodelCommand_Identifier()
  {
    return (EAttribute)selectMetamodelCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSelectModelElementFromFileCommand()
  {
    return selectModelElementFromFileCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelectModelElementFromFileCommand_ModelFile()
  {
    return (EReference)selectModelElementFromFileCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelectModelElementFromFileCommand_AddToList()
  {
    return (EReference)selectModelElementFromFileCommandEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelectModelElementFromFileCommand_List()
  {
    return (EReference)selectModelElementFromFileCommandEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSelectModelElementFromFileCommand_Identifier()
  {
    return (EAttribute)selectModelElementFromFileCommandEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSelectModelElementFromEObjectCommand()
  {
    return selectModelElementFromEObjectCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelectModelElementFromEObjectCommand_EObject()
  {
    return (EReference)selectModelElementFromEObjectCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelectModelElementFromEObjectCommand_AddToList()
  {
    return (EReference)selectModelElementFromEObjectCommandEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSelectModelElementFromEObjectCommand_List()
  {
    return (EReference)selectModelElementFromEObjectCommandEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSelectModelElementFromEObjectCommand_Identifier()
  {
    return (EAttribute)selectModelElementFromEObjectCommandEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSelectValidationCommand()
  {
    return selectValidationCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSelectValidationCommand_Identifier()
  {
    return (EAttribute)selectValidationCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSelectTransformationCommand()
  {
    return selectTransformationCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSelectTransformationCommand_Identifier()
  {
    return (EAttribute)selectTransformationCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLaunchValidationCommand()
  {
    return launchValidationCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getLaunchValidationCommand_Identifier()
  {
    return (EAttribute)launchValidationCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLaunchValidationCommand_Validation()
  {
    return (EReference)launchValidationCommandEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLaunchValidationCommand_Eobject()
  {
    return (EReference)launchValidationCommandEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLaunchValidationCommand_ModelFile()
  {
    return (EReference)launchValidationCommandEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getLaunchTransformationCommand()
  {
    return launchTransformationCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getLaunchTransformationCommand_Identifier()
  {
    return (EAttribute)launchTransformationCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLaunchTransformationCommand_Transformation()
  {
    return (EReference)launchTransformationCommandEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLaunchTransformationCommand_Inputs()
  {
    return (EReference)launchTransformationCommandEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLaunchTransformationCommand_Outputs()
  {
    return (EReference)launchTransformationCommandEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getLaunchTransformationCommand_OutputPath()
  {
    return (EReference)launchTransformationCommandEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getOpenEditorCommand()
  {
    return openEditorCommandEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOpenEditorCommand_EditorID()
  {
    return (EReference)openEditorCommandEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getOpenEditorCommand_OpenedFile()
  {
    return (EReference)openEditorCommandEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getOpenEditorCommand_Identifier()
  {
    return (EAttribute)openEditorCommandEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodFactory getMethodFactory()
  {
    return (MethodFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    gemdeMethodEClass = createEClass(GEMDE_METHOD);
    createEAttribute(gemdeMethodEClass, GEMDE_METHOD__NAME);
    createEAttribute(gemdeMethodEClass, GEMDE_METHOD__DESCRIPTION);
    createEAttribute(gemdeMethodEClass, GEMDE_METHOD__PROJECT_NAME);
    createEReference(gemdeMethodEClass, GEMDE_METHOD__PHASES);
    createEReference(gemdeMethodEClass, GEMDE_METHOD__INITIAL_PHASE);
    createEReference(gemdeMethodEClass, GEMDE_METHOD__VARIABLES);
    createEReference(gemdeMethodEClass, GEMDE_METHOD__COMMANDS);

    methodStepEClass = createEClass(METHOD_STEP);
    createEAttribute(methodStepEClass, METHOD_STEP__TITLE);
    createEAttribute(methodStepEClass, METHOD_STEP__DESCRIPTION);

    methodPartEClass = createEClass(METHOD_PART);
    createEAttribute(methodPartEClass, METHOD_PART__INTRODUCTION);
    createEAttribute(methodPartEClass, METHOD_PART__CONCLUSION);
    createEAttribute(methodPartEClass, METHOD_PART__IS_OPTIONAL);

    executableElementEClass = createEClass(EXECUTABLE_ELEMENT);
    createEReference(executableElementEClass, EXECUTABLE_ELEMENT__COMMAND);

    dataStorageElementEClass = createEClass(DATA_STORAGE_ELEMENT);

    methodVariableEClass = createEClass(METHOD_VARIABLE);
    createEAttribute(methodVariableEClass, METHOD_VARIABLE__IDENTIFIER);
    createEAttribute(methodVariableEClass, METHOD_VARIABLE__NAME);
    createEAttribute(methodVariableEClass, METHOD_VARIABLE__TYPE);

    internalVariableEClass = createEClass(INTERNAL_VARIABLE);
    createEAttribute(internalVariableEClass, INTERNAL_VARIABLE__NAME);

    phaseEClass = createEClass(PHASE);
    createEReference(phaseEClass, PHASE__TASKS);
    createEReference(phaseEClass, PHASE__INITIAL_TASK);
    createEReference(phaseEClass, PHASE__PREV);
    createEReference(phaseEClass, PHASE__NEXT);

    taskEClass = createEClass(TASK);
    createEReference(taskEClass, TASK__SUB_TASKS);
    createEReference(taskEClass, TASK__INITIAL_SUBTASK);
    createEReference(taskEClass, TASK__PREV);
    createEReference(taskEClass, TASK__NEXT);

    abstractSubTaskEClass = createEClass(ABSTRACT_SUB_TASK);
    createEReference(abstractSubTaskEClass, ABSTRACT_SUB_TASK__PREV);
    createEReference(abstractSubTaskEClass, ABSTRACT_SUB_TASK__NEXT);

    subTaskEClass = createEClass(SUB_TASK);

    conditionEClass = createEClass(CONDITION);
    createEAttribute(conditionEClass, CONDITION__VALUE);
    createEReference(conditionEClass, CONDITION__FROM);
    createEReference(conditionEClass, CONDITION__TO);

    conditionalSubTaskEClass = createEClass(CONDITIONAL_SUB_TASK);
    createEReference(conditionalSubTaskEClass, CONDITIONAL_SUB_TASK__VARIABLE);
    createEReference(conditionalSubTaskEClass, CONDITIONAL_SUB_TASK__CONDITIONAL_LINKS);

    repeatedSubTaskEClass = createEClass(REPEATED_SUB_TASK);
    createEReference(repeatedSubTaskEClass, REPEATED_SUB_TASK__REPEATED_ELEMENT);
    createEAttribute(repeatedSubTaskEClass, REPEATED_SUB_TASK__REPETITION_VECTOR);
    createEReference(repeatedSubTaskEClass, REPEATED_SUB_TASK__REPETITION_VARIABLE);

    commandParameterEClass = createEClass(COMMAND_PARAMETER);
    createEAttribute(commandParameterEClass, COMMAND_PARAMETER__IDENTIFIER);

    variableCommandParameterEClass = createEClass(VARIABLE_COMMAND_PARAMETER);
    createEReference(variableCommandParameterEClass, VARIABLE_COMMAND_PARAMETER__VALUE);

    constantCommandParameterEClass = createEClass(CONSTANT_COMMAND_PARAMETER);
    createEAttribute(constantCommandParameterEClass, CONSTANT_COMMAND_PARAMETER__VALUE);

    methodCommandEClass = createEClass(METHOD_COMMAND);
    createEAttribute(methodCommandEClass, METHOD_COMMAND__NAME);

    complexCommandEClass = createEClass(COMPLEX_COMMAND);
    createEAttribute(complexCommandEClass, COMPLEX_COMMAND__IDENTIFIER);
    createEReference(complexCommandEClass, COMPLEX_COMMAND__COMMANDS);

    simpleCommandEClass = createEClass(SIMPLE_COMMAND);
    createEReference(simpleCommandEClass, SIMPLE_COMMAND__RETURN_VALUE_TO);
    createEReference(simpleCommandEClass, SIMPLE_COMMAND__PARAMETERS);

    customCommandEClass = createEClass(CUSTOM_COMMAND);
    createEAttribute(customCommandEClass, CUSTOM_COMMAND__IDENTIFIER);

    callWizardCommandEClass = createEClass(CALL_WIZARD_COMMAND);
    createEAttribute(callWizardCommandEClass, CALL_WIZARD_COMMAND__IDENTIFIER);
    createEReference(callWizardCommandEClass, CALL_WIZARD_COMMAND__WIZARD);

    selectFileCommandEClass = createEClass(SELECT_FILE_COMMAND);
    createEAttribute(selectFileCommandEClass, SELECT_FILE_COMMAND__IDENTIFIER);
    createEReference(selectFileCommandEClass, SELECT_FILE_COMMAND__ONLY_WORKSPACE);

    selectFolderCommandEClass = createEClass(SELECT_FOLDER_COMMAND);
    createEReference(selectFolderCommandEClass, SELECT_FOLDER_COMMAND__ONLY_WORKSPACE);
    createEAttribute(selectFolderCommandEClass, SELECT_FOLDER_COMMAND__IDENTIFIER);

    selectMetamodelCommandEClass = createEClass(SELECT_METAMODEL_COMMAND);
    createEAttribute(selectMetamodelCommandEClass, SELECT_METAMODEL_COMMAND__IDENTIFIER);

    selectModelElementFromFileCommandEClass = createEClass(SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND);
    createEReference(selectModelElementFromFileCommandEClass, SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__MODEL_FILE);
    createEReference(selectModelElementFromFileCommandEClass, SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__ADD_TO_LIST);
    createEReference(selectModelElementFromFileCommandEClass, SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__LIST);
    createEAttribute(selectModelElementFromFileCommandEClass, SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__IDENTIFIER);

    selectModelElementFromEObjectCommandEClass = createEClass(SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND);
    createEReference(selectModelElementFromEObjectCommandEClass, SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND__EOBJECT);
    createEReference(selectModelElementFromEObjectCommandEClass, SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND__ADD_TO_LIST);
    createEReference(selectModelElementFromEObjectCommandEClass, SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND__LIST);
    createEAttribute(selectModelElementFromEObjectCommandEClass, SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND__IDENTIFIER);

    selectValidationCommandEClass = createEClass(SELECT_VALIDATION_COMMAND);
    createEAttribute(selectValidationCommandEClass, SELECT_VALIDATION_COMMAND__IDENTIFIER);

    selectTransformationCommandEClass = createEClass(SELECT_TRANSFORMATION_COMMAND);
    createEAttribute(selectTransformationCommandEClass, SELECT_TRANSFORMATION_COMMAND__IDENTIFIER);

    launchValidationCommandEClass = createEClass(LAUNCH_VALIDATION_COMMAND);
    createEAttribute(launchValidationCommandEClass, LAUNCH_VALIDATION_COMMAND__IDENTIFIER);
    createEReference(launchValidationCommandEClass, LAUNCH_VALIDATION_COMMAND__VALIDATION);
    createEReference(launchValidationCommandEClass, LAUNCH_VALIDATION_COMMAND__EOBJECT);
    createEReference(launchValidationCommandEClass, LAUNCH_VALIDATION_COMMAND__MODEL_FILE);

    launchTransformationCommandEClass = createEClass(LAUNCH_TRANSFORMATION_COMMAND);
    createEAttribute(launchTransformationCommandEClass, LAUNCH_TRANSFORMATION_COMMAND__IDENTIFIER);
    createEReference(launchTransformationCommandEClass, LAUNCH_TRANSFORMATION_COMMAND__TRANSFORMATION);
    createEReference(launchTransformationCommandEClass, LAUNCH_TRANSFORMATION_COMMAND__INPUTS);
    createEReference(launchTransformationCommandEClass, LAUNCH_TRANSFORMATION_COMMAND__OUTPUTS);
    createEReference(launchTransformationCommandEClass, LAUNCH_TRANSFORMATION_COMMAND__OUTPUT_PATH);

    openEditorCommandEClass = createEClass(OPEN_EDITOR_COMMAND);
    createEReference(openEditorCommandEClass, OPEN_EDITOR_COMMAND__EDITOR_ID);
    createEReference(openEditorCommandEClass, OPEN_EDITOR_COMMAND__OPENED_FILE);
    createEAttribute(openEditorCommandEClass, OPEN_EDITOR_COMMAND__IDENTIFIER);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes
    methodPartEClass.getESuperTypes().add(this.getMethodStep());
    methodVariableEClass.getESuperTypes().add(this.getDataStorageElement());
    internalVariableEClass.getESuperTypes().add(this.getDataStorageElement());
    phaseEClass.getESuperTypes().add(this.getMethodPart());
    taskEClass.getESuperTypes().add(this.getMethodStep());
    taskEClass.getESuperTypes().add(this.getExecutableElement());
    abstractSubTaskEClass.getESuperTypes().add(this.getMethodStep());
    subTaskEClass.getESuperTypes().add(this.getAbstractSubTask());
    subTaskEClass.getESuperTypes().add(this.getExecutableElement());
    conditionalSubTaskEClass.getESuperTypes().add(this.getAbstractSubTask());
    repeatedSubTaskEClass.getESuperTypes().add(this.getAbstractSubTask());
    variableCommandParameterEClass.getESuperTypes().add(this.getCommandParameter());
    constantCommandParameterEClass.getESuperTypes().add(this.getCommandParameter());
    complexCommandEClass.getESuperTypes().add(this.getMethodCommand());
    simpleCommandEClass.getESuperTypes().add(this.getMethodCommand());
    customCommandEClass.getESuperTypes().add(this.getSimpleCommand());
    callWizardCommandEClass.getESuperTypes().add(this.getSimpleCommand());
    selectFileCommandEClass.getESuperTypes().add(this.getSimpleCommand());
    selectFolderCommandEClass.getESuperTypes().add(this.getSimpleCommand());
    selectMetamodelCommandEClass.getESuperTypes().add(this.getSimpleCommand());
    selectModelElementFromFileCommandEClass.getESuperTypes().add(this.getSimpleCommand());
    selectModelElementFromEObjectCommandEClass.getESuperTypes().add(this.getSimpleCommand());
    selectValidationCommandEClass.getESuperTypes().add(this.getSimpleCommand());
    selectTransformationCommandEClass.getESuperTypes().add(this.getSimpleCommand());
    launchValidationCommandEClass.getESuperTypes().add(this.getSimpleCommand());
    launchTransformationCommandEClass.getESuperTypes().add(this.getSimpleCommand());
    openEditorCommandEClass.getESuperTypes().add(this.getSimpleCommand());

    // Initialize classes and features; add operations and parameters
    initEClass(gemdeMethodEClass, GEMDEMethod.class, "GEMDEMethod", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getGEMDEMethod_Name(), ecorePackage.getEString(), "name", null, 1, 1, GEMDEMethod.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getGEMDEMethod_Description(), ecorePackage.getEString(), "description", null, 0, 1, GEMDEMethod.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getGEMDEMethod_ProjectName(), ecorePackage.getEString(), "projectName", "es.esi.gemde.example.methodology.cheatsheet", 1, 1, GEMDEMethod.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getGEMDEMethod_Phases(), this.getPhase(), null, "phases", null, 1, -1, GEMDEMethod.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getGEMDEMethod_InitialPhase(), this.getPhase(), null, "initialPhase", null, 1, 1, GEMDEMethod.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getGEMDEMethod_Variables(), this.getMethodVariable(), null, "variables", null, 0, -1, GEMDEMethod.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getGEMDEMethod_Commands(), this.getMethodCommand(), null, "commands", null, 0, -1, GEMDEMethod.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(methodStepEClass, MethodStep.class, "MethodStep", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getMethodStep_Title(), ecorePackage.getEString(), "title", null, 1, 1, MethodStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMethodStep_Description(), ecorePackage.getEString(), "description", null, 0, 1, MethodStep.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(methodPartEClass, MethodPart.class, "MethodPart", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getMethodPart_Introduction(), ecorePackage.getEString(), "introduction", null, 0, 1, MethodPart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMethodPart_Conclusion(), ecorePackage.getEString(), "conclusion", null, 0, 1, MethodPart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMethodPart_IsOptional(), ecorePackage.getEBooleanObject(), "isOptional", "false", 0, 1, MethodPart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(executableElementEClass, ExecutableElement.class, "ExecutableElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getExecutableElement_Command(), this.getMethodCommand(), null, "command", null, 0, 1, ExecutableElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(dataStorageElementEClass, DataStorageElement.class, "DataStorageElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(methodVariableEClass, MethodVariable.class, "MethodVariable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getMethodVariable_Identifier(), ecorePackage.getEString(), "identifier", null, 1, 1, MethodVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMethodVariable_Name(), ecorePackage.getEString(), "name", null, 1, 1, MethodVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getMethodVariable_Type(), ecorePackage.getEString(), "type", null, 1, 1, MethodVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(internalVariableEClass, InternalVariable.class, "InternalVariable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getInternalVariable_Name(), ecorePackage.getEString(), "name", null, 1, 1, InternalVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(phaseEClass, Phase.class, "Phase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getPhase_Tasks(), this.getTask(), null, "tasks", null, 1, -1, Phase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getPhase_InitialTask(), this.getTask(), null, "initialTask", null, 1, 1, Phase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getPhase_Prev(), this.getPhase(), this.getPhase_Next(), "prev", null, 0, 1, Phase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getPhase_Next(), this.getPhase(), this.getPhase_Prev(), "next", null, 0, 1, Phase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(taskEClass, Task.class, "Task", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getTask_SubTasks(), this.getAbstractSubTask(), null, "subTasks", null, 0, -1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getTask_InitialSubtask(), this.getAbstractSubTask(), null, "initialSubtask", null, 0, 1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getTask_Prev(), this.getTask(), this.getTask_Next(), "prev", null, 0, 1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getTask_Next(), this.getTask(), this.getTask_Prev(), "next", null, 0, 1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(abstractSubTaskEClass, AbstractSubTask.class, "AbstractSubTask", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getAbstractSubTask_Prev(), this.getAbstractSubTask(), this.getAbstractSubTask_Next(), "prev", null, 0, 1, AbstractSubTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getAbstractSubTask_Next(), this.getAbstractSubTask(), this.getAbstractSubTask_Prev(), "next", null, 0, 1, AbstractSubTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(subTaskEClass, SubTask.class, "SubTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

    initEClass(conditionEClass, Condition.class, "Condition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getCondition_Value(), ecorePackage.getEString(), "value", null, 1, 1, Condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getCondition_From(), this.getConditionalSubTask(), this.getConditionalSubTask_ConditionalLinks(), "from", null, 1, 1, Condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getCondition_To(), this.getSubTask(), null, "to", null, 1, 1, Condition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(conditionalSubTaskEClass, ConditionalSubTask.class, "ConditionalSubTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getConditionalSubTask_Variable(), this.getMethodVariable(), null, "variable", null, 1, 1, ConditionalSubTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getConditionalSubTask_ConditionalLinks(), this.getCondition(), this.getCondition_From(), "conditionalLinks", null, 1, -1, ConditionalSubTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(repeatedSubTaskEClass, RepeatedSubTask.class, "RepeatedSubTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getRepeatedSubTask_RepeatedElement(), this.getSubTask(), null, "repeatedElement", null, 1, 1, RepeatedSubTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getRepeatedSubTask_RepetitionVector(), ecorePackage.getEString(), "repetitionVector", null, 0, -1, RepeatedSubTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getRepeatedSubTask_RepetitionVariable(), this.getInternalVariable(), null, "repetitionVariable", null, 1, 1, RepeatedSubTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(commandParameterEClass, CommandParameter.class, "CommandParameter", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getCommandParameter_Identifier(), ecorePackage.getEString(), "identifier", null, 1, 1, CommandParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(variableCommandParameterEClass, VariableCommandParameter.class, "VariableCommandParameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getVariableCommandParameter_Value(), this.getDataStorageElement(), null, "value", null, 1, 1, VariableCommandParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(constantCommandParameterEClass, ConstantCommandParameter.class, "ConstantCommandParameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getConstantCommandParameter_Value(), ecorePackage.getEString(), "value", null, 1, 1, ConstantCommandParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(methodCommandEClass, MethodCommand.class, "MethodCommand", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getMethodCommand_Name(), ecorePackage.getEString(), "name", null, 1, 1, MethodCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(complexCommandEClass, ComplexCommand.class, "ComplexCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getComplexCommand_Identifier(), ecorePackage.getEString(), "identifier", "es.esi.gemde.methodmanager.commands.complexcommand", 1, 1, ComplexCommand.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getComplexCommand_Commands(), this.getSimpleCommand(), null, "commands", null, 0, -1, ComplexCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(simpleCommandEClass, SimpleCommand.class, "SimpleCommand", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSimpleCommand_ReturnValueTo(), this.getMethodVariable(), null, "returnValueTo", null, 0, 1, SimpleCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSimpleCommand_Parameters(), this.getCommandParameter(), null, "parameters", null, 0, -1, SimpleCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(customCommandEClass, CustomCommand.class, "CustomCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getCustomCommand_Identifier(), ecorePackage.getEString(), "identifier", null, 1, 1, CustomCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(callWizardCommandEClass, CallWizardCommand.class, "CallWizardCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getCallWizardCommand_Identifier(), ecorePackage.getEString(), "identifier", "es.esi.gemde.methodmanager.commands.callwizardcommand", 1, 1, CallWizardCommand.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getCallWizardCommand_Wizard(), this.getCommandParameter(), null, "wizard", null, 1, 1, CallWizardCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(selectFileCommandEClass, SelectFileCommand.class, "SelectFileCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getSelectFileCommand_Identifier(), ecorePackage.getEString(), "identifier", "es.esi.gemde.methodmanager.commands.selectfilecommand", 1, 1, SelectFileCommand.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSelectFileCommand_OnlyWorkspace(), this.getCommandParameter(), null, "onlyWorkspace", null, 1, 1, SelectFileCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(selectFolderCommandEClass, SelectFolderCommand.class, "SelectFolderCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSelectFolderCommand_OnlyWorkspace(), this.getCommandParameter(), null, "onlyWorkspace", null, 1, 1, SelectFolderCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSelectFolderCommand_Identifier(), ecorePackage.getEString(), "identifier", "es.esi.gemde.methodmanager.commands.selectfoldercommand", 1, 1, SelectFolderCommand.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(selectMetamodelCommandEClass, SelectMetamodelCommand.class, "SelectMetamodelCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getSelectMetamodelCommand_Identifier(), ecorePackage.getEString(), "identifier", "es.esi.gemde.methodmanager.commands.selectmetamodelcommand", 1, 1, SelectMetamodelCommand.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(selectModelElementFromFileCommandEClass, SelectModelElementFromFileCommand.class, "SelectModelElementFromFileCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSelectModelElementFromFileCommand_ModelFile(), this.getCommandParameter(), null, "modelFile", null, 1, 1, SelectModelElementFromFileCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSelectModelElementFromFileCommand_AddToList(), this.getCommandParameter(), null, "addToList", null, 0, 1, SelectModelElementFromFileCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSelectModelElementFromFileCommand_List(), this.getCommandParameter(), null, "list", null, 0, 1, SelectModelElementFromFileCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSelectModelElementFromFileCommand_Identifier(), ecorePackage.getEString(), "identifier", "es.esi.gemde.methodmanager.commands.selectelemfromfilecommand", 1, 1, SelectModelElementFromFileCommand.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(selectModelElementFromEObjectCommandEClass, SelectModelElementFromEObjectCommand.class, "SelectModelElementFromEObjectCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSelectModelElementFromEObjectCommand_EObject(), this.getCommandParameter(), null, "eObject", null, 1, 1, SelectModelElementFromEObjectCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSelectModelElementFromEObjectCommand_AddToList(), this.getCommandParameter(), null, "addToList", null, 0, 1, SelectModelElementFromEObjectCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSelectModelElementFromEObjectCommand_List(), this.getCommandParameter(), null, "list", null, 0, 1, SelectModelElementFromEObjectCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSelectModelElementFromEObjectCommand_Identifier(), ecorePackage.getEString(), "identifier", "es.esi.gemde.methodmanager.commands.selectelemfromeobjectcommand", 1, 1, SelectModelElementFromEObjectCommand.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(selectValidationCommandEClass, SelectValidationCommand.class, "SelectValidationCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getSelectValidationCommand_Identifier(), ecorePackage.getEString(), "identifier", "es.esi.gemde.methodmanager.commands.selectvalidationcommand", 1, 1, SelectValidationCommand.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(selectTransformationCommandEClass, SelectTransformationCommand.class, "SelectTransformationCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getSelectTransformationCommand_Identifier(), ecorePackage.getEString(), "identifier", "es.esi.gemde.methodmanager.commands.selecttransformationcommand", 1, 1, SelectTransformationCommand.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(launchValidationCommandEClass, LaunchValidationCommand.class, "LaunchValidationCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getLaunchValidationCommand_Identifier(), ecorePackage.getEString(), "identifier", "es.esi.gemde.methodmanager.commands.launchvalidationcommand", 1, 1, LaunchValidationCommand.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLaunchValidationCommand_Validation(), this.getCommandParameter(), null, "validation", null, 1, 1, LaunchValidationCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLaunchValidationCommand_Eobject(), this.getCommandParameter(), null, "eobject", null, 0, 1, LaunchValidationCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLaunchValidationCommand_ModelFile(), this.getCommandParameter(), null, "modelFile", null, 0, 1, LaunchValidationCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(launchTransformationCommandEClass, LaunchTransformationCommand.class, "LaunchTransformationCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getLaunchTransformationCommand_Identifier(), ecorePackage.getEString(), "identifier", "es.esi.gemde.methodmanager.commands.launchtransformationcommand", 1, 1, LaunchTransformationCommand.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLaunchTransformationCommand_Transformation(), this.getCommandParameter(), null, "transformation", null, 1, 1, LaunchTransformationCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLaunchTransformationCommand_Inputs(), this.getCommandParameter(), null, "inputs", null, 1, 1, LaunchTransformationCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLaunchTransformationCommand_Outputs(), this.getCommandParameter(), null, "outputs", null, 0, 1, LaunchTransformationCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getLaunchTransformationCommand_OutputPath(), this.getCommandParameter(), null, "outputPath", null, 0, 1, LaunchTransformationCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(openEditorCommandEClass, OpenEditorCommand.class, "OpenEditorCommand", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getOpenEditorCommand_EditorID(), this.getCommandParameter(), null, "editorID", null, 1, 1, OpenEditorCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getOpenEditorCommand_OpenedFile(), this.getCommandParameter(), null, "openedFile", null, 0, 1, OpenEditorCommand.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getOpenEditorCommand_Identifier(), ecorePackage.getEString(), "identifier", "es.esi.gemde.methodmanager.commands.openeditorcommand", 1, 1, OpenEditorCommand.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Create resource
    createResource(eNS_URI);

    // Create annotations
    // emf.gen
    createEmfAnnotations();
    // gmf
    createGmfAnnotations();
    // gmf.diagram
    createGmf_1Annotations();
    // gmf.node
    createGmf_2Annotations();
    // gmf.compartment
    createGmf_3Annotations();
    // gmf.link
    createGmf_4Annotations();
  }

  /**
   * Initializes the annotations for <b>emf.gen</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createEmfAnnotations()
  {
    String source = "emf.gen";		
    addAnnotation
      (this, 
       source, 
       new String[] 
       {
       "basePackage", "es.esi.gemde.methodmanager.methodmodel"
       });																																	
  }

  /**
   * Initializes the annotations for <b>gmf</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createGmfAnnotations()
  {
    String source = "gmf";			
    addAnnotation
      (this, 
       source, 
       new String[] 
       {
       });																																
  }

  /**
   * Initializes the annotations for <b>gmf.diagram</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createGmf_1Annotations()
  {
    String source = "gmf.diagram";				
    addAnnotation
      (gemdeMethodEClass, 
       source, 
       new String[] 
       {
       "model.extension", "method",
       "diagram.extension", "methoddi"
       });																															
  }

  /**
   * Initializes the annotations for <b>gmf.node</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createGmf_2Annotations()
  {
    String source = "gmf.node";					
    addAnnotation
      (methodVariableEClass, 
       source, 
       new String[] 
       {
       "figure", "rectangle",
       "size", "75,40",
       "border.color", "0,0,0",
       "color", "255,255,255",
       "label", "name,type",
       "label.view.pattern", "{0}:{1}",
       "label.edit.pattern", "{0}:{1}",
       "tool.name", "Add Variable"
       });		
    addAnnotation
      (internalVariableEClass, 
       source, 
       new String[] 
       {
       "figure", "rectangle",
       "size", "75,40",
       "border.color", "0,0,0",
       "color", "255,255,255",
       "label", "name",
       "tool.name", "Add Internal Variable"
       });		
    addAnnotation
      (phaseEClass, 
       source, 
       new String[] 
       {
       "figure", "rounded",
       "size", "500,300",
       "border.color", "0,0,50",
       "color", "153,153,194",
       "label", "title",
       "tool.name", "Add Method Phase"
       });				
    addAnnotation
      (taskEClass, 
       source, 
       new String[] 
       {
       "figure", "rounded",
       "size", "100,50",
       "border.color", "26,51,128",
       "color", "214,224,255",
       "label", "title",
       "tool.name", "Add Task"
       });					
    addAnnotation
      (subTaskEClass, 
       source, 
       new String[] 
       {
       "figure", "rounded",
       "size", "100,50",
       "border.color", "122,163,204",
       "color", "204,230,255",
       "label", "title",
       "tool.name", "Add SubTask"
       });			
    addAnnotation
      (conditionalSubTaskEClass, 
       source, 
       new String[] 
       {
       "figure", "rounded",
       "size", "100,50",
       "border.color", "122,163,204",
       "color", "204,230,255",
       "label", "title",
       "tool.name", "Add Conditional SubTask"
       });		
    addAnnotation
      (repeatedSubTaskEClass, 
       source, 
       new String[] 
       {
       "figure", "rounded",
       "size", "100,50",
       "border.color", "122,163,204",
       "color", "204,230,255",
       "label", "title,repetitionVector",
       "label.pattern", "{0} {1}",
       "label.readOnly", "true",
       "tool.name", "Add Repeated SubTask"
       });			
    addAnnotation
      (variableCommandParameterEClass, 
       source, 
       new String[] 
       {
       "figure", "rectangle",
       "border.color", "230,204,128",
       "color", "230,204,128",
       "label", "identifier",
       "tool.name", "Add Variable Command Parameter"
       });		
    addAnnotation
      (constantCommandParameterEClass, 
       source, 
       new String[] 
       {
       "figure", "rectangle",
       "border.color", "230,204,128",
       "color", "230,204,128",
       "label", "identifier",
       "tool.name", "Add Constant Command Parameter"
       });		
    addAnnotation
      (complexCommandEClass, 
       source, 
       new String[] 
       {
       "figure", "rectangle",
       "border.color", "102,76,0",
       "color", "230,204,128",
       "label", "name",
       "tool.name", "Add Complex Command"
       });				
    addAnnotation
      (customCommandEClass, 
       source, 
       new String[] 
       {
       "figure", "rectangle",
       "border.color", "102,76,0",
       "color", "230,204,128",
       "label", "name",
       "tool.name", "Add Custom Command"
       });		
    addAnnotation
      (callWizardCommandEClass, 
       source, 
       new String[] 
       {
       "figure", "rectangle",
       "border.color", "102,76,0",
       "color", "230,204,128",
       "label", "name",
       "tool.name", "Add Call Wizard Command"
       });		
    addAnnotation
      (selectFileCommandEClass, 
       source, 
       new String[] 
       {
       "figure", "rectangle",
       "border.color", "102,76,0",
       "color", "230,204,128",
       "label", "name",
       "tool.name", "Add Select File"
       });		
    addAnnotation
      (selectFolderCommandEClass, 
       source, 
       new String[] 
       {
       "figure", "rectangle",
       "border.color", "102,76,0",
       "color", "230,204,128",
       "label", "name",
       "tool.name", "Add Select Folder Command"
       });		
    addAnnotation
      (selectMetamodelCommandEClass, 
       source, 
       new String[] 
       {
       "figure", "rectangle",
       "border.color", "102,76,0",
       "color", "230,204,128",
       "label", "name",
       "tool.name", "Add Select Metamodel"
       });		
    addAnnotation
      (selectModelElementFromFileCommandEClass, 
       source, 
       new String[] 
       {
       "figure", "rectangle",
       "border.color", "102,76,0",
       "color", "230,204,128",
       "label", "name",
       "tool.name", "Add Select EObject from File Command"
       });		
    addAnnotation
      (selectModelElementFromEObjectCommandEClass, 
       source, 
       new String[] 
       {
       "figure", "rectangle",
       "border.color", "102,76,0",
       "color", "230,204,128",
       "label", "name",
       "tool.name", "Add Select EObject from Object Command"
       });		
    addAnnotation
      (selectValidationCommandEClass, 
       source, 
       new String[] 
       {
       "figure", "rectangle",
       "border.color", "102,76,0",
       "color", "230,204,128",
       "label", "name",
       "tool.name", "Add Select Validation Command"
       });		
    addAnnotation
      (selectTransformationCommandEClass, 
       source, 
       new String[] 
       {
       "figure", "rectangle",
       "border.color", "102,76,0",
       "color", "230,204,128",
       "label", "name",
       "tool.name", "Add Select Transformation Command"
       });		
    addAnnotation
      (launchValidationCommandEClass, 
       source, 
       new String[] 
       {
       "figure", "rectangle",
       "border.color", "102,76,0",
       "color", "230,204,128",
       "label", "name",
       "tool.name", "Add Launch Validation Command"
       });		
    addAnnotation
      (launchTransformationCommandEClass, 
       source, 
       new String[] 
       {
       "figure", "rectangle",
       "border.color", "102,76,0",
       "color", "230,204,128",
       "label", "name",
       "tool.name", "Add Launch Transformation Command"
       });		
    addAnnotation
      (openEditorCommandEClass, 
       source, 
       new String[] 
       {
       "figure", "rectangle",
       "border.color", "102,76,0",
       "color", "230,204,128",
       "label", "name",
       "tool.name", "Add Open Editor Command"
       });
  }

  /**
   * Initializes the annotations for <b>gmf.compartment</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createGmf_3Annotations()
  {
    String source = "gmf.compartment";								
    addAnnotation
      (getPhase_Tasks(), 
       source, 
       new String[] 
       {
       "collapsible", "false",
       "layout", "free"
       });				
    addAnnotation
      (getTask_SubTasks(), 
       source, 
       new String[] 
       {
       "collapsible", "true",
       "layout", "free"
       });													
    addAnnotation
      (getSimpleCommand_Parameters(), 
       source, 
       new String[] 
       {
       "collapsible", "true",
       "layout", "list"
       });												
  }

  /**
   * Initializes the annotations for <b>gmf.link</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createGmf_4Annotations()
  {
    String source = "gmf.link";									
    addAnnotation
      (getPhase_Next(), 
       source, 
       new String[] 
       {
       "target.decoration", "arrow",
       "color", "0,0,50",
       "tool.name", "Next Phase",
       "tool.small.bundle", "es.esi.gemde.methodmanager.methodmodel.edit",
       "tool.small.path", "icons/full/obj16/NextPhaseTool.gif"
       });				
    addAnnotation
      (getTask_Next(), 
       source, 
       new String[] 
       {
       "target.decoration", "arrow",
       "color", "26,51,128",
       "tool.name", "Next Task",
       "tool.small.bundle", "es.esi.gemde.methodmanager.methodmodel.edit",
       "tool.small.path", "icons/full/obj16/NextTaskTool.gif"
       });		
    addAnnotation
      (getAbstractSubTask_Next(), 
       source, 
       new String[] 
       {
       "target.decoration", "arrow",
       "color", "122,163,204",
       "tool.name", "Next SubTask",
       "tool.small.bundle", "es.esi.gemde.methodmanager.methodmodel.edit",
       "tool.small.path", "icons/full/obj16/NextSubTaskTool.gif"
       });			
    addAnnotation
      (conditionEClass, 
       source, 
       new String[] 
       {
       "source", "from",
       "target", "to",
       "label", "value",
       "source.decoration", "filledrhomb",
       "color", "122,163,204",
       "tool.name", "Conditional SubTask",
       "tool.small.bundle", "es.esi.gemde.methodmanager.methodmodel.edit",
       "tool.small.path", "icons/full/obj16/ConditionLinkTool.gif"
       });				
    addAnnotation
      (getRepeatedSubTask_RepeatedElement(), 
       source, 
       new String[] 
       {
       "style", "dot",
       "width", "2",
       "color", "122,163,204",
       "tool.name", "Repeated SubTask",
       "tool.small.bundle", "es.esi.gemde.methodmanager.methodmodel.edit",
       "tool.small.path", "icons/full/obj16/RepeatedSubTaskTool.gif"
       });					
    addAnnotation
      (getComplexCommand_Commands(), 
       source, 
       new String[] 
       {
       "source.decoration", "filledrhomb",
       "color", "102,76,0",
       "tool.name", "Composing Command",
       "tool.small.bundle", "es.esi.gemde.methodmanager.methodmodel.edit",
       "tool.small.path", "icons/full/obj16/ComposingCommandTool.gif"
       });													
  }

} //MethodPackageImpl
