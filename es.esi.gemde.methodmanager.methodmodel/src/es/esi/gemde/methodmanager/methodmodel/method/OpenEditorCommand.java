/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Open Editor Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.OpenEditorCommand#getEditorID <em>Editor ID</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.OpenEditorCommand#getOpenedFile <em>Opened File</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.OpenEditorCommand#getIdentifier <em>Identifier</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getOpenEditorCommand()
 * @model annotation="gmf.node figure='rectangle' border.color='102,76,0' color='230,204,128' label='name' tool.name='Add Open Editor Command'"
 * @generated
 */
public interface OpenEditorCommand extends SimpleCommand
{
  /**
   * Returns the value of the '<em><b>Editor ID</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Editor ID</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Editor ID</em>' reference.
   * @see #setEditorID(CommandParameter)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getOpenEditorCommand_EditorID()
   * @model required="true"
   * @generated
   */
  CommandParameter getEditorID();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.OpenEditorCommand#getEditorID <em>Editor ID</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Editor ID</em>' reference.
   * @see #getEditorID()
   * @generated
   */
  void setEditorID(CommandParameter value);

  /**
   * Returns the value of the '<em><b>Opened File</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Opened File</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Opened File</em>' reference.
   * @see #setOpenedFile(CommandParameter)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getOpenEditorCommand_OpenedFile()
   * @model
   * @generated
   */
  CommandParameter getOpenedFile();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.OpenEditorCommand#getOpenedFile <em>Opened File</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Opened File</em>' reference.
   * @see #getOpenedFile()
   * @generated
   */
  void setOpenedFile(CommandParameter value);

  /**
   * Returns the value of the '<em><b>Identifier</b></em>' attribute.
   * The default value is <code>"es.esi.gemde.methodmanager.commands.openeditorcommand"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Identifier</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Identifier</em>' attribute.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getOpenEditorCommand_Identifier()
   * @model default="es.esi.gemde.methodmanager.commands.openeditorcommand" required="true" changeable="false"
   * @generated
   */
  String getIdentifier();

} // OpenEditorCommand
