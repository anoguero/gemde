/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Launch Transformation Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand#getTransformation <em>Transformation</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand#getInputs <em>Inputs</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand#getOutputs <em>Outputs</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand#getOutputPath <em>Output Path</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getLaunchTransformationCommand()
 * @model annotation="gmf.node figure='rectangle' border.color='102,76,0' color='230,204,128' label='name' tool.name='Add Launch Transformation Command'"
 * @generated
 */
public interface LaunchTransformationCommand extends SimpleCommand
{
  /**
   * Returns the value of the '<em><b>Identifier</b></em>' attribute.
   * The default value is <code>"es.esi.gemde.methodmanager.commands.launchtransformationcommand"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Identifier</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Identifier</em>' attribute.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getLaunchTransformationCommand_Identifier()
   * @model default="es.esi.gemde.methodmanager.commands.launchtransformationcommand" required="true" changeable="false"
   * @generated
   */
  String getIdentifier();

  /**
   * Returns the value of the '<em><b>Transformation</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Transformation</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Transformation</em>' reference.
   * @see #setTransformation(CommandParameter)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getLaunchTransformationCommand_Transformation()
   * @model required="true"
   * @generated
   */
  CommandParameter getTransformation();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand#getTransformation <em>Transformation</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Transformation</em>' reference.
   * @see #getTransformation()
   * @generated
   */
  void setTransformation(CommandParameter value);

  /**
   * Returns the value of the '<em><b>Inputs</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Inputs</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Inputs</em>' reference.
   * @see #setInputs(CommandParameter)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getLaunchTransformationCommand_Inputs()
   * @model required="true"
   * @generated
   */
  CommandParameter getInputs();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand#getInputs <em>Inputs</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Inputs</em>' reference.
   * @see #getInputs()
   * @generated
   */
  void setInputs(CommandParameter value);

  /**
   * Returns the value of the '<em><b>Outputs</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Outputs</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Outputs</em>' reference.
   * @see #setOutputs(CommandParameter)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getLaunchTransformationCommand_Outputs()
   * @model
   * @generated
   */
  CommandParameter getOutputs();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand#getOutputs <em>Outputs</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Outputs</em>' reference.
   * @see #getOutputs()
   * @generated
   */
  void setOutputs(CommandParameter value);

  /**
   * Returns the value of the '<em><b>Output Path</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Output Path</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Output Path</em>' reference.
   * @see #setOutputPath(CommandParameter)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getLaunchTransformationCommand_OutputPath()
   * @model
   * @generated
   */
  CommandParameter getOutputPath();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand#getOutputPath <em>Output Path</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Output Path</em>' reference.
   * @see #getOutputPath()
   * @generated
   */
  void setOutputPath(CommandParameter value);

} // LaunchTransformationCommand
