/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.impl;

import es.esi.gemde.methodmanager.methodmodel.method.CommandParameter;
import es.esi.gemde.methodmanager.methodmodel.method.MethodPackage;
import es.esi.gemde.methodmanager.methodmodel.method.OpenEditorCommand;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Open Editor Command</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.OpenEditorCommandImpl#getEditorID <em>Editor ID</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.OpenEditorCommandImpl#getOpenedFile <em>Opened File</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.OpenEditorCommandImpl#getIdentifier <em>Identifier</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class OpenEditorCommandImpl extends SimpleCommandImpl implements OpenEditorCommand
{
  /**
   * The cached value of the '{@link #getEditorID() <em>Editor ID</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEditorID()
   * @generated
   * @ordered
   */
  protected CommandParameter editorID;

  /**
   * The cached value of the '{@link #getOpenedFile() <em>Opened File</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOpenedFile()
   * @generated
   * @ordered
   */
  protected CommandParameter openedFile;

  /**
   * The default value of the '{@link #getIdentifier() <em>Identifier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdentifier()
   * @generated
   * @ordered
   */
  protected static final String IDENTIFIER_EDEFAULT = "es.esi.gemde.methodmanager.commands.openeditorcommand";

  /**
   * The cached value of the '{@link #getIdentifier() <em>Identifier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdentifier()
   * @generated
   * @ordered
   */
  protected String identifier = IDENTIFIER_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OpenEditorCommandImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MethodPackage.Literals.OPEN_EDITOR_COMMAND;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter getEditorID()
  {
    if (editorID != null && editorID.eIsProxy())
    {
      InternalEObject oldEditorID = (InternalEObject)editorID;
      editorID = (CommandParameter)eResolveProxy(oldEditorID);
      if (editorID != oldEditorID)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.OPEN_EDITOR_COMMAND__EDITOR_ID, oldEditorID, editorID));
      }
    }
    return editorID;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter basicGetEditorID()
  {
    return editorID;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEditorID(CommandParameter newEditorID)
  {
    CommandParameter oldEditorID = editorID;
    editorID = newEditorID;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.OPEN_EDITOR_COMMAND__EDITOR_ID, oldEditorID, editorID));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter getOpenedFile()
  {
    if (openedFile != null && openedFile.eIsProxy())
    {
      InternalEObject oldOpenedFile = (InternalEObject)openedFile;
      openedFile = (CommandParameter)eResolveProxy(oldOpenedFile);
      if (openedFile != oldOpenedFile)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.OPEN_EDITOR_COMMAND__OPENED_FILE, oldOpenedFile, openedFile));
      }
    }
    return openedFile;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter basicGetOpenedFile()
  {
    return openedFile;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOpenedFile(CommandParameter newOpenedFile)
  {
    CommandParameter oldOpenedFile = openedFile;
    openedFile = newOpenedFile;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.OPEN_EDITOR_COMMAND__OPENED_FILE, oldOpenedFile, openedFile));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getIdentifier()
  {
    return identifier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MethodPackage.OPEN_EDITOR_COMMAND__EDITOR_ID:
        if (resolve) return getEditorID();
        return basicGetEditorID();
      case MethodPackage.OPEN_EDITOR_COMMAND__OPENED_FILE:
        if (resolve) return getOpenedFile();
        return basicGetOpenedFile();
      case MethodPackage.OPEN_EDITOR_COMMAND__IDENTIFIER:
        return getIdentifier();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MethodPackage.OPEN_EDITOR_COMMAND__EDITOR_ID:
        setEditorID((CommandParameter)newValue);
        return;
      case MethodPackage.OPEN_EDITOR_COMMAND__OPENED_FILE:
        setOpenedFile((CommandParameter)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.OPEN_EDITOR_COMMAND__EDITOR_ID:
        setEditorID((CommandParameter)null);
        return;
      case MethodPackage.OPEN_EDITOR_COMMAND__OPENED_FILE:
        setOpenedFile((CommandParameter)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.OPEN_EDITOR_COMMAND__EDITOR_ID:
        return editorID != null;
      case MethodPackage.OPEN_EDITOR_COMMAND__OPENED_FILE:
        return openedFile != null;
      case MethodPackage.OPEN_EDITOR_COMMAND__IDENTIFIER:
        return IDENTIFIER_EDEFAULT == null ? identifier != null : !IDENTIFIER_EDEFAULT.equals(identifier);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (identifier: ");
    result.append(identifier);
    result.append(')');
    return result.toString();
  }

} //OpenEditorCommandImpl
