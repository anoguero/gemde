/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.impl;

import es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask;
import es.esi.gemde.methodmanager.methodmodel.method.MethodPackage;
import es.esi.gemde.methodmanager.methodmodel.method.SubTask;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Abstract Sub Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.AbstractSubTaskImpl#getPrev <em>Prev</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.AbstractSubTaskImpl#getNext <em>Next</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class AbstractSubTaskImpl extends MethodStepImpl implements AbstractSubTask
{
  /**
   * The cached value of the '{@link #getPrev() <em>Prev</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPrev()
   * @generated
   * @ordered
   */
  protected AbstractSubTask prev;

  /**
   * The cached value of the '{@link #getNext() <em>Next</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNext()
   * @generated
   * @ordered
   */
  protected AbstractSubTask next;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AbstractSubTaskImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MethodPackage.Literals.ABSTRACT_SUB_TASK;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AbstractSubTask getPrev()
  {
    if (prev != null && prev.eIsProxy())
    {
      InternalEObject oldPrev = (InternalEObject)prev;
      prev = (AbstractSubTask)eResolveProxy(oldPrev);
      if (prev != oldPrev)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.ABSTRACT_SUB_TASK__PREV, oldPrev, prev));
      }
    }
    return prev;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AbstractSubTask basicGetPrev()
  {
    return prev;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPrev(AbstractSubTask newPrev, NotificationChain msgs)
  {
    AbstractSubTask oldPrev = prev;
    prev = newPrev;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MethodPackage.ABSTRACT_SUB_TASK__PREV, oldPrev, newPrev);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPrev(AbstractSubTask newPrev)
  {
    if (newPrev != prev)
    {
      NotificationChain msgs = null;
      if (prev != null)
        msgs = ((InternalEObject)prev).eInverseRemove(this, MethodPackage.ABSTRACT_SUB_TASK__NEXT, AbstractSubTask.class, msgs);
      if (newPrev != null)
        msgs = ((InternalEObject)newPrev).eInverseAdd(this, MethodPackage.ABSTRACT_SUB_TASK__NEXT, AbstractSubTask.class, msgs);
      msgs = basicSetPrev(newPrev, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.ABSTRACT_SUB_TASK__PREV, newPrev, newPrev));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AbstractSubTask getNext()
  {
    if (next != null && next.eIsProxy())
    {
      InternalEObject oldNext = (InternalEObject)next;
      next = (AbstractSubTask)eResolveProxy(oldNext);
      if (next != oldNext)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.ABSTRACT_SUB_TASK__NEXT, oldNext, next));
      }
    }
    return next;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AbstractSubTask basicGetNext()
  {
    return next;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetNext(AbstractSubTask newNext, NotificationChain msgs)
  {
    AbstractSubTask oldNext = next;
    next = newNext;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MethodPackage.ABSTRACT_SUB_TASK__NEXT, oldNext, newNext);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNext(AbstractSubTask newNext)
  {
    if (newNext != next)
    {
      NotificationChain msgs = null;
      if (next != null)
        msgs = ((InternalEObject)next).eInverseRemove(this, MethodPackage.ABSTRACT_SUB_TASK__PREV, AbstractSubTask.class, msgs);
      if (newNext != null)
        msgs = ((InternalEObject)newNext).eInverseAdd(this, MethodPackage.ABSTRACT_SUB_TASK__PREV, AbstractSubTask.class, msgs);
      msgs = basicSetNext(newNext, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.ABSTRACT_SUB_TASK__NEXT, newNext, newNext));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MethodPackage.ABSTRACT_SUB_TASK__PREV:
        if (prev != null)
          msgs = ((InternalEObject)prev).eInverseRemove(this, MethodPackage.ABSTRACT_SUB_TASK__NEXT, AbstractSubTask.class, msgs);
        return basicSetPrev((AbstractSubTask)otherEnd, msgs);
      case MethodPackage.ABSTRACT_SUB_TASK__NEXT:
        if (next != null)
          msgs = ((InternalEObject)next).eInverseRemove(this, MethodPackage.ABSTRACT_SUB_TASK__PREV, AbstractSubTask.class, msgs);
        return basicSetNext((AbstractSubTask)otherEnd, msgs);
    }
    return super.eInverseAdd(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MethodPackage.ABSTRACT_SUB_TASK__PREV:
        return basicSetPrev(null, msgs);
      case MethodPackage.ABSTRACT_SUB_TASK__NEXT:
        return basicSetNext(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MethodPackage.ABSTRACT_SUB_TASK__PREV:
        if (resolve) return getPrev();
        return basicGetPrev();
      case MethodPackage.ABSTRACT_SUB_TASK__NEXT:
        if (resolve) return getNext();
        return basicGetNext();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MethodPackage.ABSTRACT_SUB_TASK__PREV:
        setPrev((AbstractSubTask)newValue);
        return;
      case MethodPackage.ABSTRACT_SUB_TASK__NEXT:
        setNext((AbstractSubTask)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.ABSTRACT_SUB_TASK__PREV:
        setPrev((AbstractSubTask)null);
        return;
      case MethodPackage.ABSTRACT_SUB_TASK__NEXT:
        setNext((AbstractSubTask)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.ABSTRACT_SUB_TASK__PREV:
        return prev != null;
      case MethodPackage.ABSTRACT_SUB_TASK__NEXT:
        return next != null;
    }
    return super.eIsSet(featureID);
  }

} //AbstractSubTaskImpl
