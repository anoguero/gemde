/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodFactory
 * @model kind="package"
 *        annotation="emf.gen basePackage='es.esi.gemde.methodmanager.methodmodel'"
 * @generated
 */
public interface MethodPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "method";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://es.esi.gemde/methodmanager/methodmodel";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "method";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  MethodPackage eINSTANCE = es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl.init();

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.GEMDEMethodImpl <em>GEMDE Method</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.GEMDEMethodImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getGEMDEMethod()
   * @generated
   */
  int GEMDE_METHOD = 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GEMDE_METHOD__NAME = 0;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GEMDE_METHOD__DESCRIPTION = 1;

  /**
   * The feature id for the '<em><b>Project Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GEMDE_METHOD__PROJECT_NAME = 2;

  /**
   * The feature id for the '<em><b>Phases</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GEMDE_METHOD__PHASES = 3;

  /**
   * The feature id for the '<em><b>Initial Phase</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GEMDE_METHOD__INITIAL_PHASE = 4;

  /**
   * The feature id for the '<em><b>Variables</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GEMDE_METHOD__VARIABLES = 5;

  /**
   * The feature id for the '<em><b>Commands</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GEMDE_METHOD__COMMANDS = 6;

  /**
   * The number of structural features of the '<em>GEMDE Method</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int GEMDE_METHOD_FEATURE_COUNT = 7;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.MethodStepImpl <em>Step</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodStepImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getMethodStep()
   * @generated
   */
  int METHOD_STEP = 1;

  /**
   * The feature id for the '<em><b>Title</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_STEP__TITLE = 0;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_STEP__DESCRIPTION = 1;

  /**
   * The number of structural features of the '<em>Step</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_STEP_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPartImpl <em>Part</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPartImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getMethodPart()
   * @generated
   */
  int METHOD_PART = 2;

  /**
   * The feature id for the '<em><b>Title</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_PART__TITLE = METHOD_STEP__TITLE;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_PART__DESCRIPTION = METHOD_STEP__DESCRIPTION;

  /**
   * The feature id for the '<em><b>Introduction</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_PART__INTRODUCTION = METHOD_STEP_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Conclusion</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_PART__CONCLUSION = METHOD_STEP_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Is Optional</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_PART__IS_OPTIONAL = METHOD_STEP_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Part</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_PART_FEATURE_COUNT = METHOD_STEP_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.ExecutableElementImpl <em>Executable Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.ExecutableElementImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getExecutableElement()
   * @generated
   */
  int EXECUTABLE_ELEMENT = 3;

  /**
   * The feature id for the '<em><b>Command</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXECUTABLE_ELEMENT__COMMAND = 0;

  /**
   * The number of structural features of the '<em>Executable Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int EXECUTABLE_ELEMENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.DataStorageElementImpl <em>Data Storage Element</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.DataStorageElementImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getDataStorageElement()
   * @generated
   */
  int DATA_STORAGE_ELEMENT = 4;

  /**
   * The number of structural features of the '<em>Data Storage Element</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DATA_STORAGE_ELEMENT_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.MethodVariableImpl <em>Variable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodVariableImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getMethodVariable()
   * @generated
   */
  int METHOD_VARIABLE = 5;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_VARIABLE__IDENTIFIER = DATA_STORAGE_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_VARIABLE__NAME = DATA_STORAGE_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Type</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_VARIABLE__TYPE = DATA_STORAGE_ELEMENT_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Variable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_VARIABLE_FEATURE_COUNT = DATA_STORAGE_ELEMENT_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.InternalVariableImpl <em>Internal Variable</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.InternalVariableImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getInternalVariable()
   * @generated
   */
  int INTERNAL_VARIABLE = 6;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERNAL_VARIABLE__NAME = DATA_STORAGE_ELEMENT_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Internal Variable</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int INTERNAL_VARIABLE_FEATURE_COUNT = DATA_STORAGE_ELEMENT_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.PhaseImpl <em>Phase</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.PhaseImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getPhase()
   * @generated
   */
  int PHASE = 7;

  /**
   * The feature id for the '<em><b>Title</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHASE__TITLE = METHOD_PART__TITLE;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHASE__DESCRIPTION = METHOD_PART__DESCRIPTION;

  /**
   * The feature id for the '<em><b>Introduction</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHASE__INTRODUCTION = METHOD_PART__INTRODUCTION;

  /**
   * The feature id for the '<em><b>Conclusion</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHASE__CONCLUSION = METHOD_PART__CONCLUSION;

  /**
   * The feature id for the '<em><b>Is Optional</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHASE__IS_OPTIONAL = METHOD_PART__IS_OPTIONAL;

  /**
   * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHASE__TASKS = METHOD_PART_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Initial Task</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHASE__INITIAL_TASK = METHOD_PART_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Prev</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHASE__PREV = METHOD_PART_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Next</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHASE__NEXT = METHOD_PART_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Phase</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int PHASE_FEATURE_COUNT = METHOD_PART_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.TaskImpl <em>Task</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.TaskImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getTask()
   * @generated
   */
  int TASK = 8;

  /**
   * The feature id for the '<em><b>Title</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASK__TITLE = METHOD_STEP__TITLE;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASK__DESCRIPTION = METHOD_STEP__DESCRIPTION;

  /**
   * The feature id for the '<em><b>Command</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASK__COMMAND = METHOD_STEP_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Sub Tasks</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASK__SUB_TASKS = METHOD_STEP_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Initial Subtask</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASK__INITIAL_SUBTASK = METHOD_STEP_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Prev</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASK__PREV = METHOD_STEP_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Next</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASK__NEXT = METHOD_STEP_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>Task</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TASK_FEATURE_COUNT = METHOD_STEP_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.AbstractSubTaskImpl <em>Abstract Sub Task</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.AbstractSubTaskImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getAbstractSubTask()
   * @generated
   */
  int ABSTRACT_SUB_TASK = 9;

  /**
   * The feature id for the '<em><b>Title</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSTRACT_SUB_TASK__TITLE = METHOD_STEP__TITLE;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSTRACT_SUB_TASK__DESCRIPTION = METHOD_STEP__DESCRIPTION;

  /**
   * The feature id for the '<em><b>Prev</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSTRACT_SUB_TASK__PREV = METHOD_STEP_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Next</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSTRACT_SUB_TASK__NEXT = METHOD_STEP_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Abstract Sub Task</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ABSTRACT_SUB_TASK_FEATURE_COUNT = METHOD_STEP_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SubTaskImpl <em>Sub Task</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.SubTaskImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getSubTask()
   * @generated
   */
  int SUB_TASK = 10;

  /**
   * The feature id for the '<em><b>Title</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TASK__TITLE = ABSTRACT_SUB_TASK__TITLE;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TASK__DESCRIPTION = ABSTRACT_SUB_TASK__DESCRIPTION;

  /**
   * The feature id for the '<em><b>Prev</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TASK__PREV = ABSTRACT_SUB_TASK__PREV;

  /**
   * The feature id for the '<em><b>Next</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TASK__NEXT = ABSTRACT_SUB_TASK__NEXT;

  /**
   * The feature id for the '<em><b>Command</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TASK__COMMAND = ABSTRACT_SUB_TASK_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Sub Task</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SUB_TASK_FEATURE_COUNT = ABSTRACT_SUB_TASK_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.ConditionImpl <em>Condition</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.ConditionImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getCondition()
   * @generated
   */
  int CONDITION = 11;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITION__VALUE = 0;

  /**
   * The feature id for the '<em><b>From</b></em>' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITION__FROM = 1;

  /**
   * The feature id for the '<em><b>To</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITION__TO = 2;

  /**
   * The number of structural features of the '<em>Condition</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.ConditionalSubTaskImpl <em>Conditional Sub Task</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.ConditionalSubTaskImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getConditionalSubTask()
   * @generated
   */
  int CONDITIONAL_SUB_TASK = 12;

  /**
   * The feature id for the '<em><b>Title</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_SUB_TASK__TITLE = ABSTRACT_SUB_TASK__TITLE;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_SUB_TASK__DESCRIPTION = ABSTRACT_SUB_TASK__DESCRIPTION;

  /**
   * The feature id for the '<em><b>Prev</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_SUB_TASK__PREV = ABSTRACT_SUB_TASK__PREV;

  /**
   * The feature id for the '<em><b>Next</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_SUB_TASK__NEXT = ABSTRACT_SUB_TASK__NEXT;

  /**
   * The feature id for the '<em><b>Variable</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_SUB_TASK__VARIABLE = ABSTRACT_SUB_TASK_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Conditional Links</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_SUB_TASK__CONDITIONAL_LINKS = ABSTRACT_SUB_TASK_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Conditional Sub Task</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONDITIONAL_SUB_TASK_FEATURE_COUNT = ABSTRACT_SUB_TASK_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.RepeatedSubTaskImpl <em>Repeated Sub Task</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.RepeatedSubTaskImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getRepeatedSubTask()
   * @generated
   */
  int REPEATED_SUB_TASK = 13;

  /**
   * The feature id for the '<em><b>Title</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPEATED_SUB_TASK__TITLE = ABSTRACT_SUB_TASK__TITLE;

  /**
   * The feature id for the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPEATED_SUB_TASK__DESCRIPTION = ABSTRACT_SUB_TASK__DESCRIPTION;

  /**
   * The feature id for the '<em><b>Prev</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPEATED_SUB_TASK__PREV = ABSTRACT_SUB_TASK__PREV;

  /**
   * The feature id for the '<em><b>Next</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPEATED_SUB_TASK__NEXT = ABSTRACT_SUB_TASK__NEXT;

  /**
   * The feature id for the '<em><b>Repeated Element</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPEATED_SUB_TASK__REPEATED_ELEMENT = ABSTRACT_SUB_TASK_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Repetition Vector</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPEATED_SUB_TASK__REPETITION_VECTOR = ABSTRACT_SUB_TASK_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Repetition Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPEATED_SUB_TASK__REPETITION_VARIABLE = ABSTRACT_SUB_TASK_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Repeated Sub Task</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPEATED_SUB_TASK_FEATURE_COUNT = ABSTRACT_SUB_TASK_FEATURE_COUNT + 3;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.CommandParameterImpl <em>Command Parameter</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.CommandParameterImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getCommandParameter()
   * @generated
   */
  int COMMAND_PARAMETER = 14;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMAND_PARAMETER__IDENTIFIER = 0;

  /**
   * The number of structural features of the '<em>Command Parameter</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMMAND_PARAMETER_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.VariableCommandParameterImpl <em>Variable Command Parameter</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.VariableCommandParameterImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getVariableCommandParameter()
   * @generated
   */
  int VARIABLE_COMMAND_PARAMETER = 15;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_COMMAND_PARAMETER__IDENTIFIER = COMMAND_PARAMETER__IDENTIFIER;

  /**
   * The feature id for the '<em><b>Value</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_COMMAND_PARAMETER__VALUE = COMMAND_PARAMETER_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Variable Command Parameter</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VARIABLE_COMMAND_PARAMETER_FEATURE_COUNT = COMMAND_PARAMETER_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.ConstantCommandParameterImpl <em>Constant Command Parameter</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.ConstantCommandParameterImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getConstantCommandParameter()
   * @generated
   */
  int CONSTANT_COMMAND_PARAMETER = 16;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT_COMMAND_PARAMETER__IDENTIFIER = COMMAND_PARAMETER__IDENTIFIER;

  /**
   * The feature id for the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT_COMMAND_PARAMETER__VALUE = COMMAND_PARAMETER_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Constant Command Parameter</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONSTANT_COMMAND_PARAMETER_FEATURE_COUNT = COMMAND_PARAMETER_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.MethodCommandImpl <em>Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodCommandImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getMethodCommand()
   * @generated
   */
  int METHOD_COMMAND = 17;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_COMMAND__NAME = 0;

  /**
   * The number of structural features of the '<em>Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int METHOD_COMMAND_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.ComplexCommandImpl <em>Complex Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.ComplexCommandImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getComplexCommand()
   * @generated
   */
  int COMPLEX_COMMAND = 18;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPLEX_COMMAND__NAME = METHOD_COMMAND__NAME;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPLEX_COMMAND__IDENTIFIER = METHOD_COMMAND_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Commands</b></em>' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPLEX_COMMAND__COMMANDS = METHOD_COMMAND_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Complex Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int COMPLEX_COMMAND_FEATURE_COUNT = METHOD_COMMAND_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SimpleCommandImpl <em>Simple Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.SimpleCommandImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getSimpleCommand()
   * @generated
   */
  int SIMPLE_COMMAND = 19;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_COMMAND__NAME = METHOD_COMMAND__NAME;

  /**
   * The feature id for the '<em><b>Return Value To</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_COMMAND__RETURN_VALUE_TO = METHOD_COMMAND_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_COMMAND__PARAMETERS = METHOD_COMMAND_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Simple Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SIMPLE_COMMAND_FEATURE_COUNT = METHOD_COMMAND_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.CustomCommandImpl <em>Custom Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.CustomCommandImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getCustomCommand()
   * @generated
   */
  int CUSTOM_COMMAND = 20;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CUSTOM_COMMAND__NAME = SIMPLE_COMMAND__NAME;

  /**
   * The feature id for the '<em><b>Return Value To</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CUSTOM_COMMAND__RETURN_VALUE_TO = SIMPLE_COMMAND__RETURN_VALUE_TO;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CUSTOM_COMMAND__PARAMETERS = SIMPLE_COMMAND__PARAMETERS;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CUSTOM_COMMAND__IDENTIFIER = SIMPLE_COMMAND_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Custom Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CUSTOM_COMMAND_FEATURE_COUNT = SIMPLE_COMMAND_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.CallWizardCommandImpl <em>Call Wizard Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.CallWizardCommandImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getCallWizardCommand()
   * @generated
   */
  int CALL_WIZARD_COMMAND = 21;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_WIZARD_COMMAND__NAME = SIMPLE_COMMAND__NAME;

  /**
   * The feature id for the '<em><b>Return Value To</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_WIZARD_COMMAND__RETURN_VALUE_TO = SIMPLE_COMMAND__RETURN_VALUE_TO;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_WIZARD_COMMAND__PARAMETERS = SIMPLE_COMMAND__PARAMETERS;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_WIZARD_COMMAND__IDENTIFIER = SIMPLE_COMMAND_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Wizard</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_WIZARD_COMMAND__WIZARD = SIMPLE_COMMAND_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Call Wizard Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CALL_WIZARD_COMMAND_FEATURE_COUNT = SIMPLE_COMMAND_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SelectFileCommandImpl <em>Select File Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.SelectFileCommandImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getSelectFileCommand()
   * @generated
   */
  int SELECT_FILE_COMMAND = 22;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_FILE_COMMAND__NAME = SIMPLE_COMMAND__NAME;

  /**
   * The feature id for the '<em><b>Return Value To</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_FILE_COMMAND__RETURN_VALUE_TO = SIMPLE_COMMAND__RETURN_VALUE_TO;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_FILE_COMMAND__PARAMETERS = SIMPLE_COMMAND__PARAMETERS;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_FILE_COMMAND__IDENTIFIER = SIMPLE_COMMAND_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Only Workspace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_FILE_COMMAND__ONLY_WORKSPACE = SIMPLE_COMMAND_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Select File Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_FILE_COMMAND_FEATURE_COUNT = SIMPLE_COMMAND_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SelectFolderCommandImpl <em>Select Folder Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.SelectFolderCommandImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getSelectFolderCommand()
   * @generated
   */
  int SELECT_FOLDER_COMMAND = 23;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_FOLDER_COMMAND__NAME = SIMPLE_COMMAND__NAME;

  /**
   * The feature id for the '<em><b>Return Value To</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_FOLDER_COMMAND__RETURN_VALUE_TO = SIMPLE_COMMAND__RETURN_VALUE_TO;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_FOLDER_COMMAND__PARAMETERS = SIMPLE_COMMAND__PARAMETERS;

  /**
   * The feature id for the '<em><b>Only Workspace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_FOLDER_COMMAND__ONLY_WORKSPACE = SIMPLE_COMMAND_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_FOLDER_COMMAND__IDENTIFIER = SIMPLE_COMMAND_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Select Folder Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_FOLDER_COMMAND_FEATURE_COUNT = SIMPLE_COMMAND_FEATURE_COUNT + 2;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SelectMetamodelCommandImpl <em>Select Metamodel Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.SelectMetamodelCommandImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getSelectMetamodelCommand()
   * @generated
   */
  int SELECT_METAMODEL_COMMAND = 24;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_METAMODEL_COMMAND__NAME = SIMPLE_COMMAND__NAME;

  /**
   * The feature id for the '<em><b>Return Value To</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_METAMODEL_COMMAND__RETURN_VALUE_TO = SIMPLE_COMMAND__RETURN_VALUE_TO;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_METAMODEL_COMMAND__PARAMETERS = SIMPLE_COMMAND__PARAMETERS;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_METAMODEL_COMMAND__IDENTIFIER = SIMPLE_COMMAND_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Select Metamodel Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_METAMODEL_COMMAND_FEATURE_COUNT = SIMPLE_COMMAND_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SelectModelElementFromFileCommandImpl <em>Select Model Element From File Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.SelectModelElementFromFileCommandImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getSelectModelElementFromFileCommand()
   * @generated
   */
  int SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND = 25;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__NAME = SIMPLE_COMMAND__NAME;

  /**
   * The feature id for the '<em><b>Return Value To</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__RETURN_VALUE_TO = SIMPLE_COMMAND__RETURN_VALUE_TO;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__PARAMETERS = SIMPLE_COMMAND__PARAMETERS;

  /**
   * The feature id for the '<em><b>Model File</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__MODEL_FILE = SIMPLE_COMMAND_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Add To List</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__ADD_TO_LIST = SIMPLE_COMMAND_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>List</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__LIST = SIMPLE_COMMAND_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__IDENTIFIER = SIMPLE_COMMAND_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Select Model Element From File Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND_FEATURE_COUNT = SIMPLE_COMMAND_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SelectModelElementFromEObjectCommandImpl <em>Select Model Element From EObject Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.SelectModelElementFromEObjectCommandImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getSelectModelElementFromEObjectCommand()
   * @generated
   */
  int SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND = 26;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND__NAME = SIMPLE_COMMAND__NAME;

  /**
   * The feature id for the '<em><b>Return Value To</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND__RETURN_VALUE_TO = SIMPLE_COMMAND__RETURN_VALUE_TO;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND__PARAMETERS = SIMPLE_COMMAND__PARAMETERS;

  /**
   * The feature id for the '<em><b>EObject</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND__EOBJECT = SIMPLE_COMMAND_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Add To List</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND__ADD_TO_LIST = SIMPLE_COMMAND_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>List</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND__LIST = SIMPLE_COMMAND_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND__IDENTIFIER = SIMPLE_COMMAND_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Select Model Element From EObject Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND_FEATURE_COUNT = SIMPLE_COMMAND_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SelectValidationCommandImpl <em>Select Validation Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.SelectValidationCommandImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getSelectValidationCommand()
   * @generated
   */
  int SELECT_VALIDATION_COMMAND = 27;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_VALIDATION_COMMAND__NAME = SIMPLE_COMMAND__NAME;

  /**
   * The feature id for the '<em><b>Return Value To</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_VALIDATION_COMMAND__RETURN_VALUE_TO = SIMPLE_COMMAND__RETURN_VALUE_TO;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_VALIDATION_COMMAND__PARAMETERS = SIMPLE_COMMAND__PARAMETERS;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_VALIDATION_COMMAND__IDENTIFIER = SIMPLE_COMMAND_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Select Validation Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_VALIDATION_COMMAND_FEATURE_COUNT = SIMPLE_COMMAND_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SelectTransformationCommandImpl <em>Select Transformation Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.SelectTransformationCommandImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getSelectTransformationCommand()
   * @generated
   */
  int SELECT_TRANSFORMATION_COMMAND = 28;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_TRANSFORMATION_COMMAND__NAME = SIMPLE_COMMAND__NAME;

  /**
   * The feature id for the '<em><b>Return Value To</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_TRANSFORMATION_COMMAND__RETURN_VALUE_TO = SIMPLE_COMMAND__RETURN_VALUE_TO;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_TRANSFORMATION_COMMAND__PARAMETERS = SIMPLE_COMMAND__PARAMETERS;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_TRANSFORMATION_COMMAND__IDENTIFIER = SIMPLE_COMMAND_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Select Transformation Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SELECT_TRANSFORMATION_COMMAND_FEATURE_COUNT = SIMPLE_COMMAND_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.LaunchValidationCommandImpl <em>Launch Validation Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.LaunchValidationCommandImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getLaunchValidationCommand()
   * @generated
   */
  int LAUNCH_VALIDATION_COMMAND = 29;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAUNCH_VALIDATION_COMMAND__NAME = SIMPLE_COMMAND__NAME;

  /**
   * The feature id for the '<em><b>Return Value To</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAUNCH_VALIDATION_COMMAND__RETURN_VALUE_TO = SIMPLE_COMMAND__RETURN_VALUE_TO;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAUNCH_VALIDATION_COMMAND__PARAMETERS = SIMPLE_COMMAND__PARAMETERS;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAUNCH_VALIDATION_COMMAND__IDENTIFIER = SIMPLE_COMMAND_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Validation</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAUNCH_VALIDATION_COMMAND__VALIDATION = SIMPLE_COMMAND_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Eobject</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAUNCH_VALIDATION_COMMAND__EOBJECT = SIMPLE_COMMAND_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Model File</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAUNCH_VALIDATION_COMMAND__MODEL_FILE = SIMPLE_COMMAND_FEATURE_COUNT + 3;

  /**
   * The number of structural features of the '<em>Launch Validation Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAUNCH_VALIDATION_COMMAND_FEATURE_COUNT = SIMPLE_COMMAND_FEATURE_COUNT + 4;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.LaunchTransformationCommandImpl <em>Launch Transformation Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.LaunchTransformationCommandImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getLaunchTransformationCommand()
   * @generated
   */
  int LAUNCH_TRANSFORMATION_COMMAND = 30;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAUNCH_TRANSFORMATION_COMMAND__NAME = SIMPLE_COMMAND__NAME;

  /**
   * The feature id for the '<em><b>Return Value To</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAUNCH_TRANSFORMATION_COMMAND__RETURN_VALUE_TO = SIMPLE_COMMAND__RETURN_VALUE_TO;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAUNCH_TRANSFORMATION_COMMAND__PARAMETERS = SIMPLE_COMMAND__PARAMETERS;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAUNCH_TRANSFORMATION_COMMAND__IDENTIFIER = SIMPLE_COMMAND_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Transformation</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAUNCH_TRANSFORMATION_COMMAND__TRANSFORMATION = SIMPLE_COMMAND_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Inputs</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAUNCH_TRANSFORMATION_COMMAND__INPUTS = SIMPLE_COMMAND_FEATURE_COUNT + 2;

  /**
   * The feature id for the '<em><b>Outputs</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAUNCH_TRANSFORMATION_COMMAND__OUTPUTS = SIMPLE_COMMAND_FEATURE_COUNT + 3;

  /**
   * The feature id for the '<em><b>Output Path</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAUNCH_TRANSFORMATION_COMMAND__OUTPUT_PATH = SIMPLE_COMMAND_FEATURE_COUNT + 4;

  /**
   * The number of structural features of the '<em>Launch Transformation Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LAUNCH_TRANSFORMATION_COMMAND_FEATURE_COUNT = SIMPLE_COMMAND_FEATURE_COUNT + 5;

  /**
   * The meta object id for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.OpenEditorCommandImpl <em>Open Editor Command</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.OpenEditorCommandImpl
   * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getOpenEditorCommand()
   * @generated
   */
  int OPEN_EDITOR_COMMAND = 31;

  /**
   * The feature id for the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPEN_EDITOR_COMMAND__NAME = SIMPLE_COMMAND__NAME;

  /**
   * The feature id for the '<em><b>Return Value To</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPEN_EDITOR_COMMAND__RETURN_VALUE_TO = SIMPLE_COMMAND__RETURN_VALUE_TO;

  /**
   * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPEN_EDITOR_COMMAND__PARAMETERS = SIMPLE_COMMAND__PARAMETERS;

  /**
   * The feature id for the '<em><b>Editor ID</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPEN_EDITOR_COMMAND__EDITOR_ID = SIMPLE_COMMAND_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Opened File</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPEN_EDITOR_COMMAND__OPENED_FILE = SIMPLE_COMMAND_FEATURE_COUNT + 1;

  /**
   * The feature id for the '<em><b>Identifier</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPEN_EDITOR_COMMAND__IDENTIFIER = SIMPLE_COMMAND_FEATURE_COUNT + 2;

  /**
   * The number of structural features of the '<em>Open Editor Command</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int OPEN_EDITOR_COMMAND_FEATURE_COUNT = SIMPLE_COMMAND_FEATURE_COUNT + 3;


  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod <em>GEMDE Method</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>GEMDE Method</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod
   * @generated
   */
  EClass getGEMDEMethod();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getName()
   * @see #getGEMDEMethod()
   * @generated
   */
  EAttribute getGEMDEMethod_Name();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getDescription <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Description</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getDescription()
   * @see #getGEMDEMethod()
   * @generated
   */
  EAttribute getGEMDEMethod_Description();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getProjectName <em>Project Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Project Name</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getProjectName()
   * @see #getGEMDEMethod()
   * @generated
   */
  EAttribute getGEMDEMethod_ProjectName();

  /**
   * Returns the meta object for the containment reference list '{@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getPhases <em>Phases</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Phases</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getPhases()
   * @see #getGEMDEMethod()
   * @generated
   */
  EReference getGEMDEMethod_Phases();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getInitialPhase <em>Initial Phase</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Initial Phase</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getInitialPhase()
   * @see #getGEMDEMethod()
   * @generated
   */
  EReference getGEMDEMethod_InitialPhase();

  /**
   * Returns the meta object for the containment reference list '{@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getVariables <em>Variables</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Variables</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getVariables()
   * @see #getGEMDEMethod()
   * @generated
   */
  EReference getGEMDEMethod_Variables();

  /**
   * Returns the meta object for the containment reference list '{@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getCommands <em>Commands</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Commands</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getCommands()
   * @see #getGEMDEMethod()
   * @generated
   */
  EReference getGEMDEMethod_Commands();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.MethodStep <em>Step</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Step</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodStep
   * @generated
   */
  EClass getMethodStep();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.MethodStep#getTitle <em>Title</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Title</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodStep#getTitle()
   * @see #getMethodStep()
   * @generated
   */
  EAttribute getMethodStep_Title();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.MethodStep#getDescription <em>Description</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Description</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodStep#getDescription()
   * @see #getMethodStep()
   * @generated
   */
  EAttribute getMethodStep_Description();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.MethodPart <em>Part</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Part</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPart
   * @generated
   */
  EClass getMethodPart();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.MethodPart#getIntroduction <em>Introduction</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Introduction</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPart#getIntroduction()
   * @see #getMethodPart()
   * @generated
   */
  EAttribute getMethodPart_Introduction();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.MethodPart#getConclusion <em>Conclusion</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Conclusion</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPart#getConclusion()
   * @see #getMethodPart()
   * @generated
   */
  EAttribute getMethodPart_Conclusion();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.MethodPart#getIsOptional <em>Is Optional</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Is Optional</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPart#getIsOptional()
   * @see #getMethodPart()
   * @generated
   */
  EAttribute getMethodPart_IsOptional();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.ExecutableElement <em>Executable Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Executable Element</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.ExecutableElement
   * @generated
   */
  EClass getExecutableElement();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.ExecutableElement#getCommand <em>Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Command</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.ExecutableElement#getCommand()
   * @see #getExecutableElement()
   * @generated
   */
  EReference getExecutableElement_Command();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.DataStorageElement <em>Data Storage Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Data Storage Element</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.DataStorageElement
   * @generated
   */
  EClass getDataStorageElement();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.MethodVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variable</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodVariable
   * @generated
   */
  EClass getMethodVariable();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.MethodVariable#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodVariable#getIdentifier()
   * @see #getMethodVariable()
   * @generated
   */
  EAttribute getMethodVariable_Identifier();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.MethodVariable#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodVariable#getName()
   * @see #getMethodVariable()
   * @generated
   */
  EAttribute getMethodVariable_Name();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.MethodVariable#getType <em>Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Type</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodVariable#getType()
   * @see #getMethodVariable()
   * @generated
   */
  EAttribute getMethodVariable_Type();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.InternalVariable <em>Internal Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Internal Variable</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.InternalVariable
   * @generated
   */
  EClass getInternalVariable();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.InternalVariable#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.InternalVariable#getName()
   * @see #getInternalVariable()
   * @generated
   */
  EAttribute getInternalVariable_Name();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.Phase <em>Phase</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Phase</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.Phase
   * @generated
   */
  EClass getPhase();

  /**
   * Returns the meta object for the containment reference list '{@link es.esi.gemde.methodmanager.methodmodel.method.Phase#getTasks <em>Tasks</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Tasks</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.Phase#getTasks()
   * @see #getPhase()
   * @generated
   */
  EReference getPhase_Tasks();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.Phase#getInitialTask <em>Initial Task</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Initial Task</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.Phase#getInitialTask()
   * @see #getPhase()
   * @generated
   */
  EReference getPhase_InitialTask();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.Phase#getPrev <em>Prev</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Prev</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.Phase#getPrev()
   * @see #getPhase()
   * @generated
   */
  EReference getPhase_Prev();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.Phase#getNext <em>Next</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Next</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.Phase#getNext()
   * @see #getPhase()
   * @generated
   */
  EReference getPhase_Next();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.Task <em>Task</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Task</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.Task
   * @generated
   */
  EClass getTask();

  /**
   * Returns the meta object for the containment reference list '{@link es.esi.gemde.methodmanager.methodmodel.method.Task#getSubTasks <em>Sub Tasks</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Sub Tasks</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.Task#getSubTasks()
   * @see #getTask()
   * @generated
   */
  EReference getTask_SubTasks();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.Task#getInitialSubtask <em>Initial Subtask</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Initial Subtask</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.Task#getInitialSubtask()
   * @see #getTask()
   * @generated
   */
  EReference getTask_InitialSubtask();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.Task#getPrev <em>Prev</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Prev</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.Task#getPrev()
   * @see #getTask()
   * @generated
   */
  EReference getTask_Prev();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.Task#getNext <em>Next</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Next</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.Task#getNext()
   * @see #getTask()
   * @generated
   */
  EReference getTask_Next();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask <em>Abstract Sub Task</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Abstract Sub Task</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask
   * @generated
   */
  EClass getAbstractSubTask();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask#getPrev <em>Prev</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Prev</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask#getPrev()
   * @see #getAbstractSubTask()
   * @generated
   */
  EReference getAbstractSubTask_Prev();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask#getNext <em>Next</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Next</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask#getNext()
   * @see #getAbstractSubTask()
   * @generated
   */
  EReference getAbstractSubTask_Next();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.SubTask <em>Sub Task</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Sub Task</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SubTask
   * @generated
   */
  EClass getSubTask();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.Condition <em>Condition</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Condition</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.Condition
   * @generated
   */
  EClass getCondition();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.Condition#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.Condition#getValue()
   * @see #getCondition()
   * @generated
   */
  EAttribute getCondition_Value();

  /**
   * Returns the meta object for the container reference '{@link es.esi.gemde.methodmanager.methodmodel.method.Condition#getFrom <em>From</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the container reference '<em>From</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.Condition#getFrom()
   * @see #getCondition()
   * @generated
   */
  EReference getCondition_From();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.Condition#getTo <em>To</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>To</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.Condition#getTo()
   * @see #getCondition()
   * @generated
   */
  EReference getCondition_To();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.ConditionalSubTask <em>Conditional Sub Task</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Conditional Sub Task</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.ConditionalSubTask
   * @generated
   */
  EClass getConditionalSubTask();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.ConditionalSubTask#getVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Variable</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.ConditionalSubTask#getVariable()
   * @see #getConditionalSubTask()
   * @generated
   */
  EReference getConditionalSubTask_Variable();

  /**
   * Returns the meta object for the containment reference list '{@link es.esi.gemde.methodmanager.methodmodel.method.ConditionalSubTask#getConditionalLinks <em>Conditional Links</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Conditional Links</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.ConditionalSubTask#getConditionalLinks()
   * @see #getConditionalSubTask()
   * @generated
   */
  EReference getConditionalSubTask_ConditionalLinks();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.RepeatedSubTask <em>Repeated Sub Task</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Repeated Sub Task</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.RepeatedSubTask
   * @generated
   */
  EClass getRepeatedSubTask();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.RepeatedSubTask#getRepeatedElement <em>Repeated Element</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Repeated Element</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.RepeatedSubTask#getRepeatedElement()
   * @see #getRepeatedSubTask()
   * @generated
   */
  EReference getRepeatedSubTask_RepeatedElement();

  /**
   * Returns the meta object for the attribute list '{@link es.esi.gemde.methodmanager.methodmodel.method.RepeatedSubTask#getRepetitionVector <em>Repetition Vector</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Repetition Vector</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.RepeatedSubTask#getRepetitionVector()
   * @see #getRepeatedSubTask()
   * @generated
   */
  EAttribute getRepeatedSubTask_RepetitionVector();

  /**
   * Returns the meta object for the containment reference '{@link es.esi.gemde.methodmanager.methodmodel.method.RepeatedSubTask#getRepetitionVariable <em>Repetition Variable</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Repetition Variable</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.RepeatedSubTask#getRepetitionVariable()
   * @see #getRepeatedSubTask()
   * @generated
   */
  EReference getRepeatedSubTask_RepetitionVariable();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.CommandParameter <em>Command Parameter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Command Parameter</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.CommandParameter
   * @generated
   */
  EClass getCommandParameter();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.CommandParameter#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.CommandParameter#getIdentifier()
   * @see #getCommandParameter()
   * @generated
   */
  EAttribute getCommandParameter_Identifier();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.VariableCommandParameter <em>Variable Command Parameter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Variable Command Parameter</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.VariableCommandParameter
   * @generated
   */
  EClass getVariableCommandParameter();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.VariableCommandParameter#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Value</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.VariableCommandParameter#getValue()
   * @see #getVariableCommandParameter()
   * @generated
   */
  EReference getVariableCommandParameter_Value();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.ConstantCommandParameter <em>Constant Command Parameter</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Constant Command Parameter</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.ConstantCommandParameter
   * @generated
   */
  EClass getConstantCommandParameter();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.ConstantCommandParameter#getValue <em>Value</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Value</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.ConstantCommandParameter#getValue()
   * @see #getConstantCommandParameter()
   * @generated
   */
  EAttribute getConstantCommandParameter_Value();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.MethodCommand <em>Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Command</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodCommand
   * @generated
   */
  EClass getMethodCommand();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.MethodCommand#getName <em>Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Name</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodCommand#getName()
   * @see #getMethodCommand()
   * @generated
   */
  EAttribute getMethodCommand_Name();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.ComplexCommand <em>Complex Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Complex Command</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.ComplexCommand
   * @generated
   */
  EClass getComplexCommand();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.ComplexCommand#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.ComplexCommand#getIdentifier()
   * @see #getComplexCommand()
   * @generated
   */
  EAttribute getComplexCommand_Identifier();

  /**
   * Returns the meta object for the reference list '{@link es.esi.gemde.methodmanager.methodmodel.method.ComplexCommand#getCommands <em>Commands</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference list '<em>Commands</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.ComplexCommand#getCommands()
   * @see #getComplexCommand()
   * @generated
   */
  EReference getComplexCommand_Commands();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.SimpleCommand <em>Simple Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Simple Command</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SimpleCommand
   * @generated
   */
  EClass getSimpleCommand();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.SimpleCommand#getReturnValueTo <em>Return Value To</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Return Value To</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SimpleCommand#getReturnValueTo()
   * @see #getSimpleCommand()
   * @generated
   */
  EReference getSimpleCommand_ReturnValueTo();

  /**
   * Returns the meta object for the containment reference list '{@link es.esi.gemde.methodmanager.methodmodel.method.SimpleCommand#getParameters <em>Parameters</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Parameters</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SimpleCommand#getParameters()
   * @see #getSimpleCommand()
   * @generated
   */
  EReference getSimpleCommand_Parameters();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.CustomCommand <em>Custom Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Custom Command</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.CustomCommand
   * @generated
   */
  EClass getCustomCommand();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.CustomCommand#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.CustomCommand#getIdentifier()
   * @see #getCustomCommand()
   * @generated
   */
  EAttribute getCustomCommand_Identifier();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.CallWizardCommand <em>Call Wizard Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Call Wizard Command</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.CallWizardCommand
   * @generated
   */
  EClass getCallWizardCommand();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.CallWizardCommand#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.CallWizardCommand#getIdentifier()
   * @see #getCallWizardCommand()
   * @generated
   */
  EAttribute getCallWizardCommand_Identifier();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.CallWizardCommand#getWizard <em>Wizard</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Wizard</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.CallWizardCommand#getWizard()
   * @see #getCallWizardCommand()
   * @generated
   */
  EReference getCallWizardCommand_Wizard();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectFileCommand <em>Select File Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Select File Command</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectFileCommand
   * @generated
   */
  EClass getSelectFileCommand();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectFileCommand#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectFileCommand#getIdentifier()
   * @see #getSelectFileCommand()
   * @generated
   */
  EAttribute getSelectFileCommand_Identifier();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectFileCommand#getOnlyWorkspace <em>Only Workspace</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Only Workspace</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectFileCommand#getOnlyWorkspace()
   * @see #getSelectFileCommand()
   * @generated
   */
  EReference getSelectFileCommand_OnlyWorkspace();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectFolderCommand <em>Select Folder Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Select Folder Command</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectFolderCommand
   * @generated
   */
  EClass getSelectFolderCommand();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectFolderCommand#getOnlyWorkspace <em>Only Workspace</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Only Workspace</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectFolderCommand#getOnlyWorkspace()
   * @see #getSelectFolderCommand()
   * @generated
   */
  EReference getSelectFolderCommand_OnlyWorkspace();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectFolderCommand#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectFolderCommand#getIdentifier()
   * @see #getSelectFolderCommand()
   * @generated
   */
  EAttribute getSelectFolderCommand_Identifier();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectMetamodelCommand <em>Select Metamodel Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Select Metamodel Command</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectMetamodelCommand
   * @generated
   */
  EClass getSelectMetamodelCommand();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectMetamodelCommand#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectMetamodelCommand#getIdentifier()
   * @see #getSelectMetamodelCommand()
   * @generated
   */
  EAttribute getSelectMetamodelCommand_Identifier();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand <em>Select Model Element From File Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Select Model Element From File Command</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand
   * @generated
   */
  EClass getSelectModelElementFromFileCommand();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand#getModelFile <em>Model File</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Model File</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand#getModelFile()
   * @see #getSelectModelElementFromFileCommand()
   * @generated
   */
  EReference getSelectModelElementFromFileCommand_ModelFile();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand#getAddToList <em>Add To List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Add To List</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand#getAddToList()
   * @see #getSelectModelElementFromFileCommand()
   * @generated
   */
  EReference getSelectModelElementFromFileCommand_AddToList();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>List</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand#getList()
   * @see #getSelectModelElementFromFileCommand()
   * @generated
   */
  EReference getSelectModelElementFromFileCommand_List();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand#getIdentifier()
   * @see #getSelectModelElementFromFileCommand()
   * @generated
   */
  EAttribute getSelectModelElementFromFileCommand_Identifier();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand <em>Select Model Element From EObject Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Select Model Element From EObject Command</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand
   * @generated
   */
  EClass getSelectModelElementFromEObjectCommand();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand#getEObject <em>EObject</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>EObject</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand#getEObject()
   * @see #getSelectModelElementFromEObjectCommand()
   * @generated
   */
  EReference getSelectModelElementFromEObjectCommand_EObject();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand#getAddToList <em>Add To List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Add To List</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand#getAddToList()
   * @see #getSelectModelElementFromEObjectCommand()
   * @generated
   */
  EReference getSelectModelElementFromEObjectCommand_AddToList();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand#getList <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>List</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand#getList()
   * @see #getSelectModelElementFromEObjectCommand()
   * @generated
   */
  EReference getSelectModelElementFromEObjectCommand_List();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand#getIdentifier()
   * @see #getSelectModelElementFromEObjectCommand()
   * @generated
   */
  EAttribute getSelectModelElementFromEObjectCommand_Identifier();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectValidationCommand <em>Select Validation Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Select Validation Command</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectValidationCommand
   * @generated
   */
  EClass getSelectValidationCommand();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectValidationCommand#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectValidationCommand#getIdentifier()
   * @see #getSelectValidationCommand()
   * @generated
   */
  EAttribute getSelectValidationCommand_Identifier();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectTransformationCommand <em>Select Transformation Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Select Transformation Command</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectTransformationCommand
   * @generated
   */
  EClass getSelectTransformationCommand();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectTransformationCommand#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectTransformationCommand#getIdentifier()
   * @see #getSelectTransformationCommand()
   * @generated
   */
  EAttribute getSelectTransformationCommand_Identifier();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand <em>Launch Validation Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Launch Validation Command</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand
   * @generated
   */
  EClass getLaunchValidationCommand();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand#getIdentifier()
   * @see #getLaunchValidationCommand()
   * @generated
   */
  EAttribute getLaunchValidationCommand_Identifier();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand#getValidation <em>Validation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Validation</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand#getValidation()
   * @see #getLaunchValidationCommand()
   * @generated
   */
  EReference getLaunchValidationCommand_Validation();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand#getEobject <em>Eobject</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Eobject</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand#getEobject()
   * @see #getLaunchValidationCommand()
   * @generated
   */
  EReference getLaunchValidationCommand_Eobject();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand#getModelFile <em>Model File</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Model File</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand#getModelFile()
   * @see #getLaunchValidationCommand()
   * @generated
   */
  EReference getLaunchValidationCommand_ModelFile();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand <em>Launch Transformation Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Launch Transformation Command</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand
   * @generated
   */
  EClass getLaunchTransformationCommand();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand#getIdentifier()
   * @see #getLaunchTransformationCommand()
   * @generated
   */
  EAttribute getLaunchTransformationCommand_Identifier();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand#getTransformation <em>Transformation</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Transformation</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand#getTransformation()
   * @see #getLaunchTransformationCommand()
   * @generated
   */
  EReference getLaunchTransformationCommand_Transformation();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand#getInputs <em>Inputs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Inputs</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand#getInputs()
   * @see #getLaunchTransformationCommand()
   * @generated
   */
  EReference getLaunchTransformationCommand_Inputs();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand#getOutputs <em>Outputs</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Outputs</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand#getOutputs()
   * @see #getLaunchTransformationCommand()
   * @generated
   */
  EReference getLaunchTransformationCommand_Outputs();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand#getOutputPath <em>Output Path</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Output Path</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand#getOutputPath()
   * @see #getLaunchTransformationCommand()
   * @generated
   */
  EReference getLaunchTransformationCommand_OutputPath();

  /**
   * Returns the meta object for class '{@link es.esi.gemde.methodmanager.methodmodel.method.OpenEditorCommand <em>Open Editor Command</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Open Editor Command</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.OpenEditorCommand
   * @generated
   */
  EClass getOpenEditorCommand();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.OpenEditorCommand#getEditorID <em>Editor ID</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Editor ID</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.OpenEditorCommand#getEditorID()
   * @see #getOpenEditorCommand()
   * @generated
   */
  EReference getOpenEditorCommand_EditorID();

  /**
   * Returns the meta object for the reference '{@link es.esi.gemde.methodmanager.methodmodel.method.OpenEditorCommand#getOpenedFile <em>Opened File</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the reference '<em>Opened File</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.OpenEditorCommand#getOpenedFile()
   * @see #getOpenEditorCommand()
   * @generated
   */
  EReference getOpenEditorCommand_OpenedFile();

  /**
   * Returns the meta object for the attribute '{@link es.esi.gemde.methodmanager.methodmodel.method.OpenEditorCommand#getIdentifier <em>Identifier</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Identifier</em>'.
   * @see es.esi.gemde.methodmanager.methodmodel.method.OpenEditorCommand#getIdentifier()
   * @see #getOpenEditorCommand()
   * @generated
   */
  EAttribute getOpenEditorCommand_Identifier();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  MethodFactory getMethodFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.GEMDEMethodImpl <em>GEMDE Method</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.GEMDEMethodImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getGEMDEMethod()
     * @generated
     */
    EClass GEMDE_METHOD = eINSTANCE.getGEMDEMethod();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GEMDE_METHOD__NAME = eINSTANCE.getGEMDEMethod_Name();

    /**
     * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GEMDE_METHOD__DESCRIPTION = eINSTANCE.getGEMDEMethod_Description();

    /**
     * The meta object literal for the '<em><b>Project Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute GEMDE_METHOD__PROJECT_NAME = eINSTANCE.getGEMDEMethod_ProjectName();

    /**
     * The meta object literal for the '<em><b>Phases</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GEMDE_METHOD__PHASES = eINSTANCE.getGEMDEMethod_Phases();

    /**
     * The meta object literal for the '<em><b>Initial Phase</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GEMDE_METHOD__INITIAL_PHASE = eINSTANCE.getGEMDEMethod_InitialPhase();

    /**
     * The meta object literal for the '<em><b>Variables</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GEMDE_METHOD__VARIABLES = eINSTANCE.getGEMDEMethod_Variables();

    /**
     * The meta object literal for the '<em><b>Commands</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference GEMDE_METHOD__COMMANDS = eINSTANCE.getGEMDEMethod_Commands();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.MethodStepImpl <em>Step</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodStepImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getMethodStep()
     * @generated
     */
    EClass METHOD_STEP = eINSTANCE.getMethodStep();

    /**
     * The meta object literal for the '<em><b>Title</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD_STEP__TITLE = eINSTANCE.getMethodStep_Title();

    /**
     * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD_STEP__DESCRIPTION = eINSTANCE.getMethodStep_Description();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPartImpl <em>Part</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPartImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getMethodPart()
     * @generated
     */
    EClass METHOD_PART = eINSTANCE.getMethodPart();

    /**
     * The meta object literal for the '<em><b>Introduction</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD_PART__INTRODUCTION = eINSTANCE.getMethodPart_Introduction();

    /**
     * The meta object literal for the '<em><b>Conclusion</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD_PART__CONCLUSION = eINSTANCE.getMethodPart_Conclusion();

    /**
     * The meta object literal for the '<em><b>Is Optional</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD_PART__IS_OPTIONAL = eINSTANCE.getMethodPart_IsOptional();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.ExecutableElementImpl <em>Executable Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.ExecutableElementImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getExecutableElement()
     * @generated
     */
    EClass EXECUTABLE_ELEMENT = eINSTANCE.getExecutableElement();

    /**
     * The meta object literal for the '<em><b>Command</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference EXECUTABLE_ELEMENT__COMMAND = eINSTANCE.getExecutableElement_Command();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.DataStorageElementImpl <em>Data Storage Element</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.DataStorageElementImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getDataStorageElement()
     * @generated
     */
    EClass DATA_STORAGE_ELEMENT = eINSTANCE.getDataStorageElement();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.MethodVariableImpl <em>Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodVariableImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getMethodVariable()
     * @generated
     */
    EClass METHOD_VARIABLE = eINSTANCE.getMethodVariable();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD_VARIABLE__IDENTIFIER = eINSTANCE.getMethodVariable_Identifier();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD_VARIABLE__NAME = eINSTANCE.getMethodVariable_Name();

    /**
     * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD_VARIABLE__TYPE = eINSTANCE.getMethodVariable_Type();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.InternalVariableImpl <em>Internal Variable</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.InternalVariableImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getInternalVariable()
     * @generated
     */
    EClass INTERNAL_VARIABLE = eINSTANCE.getInternalVariable();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute INTERNAL_VARIABLE__NAME = eINSTANCE.getInternalVariable_Name();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.PhaseImpl <em>Phase</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.PhaseImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getPhase()
     * @generated
     */
    EClass PHASE = eINSTANCE.getPhase();

    /**
     * The meta object literal for the '<em><b>Tasks</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PHASE__TASKS = eINSTANCE.getPhase_Tasks();

    /**
     * The meta object literal for the '<em><b>Initial Task</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PHASE__INITIAL_TASK = eINSTANCE.getPhase_InitialTask();

    /**
     * The meta object literal for the '<em><b>Prev</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PHASE__PREV = eINSTANCE.getPhase_Prev();

    /**
     * The meta object literal for the '<em><b>Next</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference PHASE__NEXT = eINSTANCE.getPhase_Next();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.TaskImpl <em>Task</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.TaskImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getTask()
     * @generated
     */
    EClass TASK = eINSTANCE.getTask();

    /**
     * The meta object literal for the '<em><b>Sub Tasks</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TASK__SUB_TASKS = eINSTANCE.getTask_SubTasks();

    /**
     * The meta object literal for the '<em><b>Initial Subtask</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TASK__INITIAL_SUBTASK = eINSTANCE.getTask_InitialSubtask();

    /**
     * The meta object literal for the '<em><b>Prev</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TASK__PREV = eINSTANCE.getTask_Prev();

    /**
     * The meta object literal for the '<em><b>Next</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference TASK__NEXT = eINSTANCE.getTask_Next();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.AbstractSubTaskImpl <em>Abstract Sub Task</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.AbstractSubTaskImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getAbstractSubTask()
     * @generated
     */
    EClass ABSTRACT_SUB_TASK = eINSTANCE.getAbstractSubTask();

    /**
     * The meta object literal for the '<em><b>Prev</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ABSTRACT_SUB_TASK__PREV = eINSTANCE.getAbstractSubTask_Prev();

    /**
     * The meta object literal for the '<em><b>Next</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference ABSTRACT_SUB_TASK__NEXT = eINSTANCE.getAbstractSubTask_Next();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SubTaskImpl <em>Sub Task</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.SubTaskImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getSubTask()
     * @generated
     */
    EClass SUB_TASK = eINSTANCE.getSubTask();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.ConditionImpl <em>Condition</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.ConditionImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getCondition()
     * @generated
     */
    EClass CONDITION = eINSTANCE.getCondition();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CONDITION__VALUE = eINSTANCE.getCondition_Value();

    /**
     * The meta object literal for the '<em><b>From</b></em>' container reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONDITION__FROM = eINSTANCE.getCondition_From();

    /**
     * The meta object literal for the '<em><b>To</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONDITION__TO = eINSTANCE.getCondition_To();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.ConditionalSubTaskImpl <em>Conditional Sub Task</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.ConditionalSubTaskImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getConditionalSubTask()
     * @generated
     */
    EClass CONDITIONAL_SUB_TASK = eINSTANCE.getConditionalSubTask();

    /**
     * The meta object literal for the '<em><b>Variable</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONDITIONAL_SUB_TASK__VARIABLE = eINSTANCE.getConditionalSubTask_Variable();

    /**
     * The meta object literal for the '<em><b>Conditional Links</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONDITIONAL_SUB_TASK__CONDITIONAL_LINKS = eINSTANCE.getConditionalSubTask_ConditionalLinks();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.RepeatedSubTaskImpl <em>Repeated Sub Task</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.RepeatedSubTaskImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getRepeatedSubTask()
     * @generated
     */
    EClass REPEATED_SUB_TASK = eINSTANCE.getRepeatedSubTask();

    /**
     * The meta object literal for the '<em><b>Repeated Element</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REPEATED_SUB_TASK__REPEATED_ELEMENT = eINSTANCE.getRepeatedSubTask_RepeatedElement();

    /**
     * The meta object literal for the '<em><b>Repetition Vector</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REPEATED_SUB_TASK__REPETITION_VECTOR = eINSTANCE.getRepeatedSubTask_RepetitionVector();

    /**
     * The meta object literal for the '<em><b>Repetition Variable</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference REPEATED_SUB_TASK__REPETITION_VARIABLE = eINSTANCE.getRepeatedSubTask_RepetitionVariable();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.CommandParameterImpl <em>Command Parameter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.CommandParameterImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getCommandParameter()
     * @generated
     */
    EClass COMMAND_PARAMETER = eINSTANCE.getCommandParameter();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMMAND_PARAMETER__IDENTIFIER = eINSTANCE.getCommandParameter_Identifier();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.VariableCommandParameterImpl <em>Variable Command Parameter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.VariableCommandParameterImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getVariableCommandParameter()
     * @generated
     */
    EClass VARIABLE_COMMAND_PARAMETER = eINSTANCE.getVariableCommandParameter();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference VARIABLE_COMMAND_PARAMETER__VALUE = eINSTANCE.getVariableCommandParameter_Value();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.ConstantCommandParameterImpl <em>Constant Command Parameter</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.ConstantCommandParameterImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getConstantCommandParameter()
     * @generated
     */
    EClass CONSTANT_COMMAND_PARAMETER = eINSTANCE.getConstantCommandParameter();

    /**
     * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CONSTANT_COMMAND_PARAMETER__VALUE = eINSTANCE.getConstantCommandParameter_Value();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.MethodCommandImpl <em>Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodCommandImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getMethodCommand()
     * @generated
     */
    EClass METHOD_COMMAND = eINSTANCE.getMethodCommand();

    /**
     * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute METHOD_COMMAND__NAME = eINSTANCE.getMethodCommand_Name();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.ComplexCommandImpl <em>Complex Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.ComplexCommandImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getComplexCommand()
     * @generated
     */
    EClass COMPLEX_COMMAND = eINSTANCE.getComplexCommand();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute COMPLEX_COMMAND__IDENTIFIER = eINSTANCE.getComplexCommand_Identifier();

    /**
     * The meta object literal for the '<em><b>Commands</b></em>' reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference COMPLEX_COMMAND__COMMANDS = eINSTANCE.getComplexCommand_Commands();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SimpleCommandImpl <em>Simple Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.SimpleCommandImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getSimpleCommand()
     * @generated
     */
    EClass SIMPLE_COMMAND = eINSTANCE.getSimpleCommand();

    /**
     * The meta object literal for the '<em><b>Return Value To</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIMPLE_COMMAND__RETURN_VALUE_TO = eINSTANCE.getSimpleCommand_ReturnValueTo();

    /**
     * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SIMPLE_COMMAND__PARAMETERS = eINSTANCE.getSimpleCommand_Parameters();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.CustomCommandImpl <em>Custom Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.CustomCommandImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getCustomCommand()
     * @generated
     */
    EClass CUSTOM_COMMAND = eINSTANCE.getCustomCommand();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CUSTOM_COMMAND__IDENTIFIER = eINSTANCE.getCustomCommand_Identifier();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.CallWizardCommandImpl <em>Call Wizard Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.CallWizardCommandImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getCallWizardCommand()
     * @generated
     */
    EClass CALL_WIZARD_COMMAND = eINSTANCE.getCallWizardCommand();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CALL_WIZARD_COMMAND__IDENTIFIER = eINSTANCE.getCallWizardCommand_Identifier();

    /**
     * The meta object literal for the '<em><b>Wizard</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CALL_WIZARD_COMMAND__WIZARD = eINSTANCE.getCallWizardCommand_Wizard();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SelectFileCommandImpl <em>Select File Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.SelectFileCommandImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getSelectFileCommand()
     * @generated
     */
    EClass SELECT_FILE_COMMAND = eINSTANCE.getSelectFileCommand();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SELECT_FILE_COMMAND__IDENTIFIER = eINSTANCE.getSelectFileCommand_Identifier();

    /**
     * The meta object literal for the '<em><b>Only Workspace</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SELECT_FILE_COMMAND__ONLY_WORKSPACE = eINSTANCE.getSelectFileCommand_OnlyWorkspace();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SelectFolderCommandImpl <em>Select Folder Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.SelectFolderCommandImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getSelectFolderCommand()
     * @generated
     */
    EClass SELECT_FOLDER_COMMAND = eINSTANCE.getSelectFolderCommand();

    /**
     * The meta object literal for the '<em><b>Only Workspace</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SELECT_FOLDER_COMMAND__ONLY_WORKSPACE = eINSTANCE.getSelectFolderCommand_OnlyWorkspace();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SELECT_FOLDER_COMMAND__IDENTIFIER = eINSTANCE.getSelectFolderCommand_Identifier();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SelectMetamodelCommandImpl <em>Select Metamodel Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.SelectMetamodelCommandImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getSelectMetamodelCommand()
     * @generated
     */
    EClass SELECT_METAMODEL_COMMAND = eINSTANCE.getSelectMetamodelCommand();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SELECT_METAMODEL_COMMAND__IDENTIFIER = eINSTANCE.getSelectMetamodelCommand_Identifier();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SelectModelElementFromFileCommandImpl <em>Select Model Element From File Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.SelectModelElementFromFileCommandImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getSelectModelElementFromFileCommand()
     * @generated
     */
    EClass SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND = eINSTANCE.getSelectModelElementFromFileCommand();

    /**
     * The meta object literal for the '<em><b>Model File</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__MODEL_FILE = eINSTANCE.getSelectModelElementFromFileCommand_ModelFile();

    /**
     * The meta object literal for the '<em><b>Add To List</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__ADD_TO_LIST = eINSTANCE.getSelectModelElementFromFileCommand_AddToList();

    /**
     * The meta object literal for the '<em><b>List</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__LIST = eINSTANCE.getSelectModelElementFromFileCommand_List();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__IDENTIFIER = eINSTANCE.getSelectModelElementFromFileCommand_Identifier();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SelectModelElementFromEObjectCommandImpl <em>Select Model Element From EObject Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.SelectModelElementFromEObjectCommandImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getSelectModelElementFromEObjectCommand()
     * @generated
     */
    EClass SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND = eINSTANCE.getSelectModelElementFromEObjectCommand();

    /**
     * The meta object literal for the '<em><b>EObject</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND__EOBJECT = eINSTANCE.getSelectModelElementFromEObjectCommand_EObject();

    /**
     * The meta object literal for the '<em><b>Add To List</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND__ADD_TO_LIST = eINSTANCE.getSelectModelElementFromEObjectCommand_AddToList();

    /**
     * The meta object literal for the '<em><b>List</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND__LIST = eINSTANCE.getSelectModelElementFromEObjectCommand_List();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND__IDENTIFIER = eINSTANCE.getSelectModelElementFromEObjectCommand_Identifier();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SelectValidationCommandImpl <em>Select Validation Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.SelectValidationCommandImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getSelectValidationCommand()
     * @generated
     */
    EClass SELECT_VALIDATION_COMMAND = eINSTANCE.getSelectValidationCommand();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SELECT_VALIDATION_COMMAND__IDENTIFIER = eINSTANCE.getSelectValidationCommand_Identifier();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SelectTransformationCommandImpl <em>Select Transformation Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.SelectTransformationCommandImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getSelectTransformationCommand()
     * @generated
     */
    EClass SELECT_TRANSFORMATION_COMMAND = eINSTANCE.getSelectTransformationCommand();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SELECT_TRANSFORMATION_COMMAND__IDENTIFIER = eINSTANCE.getSelectTransformationCommand_Identifier();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.LaunchValidationCommandImpl <em>Launch Validation Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.LaunchValidationCommandImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getLaunchValidationCommand()
     * @generated
     */
    EClass LAUNCH_VALIDATION_COMMAND = eINSTANCE.getLaunchValidationCommand();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LAUNCH_VALIDATION_COMMAND__IDENTIFIER = eINSTANCE.getLaunchValidationCommand_Identifier();

    /**
     * The meta object literal for the '<em><b>Validation</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LAUNCH_VALIDATION_COMMAND__VALIDATION = eINSTANCE.getLaunchValidationCommand_Validation();

    /**
     * The meta object literal for the '<em><b>Eobject</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LAUNCH_VALIDATION_COMMAND__EOBJECT = eINSTANCE.getLaunchValidationCommand_Eobject();

    /**
     * The meta object literal for the '<em><b>Model File</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LAUNCH_VALIDATION_COMMAND__MODEL_FILE = eINSTANCE.getLaunchValidationCommand_ModelFile();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.LaunchTransformationCommandImpl <em>Launch Transformation Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.LaunchTransformationCommandImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getLaunchTransformationCommand()
     * @generated
     */
    EClass LAUNCH_TRANSFORMATION_COMMAND = eINSTANCE.getLaunchTransformationCommand();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute LAUNCH_TRANSFORMATION_COMMAND__IDENTIFIER = eINSTANCE.getLaunchTransformationCommand_Identifier();

    /**
     * The meta object literal for the '<em><b>Transformation</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LAUNCH_TRANSFORMATION_COMMAND__TRANSFORMATION = eINSTANCE.getLaunchTransformationCommand_Transformation();

    /**
     * The meta object literal for the '<em><b>Inputs</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LAUNCH_TRANSFORMATION_COMMAND__INPUTS = eINSTANCE.getLaunchTransformationCommand_Inputs();

    /**
     * The meta object literal for the '<em><b>Outputs</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LAUNCH_TRANSFORMATION_COMMAND__OUTPUTS = eINSTANCE.getLaunchTransformationCommand_Outputs();

    /**
     * The meta object literal for the '<em><b>Output Path</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LAUNCH_TRANSFORMATION_COMMAND__OUTPUT_PATH = eINSTANCE.getLaunchTransformationCommand_OutputPath();

    /**
     * The meta object literal for the '{@link es.esi.gemde.methodmanager.methodmodel.method.impl.OpenEditorCommandImpl <em>Open Editor Command</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.OpenEditorCommandImpl
     * @see es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPackageImpl#getOpenEditorCommand()
     * @generated
     */
    EClass OPEN_EDITOR_COMMAND = eINSTANCE.getOpenEditorCommand();

    /**
     * The meta object literal for the '<em><b>Editor ID</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OPEN_EDITOR_COMMAND__EDITOR_ID = eINSTANCE.getOpenEditorCommand_EditorID();

    /**
     * The meta object literal for the '<em><b>Opened File</b></em>' reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference OPEN_EDITOR_COMMAND__OPENED_FILE = eINSTANCE.getOpenEditorCommand_OpenedFile();

    /**
     * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute OPEN_EDITOR_COMMAND__IDENTIFIER = eINSTANCE.getOpenEditorCommand_Identifier();

  }

} //MethodPackage
