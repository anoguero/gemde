/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Complex Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.ComplexCommand#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.ComplexCommand#getCommands <em>Commands</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getComplexCommand()
 * @model annotation="gmf.node figure='rectangle' border.color='102,76,0' color='230,204,128' label='name' tool.name='Add Complex Command'"
 * @generated
 */
public interface ComplexCommand extends MethodCommand
{
  /**
   * Returns the value of the '<em><b>Identifier</b></em>' attribute.
   * The default value is <code>"es.esi.gemde.methodmanager.commands.complexcommand"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Identifier</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Identifier</em>' attribute.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getComplexCommand_Identifier()
   * @model default="es.esi.gemde.methodmanager.commands.complexcommand" required="true" changeable="false"
   * @generated
   */
  String getIdentifier();

  /**
   * Returns the value of the '<em><b>Commands</b></em>' reference list.
   * The list contents are of type {@link es.esi.gemde.methodmanager.methodmodel.method.SimpleCommand}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Commands</em>' reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Commands</em>' reference list.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getComplexCommand_Commands()
   * @model annotation="gmf.link source.decoration='filledrhomb' color='102,76,0' tool.name='Composing Command' tool.small.bundle='es.esi.gemde.methodmanager.methodmodel.edit' tool.small.path='icons/full/obj16/ComposingCommandTool.gif'"
   * @generated
   */
  EList<SimpleCommand> getCommands();

} // ComplexCommand
