/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Call Wizard Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.CallWizardCommand#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.CallWizardCommand#getWizard <em>Wizard</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getCallWizardCommand()
 * @model annotation="gmf.node figure='rectangle' border.color='102,76,0' color='230,204,128' label='name' tool.name='Add Call Wizard Command'"
 * @generated
 */
public interface CallWizardCommand extends SimpleCommand
{
  /**
   * Returns the value of the '<em><b>Identifier</b></em>' attribute.
   * The default value is <code>"es.esi.gemde.methodmanager.commands.callwizardcommand"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Identifier</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Identifier</em>' attribute.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getCallWizardCommand_Identifier()
   * @model default="es.esi.gemde.methodmanager.commands.callwizardcommand" required="true" changeable="false"
   * @generated
   */
  String getIdentifier();

  /**
   * Returns the value of the '<em><b>Wizard</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Wizard</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Wizard</em>' reference.
   * @see #setWizard(CommandParameter)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getCallWizardCommand_Wizard()
   * @model required="true"
   * @generated
   */
  CommandParameter getWizard();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.CallWizardCommand#getWizard <em>Wizard</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Wizard</em>' reference.
   * @see #getWizard()
   * @generated
   */
  void setWizard(CommandParameter value);

} // CallWizardCommand
