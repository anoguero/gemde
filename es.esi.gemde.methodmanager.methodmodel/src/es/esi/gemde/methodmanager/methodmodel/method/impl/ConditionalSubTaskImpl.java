/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.impl;

import es.esi.gemde.methodmanager.methodmodel.method.Condition;
import es.esi.gemde.methodmanager.methodmodel.method.ConditionalSubTask;
import es.esi.gemde.methodmanager.methodmodel.method.MethodPackage;
import es.esi.gemde.methodmanager.methodmodel.method.MethodVariable;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Conditional Sub Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.ConditionalSubTaskImpl#getVariable <em>Variable</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.ConditionalSubTaskImpl#getConditionalLinks <em>Conditional Links</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ConditionalSubTaskImpl extends AbstractSubTaskImpl implements ConditionalSubTask
{
  /**
   * The cached value of the '{@link #getVariable() <em>Variable</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVariable()
   * @generated
   * @ordered
   */
  protected MethodVariable variable;

  /**
   * The cached value of the '{@link #getConditionalLinks() <em>Conditional Links</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConditionalLinks()
   * @generated
   * @ordered
   */
  protected EList<Condition> conditionalLinks;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ConditionalSubTaskImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MethodPackage.Literals.CONDITIONAL_SUB_TASK;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodVariable getVariable()
  {
    if (variable != null && variable.eIsProxy())
    {
      InternalEObject oldVariable = (InternalEObject)variable;
      variable = (MethodVariable)eResolveProxy(oldVariable);
      if (variable != oldVariable)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.CONDITIONAL_SUB_TASK__VARIABLE, oldVariable, variable));
      }
    }
    return variable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodVariable basicGetVariable()
  {
    return variable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVariable(MethodVariable newVariable)
  {
    MethodVariable oldVariable = variable;
    variable = newVariable;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.CONDITIONAL_SUB_TASK__VARIABLE, oldVariable, variable));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Condition> getConditionalLinks()
  {
    if (conditionalLinks == null)
    {
      conditionalLinks = new EObjectContainmentWithInverseEList<Condition>(Condition.class, this, MethodPackage.CONDITIONAL_SUB_TASK__CONDITIONAL_LINKS, MethodPackage.CONDITION__FROM);
    }
    return conditionalLinks;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MethodPackage.CONDITIONAL_SUB_TASK__CONDITIONAL_LINKS:
        return ((InternalEList<InternalEObject>)(InternalEList<?>)getConditionalLinks()).basicAdd(otherEnd, msgs);
    }
    return super.eInverseAdd(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MethodPackage.CONDITIONAL_SUB_TASK__CONDITIONAL_LINKS:
        return ((InternalEList<?>)getConditionalLinks()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MethodPackage.CONDITIONAL_SUB_TASK__VARIABLE:
        if (resolve) return getVariable();
        return basicGetVariable();
      case MethodPackage.CONDITIONAL_SUB_TASK__CONDITIONAL_LINKS:
        return getConditionalLinks();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MethodPackage.CONDITIONAL_SUB_TASK__VARIABLE:
        setVariable((MethodVariable)newValue);
        return;
      case MethodPackage.CONDITIONAL_SUB_TASK__CONDITIONAL_LINKS:
        getConditionalLinks().clear();
        getConditionalLinks().addAll((Collection<? extends Condition>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.CONDITIONAL_SUB_TASK__VARIABLE:
        setVariable((MethodVariable)null);
        return;
      case MethodPackage.CONDITIONAL_SUB_TASK__CONDITIONAL_LINKS:
        getConditionalLinks().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.CONDITIONAL_SUB_TASK__VARIABLE:
        return variable != null;
      case MethodPackage.CONDITIONAL_SUB_TASK__CONDITIONAL_LINKS:
        return conditionalLinks != null && !conditionalLinks.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ConditionalSubTaskImpl
