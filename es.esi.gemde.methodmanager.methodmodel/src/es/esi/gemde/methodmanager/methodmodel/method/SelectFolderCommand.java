/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Select Folder Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.SelectFolderCommand#getOnlyWorkspace <em>Only Workspace</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.SelectFolderCommand#getIdentifier <em>Identifier</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSelectFolderCommand()
 * @model annotation="gmf.node figure='rectangle' border.color='102,76,0' color='230,204,128' label='name' tool.name='Add Select Folder Command'"
 * @generated
 */
public interface SelectFolderCommand extends SimpleCommand
{
  /**
   * Returns the value of the '<em><b>Only Workspace</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Only Workspace</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Only Workspace</em>' reference.
   * @see #setOnlyWorkspace(CommandParameter)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSelectFolderCommand_OnlyWorkspace()
   * @model required="true"
   * @generated
   */
  CommandParameter getOnlyWorkspace();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectFolderCommand#getOnlyWorkspace <em>Only Workspace</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Only Workspace</em>' reference.
   * @see #getOnlyWorkspace()
   * @generated
   */
  void setOnlyWorkspace(CommandParameter value);

  /**
   * Returns the value of the '<em><b>Identifier</b></em>' attribute.
   * The default value is <code>"es.esi.gemde.methodmanager.commands.selectfoldercommand"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Identifier</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Identifier</em>' attribute.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSelectFolderCommand_Identifier()
   * @model default="es.esi.gemde.methodmanager.commands.selectfoldercommand" required="true" changeable="false"
   * @generated
   */
  String getIdentifier();

} // SelectFolderCommand
