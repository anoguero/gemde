/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Part</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.MethodPart#getIntroduction <em>Introduction</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.MethodPart#getConclusion <em>Conclusion</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.MethodPart#getIsOptional <em>Is Optional</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getMethodPart()
 * @model abstract="true"
 * @generated
 */
public interface MethodPart extends MethodStep
{
  /**
   * Returns the value of the '<em><b>Introduction</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Introduction</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Introduction</em>' attribute.
   * @see #setIntroduction(String)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getMethodPart_Introduction()
   * @model
   * @generated
   */
  String getIntroduction();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.MethodPart#getIntroduction <em>Introduction</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Introduction</em>' attribute.
   * @see #getIntroduction()
   * @generated
   */
  void setIntroduction(String value);

  /**
   * Returns the value of the '<em><b>Conclusion</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Conclusion</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Conclusion</em>' attribute.
   * @see #setConclusion(String)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getMethodPart_Conclusion()
   * @model
   * @generated
   */
  String getConclusion();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.MethodPart#getConclusion <em>Conclusion</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Conclusion</em>' attribute.
   * @see #getConclusion()
   * @generated
   */
  void setConclusion(String value);

  /**
   * Returns the value of the '<em><b>Is Optional</b></em>' attribute.
   * The default value is <code>"false"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Is Optional</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Is Optional</em>' attribute.
   * @see #setIsOptional(Boolean)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getMethodPart_IsOptional()
   * @model default="false"
   * @generated
   */
  Boolean getIsOptional();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.MethodPart#getIsOptional <em>Is Optional</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Is Optional</em>' attribute.
   * @see #getIsOptional()
   * @generated
   */
  void setIsOptional(Boolean value);

} // MethodPart
