/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.Condition#getValue <em>Value</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.Condition#getFrom <em>From</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.Condition#getTo <em>To</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getCondition()
 * @model annotation="gmf.link source='from' target='to' label='value' source.decoration='filledrhomb' color='122,163,204' tool.name='Conditional SubTask' tool.small.bundle='es.esi.gemde.methodmanager.methodmodel.edit' tool.small.path='icons/full/obj16/ConditionLinkTool.gif'"
 * @generated
 */
public interface Condition extends EObject
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' attribute.
   * @see #setValue(String)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getCondition_Value()
   * @model required="true"
   * @generated
   */
  String getValue();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.Condition#getValue <em>Value</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' attribute.
   * @see #getValue()
   * @generated
   */
  void setValue(String value);

  /**
   * Returns the value of the '<em><b>From</b></em>' container reference.
   * It is bidirectional and its opposite is '{@link es.esi.gemde.methodmanager.methodmodel.method.ConditionalSubTask#getConditionalLinks <em>Conditional Links</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>From</em>' container reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>From</em>' container reference.
   * @see #setFrom(ConditionalSubTask)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getCondition_From()
   * @see es.esi.gemde.methodmanager.methodmodel.method.ConditionalSubTask#getConditionalLinks
   * @model opposite="conditionalLinks" required="true" transient="false"
   * @generated
   */
  ConditionalSubTask getFrom();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.Condition#getFrom <em>From</em>}' container reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>From</em>' container reference.
   * @see #getFrom()
   * @generated
   */
  void setFrom(ConditionalSubTask value);

  /**
   * Returns the value of the '<em><b>To</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>To</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>To</em>' reference.
   * @see #setTo(SubTask)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getCondition_To()
   * @model required="true"
   * @generated
   */
  SubTask getTo();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.Condition#getTo <em>To</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>To</em>' reference.
   * @see #getTo()
   * @generated
   */
  void setTo(SubTask value);

} // Condition
