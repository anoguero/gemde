/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable Command Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.VariableCommandParameter#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getVariableCommandParameter()
 * @model annotation="gmf.node figure='rectangle' border.color='230,204,128' color='230,204,128' label='identifier' tool.name='Add Variable Command Parameter'"
 * @generated
 */
public interface VariableCommandParameter extends CommandParameter
{
  /**
   * Returns the value of the '<em><b>Value</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Value</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Value</em>' reference.
   * @see #setValue(DataStorageElement)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getVariableCommandParameter_Value()
   * @model required="true"
   * @generated
   */
  DataStorageElement getValue();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.VariableCommandParameter#getValue <em>Value</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Value</em>' reference.
   * @see #getValue()
   * @generated
   */
  void setValue(DataStorageElement value);

} // VariableCommandParameter
