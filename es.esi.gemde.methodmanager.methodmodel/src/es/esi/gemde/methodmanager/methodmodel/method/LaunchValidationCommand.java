/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Launch Validation Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand#getValidation <em>Validation</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand#getEobject <em>Eobject</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand#getModelFile <em>Model File</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getLaunchValidationCommand()
 * @model annotation="gmf.node figure='rectangle' border.color='102,76,0' color='230,204,128' label='name' tool.name='Add Launch Validation Command'"
 * @generated
 */
public interface LaunchValidationCommand extends SimpleCommand
{
  /**
   * Returns the value of the '<em><b>Identifier</b></em>' attribute.
   * The default value is <code>"es.esi.gemde.methodmanager.commands.launchvalidationcommand"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Identifier</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Identifier</em>' attribute.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getLaunchValidationCommand_Identifier()
   * @model default="es.esi.gemde.methodmanager.commands.launchvalidationcommand" required="true" changeable="false"
   * @generated
   */
  String getIdentifier();

  /**
   * Returns the value of the '<em><b>Validation</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Validation</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Validation</em>' reference.
   * @see #setValidation(CommandParameter)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getLaunchValidationCommand_Validation()
   * @model required="true"
   * @generated
   */
  CommandParameter getValidation();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand#getValidation <em>Validation</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Validation</em>' reference.
   * @see #getValidation()
   * @generated
   */
  void setValidation(CommandParameter value);

  /**
   * Returns the value of the '<em><b>Eobject</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Eobject</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Eobject</em>' reference.
   * @see #setEobject(CommandParameter)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getLaunchValidationCommand_Eobject()
   * @model
   * @generated
   */
  CommandParameter getEobject();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand#getEobject <em>Eobject</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Eobject</em>' reference.
   * @see #getEobject()
   * @generated
   */
  void setEobject(CommandParameter value);

  /**
   * Returns the value of the '<em><b>Model File</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Model File</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Model File</em>' reference.
   * @see #setModelFile(CommandParameter)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getLaunchValidationCommand_ModelFile()
   * @model
   * @generated
   */
  CommandParameter getModelFile();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand#getModelFile <em>Model File</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Model File</em>' reference.
   * @see #getModelFile()
   * @generated
   */
  void setModelFile(CommandParameter value);

} // LaunchValidationCommand
