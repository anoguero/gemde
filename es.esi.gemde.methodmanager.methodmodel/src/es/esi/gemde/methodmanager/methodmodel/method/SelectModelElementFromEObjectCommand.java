/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Select Model Element From EObject Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand#getEObject <em>EObject</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand#getAddToList <em>Add To List</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand#getList <em>List</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand#getIdentifier <em>Identifier</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSelectModelElementFromEObjectCommand()
 * @model annotation="gmf.node figure='rectangle' border.color='102,76,0' color='230,204,128' label='name' tool.name='Add Select EObject from Object Command'"
 * @generated
 */
public interface SelectModelElementFromEObjectCommand extends SimpleCommand
{
  /**
   * Returns the value of the '<em><b>EObject</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>EObject</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>EObject</em>' reference.
   * @see #setEObject(CommandParameter)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSelectModelElementFromEObjectCommand_EObject()
   * @model required="true"
   * @generated
   */
  CommandParameter getEObject();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand#getEObject <em>EObject</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>EObject</em>' reference.
   * @see #getEObject()
   * @generated
   */
  void setEObject(CommandParameter value);

  /**
   * Returns the value of the '<em><b>Add To List</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Add To List</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Add To List</em>' reference.
   * @see #setAddToList(CommandParameter)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSelectModelElementFromEObjectCommand_AddToList()
   * @model
   * @generated
   */
  CommandParameter getAddToList();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand#getAddToList <em>Add To List</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Add To List</em>' reference.
   * @see #getAddToList()
   * @generated
   */
  void setAddToList(CommandParameter value);

  /**
   * Returns the value of the '<em><b>List</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>List</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>List</em>' reference.
   * @see #setList(CommandParameter)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSelectModelElementFromEObjectCommand_List()
   * @model
   * @generated
   */
  CommandParameter getList();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand#getList <em>List</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>List</em>' reference.
   * @see #getList()
   * @generated
   */
  void setList(CommandParameter value);

  /**
   * Returns the value of the '<em><b>Identifier</b></em>' attribute.
   * The default value is <code>"es.esi.gemde.methodmanager.commands.selectelemfromeobjectcommand"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Identifier</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Identifier</em>' attribute.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSelectModelElementFromEObjectCommand_Identifier()
   * @model default="es.esi.gemde.methodmanager.commands.selectelemfromeobjectcommand" required="true" changeable="false"
   * @generated
   */
  String getIdentifier();

} // SelectModelElementFromEObjectCommand
