/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Phase</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.Phase#getTasks <em>Tasks</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.Phase#getInitialTask <em>Initial Task</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.Phase#getPrev <em>Prev</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.Phase#getNext <em>Next</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getPhase()
 * @model annotation="gmf.node figure='rounded' size='500,300' border.color='0,0,50' color='153,153,194' label='title' tool.name='Add Method Phase'"
 * @generated
 */
public interface Phase extends MethodPart
{
  /**
   * Returns the value of the '<em><b>Tasks</b></em>' containment reference list.
   * The list contents are of type {@link es.esi.gemde.methodmanager.methodmodel.method.Task}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Tasks</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Tasks</em>' containment reference list.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getPhase_Tasks()
   * @model containment="true" required="true"
   *        annotation="gmf.compartment collapsible='false' layout='free'"
   * @generated
   */
  EList<Task> getTasks();

  /**
   * Returns the value of the '<em><b>Initial Task</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Initial Task</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Initial Task</em>' reference.
   * @see #setInitialTask(Task)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getPhase_InitialTask()
   * @model required="true"
   * @generated
   */
  Task getInitialTask();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.Phase#getInitialTask <em>Initial Task</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Initial Task</em>' reference.
   * @see #getInitialTask()
   * @generated
   */
  void setInitialTask(Task value);

  /**
   * Returns the value of the '<em><b>Prev</b></em>' reference.
   * It is bidirectional and its opposite is '{@link es.esi.gemde.methodmanager.methodmodel.method.Phase#getNext <em>Next</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Prev</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Prev</em>' reference.
   * @see #setPrev(Phase)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getPhase_Prev()
   * @see es.esi.gemde.methodmanager.methodmodel.method.Phase#getNext
   * @model opposite="next"
   * @generated
   */
  Phase getPrev();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.Phase#getPrev <em>Prev</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Prev</em>' reference.
   * @see #getPrev()
   * @generated
   */
  void setPrev(Phase value);

  /**
   * Returns the value of the '<em><b>Next</b></em>' reference.
   * It is bidirectional and its opposite is '{@link es.esi.gemde.methodmanager.methodmodel.method.Phase#getPrev <em>Prev</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Next</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Next</em>' reference.
   * @see #setNext(Phase)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getPhase_Next()
   * @see es.esi.gemde.methodmanager.methodmodel.method.Phase#getPrev
   * @model opposite="prev"
   *        annotation="gmf.link target.decoration='arrow' color='0,0,50' tool.name='Next Phase' tool.small.bundle='es.esi.gemde.methodmanager.methodmodel.edit' tool.small.path='icons/full/obj16/NextPhaseTool.gif'"
   * @generated
   */
  Phase getNext();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.Phase#getNext <em>Next</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Next</em>' reference.
   * @see #getNext()
   * @generated
   */
  void setNext(Phase value);

} // Phase
