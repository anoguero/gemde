/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Executable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.ExecutableElement#getCommand <em>Command</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getExecutableElement()
 * @model abstract="true"
 * @generated
 */
public interface ExecutableElement extends EObject
{
  /**
   * Returns the value of the '<em><b>Command</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Command</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Command</em>' reference.
   * @see #setCommand(MethodCommand)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getExecutableElement_Command()
   * @model
   * @generated
   */
  MethodCommand getCommand();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.ExecutableElement#getCommand <em>Command</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Command</em>' reference.
   * @see #getCommand()
   * @generated
   */
  void setCommand(MethodCommand value);

} // ExecutableElement
