/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sub Task</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSubTask()
 * @model annotation="gmf.node figure='rounded' size='100,50' border.color='122,163,204' color='204,230,255' label='title' tool.name='Add SubTask'"
 * @generated
 */
public interface SubTask extends AbstractSubTask, ExecutableElement
{
} // SubTask
