/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.impl;

import es.esi.gemde.methodmanager.methodmodel.method.CommandParameter;
import es.esi.gemde.methodmanager.methodmodel.method.MethodPackage;
import es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Select Model Element From File Command</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SelectModelElementFromFileCommandImpl#getModelFile <em>Model File</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SelectModelElementFromFileCommandImpl#getAddToList <em>Add To List</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SelectModelElementFromFileCommandImpl#getList <em>List</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SelectModelElementFromFileCommandImpl#getIdentifier <em>Identifier</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SelectModelElementFromFileCommandImpl extends SimpleCommandImpl implements SelectModelElementFromFileCommand
{
  /**
   * The cached value of the '{@link #getModelFile() <em>Model File</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModelFile()
   * @generated
   * @ordered
   */
  protected CommandParameter modelFile;

  /**
   * The cached value of the '{@link #getAddToList() <em>Add To List</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAddToList()
   * @generated
   * @ordered
   */
  protected CommandParameter addToList;

  /**
   * The cached value of the '{@link #getList() <em>List</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getList()
   * @generated
   * @ordered
   */
  protected CommandParameter list;

  /**
   * The default value of the '{@link #getIdentifier() <em>Identifier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdentifier()
   * @generated
   * @ordered
   */
  protected static final String IDENTIFIER_EDEFAULT = "es.esi.gemde.methodmanager.commands.selectelemfromfilecommand";

  /**
   * The cached value of the '{@link #getIdentifier() <em>Identifier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdentifier()
   * @generated
   * @ordered
   */
  protected String identifier = IDENTIFIER_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SelectModelElementFromFileCommandImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MethodPackage.Literals.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter getModelFile()
  {
    if (modelFile != null && modelFile.eIsProxy())
    {
      InternalEObject oldModelFile = (InternalEObject)modelFile;
      modelFile = (CommandParameter)eResolveProxy(oldModelFile);
      if (modelFile != oldModelFile)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__MODEL_FILE, oldModelFile, modelFile));
      }
    }
    return modelFile;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter basicGetModelFile()
  {
    return modelFile;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setModelFile(CommandParameter newModelFile)
  {
    CommandParameter oldModelFile = modelFile;
    modelFile = newModelFile;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__MODEL_FILE, oldModelFile, modelFile));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter getAddToList()
  {
    if (addToList != null && addToList.eIsProxy())
    {
      InternalEObject oldAddToList = (InternalEObject)addToList;
      addToList = (CommandParameter)eResolveProxy(oldAddToList);
      if (addToList != oldAddToList)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__ADD_TO_LIST, oldAddToList, addToList));
      }
    }
    return addToList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter basicGetAddToList()
  {
    return addToList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAddToList(CommandParameter newAddToList)
  {
    CommandParameter oldAddToList = addToList;
    addToList = newAddToList;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__ADD_TO_LIST, oldAddToList, addToList));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter getList()
  {
    if (list != null && list.eIsProxy())
    {
      InternalEObject oldList = (InternalEObject)list;
      list = (CommandParameter)eResolveProxy(oldList);
      if (list != oldList)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__LIST, oldList, list));
      }
    }
    return list;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter basicGetList()
  {
    return list;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setList(CommandParameter newList)
  {
    CommandParameter oldList = list;
    list = newList;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__LIST, oldList, list));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getIdentifier()
  {
    return identifier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__MODEL_FILE:
        if (resolve) return getModelFile();
        return basicGetModelFile();
      case MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__ADD_TO_LIST:
        if (resolve) return getAddToList();
        return basicGetAddToList();
      case MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__LIST:
        if (resolve) return getList();
        return basicGetList();
      case MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__IDENTIFIER:
        return getIdentifier();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__MODEL_FILE:
        setModelFile((CommandParameter)newValue);
        return;
      case MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__ADD_TO_LIST:
        setAddToList((CommandParameter)newValue);
        return;
      case MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__LIST:
        setList((CommandParameter)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__MODEL_FILE:
        setModelFile((CommandParameter)null);
        return;
      case MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__ADD_TO_LIST:
        setAddToList((CommandParameter)null);
        return;
      case MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__LIST:
        setList((CommandParameter)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__MODEL_FILE:
        return modelFile != null;
      case MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__ADD_TO_LIST:
        return addToList != null;
      case MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__LIST:
        return list != null;
      case MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__IDENTIFIER:
        return IDENTIFIER_EDEFAULT == null ? identifier != null : !IDENTIFIER_EDEFAULT.equals(identifier);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (identifier: ");
    result.append(identifier);
    result.append(')');
    return result.toString();
  }

} //SelectModelElementFromFileCommandImpl
