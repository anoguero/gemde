/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.impl;

import es.esi.gemde.methodmanager.methodmodel.method.CommandParameter;
import es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand;
import es.esi.gemde.methodmanager.methodmodel.method.MethodPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Launch Transformation Command</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.LaunchTransformationCommandImpl#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.LaunchTransformationCommandImpl#getTransformation <em>Transformation</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.LaunchTransformationCommandImpl#getInputs <em>Inputs</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.LaunchTransformationCommandImpl#getOutputs <em>Outputs</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.LaunchTransformationCommandImpl#getOutputPath <em>Output Path</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LaunchTransformationCommandImpl extends SimpleCommandImpl implements LaunchTransformationCommand
{
  /**
   * The default value of the '{@link #getIdentifier() <em>Identifier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdentifier()
   * @generated
   * @ordered
   */
  protected static final String IDENTIFIER_EDEFAULT = "es.esi.gemde.methodmanager.commands.launchtransformationcommand";

  /**
   * The cached value of the '{@link #getIdentifier() <em>Identifier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdentifier()
   * @generated
   * @ordered
   */
  protected String identifier = IDENTIFIER_EDEFAULT;

  /**
   * The cached value of the '{@link #getTransformation() <em>Transformation</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTransformation()
   * @generated
   * @ordered
   */
  protected CommandParameter transformation;

  /**
   * The cached value of the '{@link #getInputs() <em>Inputs</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInputs()
   * @generated
   * @ordered
   */
  protected CommandParameter inputs;

  /**
   * The cached value of the '{@link #getOutputs() <em>Outputs</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOutputs()
   * @generated
   * @ordered
   */
  protected CommandParameter outputs;

  /**
   * The cached value of the '{@link #getOutputPath() <em>Output Path</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOutputPath()
   * @generated
   * @ordered
   */
  protected CommandParameter outputPath;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected LaunchTransformationCommandImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MethodPackage.Literals.LAUNCH_TRANSFORMATION_COMMAND;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getIdentifier()
  {
    return identifier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter getTransformation()
  {
    if (transformation != null && transformation.eIsProxy())
    {
      InternalEObject oldTransformation = (InternalEObject)transformation;
      transformation = (CommandParameter)eResolveProxy(oldTransformation);
      if (transformation != oldTransformation)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__TRANSFORMATION, oldTransformation, transformation));
      }
    }
    return transformation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter basicGetTransformation()
  {
    return transformation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setTransformation(CommandParameter newTransformation)
  {
    CommandParameter oldTransformation = transformation;
    transformation = newTransformation;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__TRANSFORMATION, oldTransformation, transformation));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter getInputs()
  {
    if (inputs != null && inputs.eIsProxy())
    {
      InternalEObject oldInputs = (InternalEObject)inputs;
      inputs = (CommandParameter)eResolveProxy(oldInputs);
      if (inputs != oldInputs)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__INPUTS, oldInputs, inputs));
      }
    }
    return inputs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter basicGetInputs()
  {
    return inputs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInputs(CommandParameter newInputs)
  {
    CommandParameter oldInputs = inputs;
    inputs = newInputs;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__INPUTS, oldInputs, inputs));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter getOutputs()
  {
    if (outputs != null && outputs.eIsProxy())
    {
      InternalEObject oldOutputs = (InternalEObject)outputs;
      outputs = (CommandParameter)eResolveProxy(oldOutputs);
      if (outputs != oldOutputs)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__OUTPUTS, oldOutputs, outputs));
      }
    }
    return outputs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter basicGetOutputs()
  {
    return outputs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOutputs(CommandParameter newOutputs)
  {
    CommandParameter oldOutputs = outputs;
    outputs = newOutputs;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__OUTPUTS, oldOutputs, outputs));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter getOutputPath()
  {
    if (outputPath != null && outputPath.eIsProxy())
    {
      InternalEObject oldOutputPath = (InternalEObject)outputPath;
      outputPath = (CommandParameter)eResolveProxy(oldOutputPath);
      if (outputPath != oldOutputPath)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__OUTPUT_PATH, oldOutputPath, outputPath));
      }
    }
    return outputPath;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter basicGetOutputPath()
  {
    return outputPath;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setOutputPath(CommandParameter newOutputPath)
  {
    CommandParameter oldOutputPath = outputPath;
    outputPath = newOutputPath;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__OUTPUT_PATH, oldOutputPath, outputPath));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__IDENTIFIER:
        return getIdentifier();
      case MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__TRANSFORMATION:
        if (resolve) return getTransformation();
        return basicGetTransformation();
      case MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__INPUTS:
        if (resolve) return getInputs();
        return basicGetInputs();
      case MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__OUTPUTS:
        if (resolve) return getOutputs();
        return basicGetOutputs();
      case MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__OUTPUT_PATH:
        if (resolve) return getOutputPath();
        return basicGetOutputPath();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__TRANSFORMATION:
        setTransformation((CommandParameter)newValue);
        return;
      case MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__INPUTS:
        setInputs((CommandParameter)newValue);
        return;
      case MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__OUTPUTS:
        setOutputs((CommandParameter)newValue);
        return;
      case MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__OUTPUT_PATH:
        setOutputPath((CommandParameter)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__TRANSFORMATION:
        setTransformation((CommandParameter)null);
        return;
      case MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__INPUTS:
        setInputs((CommandParameter)null);
        return;
      case MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__OUTPUTS:
        setOutputs((CommandParameter)null);
        return;
      case MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__OUTPUT_PATH:
        setOutputPath((CommandParameter)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__IDENTIFIER:
        return IDENTIFIER_EDEFAULT == null ? identifier != null : !IDENTIFIER_EDEFAULT.equals(identifier);
      case MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__TRANSFORMATION:
        return transformation != null;
      case MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__INPUTS:
        return inputs != null;
      case MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__OUTPUTS:
        return outputs != null;
      case MethodPackage.LAUNCH_TRANSFORMATION_COMMAND__OUTPUT_PATH:
        return outputPath != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (identifier: ");
    result.append(identifier);
    result.append(')');
    return result.toString();
  }

} //LaunchTransformationCommandImpl
