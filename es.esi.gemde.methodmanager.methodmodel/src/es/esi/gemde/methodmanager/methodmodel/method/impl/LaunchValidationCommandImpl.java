/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.impl;

import es.esi.gemde.methodmanager.methodmodel.method.CommandParameter;
import es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand;
import es.esi.gemde.methodmanager.methodmodel.method.MethodPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Launch Validation Command</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.LaunchValidationCommandImpl#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.LaunchValidationCommandImpl#getValidation <em>Validation</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.LaunchValidationCommandImpl#getEobject <em>Eobject</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.LaunchValidationCommandImpl#getModelFile <em>Model File</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LaunchValidationCommandImpl extends SimpleCommandImpl implements LaunchValidationCommand
{
  /**
   * The default value of the '{@link #getIdentifier() <em>Identifier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdentifier()
   * @generated
   * @ordered
   */
  protected static final String IDENTIFIER_EDEFAULT = "es.esi.gemde.methodmanager.commands.launchvalidationcommand";

  /**
   * The cached value of the '{@link #getIdentifier() <em>Identifier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdentifier()
   * @generated
   * @ordered
   */
  protected String identifier = IDENTIFIER_EDEFAULT;

  /**
   * The cached value of the '{@link #getValidation() <em>Validation</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getValidation()
   * @generated
   * @ordered
   */
  protected CommandParameter validation;

  /**
   * The cached value of the '{@link #getEobject() <em>Eobject</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEobject()
   * @generated
   * @ordered
   */
  protected CommandParameter eobject;

  /**
   * The cached value of the '{@link #getModelFile() <em>Model File</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getModelFile()
   * @generated
   * @ordered
   */
  protected CommandParameter modelFile;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected LaunchValidationCommandImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MethodPackage.Literals.LAUNCH_VALIDATION_COMMAND;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getIdentifier()
  {
    return identifier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter getValidation()
  {
    if (validation != null && validation.eIsProxy())
    {
      InternalEObject oldValidation = (InternalEObject)validation;
      validation = (CommandParameter)eResolveProxy(oldValidation);
      if (validation != oldValidation)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.LAUNCH_VALIDATION_COMMAND__VALIDATION, oldValidation, validation));
      }
    }
    return validation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter basicGetValidation()
  {
    return validation;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setValidation(CommandParameter newValidation)
  {
    CommandParameter oldValidation = validation;
    validation = newValidation;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.LAUNCH_VALIDATION_COMMAND__VALIDATION, oldValidation, validation));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter getEobject()
  {
    if (eobject != null && eobject.eIsProxy())
    {
      InternalEObject oldEobject = (InternalEObject)eobject;
      eobject = (CommandParameter)eResolveProxy(oldEobject);
      if (eobject != oldEobject)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.LAUNCH_VALIDATION_COMMAND__EOBJECT, oldEobject, eobject));
      }
    }
    return eobject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter basicGetEobject()
  {
    return eobject;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEobject(CommandParameter newEobject)
  {
    CommandParameter oldEobject = eobject;
    eobject = newEobject;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.LAUNCH_VALIDATION_COMMAND__EOBJECT, oldEobject, eobject));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter getModelFile()
  {
    if (modelFile != null && modelFile.eIsProxy())
    {
      InternalEObject oldModelFile = (InternalEObject)modelFile;
      modelFile = (CommandParameter)eResolveProxy(oldModelFile);
      if (modelFile != oldModelFile)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.LAUNCH_VALIDATION_COMMAND__MODEL_FILE, oldModelFile, modelFile));
      }
    }
    return modelFile;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter basicGetModelFile()
  {
    return modelFile;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setModelFile(CommandParameter newModelFile)
  {
    CommandParameter oldModelFile = modelFile;
    modelFile = newModelFile;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.LAUNCH_VALIDATION_COMMAND__MODEL_FILE, oldModelFile, modelFile));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MethodPackage.LAUNCH_VALIDATION_COMMAND__IDENTIFIER:
        return getIdentifier();
      case MethodPackage.LAUNCH_VALIDATION_COMMAND__VALIDATION:
        if (resolve) return getValidation();
        return basicGetValidation();
      case MethodPackage.LAUNCH_VALIDATION_COMMAND__EOBJECT:
        if (resolve) return getEobject();
        return basicGetEobject();
      case MethodPackage.LAUNCH_VALIDATION_COMMAND__MODEL_FILE:
        if (resolve) return getModelFile();
        return basicGetModelFile();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MethodPackage.LAUNCH_VALIDATION_COMMAND__VALIDATION:
        setValidation((CommandParameter)newValue);
        return;
      case MethodPackage.LAUNCH_VALIDATION_COMMAND__EOBJECT:
        setEobject((CommandParameter)newValue);
        return;
      case MethodPackage.LAUNCH_VALIDATION_COMMAND__MODEL_FILE:
        setModelFile((CommandParameter)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.LAUNCH_VALIDATION_COMMAND__VALIDATION:
        setValidation((CommandParameter)null);
        return;
      case MethodPackage.LAUNCH_VALIDATION_COMMAND__EOBJECT:
        setEobject((CommandParameter)null);
        return;
      case MethodPackage.LAUNCH_VALIDATION_COMMAND__MODEL_FILE:
        setModelFile((CommandParameter)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.LAUNCH_VALIDATION_COMMAND__IDENTIFIER:
        return IDENTIFIER_EDEFAULT == null ? identifier != null : !IDENTIFIER_EDEFAULT.equals(identifier);
      case MethodPackage.LAUNCH_VALIDATION_COMMAND__VALIDATION:
        return validation != null;
      case MethodPackage.LAUNCH_VALIDATION_COMMAND__EOBJECT:
        return eobject != null;
      case MethodPackage.LAUNCH_VALIDATION_COMMAND__MODEL_FILE:
        return modelFile != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (identifier: ");
    result.append(identifier);
    result.append(')');
    return result.toString();
  }

} //LaunchValidationCommandImpl
