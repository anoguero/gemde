/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.util;

import es.esi.gemde.methodmanager.methodmodel.method.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage
 * @generated
 */
public class MethodSwitch<T> extends Switch<T>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static MethodPackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodSwitch()
  {
    if (modelPackage == null)
    {
      modelPackage = MethodPackage.eINSTANCE;
    }
  }

  /**
   * Checks whether this is a switch for the given package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @parameter ePackage the package in question.
   * @return whether this is a switch for the given package.
   * @generated
   */
  @Override
  protected boolean isSwitchFor(EPackage ePackage)
  {
    return ePackage == modelPackage;
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @Override
  protected T doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case MethodPackage.GEMDE_METHOD:
      {
        GEMDEMethod gemdeMethod = (GEMDEMethod)theEObject;
        T result = caseGEMDEMethod(gemdeMethod);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.METHOD_STEP:
      {
        MethodStep methodStep = (MethodStep)theEObject;
        T result = caseMethodStep(methodStep);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.METHOD_PART:
      {
        MethodPart methodPart = (MethodPart)theEObject;
        T result = caseMethodPart(methodPart);
        if (result == null) result = caseMethodStep(methodPart);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.EXECUTABLE_ELEMENT:
      {
        ExecutableElement executableElement = (ExecutableElement)theEObject;
        T result = caseExecutableElement(executableElement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.DATA_STORAGE_ELEMENT:
      {
        DataStorageElement dataStorageElement = (DataStorageElement)theEObject;
        T result = caseDataStorageElement(dataStorageElement);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.METHOD_VARIABLE:
      {
        MethodVariable methodVariable = (MethodVariable)theEObject;
        T result = caseMethodVariable(methodVariable);
        if (result == null) result = caseDataStorageElement(methodVariable);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.INTERNAL_VARIABLE:
      {
        InternalVariable internalVariable = (InternalVariable)theEObject;
        T result = caseInternalVariable(internalVariable);
        if (result == null) result = caseDataStorageElement(internalVariable);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.PHASE:
      {
        Phase phase = (Phase)theEObject;
        T result = casePhase(phase);
        if (result == null) result = caseMethodPart(phase);
        if (result == null) result = caseMethodStep(phase);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.TASK:
      {
        Task task = (Task)theEObject;
        T result = caseTask(task);
        if (result == null) result = caseMethodStep(task);
        if (result == null) result = caseExecutableElement(task);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.ABSTRACT_SUB_TASK:
      {
        AbstractSubTask abstractSubTask = (AbstractSubTask)theEObject;
        T result = caseAbstractSubTask(abstractSubTask);
        if (result == null) result = caseMethodStep(abstractSubTask);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.SUB_TASK:
      {
        SubTask subTask = (SubTask)theEObject;
        T result = caseSubTask(subTask);
        if (result == null) result = caseAbstractSubTask(subTask);
        if (result == null) result = caseExecutableElement(subTask);
        if (result == null) result = caseMethodStep(subTask);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.CONDITION:
      {
        Condition condition = (Condition)theEObject;
        T result = caseCondition(condition);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.CONDITIONAL_SUB_TASK:
      {
        ConditionalSubTask conditionalSubTask = (ConditionalSubTask)theEObject;
        T result = caseConditionalSubTask(conditionalSubTask);
        if (result == null) result = caseAbstractSubTask(conditionalSubTask);
        if (result == null) result = caseMethodStep(conditionalSubTask);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.REPEATED_SUB_TASK:
      {
        RepeatedSubTask repeatedSubTask = (RepeatedSubTask)theEObject;
        T result = caseRepeatedSubTask(repeatedSubTask);
        if (result == null) result = caseAbstractSubTask(repeatedSubTask);
        if (result == null) result = caseMethodStep(repeatedSubTask);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.COMMAND_PARAMETER:
      {
        CommandParameter commandParameter = (CommandParameter)theEObject;
        T result = caseCommandParameter(commandParameter);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.VARIABLE_COMMAND_PARAMETER:
      {
        VariableCommandParameter variableCommandParameter = (VariableCommandParameter)theEObject;
        T result = caseVariableCommandParameter(variableCommandParameter);
        if (result == null) result = caseCommandParameter(variableCommandParameter);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.CONSTANT_COMMAND_PARAMETER:
      {
        ConstantCommandParameter constantCommandParameter = (ConstantCommandParameter)theEObject;
        T result = caseConstantCommandParameter(constantCommandParameter);
        if (result == null) result = caseCommandParameter(constantCommandParameter);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.METHOD_COMMAND:
      {
        MethodCommand methodCommand = (MethodCommand)theEObject;
        T result = caseMethodCommand(methodCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.COMPLEX_COMMAND:
      {
        ComplexCommand complexCommand = (ComplexCommand)theEObject;
        T result = caseComplexCommand(complexCommand);
        if (result == null) result = caseMethodCommand(complexCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.SIMPLE_COMMAND:
      {
        SimpleCommand simpleCommand = (SimpleCommand)theEObject;
        T result = caseSimpleCommand(simpleCommand);
        if (result == null) result = caseMethodCommand(simpleCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.CUSTOM_COMMAND:
      {
        CustomCommand customCommand = (CustomCommand)theEObject;
        T result = caseCustomCommand(customCommand);
        if (result == null) result = caseSimpleCommand(customCommand);
        if (result == null) result = caseMethodCommand(customCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.CALL_WIZARD_COMMAND:
      {
        CallWizardCommand callWizardCommand = (CallWizardCommand)theEObject;
        T result = caseCallWizardCommand(callWizardCommand);
        if (result == null) result = caseSimpleCommand(callWizardCommand);
        if (result == null) result = caseMethodCommand(callWizardCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.SELECT_FILE_COMMAND:
      {
        SelectFileCommand selectFileCommand = (SelectFileCommand)theEObject;
        T result = caseSelectFileCommand(selectFileCommand);
        if (result == null) result = caseSimpleCommand(selectFileCommand);
        if (result == null) result = caseMethodCommand(selectFileCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.SELECT_FOLDER_COMMAND:
      {
        SelectFolderCommand selectFolderCommand = (SelectFolderCommand)theEObject;
        T result = caseSelectFolderCommand(selectFolderCommand);
        if (result == null) result = caseSimpleCommand(selectFolderCommand);
        if (result == null) result = caseMethodCommand(selectFolderCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.SELECT_METAMODEL_COMMAND:
      {
        SelectMetamodelCommand selectMetamodelCommand = (SelectMetamodelCommand)theEObject;
        T result = caseSelectMetamodelCommand(selectMetamodelCommand);
        if (result == null) result = caseSimpleCommand(selectMetamodelCommand);
        if (result == null) result = caseMethodCommand(selectMetamodelCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND:
      {
        SelectModelElementFromFileCommand selectModelElementFromFileCommand = (SelectModelElementFromFileCommand)theEObject;
        T result = caseSelectModelElementFromFileCommand(selectModelElementFromFileCommand);
        if (result == null) result = caseSimpleCommand(selectModelElementFromFileCommand);
        if (result == null) result = caseMethodCommand(selectModelElementFromFileCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND:
      {
        SelectModelElementFromEObjectCommand selectModelElementFromEObjectCommand = (SelectModelElementFromEObjectCommand)theEObject;
        T result = caseSelectModelElementFromEObjectCommand(selectModelElementFromEObjectCommand);
        if (result == null) result = caseSimpleCommand(selectModelElementFromEObjectCommand);
        if (result == null) result = caseMethodCommand(selectModelElementFromEObjectCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.SELECT_VALIDATION_COMMAND:
      {
        SelectValidationCommand selectValidationCommand = (SelectValidationCommand)theEObject;
        T result = caseSelectValidationCommand(selectValidationCommand);
        if (result == null) result = caseSimpleCommand(selectValidationCommand);
        if (result == null) result = caseMethodCommand(selectValidationCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.SELECT_TRANSFORMATION_COMMAND:
      {
        SelectTransformationCommand selectTransformationCommand = (SelectTransformationCommand)theEObject;
        T result = caseSelectTransformationCommand(selectTransformationCommand);
        if (result == null) result = caseSimpleCommand(selectTransformationCommand);
        if (result == null) result = caseMethodCommand(selectTransformationCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.LAUNCH_VALIDATION_COMMAND:
      {
        LaunchValidationCommand launchValidationCommand = (LaunchValidationCommand)theEObject;
        T result = caseLaunchValidationCommand(launchValidationCommand);
        if (result == null) result = caseSimpleCommand(launchValidationCommand);
        if (result == null) result = caseMethodCommand(launchValidationCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.LAUNCH_TRANSFORMATION_COMMAND:
      {
        LaunchTransformationCommand launchTransformationCommand = (LaunchTransformationCommand)theEObject;
        T result = caseLaunchTransformationCommand(launchTransformationCommand);
        if (result == null) result = caseSimpleCommand(launchTransformationCommand);
        if (result == null) result = caseMethodCommand(launchTransformationCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case MethodPackage.OPEN_EDITOR_COMMAND:
      {
        OpenEditorCommand openEditorCommand = (OpenEditorCommand)theEObject;
        T result = caseOpenEditorCommand(openEditorCommand);
        if (result == null) result = caseSimpleCommand(openEditorCommand);
        if (result == null) result = caseMethodCommand(openEditorCommand);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>GEMDE Method</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>GEMDE Method</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseGEMDEMethod(GEMDEMethod object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Step</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Step</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMethodStep(MethodStep object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Part</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Part</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMethodPart(MethodPart object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Executable Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Executable Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseExecutableElement(ExecutableElement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Data Storage Element</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Data Storage Element</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDataStorageElement(DataStorageElement object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Variable</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Variable</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMethodVariable(MethodVariable object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Internal Variable</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Internal Variable</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseInternalVariable(InternalVariable object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Phase</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Phase</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T casePhase(Phase object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Task</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Task</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseTask(Task object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Abstract Sub Task</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Abstract Sub Task</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAbstractSubTask(AbstractSubTask object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Sub Task</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Sub Task</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSubTask(SubTask object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Condition</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Condition</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCondition(Condition object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Conditional Sub Task</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Conditional Sub Task</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConditionalSubTask(ConditionalSubTask object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Repeated Sub Task</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Repeated Sub Task</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseRepeatedSubTask(RepeatedSubTask object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Command Parameter</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Command Parameter</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCommandParameter(CommandParameter object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Variable Command Parameter</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Variable Command Parameter</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseVariableCommandParameter(VariableCommandParameter object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Constant Command Parameter</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Constant Command Parameter</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseConstantCommandParameter(ConstantCommandParameter object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseMethodCommand(MethodCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Complex Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Complex Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseComplexCommand(ComplexCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Simple Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Simple Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSimpleCommand(SimpleCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Custom Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Custom Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCustomCommand(CustomCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Call Wizard Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Call Wizard Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseCallWizardCommand(CallWizardCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Select File Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Select File Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSelectFileCommand(SelectFileCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Select Folder Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Select Folder Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSelectFolderCommand(SelectFolderCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Select Metamodel Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Select Metamodel Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSelectMetamodelCommand(SelectMetamodelCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Select Model Element From File Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Select Model Element From File Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSelectModelElementFromFileCommand(SelectModelElementFromFileCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Select Model Element From EObject Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Select Model Element From EObject Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSelectModelElementFromEObjectCommand(SelectModelElementFromEObjectCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Select Validation Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Select Validation Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSelectValidationCommand(SelectValidationCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Select Transformation Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Select Transformation Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSelectTransformationCommand(SelectTransformationCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Launch Validation Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Launch Validation Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLaunchValidationCommand(LaunchValidationCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Launch Transformation Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Launch Transformation Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseLaunchTransformationCommand(LaunchTransformationCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Open Editor Command</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Open Editor Command</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseOpenEditorCommand(OpenEditorCommand object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  @Override
  public T defaultCase(EObject object)
  {
    return null;
  }

} //MethodSwitch
