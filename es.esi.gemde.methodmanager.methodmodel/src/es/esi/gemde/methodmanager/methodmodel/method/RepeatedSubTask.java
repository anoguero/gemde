/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Repeated Sub Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.RepeatedSubTask#getRepeatedElement <em>Repeated Element</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.RepeatedSubTask#getRepetitionVector <em>Repetition Vector</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.RepeatedSubTask#getRepetitionVariable <em>Repetition Variable</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getRepeatedSubTask()
 * @model annotation="gmf.node figure='rounded' size='100,50' border.color='122,163,204' color='204,230,255' label='title,repetitionVector' label.pattern='{0} {1}' label.readOnly='true' tool.name='Add Repeated SubTask'"
 * @generated
 */
public interface RepeatedSubTask extends AbstractSubTask
{
  /**
   * Returns the value of the '<em><b>Repeated Element</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Repeated Element</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Repeated Element</em>' reference.
   * @see #setRepeatedElement(SubTask)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getRepeatedSubTask_RepeatedElement()
   * @model required="true"
   *        annotation="gmf.link style='dot' width='2' color='122,163,204' tool.name='Repeated SubTask' tool.small.bundle='es.esi.gemde.methodmanager.methodmodel.edit' tool.small.path='icons/full/obj16/RepeatedSubTaskTool.gif'"
   * @generated
   */
  SubTask getRepeatedElement();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.RepeatedSubTask#getRepeatedElement <em>Repeated Element</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Repeated Element</em>' reference.
   * @see #getRepeatedElement()
   * @generated
   */
  void setRepeatedElement(SubTask value);

  /**
   * Returns the value of the '<em><b>Repetition Vector</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Repetition Vector</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Repetition Vector</em>' attribute list.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getRepeatedSubTask_RepetitionVector()
   * @model unique="false"
   * @generated
   */
  EList<String> getRepetitionVector();

  /**
   * Returns the value of the '<em><b>Repetition Variable</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Repetition Variable</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Repetition Variable</em>' containment reference.
   * @see #setRepetitionVariable(InternalVariable)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getRepeatedSubTask_RepetitionVariable()
   * @model containment="true" required="true"
   * @generated
   */
  InternalVariable getRepetitionVariable();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.RepeatedSubTask#getRepetitionVariable <em>Repetition Variable</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Repetition Variable</em>' containment reference.
   * @see #getRepetitionVariable()
   * @generated
   */
  void setRepetitionVariable(InternalVariable value);

} // RepeatedSubTask
