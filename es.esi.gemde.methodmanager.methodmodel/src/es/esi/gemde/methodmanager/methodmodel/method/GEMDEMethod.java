/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>GEMDE Method</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getName <em>Name</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getDescription <em>Description</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getProjectName <em>Project Name</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getPhases <em>Phases</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getInitialPhase <em>Initial Phase</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getVariables <em>Variables</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getCommands <em>Commands</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getGEMDEMethod()
 * @model annotation="gmf.diagram model.extension='method' diagram.extension='methoddi'"
 * @generated
 */
public interface GEMDEMethod extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getGEMDEMethod_Name()
   * @model required="true"
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Description</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Description</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Description</em>' attribute.
   * @see #setDescription(String)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getGEMDEMethod_Description()
   * @model
   * @generated
   */
  String getDescription();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getDescription <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Description</em>' attribute.
   * @see #getDescription()
   * @generated
   */
  void setDescription(String value);

  /**
   * Returns the value of the '<em><b>Project Name</b></em>' attribute.
   * The default value is <code>"es.esi.gemde.example.methodology.cheatsheet"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Project Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Project Name</em>' attribute.
   * @see #setProjectName(String)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getGEMDEMethod_ProjectName()
   * @model default="es.esi.gemde.example.methodology.cheatsheet" required="true"
   * @generated
   */
  String getProjectName();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getProjectName <em>Project Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Project Name</em>' attribute.
   * @see #getProjectName()
   * @generated
   */
  void setProjectName(String value);

  /**
   * Returns the value of the '<em><b>Phases</b></em>' containment reference list.
   * The list contents are of type {@link es.esi.gemde.methodmanager.methodmodel.method.Phase}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Phases</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Phases</em>' containment reference list.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getGEMDEMethod_Phases()
   * @model containment="true" required="true"
   * @generated
   */
  EList<Phase> getPhases();

  /**
   * Returns the value of the '<em><b>Initial Phase</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Initial Phase</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Initial Phase</em>' reference.
   * @see #setInitialPhase(Phase)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getGEMDEMethod_InitialPhase()
   * @model required="true"
   * @generated
   */
  Phase getInitialPhase();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod#getInitialPhase <em>Initial Phase</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Initial Phase</em>' reference.
   * @see #getInitialPhase()
   * @generated
   */
  void setInitialPhase(Phase value);

  /**
   * Returns the value of the '<em><b>Variables</b></em>' containment reference list.
   * The list contents are of type {@link es.esi.gemde.methodmanager.methodmodel.method.MethodVariable}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Variables</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Variables</em>' containment reference list.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getGEMDEMethod_Variables()
   * @model containment="true"
   * @generated
   */
  EList<MethodVariable> getVariables();

  /**
   * Returns the value of the '<em><b>Commands</b></em>' containment reference list.
   * The list contents are of type {@link es.esi.gemde.methodmanager.methodmodel.method.MethodCommand}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Commands</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Commands</em>' containment reference list.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getGEMDEMethod_Commands()
   * @model containment="true"
   * @generated
   */
  EList<MethodCommand> getCommands();

} // GEMDEMethod
