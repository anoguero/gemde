/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Select Metamodel Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.SelectMetamodelCommand#getIdentifier <em>Identifier</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSelectMetamodelCommand()
 * @model annotation="gmf.node figure='rectangle' border.color='102,76,0' color='230,204,128' label='name' tool.name='Add Select Metamodel'"
 * @generated
 */
public interface SelectMetamodelCommand extends SimpleCommand
{
  /**
   * Returns the value of the '<em><b>Identifier</b></em>' attribute.
   * The default value is <code>"es.esi.gemde.methodmanager.commands.selectmetamodelcommand"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Identifier</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Identifier</em>' attribute.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSelectMetamodelCommand_Identifier()
   * @model default="es.esi.gemde.methodmanager.commands.selectmetamodelcommand" required="true" changeable="false"
   * @generated
   */
  String getIdentifier();

} // SelectMetamodelCommand
