/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.impl;

import es.esi.gemde.methodmanager.methodmodel.method.MethodPackage;
import es.esi.gemde.methodmanager.methodmodel.method.Phase;
import es.esi.gemde.methodmanager.methodmodel.method.Task;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Phase</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.PhaseImpl#getTasks <em>Tasks</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.PhaseImpl#getInitialTask <em>Initial Task</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.PhaseImpl#getPrev <em>Prev</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.PhaseImpl#getNext <em>Next</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PhaseImpl extends MethodPartImpl implements Phase
{
  /**
   * The cached value of the '{@link #getTasks() <em>Tasks</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTasks()
   * @generated
   * @ordered
   */
  protected EList<Task> tasks;

  /**
   * The cached value of the '{@link #getInitialTask() <em>Initial Task</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInitialTask()
   * @generated
   * @ordered
   */
  protected Task initialTask;

  /**
   * The cached value of the '{@link #getPrev() <em>Prev</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPrev()
   * @generated
   * @ordered
   */
  protected Phase prev;

  /**
   * The cached value of the '{@link #getNext() <em>Next</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNext()
   * @generated
   * @ordered
   */
  protected Phase next;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected PhaseImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MethodPackage.Literals.PHASE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Task> getTasks()
  {
    if (tasks == null)
    {
      tasks = new EObjectContainmentEList<Task>(Task.class, this, MethodPackage.PHASE__TASKS);
    }
    return tasks;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Task getInitialTask()
  {
    if (initialTask != null && initialTask.eIsProxy())
    {
      InternalEObject oldInitialTask = (InternalEObject)initialTask;
      initialTask = (Task)eResolveProxy(oldInitialTask);
      if (initialTask != oldInitialTask)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.PHASE__INITIAL_TASK, oldInitialTask, initialTask));
      }
    }
    return initialTask;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Task basicGetInitialTask()
  {
    return initialTask;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInitialTask(Task newInitialTask)
  {
    Task oldInitialTask = initialTask;
    initialTask = newInitialTask;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.PHASE__INITIAL_TASK, oldInitialTask, initialTask));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Phase getPrev()
  {
    if (prev != null && prev.eIsProxy())
    {
      InternalEObject oldPrev = (InternalEObject)prev;
      prev = (Phase)eResolveProxy(oldPrev);
      if (prev != oldPrev)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.PHASE__PREV, oldPrev, prev));
      }
    }
    return prev;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Phase basicGetPrev()
  {
    return prev;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPrev(Phase newPrev, NotificationChain msgs)
  {
    Phase oldPrev = prev;
    prev = newPrev;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MethodPackage.PHASE__PREV, oldPrev, newPrev);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPrev(Phase newPrev)
  {
    if (newPrev != prev)
    {
      NotificationChain msgs = null;
      if (prev != null)
        msgs = ((InternalEObject)prev).eInverseRemove(this, MethodPackage.PHASE__NEXT, Phase.class, msgs);
      if (newPrev != null)
        msgs = ((InternalEObject)newPrev).eInverseAdd(this, MethodPackage.PHASE__NEXT, Phase.class, msgs);
      msgs = basicSetPrev(newPrev, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.PHASE__PREV, newPrev, newPrev));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Phase getNext()
  {
    if (next != null && next.eIsProxy())
    {
      InternalEObject oldNext = (InternalEObject)next;
      next = (Phase)eResolveProxy(oldNext);
      if (next != oldNext)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.PHASE__NEXT, oldNext, next));
      }
    }
    return next;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Phase basicGetNext()
  {
    return next;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetNext(Phase newNext, NotificationChain msgs)
  {
    Phase oldNext = next;
    next = newNext;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MethodPackage.PHASE__NEXT, oldNext, newNext);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNext(Phase newNext)
  {
    if (newNext != next)
    {
      NotificationChain msgs = null;
      if (next != null)
        msgs = ((InternalEObject)next).eInverseRemove(this, MethodPackage.PHASE__PREV, Phase.class, msgs);
      if (newNext != null)
        msgs = ((InternalEObject)newNext).eInverseAdd(this, MethodPackage.PHASE__PREV, Phase.class, msgs);
      msgs = basicSetNext(newNext, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.PHASE__NEXT, newNext, newNext));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MethodPackage.PHASE__PREV:
        if (prev != null)
          msgs = ((InternalEObject)prev).eInverseRemove(this, MethodPackage.PHASE__NEXT, Phase.class, msgs);
        return basicSetPrev((Phase)otherEnd, msgs);
      case MethodPackage.PHASE__NEXT:
        if (next != null)
          msgs = ((InternalEObject)next).eInverseRemove(this, MethodPackage.PHASE__PREV, Phase.class, msgs);
        return basicSetNext((Phase)otherEnd, msgs);
    }
    return super.eInverseAdd(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MethodPackage.PHASE__TASKS:
        return ((InternalEList<?>)getTasks()).basicRemove(otherEnd, msgs);
      case MethodPackage.PHASE__PREV:
        return basicSetPrev(null, msgs);
      case MethodPackage.PHASE__NEXT:
        return basicSetNext(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MethodPackage.PHASE__TASKS:
        return getTasks();
      case MethodPackage.PHASE__INITIAL_TASK:
        if (resolve) return getInitialTask();
        return basicGetInitialTask();
      case MethodPackage.PHASE__PREV:
        if (resolve) return getPrev();
        return basicGetPrev();
      case MethodPackage.PHASE__NEXT:
        if (resolve) return getNext();
        return basicGetNext();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MethodPackage.PHASE__TASKS:
        getTasks().clear();
        getTasks().addAll((Collection<? extends Task>)newValue);
        return;
      case MethodPackage.PHASE__INITIAL_TASK:
        setInitialTask((Task)newValue);
        return;
      case MethodPackage.PHASE__PREV:
        setPrev((Phase)newValue);
        return;
      case MethodPackage.PHASE__NEXT:
        setNext((Phase)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.PHASE__TASKS:
        getTasks().clear();
        return;
      case MethodPackage.PHASE__INITIAL_TASK:
        setInitialTask((Task)null);
        return;
      case MethodPackage.PHASE__PREV:
        setPrev((Phase)null);
        return;
      case MethodPackage.PHASE__NEXT:
        setNext((Phase)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.PHASE__TASKS:
        return tasks != null && !tasks.isEmpty();
      case MethodPackage.PHASE__INITIAL_TASK:
        return initialTask != null;
      case MethodPackage.PHASE__PREV:
        return prev != null;
      case MethodPackage.PHASE__NEXT:
        return next != null;
    }
    return super.eIsSet(featureID);
  }

} //PhaseImpl
