/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.impl;

import es.esi.gemde.methodmanager.methodmodel.method.CallWizardCommand;
import es.esi.gemde.methodmanager.methodmodel.method.CommandParameter;
import es.esi.gemde.methodmanager.methodmodel.method.MethodPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Call Wizard Command</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.CallWizardCommandImpl#getIdentifier <em>Identifier</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.CallWizardCommandImpl#getWizard <em>Wizard</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CallWizardCommandImpl extends SimpleCommandImpl implements CallWizardCommand
{
  /**
   * The default value of the '{@link #getIdentifier() <em>Identifier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdentifier()
   * @generated
   * @ordered
   */
  protected static final String IDENTIFIER_EDEFAULT = "es.esi.gemde.methodmanager.commands.callwizardcommand";

  /**
   * The cached value of the '{@link #getIdentifier() <em>Identifier</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdentifier()
   * @generated
   * @ordered
   */
  protected String identifier = IDENTIFIER_EDEFAULT;

  /**
   * The cached value of the '{@link #getWizard() <em>Wizard</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getWizard()
   * @generated
   * @ordered
   */
  protected CommandParameter wizard;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CallWizardCommandImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MethodPackage.Literals.CALL_WIZARD_COMMAND;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getIdentifier()
  {
    return identifier;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter getWizard()
  {
    if (wizard != null && wizard.eIsProxy())
    {
      InternalEObject oldWizard = (InternalEObject)wizard;
      wizard = (CommandParameter)eResolveProxy(oldWizard);
      if (wizard != oldWizard)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.CALL_WIZARD_COMMAND__WIZARD, oldWizard, wizard));
      }
    }
    return wizard;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CommandParameter basicGetWizard()
  {
    return wizard;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setWizard(CommandParameter newWizard)
  {
    CommandParameter oldWizard = wizard;
    wizard = newWizard;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.CALL_WIZARD_COMMAND__WIZARD, oldWizard, wizard));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MethodPackage.CALL_WIZARD_COMMAND__IDENTIFIER:
        return getIdentifier();
      case MethodPackage.CALL_WIZARD_COMMAND__WIZARD:
        if (resolve) return getWizard();
        return basicGetWizard();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MethodPackage.CALL_WIZARD_COMMAND__WIZARD:
        setWizard((CommandParameter)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.CALL_WIZARD_COMMAND__WIZARD:
        setWizard((CommandParameter)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.CALL_WIZARD_COMMAND__IDENTIFIER:
        return IDENTIFIER_EDEFAULT == null ? identifier != null : !IDENTIFIER_EDEFAULT.equals(identifier);
      case MethodPackage.CALL_WIZARD_COMMAND__WIZARD:
        return wizard != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (identifier: ");
    result.append(identifier);
    result.append(')');
    return result.toString();
  }

} //CallWizardCommandImpl
