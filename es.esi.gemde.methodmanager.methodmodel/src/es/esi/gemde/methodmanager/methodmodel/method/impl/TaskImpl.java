/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.impl;

import es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask;
import es.esi.gemde.methodmanager.methodmodel.method.ExecutableElement;
import es.esi.gemde.methodmanager.methodmodel.method.MethodCommand;
import es.esi.gemde.methodmanager.methodmodel.method.MethodPackage;
import es.esi.gemde.methodmanager.methodmodel.method.Task;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.TaskImpl#getCommand <em>Command</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.TaskImpl#getSubTasks <em>Sub Tasks</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.TaskImpl#getInitialSubtask <em>Initial Subtask</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.TaskImpl#getPrev <em>Prev</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.TaskImpl#getNext <em>Next</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TaskImpl extends MethodStepImpl implements Task
{
  /**
   * The cached value of the '{@link #getCommand() <em>Command</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCommand()
   * @generated
   * @ordered
   */
  protected MethodCommand command;

  /**
   * The cached value of the '{@link #getSubTasks() <em>Sub Tasks</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSubTasks()
   * @generated
   * @ordered
   */
  protected EList<AbstractSubTask> subTasks;

  /**
   * The cached value of the '{@link #getInitialSubtask() <em>Initial Subtask</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInitialSubtask()
   * @generated
   * @ordered
   */
  protected AbstractSubTask initialSubtask;

  /**
   * The cached value of the '{@link #getPrev() <em>Prev</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPrev()
   * @generated
   * @ordered
   */
  protected Task prev;

  /**
   * The cached value of the '{@link #getNext() <em>Next</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNext()
   * @generated
   * @ordered
   */
  protected Task next;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected TaskImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MethodPackage.Literals.TASK;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodCommand getCommand()
  {
    if (command != null && command.eIsProxy())
    {
      InternalEObject oldCommand = (InternalEObject)command;
      command = (MethodCommand)eResolveProxy(oldCommand);
      if (command != oldCommand)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.TASK__COMMAND, oldCommand, command));
      }
    }
    return command;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodCommand basicGetCommand()
  {
    return command;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCommand(MethodCommand newCommand)
  {
    MethodCommand oldCommand = command;
    command = newCommand;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.TASK__COMMAND, oldCommand, command));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<AbstractSubTask> getSubTasks()
  {
    if (subTasks == null)
    {
      subTasks = new EObjectContainmentEList<AbstractSubTask>(AbstractSubTask.class, this, MethodPackage.TASK__SUB_TASKS);
    }
    return subTasks;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AbstractSubTask getInitialSubtask()
  {
    if (initialSubtask != null && initialSubtask.eIsProxy())
    {
      InternalEObject oldInitialSubtask = (InternalEObject)initialSubtask;
      initialSubtask = (AbstractSubTask)eResolveProxy(oldInitialSubtask);
      if (initialSubtask != oldInitialSubtask)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.TASK__INITIAL_SUBTASK, oldInitialSubtask, initialSubtask));
      }
    }
    return initialSubtask;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AbstractSubTask basicGetInitialSubtask()
  {
    return initialSubtask;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInitialSubtask(AbstractSubTask newInitialSubtask)
  {
    AbstractSubTask oldInitialSubtask = initialSubtask;
    initialSubtask = newInitialSubtask;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.TASK__INITIAL_SUBTASK, oldInitialSubtask, initialSubtask));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Task getPrev()
  {
    if (prev != null && prev.eIsProxy())
    {
      InternalEObject oldPrev = (InternalEObject)prev;
      prev = (Task)eResolveProxy(oldPrev);
      if (prev != oldPrev)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.TASK__PREV, oldPrev, prev));
      }
    }
    return prev;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Task basicGetPrev()
  {
    return prev;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPrev(Task newPrev, NotificationChain msgs)
  {
    Task oldPrev = prev;
    prev = newPrev;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MethodPackage.TASK__PREV, oldPrev, newPrev);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPrev(Task newPrev)
  {
    if (newPrev != prev)
    {
      NotificationChain msgs = null;
      if (prev != null)
        msgs = ((InternalEObject)prev).eInverseRemove(this, MethodPackage.TASK__NEXT, Task.class, msgs);
      if (newPrev != null)
        msgs = ((InternalEObject)newPrev).eInverseAdd(this, MethodPackage.TASK__NEXT, Task.class, msgs);
      msgs = basicSetPrev(newPrev, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.TASK__PREV, newPrev, newPrev));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Task getNext()
  {
    if (next != null && next.eIsProxy())
    {
      InternalEObject oldNext = (InternalEObject)next;
      next = (Task)eResolveProxy(oldNext);
      if (next != oldNext)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.TASK__NEXT, oldNext, next));
      }
    }
    return next;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Task basicGetNext()
  {
    return next;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetNext(Task newNext, NotificationChain msgs)
  {
    Task oldNext = next;
    next = newNext;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MethodPackage.TASK__NEXT, oldNext, newNext);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNext(Task newNext)
  {
    if (newNext != next)
    {
      NotificationChain msgs = null;
      if (next != null)
        msgs = ((InternalEObject)next).eInverseRemove(this, MethodPackage.TASK__PREV, Task.class, msgs);
      if (newNext != null)
        msgs = ((InternalEObject)newNext).eInverseAdd(this, MethodPackage.TASK__PREV, Task.class, msgs);
      msgs = basicSetNext(newNext, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.TASK__NEXT, newNext, newNext));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MethodPackage.TASK__PREV:
        if (prev != null)
          msgs = ((InternalEObject)prev).eInverseRemove(this, MethodPackage.TASK__NEXT, Task.class, msgs);
        return basicSetPrev((Task)otherEnd, msgs);
      case MethodPackage.TASK__NEXT:
        if (next != null)
          msgs = ((InternalEObject)next).eInverseRemove(this, MethodPackage.TASK__PREV, Task.class, msgs);
        return basicSetNext((Task)otherEnd, msgs);
    }
    return super.eInverseAdd(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MethodPackage.TASK__SUB_TASKS:
        return ((InternalEList<?>)getSubTasks()).basicRemove(otherEnd, msgs);
      case MethodPackage.TASK__PREV:
        return basicSetPrev(null, msgs);
      case MethodPackage.TASK__NEXT:
        return basicSetNext(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MethodPackage.TASK__COMMAND:
        if (resolve) return getCommand();
        return basicGetCommand();
      case MethodPackage.TASK__SUB_TASKS:
        return getSubTasks();
      case MethodPackage.TASK__INITIAL_SUBTASK:
        if (resolve) return getInitialSubtask();
        return basicGetInitialSubtask();
      case MethodPackage.TASK__PREV:
        if (resolve) return getPrev();
        return basicGetPrev();
      case MethodPackage.TASK__NEXT:
        if (resolve) return getNext();
        return basicGetNext();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MethodPackage.TASK__COMMAND:
        setCommand((MethodCommand)newValue);
        return;
      case MethodPackage.TASK__SUB_TASKS:
        getSubTasks().clear();
        getSubTasks().addAll((Collection<? extends AbstractSubTask>)newValue);
        return;
      case MethodPackage.TASK__INITIAL_SUBTASK:
        setInitialSubtask((AbstractSubTask)newValue);
        return;
      case MethodPackage.TASK__PREV:
        setPrev((Task)newValue);
        return;
      case MethodPackage.TASK__NEXT:
        setNext((Task)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.TASK__COMMAND:
        setCommand((MethodCommand)null);
        return;
      case MethodPackage.TASK__SUB_TASKS:
        getSubTasks().clear();
        return;
      case MethodPackage.TASK__INITIAL_SUBTASK:
        setInitialSubtask((AbstractSubTask)null);
        return;
      case MethodPackage.TASK__PREV:
        setPrev((Task)null);
        return;
      case MethodPackage.TASK__NEXT:
        setNext((Task)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.TASK__COMMAND:
        return command != null;
      case MethodPackage.TASK__SUB_TASKS:
        return subTasks != null && !subTasks.isEmpty();
      case MethodPackage.TASK__INITIAL_SUBTASK:
        return initialSubtask != null;
      case MethodPackage.TASK__PREV:
        return prev != null;
      case MethodPackage.TASK__NEXT:
        return next != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass)
  {
    if (baseClass == ExecutableElement.class)
    {
      switch (derivedFeatureID)
      {
        case MethodPackage.TASK__COMMAND: return MethodPackage.EXECUTABLE_ELEMENT__COMMAND;
        default: return -1;
      }
    }
    return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass)
  {
    if (baseClass == ExecutableElement.class)
    {
      switch (baseFeatureID)
      {
        case MethodPackage.EXECUTABLE_ELEMENT__COMMAND: return MethodPackage.TASK__COMMAND;
        default: return -1;
      }
    }
    return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
  }

} //TaskImpl
