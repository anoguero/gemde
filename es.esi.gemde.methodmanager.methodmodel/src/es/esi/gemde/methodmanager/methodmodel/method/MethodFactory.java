/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage
 * @generated
 */
public interface MethodFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  MethodFactory eINSTANCE = es.esi.gemde.methodmanager.methodmodel.method.impl.MethodFactoryImpl.init();

  /**
   * Returns a new object of class '<em>GEMDE Method</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>GEMDE Method</em>'.
   * @generated
   */
  GEMDEMethod createGEMDEMethod();

  /**
   * Returns a new object of class '<em>Variable</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Variable</em>'.
   * @generated
   */
  MethodVariable createMethodVariable();

  /**
   * Returns a new object of class '<em>Internal Variable</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Internal Variable</em>'.
   * @generated
   */
  InternalVariable createInternalVariable();

  /**
   * Returns a new object of class '<em>Phase</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Phase</em>'.
   * @generated
   */
  Phase createPhase();

  /**
   * Returns a new object of class '<em>Task</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Task</em>'.
   * @generated
   */
  Task createTask();

  /**
   * Returns a new object of class '<em>Sub Task</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Sub Task</em>'.
   * @generated
   */
  SubTask createSubTask();

  /**
   * Returns a new object of class '<em>Condition</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Condition</em>'.
   * @generated
   */
  Condition createCondition();

  /**
   * Returns a new object of class '<em>Conditional Sub Task</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Conditional Sub Task</em>'.
   * @generated
   */
  ConditionalSubTask createConditionalSubTask();

  /**
   * Returns a new object of class '<em>Repeated Sub Task</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Repeated Sub Task</em>'.
   * @generated
   */
  RepeatedSubTask createRepeatedSubTask();

  /**
   * Returns a new object of class '<em>Variable Command Parameter</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Variable Command Parameter</em>'.
   * @generated
   */
  VariableCommandParameter createVariableCommandParameter();

  /**
   * Returns a new object of class '<em>Constant Command Parameter</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Constant Command Parameter</em>'.
   * @generated
   */
  ConstantCommandParameter createConstantCommandParameter();

  /**
   * Returns a new object of class '<em>Complex Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Complex Command</em>'.
   * @generated
   */
  ComplexCommand createComplexCommand();

  /**
   * Returns a new object of class '<em>Custom Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Custom Command</em>'.
   * @generated
   */
  CustomCommand createCustomCommand();

  /**
   * Returns a new object of class '<em>Call Wizard Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Call Wizard Command</em>'.
   * @generated
   */
  CallWizardCommand createCallWizardCommand();

  /**
   * Returns a new object of class '<em>Select File Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Select File Command</em>'.
   * @generated
   */
  SelectFileCommand createSelectFileCommand();

  /**
   * Returns a new object of class '<em>Select Folder Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Select Folder Command</em>'.
   * @generated
   */
  SelectFolderCommand createSelectFolderCommand();

  /**
   * Returns a new object of class '<em>Select Metamodel Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Select Metamodel Command</em>'.
   * @generated
   */
  SelectMetamodelCommand createSelectMetamodelCommand();

  /**
   * Returns a new object of class '<em>Select Model Element From File Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Select Model Element From File Command</em>'.
   * @generated
   */
  SelectModelElementFromFileCommand createSelectModelElementFromFileCommand();

  /**
   * Returns a new object of class '<em>Select Model Element From EObject Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Select Model Element From EObject Command</em>'.
   * @generated
   */
  SelectModelElementFromEObjectCommand createSelectModelElementFromEObjectCommand();

  /**
   * Returns a new object of class '<em>Select Validation Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Select Validation Command</em>'.
   * @generated
   */
  SelectValidationCommand createSelectValidationCommand();

  /**
   * Returns a new object of class '<em>Select Transformation Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Select Transformation Command</em>'.
   * @generated
   */
  SelectTransformationCommand createSelectTransformationCommand();

  /**
   * Returns a new object of class '<em>Launch Validation Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Launch Validation Command</em>'.
   * @generated
   */
  LaunchValidationCommand createLaunchValidationCommand();

  /**
   * Returns a new object of class '<em>Launch Transformation Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Launch Transformation Command</em>'.
   * @generated
   */
  LaunchTransformationCommand createLaunchTransformationCommand();

  /**
   * Returns a new object of class '<em>Open Editor Command</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Open Editor Command</em>'.
   * @generated
   */
  OpenEditorCommand createOpenEditorCommand();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  MethodPackage getMethodPackage();

} //MethodFactory
