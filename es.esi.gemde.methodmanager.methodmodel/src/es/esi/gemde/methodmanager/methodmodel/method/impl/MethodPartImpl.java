/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.impl;

import es.esi.gemde.methodmanager.methodmodel.method.MethodPackage;
import es.esi.gemde.methodmanager.methodmodel.method.MethodPart;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Part</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPartImpl#getIntroduction <em>Introduction</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPartImpl#getConclusion <em>Conclusion</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.MethodPartImpl#getIsOptional <em>Is Optional</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class MethodPartImpl extends MethodStepImpl implements MethodPart
{
  /**
   * The default value of the '{@link #getIntroduction() <em>Introduction</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIntroduction()
   * @generated
   * @ordered
   */
  protected static final String INTRODUCTION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getIntroduction() <em>Introduction</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIntroduction()
   * @generated
   * @ordered
   */
  protected String introduction = INTRODUCTION_EDEFAULT;

  /**
   * The default value of the '{@link #getConclusion() <em>Conclusion</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConclusion()
   * @generated
   * @ordered
   */
  protected static final String CONCLUSION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getConclusion() <em>Conclusion</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getConclusion()
   * @generated
   * @ordered
   */
  protected String conclusion = CONCLUSION_EDEFAULT;

  /**
   * The default value of the '{@link #getIsOptional() <em>Is Optional</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIsOptional()
   * @generated
   * @ordered
   */
  protected static final Boolean IS_OPTIONAL_EDEFAULT = Boolean.FALSE;

  /**
   * The cached value of the '{@link #getIsOptional() <em>Is Optional</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIsOptional()
   * @generated
   * @ordered
   */
  protected Boolean isOptional = IS_OPTIONAL_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MethodPartImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MethodPackage.Literals.METHOD_PART;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getIntroduction()
  {
    return introduction;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIntroduction(String newIntroduction)
  {
    String oldIntroduction = introduction;
    introduction = newIntroduction;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.METHOD_PART__INTRODUCTION, oldIntroduction, introduction));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getConclusion()
  {
    return conclusion;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setConclusion(String newConclusion)
  {
    String oldConclusion = conclusion;
    conclusion = newConclusion;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.METHOD_PART__CONCLUSION, oldConclusion, conclusion));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Boolean getIsOptional()
  {
    return isOptional;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setIsOptional(Boolean newIsOptional)
  {
    Boolean oldIsOptional = isOptional;
    isOptional = newIsOptional;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.METHOD_PART__IS_OPTIONAL, oldIsOptional, isOptional));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MethodPackage.METHOD_PART__INTRODUCTION:
        return getIntroduction();
      case MethodPackage.METHOD_PART__CONCLUSION:
        return getConclusion();
      case MethodPackage.METHOD_PART__IS_OPTIONAL:
        return getIsOptional();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MethodPackage.METHOD_PART__INTRODUCTION:
        setIntroduction((String)newValue);
        return;
      case MethodPackage.METHOD_PART__CONCLUSION:
        setConclusion((String)newValue);
        return;
      case MethodPackage.METHOD_PART__IS_OPTIONAL:
        setIsOptional((Boolean)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.METHOD_PART__INTRODUCTION:
        setIntroduction(INTRODUCTION_EDEFAULT);
        return;
      case MethodPackage.METHOD_PART__CONCLUSION:
        setConclusion(CONCLUSION_EDEFAULT);
        return;
      case MethodPackage.METHOD_PART__IS_OPTIONAL:
        setIsOptional(IS_OPTIONAL_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.METHOD_PART__INTRODUCTION:
        return INTRODUCTION_EDEFAULT == null ? introduction != null : !INTRODUCTION_EDEFAULT.equals(introduction);
      case MethodPackage.METHOD_PART__CONCLUSION:
        return CONCLUSION_EDEFAULT == null ? conclusion != null : !CONCLUSION_EDEFAULT.equals(conclusion);
      case MethodPackage.METHOD_PART__IS_OPTIONAL:
        return IS_OPTIONAL_EDEFAULT == null ? isOptional != null : !IS_OPTIONAL_EDEFAULT.equals(isOptional);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (introduction: ");
    result.append(introduction);
    result.append(", conclusion: ");
    result.append(conclusion);
    result.append(", isOptional: ");
    result.append(isOptional);
    result.append(')');
    return result.toString();
  }

} //MethodPartImpl
