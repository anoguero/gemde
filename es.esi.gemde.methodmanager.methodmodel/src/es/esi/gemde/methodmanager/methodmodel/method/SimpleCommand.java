/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Simple Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.SimpleCommand#getReturnValueTo <em>Return Value To</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.SimpleCommand#getParameters <em>Parameters</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSimpleCommand()
 * @model abstract="true"
 * @generated
 */
public interface SimpleCommand extends MethodCommand
{
  /**
   * Returns the value of the '<em><b>Return Value To</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Return Value To</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Return Value To</em>' reference.
   * @see #setReturnValueTo(MethodVariable)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSimpleCommand_ReturnValueTo()
   * @model
   * @generated
   */
  MethodVariable getReturnValueTo();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.SimpleCommand#getReturnValueTo <em>Return Value To</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Return Value To</em>' reference.
   * @see #getReturnValueTo()
   * @generated
   */
  void setReturnValueTo(MethodVariable value);

  /**
   * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
   * The list contents are of type {@link es.esi.gemde.methodmanager.methodmodel.method.CommandParameter}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Parameters</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Parameters</em>' containment reference list.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getSimpleCommand_Parameters()
   * @model containment="true"
   *        annotation="gmf.compartment collapsible='true' layout='list'"
   * @generated
   */
  EList<CommandParameter> getParameters();

} // SimpleCommand
