/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.Task#getSubTasks <em>Sub Tasks</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.Task#getInitialSubtask <em>Initial Subtask</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.Task#getPrev <em>Prev</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.Task#getNext <em>Next</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getTask()
 * @model annotation="gmf.node figure='rounded' size='100,50' border.color='26,51,128' color='214,224,255' label='title' tool.name='Add Task'"
 * @generated
 */
public interface Task extends MethodStep, ExecutableElement
{
  /**
   * Returns the value of the '<em><b>Sub Tasks</b></em>' containment reference list.
   * The list contents are of type {@link es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Sub Tasks</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sub Tasks</em>' containment reference list.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getTask_SubTasks()
   * @model containment="true"
   *        annotation="gmf.compartment collapsible='true' layout='free'"
   * @generated
   */
  EList<AbstractSubTask> getSubTasks();

  /**
   * Returns the value of the '<em><b>Initial Subtask</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Initial Subtask</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Initial Subtask</em>' reference.
   * @see #setInitialSubtask(AbstractSubTask)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getTask_InitialSubtask()
   * @model
   * @generated
   */
  AbstractSubTask getInitialSubtask();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.Task#getInitialSubtask <em>Initial Subtask</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Initial Subtask</em>' reference.
   * @see #getInitialSubtask()
   * @generated
   */
  void setInitialSubtask(AbstractSubTask value);

  /**
   * Returns the value of the '<em><b>Prev</b></em>' reference.
   * It is bidirectional and its opposite is '{@link es.esi.gemde.methodmanager.methodmodel.method.Task#getNext <em>Next</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Prev</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Prev</em>' reference.
   * @see #setPrev(Task)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getTask_Prev()
   * @see es.esi.gemde.methodmanager.methodmodel.method.Task#getNext
   * @model opposite="next"
   * @generated
   */
  Task getPrev();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.Task#getPrev <em>Prev</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Prev</em>' reference.
   * @see #getPrev()
   * @generated
   */
  void setPrev(Task value);

  /**
   * Returns the value of the '<em><b>Next</b></em>' reference.
   * It is bidirectional and its opposite is '{@link es.esi.gemde.methodmanager.methodmodel.method.Task#getPrev <em>Prev</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Next</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Next</em>' reference.
   * @see #setNext(Task)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getTask_Next()
   * @see es.esi.gemde.methodmanager.methodmodel.method.Task#getPrev
   * @model opposite="prev"
   *        annotation="gmf.link target.decoration='arrow' color='26,51,128' tool.name='Next Task' tool.small.bundle='es.esi.gemde.methodmanager.methodmodel.edit' tool.small.path='icons/full/obj16/NextTaskTool.gif'"
   * @generated
   */
  Task getNext();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.Task#getNext <em>Next</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Next</em>' reference.
   * @see #getNext()
   * @generated
   */
  void setNext(Task value);

} // Task
