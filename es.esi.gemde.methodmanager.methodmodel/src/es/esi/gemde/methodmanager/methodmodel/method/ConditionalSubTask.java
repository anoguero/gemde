/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Conditional Sub Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.ConditionalSubTask#getVariable <em>Variable</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.ConditionalSubTask#getConditionalLinks <em>Conditional Links</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getConditionalSubTask()
 * @model annotation="gmf.node figure='rounded' size='100,50' border.color='122,163,204' color='204,230,255' label='title' tool.name='Add Conditional SubTask'"
 * @generated
 */
public interface ConditionalSubTask extends AbstractSubTask
{
  /**
   * Returns the value of the '<em><b>Variable</b></em>' reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Variable</em>' reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Variable</em>' reference.
   * @see #setVariable(MethodVariable)
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getConditionalSubTask_Variable()
   * @model required="true"
   * @generated
   */
  MethodVariable getVariable();

  /**
   * Sets the value of the '{@link es.esi.gemde.methodmanager.methodmodel.method.ConditionalSubTask#getVariable <em>Variable</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Variable</em>' reference.
   * @see #getVariable()
   * @generated
   */
  void setVariable(MethodVariable value);

  /**
   * Returns the value of the '<em><b>Conditional Links</b></em>' containment reference list.
   * The list contents are of type {@link es.esi.gemde.methodmanager.methodmodel.method.Condition}.
   * It is bidirectional and its opposite is '{@link es.esi.gemde.methodmanager.methodmodel.method.Condition#getFrom <em>From</em>}'.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Conditional Links</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Conditional Links</em>' containment reference list.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getConditionalSubTask_ConditionalLinks()
   * @see es.esi.gemde.methodmanager.methodmodel.method.Condition#getFrom
   * @model opposite="from" containment="true" required="true"
   * @generated
   */
  EList<Condition> getConditionalLinks();

} // ConditionalSubTask
