/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.impl;

import es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod;
import es.esi.gemde.methodmanager.methodmodel.method.MethodCommand;
import es.esi.gemde.methodmanager.methodmodel.method.MethodPackage;
import es.esi.gemde.methodmanager.methodmodel.method.MethodVariable;
import es.esi.gemde.methodmanager.methodmodel.method.Phase;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>GEMDE Method</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.GEMDEMethodImpl#getName <em>Name</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.GEMDEMethodImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.GEMDEMethodImpl#getProjectName <em>Project Name</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.GEMDEMethodImpl#getPhases <em>Phases</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.GEMDEMethodImpl#getInitialPhase <em>Initial Phase</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.GEMDEMethodImpl#getVariables <em>Variables</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.GEMDEMethodImpl#getCommands <em>Commands</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class GEMDEMethodImpl extends EObjectImpl implements GEMDEMethod
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDescription()
   * @generated
   * @ordered
   */
  protected static final String DESCRIPTION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDescription()
   * @generated
   * @ordered
   */
  protected String description = DESCRIPTION_EDEFAULT;

  /**
   * The default value of the '{@link #getProjectName() <em>Project Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProjectName()
   * @generated
   * @ordered
   */
  protected static final String PROJECT_NAME_EDEFAULT = "es.esi.gemde.example.methodology.cheatsheet";

  /**
   * The cached value of the '{@link #getProjectName() <em>Project Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProjectName()
   * @generated
   * @ordered
   */
  protected String projectName = PROJECT_NAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getPhases() <em>Phases</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPhases()
   * @generated
   * @ordered
   */
  protected EList<Phase> phases;

  /**
   * The cached value of the '{@link #getInitialPhase() <em>Initial Phase</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInitialPhase()
   * @generated
   * @ordered
   */
  protected Phase initialPhase;

  /**
   * The cached value of the '{@link #getVariables() <em>Variables</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVariables()
   * @generated
   * @ordered
   */
  protected EList<MethodVariable> variables;

  /**
   * The cached value of the '{@link #getCommands() <em>Commands</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCommands()
   * @generated
   * @ordered
   */
  protected EList<MethodCommand> commands;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected GEMDEMethodImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MethodPackage.Literals.GEMDE_METHOD;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.GEMDE_METHOD__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getDescription()
  {
    return description;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setDescription(String newDescription)
  {
    String oldDescription = description;
    description = newDescription;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.GEMDE_METHOD__DESCRIPTION, oldDescription, description));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getProjectName()
  {
    return projectName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setProjectName(String newProjectName)
  {
    String oldProjectName = projectName;
    projectName = newProjectName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.GEMDE_METHOD__PROJECT_NAME, oldProjectName, projectName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<Phase> getPhases()
  {
    if (phases == null)
    {
      phases = new EObjectContainmentEList<Phase>(Phase.class, this, MethodPackage.GEMDE_METHOD__PHASES);
    }
    return phases;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Phase getInitialPhase()
  {
    if (initialPhase != null && initialPhase.eIsProxy())
    {
      InternalEObject oldInitialPhase = (InternalEObject)initialPhase;
      initialPhase = (Phase)eResolveProxy(oldInitialPhase);
      if (initialPhase != oldInitialPhase)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.GEMDE_METHOD__INITIAL_PHASE, oldInitialPhase, initialPhase));
      }
    }
    return initialPhase;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Phase basicGetInitialPhase()
  {
    return initialPhase;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setInitialPhase(Phase newInitialPhase)
  {
    Phase oldInitialPhase = initialPhase;
    initialPhase = newInitialPhase;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.GEMDE_METHOD__INITIAL_PHASE, oldInitialPhase, initialPhase));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<MethodVariable> getVariables()
  {
    if (variables == null)
    {
      variables = new EObjectContainmentEList<MethodVariable>(MethodVariable.class, this, MethodPackage.GEMDE_METHOD__VARIABLES);
    }
    return variables;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<MethodCommand> getCommands()
  {
    if (commands == null)
    {
      commands = new EObjectContainmentEList<MethodCommand>(MethodCommand.class, this, MethodPackage.GEMDE_METHOD__COMMANDS);
    }
    return commands;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MethodPackage.GEMDE_METHOD__PHASES:
        return ((InternalEList<?>)getPhases()).basicRemove(otherEnd, msgs);
      case MethodPackage.GEMDE_METHOD__VARIABLES:
        return ((InternalEList<?>)getVariables()).basicRemove(otherEnd, msgs);
      case MethodPackage.GEMDE_METHOD__COMMANDS:
        return ((InternalEList<?>)getCommands()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MethodPackage.GEMDE_METHOD__NAME:
        return getName();
      case MethodPackage.GEMDE_METHOD__DESCRIPTION:
        return getDescription();
      case MethodPackage.GEMDE_METHOD__PROJECT_NAME:
        return getProjectName();
      case MethodPackage.GEMDE_METHOD__PHASES:
        return getPhases();
      case MethodPackage.GEMDE_METHOD__INITIAL_PHASE:
        if (resolve) return getInitialPhase();
        return basicGetInitialPhase();
      case MethodPackage.GEMDE_METHOD__VARIABLES:
        return getVariables();
      case MethodPackage.GEMDE_METHOD__COMMANDS:
        return getCommands();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MethodPackage.GEMDE_METHOD__NAME:
        setName((String)newValue);
        return;
      case MethodPackage.GEMDE_METHOD__DESCRIPTION:
        setDescription((String)newValue);
        return;
      case MethodPackage.GEMDE_METHOD__PROJECT_NAME:
        setProjectName((String)newValue);
        return;
      case MethodPackage.GEMDE_METHOD__PHASES:
        getPhases().clear();
        getPhases().addAll((Collection<? extends Phase>)newValue);
        return;
      case MethodPackage.GEMDE_METHOD__INITIAL_PHASE:
        setInitialPhase((Phase)newValue);
        return;
      case MethodPackage.GEMDE_METHOD__VARIABLES:
        getVariables().clear();
        getVariables().addAll((Collection<? extends MethodVariable>)newValue);
        return;
      case MethodPackage.GEMDE_METHOD__COMMANDS:
        getCommands().clear();
        getCommands().addAll((Collection<? extends MethodCommand>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.GEMDE_METHOD__NAME:
        setName(NAME_EDEFAULT);
        return;
      case MethodPackage.GEMDE_METHOD__DESCRIPTION:
        setDescription(DESCRIPTION_EDEFAULT);
        return;
      case MethodPackage.GEMDE_METHOD__PROJECT_NAME:
        setProjectName(PROJECT_NAME_EDEFAULT);
        return;
      case MethodPackage.GEMDE_METHOD__PHASES:
        getPhases().clear();
        return;
      case MethodPackage.GEMDE_METHOD__INITIAL_PHASE:
        setInitialPhase((Phase)null);
        return;
      case MethodPackage.GEMDE_METHOD__VARIABLES:
        getVariables().clear();
        return;
      case MethodPackage.GEMDE_METHOD__COMMANDS:
        getCommands().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.GEMDE_METHOD__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case MethodPackage.GEMDE_METHOD__DESCRIPTION:
        return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
      case MethodPackage.GEMDE_METHOD__PROJECT_NAME:
        return PROJECT_NAME_EDEFAULT == null ? projectName != null : !PROJECT_NAME_EDEFAULT.equals(projectName);
      case MethodPackage.GEMDE_METHOD__PHASES:
        return phases != null && !phases.isEmpty();
      case MethodPackage.GEMDE_METHOD__INITIAL_PHASE:
        return initialPhase != null;
      case MethodPackage.GEMDE_METHOD__VARIABLES:
        return variables != null && !variables.isEmpty();
      case MethodPackage.GEMDE_METHOD__COMMANDS:
        return commands != null && !commands.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(", description: ");
    result.append(description);
    result.append(", projectName: ");
    result.append(projectName);
    result.append(')');
    return result.toString();
  }

} //GEMDEMethodImpl
