/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.util;

import es.esi.gemde.methodmanager.methodmodel.method.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage
 * @generated
 */
public class MethodAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static MethodPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = MethodPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected MethodSwitch<Adapter> modelSwitch =
    new MethodSwitch<Adapter>()
    {
      @Override
      public Adapter caseGEMDEMethod(GEMDEMethod object)
      {
        return createGEMDEMethodAdapter();
      }
      @Override
      public Adapter caseMethodStep(MethodStep object)
      {
        return createMethodStepAdapter();
      }
      @Override
      public Adapter caseMethodPart(MethodPart object)
      {
        return createMethodPartAdapter();
      }
      @Override
      public Adapter caseExecutableElement(ExecutableElement object)
      {
        return createExecutableElementAdapter();
      }
      @Override
      public Adapter caseDataStorageElement(DataStorageElement object)
      {
        return createDataStorageElementAdapter();
      }
      @Override
      public Adapter caseMethodVariable(MethodVariable object)
      {
        return createMethodVariableAdapter();
      }
      @Override
      public Adapter caseInternalVariable(InternalVariable object)
      {
        return createInternalVariableAdapter();
      }
      @Override
      public Adapter casePhase(Phase object)
      {
        return createPhaseAdapter();
      }
      @Override
      public Adapter caseTask(Task object)
      {
        return createTaskAdapter();
      }
      @Override
      public Adapter caseAbstractSubTask(AbstractSubTask object)
      {
        return createAbstractSubTaskAdapter();
      }
      @Override
      public Adapter caseSubTask(SubTask object)
      {
        return createSubTaskAdapter();
      }
      @Override
      public Adapter caseCondition(Condition object)
      {
        return createConditionAdapter();
      }
      @Override
      public Adapter caseConditionalSubTask(ConditionalSubTask object)
      {
        return createConditionalSubTaskAdapter();
      }
      @Override
      public Adapter caseRepeatedSubTask(RepeatedSubTask object)
      {
        return createRepeatedSubTaskAdapter();
      }
      @Override
      public Adapter caseCommandParameter(CommandParameter object)
      {
        return createCommandParameterAdapter();
      }
      @Override
      public Adapter caseVariableCommandParameter(VariableCommandParameter object)
      {
        return createVariableCommandParameterAdapter();
      }
      @Override
      public Adapter caseConstantCommandParameter(ConstantCommandParameter object)
      {
        return createConstantCommandParameterAdapter();
      }
      @Override
      public Adapter caseMethodCommand(MethodCommand object)
      {
        return createMethodCommandAdapter();
      }
      @Override
      public Adapter caseComplexCommand(ComplexCommand object)
      {
        return createComplexCommandAdapter();
      }
      @Override
      public Adapter caseSimpleCommand(SimpleCommand object)
      {
        return createSimpleCommandAdapter();
      }
      @Override
      public Adapter caseCustomCommand(CustomCommand object)
      {
        return createCustomCommandAdapter();
      }
      @Override
      public Adapter caseCallWizardCommand(CallWizardCommand object)
      {
        return createCallWizardCommandAdapter();
      }
      @Override
      public Adapter caseSelectFileCommand(SelectFileCommand object)
      {
        return createSelectFileCommandAdapter();
      }
      @Override
      public Adapter caseSelectFolderCommand(SelectFolderCommand object)
      {
        return createSelectFolderCommandAdapter();
      }
      @Override
      public Adapter caseSelectMetamodelCommand(SelectMetamodelCommand object)
      {
        return createSelectMetamodelCommandAdapter();
      }
      @Override
      public Adapter caseSelectModelElementFromFileCommand(SelectModelElementFromFileCommand object)
      {
        return createSelectModelElementFromFileCommandAdapter();
      }
      @Override
      public Adapter caseSelectModelElementFromEObjectCommand(SelectModelElementFromEObjectCommand object)
      {
        return createSelectModelElementFromEObjectCommandAdapter();
      }
      @Override
      public Adapter caseSelectValidationCommand(SelectValidationCommand object)
      {
        return createSelectValidationCommandAdapter();
      }
      @Override
      public Adapter caseSelectTransformationCommand(SelectTransformationCommand object)
      {
        return createSelectTransformationCommandAdapter();
      }
      @Override
      public Adapter caseLaunchValidationCommand(LaunchValidationCommand object)
      {
        return createLaunchValidationCommandAdapter();
      }
      @Override
      public Adapter caseLaunchTransformationCommand(LaunchTransformationCommand object)
      {
        return createLaunchTransformationCommandAdapter();
      }
      @Override
      public Adapter caseOpenEditorCommand(OpenEditorCommand object)
      {
        return createOpenEditorCommandAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod <em>GEMDE Method</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod
   * @generated
   */
  public Adapter createGEMDEMethodAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.MethodStep <em>Step</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodStep
   * @generated
   */
  public Adapter createMethodStepAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.MethodPart <em>Part</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPart
   * @generated
   */
  public Adapter createMethodPartAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.ExecutableElement <em>Executable Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.ExecutableElement
   * @generated
   */
  public Adapter createExecutableElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.DataStorageElement <em>Data Storage Element</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.DataStorageElement
   * @generated
   */
  public Adapter createDataStorageElementAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.MethodVariable <em>Variable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodVariable
   * @generated
   */
  public Adapter createMethodVariableAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.InternalVariable <em>Internal Variable</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.InternalVariable
   * @generated
   */
  public Adapter createInternalVariableAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.Phase <em>Phase</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.Phase
   * @generated
   */
  public Adapter createPhaseAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.Task <em>Task</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.Task
   * @generated
   */
  public Adapter createTaskAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask <em>Abstract Sub Task</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask
   * @generated
   */
  public Adapter createAbstractSubTaskAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.SubTask <em>Sub Task</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SubTask
   * @generated
   */
  public Adapter createSubTaskAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.Condition <em>Condition</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.Condition
   * @generated
   */
  public Adapter createConditionAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.ConditionalSubTask <em>Conditional Sub Task</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.ConditionalSubTask
   * @generated
   */
  public Adapter createConditionalSubTaskAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.RepeatedSubTask <em>Repeated Sub Task</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.RepeatedSubTask
   * @generated
   */
  public Adapter createRepeatedSubTaskAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.CommandParameter <em>Command Parameter</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.CommandParameter
   * @generated
   */
  public Adapter createCommandParameterAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.VariableCommandParameter <em>Variable Command Parameter</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.VariableCommandParameter
   * @generated
   */
  public Adapter createVariableCommandParameterAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.ConstantCommandParameter <em>Constant Command Parameter</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.ConstantCommandParameter
   * @generated
   */
  public Adapter createConstantCommandParameterAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.MethodCommand <em>Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.MethodCommand
   * @generated
   */
  public Adapter createMethodCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.ComplexCommand <em>Complex Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.ComplexCommand
   * @generated
   */
  public Adapter createComplexCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.SimpleCommand <em>Simple Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SimpleCommand
   * @generated
   */
  public Adapter createSimpleCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.CustomCommand <em>Custom Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.CustomCommand
   * @generated
   */
  public Adapter createCustomCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.CallWizardCommand <em>Call Wizard Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.CallWizardCommand
   * @generated
   */
  public Adapter createCallWizardCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectFileCommand <em>Select File Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectFileCommand
   * @generated
   */
  public Adapter createSelectFileCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectFolderCommand <em>Select Folder Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectFolderCommand
   * @generated
   */
  public Adapter createSelectFolderCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectMetamodelCommand <em>Select Metamodel Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectMetamodelCommand
   * @generated
   */
  public Adapter createSelectMetamodelCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand <em>Select Model Element From File Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand
   * @generated
   */
  public Adapter createSelectModelElementFromFileCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand <em>Select Model Element From EObject Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand
   * @generated
   */
  public Adapter createSelectModelElementFromEObjectCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectValidationCommand <em>Select Validation Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectValidationCommand
   * @generated
   */
  public Adapter createSelectValidationCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.SelectTransformationCommand <em>Select Transformation Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.SelectTransformationCommand
   * @generated
   */
  public Adapter createSelectTransformationCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand <em>Launch Validation Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand
   * @generated
   */
  public Adapter createLaunchValidationCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand <em>Launch Transformation Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand
   * @generated
   */
  public Adapter createLaunchTransformationCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link es.esi.gemde.methodmanager.methodmodel.method.OpenEditorCommand <em>Open Editor Command</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see es.esi.gemde.methodmanager.methodmodel.method.OpenEditorCommand
   * @generated
   */
  public Adapter createOpenEditorCommandAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //MethodAdapterFactory
