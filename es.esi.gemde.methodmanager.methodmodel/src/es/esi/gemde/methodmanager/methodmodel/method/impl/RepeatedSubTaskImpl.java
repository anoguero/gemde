/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.impl;

import es.esi.gemde.methodmanager.methodmodel.method.InternalVariable;
import es.esi.gemde.methodmanager.methodmodel.method.MethodPackage;
import es.esi.gemde.methodmanager.methodmodel.method.RepeatedSubTask;
import es.esi.gemde.methodmanager.methodmodel.method.SubTask;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Repeated Sub Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.RepeatedSubTaskImpl#getRepeatedElement <em>Repeated Element</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.RepeatedSubTaskImpl#getRepetitionVector <em>Repetition Vector</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.RepeatedSubTaskImpl#getRepetitionVariable <em>Repetition Variable</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RepeatedSubTaskImpl extends AbstractSubTaskImpl implements RepeatedSubTask
{
  /**
   * The cached value of the '{@link #getRepeatedElement() <em>Repeated Element</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRepeatedElement()
   * @generated
   * @ordered
   */
  protected SubTask repeatedElement;

  /**
   * The cached value of the '{@link #getRepetitionVector() <em>Repetition Vector</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRepetitionVector()
   * @generated
   * @ordered
   */
  protected EList<String> repetitionVector;

  /**
   * The cached value of the '{@link #getRepetitionVariable() <em>Repetition Variable</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRepetitionVariable()
   * @generated
   * @ordered
   */
  protected InternalVariable repetitionVariable;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RepeatedSubTaskImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MethodPackage.Literals.REPEATED_SUB_TASK;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SubTask getRepeatedElement()
  {
    if (repeatedElement != null && repeatedElement.eIsProxy())
    {
      InternalEObject oldRepeatedElement = (InternalEObject)repeatedElement;
      repeatedElement = (SubTask)eResolveProxy(oldRepeatedElement);
      if (repeatedElement != oldRepeatedElement)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.REPEATED_SUB_TASK__REPEATED_ELEMENT, oldRepeatedElement, repeatedElement));
      }
    }
    return repeatedElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SubTask basicGetRepeatedElement()
  {
    return repeatedElement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRepeatedElement(SubTask newRepeatedElement)
  {
    SubTask oldRepeatedElement = repeatedElement;
    repeatedElement = newRepeatedElement;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.REPEATED_SUB_TASK__REPEATED_ELEMENT, oldRepeatedElement, repeatedElement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<String> getRepetitionVector()
  {
    if (repetitionVector == null)
    {
      repetitionVector = new EDataTypeEList<String>(String.class, this, MethodPackage.REPEATED_SUB_TASK__REPETITION_VECTOR);
    }
    return repetitionVector;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InternalVariable getRepetitionVariable()
  {
    return repetitionVariable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRepetitionVariable(InternalVariable newRepetitionVariable, NotificationChain msgs)
  {
    InternalVariable oldRepetitionVariable = repetitionVariable;
    repetitionVariable = newRepetitionVariable;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MethodPackage.REPEATED_SUB_TASK__REPETITION_VARIABLE, oldRepetitionVariable, newRepetitionVariable);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRepetitionVariable(InternalVariable newRepetitionVariable)
  {
    if (newRepetitionVariable != repetitionVariable)
    {
      NotificationChain msgs = null;
      if (repetitionVariable != null)
        msgs = ((InternalEObject)repetitionVariable).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MethodPackage.REPEATED_SUB_TASK__REPETITION_VARIABLE, null, msgs);
      if (newRepetitionVariable != null)
        msgs = ((InternalEObject)newRepetitionVariable).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MethodPackage.REPEATED_SUB_TASK__REPETITION_VARIABLE, null, msgs);
      msgs = basicSetRepetitionVariable(newRepetitionVariable, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.REPEATED_SUB_TASK__REPETITION_VARIABLE, newRepetitionVariable, newRepetitionVariable));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MethodPackage.REPEATED_SUB_TASK__REPETITION_VARIABLE:
        return basicSetRepetitionVariable(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MethodPackage.REPEATED_SUB_TASK__REPEATED_ELEMENT:
        if (resolve) return getRepeatedElement();
        return basicGetRepeatedElement();
      case MethodPackage.REPEATED_SUB_TASK__REPETITION_VECTOR:
        return getRepetitionVector();
      case MethodPackage.REPEATED_SUB_TASK__REPETITION_VARIABLE:
        return getRepetitionVariable();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MethodPackage.REPEATED_SUB_TASK__REPEATED_ELEMENT:
        setRepeatedElement((SubTask)newValue);
        return;
      case MethodPackage.REPEATED_SUB_TASK__REPETITION_VECTOR:
        getRepetitionVector().clear();
        getRepetitionVector().addAll((Collection<? extends String>)newValue);
        return;
      case MethodPackage.REPEATED_SUB_TASK__REPETITION_VARIABLE:
        setRepetitionVariable((InternalVariable)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.REPEATED_SUB_TASK__REPEATED_ELEMENT:
        setRepeatedElement((SubTask)null);
        return;
      case MethodPackage.REPEATED_SUB_TASK__REPETITION_VECTOR:
        getRepetitionVector().clear();
        return;
      case MethodPackage.REPEATED_SUB_TASK__REPETITION_VARIABLE:
        setRepetitionVariable((InternalVariable)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.REPEATED_SUB_TASK__REPEATED_ELEMENT:
        return repeatedElement != null;
      case MethodPackage.REPEATED_SUB_TASK__REPETITION_VECTOR:
        return repetitionVector != null && !repetitionVector.isEmpty();
      case MethodPackage.REPEATED_SUB_TASK__REPETITION_VARIABLE:
        return repetitionVariable != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (repetitionVector: ");
    result.append(repetitionVector);
    result.append(')');
    return result.toString();
  }

} //RepeatedSubTaskImpl
