/**
 */
package es.esi.gemde.methodmanager.methodmodel.method;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Storage Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see es.esi.gemde.methodmanager.methodmodel.method.MethodPackage#getDataStorageElement()
 * @model abstract="true"
 * @generated
 */
public interface DataStorageElement extends EObject
{
} // DataStorageElement
