/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.impl;

import es.esi.gemde.methodmanager.methodmodel.method.CommandParameter;
import es.esi.gemde.methodmanager.methodmodel.method.MethodPackage;
import es.esi.gemde.methodmanager.methodmodel.method.MethodVariable;
import es.esi.gemde.methodmanager.methodmodel.method.SimpleCommand;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Simple Command</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SimpleCommandImpl#getReturnValueTo <em>Return Value To</em>}</li>
 *   <li>{@link es.esi.gemde.methodmanager.methodmodel.method.impl.SimpleCommandImpl#getParameters <em>Parameters</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class SimpleCommandImpl extends MethodCommandImpl implements SimpleCommand
{
  /**
   * The cached value of the '{@link #getReturnValueTo() <em>Return Value To</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReturnValueTo()
   * @generated
   * @ordered
   */
  protected MethodVariable returnValueTo;

  /**
   * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getParameters()
   * @generated
   * @ordered
   */
  protected EList<CommandParameter> parameters;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SimpleCommandImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MethodPackage.Literals.SIMPLE_COMMAND;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodVariable getReturnValueTo()
  {
    if (returnValueTo != null && returnValueTo.eIsProxy())
    {
      InternalEObject oldReturnValueTo = (InternalEObject)returnValueTo;
      returnValueTo = (MethodVariable)eResolveProxy(oldReturnValueTo);
      if (returnValueTo != oldReturnValueTo)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, MethodPackage.SIMPLE_COMMAND__RETURN_VALUE_TO, oldReturnValueTo, returnValueTo));
      }
    }
    return returnValueTo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodVariable basicGetReturnValueTo()
  {
    return returnValueTo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setReturnValueTo(MethodVariable newReturnValueTo)
  {
    MethodVariable oldReturnValueTo = returnValueTo;
    returnValueTo = newReturnValueTo;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MethodPackage.SIMPLE_COMMAND__RETURN_VALUE_TO, oldReturnValueTo, returnValueTo));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<CommandParameter> getParameters()
  {
    if (parameters == null)
    {
      parameters = new EObjectContainmentEList<CommandParameter>(CommandParameter.class, this, MethodPackage.SIMPLE_COMMAND__PARAMETERS);
    }
    return parameters;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MethodPackage.SIMPLE_COMMAND__PARAMETERS:
        return ((InternalEList<?>)getParameters()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MethodPackage.SIMPLE_COMMAND__RETURN_VALUE_TO:
        if (resolve) return getReturnValueTo();
        return basicGetReturnValueTo();
      case MethodPackage.SIMPLE_COMMAND__PARAMETERS:
        return getParameters();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MethodPackage.SIMPLE_COMMAND__RETURN_VALUE_TO:
        setReturnValueTo((MethodVariable)newValue);
        return;
      case MethodPackage.SIMPLE_COMMAND__PARAMETERS:
        getParameters().clear();
        getParameters().addAll((Collection<? extends CommandParameter>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.SIMPLE_COMMAND__RETURN_VALUE_TO:
        setReturnValueTo((MethodVariable)null);
        return;
      case MethodPackage.SIMPLE_COMMAND__PARAMETERS:
        getParameters().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MethodPackage.SIMPLE_COMMAND__RETURN_VALUE_TO:
        return returnValueTo != null;
      case MethodPackage.SIMPLE_COMMAND__PARAMETERS:
        return parameters != null && !parameters.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //SimpleCommandImpl
