/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.impl;

import es.esi.gemde.methodmanager.methodmodel.method.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MethodFactoryImpl extends EFactoryImpl implements MethodFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static MethodFactory init()
  {
    try
    {
      MethodFactory theMethodFactory = (MethodFactory)EPackage.Registry.INSTANCE.getEFactory(MethodPackage.eNS_URI);
      if (theMethodFactory != null)
      {
        return theMethodFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new MethodFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case MethodPackage.GEMDE_METHOD: return createGEMDEMethod();
      case MethodPackage.METHOD_VARIABLE: return createMethodVariable();
      case MethodPackage.INTERNAL_VARIABLE: return createInternalVariable();
      case MethodPackage.PHASE: return createPhase();
      case MethodPackage.TASK: return createTask();
      case MethodPackage.SUB_TASK: return createSubTask();
      case MethodPackage.CONDITION: return createCondition();
      case MethodPackage.CONDITIONAL_SUB_TASK: return createConditionalSubTask();
      case MethodPackage.REPEATED_SUB_TASK: return createRepeatedSubTask();
      case MethodPackage.VARIABLE_COMMAND_PARAMETER: return createVariableCommandParameter();
      case MethodPackage.CONSTANT_COMMAND_PARAMETER: return createConstantCommandParameter();
      case MethodPackage.COMPLEX_COMMAND: return createComplexCommand();
      case MethodPackage.CUSTOM_COMMAND: return createCustomCommand();
      case MethodPackage.CALL_WIZARD_COMMAND: return createCallWizardCommand();
      case MethodPackage.SELECT_FILE_COMMAND: return createSelectFileCommand();
      case MethodPackage.SELECT_FOLDER_COMMAND: return createSelectFolderCommand();
      case MethodPackage.SELECT_METAMODEL_COMMAND: return createSelectMetamodelCommand();
      case MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND: return createSelectModelElementFromFileCommand();
      case MethodPackage.SELECT_MODEL_ELEMENT_FROM_EOBJECT_COMMAND: return createSelectModelElementFromEObjectCommand();
      case MethodPackage.SELECT_VALIDATION_COMMAND: return createSelectValidationCommand();
      case MethodPackage.SELECT_TRANSFORMATION_COMMAND: return createSelectTransformationCommand();
      case MethodPackage.LAUNCH_VALIDATION_COMMAND: return createLaunchValidationCommand();
      case MethodPackage.LAUNCH_TRANSFORMATION_COMMAND: return createLaunchTransformationCommand();
      case MethodPackage.OPEN_EDITOR_COMMAND: return createOpenEditorCommand();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GEMDEMethod createGEMDEMethod()
  {
    GEMDEMethodImpl gemdeMethod = new GEMDEMethodImpl();
    return gemdeMethod;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodVariable createMethodVariable()
  {
    MethodVariableImpl methodVariable = new MethodVariableImpl();
    return methodVariable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public InternalVariable createInternalVariable()
  {
    InternalVariableImpl internalVariable = new InternalVariableImpl();
    return internalVariable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Phase createPhase()
  {
    PhaseImpl phase = new PhaseImpl();
    return phase;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Task createTask()
  {
    TaskImpl task = new TaskImpl();
    return task;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SubTask createSubTask()
  {
    SubTaskImpl subTask = new SubTaskImpl();
    return subTask;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Condition createCondition()
  {
    ConditionImpl condition = new ConditionImpl();
    return condition;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConditionalSubTask createConditionalSubTask()
  {
    ConditionalSubTaskImpl conditionalSubTask = new ConditionalSubTaskImpl();
    return conditionalSubTask;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RepeatedSubTask createRepeatedSubTask()
  {
    RepeatedSubTaskImpl repeatedSubTask = new RepeatedSubTaskImpl();
    return repeatedSubTask;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VariableCommandParameter createVariableCommandParameter()
  {
    VariableCommandParameterImpl variableCommandParameter = new VariableCommandParameterImpl();
    return variableCommandParameter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ConstantCommandParameter createConstantCommandParameter()
  {
    ConstantCommandParameterImpl constantCommandParameter = new ConstantCommandParameterImpl();
    return constantCommandParameter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ComplexCommand createComplexCommand()
  {
    ComplexCommandImpl complexCommand = new ComplexCommandImpl();
    return complexCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CustomCommand createCustomCommand()
  {
    CustomCommandImpl customCommand = new CustomCommandImpl();
    return customCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CallWizardCommand createCallWizardCommand()
  {
    CallWizardCommandImpl callWizardCommand = new CallWizardCommandImpl();
    return callWizardCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SelectFileCommand createSelectFileCommand()
  {
    SelectFileCommandImpl selectFileCommand = new SelectFileCommandImpl();
    return selectFileCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SelectFolderCommand createSelectFolderCommand()
  {
    SelectFolderCommandImpl selectFolderCommand = new SelectFolderCommandImpl();
    return selectFolderCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SelectMetamodelCommand createSelectMetamodelCommand()
  {
    SelectMetamodelCommandImpl selectMetamodelCommand = new SelectMetamodelCommandImpl();
    return selectMetamodelCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SelectModelElementFromFileCommand createSelectModelElementFromFileCommand()
  {
    SelectModelElementFromFileCommandImpl selectModelElementFromFileCommand = new SelectModelElementFromFileCommandImpl();
    return selectModelElementFromFileCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SelectModelElementFromEObjectCommand createSelectModelElementFromEObjectCommand()
  {
    SelectModelElementFromEObjectCommandImpl selectModelElementFromEObjectCommand = new SelectModelElementFromEObjectCommandImpl();
    return selectModelElementFromEObjectCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SelectValidationCommand createSelectValidationCommand()
  {
    SelectValidationCommandImpl selectValidationCommand = new SelectValidationCommandImpl();
    return selectValidationCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SelectTransformationCommand createSelectTransformationCommand()
  {
    SelectTransformationCommandImpl selectTransformationCommand = new SelectTransformationCommandImpl();
    return selectTransformationCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LaunchValidationCommand createLaunchValidationCommand()
  {
    LaunchValidationCommandImpl launchValidationCommand = new LaunchValidationCommandImpl();
    return launchValidationCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public LaunchTransformationCommand createLaunchTransformationCommand()
  {
    LaunchTransformationCommandImpl launchTransformationCommand = new LaunchTransformationCommandImpl();
    return launchTransformationCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public OpenEditorCommand createOpenEditorCommand()
  {
    OpenEditorCommandImpl openEditorCommand = new OpenEditorCommandImpl();
    return openEditorCommand;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MethodPackage getMethodPackage()
  {
    return (MethodPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static MethodPackage getPackage()
  {
    return MethodPackage.eINSTANCE;
  }

} //MethodFactoryImpl
