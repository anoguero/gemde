@namespace(uri="http://es.esi.gemde/methodmanager/methodmodel", prefix="method")
@emf.gen(basePackage="es.esi.gemde.methodmanager.methodmodel")
@gmf
package method;

@gmf.diagram(model.extension="method", diagram.extension="methoddi")
class GEMDEMethod {
   attr String [1] name;
   attr String description;
   attr String [1] projectName = "es.esi.gemde.example.methodology.cheatsheet";
   val Phase [+] phases;
   ref Phase [1] initialPhase;
   val MethodVariable [*] variables;
   val MethodCommand [*] commands;
}

abstract class MethodStep {
   attr String [1] title;
   attr String description;
}

abstract class MethodPart extends MethodStep {
   attr String introduction;
   attr String conclusion;
   attr Boolean isOptional = false;
}

abstract class ExecutableElement {
   ref MethodCommand command;
}

abstract class DataStorageElement {}

@gmf.node(figure="rectangle", size="75,40", border.color="0,0,0", color="255,255,255", label="name,type", label.view.pattern="{0}:{1}", label.edit.pattern="{0}:{1}" , tool.name="Add Variable")
class MethodVariable extends DataStorageElement {
   id attr String [1] identifier;
   attr String [1] name;
   attr String [1] type;
}

@gmf.node(figure="rectangle", size="75,40", border.color="0,0,0", color="255,255,255", label="name", tool.name="Add Internal Variable")
class InternalVariable extends DataStorageElement {
   attr String [1] name;
}

@gmf.node(figure="rounded", size="500,300", border.color="0,0,50", color="153,153,194", label="title", tool.name="Add Method Phase")
class Phase extends MethodPart {
   @gmf.compartment(collapsible="false", layout="free")
   val Task [+] tasks;
   ref Task [1] initialTask;
   ref Phase#next prev;
   @gmf.link(target.decoration="arrow", color="0,0,50", tool.name="Next Phase", tool.small.bundle="es.esi.gemde.methodmanager.methodmodel.edit", tool.small.path="icons/full/obj16/NextPhaseTool.gif")
   ref Phase#prev next;
}

@gmf.node(figure="rounded", size="100,50", border.color="26,51,128", color="214,224,255", label="title", tool.name="Add Task")
class Task extends MethodStep, ExecutableElement {
   @gmf.compartment(collapsible="true", layout="free")
   val AbstractSubTask [*] subTasks;
   ref AbstractSubTask initialSubtask;
   ref Task#next prev;
   @gmf.link(target.decoration="arrow", color="26,51,128", tool.name="Next Task", tool.small.bundle="es.esi.gemde.methodmanager.methodmodel.edit", tool.small.path="icons/full/obj16/NextTaskTool.gif")
   ref Task#prev next;
}

abstract class AbstractSubTask extends MethodStep {
   ref AbstractSubTask#next prev;
   @gmf.link(target.decoration="arrow", color="122,163,204", tool.name="Next SubTask", tool.small.bundle="es.esi.gemde.methodmanager.methodmodel.edit", tool.small.path="icons/full/obj16/NextSubTaskTool.gif")
   ref AbstractSubTask#prev next;
}

@gmf.node(figure="rounded", size="100,50", border.color="122,163,204", color="204,230,255", label="title", tool.name="Add SubTask")
class SubTask extends AbstractSubTask, ExecutableElement {
   
}

@gmf.link(source="from", target="to", label="value", source.decoration="filledrhomb", color="122,163,204", tool.name="Conditional SubTask", tool.small.bundle="es.esi.gemde.methodmanager.methodmodel.edit", tool.small.path="icons/full/obj16/ConditionLinkTool.gif")
class Condition {
    attr String [1] value;
	ref ConditionalSubTask [1]#conditionalLinks from;
	ref SubTask [1] to;
}

@gmf.node(figure="rounded", size="100,50", border.color="122,163,204", color="204,230,255", label="title", tool.name="Add Conditional SubTask")
class ConditionalSubTask extends AbstractSubTask {
	ref MethodVariable [1] variable;
	val Condition [+]#from conditionalLinks;
}

@gmf.node(figure="rounded", size="100,50", border.color="122,163,204", color="204,230,255", label="title,repetitionVector", label.pattern="{0} {1}", label.readOnly="true", tool.name="Add Repeated SubTask")
class RepeatedSubTask extends AbstractSubTask {
	@gmf.link(style="dot", width="2", color="122,163,204", tool.name="Repeated SubTask", tool.small.bundle="es.esi.gemde.methodmanager.methodmodel.edit", tool.small.path="icons/full/obj16/RepeatedSubTaskTool.gif")
	ref SubTask [1] repeatedElement;
	!unique attr String [*] repetitionVector;
	val InternalVariable [1] repetitionVariable;
}

abstract class CommandParameter {
   attr String [1] identifier;
}

// border.color="102,76,0""
@gmf.node(figure="rectangle", border.color="230,204,128", color="230,204,128", label="identifier", tool.name="Add Variable Command Parameter")
class VariableCommandParameter extends CommandParameter {
   ref DataStorageElement [1] value;
}

@gmf.node(figure="rectangle", border.color="230,204,128", color="230,204,128", label="identifier", tool.name="Add Constant Command Parameter")
class ConstantCommandParameter extends CommandParameter {
   attr String [1] value;
}

abstract class MethodCommand {
   attr String [1] name;
}

// figure="polygon", polygon.x="7 0 14 0 14 7 44 29 37", polygon.y="0 15 15 30 30 45 15 15 0",
@gmf.node(figure="rectangle", border.color="102,76,0", color="230,204,128", label="name", tool.name="Add Complex Command")
class ComplexCommand extends MethodCommand {
	readonly attr String [1] identifier = "es.esi.gemde.methodmanager.commands.complexcommand";
	@gmf.link(source.decoration="filledrhomb", color="102,76,0", tool.name="Composing Command", tool.small.bundle="es.esi.gemde.methodmanager.methodmodel.edit", tool.small.path="icons/full/obj16/ComposingCommandTool.gif")
	ref SimpleCommand [*] commands;
}

abstract class SimpleCommand extends MethodCommand {
   ref MethodVariable returnValueTo;
   @gmf.compartment(collapsible="true",layout="list")
   val CommandParameter [*] parameters; 
}

@gmf.node(figure="rectangle", border.color="102,76,0", color="230,204,128", label="name", tool.name="Add Custom Command")
class CustomCommand extends SimpleCommand {
   attr String [1] identifier;
}

@gmf.node(figure="rectangle", border.color="102,76,0", color="230,204,128", label="name", tool.name="Add Call Wizard Command")
class CallWizardCommand extends SimpleCommand {
   readonly attr String [1] identifier = "es.esi.gemde.methodmanager.commands.callwizardcommand";
   ref CommandParameter [1] wizard;
}

@gmf.node(figure="rectangle", border.color="102,76,0", color="230,204,128", label="name", tool.name="Add Select File")
class SelectFileCommand extends SimpleCommand {
	readonly attr String [1] identifier = "es.esi.gemde.methodmanager.commands.selectfilecommand";
	ref CommandParameter [1] onlyWorkspace;
}

@gmf.node(figure="rectangle", border.color="102,76,0", color="230,204,128", label="name", tool.name="Add Select Folder Command")
class SelectFolderCommand extends SimpleCommand {
	ref CommandParameter [1] onlyWorkspace;
	readonly attr String [1] identifier = "es.esi.gemde.methodmanager.commands.selectfoldercommand";
}

@gmf.node(figure="rectangle", border.color="102,76,0", color="230,204,128", label="name", tool.name="Add Select Metamodel")
class SelectMetamodelCommand extends SimpleCommand {
	readonly attr String [1] identifier = "es.esi.gemde.methodmanager.commands.selectmetamodelcommand";
}

@gmf.node(figure="rectangle", border.color="102,76,0", color="230,204,128", label="name", tool.name="Add Select EObject from File Command")
class SelectModelElementFromFileCommand extends SimpleCommand {
	ref CommandParameter [1] modelFile;
	ref CommandParameter addToList;
	ref CommandParameter list;
	readonly attr String [1] identifier = "es.esi.gemde.methodmanager.commands.selectelemfromfilecommand";
}

@gmf.node(figure="rectangle", border.color="102,76,0", color="230,204,128", label="name", tool.name="Add Select EObject from Object Command")
class SelectModelElementFromEObjectCommand extends SimpleCommand {
	ref CommandParameter [1] eObject;
	ref CommandParameter addToList;
	ref CommandParameter list;
	readonly attr String [1] identifier = "es.esi.gemde.methodmanager.commands.selectelemfromeobjectcommand";
}

@gmf.node(figure="rectangle", border.color="102,76,0", color="230,204,128", label="name", tool.name="Add Select Validation Command")
class SelectValidationCommand extends SimpleCommand {
	readonly attr String [1] identifier = "es.esi.gemde.methodmanager.commands.selectvalidationcommand";
}

@gmf.node(figure="rectangle", border.color="102,76,0", color="230,204,128", label="name", tool.name="Add Select Transformation Command")
class SelectTransformationCommand extends SimpleCommand {
	readonly attr String [1] identifier = "es.esi.gemde.methodmanager.commands.selecttransformationcommand";
}

@gmf.node(figure="rectangle", border.color="102,76,0", color="230,204,128", label="name", tool.name="Add Launch Validation Command")
class LaunchValidationCommand extends SimpleCommand {
   readonly attr String [1] identifier = "es.esi.gemde.methodmanager.commands.launchvalidationcommand";
   ref CommandParameter [1] validation;
   ref CommandParameter eobject;
   ref CommandParameter modelFile;

}

@gmf.node(figure="rectangle", border.color="102,76,0", color="230,204,128", label="name", tool.name="Add Launch Transformation Command")
class LaunchTransformationCommand extends SimpleCommand {
   readonly attr String [1] identifier = "es.esi.gemde.methodmanager.commands.launchtransformationcommand";
   ref CommandParameter [1] transformation;
   ref CommandParameter [1] inputs;
   ref CommandParameter outputs;
   ref CommandParameter outputPath;

}

@gmf.node(figure="rectangle", border.color="102,76,0", color="230,204,128", label="name", tool.name="Add Open Editor Command")
class OpenEditorCommand extends SimpleCommand {
   ref CommandParameter [1] editorID;
   ref CommandParameter openedFile;
   readonly attr String [1] identifier = "es.esi.gemde.methodmanager.commands.openeditorcommand";
}