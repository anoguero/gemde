/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult.provider;


import es.esi.gemde.vv.mast.mastresult.ResultFactory;
import es.esi.gemde.vv.mast.mastresult.ResultPackage;
import es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class Scheduling_Server_ResultsItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scheduling_Server_ResultsItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Scheduling_Server_Results_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Scheduling_Server_Results_name_feature", "_UI_Scheduling_Server_Results_type"),
				 ResultPackage.Literals.SCHEDULING_SERVER_RESULTS__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ResultPackage.Literals.SCHEDULING_SERVER_RESULTS__NON_PREEMPTIBLE_FP_POLICY);
			childrenFeatures.add(ResultPackage.Literals.SCHEDULING_SERVER_RESULTS__FIXED_PRIORITY_POLICY);
			childrenFeatures.add(ResultPackage.Literals.SCHEDULING_SERVER_RESULTS__INTERRUPT_FP_POLICY);
			childrenFeatures.add(ResultPackage.Literals.SCHEDULING_SERVER_RESULTS__POLLING_POLICY);
			childrenFeatures.add(ResultPackage.Literals.SCHEDULING_SERVER_RESULTS__SPORADIC_SERVER_POLICY);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Scheduling_Server_Results.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Scheduling_Server_Results"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Scheduling_Server_Results)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Scheduling_Server_Results_type") :
			getString("_UI_Scheduling_Server_Results_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Scheduling_Server_Results.class)) {
			case ResultPackage.SCHEDULING_SERVER_RESULTS__NAME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ResultPackage.SCHEDULING_SERVER_RESULTS__NON_PREEMPTIBLE_FP_POLICY:
			case ResultPackage.SCHEDULING_SERVER_RESULTS__FIXED_PRIORITY_POLICY:
			case ResultPackage.SCHEDULING_SERVER_RESULTS__INTERRUPT_FP_POLICY:
			case ResultPackage.SCHEDULING_SERVER_RESULTS__POLLING_POLICY:
			case ResultPackage.SCHEDULING_SERVER_RESULTS__SPORADIC_SERVER_POLICY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ResultPackage.Literals.SCHEDULING_SERVER_RESULTS__NON_PREEMPTIBLE_FP_POLICY,
				 ResultFactory.eINSTANCE.createNon_Preemptible_FP_Policy()));

		newChildDescriptors.add
			(createChildParameter
				(ResultPackage.Literals.SCHEDULING_SERVER_RESULTS__FIXED_PRIORITY_POLICY,
				 ResultFactory.eINSTANCE.createFixed_Priority_Policy()));

		newChildDescriptors.add
			(createChildParameter
				(ResultPackage.Literals.SCHEDULING_SERVER_RESULTS__INTERRUPT_FP_POLICY,
				 ResultFactory.eINSTANCE.createInterrupt_FP_Policy()));

		newChildDescriptors.add
			(createChildParameter
				(ResultPackage.Literals.SCHEDULING_SERVER_RESULTS__POLLING_POLICY,
				 ResultFactory.eINSTANCE.createPolling_Policy()));

		newChildDescriptors.add
			(createChildParameter
				(ResultPackage.Literals.SCHEDULING_SERVER_RESULTS__SPORADIC_SERVER_POLICY,
				 ResultFactory.eINSTANCE.createSporadic_Server_Policy()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MastResultsEditPlugin.INSTANCE;
	}

}
