/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult.provider;


import es.esi.gemde.vv.mast.mastresult.ResultPackage;
import es.esi.gemde.vv.mast.mastresult.Sporadic_Server_Policy;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link es.esi.gemde.vv.mast.mastresult.Sporadic_Server_Policy} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class Sporadic_Server_PolicyItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Sporadic_Server_PolicyItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addBackgroundPriorityPropertyDescriptor(object);
			addInitialCapacityPropertyDescriptor(object);
			addMaxPendingReplenishmentsPropertyDescriptor(object);
			addNormalPriorityPropertyDescriptor(object);
			addPreassignedPropertyDescriptor(object);
			addReplenishmentPeriodPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Background Priority feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBackgroundPriorityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Sporadic_Server_Policy_backgroundPriority_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Sporadic_Server_Policy_backgroundPriority_feature", "_UI_Sporadic_Server_Policy_type"),
				 ResultPackage.Literals.SPORADIC_SERVER_POLICY__BACKGROUND_PRIORITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Initial Capacity feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInitialCapacityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Sporadic_Server_Policy_initialCapacity_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Sporadic_Server_Policy_initialCapacity_feature", "_UI_Sporadic_Server_Policy_type"),
				 ResultPackage.Literals.SPORADIC_SERVER_POLICY__INITIAL_CAPACITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Max Pending Replenishments feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMaxPendingReplenishmentsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Sporadic_Server_Policy_maxPendingReplenishments_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Sporadic_Server_Policy_maxPendingReplenishments_feature", "_UI_Sporadic_Server_Policy_type"),
				 ResultPackage.Literals.SPORADIC_SERVER_POLICY__MAX_PENDING_REPLENISHMENTS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Normal Priority feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNormalPriorityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Sporadic_Server_Policy_normalPriority_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Sporadic_Server_Policy_normalPriority_feature", "_UI_Sporadic_Server_Policy_type"),
				 ResultPackage.Literals.SPORADIC_SERVER_POLICY__NORMAL_PRIORITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Preassigned feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPreassignedPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Sporadic_Server_Policy_preassigned_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Sporadic_Server_Policy_preassigned_feature", "_UI_Sporadic_Server_Policy_type"),
				 ResultPackage.Literals.SPORADIC_SERVER_POLICY__PREASSIGNED,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Replenishment Period feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReplenishmentPeriodPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Sporadic_Server_Policy_replenishmentPeriod_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Sporadic_Server_Policy_replenishmentPeriod_feature", "_UI_Sporadic_Server_Policy_type"),
				 ResultPackage.Literals.SPORADIC_SERVER_POLICY__REPLENISHMENT_PERIOD,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns Sporadic_Server_Policy.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Sporadic_Server_Policy"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		Sporadic_Server_Policy sporadic_Server_Policy = (Sporadic_Server_Policy)object;
		return getString("_UI_Sporadic_Server_Policy_type") + " " + sporadic_Server_Policy.getBackgroundPriority();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Sporadic_Server_Policy.class)) {
			case ResultPackage.SPORADIC_SERVER_POLICY__BACKGROUND_PRIORITY:
			case ResultPackage.SPORADIC_SERVER_POLICY__INITIAL_CAPACITY:
			case ResultPackage.SPORADIC_SERVER_POLICY__MAX_PENDING_REPLENISHMENTS:
			case ResultPackage.SPORADIC_SERVER_POLICY__NORMAL_PRIORITY:
			case ResultPackage.SPORADIC_SERVER_POLICY__PREASSIGNED:
			case ResultPackage.SPORADIC_SERVER_POLICY__REPLENISHMENT_PERIOD:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MastResultsEditPlugin.INSTANCE;
	}

}
