/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult.provider;


import es.esi.gemde.vv.mast.mastresult.ResultFactory;
import es.esi.gemde.vv.mast.mastresult.ResultPackage;
import es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class Simulation_Timing_ResultItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Simulation_Timing_ResultItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAvgBlockingTimePropertyDescriptor(object);
			addAvgLocalResponseTimePropertyDescriptor(object);
			addBestLocalResponseTimePropertyDescriptor(object);
			addEventNamePropertyDescriptor(object);
			addMaxPreemptionTimePropertyDescriptor(object);
			addNumOfQueuedActivationsPropertyDescriptor(object);
			addNumOfSuspensionsPropertyDescriptor(object);
			addSuspensionTimePropertyDescriptor(object);
			addWorstBlockingTimePropertyDescriptor(object);
			addWorstLocalResponseTimePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Avg Blocking Time feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAvgBlockingTimePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Simulation_Timing_Result_avgBlockingTime_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Simulation_Timing_Result_avgBlockingTime_feature", "_UI_Simulation_Timing_Result_type"),
				 ResultPackage.Literals.SIMULATION_TIMING_RESULT__AVG_BLOCKING_TIME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Avg Local Response Time feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAvgLocalResponseTimePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Simulation_Timing_Result_avgLocalResponseTime_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Simulation_Timing_Result_avgLocalResponseTime_feature", "_UI_Simulation_Timing_Result_type"),
				 ResultPackage.Literals.SIMULATION_TIMING_RESULT__AVG_LOCAL_RESPONSE_TIME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Best Local Response Time feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBestLocalResponseTimePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Simulation_Timing_Result_bestLocalResponseTime_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Simulation_Timing_Result_bestLocalResponseTime_feature", "_UI_Simulation_Timing_Result_type"),
				 ResultPackage.Literals.SIMULATION_TIMING_RESULT__BEST_LOCAL_RESPONSE_TIME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Event Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEventNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Simulation_Timing_Result_eventName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Simulation_Timing_Result_eventName_feature", "_UI_Simulation_Timing_Result_type"),
				 ResultPackage.Literals.SIMULATION_TIMING_RESULT__EVENT_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Max Preemption Time feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMaxPreemptionTimePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Simulation_Timing_Result_maxPreemptionTime_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Simulation_Timing_Result_maxPreemptionTime_feature", "_UI_Simulation_Timing_Result_type"),
				 ResultPackage.Literals.SIMULATION_TIMING_RESULT__MAX_PREEMPTION_TIME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Num Of Queued Activations feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNumOfQueuedActivationsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Simulation_Timing_Result_numOfQueuedActivations_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Simulation_Timing_Result_numOfQueuedActivations_feature", "_UI_Simulation_Timing_Result_type"),
				 ResultPackage.Literals.SIMULATION_TIMING_RESULT__NUM_OF_QUEUED_ACTIVATIONS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Num Of Suspensions feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNumOfSuspensionsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Simulation_Timing_Result_numOfSuspensions_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Simulation_Timing_Result_numOfSuspensions_feature", "_UI_Simulation_Timing_Result_type"),
				 ResultPackage.Literals.SIMULATION_TIMING_RESULT__NUM_OF_SUSPENSIONS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Suspension Time feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSuspensionTimePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Simulation_Timing_Result_suspensionTime_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Simulation_Timing_Result_suspensionTime_feature", "_UI_Simulation_Timing_Result_type"),
				 ResultPackage.Literals.SIMULATION_TIMING_RESULT__SUSPENSION_TIME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Worst Blocking Time feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addWorstBlockingTimePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Simulation_Timing_Result_worstBlockingTime_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Simulation_Timing_Result_worstBlockingTime_feature", "_UI_Simulation_Timing_Result_type"),
				 ResultPackage.Literals.SIMULATION_TIMING_RESULT__WORST_BLOCKING_TIME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Worst Local Response Time feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addWorstLocalResponseTimePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Simulation_Timing_Result_worstLocalResponseTime_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Simulation_Timing_Result_worstLocalResponseTime_feature", "_UI_Simulation_Timing_Result_type"),
				 ResultPackage.Literals.SIMULATION_TIMING_RESULT__WORST_LOCAL_RESPONSE_TIME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ResultPackage.Literals.SIMULATION_TIMING_RESULT__WORST_GLOBAL_RESPONSE_TIMES);
			childrenFeatures.add(ResultPackage.Literals.SIMULATION_TIMING_RESULT__AVG_GLOBAL_RESPONSE_TIMES);
			childrenFeatures.add(ResultPackage.Literals.SIMULATION_TIMING_RESULT__BEST_GLOBAL_RESPONSE_TIMES);
			childrenFeatures.add(ResultPackage.Literals.SIMULATION_TIMING_RESULT__JITTERS);
			childrenFeatures.add(ResultPackage.Literals.SIMULATION_TIMING_RESULT__LOCAL_MISS_RATIOS);
			childrenFeatures.add(ResultPackage.Literals.SIMULATION_TIMING_RESULT__GLOBAL_MISS_RATIOS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Simulation_Timing_Result.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Simulation_Timing_Result"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Simulation_Timing_Result)object).getEventName();
		return label == null || label.length() == 0 ?
			getString("_UI_Simulation_Timing_Result_type") :
			getString("_UI_Simulation_Timing_Result_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Simulation_Timing_Result.class)) {
			case ResultPackage.SIMULATION_TIMING_RESULT__AVG_BLOCKING_TIME:
			case ResultPackage.SIMULATION_TIMING_RESULT__AVG_LOCAL_RESPONSE_TIME:
			case ResultPackage.SIMULATION_TIMING_RESULT__BEST_LOCAL_RESPONSE_TIME:
			case ResultPackage.SIMULATION_TIMING_RESULT__EVENT_NAME:
			case ResultPackage.SIMULATION_TIMING_RESULT__MAX_PREEMPTION_TIME:
			case ResultPackage.SIMULATION_TIMING_RESULT__NUM_OF_QUEUED_ACTIVATIONS:
			case ResultPackage.SIMULATION_TIMING_RESULT__NUM_OF_SUSPENSIONS:
			case ResultPackage.SIMULATION_TIMING_RESULT__SUSPENSION_TIME:
			case ResultPackage.SIMULATION_TIMING_RESULT__WORST_BLOCKING_TIME:
			case ResultPackage.SIMULATION_TIMING_RESULT__WORST_LOCAL_RESPONSE_TIME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ResultPackage.SIMULATION_TIMING_RESULT__WORST_GLOBAL_RESPONSE_TIMES:
			case ResultPackage.SIMULATION_TIMING_RESULT__AVG_GLOBAL_RESPONSE_TIMES:
			case ResultPackage.SIMULATION_TIMING_RESULT__BEST_GLOBAL_RESPONSE_TIMES:
			case ResultPackage.SIMULATION_TIMING_RESULT__JITTERS:
			case ResultPackage.SIMULATION_TIMING_RESULT__LOCAL_MISS_RATIOS:
			case ResultPackage.SIMULATION_TIMING_RESULT__GLOBAL_MISS_RATIOS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ResultPackage.Literals.SIMULATION_TIMING_RESULT__WORST_GLOBAL_RESPONSE_TIMES,
				 ResultFactory.eINSTANCE.createGlobal_Response_Time_List()));

		newChildDescriptors.add
			(createChildParameter
				(ResultPackage.Literals.SIMULATION_TIMING_RESULT__AVG_GLOBAL_RESPONSE_TIMES,
				 ResultFactory.eINSTANCE.createGlobal_Response_Time_List()));

		newChildDescriptors.add
			(createChildParameter
				(ResultPackage.Literals.SIMULATION_TIMING_RESULT__BEST_GLOBAL_RESPONSE_TIMES,
				 ResultFactory.eINSTANCE.createGlobal_Response_Time_List()));

		newChildDescriptors.add
			(createChildParameter
				(ResultPackage.Literals.SIMULATION_TIMING_RESULT__JITTERS,
				 ResultFactory.eINSTANCE.createGlobal_Response_Time_List()));

		newChildDescriptors.add
			(createChildParameter
				(ResultPackage.Literals.SIMULATION_TIMING_RESULT__LOCAL_MISS_RATIOS,
				 ResultFactory.eINSTANCE.createMiss_Ratio_List()));

		newChildDescriptors.add
			(createChildParameter
				(ResultPackage.Literals.SIMULATION_TIMING_RESULT__GLOBAL_MISS_RATIOS,
				 ResultFactory.eINSTANCE.createGlobal_Miss_Ratio_List()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == ResultPackage.Literals.SIMULATION_TIMING_RESULT__WORST_GLOBAL_RESPONSE_TIMES ||
			childFeature == ResultPackage.Literals.SIMULATION_TIMING_RESULT__AVG_GLOBAL_RESPONSE_TIMES ||
			childFeature == ResultPackage.Literals.SIMULATION_TIMING_RESULT__BEST_GLOBAL_RESPONSE_TIMES ||
			childFeature == ResultPackage.Literals.SIMULATION_TIMING_RESULT__JITTERS;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MastResultsEditPlugin.INSTANCE;
	}

}
