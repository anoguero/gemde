/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult.provider;


import es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION;
import es.esi.gemde.vv.mast.mastresult.ResultFactory;
import es.esi.gemde.vv.mast.mastresult.ResultPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.util.FeatureMapUtil;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class REAL_TIME_SITUATIONItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public REAL_TIME_SITUATIONItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addGenerationDatePropertyDescriptor(object);
			addGenerationProfilePropertyDescriptor(object);
			addGenerationToolPropertyDescriptor(object);
			addModelDatePropertyDescriptor(object);
			addModelNamePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Generation Date feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addGenerationDatePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_REAL_TIME_SITUATION_generationDate_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_REAL_TIME_SITUATION_generationDate_feature", "_UI_REAL_TIME_SITUATION_type"),
				 ResultPackage.Literals.REAL_TIME_SITUATION__GENERATION_DATE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Generation Profile feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addGenerationProfilePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_REAL_TIME_SITUATION_generationProfile_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_REAL_TIME_SITUATION_generationProfile_feature", "_UI_REAL_TIME_SITUATION_type"),
				 ResultPackage.Literals.REAL_TIME_SITUATION__GENERATION_PROFILE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Generation Tool feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addGenerationToolPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_REAL_TIME_SITUATION_generationTool_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_REAL_TIME_SITUATION_generationTool_feature", "_UI_REAL_TIME_SITUATION_type"),
				 ResultPackage.Literals.REAL_TIME_SITUATION__GENERATION_TOOL,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Model Date feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addModelDatePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_REAL_TIME_SITUATION_modelDate_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_REAL_TIME_SITUATION_modelDate_feature", "_UI_REAL_TIME_SITUATION_type"),
				 ResultPackage.Literals.REAL_TIME_SITUATION__MODEL_DATE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Model Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addModelNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_REAL_TIME_SITUATION_modelName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_REAL_TIME_SITUATION_modelName_feature", "_UI_REAL_TIME_SITUATION_type"),
				 ResultPackage.Literals.REAL_TIME_SITUATION__MODEL_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ResultPackage.Literals.REAL_TIME_SITUATION__GROUP);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns REAL_TIME_SITUATION.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/REAL_TIME_SITUATION"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((REAL_TIME_SITUATION)object).getModelName();
		return label == null || label.length() == 0 ?
			getString("_UI_REAL_TIME_SITUATION_type") :
			getString("_UI_REAL_TIME_SITUATION_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(REAL_TIME_SITUATION.class)) {
			case ResultPackage.REAL_TIME_SITUATION__GENERATION_DATE:
			case ResultPackage.REAL_TIME_SITUATION__GENERATION_PROFILE:
			case ResultPackage.REAL_TIME_SITUATION__GENERATION_TOOL:
			case ResultPackage.REAL_TIME_SITUATION__MODEL_DATE:
			case ResultPackage.REAL_TIME_SITUATION__MODEL_NAME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ResultPackage.REAL_TIME_SITUATION__GROUP:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ResultPackage.Literals.REAL_TIME_SITUATION__GROUP,
				 FeatureMapUtil.createEntry
					(ResultPackage.Literals.REAL_TIME_SITUATION__SYSTEM_SLACK,
					 ResultFactory.eINSTANCE.createSlack())));

		newChildDescriptors.add
			(createChildParameter
				(ResultPackage.Literals.REAL_TIME_SITUATION__GROUP,
				 FeatureMapUtil.createEntry
					(ResultPackage.Literals.REAL_TIME_SITUATION__TRACE,
					 ResultFactory.eINSTANCE.createTrace())));

		newChildDescriptors.add
			(createChildParameter
				(ResultPackage.Literals.REAL_TIME_SITUATION__GROUP,
				 FeatureMapUtil.createEntry
					(ResultPackage.Literals.REAL_TIME_SITUATION__TRANSACTION,
					 ResultFactory.eINSTANCE.createTransaction_Results())));

		newChildDescriptors.add
			(createChildParameter
				(ResultPackage.Literals.REAL_TIME_SITUATION__GROUP,
				 FeatureMapUtil.createEntry
					(ResultPackage.Literals.REAL_TIME_SITUATION__PROCESSING_RESOURCE,
					 ResultFactory.eINSTANCE.createProcessing_Resource_Results())));

		newChildDescriptors.add
			(createChildParameter
				(ResultPackage.Literals.REAL_TIME_SITUATION__GROUP,
				 FeatureMapUtil.createEntry
					(ResultPackage.Literals.REAL_TIME_SITUATION__OPERATION,
					 ResultFactory.eINSTANCE.createOperation_Results())));

		newChildDescriptors.add
			(createChildParameter
				(ResultPackage.Literals.REAL_TIME_SITUATION__GROUP,
				 FeatureMapUtil.createEntry
					(ResultPackage.Literals.REAL_TIME_SITUATION__SCHEDULING_SERVER,
					 ResultFactory.eINSTANCE.createScheduling_Server_Results())));

		newChildDescriptors.add
			(createChildParameter
				(ResultPackage.Literals.REAL_TIME_SITUATION__GROUP,
				 FeatureMapUtil.createEntry
					(ResultPackage.Literals.REAL_TIME_SITUATION__SHARED_RESOURCE,
					 ResultFactory.eINSTANCE.createShared_Resource_Results())));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MastResultsEditPlugin.INSTANCE;
	}

}
