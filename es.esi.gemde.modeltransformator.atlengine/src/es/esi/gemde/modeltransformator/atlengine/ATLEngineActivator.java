package es.esi.gemde.modeltransformator.atlengine;

import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

public class ATLEngineActivator extends AbstractUIPlugin {

	private static BundleContext context;
	
	public static final String PLUGIN_ID = "es.esi.gemde.modeltransformator.atlengine";
	
	public static final String METAMODEL_ICON = "icons/metamodel.gif";
	public static final String OUTPUT_ICON = "icons/output.gif";

	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		ATLEngineActivator.context = bundleContext;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		ATLEngineActivator.context = null;
	}
	
	public static Image getMetamodelIcon() {
		return imageDescriptorFromPlugin(PLUGIN_ID, METAMODEL_ICON).createImage();
	}
	
	public static Image getOutputIcon() {
		return imageDescriptorFromPlugin(PLUGIN_ID, OUTPUT_ICON).createImage();
	}

}
