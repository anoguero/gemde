/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.modeltransformator.exceptions;

/**
 * A wrapper exception for any exceptions that should be raised during a
 * transformation.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
@SuppressWarnings("serial")
public class TransformationEngineException extends Exception {

	/**
	 * A constructor with a parameter describing the Exception that raised
	 * this TransforationEngineException.
	 *
	 *@param e the exception that raised this TransforationEngineException
	 */
	public TransformationEngineException(Exception e) {
		super(e);
	}
	
	/**
	 * A constructor with a message describing the error. Normally used for Java transformations.
	 *
	 *@param message the error message
	 */
	public TransformationEngineException(String message) {
		super(message);
	}

	

}
