/*******************************************************************************
 * Copyright (c) 2012 Tecnalia. Software Systems Engineering Unit.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@tecnalia.com)
 *     
 *******************************************************************************/
package es.esi.gemde.modeltransformator.exceptions;

/**
 * Exception raised if the plug-in tries to locate a transformation engine
 * that is not registered.
 *
 * @author Adri�n Noguero (adrian.noguero@tecnalia.com)
 * @version 1.0
 * @since 1.0
 *
 */
@SuppressWarnings("serial")
public class NoPreviousTransformationException extends Exception {

	/**
	 * A constructor for the exception.
	 */
	public NoPreviousTransformationException() {
		super("No valid transformations have been launched!");
	}

}
