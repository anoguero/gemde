/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adrian Noguero (adrian.noguero@tecnalia.com)
 *     
 *******************************************************************************/
package es.esi.gemde.modeltransformator.service;

import java.net.URI;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import es.esi.gemde.modeltransformator.exceptions.TransformationEngineException;

/**
 * Interface that defines a transformation engine. This interface is to be implemented 
 * by plug-ins that extend the Model Transformator plug-in with a new transformation engine.
 *
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 * @version 1.0
 * @since 1.0
 *
 */
public interface ITransformationEngine {

	/**
	 * Returns the name of the transformation engine. It should NOT be null!
	 * 
	 * @return the name of the engine.
	 */
	public String getName();
	
	/**
	 * This method will check the conformance of the provided transformation
	 * with the requirements of the underlying transformation engine.
	 * The transformation will be read via the provided URI.
	 * 
	 * @param uris the list of URI containing the transformation implementation.
	 * @return true if the file is valid for the underlying engine. False otherwise.
	 */
	public boolean checkTransformationURIValidity(List<URI> uris);
	
	/**
	 * This method executes a transformation with the given inputs. Transformation results 
	 * will be place in the directory provided by the outputPath. The method will return an
	 * {@link ITransformationResult} instance.
	 * 
	 * @param inputs the set of {@link EObject}s that will be provided to the engine as inputs.
	 * @param transformation the transformation specification.
	 * @param outputPath the path defining the directory where the transformation outputs will be placed.
	 * @return the results of this transformation.
	 * @throws IllegalArgumentException if the provided path does not point to a valid directory.
	 * @throws TransformationEngineException if any exception is captured during the transformation process.
	 */
	public IStatus executeTransformation(EObject[] inputs, ITransformation transformation, String outputPath) throws IllegalArgumentException, TransformationEngineException;
	
	/**
	 * This method is intended to return a list of option names that will enable GEMDE to create UI and control inputs.
	 * of any engine specific information to be included in the transformations.
	 * 
	 * @return the instance of the composite or null if none is needed
	 */
	public String[] getAdditionalOptionsList();
	

	/**
	 * Check whether a set of inputs, outputs and options can fit the needs of the engine for a transformation.
	 * 
	 * @param inputs the inputs map.
	 * @param outputs the outputs map.
	 * @param options the options map.
	 * @return true if the current selection is valid.
	 */
	public boolean checkTransformationParamsValidity(Map<String, EClass> inputs, Map<String, EClass> outputs, Map<String, Object> options);
	
}
