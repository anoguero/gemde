/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.modeltransformator.service;

import java.net.URI;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;

import es.esi.gemde.core.resources.IGemdeResource;

/**
 * Interface that specifies a transformation in GEMDE
 *
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 * @version 1.0
 * @since 1.0
 *
 */
public interface ITransformation extends IGemdeResource {

	/**
	 * Returns the name of this transformation.
	 * 
	 * @return the name.
	 */
	public String getName();
	
	/**
	 * Returns the name of the engine instance related to this transformation.
	 * 
	 * @return the name of the transformation engine.
	 */
	public String getEngine();
	
	/**
	 * Returns the list with the URIs of the files where the transformation implementation is located.
	 * 
	 * @return the list of URIs of the transformation resources.
	 */
	public List<URI> getTransformationURIs();
	
	/**
	 * Returns the ordered list containing the EClass objects of the required inputs for the transformation.
	 * 
	 * @return the ordered list with the required Class objects.
	 */
	public List<EClass> getRequiredInputList ();
	
	/**
	 * Returns a map mapping the name of each input with its EClass.
	 * 
	 * @return the ordered map with the required String, Class objects pairs.
	 */
	public Map<String, EClass> getInputs();
	
	/**
	 * Returns the list containing the EClass objects representing the generated outputs for this transformation.
	 * The list might be empty as many engines do not use this information.
	 * 
	 * @return the ordered list with the required EClass objects.
	 */
	public List<EClass> getProducedOutputList ();
	
	/**
	 * Returns a map mapping the name of each output with its EClass.
	 * This is not used by every engine, so it might be empty.
	 * 
	 * @return the ordered map with the required String, Class objects pairs.
	 */
	public Map<String, EClass> getOutputs();

	/**
	 * Returns the textual description of this transformation.
	 * 
	 * @return the description of the transformation
	 */
	public String getDescription();
	
	/**
	 * Checks whether this transformation is session protected.
	 * 
	 * @return true is the transformation is protected. False otherwise.
	 * @deprecated Please use {@link IGemdeResource#getResourceType()} instead.
	 */
	public boolean isProtected();
	
	/**
	 * Allows the inclusion of engine specific options in the transformation definition.
	 * 
	 * @param option the name of the option.
	 * @param value the value of the option.
	 */
	public void addTransformationOption(String option, Object value);
	
	/**
	 * Get the engine specific options for this transformation in the format <key,vale>
	 * 
	 * @return the {@link Map} containing the options.
	 */
	public Map<String, Object> getOptions();
	
}
