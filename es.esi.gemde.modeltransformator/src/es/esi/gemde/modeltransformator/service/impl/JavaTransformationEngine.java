/*******************************************************************************
 * Copyright (c) 2012 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adrian Noguero (adrian.noguero@tecnalia.com)
 *     
 *******************************************************************************/
package es.esi.gemde.modeltransformator.service.impl;

import java.net.URI;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import es.esi.gemde.modeltransformator.exceptions.TransformationEngineException;
import es.esi.gemde.modeltransformator.service.AbstractJavaTransformation;
import es.esi.gemde.modeltransformator.service.ITransformation;
import es.esi.gemde.modeltransformator.service.ITransformationEngine;

/**
 * Implementation of the {@link ITransformationEngine} interface to be used for
 * Java transformations contributed to GEMDE.
 *
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 * @version 1.0
 * @since 1.3
 *
 */
public class JavaTransformationEngine implements ITransformationEngine {

	@Override
	public String getName() {
		return AbstractJavaTransformation.ENGINE_NAME;
	}

	@Override
	public boolean checkTransformationURIValidity(List<URI> uris) {
		return true;
	}

	@Override
	public IStatus executeTransformation(EObject[] inputs,
			ITransformation transformation, String outputPath)
			throws IllegalArgumentException, TransformationEngineException {
		if (!(transformation instanceof IJavaTransformation)) {
			throw new IllegalArgumentException("Provided transformation is not a Java transformation.");
		}
		IJavaTransformation t = (IJavaTransformation)transformation;
		return t.execute(inputs, outputPath);
	}

	@Override
	public String[] getAdditionalOptionsList() {
		return new String[0];
	}

	@Override
	public boolean checkTransformationParamsValidity(
			Map<String, EClass> inputs, Map<String, EClass> outputs,
			Map<String, Object> options) {
		return true;
	}

}
