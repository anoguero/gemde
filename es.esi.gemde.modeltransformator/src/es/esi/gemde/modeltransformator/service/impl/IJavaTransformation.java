/*******************************************************************************
 * Copyright (c) 2012 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adrian Noguero (adrian.noguero@tecnalia.com)
 *     
 *******************************************************************************/
package es.esi.gemde.modeltransformator.service.impl;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;

import es.esi.gemde.modeltransformator.exceptions.TransformationEngineException;
import es.esi.gemde.modeltransformator.service.ITransformation;

/**
 * Interface that specifies a java transformation in GEMDE
 *
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 * @version 1.0
 * @since 1.3
 *
 */
public interface IJavaTransformation extends ITransformation {

	/**
	 * This method will be used by the GEMDE Java transformation engine to launch the transformation
	 * 
	 * @param inputs the set of {@link EObject}s that will be provided to the transformation as inputs.
	 * @param outputPath the absolute path of the directory where the transformation outputs will be placed.
	 * @return the results of this transformation.
	 * @throws IllegalArgumentException if the provided path does not point to a valid directory.
	 * @throws TransformationEngineException if any exception is captured during the transformation process.
	 */
	public IStatus execute(EObject[] inputs, String outputPath) throws IllegalArgumentException, TransformationEngineException;
	
}
