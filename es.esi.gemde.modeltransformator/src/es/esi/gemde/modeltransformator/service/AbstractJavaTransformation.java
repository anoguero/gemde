/*******************************************************************************
 * Copyright (c) 2012 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adrian Noguero (adrian.noguero@tecnalia.com)
 *     
 *******************************************************************************/
package es.esi.gemde.modeltransformator.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.eclipse.emf.ecore.EClass;

import es.esi.gemde.modeltransformator.service.impl.IJavaTransformation;

/**
 * Partial implementation of the IJavaTransformation interface.
 * Users are expected to extend this class, rather than implementing
 * the IJavaTransformation interface (not visible).
 *
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 * @version 1.0
 * @since 1.3
 *
 */
public abstract class AbstractJavaTransformation implements IJavaTransformation {

	public static final String ENGINE_NAME = "Java";
	public static final String ICON_PATH = "icons/java_transformation.gif";
	
	protected Map<String, EClass> inputs = new HashMap<String, EClass>();
	protected Map<String, EClass> outputs = new HashMap<String, EClass>();
	protected Map<String, Object> options = new HashMap<String, Object>();
	
	/* (non-Javadoc)
	 * @see es.esi.gemde.modeltransformator.service.ITransformation#getEngine()
	 */
	@Override
	public final String getEngine() {
		return ENGINE_NAME;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modeltransformator.service.ITransformation#getTransformationURIs()
	 */
	@Override
	public final List<URI> getTransformationURIs() {
		return new Vector<URI>();
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modeltransformator.service.ITransformation#isProtected()
	 */
	@Override
	public final boolean isProtected() {
		return true;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.core.resources.IGemdeResource#getResourceType()
	 */
	@Override
	public ResourceType getResourceType() {
		return ResourceType.PROTECTED;
	}
	
	/* (non-Javadoc)
	 * @see es.esi.gemde.modeltransformator.service.ITransformation#getRequiredInputList()
	 */
	@Override
	public List<EClass> getRequiredInputList() {
	    List<EClass> v = new ArrayList<EClass>();
		v.addAll(inputs.values());
		return v;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modeltransformator.service.ITransformation#getInputs()
	 */
	@Override
	public Map<String, EClass> getInputs() {
		return inputs;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modeltransformator.service.ITransformation#getProducedOutputList()
	 */
	@Override
	public List<EClass> getProducedOutputList() {
		List<EClass> v = new ArrayList<EClass>();
		v.addAll(outputs.values());
		return v;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modeltransformator.service.ITransformation#getOutputs()
	 */
	@Override
	public Map<String, EClass> getOutputs() {
		return outputs;
	}
	
	/* (non-Javadoc)
	 * @see es.esi.gemde.modeltransformator.service.ITransformation#addTransformationOption(java.lang.String, java.lang.String)
	 */
	@Override
	public void addTransformationOption(String option, Object value) {
		options.put(option, value);
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.modeltransformator.service.ITransformation#getOptions()
	 */
	@Override
	public Map<String, Object> getOptions() {
		return options;
	}

}
