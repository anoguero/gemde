package com.tecnalia.registry.client.ui;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.core.runtime.Status;

import com.tecnalia.registry.Link;
import com.tecnalia.registry.RegistryClient;
import com.tecnalia.registry.client.ui.events.EclipseRegistryEvent;
import com.tecnalia.registry.client.ui.events.EclipseRegistryObservable;
import com.tecnalia.registry.client.ui.preferences.PreferencesRegistry;
import com.tecnalia.registry.resource.Resource;
import com.tecnalia.util.eclipse.XEclipse;
import com.tecnalia.util.java.file.XFile;

/**
 * This class handles client features for accessing Tecnalia's REGISTRY (repository of resources), and manage the content of the local repository.
 * <p>If the development is independent from Eclipse, or from eclipse.ui use @see com.tecnalia.registry.RegistryClient instead</p>
 * <p>This class is in a ui package because it is the easiest way for a developer to create this object (the registryurl address is taken from preferences page)</p>
 */
public class EclipseRegistryClient extends RegistryClient  { 
	
	public static final String REGISTRY_LOCAL_PROJECT="_Registry";
	
	/**Qualifier name to be added to Registry IResources(IFile, IFolder, IProject, ...)*/
	public static final QualifiedName  resourcePropertyQualifiedNameStatus = new QualifiedName  (EclipseRegistryClient.class.getPackage().getName()+".status",EclipseRegistryClient.class.getPackage().getName()+".status");
	public static final QualifiedName  resourcePropertyQualifiedNameIsRegistryProject = new QualifiedName  (EclipseRegistryClient.class.getPackage().getName()+".isRegistryProject","isRegistryProject");
	public static final QualifiedName  resourcePropertyQualifiedNameID = new QualifiedName  (EclipseRegistryClient.class.getPackage().getName()+".id","id");
	
	private static  EclipseRegistryClient eclipseRegistryClient=null;
	private  IProject iProjectRegistry;
	private static final String STORAGE_LITERAL="storage.jsp?file=";
	private final EclipseRegistryObservable observable;
	private final Logger logger;
	/*[ID,VERSION] index for the local repository*/
	//public final Map<String, ResourceVersion> mapVersions;
	
	
	/**
	 * This method replaces the default constructor in order to get a (unique) singleton instance.
	 * @return
	 * @throws Exception
	 */
	public static EclipseRegistryClient getInstance() throws Exception{
		//get current url from preferences page
		String url_registry_services=RegistryClientUIActivator.getDefault().getPreferenceStore().getString(PreferencesRegistry.KEY_URL_REGISTRY_SERVICES);
		if (url_registry_services==null | url_registry_services.equals("")==true) throw new Exception("Registry url services cannot be empty. Review window/preferences page");
		if(eclipseRegistryClient==null){
			eclipseRegistryClient=new EclipseRegistryClient(url_registry_services);
		}else{
			//compare-update registry url
			if(url_registry_services.equals(eclipseRegistryClient.urlAddress)==false){
				eclipseRegistryClient.urlAddress=url_registry_services;
			}
		}
		//return singleton instance
		return eclipseRegistryClient;
	}
	
	
	/**
	 * If registry parameters have changed, this method will create a new server instance with the new values 
	 * Already controlled in getInstanceMethod
	 * @deprecated
	 */
	public static void restart(){
		;
	}
	
	
    /**
     * Constructor. Create a registry client to consume services.
     * @param registryServicesURL http address of the Registry server service endpoint , this address is typically http://server:port/registry/process.
     */
    private EclipseRegistryClient(String registryServicesURL) throws Exception{     
    		super(registryServicesURL);
    		logger=Logger.getLogger(EclipseRegistryClient.class.getName());
    		//check project
    		checkCreateProjectInNavigator();
    		EclipseRegistryClient.eclipseRegistryClient=this;
    		observable=new EclipseRegistryObservable();
    		//mapVersions=new ConcurrentHashMap<String, ResourceVersion> ();
    }
    
    
    /**
     * Store resource data and files (if resource contains a parameter called files)
     * @param resource
     */
    public void downloadResource(Resource resource) throws Exception{
    	try{
    		checkCreateProjectInNavigator();
    		int eventType=EclipseRegistryEvent.EVENT_DOWNLOAD;
	    	String id=resource.id;
	    	String name=resource.name;
	    	//get folder for that resource
	    	IFolder ifolder=iProjectRegistry.getFolder(name);
	    	File folder=ifolder.getLocation().toFile();
	    	if (ifolder.exists()==false)  {
	    		ifolder.create(true, true, null);
	    		ifolder.setPersistentProperty(resourcePropertyQualifiedNameID,id);
	    		eventType=EclipseRegistryEvent.EVENT_DOWNLOAD;
	    	}
	    	//download data to temp folder
	    	IFolder iTempFolder=iProjectRegistry.getFolder("temp");
	    	File tempFolder=new File (iTempFolder.getLocation().toOSString()); 
	    	if (iTempFolder.exists()==false) {
	    		iTempFolder.create(true,true, null);
	    		iTempFolder.setHidden(true);	
	    	}

	    	String json=resource.toString();
	    	Field[] fields=resource.getClass().getDeclaredFields();
	    	String stringFiles=null;
	    	for(Field field:fields){
	    		if (field.getName().equals("files")) {
	    			Object obj=field.get(resource);
	    			if (obj instanceof String){
	    				stringFiles=(String) obj;break;
	    			}
	    		}
	    	}
	    	if(stringFiles!=null){
	    		List<Link> links=Link.getLinks(stringFiles);
	    		for(Link link:links){
	    			String url=link.href;
	    			if (url.indexOf("http://")!=0) url=getRegistryAddressWithoutHTTPS()+"/"+url;
	    			//download & store file
	    			String fileName=link.href;
	    			if (fileName.contains(STORAGE_LITERAL)) fileName=fileName.replace(STORAGE_LITERAL,""); 
	    			//remove numberPrefix from downloaded files
	    			fileName=removePrefixFileName(fileName);
	    			fileName=tempFolder.getAbsolutePath()+File.separator+fileName;
	    			 downloadFile(url, fileName);
	    		}
	    	}
    		//create the file with json data
    		XFile.writeTextFile(json, tempFolder.getAbsolutePath()+File.separator+"resource.json", true);
    		//everything ok, replace files
    		File[] oldFiles=folder.listFiles();
    		for(File file:oldFiles) file.delete();
    		File[] files=tempFolder.listFiles();
    		for(File file:files) file.renameTo( new File(folder, file.getName()) );
    		//Refresh before notifying observers
    		XEclipse.refreshWorkspace();
	    	//notify observers
	    	EclipseRegistryEvent event=new EclipseRegistryEvent(eventType,resource);
	    	observable.notifyObservers(event);
	    	Map<String, Resource> resources=getLocalResources();
	    	for (String sid:resources.keySet()) System.out.println("resource ID: "+sid);
    	}catch(Exception e){
    		XEclipse.refreshWorkspace();
    		throw e;
    	}
    }
    
    /**
     * Get a Resource from the <strong>local</strong> Repository
     * @param id
     * @return
     * @throws Exception
     */
    @SuppressWarnings("unchecked")
	public Resource getLocalResource(String id) throws Exception{
    	try {
    		checkCreateProjectInNavigator();
			Resource resource=null;
			//get local folders
			if (iProjectRegistry==null) return null;
			for(IResource iResource:iProjectRegistry.members()){
				if(iResource instanceof IFolder){
					IFolder folder=(IFolder) iResource;
					String fid=folder.getPersistentProperty(resourcePropertyQualifiedNameID);
					if (fid.equals(id)){
						IFile iFile=folder.getFile("resource.json");
						if (iFile==null || iFile.exists() ==false) return null;
						String json=XFile.readTextFile(iFile.getLocation().toOSString());
						int index=json.indexOf("\"type\":");
						int index2=json.indexOf(":",index);
						int index3=json.indexOf(",",index2);
						String type=json.substring(index2+2,index3-1);
						Class<Resource> cl;
						try{cl=(Class<Resource>) Class.forName(type);}catch(Exception e){cl=(Class<Resource>) Resource.class;}
						resource=gson.fromJson(json,cl);
						break;
					}
				}
			}
			return resource;
		} catch (Exception e) {
			throw new Exception("Error when retrieving local resource:"+id+". "+e.getMessage(), e);
		}
    }
    
    public IProject getIProjectRegistry(){
    	return iProjectRegistry;
    }
    
    
	/**
	 * Get all the RegistryResource components in the local repository
	 * @return Map containing <IDResource,Resource>
	 * @throws Exception
	 */
    public Map<String,Resource> getLocalResources() throws Exception{
    	Map<String,Resource> mapResources=new LinkedHashMap<String,Resource>();
		for(IResource iResource:iProjectRegistry.members()){
			if (iResource instanceof IFolder){
				IFolder folder= (IFolder) iResource;
				String id=folder.getPersistentProperty(EclipseRegistryClient.resourcePropertyQualifiedNameID);
				if (id!=null){
					Resource resource = getLocalResource(id);
					mapResources.put(id, resource);
				}
			}
		}
		return mapResources;
    }
    
    /**
     * Get the local registry folder. If folder doesn't exist, return nukk
     * @return
     */
    public IFolder getLocalFolder(String id) throws Exception{
    	if(iProjectRegistry==null) return null;
    	Resource resource=getLocalResource(id);
    	return iProjectRegistry.getFolder(resource.name);
    }
    	    
    
    private File downloadFile(String urlAddress, String filePath) throws Exception{
    	try{
    		URL url=new URL(urlAddress);
    	    ReadableByteChannel rbc = Channels.newChannel(url.openStream());
    	    File file=new File(filePath);
    	    FileOutputStream fos = new FileOutputStream(file);
    	    fos.getChannel().transferFrom(rbc, 0, 1 << 24);
    	    fos.flush();
    	    fos.close();rbc.close();
    	    return file;
    	}catch(Exception e){
    		throw new Exception ("Error when downlading file "+filePath+". "+e.getMessage(),e);
    	}
    	/*
            URL url=new URL(urlAddress);
            URLConnection con = url.openConnection();
            con.setConnectTimeout(1000*60);
            ByteArrayInputStream b
            DataInputStream dis = new DataInputStream(con.getInputStream());
            String inputLine;
            StringBuilder sb=new StringBuilder();
            while ((inputLine = dis.readLine()) != null) {
                sb.append(inputLine);sb.append("\n");
            }
            dis.close();
            return sb.toString();
       */
    }
    
    /**
     * Get the http address of the registry by using its http (and not the https) address of the server.
     * <p>This value is useful to get registry data without human interaction, and without ssl certificates<p/>
     * @return
     */
    private String getRegistryAddressWithoutHTTPS(){
    	String serviceAddress=urlAddress;
    	String registryAddress=serviceAddress.replace("/process", "");
    	if (registryAddress.endsWith("/")) registryAddress=registryAddress.substring(0, registryAddress.length()-1);
    	return registryAddress;
    }
    
    
    public void addObserver(Observer o){
    	observable.addObserver(o);
    }
    public void removeObserver(Observer o){
    	observable.deleteObserver(o);
    } 

	
	/**
	 * Mark an IResource (IProject, IFolder, IFile...) with special registry properties, so the resource can be later decorated with error, warning icons.
	 * <p style="color:red"><strong>WARNING: </strong> Check if the event to decorate the folder is triggered when the resource is an @see IFolder. This method will perform perfectly for folders but the touch() method seems to be empty for folders. So keep an eye on this type of @see IResource</p>
	 * @param iResource
	 * @param status any value taken from IStatus class
	 * @param message message to be sent to the error log viewer
	 * @param e exception can be null if status not error
	 * @param refresh if true, refresh resource in the workspace (this will cause decorator extensions to be called automatically).
	 */
	public void markIResource(IResource iResource, int status,String message, Exception e,boolean refresh){
		IStatus iStatus=null;
		switch (status){
			case Status.OK:
				break;
			case Status.INFO:
				logger.log(Level.FINER,message);
				break;
			case Status.WARNING:
				logger.log(Level.WARNING,message);
				logger.log(Level.FINER,message,e);
				break;
			case Status.ERROR:
				logger.log(Level.SEVERE,message);
				logger.log(Level.FINER,message,e);
				break;
			default:
				status=Status.ERROR;
				message="DEVELOPER ERROR- status type= "+status+" is not implemented";
				logger.log(Level.SEVERE,message);
		}
		if (status!=Status.OK)iStatus=new Status(status, RegistryClientUIActivator.PLUGIN_ID,  message);
		try{	
			iResource.setPersistentProperty(resourcePropertyQualifiedNameStatus, Integer.toString(status));
			//IMPORTANT:  this method will notify eclipse to redecorate resources (if needed). Calling refreshWorkspace is not enough to fire this event.
			iResource.touch(null);
		}catch(Exception ex){
			logger.log(Level.FINEST, e.getMessage(),e);
		}
		if(iStatus!=null){ RegistryClientUIActivator.getDefault().getLog().log(iStatus);}
		if (refresh)XEclipse.refreshWorkspace(iResource.getLocation().toOSString());
	}
	
	/**
	 * Check if Registry project exists in Eclipse. If it has been deleted and any operation is performed, this method will create the project 
	 * @throws CoreException 
	 */
	private void checkCreateProjectInNavigator() throws CoreException{
		iProjectRegistry=XEclipse.getIProject(REGISTRY_LOCAL_PROJECT);
    	if (iProjectRegistry==null || iProjectRegistry.exists()==false){
    		//the registry project could be deleted manually->create it again
			iProjectRegistry=XEclipse.createIProject(REGISTRY_LOCAL_PROJECT);
			iProjectRegistry.setPersistentProperty(resourcePropertyQualifiedNameIsRegistryProject,"true");
    	}
	}
	
	
	private String removePrefixFileName(String fileName){
		int index=fileName.indexOf("_");
		if (index>0){
			String prefix=fileName.substring(0,index);
			try{
				Integer.valueOf(prefix);
				return fileName.substring(index+1);
			}catch(Exception e){
				return fileName;
			}
		}else{
			return fileName;
		}
	}
	
}



