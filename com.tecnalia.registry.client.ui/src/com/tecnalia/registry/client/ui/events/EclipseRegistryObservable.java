package com.tecnalia.registry.client.ui.events;

import java.util.Observable;

/**
 * Observer pattern
 *<p>This class contains the observers to be notified when local registry changes</p>
 *<p>See <a href="http://en.wikipedia.org/wiki/Observer_pattern" target="_blank">http://en.wikipedia.org/wiki/Observer_pattern</a></p>
 */
 public class EclipseRegistryObservable extends Observable {

	 @Override	
	 public void notifyObservers(Object arg){
		 setChanged();
		 super.notifyObservers(arg);
	 }
	 
}