package com.tecnalia.registry.client.ui.events;
import org.eclipse.ui.IStartup;


public class Startup implements IStartup {

	/**
	 * Start plugin, no implementation is required sinc the RegistryClientUIActivator.start() will be called
	 */
	@Override
	public void earlyStartup() {

	}

}
