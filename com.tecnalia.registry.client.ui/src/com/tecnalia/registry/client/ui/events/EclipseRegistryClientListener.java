package com.tecnalia.registry.client.ui.events;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import com.tecnalia.registry.client.ui.EclipseRegistryClient;
import com.tecnalia.registry.client.ui.RegistryClientUIActivator;
import com.tecnalia.registry.client.ui.decorators.RegistryDecorator;
import com.tecnalia.registry.client.ui.preferences.PreferencesRegistry;
import com.tecnalia.registry.resource.Resource;
import com.tecnalia.util.eclipse.XEclipse;

/**
 * LIstener for changes in the <strong>remote</strong>  registry.
 * <p>This class will create a thread to check for newer versions in the remote registry, and update the workspace</p>
 */
public class EclipseRegistryClientListener implements Runnable{
	/**Logger class*/
	private final Logger logger;
	/**History of errors,warnings, infos, the next time the listener is running, notify to the platform just if something has changed)*/
	private final List<String> listMessages;
	private final List<IResource> listElementsToBeDecorated;
	
	public EclipseRegistryClientListener(){
		logger=Logger.getLogger(EclipseRegistryClientListener.class.getName());
		listMessages=new ArrayList<String>();
		listElementsToBeDecorated=new  ArrayList<IResource>();
	}
	
	public void run(){
		checkVersionsLocalRemote();
	}
	
	
	/**
	 * Status of the registry project
	 * <p>If one resource is WARNING, another one is ERROR, and the rest is OK, the project global status is <string>ERROR</strong></p>
	 *
	 */
	class ProjectStatus {
		int status=Status.OK;
		public void update(int newStatus){
			if(status==Status.ERROR){
				;
			}else if (status==Status.WARNING){
				if(newStatus==Status.ERROR) status=newStatus;
			}else if (status==Status.INFO){
				if(newStatus==Status.ERROR || newStatus==Status.WARNING) status=newStatus;
			}else {
				//Status.OK or any other status
				if(newStatus==Status.ERROR || newStatus==Status.WARNING || newStatus==Status.INFO) status=newStatus;
			}
		}
	}
	
	private void checkVersionsLocalRemote() {
		/**
		 * Performance variable. Store last project status. If new status,the workspace will be updated 
		 */
		int previousProjectStatus=0;
		List<String> listNewMessages=new ArrayList<String>();//the new list of errors
		listElementsToBeDecorated.clear();//reset container of elements to be decorated
		EclipseRegistryClient client=null;
		ProjectStatus projectStatus=null;
		IProject iProject=null;
		try {
			 client=EclipseRegistryClient.getInstance();
			iProject=XEclipse.getIProject(EclipseRegistryClient.REGISTRY_LOCAL_PROJECT); 
			projectStatus=new ProjectStatus();// This value marks if any error, warning or just info in the Registry, so the appropriate decorator icon will be rendered later
			String sStatus=iProject.getPersistentProperty(EclipseRegistryClient.resourcePropertyQualifiedNameStatus);
			if (sStatus!=null)	previousProjectStatus=Integer.valueOf(sStatus);
			//--------------------------------------------------------------------------------------------------------------------------------------------
			//					FOREACH LOCAL RESOURCE COMPARE VERSIONS
			//--------------------------------------------------------------------------------------------------------------------------------------------
			if (iProject!=null){
				for(IResource iResource : iProject.members()){
					
					if(iResource instanceof IFolder){
						IFolder iFolder=(IFolder) iResource;
						String folderName=iFolder.getName();
						if (folderName.toLowerCase().equals("temp")) continue;
						String id=iFolder.getPersistentProperty(EclipseRegistryClient.resourcePropertyQualifiedNameID);
						if (id==null){
							markIResource(client,iFolder, IStatus.ERROR, "This folder is not a valid Registry folder. You should delete the folder and download it again", null, projectStatus, listMessages, listNewMessages);
						}
						try{
							//System.out.println("mustSynchronize "+folderName );
							Resource localResource=client.getLocalResource(id);
							Resource remoteResource=client.get(id);
							if(localResource==null){  
								String message="Local Resource "+folderName+" is incorrect. A synchronization is recommended for this resource";
								markIResource(client,iFolder, IStatus.WARNING, message, null, projectStatus, listMessages, listNewMessages);
							}else if(remoteResource==null){
								String message="Local Resource  "+folderName+"  doesn't exists in the remote registry";
								markIResource(client,iFolder, IStatus.WARNING, message, null, projectStatus, listMessages, listNewMessages);
							}else if(remoteResource.version!=localResource.version){
								String message="Local resource version ("+localResource.version+") is different than the remote one(" +remoteResource.version+"). You may want to update it";
								markIResource(client,iFolder, IStatus.INFO, message, null,projectStatus, listMessages, listNewMessages);
							}else{
								//TODO if local repo data has changed but keeping the same version number, this method is currently returning ok, ¿should we check more info than version?
								String message="ok "+folderName;//ok+name/id because any other resource could put ok , then this resource won't be updated later (since the message would be the same)
								markIResource(client,iFolder, IStatus.OK, message, null, projectStatus, listMessages, listNewMessages);
							}
						}catch(Exception e){
							String message="Error when comparing Local vs Remote Registry element: "+folderName+". "+e.getClass().getSimpleName()+". "+e.getMessage();
							markIResource(client,iFolder, IStatus.ERROR, message, e, projectStatus, listMessages, listNewMessages);
						}
					}
				}
				
			}
			//--------------------------------------------------------------------------------------------------------------------------------------------
			//					CHECK IF REMOTE REGISTRY IS RUNNING
			//--------------------------------------------------------------------------------------------------------------------------------------------
			if (client.checkServerIsAlive()){
				//String message= "OK-project";
				//if (previousProjectStatus!=IStatus.OK && projectStatus.status==IStatus.OK)	markIResource(client,iProject, IStatus.OK, message, null, projectStatus, listMessages, listNewMessages);
			}else{
				String message= "Remote Registry server error. Check window/preferences page, Internet connection, or go to ' "+RegistryClientUIActivator.getDefault().getPreferenceStore().getString(PreferencesRegistry.KEY_URL_REGISTRY)+" '";
				//markIResource(client,iProject, IStatus.ERROR, message, null, projectStatus, listMessages, listNewMessages);
				throw new Exception (message);
			}
		} catch (Exception e) {
			String message="Error when checking local Repository against the Remote one. " + e.getMessage();
			markIResource(client,iProject, IStatus.ERROR, message, null, projectStatus, listMessages, listNewMessages);
		}finally{
			//--------------------------------------------------------------------------------------------------------------------------------------------
			//					UPDATE REAL PROJECT STATUS
			//--------------------------------------------------------------------------------------------------------------------------------------------
			if(projectStatus.status!=previousProjectStatus){
				if (projectStatus.status==Status.ERROR)markIResource(client,iProject, projectStatus.status, "The local Registry contains errors", null, projectStatus,listMessages, listNewMessages);
				else if (projectStatus.status==Status.WARNING)markIResource(client,iProject, projectStatus.status, "The local Registry contains warnings", null, projectStatus, listMessages, listNewMessages);
				else if (projectStatus.status==Status.INFO)markIResource(client,iProject, projectStatus.status, "The local Registry is OK but not the same as the Remote one", null, projectStatus, listMessages, listNewMessages);
				else if (projectStatus.status==Status.OK)markIResource(client,iProject, projectStatus.status, "The local Registry is OK", null,projectStatus,  listMessages, listNewMessages);
			}

			//--------------------------------------------------------------------------------------------------------------------------------------------
			//					DECORATE RESOURCES (warning, error, info... icons)
			//--------------------------------------------------------------------------------------------------------------------------------------------
			//check if different list of messages->update workspace
			boolean mustDecorate=false;
			if (listMessages.size()==listNewMessages.size()){
				for(String message:listNewMessages) listMessages.remove(message);
				if(listMessages.size()>0) mustDecorate=true;
			}else{
				mustDecorate=true;
			}
			if(mustDecorate){
				if (listElementsToBeDecorated.isEmpty()==false){
					Object[] obj=listElementsToBeDecorated.toArray();
					try{
						RegistryDecorator.getDecorator().fireDecorateElements(obj);
					}catch(Exception e){
						logger.log(Level.SEVERE,"Error when decorating Registry resources. "+e.getMessage(),e);
					}
				}
			}

			
			
			//update list of errors to avoid duplication of errors on new iteration
			listMessages.clear();
			for(String message:listNewMessages)listMessages.add(message);
			
		}
		
	}
	
	
	/**
	 * Mark resource to be decorated with info, error, warning icons, in the project navigation panel
	 * @param eclipseRegistryClient
	 * @param iResource
	 * @param status Eclipse status value(e.j: Status.OK)
	 * @param message Description of status (error message, etc)
	 * @param e if error, Exception that threw the error
	 * @param projectStatus IProject registry is updated with this value (if error in  any resource, project itself should display error)
	 * @param listMessages messages that appeared during the previous synchronization
	 * @param listNewMessages new Messages that appeared after synchronization;
	 */
	private void markIResource(EclipseRegistryClient eclipseRegistryClient, IResource iResource,int status,String message, Exception e, ProjectStatus projectStatus,  List<String> listMessages, List<String> listNewMessages){
		if(listMessages.contains(message)==false) {eclipseRegistryClient.markIResource(iResource,status, message, e, false);}
		projectStatus.update(status);
		listNewMessages.add(message);
		listElementsToBeDecorated.add(iResource);
	}
	

//	private int  compareVersions(double v1, double v2){
//		if(v1>v2) return -1;
//		else if (v1==v2) return 0;
//		else return 1;
//		
//	}
	
  
	  
}


