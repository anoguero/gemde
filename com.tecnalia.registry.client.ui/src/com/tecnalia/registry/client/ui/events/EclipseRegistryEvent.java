package com.tecnalia.registry.client.ui.events;

import com.tecnalia.registry.resource.Resource;

/**
 * Information about events occurred in the local registry
 */
public class EclipseRegistryEvent {
	
	public static final int EVENT_NONE=0;
	public static final int EVENT_DOWNLOAD=1;
	public static final int EVENT_UPLOAD=2;
	public static final int EVENT_DELETE=3;
	
	public final int eventType;
	public final Resource resource;
	public final Object additionalData;
	
	public EclipseRegistryEvent(int eventType){
		this.eventType=eventType;
		this.resource=null;
		this.additionalData=null;
	}
	
	public EclipseRegistryEvent(int eventType, Resource resource){
		this.eventType=eventType;
		this.resource=resource;
		this.additionalData=null;
	}
	
	public EclipseRegistryEvent(int eventType, Resource resource,Object additionalData){
		this.eventType=eventType;
		this.resource=resource;
		this.additionalData=additionalData;
	}
	
	
}
