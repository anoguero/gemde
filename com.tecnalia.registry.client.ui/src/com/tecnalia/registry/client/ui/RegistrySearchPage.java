package com.tecnalia.registry.client.ui;

import org.eclipse.jface.dialogs.DialogPage;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.search.ui.ISearchPage;
import org.eclipse.search.ui.ISearchPageContainer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.tecnalia.registry.Query;
import com.tecnalia.registry.client.ui.preferences.PreferencesRegistry;
import com.tecnalia.registry.resource.Resource;
import com.tecnalia.util.java.XUtil;
import com.tecnalia.util.ui.XEclipseUI;

public class RegistrySearchPage extends DialogPage implements ISearchPage {

	private String selected;
	private Text idText;
	
	public RegistrySearchPage() {
		super();
	}

	public RegistrySearchPage(String title) {
		super(title);
	}

	public RegistrySearchPage(String title, ImageDescriptor image) {
		super(title, image);

	}

	@Override
	public void createControl(Composite parent) {
		GridLayout layout = new GridLayout(1, false);
		layout.horizontalSpacing = 5;
		layout.verticalSpacing = 5;
		parent.setLayout(layout);
		new Label(parent, 0).setText("Search for GEMDE files\nempty string or * is allowed:");
		//TODO add a radiobutton to select validations or transformations or all gemde like resources)
		idText = new Text(parent, SWT.BORDER);
		idText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		if (selected != null) {
			idText.setText(selected);
			idText.setSelection(0, selected.length());
		}
		setControl(parent);

	}

	@Override
	public boolean performAction() {
		try {
			String url_registry_services="";
			String url_registry="";
			try{
				IPreferenceStore preferenceStore=RegistryClientUIActivator.getDefault().getPreferenceStore();
				url_registry_services=preferenceStore.getString(PreferencesRegistry.KEY_URL_REGISTRY_SERVICES);
				if (url_registry_services==null | url_registry_services.equals("")==true) throw new Exception("Registry url services cannot be empty");
				//check also other properties (better to throw exeption here than in the results window
				url_registry=preferenceStore.getString(PreferencesRegistry.KEY_URL_REGISTRY);
				if (XUtil.isEmpty(url_registry)) throw new Exception("Registry url web address cannot be empty");
			}catch(Exception e) {
				throw new Exception("Review Registry values in Window/preferences page. "+e.getMessage(),e);
			}
			//NewSearchUI.runQueryInBackground(TextSearchQueryProvider.getPreferred().createQuery(idText.getText())); //TODO display results in a results page  is  too complicated, in the meantime replace it by a custom SWT widget to display results
			//ISearchResultViewPart resultViewer=NewSearchUI.activateSearchResultView();
			EclipseRegistryClient registryClient=EclipseRegistryClient.getInstance();
			Query query=new Query();
			query.fullText=idText.getText();
			query.resultType="";
			query.onlyFirstLevelResults=true;
			java.util.List<Resource> resources=registryClient.search(query);
			for(Resource res:resources){
				System.out.println(res.toString());
			}
			RegistryResultsPage resultsPage=new RegistryResultsPage(resources);
			this.getShell().close();//CLOSE THE SEARCH WINDOW
			
			resultsPage.run();
			
		} catch (Exception e) {
			XEclipseUI.messageBoxError("Error when searching",e.getMessage());
		}
        return true;
	}

	@Override
	public void setContainer(ISearchPageContainer container) {
		if (container.getSelection() instanceof TextSelection) {
			selected = ((TextSelection) container.getSelection()).getText();
		}
	}
	
	
	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		idText.setFocus();
	}
	


}
