package com.tecnalia.registry.client.ui;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import com.tecnalia.registry.client.ui.events.EclipseRegistryClientListener;


/**
 * The activator class controls the plug-in life cycle
 */
public class RegistryClientUIActivator extends AbstractUIPlugin {
	
	/**
	 * Connect to Remote Registry every X time
	 */
	public static final long PERIOD_IN_MILLIS=5000;
	
	//Images
	public static  final String ICON_NAME_PROJECT="globe-network.png";
	public static  final String ICON_NAME_INFO="miniicon_info.png";
	public static final String ICON_NAME_WARNING="miniicon_warning.png";
	//public static final String ICON_NAME_ERROR="miniicon_error.png";
	public static final String ICON_NAME_ERROR="cross-small.png";
	// The plug-in ID
	public static final String PLUGIN_ID = "com.tecnalia.registry.client.ui"; //$NON-NLS-1$

	// The shared instance
	private static RegistryClientUIActivator plugin;
	private boolean isStarted=false;
	private ScheduledExecutorService executorService=null;
	
	/**
	 * The constructor
	 */
	public RegistryClientUIActivator() {
	}

	/*
	 * Start the plugin. Create scheduledExecutor for checking local and remote repo differences.
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		System.out.println("Starting registry...");
		super.start(context);
		plugin = this;
		//ACTIVATE PERIODIC SYNCRONIZATION
		executorService=Executors.newScheduledThreadPool(1);
		executorService.scheduleAtFixedRate(new EclipseRegistryClientListener(), 5000L, PERIOD_IN_MILLIS, TimeUnit.MILLISECONDS);
		isStarted=true;
		//CREATE COMMON IMAGES
		initializeImages();
		System.out.println("Registry started!!!");
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		executorService.shutdown();
		executorService.awaitTermination(500, TimeUnit.MILLISECONDS);
		plugin = null;
		isStarted=false;
		super.stop(context);
	}

	public boolean isStarted(){return isStarted;}
	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static RegistryClientUIActivator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	
	public void initializeImages(){
		this.getImageRegistry().put(ICON_NAME_ERROR,imageDescriptorFromPlugin(PLUGIN_ID, "icons/"+ICON_NAME_ERROR));
		this.getImageRegistry().put(ICON_NAME_WARNING,imageDescriptorFromPlugin(PLUGIN_ID, "icons/"+ICON_NAME_WARNING));
		this.getImageRegistry().put(ICON_NAME_INFO,imageDescriptorFromPlugin(PLUGIN_ID, "icons/"+ICON_NAME_INFO));
		this.getImageRegistry().put(ICON_NAME_PROJECT,imageDescriptorFromPlugin(PLUGIN_ID, "icons/"+ICON_NAME_PROJECT));
	}
	
}
