package com.tecnalia.registry.client.ui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;

import com.tecnalia.registry.client.ui.preferences.PreferencesRegistry;
import com.tecnalia.registry.resource.Resource;
import com.tecnalia.util.java.XUtil;
import com.tecnalia.util.ui.XEclipseUI;


/**
 * SWT window to display results from  the remote registry
 * <p>NOTE. this class should be replaced by a eclipse results page</p>
 *
 */
public class RegistryResultsPage {
//FIXME replace this class by an eclipse results page, for a better integration
	public static final int COLUMN_TYPE=0;
	public static final int COLUMN_DATA=1;
	public static final int COLUMN_ACTIONS=2;
	public static final int COLUMN_ID=3;
	
	public final int SHELL_WIDTH=800;
	public final int SHELL_HEIGHT=600;
	/**List of resources from the search */
	private List<Resource> listResults;
	/**address of registry, useful to create real http paths */
	private String url_registry;
	/**the details control in a html way*/
	private Browser browser=null;
	/** Current resource data to be displayed on results */
	private Resource selectedResource=null;
	
	public RegistryResultsPage(List<Resource> listResults){
		this.listResults=listResults;
		url_registry=RegistryClientUIActivator.getDefault().getPreferenceStore().getString(PreferencesRegistry.KEY_URL_REGISTRY);
	}
	
	/**
	 * Runs the application
	 */
	public void run() {
		Display display = getDisplay();
		Shell shell = new Shell(display,SWT.OK|SWT.CANCEL);
		shell.setSize(SHELL_WIDTH, SHELL_HEIGHT);
		shell.setText("Search results");
		createContents(shell);
		//shell.pack();
		shell.open();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		//display.dispose();
	}


	private void createContents(final Shell shell) {
		final int NUM_GRID=5;
		GridLayout gridLayout =new GridLayout();gridLayout.numColumns=NUM_GRID;
		shell.setLayout(gridLayout);
		shell.addListener (SWT.Traverse, new Listener () {
			public void handleEvent (Event event) {
				switch (event.detail) {
					case SWT.TRAVERSE_ESCAPE:
						shell.close ();
						event.detail = SWT.TRAVERSE_NONE;
						event.doit = false;
						break;
				}
			}
		});
		
		//-----------------------------------------------------------------------------------------------------------
		//										RESULTS LIST
		//-----------------------------------------------------------------------------------------------------------
		final Tree  tree = new Tree(shell, SWT.NONE); tree.setHeaderVisible(true); tree.setLinesVisible(true);
		TreeColumn treeColumnType=new TreeColumn(tree, SWT.LEFT);treeColumnType.setText("Name"); treeColumnType.setWidth(200);
		TreeColumn treeColumnData=new TreeColumn(tree, SWT.LEFT);treeColumnData.setText("Resource"); treeColumnData.setWidth(500);
		TreeColumn treeColumnActions=new TreeColumn(tree, SWT.LEFT);treeColumnActions.setText("Actions"); treeColumnActions.setWidth(100);
		TreeColumn treeColumnID=new TreeColumn(tree, SWT.Hide);treeColumnID.setText("ID"); treeColumnID.setWidth(0);
		tree.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				displayResults(e);
			}
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				displayResults(e);
			}
			public void displayResults(SelectionEvent e){
		          TreeItem[] selection = tree.getSelection();
		          if(XUtil.isEmpty(selection)==false){
		        	  TreeItem item=selection[0];
		        	  String id=item.getText(COLUMN_ID);
		        	  if (XUtil.isEmpty(id)==false){
		        		  //System.out.println("Selection={" + item.getText() + "}");
		        		  Resource resource=null;
		        		  for(Resource res:listResults) {
		        			  if(res.id.equals(id)){resource=res;break;}
		        		  }
		        		  selectedResource=resource;
		        		  boolean ok=browser.execute("paintResource("+resource.toString()+")");
		        		  if (!ok){XEclipseUI.messageBoxInfo("resource",resource.toString());}//TODO if javascript error replace this messageBox by a better alternative display of details
		        	  }else{
		        		  //selectedResource=null;
		        	  }
		          }
			}
	 });
		GridData g1 = new GridData(); g1.horizontalAlignment = GridData.END; g1.grabExcessHorizontalSpace = true;g1.horizontalSpan=NUM_GRID;
		tree.setLayoutData(g1);
		
		//-----------------------------------------------------------------------------------------------------------
		//										DETAILS
		//-----------------------------------------------------------------------------------------------------------
		try {
				browser = new Browser(shell, SWT.BORDER);
				String html = "<HTML><HEAD><TITLE>results</TITLE></HEAD><BODY>"+htmlCode; html=html.replace("$SERVER", this.url_registry);html += "</BODY></HTML>";
				browser.setText(html);browser.setSize(600,300);
				GridData g2 = new GridData(SWT.FILL, SWT.FILL, true, true);g2.grabExcessHorizontalSpace = true;g2.horizontalSpan=NUM_GRID;
				browser.setLayoutData(g2);
		} catch (SWTError e) {
			XEclipseUI.messageBoxError("Browser not available", "Eclipse couldn't create the details box. "+e.getMessage());
		}

		//-----------------------------------------------------------------------------------------------------------
		//										ACTIONS BUTTONS
		//-----------------------------------------------------------------------------------------------------------
		Button buttonDownload=new Button(shell,SWT.PUSH);	    buttonDownload.setText("download resource");
	    Button buttonClose=new Button(shell,SWT.PUSH);	    buttonClose.setText("close");
	    GridData g3 = new GridData(); g3.horizontalAlignment = GridData.END; g3.grabExcessHorizontalSpace = true;g3.horizontalSpan=NUM_GRID-1;
	    GridData g4 = new GridData(); g4.horizontalAlignment = GridData.END; 
	    buttonDownload.setLayoutData(g3);
	    buttonClose.setLayoutData(g4);
	    
	    buttonDownload.addSelectionListener(new SelectionAdapter() {
	        @Override
	        public void widgetSelected(SelectionEvent e) {
	        	try{
	        		EclipseRegistryClient eclipseRegistryClient=EclipseRegistryClient.getInstance();
	        		eclipseRegistryClient.downloadResource(selectedResource);
	        		shell.close();
	        	}catch(NullPointerException npe){
	        		XEclipseUI.messageBoxError("Error when downloading resources","NullPointerException");
	        	}catch(Exception ex){
	        		XEclipseUI.messageBoxError("Error when downloading resources",ex.getMessage());
	        	}
	        }
	    });
	    
	    buttonClose.addSelectionListener(new SelectionAdapter() {
	        @Override
	        public void widgetSelected(SelectionEvent e) {
				shell.close ();
	        }
	    });
    
		if(listResults!=null && listResults.size()>0 ) {
			//-----------------------------------------------------------------------------------------------------------
			//										display results
			//-----------------------------------------------------------------------------------------------------------
			//create collapsable categories
			Map<String,TreeItem> mapCategories=new  HashMap<String,TreeItem>();
			for (Resource resource: listResults){
					TreeItem  item = new TreeItem(tree, SWT.NONE);
					item.setText(0, resource.type);
					mapCategories.put(resource.type,item);
			}
			//insert results into category;
			for (Resource resource: listResults){
				paintResult(mapCategories, resource);
			}
			//expand all categories	
			for(TreeItem item: mapCategories.values()) {
				item.setExpanded(true);
			}
			
		}else{
			//-----------------------------------------------------------------------------------------------------------
			//										no results
			//-----------------------------------------------------------------------------------------------------------
			TreeItem  item = new TreeItem(tree, SWT.NONE);
			item.setText(0, "no results were found");
		}
		
		// Pack 
		///tree.pack();
		 //shell.pack();
	}
	
	private void paintResult(Map<String,TreeItem> mapCategories,Resource resource){
		String name=resource.name;
		String data="";
		String actions="";
		String id=resource.id;
		TreeItem parent=mapCategories.get(resource.type);
		TreeItem item=new TreeItem(parent, SWT.NONE);
        item.setText(COLUMN_TYPE,name); item.setText(COLUMN_DATA,data); item.setText(COLUMN_ACTIONS,actions);item.setText(COLUMN_ID,id);
		//if (resource..description!=null || resource.description.equals("")==false)
	}
	
	
	
	   public static Display getDisplay() {
		      Display display = Display.getCurrent();
		      //may be null if outside the UI thread
		      if (display == null)
		         display = Display.getDefault();
		      return display;		
		   }
	   
	   
	   /**
		 * WARNING: add $SERVER to js files
		 * WARNING2:be careful with css, since when creating nodes automatically(javascript), the css renderer is not called
		 */
		String htmlCode=
	            "    <div id='resource' >\n" + 
	            "        <table  id=\"resourceTable\" style='font-size:12px;font-family:Verdana,Arial,Sans-serif'>\n" + 
	            "        </table>\n" + 
	            "    </div>\n" + 
	            "    <style>\n" + 
	            "         th{text-align:left; vertical-align: text-top;}\n" + 
	            "         th span{margin-right:12px;}\n" + 
	            "    </style>\n" + 
	            "        <script src='$SERVER/js/registry.js'></script>\n" + 
	            "        <script src='$SERVER/js/wiki.js'></script>\n" + 
	            "        <script>\n" + 
	            "            function paintResource(resource){\n" + 
	            "                var table=document.getElementById('resourceTable');\n" + 
	            "                table.innerHTML='';\n" + 
	            "                for(var key in resource){\n" + 
	            "                   var value=resource[key];\n" + 
	            "                   if (!value) value='';\n" + 
	            "                   if(key=='type'){ var data=value.split('.'); value=data[data.length-1];}\n" + 
	            "                   value=com.tecnalia.wiki. fromWIKIToHTML(value);\n" + 
	            "                   var row = document.createElement('tr');\n" + 
	            "                   row.innerHTML='<th><span>'+key+'<\\/span><\\/th>'+'<td>'+value+'<\\/td>';\n" + 
	            "                   table.appendChild(row);\n" + 
	            "                    \n" + 
	            "                }\n" + 
	            "            }        \n" + 
	            "    </script>";
}
