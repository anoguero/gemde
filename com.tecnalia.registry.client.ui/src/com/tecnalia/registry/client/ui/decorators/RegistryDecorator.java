package com.tecnalia.registry.client.ui.decorators;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DecorationContext;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ILightweightLabelDecorator;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.LabelProviderChangedEvent;
import org.eclipse.ui.IDecoratorManager;

import com.tecnalia.registry.client.ui.EclipseRegistryClient;
import com.tecnalia.registry.client.ui.RegistryClientUIActivator;

/**
 * Paint status icons in resource explorer views 
 * <p><strong>http://www.eclipse.org/articles/Article-Decorators/decorators.html</strong></p>
 *<p>http://stackoverflow.com/questions/7552638/overview-of-isharedimages</p>
 *<p>http://pic.dhe.ibm.com/infocenter/ratdevz/v8r0/index.jsp?topic=%2Fcom.ibm.etools.carma.doc%2Ftopics%2Ftut_carma_mod_2_extend_ex_1_les_4.html</p>
 *<p>http://www.vorlesungsfrei.de/kai/index.php?location=eclipse&sub=sharedimages</p>
 *<p>regarding sharedImages: http://www.refractions.net:8080/confluence/display/DEV/ImageRegistry+and+Images</p>
 */
public class RegistryDecorator extends LabelProvider  implements ILightweightLabelDecorator {
	private ILabelProviderListener listener;
	private static final String DECORATOR_EXTENSION_ID="com.tecnalia.registry.client.ui.registryDecorator";
	
	//LOGGER
	private static Logger logger=Logger.getLogger(RegistryDecorator.class.getName());
	
	@Override
	public void addListener(ILabelProviderListener listener) {
		this.listener=listener;
	}

	@Override
	public void dispose() {
		;
		
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
		this.listener=null;
	}
	
	
	@Override
	public void decorate(Object element, IDecoration decoration) {
		if(element instanceof IResource){
			IResource resource=(IResource) element;
			IProject project=resource.getProject();
			if (isRegistryProject(project)==false) return;//DON'T DECORATE ANY OTHER RESOURCES!!!
			QualifiedName qName = EclipseRegistryClient.resourcePropertyQualifiedNameStatus;
			String icon=null;
			try {
				if(resource instanceof IProject){
					IProject pr=(IProject) element;
//					IResource[] members=pr.members();
					//for (IResource res:members) decorate (res, decoration);
					decorateRegistryProjectIcon(pr,decoration);
				}else if (resource instanceof IFolder){
//					IFolder folder=(IFolder) element;
//					IResource[] members=folder.members();
					//for (IResource res:members) decorate (res, decoration);
				}
				String sStatus=resource.getPersistentProperty(qName);
				int status=Integer.valueOf(sStatus);
				switch (status) {
				case Status.ERROR:
					icon=RegistryClientUIActivator.ICON_NAME_ERROR;
					break;
				case Status.WARNING:
					icon=RegistryClientUIActivator.ICON_NAME_WARNING;
					break;
				case Status.INFO:
					icon=RegistryClientUIActivator.ICON_NAME_INFO;
					break;
				case Status.OK:
					icon=null;
				default:
					icon=null;
					logger.log(Level.FINEST,"Implement icon status="+status);
					break;
				}
			} catch (Exception e) {
				logger.log(Level.FINEST,"Error when decorating IResource "+e.getMessage(),e);
			}
			if (icon==null){
				//OK->remove old icons from resource
				decoration.addOverlay(null);
			}else{
				//Decorate resource
				
				/* don't delete these commented sentences
				//Image image = PlatformUI.getWorkbench().getSharedImages().getImage(icon);
				//Image image = PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_DEC_FIELD_ERROR);
				ImageDescriptor imageDescriptor=ImageDescriptor.createFromImage(image);
				*/
				ImageDescriptor imageDescriptor=RegistryClientUIActivator.getDefault().getImageRegistry().getDescriptor(icon);
				decoration.addOverlay(imageDescriptor, IDecoration.BOTTOM_RIGHT);
				//decoration.addPrefix("ERROR");
			}
			try{
				//project.refreshLocal(IResource.DEPTH_INFINITE, null);
			}catch (Exception e){
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Add custom icon for Registry project, in navigation pane 
	 * @param project
	 * @throws Exception
	 */
	private void decorateRegistryProjectIcon(IProject project,IDecoration decoration) throws Exception{
		if (isRegistryProject(project)){
            if (decoration.getDecorationContext()instanceof DecorationContext) {
            	DecorationContext context=(DecorationContext) decoration.getDecorationContext();
            	if (context.getProperty(IDecoration.ENABLE_REPLACE)!=Boolean.TRUE)context.putProperty(IDecoration.ENABLE_REPLACE, Boolean.TRUE);
        		ImageDescriptor imageDescriptor=RegistryClientUIActivator.getDefault().getImageRegistry().getDescriptor(RegistryClientUIActivator.ICON_NAME_PROJECT);
        		decoration.addOverlay(imageDescriptor, IDecoration.REPLACE);
            	
            }

		}
	}
	
	private boolean isRegistryProject(IProject project){
		try{
			String isRegistry=project.getPersistentProperty(EclipseRegistryClient.resourcePropertyQualifiedNameIsRegistryProject);
			if (isRegistry!=null)return true;
			else return false;
		}catch(Exception e){
			return false;
		}
	}
	
	/**
	 * Call decorator event to display status icons in eclipse.
	 * This method is necessary, since the extension itself is not able to fire the event when a resource property has changed
	 * If resource.refreshLocal(IResource.DEPTH_INFINITE, null) is called, the project explorer view displays the decorations nicely,
	 * but the package explorer view is not able to fire this decoration event by itself.
	 * <p></p>
	 * @param elements The items to be decorated. In this case, any IResource is suitable to be decorated
	 */
	public void fireDecorateElements(Object[] elements){
		LabelProviderChangedEvent event=new LabelProviderChangedEvent(this,elements);
		this.listener.labelProviderChanged(event);
	}

	
  
  /**
   * Get RegistryDecorator instance	
   * @return
   */
  public static RegistryDecorator getDecorator(){
	    IDecoratorManager decoratorManager = RegistryClientUIActivator.getDefault().getWorkbench().getDecoratorManager();
	    if (decoratorManager.getEnabled(DECORATOR_EXTENSION_ID)){
	      return (RegistryDecorator) decoratorManager.getBaseLabelProvider(DECORATOR_EXTENSION_ID);
	    }
	    return null;
  }
	
}
