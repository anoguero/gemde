package com.tecnalia.registry.client.ui;

public class ResourceVersion{
	public final String id;
	public double version;
	public  boolean mustUpdate=true;
	public String updateComment="";
	public ResourceVersion(String id,double version){
		this.id=id;
		this.version=version;
	}
}