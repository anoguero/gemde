package com.tecnalia.registry.client.ui.handlers;

import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWebBrowser;

import com.tecnalia.registry.client.ui.EclipseRegistryClient;
import com.tecnalia.registry.client.ui.RegistryClientUIActivator;
import com.tecnalia.registry.client.ui.preferences.PreferencesRegistry;
import com.tecnalia.util.ui.XEclipseUI;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class ConnectToRegistryHandler extends AbstractHandler {
	
	/**
	 * The constructor.
	 */
	public ConnectToRegistryHandler() {
		
	}

	/**
	 * the command has been executed, so extract extract the needed information
	 * from the application context.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		String url=null;
		try {
			url=RegistryClientUIActivator.getDefault().getPreferenceStore().getString(PreferencesRegistry.KEY_URL_REGISTRY);
			//MessageDialog.openInformation(window.getShell(),"Client","Open Registry");
			final IWebBrowser browser = PlatformUI.getWorkbench().getBrowserSupport().createBrowser(null );
			if(EclipseRegistryClient.getInstance().checkServerIsAlive()==false) throw new Exception ("Is server running?. Check Registry configuration at window/preferences or try browsing at ' "+url+" '");
			browser.openURL(new URL(url)); //TODO Angel to Adrian: open url causes  external MOFScript exception (either update(if possible)MOFScript or open ticket in  mof project ) !MESSAGE Error reading help table of contents file /"org.eclipse.mofscript.editor/help/tocsamples.xml" (skipping file)
		} catch (Exception  e) {
			String message="Error when accessing to Registry at" +url+"." +e.getMessage();
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, message, e);
			XEclipseUI.messageBoxError("Error when connectiong to Remote registry", message);
			throw new ExecutionException(message,e);
		}
		return null;
	}
}
