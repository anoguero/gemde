package com.tecnalia.registry.client.ui;

import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

public class PreferencesTecnalia extends PreferencePage implements IWorkbenchPreferencePage {
	
	public PreferencesTecnalia() {
		
	}

	public PreferencesTecnalia(String title) {
		super(title);
		
	}

	public PreferencesTecnalia(String title, ImageDescriptor image) {
		super(title, image);
		
	}

	@Override
	public void init(IWorkbench workbench) {
	

	}

	@Override
	protected Control createContents(Composite parent) {
		return null;
	}

}
