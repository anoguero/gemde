package com.tecnalia.registry.client.ui.preferences;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import com.tecnalia.registry.client.ui.EclipseRegistryClient;
import com.tecnalia.registry.client.ui.RegistryClientUIActivator;
import com.tecnalia.util.ui.XEclipseUI;

/**
 * This class represents a preference page that
 * is contributed to the Preferences dialog. By 
 * subclassing <samp>FieldEditorPreferencePage</samp>, we
 * can use the field support built into JFace that allows
 * us to create a page that is small and knows how to 
 * save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They
 * are stored in the preference store that belongs to
 * the main plug-in class. That way, preferences can
 * be accessed directly via the preference store.
 */

public class PreferencesRegistry extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	
	public static final String KEY_URL_REGISTRY="KEY_URL_REGISTRY";
	public static final String KEY_URL_REGISTRY_SERVICES="KEY_URL_REGISTRY_SERVICES";
	public static final String KEY_USER_REGISTRY="KEY_USER_REGISTRY";
	public static final String KEY_PASWORD_REGISTRY="KEY_PASSWORD_REGISTRY";
	
	public PreferencesRegistry() {
		super(GRID);
		setPreferenceStore(RegistryClientUIActivator.getDefault().getPreferenceStore());
		setDescription("Access to Registry web application and remote services");
	}
	
	/**
	 * Creates the field editors. Field editors are abstractions of
	 * the common GUI blocks needed to manipulate various types
	 * of preferences. Each field editor knows how to save and
	 * restore itself.
	 */
	public void createFieldEditors() {
		addField(new StringFieldEditor(KEY_URL_REGISTRY, "URL of web application", getFieldEditorParent()));
		addField(new StringFieldEditor(KEY_URL_REGISTRY_SERVICES, "URL of remote services", getFieldEditorParent()));
		addField(new StringFieldEditor(KEY_USER_REGISTRY, "User name (optional)", getFieldEditorParent()));
		addField(new StringFieldEditor(KEY_PASWORD_REGISTRY, "User password (optional)", getFieldEditorParent()));
		/* 
		 * addField(new DirectoryFieldEditor(KEY_URL_REGISTRY_SERVICES, , getFieldEditorParent()));
		addField(
			new BooleanFieldEditor(
				PreferenceConstants.P_BOOLEAN,
				"&An example of a boolean preference",
				getFieldEditorParent()));
		
		addField(new RadioGroupFieldEditor(
				PreferenceConstants.P_CHOICE,
			"An example of a multiple-choice preference",
			1,
			new String[][] { { "&Choice 1", "choice1" }, {
				"C&hoice 2", "choice2" }
		}, getFieldEditorParent()));
		addField(
			new StringFieldEditor(PreferenceConstants.P_STRING, "A &text preference:", getFieldEditorParent()));
		*/
	}

	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
	}
	
	
	@Override
	public boolean performOk(){
		boolean res=super.performOk();
		try{
			//restart is not needed, but check if new URL is valid;
			if (EclipseRegistryClient.getInstance().checkServerIsAlive()==false) {
				String message="Is the server running?.\n Check Registry configuration at window/preferences, then Internet connection, and finally try browsing at registry addresses";
				XEclipseUI.messageBoxError("Remote registry error", message);
				res=false;
			}
		} catch (Exception e){
			XEclipseUI.messageBoxError("Remote registry error", e.getMessage());
			res=false;
		}
		return res;
	}
	
}