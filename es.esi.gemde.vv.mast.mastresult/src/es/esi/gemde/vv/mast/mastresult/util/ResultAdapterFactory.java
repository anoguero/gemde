/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult.util;

import es.esi.gemde.vv.mast.mastresult.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage
 * @generated
 */
public class ResultAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ResultPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResultAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ResultPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResultSwitch<Adapter> modelSwitch =
		new ResultSwitch<Adapter>() {
			@Override
			public Adapter caseDetailed_Utilization(Detailed_Utilization object) {
				return createDetailed_UtilizationAdapter();
			}
			@Override
			public Adapter caseDocument_Root(Document_Root object) {
				return createDocument_RootAdapter();
			}
			@Override
			public Adapter caseFixed_Priority_Policy(Fixed_Priority_Policy object) {
				return createFixed_Priority_PolicyAdapter();
			}
			@Override
			public Adapter caseGlobal_Miss_Ratio(Global_Miss_Ratio object) {
				return createGlobal_Miss_RatioAdapter();
			}
			@Override
			public Adapter caseGlobal_Miss_Ratio_List(Global_Miss_Ratio_List object) {
				return createGlobal_Miss_Ratio_ListAdapter();
			}
			@Override
			public Adapter caseGlobal_Response_Time(Global_Response_Time object) {
				return createGlobal_Response_TimeAdapter();
			}
			@Override
			public Adapter caseGlobal_Response_Time_List(Global_Response_Time_List object) {
				return createGlobal_Response_Time_ListAdapter();
			}
			@Override
			public Adapter caseInterrupt_FP_Policy(Interrupt_FP_Policy object) {
				return createInterrupt_FP_PolicyAdapter();
			}
			@Override
			public Adapter caseMiss_Ratio(Miss_Ratio object) {
				return createMiss_RatioAdapter();
			}
			@Override
			public Adapter caseMiss_Ratio_List(Miss_Ratio_List object) {
				return createMiss_Ratio_ListAdapter();
			}
			@Override
			public Adapter caseNon_Preemptible_FP_Policy(Non_Preemptible_FP_Policy object) {
				return createNon_Preemptible_FP_PolicyAdapter();
			}
			@Override
			public Adapter caseOperation_Results(Operation_Results object) {
				return createOperation_ResultsAdapter();
			}
			@Override
			public Adapter casePolling_Policy(Polling_Policy object) {
				return createPolling_PolicyAdapter();
			}
			@Override
			public Adapter casePriority_Ceiling(Priority_Ceiling object) {
				return createPriority_CeilingAdapter();
			}
			@Override
			public Adapter caseProcessing_Resource_Results(Processing_Resource_Results object) {
				return createProcessing_Resource_ResultsAdapter();
			}
			@Override
			public Adapter caseQueue_Size(Queue_Size object) {
				return createQueue_SizeAdapter();
			}
			@Override
			public Adapter caseReady_Queue_Size(Ready_Queue_Size object) {
				return createReady_Queue_SizeAdapter();
			}
			@Override
			public Adapter caseREAL_TIME_SITUATION(REAL_TIME_SITUATION object) {
				return createREAL_TIME_SITUATIONAdapter();
			}
			@Override
			public Adapter caseScheduling_Server_Results(Scheduling_Server_Results object) {
				return createScheduling_Server_ResultsAdapter();
			}
			@Override
			public Adapter caseShared_Resource_Results(Shared_Resource_Results object) {
				return createShared_Resource_ResultsAdapter();
			}
			@Override
			public Adapter caseSimulation_Timing_Result(Simulation_Timing_Result object) {
				return createSimulation_Timing_ResultAdapter();
			}
			@Override
			public Adapter caseSlack(Slack object) {
				return createSlackAdapter();
			}
			@Override
			public Adapter caseSporadic_Server_Policy(Sporadic_Server_Policy object) {
				return createSporadic_Server_PolicyAdapter();
			}
			@Override
			public Adapter caseTiming_Result(Timing_Result object) {
				return createTiming_ResultAdapter();
			}
			@Override
			public Adapter caseTrace(Trace object) {
				return createTraceAdapter();
			}
			@Override
			public Adapter caseTransaction_Results(Transaction_Results object) {
				return createTransaction_ResultsAdapter();
			}
			@Override
			public Adapter caseUtilization(Utilization object) {
				return createUtilizationAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Detailed_Utilization <em>Detailed Utilization</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Detailed_Utilization
	 * @generated
	 */
	public Adapter createDetailed_UtilizationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Document_Root <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Document_Root
	 * @generated
	 */
	public Adapter createDocument_RootAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Fixed_Priority_Policy <em>Fixed Priority Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Fixed_Priority_Policy
	 * @generated
	 */
	public Adapter createFixed_Priority_PolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio <em>Global Miss Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio
	 * @generated
	 */
	public Adapter createGlobal_Miss_RatioAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio_List <em>Global Miss Ratio List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio_List
	 * @generated
	 */
	public Adapter createGlobal_Miss_Ratio_ListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Global_Response_Time <em>Global Response Time</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Global_Response_Time
	 * @generated
	 */
	public Adapter createGlobal_Response_TimeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Global_Response_Time_List <em>Global Response Time List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Global_Response_Time_List
	 * @generated
	 */
	public Adapter createGlobal_Response_Time_ListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Interrupt_FP_Policy <em>Interrupt FP Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Interrupt_FP_Policy
	 * @generated
	 */
	public Adapter createInterrupt_FP_PolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Miss_Ratio <em>Miss Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Miss_Ratio
	 * @generated
	 */
	public Adapter createMiss_RatioAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Miss_Ratio_List <em>Miss Ratio List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Miss_Ratio_List
	 * @generated
	 */
	public Adapter createMiss_Ratio_ListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Non_Preemptible_FP_Policy <em>Non Preemptible FP Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Non_Preemptible_FP_Policy
	 * @generated
	 */
	public Adapter createNon_Preemptible_FP_PolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Operation_Results <em>Operation Results</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Operation_Results
	 * @generated
	 */
	public Adapter createOperation_ResultsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy <em>Polling Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Polling_Policy
	 * @generated
	 */
	public Adapter createPolling_PolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Priority_Ceiling <em>Priority Ceiling</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Priority_Ceiling
	 * @generated
	 */
	public Adapter createPriority_CeilingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results <em>Processing Resource Results</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results
	 * @generated
	 */
	public Adapter createProcessing_Resource_ResultsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Queue_Size <em>Queue Size</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Queue_Size
	 * @generated
	 */
	public Adapter createQueue_SizeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Ready_Queue_Size <em>Ready Queue Size</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Ready_Queue_Size
	 * @generated
	 */
	public Adapter createReady_Queue_SizeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION <em>REAL TIME SITUATION</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION
	 * @generated
	 */
	public Adapter createREAL_TIME_SITUATIONAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results <em>Scheduling Server Results</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results
	 * @generated
	 */
	public Adapter createScheduling_Server_ResultsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results <em>Shared Resource Results</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results
	 * @generated
	 */
	public Adapter createShared_Resource_ResultsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result <em>Simulation Timing Result</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result
	 * @generated
	 */
	public Adapter createSimulation_Timing_ResultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Slack <em>Slack</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Slack
	 * @generated
	 */
	public Adapter createSlackAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Sporadic_Server_Policy <em>Sporadic Server Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Sporadic_Server_Policy
	 * @generated
	 */
	public Adapter createSporadic_Server_PolicyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Timing_Result <em>Timing Result</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Timing_Result
	 * @generated
	 */
	public Adapter createTiming_ResultAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Trace <em>Trace</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Trace
	 * @generated
	 */
	public Adapter createTraceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Transaction_Results <em>Transaction Results</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Transaction_Results
	 * @generated
	 */
	public Adapter createTransaction_ResultsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link es.esi.gemde.vv.mast.mastresult.Utilization <em>Utilization</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see es.esi.gemde.vv.mast.mastresult.Utilization
	 * @generated
	 */
	public Adapter createUtilizationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ResultAdapterFactory
