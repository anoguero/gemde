/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult.util;

import es.esi.gemde.vv.mast.mastresult.*;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage
 * @generated
 */
public class ResultSwitch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ResultPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResultSwitch() {
		if (modelPackage == null) {
			modelPackage = ResultPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ResultPackage.DETAILED_UTILIZATION: {
				Detailed_Utilization detailed_Utilization = (Detailed_Utilization)theEObject;
				T result = caseDetailed_Utilization(detailed_Utilization);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.DOCUMENT_ROOT: {
				Document_Root document_Root = (Document_Root)theEObject;
				T result = caseDocument_Root(document_Root);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.FIXED_PRIORITY_POLICY: {
				Fixed_Priority_Policy fixed_Priority_Policy = (Fixed_Priority_Policy)theEObject;
				T result = caseFixed_Priority_Policy(fixed_Priority_Policy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.GLOBAL_MISS_RATIO: {
				Global_Miss_Ratio global_Miss_Ratio = (Global_Miss_Ratio)theEObject;
				T result = caseGlobal_Miss_Ratio(global_Miss_Ratio);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.GLOBAL_MISS_RATIO_LIST: {
				Global_Miss_Ratio_List global_Miss_Ratio_List = (Global_Miss_Ratio_List)theEObject;
				T result = caseGlobal_Miss_Ratio_List(global_Miss_Ratio_List);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.GLOBAL_RESPONSE_TIME: {
				Global_Response_Time global_Response_Time = (Global_Response_Time)theEObject;
				T result = caseGlobal_Response_Time(global_Response_Time);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.GLOBAL_RESPONSE_TIME_LIST: {
				Global_Response_Time_List global_Response_Time_List = (Global_Response_Time_List)theEObject;
				T result = caseGlobal_Response_Time_List(global_Response_Time_List);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.INTERRUPT_FP_POLICY: {
				Interrupt_FP_Policy interrupt_FP_Policy = (Interrupt_FP_Policy)theEObject;
				T result = caseInterrupt_FP_Policy(interrupt_FP_Policy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.MISS_RATIO: {
				Miss_Ratio miss_Ratio = (Miss_Ratio)theEObject;
				T result = caseMiss_Ratio(miss_Ratio);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.MISS_RATIO_LIST: {
				Miss_Ratio_List miss_Ratio_List = (Miss_Ratio_List)theEObject;
				T result = caseMiss_Ratio_List(miss_Ratio_List);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.NON_PREEMPTIBLE_FP_POLICY: {
				Non_Preemptible_FP_Policy non_Preemptible_FP_Policy = (Non_Preemptible_FP_Policy)theEObject;
				T result = caseNon_Preemptible_FP_Policy(non_Preemptible_FP_Policy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.OPERATION_RESULTS: {
				Operation_Results operation_Results = (Operation_Results)theEObject;
				T result = caseOperation_Results(operation_Results);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.POLLING_POLICY: {
				Polling_Policy polling_Policy = (Polling_Policy)theEObject;
				T result = casePolling_Policy(polling_Policy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.PRIORITY_CEILING: {
				Priority_Ceiling priority_Ceiling = (Priority_Ceiling)theEObject;
				T result = casePriority_Ceiling(priority_Ceiling);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.PROCESSING_RESOURCE_RESULTS: {
				Processing_Resource_Results processing_Resource_Results = (Processing_Resource_Results)theEObject;
				T result = caseProcessing_Resource_Results(processing_Resource_Results);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.QUEUE_SIZE: {
				Queue_Size queue_Size = (Queue_Size)theEObject;
				T result = caseQueue_Size(queue_Size);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.READY_QUEUE_SIZE: {
				Ready_Queue_Size ready_Queue_Size = (Ready_Queue_Size)theEObject;
				T result = caseReady_Queue_Size(ready_Queue_Size);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.REAL_TIME_SITUATION: {
				REAL_TIME_SITUATION reaL_TIME_SITUATION = (REAL_TIME_SITUATION)theEObject;
				T result = caseREAL_TIME_SITUATION(reaL_TIME_SITUATION);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.SCHEDULING_SERVER_RESULTS: {
				Scheduling_Server_Results scheduling_Server_Results = (Scheduling_Server_Results)theEObject;
				T result = caseScheduling_Server_Results(scheduling_Server_Results);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.SHARED_RESOURCE_RESULTS: {
				Shared_Resource_Results shared_Resource_Results = (Shared_Resource_Results)theEObject;
				T result = caseShared_Resource_Results(shared_Resource_Results);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.SIMULATION_TIMING_RESULT: {
				Simulation_Timing_Result simulation_Timing_Result = (Simulation_Timing_Result)theEObject;
				T result = caseSimulation_Timing_Result(simulation_Timing_Result);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.SLACK: {
				Slack slack = (Slack)theEObject;
				T result = caseSlack(slack);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.SPORADIC_SERVER_POLICY: {
				Sporadic_Server_Policy sporadic_Server_Policy = (Sporadic_Server_Policy)theEObject;
				T result = caseSporadic_Server_Policy(sporadic_Server_Policy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.TIMING_RESULT: {
				Timing_Result timing_Result = (Timing_Result)theEObject;
				T result = caseTiming_Result(timing_Result);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.TRACE: {
				Trace trace = (Trace)theEObject;
				T result = caseTrace(trace);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.TRANSACTION_RESULTS: {
				Transaction_Results transaction_Results = (Transaction_Results)theEObject;
				T result = caseTransaction_Results(transaction_Results);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ResultPackage.UTILIZATION: {
				Utilization utilization = (Utilization)theEObject;
				T result = caseUtilization(utilization);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Detailed Utilization</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Detailed Utilization</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDetailed_Utilization(Detailed_Utilization object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Document Root</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDocument_Root(Document_Root object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Fixed Priority Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Fixed Priority Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFixed_Priority_Policy(Fixed_Priority_Policy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Global Miss Ratio</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Global Miss Ratio</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGlobal_Miss_Ratio(Global_Miss_Ratio object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Global Miss Ratio List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Global Miss Ratio List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGlobal_Miss_Ratio_List(Global_Miss_Ratio_List object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Global Response Time</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Global Response Time</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGlobal_Response_Time(Global_Response_Time object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Global Response Time List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Global Response Time List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGlobal_Response_Time_List(Global_Response_Time_List object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interrupt FP Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interrupt FP Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInterrupt_FP_Policy(Interrupt_FP_Policy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Miss Ratio</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Miss Ratio</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMiss_Ratio(Miss_Ratio object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Miss Ratio List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Miss Ratio List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMiss_Ratio_List(Miss_Ratio_List object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Non Preemptible FP Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Non Preemptible FP Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNon_Preemptible_FP_Policy(Non_Preemptible_FP_Policy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Results</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Results</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperation_Results(Operation_Results object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Polling Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Polling Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePolling_Policy(Polling_Policy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Priority Ceiling</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Priority Ceiling</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePriority_Ceiling(Priority_Ceiling object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Processing Resource Results</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Processing Resource Results</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProcessing_Resource_Results(Processing_Resource_Results object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Queue Size</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Queue Size</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQueue_Size(Queue_Size object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ready Queue Size</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ready Queue Size</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseReady_Queue_Size(Ready_Queue_Size object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>REAL TIME SITUATION</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>REAL TIME SITUATION</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseREAL_TIME_SITUATION(REAL_TIME_SITUATION object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Scheduling Server Results</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Scheduling Server Results</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScheduling_Server_Results(Scheduling_Server_Results object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Shared Resource Results</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Shared Resource Results</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseShared_Resource_Results(Shared_Resource_Results object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simulation Timing Result</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simulation Timing Result</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSimulation_Timing_Result(Simulation_Timing_Result object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Slack</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Slack</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSlack(Slack object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sporadic Server Policy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sporadic Server Policy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSporadic_Server_Policy(Sporadic_Server_Policy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Timing Result</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Timing Result</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTiming_Result(Timing_Result object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Trace</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Trace</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTrace(Trace object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Transaction Results</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Transaction Results</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTransaction_Results(Transaction_Results object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Utilization</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Utilization</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUtilization(Utilization object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} //ResultSwitch
