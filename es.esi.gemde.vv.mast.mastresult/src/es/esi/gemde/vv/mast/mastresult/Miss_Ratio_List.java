/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Miss Ratio List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Miss_Ratio_List#getMissRatio <em>Miss Ratio</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getMiss_Ratio_List()
 * @model extendedMetaData="name='Miss_Ratio_List' kind='elementOnly'"
 * @generated
 */
public interface Miss_Ratio_List extends EObject {
	/**
	 * Returns the value of the '<em><b>Miss Ratio</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastresult.Miss_Ratio}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Miss Ratio</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Miss Ratio</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getMiss_Ratio_List_MissRatio()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Miss_Ratio' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Miss_Ratio> getMissRatio();

} // Miss_Ratio_List
