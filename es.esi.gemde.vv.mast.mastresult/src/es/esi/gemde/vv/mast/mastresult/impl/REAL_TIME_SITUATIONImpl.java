/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult.impl;

import es.esi.gemde.vv.mast.mastresult.Operation_Results;
import es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results;
import es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION;
import es.esi.gemde.vv.mast.mastresult.ResultPackage;
import es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results;
import es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results;
import es.esi.gemde.vv.mast.mastresult.Slack;
import es.esi.gemde.vv.mast.mastresult.Trace;
import es.esi.gemde.vv.mast.mastresult.Transaction_Results;

import java.util.Collection;

import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>REAL TIME SITUATION</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.REAL_TIME_SITUATIONImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.REAL_TIME_SITUATIONImpl#getSystemSlack <em>System Slack</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.REAL_TIME_SITUATIONImpl#getTrace <em>Trace</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.REAL_TIME_SITUATIONImpl#getTransaction <em>Transaction</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.REAL_TIME_SITUATIONImpl#getProcessingResource <em>Processing Resource</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.REAL_TIME_SITUATIONImpl#getOperation <em>Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.REAL_TIME_SITUATIONImpl#getSchedulingServer <em>Scheduling Server</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.REAL_TIME_SITUATIONImpl#getSharedResource <em>Shared Resource</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.REAL_TIME_SITUATIONImpl#getGenerationDate <em>Generation Date</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.REAL_TIME_SITUATIONImpl#getGenerationProfile <em>Generation Profile</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.REAL_TIME_SITUATIONImpl#getGenerationTool <em>Generation Tool</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.REAL_TIME_SITUATIONImpl#getModelDate <em>Model Date</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.REAL_TIME_SITUATIONImpl#getModelName <em>Model Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class REAL_TIME_SITUATIONImpl extends EObjectImpl implements REAL_TIME_SITUATION {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * The default value of the '{@link #getGenerationDate() <em>Generation Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenerationDate()
	 * @generated
	 * @ordered
	 */
	protected static final XMLGregorianCalendar GENERATION_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getGenerationDate() <em>Generation Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenerationDate()
	 * @generated
	 * @ordered
	 */
	protected XMLGregorianCalendar generationDate = GENERATION_DATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getGenerationProfile() <em>Generation Profile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenerationProfile()
	 * @generated
	 * @ordered
	 */
	protected static final String GENERATION_PROFILE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getGenerationProfile() <em>Generation Profile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenerationProfile()
	 * @generated
	 * @ordered
	 */
	protected String generationProfile = GENERATION_PROFILE_EDEFAULT;

	/**
	 * The default value of the '{@link #getGenerationTool() <em>Generation Tool</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenerationTool()
	 * @generated
	 * @ordered
	 */
	protected static final String GENERATION_TOOL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getGenerationTool() <em>Generation Tool</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGenerationTool()
	 * @generated
	 * @ordered
	 */
	protected String generationTool = GENERATION_TOOL_EDEFAULT;

	/**
	 * The default value of the '{@link #getModelDate() <em>Model Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelDate()
	 * @generated
	 * @ordered
	 */
	protected static final XMLGregorianCalendar MODEL_DATE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getModelDate() <em>Model Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelDate()
	 * @generated
	 * @ordered
	 */
	protected XMLGregorianCalendar modelDate = MODEL_DATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getModelName() <em>Model Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelName()
	 * @generated
	 * @ordered
	 */
	protected static final String MODEL_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getModelName() <em>Model Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelName()
	 * @generated
	 * @ordered
	 */
	protected String modelName = MODEL_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected REAL_TIME_SITUATIONImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResultPackage.Literals.REAL_TIME_SITUATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, ResultPackage.REAL_TIME_SITUATION__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Slack> getSystemSlack() {
		return getGroup().list(ResultPackage.Literals.REAL_TIME_SITUATION__SYSTEM_SLACK);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Trace> getTrace() {
		return getGroup().list(ResultPackage.Literals.REAL_TIME_SITUATION__TRACE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Transaction_Results> getTransaction() {
		return getGroup().list(ResultPackage.Literals.REAL_TIME_SITUATION__TRANSACTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Processing_Resource_Results> getProcessingResource() {
		return getGroup().list(ResultPackage.Literals.REAL_TIME_SITUATION__PROCESSING_RESOURCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Operation_Results> getOperation() {
		return getGroup().list(ResultPackage.Literals.REAL_TIME_SITUATION__OPERATION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Scheduling_Server_Results> getSchedulingServer() {
		return getGroup().list(ResultPackage.Literals.REAL_TIME_SITUATION__SCHEDULING_SERVER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Shared_Resource_Results> getSharedResource() {
		return getGroup().list(ResultPackage.Literals.REAL_TIME_SITUATION__SHARED_RESOURCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XMLGregorianCalendar getGenerationDate() {
		return generationDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenerationDate(XMLGregorianCalendar newGenerationDate) {
		XMLGregorianCalendar oldGenerationDate = generationDate;
		generationDate = newGenerationDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultPackage.REAL_TIME_SITUATION__GENERATION_DATE, oldGenerationDate, generationDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getGenerationProfile() {
		return generationProfile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenerationProfile(String newGenerationProfile) {
		String oldGenerationProfile = generationProfile;
		generationProfile = newGenerationProfile;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultPackage.REAL_TIME_SITUATION__GENERATION_PROFILE, oldGenerationProfile, generationProfile));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getGenerationTool() {
		return generationTool;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGenerationTool(String newGenerationTool) {
		String oldGenerationTool = generationTool;
		generationTool = newGenerationTool;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultPackage.REAL_TIME_SITUATION__GENERATION_TOOL, oldGenerationTool, generationTool));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XMLGregorianCalendar getModelDate() {
		return modelDate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelDate(XMLGregorianCalendar newModelDate) {
		XMLGregorianCalendar oldModelDate = modelDate;
		modelDate = newModelDate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultPackage.REAL_TIME_SITUATION__MODEL_DATE, oldModelDate, modelDate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getModelName() {
		return modelName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelName(String newModelName) {
		String oldModelName = modelName;
		modelName = newModelName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultPackage.REAL_TIME_SITUATION__MODEL_NAME, oldModelName, modelName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResultPackage.REAL_TIME_SITUATION__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case ResultPackage.REAL_TIME_SITUATION__SYSTEM_SLACK:
				return ((InternalEList<?>)getSystemSlack()).basicRemove(otherEnd, msgs);
			case ResultPackage.REAL_TIME_SITUATION__TRACE:
				return ((InternalEList<?>)getTrace()).basicRemove(otherEnd, msgs);
			case ResultPackage.REAL_TIME_SITUATION__TRANSACTION:
				return ((InternalEList<?>)getTransaction()).basicRemove(otherEnd, msgs);
			case ResultPackage.REAL_TIME_SITUATION__PROCESSING_RESOURCE:
				return ((InternalEList<?>)getProcessingResource()).basicRemove(otherEnd, msgs);
			case ResultPackage.REAL_TIME_SITUATION__OPERATION:
				return ((InternalEList<?>)getOperation()).basicRemove(otherEnd, msgs);
			case ResultPackage.REAL_TIME_SITUATION__SCHEDULING_SERVER:
				return ((InternalEList<?>)getSchedulingServer()).basicRemove(otherEnd, msgs);
			case ResultPackage.REAL_TIME_SITUATION__SHARED_RESOURCE:
				return ((InternalEList<?>)getSharedResource()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResultPackage.REAL_TIME_SITUATION__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case ResultPackage.REAL_TIME_SITUATION__SYSTEM_SLACK:
				return getSystemSlack();
			case ResultPackage.REAL_TIME_SITUATION__TRACE:
				return getTrace();
			case ResultPackage.REAL_TIME_SITUATION__TRANSACTION:
				return getTransaction();
			case ResultPackage.REAL_TIME_SITUATION__PROCESSING_RESOURCE:
				return getProcessingResource();
			case ResultPackage.REAL_TIME_SITUATION__OPERATION:
				return getOperation();
			case ResultPackage.REAL_TIME_SITUATION__SCHEDULING_SERVER:
				return getSchedulingServer();
			case ResultPackage.REAL_TIME_SITUATION__SHARED_RESOURCE:
				return getSharedResource();
			case ResultPackage.REAL_TIME_SITUATION__GENERATION_DATE:
				return getGenerationDate();
			case ResultPackage.REAL_TIME_SITUATION__GENERATION_PROFILE:
				return getGenerationProfile();
			case ResultPackage.REAL_TIME_SITUATION__GENERATION_TOOL:
				return getGenerationTool();
			case ResultPackage.REAL_TIME_SITUATION__MODEL_DATE:
				return getModelDate();
			case ResultPackage.REAL_TIME_SITUATION__MODEL_NAME:
				return getModelName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResultPackage.REAL_TIME_SITUATION__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case ResultPackage.REAL_TIME_SITUATION__SYSTEM_SLACK:
				getSystemSlack().clear();
				getSystemSlack().addAll((Collection<? extends Slack>)newValue);
				return;
			case ResultPackage.REAL_TIME_SITUATION__TRACE:
				getTrace().clear();
				getTrace().addAll((Collection<? extends Trace>)newValue);
				return;
			case ResultPackage.REAL_TIME_SITUATION__TRANSACTION:
				getTransaction().clear();
				getTransaction().addAll((Collection<? extends Transaction_Results>)newValue);
				return;
			case ResultPackage.REAL_TIME_SITUATION__PROCESSING_RESOURCE:
				getProcessingResource().clear();
				getProcessingResource().addAll((Collection<? extends Processing_Resource_Results>)newValue);
				return;
			case ResultPackage.REAL_TIME_SITUATION__OPERATION:
				getOperation().clear();
				getOperation().addAll((Collection<? extends Operation_Results>)newValue);
				return;
			case ResultPackage.REAL_TIME_SITUATION__SCHEDULING_SERVER:
				getSchedulingServer().clear();
				getSchedulingServer().addAll((Collection<? extends Scheduling_Server_Results>)newValue);
				return;
			case ResultPackage.REAL_TIME_SITUATION__SHARED_RESOURCE:
				getSharedResource().clear();
				getSharedResource().addAll((Collection<? extends Shared_Resource_Results>)newValue);
				return;
			case ResultPackage.REAL_TIME_SITUATION__GENERATION_DATE:
				setGenerationDate((XMLGregorianCalendar)newValue);
				return;
			case ResultPackage.REAL_TIME_SITUATION__GENERATION_PROFILE:
				setGenerationProfile((String)newValue);
				return;
			case ResultPackage.REAL_TIME_SITUATION__GENERATION_TOOL:
				setGenerationTool((String)newValue);
				return;
			case ResultPackage.REAL_TIME_SITUATION__MODEL_DATE:
				setModelDate((XMLGregorianCalendar)newValue);
				return;
			case ResultPackage.REAL_TIME_SITUATION__MODEL_NAME:
				setModelName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResultPackage.REAL_TIME_SITUATION__GROUP:
				getGroup().clear();
				return;
			case ResultPackage.REAL_TIME_SITUATION__SYSTEM_SLACK:
				getSystemSlack().clear();
				return;
			case ResultPackage.REAL_TIME_SITUATION__TRACE:
				getTrace().clear();
				return;
			case ResultPackage.REAL_TIME_SITUATION__TRANSACTION:
				getTransaction().clear();
				return;
			case ResultPackage.REAL_TIME_SITUATION__PROCESSING_RESOURCE:
				getProcessingResource().clear();
				return;
			case ResultPackage.REAL_TIME_SITUATION__OPERATION:
				getOperation().clear();
				return;
			case ResultPackage.REAL_TIME_SITUATION__SCHEDULING_SERVER:
				getSchedulingServer().clear();
				return;
			case ResultPackage.REAL_TIME_SITUATION__SHARED_RESOURCE:
				getSharedResource().clear();
				return;
			case ResultPackage.REAL_TIME_SITUATION__GENERATION_DATE:
				setGenerationDate(GENERATION_DATE_EDEFAULT);
				return;
			case ResultPackage.REAL_TIME_SITUATION__GENERATION_PROFILE:
				setGenerationProfile(GENERATION_PROFILE_EDEFAULT);
				return;
			case ResultPackage.REAL_TIME_SITUATION__GENERATION_TOOL:
				setGenerationTool(GENERATION_TOOL_EDEFAULT);
				return;
			case ResultPackage.REAL_TIME_SITUATION__MODEL_DATE:
				setModelDate(MODEL_DATE_EDEFAULT);
				return;
			case ResultPackage.REAL_TIME_SITUATION__MODEL_NAME:
				setModelName(MODEL_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResultPackage.REAL_TIME_SITUATION__GROUP:
				return group != null && !group.isEmpty();
			case ResultPackage.REAL_TIME_SITUATION__SYSTEM_SLACK:
				return !getSystemSlack().isEmpty();
			case ResultPackage.REAL_TIME_SITUATION__TRACE:
				return !getTrace().isEmpty();
			case ResultPackage.REAL_TIME_SITUATION__TRANSACTION:
				return !getTransaction().isEmpty();
			case ResultPackage.REAL_TIME_SITUATION__PROCESSING_RESOURCE:
				return !getProcessingResource().isEmpty();
			case ResultPackage.REAL_TIME_SITUATION__OPERATION:
				return !getOperation().isEmpty();
			case ResultPackage.REAL_TIME_SITUATION__SCHEDULING_SERVER:
				return !getSchedulingServer().isEmpty();
			case ResultPackage.REAL_TIME_SITUATION__SHARED_RESOURCE:
				return !getSharedResource().isEmpty();
			case ResultPackage.REAL_TIME_SITUATION__GENERATION_DATE:
				return GENERATION_DATE_EDEFAULT == null ? generationDate != null : !GENERATION_DATE_EDEFAULT.equals(generationDate);
			case ResultPackage.REAL_TIME_SITUATION__GENERATION_PROFILE:
				return GENERATION_PROFILE_EDEFAULT == null ? generationProfile != null : !GENERATION_PROFILE_EDEFAULT.equals(generationProfile);
			case ResultPackage.REAL_TIME_SITUATION__GENERATION_TOOL:
				return GENERATION_TOOL_EDEFAULT == null ? generationTool != null : !GENERATION_TOOL_EDEFAULT.equals(generationTool);
			case ResultPackage.REAL_TIME_SITUATION__MODEL_DATE:
				return MODEL_DATE_EDEFAULT == null ? modelDate != null : !MODEL_DATE_EDEFAULT.equals(modelDate);
			case ResultPackage.REAL_TIME_SITUATION__MODEL_NAME:
				return MODEL_NAME_EDEFAULT == null ? modelName != null : !MODEL_NAME_EDEFAULT.equals(modelName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(", generationDate: ");
		result.append(generationDate);
		result.append(", generationProfile: ");
		result.append(generationProfile);
		result.append(", generationTool: ");
		result.append(generationTool);
		result.append(", modelDate: ");
		result.append(modelDate);
		result.append(", modelName: ");
		result.append(modelName);
		result.append(')');
		return result.toString();
	}

} //REAL_TIME_SITUATIONImpl
