/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Interrupt FP Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Interrupt_FP_Policy#getPreassigned <em>Preassigned</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Interrupt_FP_Policy#getThePriority <em>The Priority</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getInterrupt_FP_Policy()
 * @model extendedMetaData="name='Interrupt_FP_Policy' kind='empty'"
 * @generated
 */
public interface Interrupt_FP_Policy extends EObject {
	/**
	 * Returns the value of the '<em><b>Preassigned</b></em>' attribute.
	 * The literals are from the enumeration {@link es.esi.gemde.vv.mast.mastresult.Affirmative_Assertion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preassigned</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preassigned</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastresult.Affirmative_Assertion
	 * @see #isSetPreassigned()
	 * @see #unsetPreassigned()
	 * @see #setPreassigned(Affirmative_Assertion)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getInterrupt_FP_Policy_Preassigned()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='Preassigned'"
	 * @generated
	 */
	Affirmative_Assertion getPreassigned();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Interrupt_FP_Policy#getPreassigned <em>Preassigned</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preassigned</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastresult.Affirmative_Assertion
	 * @see #isSetPreassigned()
	 * @see #unsetPreassigned()
	 * @see #getPreassigned()
	 * @generated
	 */
	void setPreassigned(Affirmative_Assertion value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Interrupt_FP_Policy#getPreassigned <em>Preassigned</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPreassigned()
	 * @see #getPreassigned()
	 * @see #setPreassigned(Affirmative_Assertion)
	 * @generated
	 */
	void unsetPreassigned();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastresult.Interrupt_FP_Policy#getPreassigned <em>Preassigned</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Preassigned</em>' attribute is set.
	 * @see #unsetPreassigned()
	 * @see #getPreassigned()
	 * @see #setPreassigned(Affirmative_Assertion)
	 * @generated
	 */
	boolean isSetPreassigned();

	/**
	 * Returns the value of the '<em><b>The Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>The Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>The Priority</em>' attribute.
	 * @see #isSetThePriority()
	 * @see #unsetThePriority()
	 * @see #setThePriority(int)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getInterrupt_FP_Policy_ThePriority()
	 * @model unsettable="true" dataType="es.esi.gemde.vv.mast.mastresult.Priority"
	 *        extendedMetaData="kind='attribute' name='The_Priority'"
	 * @generated
	 */
	int getThePriority();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Interrupt_FP_Policy#getThePriority <em>The Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>The Priority</em>' attribute.
	 * @see #isSetThePriority()
	 * @see #unsetThePriority()
	 * @see #getThePriority()
	 * @generated
	 */
	void setThePriority(int value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Interrupt_FP_Policy#getThePriority <em>The Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetThePriority()
	 * @see #getThePriority()
	 * @see #setThePriority(int)
	 * @generated
	 */
	void unsetThePriority();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastresult.Interrupt_FP_Policy#getThePriority <em>The Priority</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>The Priority</em>' attribute is set.
	 * @see #unsetThePriority()
	 * @see #getThePriority()
	 * @see #setThePriority(int)
	 * @generated
	 */
	boolean isSetThePriority();

} // Interrupt_FP_Policy
