/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scheduling Server Results</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getNonPreemptibleFPPolicy <em>Non Preemptible FP Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getFixedPriorityPolicy <em>Fixed Priority Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getInterruptFPPolicy <em>Interrupt FP Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getPollingPolicy <em>Polling Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getSporadicServerPolicy <em>Sporadic Server Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getScheduling_Server_Results()
 * @model extendedMetaData="name='Scheduling_Server_Results' kind='elementOnly'"
 * @generated
 */
public interface Scheduling_Server_Results extends EObject {
	/**
	 * Returns the value of the '<em><b>Non Preemptible FP Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Non Preemptible FP Policy</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Non Preemptible FP Policy</em>' containment reference.
	 * @see #setNonPreemptibleFPPolicy(Non_Preemptible_FP_Policy)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getScheduling_Server_Results_NonPreemptibleFPPolicy()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Non_Preemptible_FP_Policy' namespace='##targetNamespace'"
	 * @generated
	 */
	Non_Preemptible_FP_Policy getNonPreemptibleFPPolicy();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getNonPreemptibleFPPolicy <em>Non Preemptible FP Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Non Preemptible FP Policy</em>' containment reference.
	 * @see #getNonPreemptibleFPPolicy()
	 * @generated
	 */
	void setNonPreemptibleFPPolicy(Non_Preemptible_FP_Policy value);

	/**
	 * Returns the value of the '<em><b>Fixed Priority Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fixed Priority Policy</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fixed Priority Policy</em>' containment reference.
	 * @see #setFixedPriorityPolicy(Fixed_Priority_Policy)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getScheduling_Server_Results_FixedPriorityPolicy()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Fixed_Priority_Policy' namespace='##targetNamespace'"
	 * @generated
	 */
	Fixed_Priority_Policy getFixedPriorityPolicy();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getFixedPriorityPolicy <em>Fixed Priority Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Fixed Priority Policy</em>' containment reference.
	 * @see #getFixedPriorityPolicy()
	 * @generated
	 */
	void setFixedPriorityPolicy(Fixed_Priority_Policy value);

	/**
	 * Returns the value of the '<em><b>Interrupt FP Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interrupt FP Policy</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interrupt FP Policy</em>' containment reference.
	 * @see #setInterruptFPPolicy(Interrupt_FP_Policy)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getScheduling_Server_Results_InterruptFPPolicy()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Interrupt_FP_Policy' namespace='##targetNamespace'"
	 * @generated
	 */
	Interrupt_FP_Policy getInterruptFPPolicy();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getInterruptFPPolicy <em>Interrupt FP Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interrupt FP Policy</em>' containment reference.
	 * @see #getInterruptFPPolicy()
	 * @generated
	 */
	void setInterruptFPPolicy(Interrupt_FP_Policy value);

	/**
	 * Returns the value of the '<em><b>Polling Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Polling Policy</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Polling Policy</em>' containment reference.
	 * @see #setPollingPolicy(Polling_Policy)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getScheduling_Server_Results_PollingPolicy()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Polling_Policy' namespace='##targetNamespace'"
	 * @generated
	 */
	Polling_Policy getPollingPolicy();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getPollingPolicy <em>Polling Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Polling Policy</em>' containment reference.
	 * @see #getPollingPolicy()
	 * @generated
	 */
	void setPollingPolicy(Polling_Policy value);

	/**
	 * Returns the value of the '<em><b>Sporadic Server Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sporadic Server Policy</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sporadic Server Policy</em>' containment reference.
	 * @see #setSporadicServerPolicy(Sporadic_Server_Policy)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getScheduling_Server_Results_SporadicServerPolicy()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Sporadic_Server_Policy' namespace='##targetNamespace'"
	 * @generated
	 */
	Sporadic_Server_Policy getSporadicServerPolicy();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getSporadicServerPolicy <em>Sporadic Server Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sporadic Server Policy</em>' containment reference.
	 * @see #getSporadicServerPolicy()
	 * @generated
	 */
	void setSporadicServerPolicy(Sporadic_Server_Policy value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getScheduling_Server_Results_Name()
	 * @model id="true" dataType="es.esi.gemde.vv.mast.mastresult.Identifier" required="true"
	 *        extendedMetaData="kind='attribute' name='Name'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Scheduling_Server_Results
