/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see es.esi.gemde.vv.mast.mastresult.ResultFactory
 * @model kind="package"
 * @generated
 */
public interface ResultPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mastresult";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://mast.unican.es/xmlmast/result";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mast_res";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ResultPackage eINSTANCE = es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl.init();

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Detailed_UtilizationImpl <em>Detailed Utilization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Detailed_UtilizationImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getDetailed_Utilization()
	 * @generated
	 */
	int DETAILED_UTILIZATION = 0;

	/**
	 * The feature id for the '<em><b>Application</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETAILED_UTILIZATION__APPLICATION = 0;

	/**
	 * The feature id for the '<em><b>Context Switch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETAILED_UTILIZATION__CONTEXT_SWITCH = 1;

	/**
	 * The feature id for the '<em><b>Driver</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETAILED_UTILIZATION__DRIVER = 2;

	/**
	 * The feature id for the '<em><b>Timer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETAILED_UTILIZATION__TIMER = 3;

	/**
	 * The feature id for the '<em><b>Total</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETAILED_UTILIZATION__TOTAL = 4;

	/**
	 * The number of structural features of the '<em>Detailed Utilization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DETAILED_UTILIZATION_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Document_RootImpl <em>Document Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Document_RootImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getDocument_Root()
	 * @generated
	 */
	int DOCUMENT_ROOT = 1;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__MIXED = 0;

	/**
	 * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

	/**
	 * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

	/**
	 * The feature id for the '<em><b>REALTIMESITUATION</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__REALTIMESITUATION = 3;

	/**
	 * The number of structural features of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Fixed_Priority_PolicyImpl <em>Fixed Priority Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Fixed_Priority_PolicyImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getFixed_Priority_Policy()
	 * @generated
	 */
	int FIXED_PRIORITY_POLICY = 2;

	/**
	 * The feature id for the '<em><b>Preassigned</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_PRIORITY_POLICY__PREASSIGNED = 0;

	/**
	 * The feature id for the '<em><b>The Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_PRIORITY_POLICY__THE_PRIORITY = 1;

	/**
	 * The number of structural features of the '<em>Fixed Priority Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIXED_PRIORITY_POLICY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Global_Miss_RatioImpl <em>Global Miss Ratio</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Global_Miss_RatioImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getGlobal_Miss_Ratio()
	 * @generated
	 */
	int GLOBAL_MISS_RATIO = 3;

	/**
	 * The feature id for the '<em><b>Miss Ratios</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLOBAL_MISS_RATIO__MISS_RATIOS = 0;

	/**
	 * The feature id for the '<em><b>Referenced Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLOBAL_MISS_RATIO__REFERENCED_EVENT = 1;

	/**
	 * The number of structural features of the '<em>Global Miss Ratio</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLOBAL_MISS_RATIO_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Global_Miss_Ratio_ListImpl <em>Global Miss Ratio List</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Global_Miss_Ratio_ListImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getGlobal_Miss_Ratio_List()
	 * @generated
	 */
	int GLOBAL_MISS_RATIO_LIST = 4;

	/**
	 * The feature id for the '<em><b>Global Miss Ratio</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLOBAL_MISS_RATIO_LIST__GLOBAL_MISS_RATIO = 0;

	/**
	 * The number of structural features of the '<em>Global Miss Ratio List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLOBAL_MISS_RATIO_LIST_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Global_Response_TimeImpl <em>Global Response Time</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Global_Response_TimeImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getGlobal_Response_Time()
	 * @generated
	 */
	int GLOBAL_RESPONSE_TIME = 5;

	/**
	 * The feature id for the '<em><b>Referenced Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLOBAL_RESPONSE_TIME__REFERENCED_EVENT = 0;

	/**
	 * The feature id for the '<em><b>Time Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLOBAL_RESPONSE_TIME__TIME_VALUE = 1;

	/**
	 * The number of structural features of the '<em>Global Response Time</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLOBAL_RESPONSE_TIME_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Global_Response_Time_ListImpl <em>Global Response Time List</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Global_Response_Time_ListImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getGlobal_Response_Time_List()
	 * @generated
	 */
	int GLOBAL_RESPONSE_TIME_LIST = 6;

	/**
	 * The feature id for the '<em><b>Global Response Time</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLOBAL_RESPONSE_TIME_LIST__GLOBAL_RESPONSE_TIME = 0;

	/**
	 * The number of structural features of the '<em>Global Response Time List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GLOBAL_RESPONSE_TIME_LIST_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Interrupt_FP_PolicyImpl <em>Interrupt FP Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Interrupt_FP_PolicyImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getInterrupt_FP_Policy()
	 * @generated
	 */
	int INTERRUPT_FP_POLICY = 7;

	/**
	 * The feature id for the '<em><b>Preassigned</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERRUPT_FP_POLICY__PREASSIGNED = 0;

	/**
	 * The feature id for the '<em><b>The Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERRUPT_FP_POLICY__THE_PRIORITY = 1;

	/**
	 * The number of structural features of the '<em>Interrupt FP Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERRUPT_FP_POLICY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Miss_RatioImpl <em>Miss Ratio</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Miss_RatioImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getMiss_Ratio()
	 * @generated
	 */
	int MISS_RATIO = 8;

	/**
	 * The feature id for the '<em><b>Deadline</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISS_RATIO__DEADLINE = 0;

	/**
	 * The feature id for the '<em><b>Ratio</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISS_RATIO__RATIO = 1;

	/**
	 * The number of structural features of the '<em>Miss Ratio</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISS_RATIO_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Miss_Ratio_ListImpl <em>Miss Ratio List</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Miss_Ratio_ListImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getMiss_Ratio_List()
	 * @generated
	 */
	int MISS_RATIO_LIST = 9;

	/**
	 * The feature id for the '<em><b>Miss Ratio</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISS_RATIO_LIST__MISS_RATIO = 0;

	/**
	 * The number of structural features of the '<em>Miss Ratio List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MISS_RATIO_LIST_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Non_Preemptible_FP_PolicyImpl <em>Non Preemptible FP Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Non_Preemptible_FP_PolicyImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getNon_Preemptible_FP_Policy()
	 * @generated
	 */
	int NON_PREEMPTIBLE_FP_POLICY = 10;

	/**
	 * The feature id for the '<em><b>Preassigned</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_PREEMPTIBLE_FP_POLICY__PREASSIGNED = 0;

	/**
	 * The feature id for the '<em><b>The Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_PREEMPTIBLE_FP_POLICY__THE_PRIORITY = 1;

	/**
	 * The number of structural features of the '<em>Non Preemptible FP Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_PREEMPTIBLE_FP_POLICY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Operation_ResultsImpl <em>Operation Results</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Operation_ResultsImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getOperation_Results()
	 * @generated
	 */
	int OPERATION_RESULTS = 11;

	/**
	 * The feature id for the '<em><b>Slack</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_RESULTS__SLACK = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_RESULTS__NAME = 1;

	/**
	 * The number of structural features of the '<em>Operation Results</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_RESULTS_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Polling_PolicyImpl <em>Polling Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Polling_PolicyImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getPolling_Policy()
	 * @generated
	 */
	int POLLING_POLICY = 12;

	/**
	 * The feature id for the '<em><b>Polling Avg Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLLING_POLICY__POLLING_AVG_OVERHEAD = 0;

	/**
	 * The feature id for the '<em><b>Polling Best Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLLING_POLICY__POLLING_BEST_OVERHEAD = 1;

	/**
	 * The feature id for the '<em><b>Polling Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLLING_POLICY__POLLING_PERIOD = 2;

	/**
	 * The feature id for the '<em><b>Polling Worst Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLLING_POLICY__POLLING_WORST_OVERHEAD = 3;

	/**
	 * The feature id for the '<em><b>Preassigned</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLLING_POLICY__PREASSIGNED = 4;

	/**
	 * The feature id for the '<em><b>The Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLLING_POLICY__THE_PRIORITY = 5;

	/**
	 * The number of structural features of the '<em>Polling Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int POLLING_POLICY_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Priority_CeilingImpl <em>Priority Ceiling</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Priority_CeilingImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getPriority_Ceiling()
	 * @generated
	 */
	int PRIORITY_CEILING = 13;

	/**
	 * The feature id for the '<em><b>Ceiling</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIORITY_CEILING__CEILING = 0;

	/**
	 * The number of structural features of the '<em>Priority Ceiling</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PRIORITY_CEILING_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Processing_Resource_ResultsImpl <em>Processing Resource Results</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Processing_Resource_ResultsImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getProcessing_Resource_Results()
	 * @generated
	 */
	int PROCESSING_RESOURCE_RESULTS = 14;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSING_RESOURCE_RESULTS__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Slack</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSING_RESOURCE_RESULTS__SLACK = 1;

	/**
	 * The feature id for the '<em><b>Detailed Utilization</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSING_RESOURCE_RESULTS__DETAILED_UTILIZATION = 2;

	/**
	 * The feature id for the '<em><b>Utilization</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSING_RESOURCE_RESULTS__UTILIZATION = 3;

	/**
	 * The feature id for the '<em><b>Ready Queue Size</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSING_RESOURCE_RESULTS__READY_QUEUE_SIZE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSING_RESOURCE_RESULTS__NAME = 5;

	/**
	 * The number of structural features of the '<em>Processing Resource Results</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSING_RESOURCE_RESULTS_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Queue_SizeImpl <em>Queue Size</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Queue_SizeImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getQueue_Size()
	 * @generated
	 */
	int QUEUE_SIZE = 15;

	/**
	 * The feature id for the '<em><b>Max Num</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUEUE_SIZE__MAX_NUM = 0;

	/**
	 * The number of structural features of the '<em>Queue Size</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUEUE_SIZE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Ready_Queue_SizeImpl <em>Ready Queue Size</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Ready_Queue_SizeImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getReady_Queue_Size()
	 * @generated
	 */
	int READY_QUEUE_SIZE = 16;

	/**
	 * The feature id for the '<em><b>Max Num</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READY_QUEUE_SIZE__MAX_NUM = 0;

	/**
	 * The number of structural features of the '<em>Ready Queue Size</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int READY_QUEUE_SIZE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.REAL_TIME_SITUATIONImpl <em>REAL TIME SITUATION</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.REAL_TIME_SITUATIONImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getREAL_TIME_SITUATION()
	 * @generated
	 */
	int REAL_TIME_SITUATION = 17;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_TIME_SITUATION__GROUP = 0;

	/**
	 * The feature id for the '<em><b>System Slack</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_TIME_SITUATION__SYSTEM_SLACK = 1;

	/**
	 * The feature id for the '<em><b>Trace</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_TIME_SITUATION__TRACE = 2;

	/**
	 * The feature id for the '<em><b>Transaction</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_TIME_SITUATION__TRANSACTION = 3;

	/**
	 * The feature id for the '<em><b>Processing Resource</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_TIME_SITUATION__PROCESSING_RESOURCE = 4;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_TIME_SITUATION__OPERATION = 5;

	/**
	 * The feature id for the '<em><b>Scheduling Server</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_TIME_SITUATION__SCHEDULING_SERVER = 6;

	/**
	 * The feature id for the '<em><b>Shared Resource</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_TIME_SITUATION__SHARED_RESOURCE = 7;

	/**
	 * The feature id for the '<em><b>Generation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_TIME_SITUATION__GENERATION_DATE = 8;

	/**
	 * The feature id for the '<em><b>Generation Profile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_TIME_SITUATION__GENERATION_PROFILE = 9;

	/**
	 * The feature id for the '<em><b>Generation Tool</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_TIME_SITUATION__GENERATION_TOOL = 10;

	/**
	 * The feature id for the '<em><b>Model Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_TIME_SITUATION__MODEL_DATE = 11;

	/**
	 * The feature id for the '<em><b>Model Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_TIME_SITUATION__MODEL_NAME = 12;

	/**
	 * The number of structural features of the '<em>REAL TIME SITUATION</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_TIME_SITUATION_FEATURE_COUNT = 13;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Scheduling_Server_ResultsImpl <em>Scheduling Server Results</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Scheduling_Server_ResultsImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getScheduling_Server_Results()
	 * @generated
	 */
	int SCHEDULING_SERVER_RESULTS = 18;

	/**
	 * The feature id for the '<em><b>Non Preemptible FP Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULING_SERVER_RESULTS__NON_PREEMPTIBLE_FP_POLICY = 0;

	/**
	 * The feature id for the '<em><b>Fixed Priority Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULING_SERVER_RESULTS__FIXED_PRIORITY_POLICY = 1;

	/**
	 * The feature id for the '<em><b>Interrupt FP Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULING_SERVER_RESULTS__INTERRUPT_FP_POLICY = 2;

	/**
	 * The feature id for the '<em><b>Polling Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULING_SERVER_RESULTS__POLLING_POLICY = 3;

	/**
	 * The feature id for the '<em><b>Sporadic Server Policy</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULING_SERVER_RESULTS__SPORADIC_SERVER_POLICY = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULING_SERVER_RESULTS__NAME = 5;

	/**
	 * The number of structural features of the '<em>Scheduling Server Results</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULING_SERVER_RESULTS_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Shared_Resource_ResultsImpl <em>Shared Resource Results</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Shared_Resource_ResultsImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getShared_Resource_Results()
	 * @generated
	 */
	int SHARED_RESOURCE_RESULTS = 19;

	/**
	 * The feature id for the '<em><b>Priority Ceiling</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARED_RESOURCE_RESULTS__PRIORITY_CEILING = 0;

	/**
	 * The feature id for the '<em><b>Utilization</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARED_RESOURCE_RESULTS__UTILIZATION = 1;

	/**
	 * The feature id for the '<em><b>Queue Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARED_RESOURCE_RESULTS__QUEUE_SIZE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARED_RESOURCE_RESULTS__NAME = 3;

	/**
	 * The number of structural features of the '<em>Shared Resource Results</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHARED_RESOURCE_RESULTS_FEATURE_COUNT = 4;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Simulation_Timing_ResultImpl <em>Simulation Timing Result</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Simulation_Timing_ResultImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getSimulation_Timing_Result()
	 * @generated
	 */
	int SIMULATION_TIMING_RESULT = 20;

	/**
	 * The feature id for the '<em><b>Worst Global Response Times</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_TIMING_RESULT__WORST_GLOBAL_RESPONSE_TIMES = 0;

	/**
	 * The feature id for the '<em><b>Avg Global Response Times</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_TIMING_RESULT__AVG_GLOBAL_RESPONSE_TIMES = 1;

	/**
	 * The feature id for the '<em><b>Best Global Response Times</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_TIMING_RESULT__BEST_GLOBAL_RESPONSE_TIMES = 2;

	/**
	 * The feature id for the '<em><b>Jitters</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_TIMING_RESULT__JITTERS = 3;

	/**
	 * The feature id for the '<em><b>Local Miss Ratios</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_TIMING_RESULT__LOCAL_MISS_RATIOS = 4;

	/**
	 * The feature id for the '<em><b>Global Miss Ratios</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_TIMING_RESULT__GLOBAL_MISS_RATIOS = 5;

	/**
	 * The feature id for the '<em><b>Avg Blocking Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_TIMING_RESULT__AVG_BLOCKING_TIME = 6;

	/**
	 * The feature id for the '<em><b>Avg Local Response Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_TIMING_RESULT__AVG_LOCAL_RESPONSE_TIME = 7;

	/**
	 * The feature id for the '<em><b>Best Local Response Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_TIMING_RESULT__BEST_LOCAL_RESPONSE_TIME = 8;

	/**
	 * The feature id for the '<em><b>Event Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_TIMING_RESULT__EVENT_NAME = 9;

	/**
	 * The feature id for the '<em><b>Max Preemption Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_TIMING_RESULT__MAX_PREEMPTION_TIME = 10;

	/**
	 * The feature id for the '<em><b>Num Of Queued Activations</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_TIMING_RESULT__NUM_OF_QUEUED_ACTIVATIONS = 11;

	/**
	 * The feature id for the '<em><b>Num Of Suspensions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_TIMING_RESULT__NUM_OF_SUSPENSIONS = 12;

	/**
	 * The feature id for the '<em><b>Suspension Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_TIMING_RESULT__SUSPENSION_TIME = 13;

	/**
	 * The feature id for the '<em><b>Worst Blocking Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_TIMING_RESULT__WORST_BLOCKING_TIME = 14;

	/**
	 * The feature id for the '<em><b>Worst Local Response Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_TIMING_RESULT__WORST_LOCAL_RESPONSE_TIME = 15;

	/**
	 * The number of structural features of the '<em>Simulation Timing Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SIMULATION_TIMING_RESULT_FEATURE_COUNT = 16;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.SlackImpl <em>Slack</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.SlackImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getSlack()
	 * @generated
	 */
	int SLACK = 21;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLACK__VALUE = 0;

	/**
	 * The number of structural features of the '<em>Slack</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLACK_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Sporadic_Server_PolicyImpl <em>Sporadic Server Policy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Sporadic_Server_PolicyImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getSporadic_Server_Policy()
	 * @generated
	 */
	int SPORADIC_SERVER_POLICY = 22;

	/**
	 * The feature id for the '<em><b>Background Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPORADIC_SERVER_POLICY__BACKGROUND_PRIORITY = 0;

	/**
	 * The feature id for the '<em><b>Initial Capacity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPORADIC_SERVER_POLICY__INITIAL_CAPACITY = 1;

	/**
	 * The feature id for the '<em><b>Max Pending Replenishments</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPORADIC_SERVER_POLICY__MAX_PENDING_REPLENISHMENTS = 2;

	/**
	 * The feature id for the '<em><b>Normal Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPORADIC_SERVER_POLICY__NORMAL_PRIORITY = 3;

	/**
	 * The feature id for the '<em><b>Preassigned</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPORADIC_SERVER_POLICY__PREASSIGNED = 4;

	/**
	 * The feature id for the '<em><b>Replenishment Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPORADIC_SERVER_POLICY__REPLENISHMENT_PERIOD = 5;

	/**
	 * The number of structural features of the '<em>Sporadic Server Policy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SPORADIC_SERVER_POLICY_FEATURE_COUNT = 6;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Timing_ResultImpl <em>Timing Result</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Timing_ResultImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getTiming_Result()
	 * @generated
	 */
	int TIMING_RESULT = 23;

	/**
	 * The feature id for the '<em><b>Worst Global Response Times</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_RESULT__WORST_GLOBAL_RESPONSE_TIMES = 0;

	/**
	 * The feature id for the '<em><b>Best Global Response Times</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_RESULT__BEST_GLOBAL_RESPONSE_TIMES = 1;

	/**
	 * The feature id for the '<em><b>Jitters</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_RESULT__JITTERS = 2;

	/**
	 * The feature id for the '<em><b>Best Local Response Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_RESULT__BEST_LOCAL_RESPONSE_TIME = 3;

	/**
	 * The feature id for the '<em><b>Event Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_RESULT__EVENT_NAME = 4;

	/**
	 * The feature id for the '<em><b>Num Of Suspensions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_RESULT__NUM_OF_SUSPENSIONS = 5;

	/**
	 * The feature id for the '<em><b>Worst Blocking Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_RESULT__WORST_BLOCKING_TIME = 6;

	/**
	 * The feature id for the '<em><b>Worst Local Response Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_RESULT__WORST_LOCAL_RESPONSE_TIME = 7;

	/**
	 * The number of structural features of the '<em>Timing Result</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMING_RESULT_FEATURE_COUNT = 8;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.TraceImpl <em>Trace</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.TraceImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getTrace()
	 * @generated
	 */
	int TRACE = 24;

	/**
	 * The feature id for the '<em><b>Pathname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE__PATHNAME = 0;

	/**
	 * The number of structural features of the '<em>Trace</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRACE_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Transaction_ResultsImpl <em>Transaction Results</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.Transaction_ResultsImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getTransaction_Results()
	 * @generated
	 */
	int TRANSACTION_RESULTS = 25;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSACTION_RESULTS__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Slack</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSACTION_RESULTS__SLACK = 1;

	/**
	 * The feature id for the '<em><b>Timing Result</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSACTION_RESULTS__TIMING_RESULT = 2;

	/**
	 * The feature id for the '<em><b>Simulation Timing Result</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSACTION_RESULTS__SIMULATION_TIMING_RESULT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSACTION_RESULTS__NAME = 4;

	/**
	 * The number of structural features of the '<em>Transaction Results</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSACTION_RESULTS_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.impl.UtilizationImpl <em>Utilization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.UtilizationImpl
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getUtilization()
	 * @generated
	 */
	int UTILIZATION = 26;

	/**
	 * The feature id for the '<em><b>Total</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTILIZATION__TOTAL = 0;

	/**
	 * The number of structural features of the '<em>Utilization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTILIZATION_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.Affirmative_Assertion <em>Affirmative Assertion</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.Affirmative_Assertion
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getAffirmative_Assertion()
	 * @generated
	 */
	int AFFIRMATIVE_ASSERTION = 27;

	/**
	 * The meta object id for the '{@link es.esi.gemde.vv.mast.mastresult.Assertion <em>Assertion</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.Assertion
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getAssertion()
	 * @generated
	 */
	int ASSERTION = 28;

	/**
	 * The meta object id for the '<em>Affirmative Assertion Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.Affirmative_Assertion
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getAffirmative_Assertion_Object()
	 * @generated
	 */
	int AFFIRMATIVE_ASSERTION_OBJECT = 29;

	/**
	 * The meta object id for the '<em>Assertion Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.Assertion
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getAssertion_Object()
	 * @generated
	 */
	int ASSERTION_OBJECT = 30;

	/**
	 * The meta object id for the '<em>Date Time</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see javax.xml.datatype.XMLGregorianCalendar
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getDate_Time()
	 * @generated
	 */
	int DATE_TIME = 31;

	/**
	 * The meta object id for the '<em>External Reference</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getExternal_Reference()
	 * @generated
	 */
	int EXTERNAL_REFERENCE = 32;

	/**
	 * The meta object id for the '<em>Factor</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getFactor()
	 * @generated
	 */
	int FACTOR = 33;

	/**
	 * The meta object id for the '<em>Factor Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.Float
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getFactor_Object()
	 * @generated
	 */
	int FACTOR_OBJECT = 34;

	/**
	 * The meta object id for the '<em>Identifier</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.String
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getIdentifier()
	 * @generated
	 */
	int IDENTIFIER = 35;

	/**
	 * The meta object id for the '<em>Normalized Execution Time</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getNormalized_Execution_Time()
	 * @generated
	 */
	int NORMALIZED_EXECUTION_TIME = 36;

	/**
	 * The meta object id for the '<em>Normalized Execution Time Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.Float
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getNormalized_Execution_Time_Object()
	 * @generated
	 */
	int NORMALIZED_EXECUTION_TIME_OBJECT = 37;

	/**
	 * The meta object id for the '<em>Percentage</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getPercentage()
	 * @generated
	 */
	int PERCENTAGE = 38;

	/**
	 * The meta object id for the '<em>Percentage Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.Float
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getPercentage_Object()
	 * @generated
	 */
	int PERCENTAGE_OBJECT = 39;

	/**
	 * The meta object id for the '<em>Priority</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getPriority()
	 * @generated
	 */
	int PRIORITY = 40;

	/**
	 * The meta object id for the '<em>Priority Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.Integer
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getPriority_Object()
	 * @generated
	 */
	int PRIORITY_OBJECT = 41;

	/**
	 * The meta object id for the '<em>Time</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getTime()
	 * @generated
	 */
	int TIME = 42;

	/**
	 * The meta object id for the '<em>Time Object</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.lang.Float
	 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getTime_Object()
	 * @generated
	 */
	int TIME_OBJECT = 43;


	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Detailed_Utilization <em>Detailed Utilization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Detailed Utilization</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Detailed_Utilization
	 * @generated
	 */
	EClass getDetailed_Utilization();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Detailed_Utilization#getApplication <em>Application</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Application</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Detailed_Utilization#getApplication()
	 * @see #getDetailed_Utilization()
	 * @generated
	 */
	EAttribute getDetailed_Utilization_Application();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Detailed_Utilization#getContextSwitch <em>Context Switch</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Context Switch</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Detailed_Utilization#getContextSwitch()
	 * @see #getDetailed_Utilization()
	 * @generated
	 */
	EAttribute getDetailed_Utilization_ContextSwitch();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Detailed_Utilization#getDriver <em>Driver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Driver</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Detailed_Utilization#getDriver()
	 * @see #getDetailed_Utilization()
	 * @generated
	 */
	EAttribute getDetailed_Utilization_Driver();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Detailed_Utilization#getTimer <em>Timer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timer</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Detailed_Utilization#getTimer()
	 * @see #getDetailed_Utilization()
	 * @generated
	 */
	EAttribute getDetailed_Utilization_Timer();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Detailed_Utilization#getTotal <em>Total</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Total</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Detailed_Utilization#getTotal()
	 * @see #getDetailed_Utilization()
	 * @generated
	 */
	EAttribute getDetailed_Utilization_Total();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Document_Root <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Root</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Document_Root
	 * @generated
	 */
	EClass getDocument_Root();

	/**
	 * Returns the meta object for the attribute list '{@link es.esi.gemde.vv.mast.mastresult.Document_Root#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Document_Root#getMixed()
	 * @see #getDocument_Root()
	 * @generated
	 */
	EAttribute getDocument_Root_Mixed();

	/**
	 * Returns the meta object for the map '{@link es.esi.gemde.vv.mast.mastresult.Document_Root#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Document_Root#getXMLNSPrefixMap()
	 * @see #getDocument_Root()
	 * @generated
	 */
	EReference getDocument_Root_XMLNSPrefixMap();

	/**
	 * Returns the meta object for the map '{@link es.esi.gemde.vv.mast.mastresult.Document_Root#getXSISchemaLocation <em>XSI Schema Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XSI Schema Location</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Document_Root#getXSISchemaLocation()
	 * @see #getDocument_Root()
	 * @generated
	 */
	EReference getDocument_Root_XSISchemaLocation();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastresult.Document_Root#getREALTIMESITUATION <em>REALTIMESITUATION</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>REALTIMESITUATION</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Document_Root#getREALTIMESITUATION()
	 * @see #getDocument_Root()
	 * @generated
	 */
	EReference getDocument_Root_REALTIMESITUATION();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Fixed_Priority_Policy <em>Fixed Priority Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Fixed Priority Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Fixed_Priority_Policy
	 * @generated
	 */
	EClass getFixed_Priority_Policy();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Fixed_Priority_Policy#getPreassigned <em>Preassigned</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Preassigned</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Fixed_Priority_Policy#getPreassigned()
	 * @see #getFixed_Priority_Policy()
	 * @generated
	 */
	EAttribute getFixed_Priority_Policy_Preassigned();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Fixed_Priority_Policy#getThePriority <em>The Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>The Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Fixed_Priority_Policy#getThePriority()
	 * @see #getFixed_Priority_Policy()
	 * @generated
	 */
	EAttribute getFixed_Priority_Policy_ThePriority();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio <em>Global Miss Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Global Miss Ratio</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio
	 * @generated
	 */
	EClass getGlobal_Miss_Ratio();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio#getMissRatios <em>Miss Ratios</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Miss Ratios</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio#getMissRatios()
	 * @see #getGlobal_Miss_Ratio()
	 * @generated
	 */
	EReference getGlobal_Miss_Ratio_MissRatios();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio#getReferencedEvent <em>Referenced Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Referenced Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio#getReferencedEvent()
	 * @see #getGlobal_Miss_Ratio()
	 * @generated
	 */
	EAttribute getGlobal_Miss_Ratio_ReferencedEvent();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio_List <em>Global Miss Ratio List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Global Miss Ratio List</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio_List
	 * @generated
	 */
	EClass getGlobal_Miss_Ratio_List();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio_List#getGlobalMissRatio <em>Global Miss Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Global Miss Ratio</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio_List#getGlobalMissRatio()
	 * @see #getGlobal_Miss_Ratio_List()
	 * @generated
	 */
	EReference getGlobal_Miss_Ratio_List_GlobalMissRatio();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Global_Response_Time <em>Global Response Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Global Response Time</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Global_Response_Time
	 * @generated
	 */
	EClass getGlobal_Response_Time();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Global_Response_Time#getReferencedEvent <em>Referenced Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Referenced Event</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Global_Response_Time#getReferencedEvent()
	 * @see #getGlobal_Response_Time()
	 * @generated
	 */
	EAttribute getGlobal_Response_Time_ReferencedEvent();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Global_Response_Time#getTimeValue <em>Time Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Value</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Global_Response_Time#getTimeValue()
	 * @see #getGlobal_Response_Time()
	 * @generated
	 */
	EAttribute getGlobal_Response_Time_TimeValue();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Global_Response_Time_List <em>Global Response Time List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Global Response Time List</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Global_Response_Time_List
	 * @generated
	 */
	EClass getGlobal_Response_Time_List();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastresult.Global_Response_Time_List#getGlobalResponseTime <em>Global Response Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Global Response Time</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Global_Response_Time_List#getGlobalResponseTime()
	 * @see #getGlobal_Response_Time_List()
	 * @generated
	 */
	EReference getGlobal_Response_Time_List_GlobalResponseTime();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Interrupt_FP_Policy <em>Interrupt FP Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interrupt FP Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Interrupt_FP_Policy
	 * @generated
	 */
	EClass getInterrupt_FP_Policy();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Interrupt_FP_Policy#getPreassigned <em>Preassigned</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Preassigned</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Interrupt_FP_Policy#getPreassigned()
	 * @see #getInterrupt_FP_Policy()
	 * @generated
	 */
	EAttribute getInterrupt_FP_Policy_Preassigned();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Interrupt_FP_Policy#getThePriority <em>The Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>The Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Interrupt_FP_Policy#getThePriority()
	 * @see #getInterrupt_FP_Policy()
	 * @generated
	 */
	EAttribute getInterrupt_FP_Policy_ThePriority();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Miss_Ratio <em>Miss Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Miss Ratio</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Miss_Ratio
	 * @generated
	 */
	EClass getMiss_Ratio();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Miss_Ratio#getDeadline <em>Deadline</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deadline</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Miss_Ratio#getDeadline()
	 * @see #getMiss_Ratio()
	 * @generated
	 */
	EAttribute getMiss_Ratio_Deadline();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Miss_Ratio#getRatio <em>Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ratio</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Miss_Ratio#getRatio()
	 * @see #getMiss_Ratio()
	 * @generated
	 */
	EAttribute getMiss_Ratio_Ratio();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Miss_Ratio_List <em>Miss Ratio List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Miss Ratio List</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Miss_Ratio_List
	 * @generated
	 */
	EClass getMiss_Ratio_List();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastresult.Miss_Ratio_List#getMissRatio <em>Miss Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Miss Ratio</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Miss_Ratio_List#getMissRatio()
	 * @see #getMiss_Ratio_List()
	 * @generated
	 */
	EReference getMiss_Ratio_List_MissRatio();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Non_Preemptible_FP_Policy <em>Non Preemptible FP Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Non Preemptible FP Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Non_Preemptible_FP_Policy
	 * @generated
	 */
	EClass getNon_Preemptible_FP_Policy();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Non_Preemptible_FP_Policy#getPreassigned <em>Preassigned</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Preassigned</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Non_Preemptible_FP_Policy#getPreassigned()
	 * @see #getNon_Preemptible_FP_Policy()
	 * @generated
	 */
	EAttribute getNon_Preemptible_FP_Policy_Preassigned();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Non_Preemptible_FP_Policy#getThePriority <em>The Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>The Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Non_Preemptible_FP_Policy#getThePriority()
	 * @see #getNon_Preemptible_FP_Policy()
	 * @generated
	 */
	EAttribute getNon_Preemptible_FP_Policy_ThePriority();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Operation_Results <em>Operation Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation Results</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Operation_Results
	 * @generated
	 */
	EClass getOperation_Results();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastresult.Operation_Results#getSlack <em>Slack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Slack</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Operation_Results#getSlack()
	 * @see #getOperation_Results()
	 * @generated
	 */
	EReference getOperation_Results_Slack();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Operation_Results#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Operation_Results#getName()
	 * @see #getOperation_Results()
	 * @generated
	 */
	EAttribute getOperation_Results_Name();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy <em>Polling Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Polling Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Polling_Policy
	 * @generated
	 */
	EClass getPolling_Policy();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingAvgOverhead <em>Polling Avg Overhead</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Polling Avg Overhead</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingAvgOverhead()
	 * @see #getPolling_Policy()
	 * @generated
	 */
	EAttribute getPolling_Policy_PollingAvgOverhead();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingBestOverhead <em>Polling Best Overhead</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Polling Best Overhead</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingBestOverhead()
	 * @see #getPolling_Policy()
	 * @generated
	 */
	EAttribute getPolling_Policy_PollingBestOverhead();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingPeriod <em>Polling Period</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Polling Period</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingPeriod()
	 * @see #getPolling_Policy()
	 * @generated
	 */
	EAttribute getPolling_Policy_PollingPeriod();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingWorstOverhead <em>Polling Worst Overhead</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Polling Worst Overhead</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingWorstOverhead()
	 * @see #getPolling_Policy()
	 * @generated
	 */
	EAttribute getPolling_Policy_PollingWorstOverhead();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPreassigned <em>Preassigned</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Preassigned</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPreassigned()
	 * @see #getPolling_Policy()
	 * @generated
	 */
	EAttribute getPolling_Policy_Preassigned();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getThePriority <em>The Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>The Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Polling_Policy#getThePriority()
	 * @see #getPolling_Policy()
	 * @generated
	 */
	EAttribute getPolling_Policy_ThePriority();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Priority_Ceiling <em>Priority Ceiling</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Priority Ceiling</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Priority_Ceiling
	 * @generated
	 */
	EClass getPriority_Ceiling();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Priority_Ceiling#getCeiling <em>Ceiling</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ceiling</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Priority_Ceiling#getCeiling()
	 * @see #getPriority_Ceiling()
	 * @generated
	 */
	EAttribute getPriority_Ceiling_Ceiling();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results <em>Processing Resource Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Processing Resource Results</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results
	 * @generated
	 */
	EClass getProcessing_Resource_Results();

	/**
	 * Returns the meta object for the attribute list '{@link es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results#getGroup()
	 * @see #getProcessing_Resource_Results()
	 * @generated
	 */
	EAttribute getProcessing_Resource_Results_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results#getSlack <em>Slack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Slack</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results#getSlack()
	 * @see #getProcessing_Resource_Results()
	 * @generated
	 */
	EReference getProcessing_Resource_Results_Slack();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results#getDetailedUtilization <em>Detailed Utilization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Detailed Utilization</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results#getDetailedUtilization()
	 * @see #getProcessing_Resource_Results()
	 * @generated
	 */
	EReference getProcessing_Resource_Results_DetailedUtilization();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results#getUtilization <em>Utilization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Utilization</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results#getUtilization()
	 * @see #getProcessing_Resource_Results()
	 * @generated
	 */
	EReference getProcessing_Resource_Results_Utilization();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results#getReadyQueueSize <em>Ready Queue Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Ready Queue Size</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results#getReadyQueueSize()
	 * @see #getProcessing_Resource_Results()
	 * @generated
	 */
	EReference getProcessing_Resource_Results_ReadyQueueSize();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results#getName()
	 * @see #getProcessing_Resource_Results()
	 * @generated
	 */
	EAttribute getProcessing_Resource_Results_Name();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Queue_Size <em>Queue Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Queue Size</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Queue_Size
	 * @generated
	 */
	EClass getQueue_Size();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Queue_Size#getMaxNum <em>Max Num</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Num</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Queue_Size#getMaxNum()
	 * @see #getQueue_Size()
	 * @generated
	 */
	EAttribute getQueue_Size_MaxNum();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Ready_Queue_Size <em>Ready Queue Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ready Queue Size</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Ready_Queue_Size
	 * @generated
	 */
	EClass getReady_Queue_Size();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Ready_Queue_Size#getMaxNum <em>Max Num</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Num</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Ready_Queue_Size#getMaxNum()
	 * @see #getReady_Queue_Size()
	 * @generated
	 */
	EAttribute getReady_Queue_Size_MaxNum();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION <em>REAL TIME SITUATION</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>REAL TIME SITUATION</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION
	 * @generated
	 */
	EClass getREAL_TIME_SITUATION();

	/**
	 * Returns the meta object for the attribute list '{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getGroup()
	 * @see #getREAL_TIME_SITUATION()
	 * @generated
	 */
	EAttribute getREAL_TIME_SITUATION_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getSystemSlack <em>System Slack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>System Slack</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getSystemSlack()
	 * @see #getREAL_TIME_SITUATION()
	 * @generated
	 */
	EReference getREAL_TIME_SITUATION_SystemSlack();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getTrace <em>Trace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Trace</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getTrace()
	 * @see #getREAL_TIME_SITUATION()
	 * @generated
	 */
	EReference getREAL_TIME_SITUATION_Trace();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getTransaction <em>Transaction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transaction</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getTransaction()
	 * @see #getREAL_TIME_SITUATION()
	 * @generated
	 */
	EReference getREAL_TIME_SITUATION_Transaction();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getProcessingResource <em>Processing Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Processing Resource</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getProcessingResource()
	 * @see #getREAL_TIME_SITUATION()
	 * @generated
	 */
	EReference getREAL_TIME_SITUATION_ProcessingResource();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operation</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getOperation()
	 * @see #getREAL_TIME_SITUATION()
	 * @generated
	 */
	EReference getREAL_TIME_SITUATION_Operation();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getSchedulingServer <em>Scheduling Server</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Scheduling Server</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getSchedulingServer()
	 * @see #getREAL_TIME_SITUATION()
	 * @generated
	 */
	EReference getREAL_TIME_SITUATION_SchedulingServer();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getSharedResource <em>Shared Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Shared Resource</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getSharedResource()
	 * @see #getREAL_TIME_SITUATION()
	 * @generated
	 */
	EReference getREAL_TIME_SITUATION_SharedResource();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getGenerationDate <em>Generation Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Generation Date</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getGenerationDate()
	 * @see #getREAL_TIME_SITUATION()
	 * @generated
	 */
	EAttribute getREAL_TIME_SITUATION_GenerationDate();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getGenerationProfile <em>Generation Profile</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Generation Profile</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getGenerationProfile()
	 * @see #getREAL_TIME_SITUATION()
	 * @generated
	 */
	EAttribute getREAL_TIME_SITUATION_GenerationProfile();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getGenerationTool <em>Generation Tool</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Generation Tool</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getGenerationTool()
	 * @see #getREAL_TIME_SITUATION()
	 * @generated
	 */
	EAttribute getREAL_TIME_SITUATION_GenerationTool();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getModelDate <em>Model Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model Date</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getModelDate()
	 * @see #getREAL_TIME_SITUATION()
	 * @generated
	 */
	EAttribute getREAL_TIME_SITUATION_ModelDate();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getModelName <em>Model Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getModelName()
	 * @see #getREAL_TIME_SITUATION()
	 * @generated
	 */
	EAttribute getREAL_TIME_SITUATION_ModelName();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results <em>Scheduling Server Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scheduling Server Results</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results
	 * @generated
	 */
	EClass getScheduling_Server_Results();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getNonPreemptibleFPPolicy <em>Non Preemptible FP Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Non Preemptible FP Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getNonPreemptibleFPPolicy()
	 * @see #getScheduling_Server_Results()
	 * @generated
	 */
	EReference getScheduling_Server_Results_NonPreemptibleFPPolicy();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getFixedPriorityPolicy <em>Fixed Priority Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Fixed Priority Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getFixedPriorityPolicy()
	 * @see #getScheduling_Server_Results()
	 * @generated
	 */
	EReference getScheduling_Server_Results_FixedPriorityPolicy();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getInterruptFPPolicy <em>Interrupt FP Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Interrupt FP Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getInterruptFPPolicy()
	 * @see #getScheduling_Server_Results()
	 * @generated
	 */
	EReference getScheduling_Server_Results_InterruptFPPolicy();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getPollingPolicy <em>Polling Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Polling Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getPollingPolicy()
	 * @see #getScheduling_Server_Results()
	 * @generated
	 */
	EReference getScheduling_Server_Results_PollingPolicy();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getSporadicServerPolicy <em>Sporadic Server Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sporadic Server Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getSporadicServerPolicy()
	 * @see #getScheduling_Server_Results()
	 * @generated
	 */
	EReference getScheduling_Server_Results_SporadicServerPolicy();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results#getName()
	 * @see #getScheduling_Server_Results()
	 * @generated
	 */
	EAttribute getScheduling_Server_Results_Name();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results <em>Shared Resource Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Shared Resource Results</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results
	 * @generated
	 */
	EClass getShared_Resource_Results();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results#getPriorityCeiling <em>Priority Ceiling</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Priority Ceiling</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results#getPriorityCeiling()
	 * @see #getShared_Resource_Results()
	 * @generated
	 */
	EAttribute getShared_Resource_Results_PriorityCeiling();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results#getUtilization <em>Utilization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Utilization</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results#getUtilization()
	 * @see #getShared_Resource_Results()
	 * @generated
	 */
	EReference getShared_Resource_Results_Utilization();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results#getQueueSize <em>Queue Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Queue Size</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results#getQueueSize()
	 * @see #getShared_Resource_Results()
	 * @generated
	 */
	EReference getShared_Resource_Results_QueueSize();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results#getName()
	 * @see #getShared_Resource_Results()
	 * @generated
	 */
	EAttribute getShared_Resource_Results_Name();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result <em>Simulation Timing Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Simulation Timing Result</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result
	 * @generated
	 */
	EClass getSimulation_Timing_Result();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getWorstGlobalResponseTimes <em>Worst Global Response Times</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Worst Global Response Times</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getWorstGlobalResponseTimes()
	 * @see #getSimulation_Timing_Result()
	 * @generated
	 */
	EReference getSimulation_Timing_Result_WorstGlobalResponseTimes();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getAvgGlobalResponseTimes <em>Avg Global Response Times</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Avg Global Response Times</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getAvgGlobalResponseTimes()
	 * @see #getSimulation_Timing_Result()
	 * @generated
	 */
	EReference getSimulation_Timing_Result_AvgGlobalResponseTimes();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getBestGlobalResponseTimes <em>Best Global Response Times</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Best Global Response Times</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getBestGlobalResponseTimes()
	 * @see #getSimulation_Timing_Result()
	 * @generated
	 */
	EReference getSimulation_Timing_Result_BestGlobalResponseTimes();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getJitters <em>Jitters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Jitters</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getJitters()
	 * @see #getSimulation_Timing_Result()
	 * @generated
	 */
	EReference getSimulation_Timing_Result_Jitters();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getLocalMissRatios <em>Local Miss Ratios</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Local Miss Ratios</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getLocalMissRatios()
	 * @see #getSimulation_Timing_Result()
	 * @generated
	 */
	EReference getSimulation_Timing_Result_LocalMissRatios();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getGlobalMissRatios <em>Global Miss Ratios</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Global Miss Ratios</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getGlobalMissRatios()
	 * @see #getSimulation_Timing_Result()
	 * @generated
	 */
	EReference getSimulation_Timing_Result_GlobalMissRatios();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getAvgBlockingTime <em>Avg Blocking Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Avg Blocking Time</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getAvgBlockingTime()
	 * @see #getSimulation_Timing_Result()
	 * @generated
	 */
	EAttribute getSimulation_Timing_Result_AvgBlockingTime();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getAvgLocalResponseTime <em>Avg Local Response Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Avg Local Response Time</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getAvgLocalResponseTime()
	 * @see #getSimulation_Timing_Result()
	 * @generated
	 */
	EAttribute getSimulation_Timing_Result_AvgLocalResponseTime();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getBestLocalResponseTime <em>Best Local Response Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Best Local Response Time</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getBestLocalResponseTime()
	 * @see #getSimulation_Timing_Result()
	 * @generated
	 */
	EAttribute getSimulation_Timing_Result_BestLocalResponseTime();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getEventName <em>Event Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Event Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getEventName()
	 * @see #getSimulation_Timing_Result()
	 * @generated
	 */
	EAttribute getSimulation_Timing_Result_EventName();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getMaxPreemptionTime <em>Max Preemption Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Preemption Time</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getMaxPreemptionTime()
	 * @see #getSimulation_Timing_Result()
	 * @generated
	 */
	EAttribute getSimulation_Timing_Result_MaxPreemptionTime();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getNumOfQueuedActivations <em>Num Of Queued Activations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Of Queued Activations</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getNumOfQueuedActivations()
	 * @see #getSimulation_Timing_Result()
	 * @generated
	 */
	EAttribute getSimulation_Timing_Result_NumOfQueuedActivations();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getNumOfSuspensions <em>Num Of Suspensions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Of Suspensions</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getNumOfSuspensions()
	 * @see #getSimulation_Timing_Result()
	 * @generated
	 */
	EAttribute getSimulation_Timing_Result_NumOfSuspensions();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getSuspensionTime <em>Suspension Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Suspension Time</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getSuspensionTime()
	 * @see #getSimulation_Timing_Result()
	 * @generated
	 */
	EAttribute getSimulation_Timing_Result_SuspensionTime();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getWorstBlockingTime <em>Worst Blocking Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Worst Blocking Time</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getWorstBlockingTime()
	 * @see #getSimulation_Timing_Result()
	 * @generated
	 */
	EAttribute getSimulation_Timing_Result_WorstBlockingTime();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getWorstLocalResponseTime <em>Worst Local Response Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Worst Local Response Time</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result#getWorstLocalResponseTime()
	 * @see #getSimulation_Timing_Result()
	 * @generated
	 */
	EAttribute getSimulation_Timing_Result_WorstLocalResponseTime();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Slack <em>Slack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Slack</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Slack
	 * @generated
	 */
	EClass getSlack();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Slack#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Slack#getValue()
	 * @see #getSlack()
	 * @generated
	 */
	EAttribute getSlack_Value();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Sporadic_Server_Policy <em>Sporadic Server Policy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sporadic Server Policy</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Sporadic_Server_Policy
	 * @generated
	 */
	EClass getSporadic_Server_Policy();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Sporadic_Server_Policy#getBackgroundPriority <em>Background Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Background Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Sporadic_Server_Policy#getBackgroundPriority()
	 * @see #getSporadic_Server_Policy()
	 * @generated
	 */
	EAttribute getSporadic_Server_Policy_BackgroundPriority();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Sporadic_Server_Policy#getInitialCapacity <em>Initial Capacity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Capacity</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Sporadic_Server_Policy#getInitialCapacity()
	 * @see #getSporadic_Server_Policy()
	 * @generated
	 */
	EAttribute getSporadic_Server_Policy_InitialCapacity();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Sporadic_Server_Policy#getMaxPendingReplenishments <em>Max Pending Replenishments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Pending Replenishments</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Sporadic_Server_Policy#getMaxPendingReplenishments()
	 * @see #getSporadic_Server_Policy()
	 * @generated
	 */
	EAttribute getSporadic_Server_Policy_MaxPendingReplenishments();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Sporadic_Server_Policy#getNormalPriority <em>Normal Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Normal Priority</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Sporadic_Server_Policy#getNormalPriority()
	 * @see #getSporadic_Server_Policy()
	 * @generated
	 */
	EAttribute getSporadic_Server_Policy_NormalPriority();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Sporadic_Server_Policy#getPreassigned <em>Preassigned</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Preassigned</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Sporadic_Server_Policy#getPreassigned()
	 * @see #getSporadic_Server_Policy()
	 * @generated
	 */
	EAttribute getSporadic_Server_Policy_Preassigned();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Sporadic_Server_Policy#getReplenishmentPeriod <em>Replenishment Period</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Replenishment Period</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Sporadic_Server_Policy#getReplenishmentPeriod()
	 * @see #getSporadic_Server_Policy()
	 * @generated
	 */
	EAttribute getSporadic_Server_Policy_ReplenishmentPeriod();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Timing_Result <em>Timing Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Timing Result</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Timing_Result
	 * @generated
	 */
	EClass getTiming_Result();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastresult.Timing_Result#getWorstGlobalResponseTimes <em>Worst Global Response Times</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Worst Global Response Times</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Timing_Result#getWorstGlobalResponseTimes()
	 * @see #getTiming_Result()
	 * @generated
	 */
	EReference getTiming_Result_WorstGlobalResponseTimes();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastresult.Timing_Result#getBestGlobalResponseTimes <em>Best Global Response Times</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Best Global Response Times</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Timing_Result#getBestGlobalResponseTimes()
	 * @see #getTiming_Result()
	 * @generated
	 */
	EReference getTiming_Result_BestGlobalResponseTimes();

	/**
	 * Returns the meta object for the containment reference '{@link es.esi.gemde.vv.mast.mastresult.Timing_Result#getJitters <em>Jitters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Jitters</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Timing_Result#getJitters()
	 * @see #getTiming_Result()
	 * @generated
	 */
	EReference getTiming_Result_Jitters();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Timing_Result#getBestLocalResponseTime <em>Best Local Response Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Best Local Response Time</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Timing_Result#getBestLocalResponseTime()
	 * @see #getTiming_Result()
	 * @generated
	 */
	EAttribute getTiming_Result_BestLocalResponseTime();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Timing_Result#getEventName <em>Event Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Event Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Timing_Result#getEventName()
	 * @see #getTiming_Result()
	 * @generated
	 */
	EAttribute getTiming_Result_EventName();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Timing_Result#getNumOfSuspensions <em>Num Of Suspensions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Of Suspensions</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Timing_Result#getNumOfSuspensions()
	 * @see #getTiming_Result()
	 * @generated
	 */
	EAttribute getTiming_Result_NumOfSuspensions();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Timing_Result#getWorstBlockingTime <em>Worst Blocking Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Worst Blocking Time</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Timing_Result#getWorstBlockingTime()
	 * @see #getTiming_Result()
	 * @generated
	 */
	EAttribute getTiming_Result_WorstBlockingTime();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Timing_Result#getWorstLocalResponseTime <em>Worst Local Response Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Worst Local Response Time</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Timing_Result#getWorstLocalResponseTime()
	 * @see #getTiming_Result()
	 * @generated
	 */
	EAttribute getTiming_Result_WorstLocalResponseTime();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Trace <em>Trace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Trace</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Trace
	 * @generated
	 */
	EClass getTrace();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Trace#getPathname <em>Pathname</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Pathname</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Trace#getPathname()
	 * @see #getTrace()
	 * @generated
	 */
	EAttribute getTrace_Pathname();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Transaction_Results <em>Transaction Results</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transaction Results</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Transaction_Results
	 * @generated
	 */
	EClass getTransaction_Results();

	/**
	 * Returns the meta object for the attribute list '{@link es.esi.gemde.vv.mast.mastresult.Transaction_Results#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Transaction_Results#getGroup()
	 * @see #getTransaction_Results()
	 * @generated
	 */
	EAttribute getTransaction_Results_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastresult.Transaction_Results#getSlack <em>Slack</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Slack</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Transaction_Results#getSlack()
	 * @see #getTransaction_Results()
	 * @generated
	 */
	EReference getTransaction_Results_Slack();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastresult.Transaction_Results#getTimingResult <em>Timing Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Timing Result</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Transaction_Results#getTimingResult()
	 * @see #getTransaction_Results()
	 * @generated
	 */
	EReference getTransaction_Results_TimingResult();

	/**
	 * Returns the meta object for the containment reference list '{@link es.esi.gemde.vv.mast.mastresult.Transaction_Results#getSimulationTimingResult <em>Simulation Timing Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Simulation Timing Result</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Transaction_Results#getSimulationTimingResult()
	 * @see #getTransaction_Results()
	 * @generated
	 */
	EReference getTransaction_Results_SimulationTimingResult();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Transaction_Results#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Transaction_Results#getName()
	 * @see #getTransaction_Results()
	 * @generated
	 */
	EAttribute getTransaction_Results_Name();

	/**
	 * Returns the meta object for class '{@link es.esi.gemde.vv.mast.mastresult.Utilization <em>Utilization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Utilization</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Utilization
	 * @generated
	 */
	EClass getUtilization();

	/**
	 * Returns the meta object for the attribute '{@link es.esi.gemde.vv.mast.mastresult.Utilization#getTotal <em>Total</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Total</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Utilization#getTotal()
	 * @see #getUtilization()
	 * @generated
	 */
	EAttribute getUtilization_Total();

	/**
	 * Returns the meta object for enum '{@link es.esi.gemde.vv.mast.mastresult.Affirmative_Assertion <em>Affirmative Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Affirmative Assertion</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Affirmative_Assertion
	 * @generated
	 */
	EEnum getAffirmative_Assertion();

	/**
	 * Returns the meta object for enum '{@link es.esi.gemde.vv.mast.mastresult.Assertion <em>Assertion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Assertion</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Assertion
	 * @generated
	 */
	EEnum getAssertion();

	/**
	 * Returns the meta object for data type '{@link es.esi.gemde.vv.mast.mastresult.Affirmative_Assertion <em>Affirmative Assertion Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Affirmative Assertion Object</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Affirmative_Assertion
	 * @model instanceClass="es.esi.gemde.vv.mast.mastresult.Affirmative_Assertion"
	 *        extendedMetaData="name='Affirmative_Assertion:Object' baseType='Affirmative_Assertion'"
	 * @generated
	 */
	EDataType getAffirmative_Assertion_Object();

	/**
	 * Returns the meta object for data type '{@link es.esi.gemde.vv.mast.mastresult.Assertion <em>Assertion Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Assertion Object</em>'.
	 * @see es.esi.gemde.vv.mast.mastresult.Assertion
	 * @model instanceClass="es.esi.gemde.vv.mast.mastresult.Assertion"
	 *        extendedMetaData="name='Assertion:Object' baseType='Assertion'"
	 * @generated
	 */
	EDataType getAssertion_Object();

	/**
	 * Returns the meta object for data type '{@link javax.xml.datatype.XMLGregorianCalendar <em>Date Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Date Time</em>'.
	 * @see javax.xml.datatype.XMLGregorianCalendar
	 * @model instanceClass="javax.xml.datatype.XMLGregorianCalendar"
	 *        extendedMetaData="name='Date_Time' baseType='http://www.eclipse.org/emf/2003/XMLType#dateTime'"
	 * @generated
	 */
	EDataType getDate_Time();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>External Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>External Reference</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 *        extendedMetaData="name='External_Reference' baseType='http://www.eclipse.org/emf/2003/XMLType#NCName' pattern='([a-z]|[A-Z])([a-z]|[A-Z]|[0-9]|.|_)*'"
	 * @generated
	 */
	EDataType getExternal_Reference();

	/**
	 * Returns the meta object for data type '<em>Factor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Factor</em>'.
	 * @model instanceClass="float"
	 *        extendedMetaData="name='Factor' baseType='http://www.eclipse.org/emf/2003/XMLType#float' minExclusive='0'"
	 * @generated
	 */
	EDataType getFactor();

	/**
	 * Returns the meta object for data type '{@link java.lang.Float <em>Factor Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Factor Object</em>'.
	 * @see java.lang.Float
	 * @model instanceClass="java.lang.Float"
	 *        extendedMetaData="name='Factor:Object' baseType='Factor'"
	 * @generated
	 */
	EDataType getFactor_Object();

	/**
	 * Returns the meta object for data type '{@link java.lang.String <em>Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Identifier</em>'.
	 * @see java.lang.String
	 * @model instanceClass="java.lang.String"
	 *        extendedMetaData="name='Identifier' baseType='http://www.eclipse.org/emf/2003/XMLType#ID' pattern='([a-z]|[A-Z])([a-z]|[A-Z]|[0-9]|.|_)*'"
	 * @generated
	 */
	EDataType getIdentifier();

	/**
	 * Returns the meta object for data type '<em>Normalized Execution Time</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Normalized Execution Time</em>'.
	 * @model instanceClass="float"
	 *        extendedMetaData="name='Normalized_Execution_Time' baseType='http://www.eclipse.org/emf/2003/XMLType#float' minInclusive='0.0'"
	 * @generated
	 */
	EDataType getNormalized_Execution_Time();

	/**
	 * Returns the meta object for data type '{@link java.lang.Float <em>Normalized Execution Time Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Normalized Execution Time Object</em>'.
	 * @see java.lang.Float
	 * @model instanceClass="java.lang.Float"
	 *        extendedMetaData="name='Normalized_Execution_Time:Object' baseType='Normalized_Execution_Time'"
	 * @generated
	 */
	EDataType getNormalized_Execution_Time_Object();

	/**
	 * Returns the meta object for data type '<em>Percentage</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Percentage</em>'.
	 * @model instanceClass="float"
	 *        extendedMetaData="name='Percentage' baseType='http://www.eclipse.org/emf/2003/XMLType#float' minInclusive='0.0' maxInclusive='100.0'"
	 * @generated
	 */
	EDataType getPercentage();

	/**
	 * Returns the meta object for data type '{@link java.lang.Float <em>Percentage Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Percentage Object</em>'.
	 * @see java.lang.Float
	 * @model instanceClass="java.lang.Float"
	 *        extendedMetaData="name='Percentage:Object' baseType='Percentage'"
	 * @generated
	 */
	EDataType getPercentage_Object();

	/**
	 * Returns the meta object for data type '<em>Priority</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Priority</em>'.
	 * @model instanceClass="int"
	 *        extendedMetaData="name='Priority' baseType='http://www.eclipse.org/emf/2003/XMLType#int' minInclusive='0'"
	 * @generated
	 */
	EDataType getPriority();

	/**
	 * Returns the meta object for data type '{@link java.lang.Integer <em>Priority Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Priority Object</em>'.
	 * @see java.lang.Integer
	 * @model instanceClass="java.lang.Integer"
	 *        extendedMetaData="name='Priority:Object' baseType='Priority'"
	 * @generated
	 */
	EDataType getPriority_Object();

	/**
	 * Returns the meta object for data type '<em>Time</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Time</em>'.
	 * @model instanceClass="float"
	 *        extendedMetaData="name='Time' baseType='http://www.eclipse.org/emf/2003/XMLType#float' minInclusive='0.0'"
	 * @generated
	 */
	EDataType getTime();

	/**
	 * Returns the meta object for data type '{@link java.lang.Float <em>Time Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Time Object</em>'.
	 * @see java.lang.Float
	 * @model instanceClass="java.lang.Float"
	 *        extendedMetaData="name='Time:Object' baseType='Time'"
	 * @generated
	 */
	EDataType getTime_Object();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ResultFactory getResultFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Detailed_UtilizationImpl <em>Detailed Utilization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Detailed_UtilizationImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getDetailed_Utilization()
		 * @generated
		 */
		EClass DETAILED_UTILIZATION = eINSTANCE.getDetailed_Utilization();

		/**
		 * The meta object literal for the '<em><b>Application</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DETAILED_UTILIZATION__APPLICATION = eINSTANCE.getDetailed_Utilization_Application();

		/**
		 * The meta object literal for the '<em><b>Context Switch</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DETAILED_UTILIZATION__CONTEXT_SWITCH = eINSTANCE.getDetailed_Utilization_ContextSwitch();

		/**
		 * The meta object literal for the '<em><b>Driver</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DETAILED_UTILIZATION__DRIVER = eINSTANCE.getDetailed_Utilization_Driver();

		/**
		 * The meta object literal for the '<em><b>Timer</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DETAILED_UTILIZATION__TIMER = eINSTANCE.getDetailed_Utilization_Timer();

		/**
		 * The meta object literal for the '<em><b>Total</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DETAILED_UTILIZATION__TOTAL = eINSTANCE.getDetailed_Utilization_Total();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Document_RootImpl <em>Document Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Document_RootImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getDocument_Root()
		 * @generated
		 */
		EClass DOCUMENT_ROOT = eINSTANCE.getDocument_Root();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocument_Root_Mixed();

		/**
		 * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocument_Root_XMLNSPrefixMap();

		/**
		 * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocument_Root_XSISchemaLocation();

		/**
		 * The meta object literal for the '<em><b>REALTIMESITUATION</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__REALTIMESITUATION = eINSTANCE.getDocument_Root_REALTIMESITUATION();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Fixed_Priority_PolicyImpl <em>Fixed Priority Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Fixed_Priority_PolicyImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getFixed_Priority_Policy()
		 * @generated
		 */
		EClass FIXED_PRIORITY_POLICY = eINSTANCE.getFixed_Priority_Policy();

		/**
		 * The meta object literal for the '<em><b>Preassigned</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_PRIORITY_POLICY__PREASSIGNED = eINSTANCE.getFixed_Priority_Policy_Preassigned();

		/**
		 * The meta object literal for the '<em><b>The Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIXED_PRIORITY_POLICY__THE_PRIORITY = eINSTANCE.getFixed_Priority_Policy_ThePriority();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Global_Miss_RatioImpl <em>Global Miss Ratio</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Global_Miss_RatioImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getGlobal_Miss_Ratio()
		 * @generated
		 */
		EClass GLOBAL_MISS_RATIO = eINSTANCE.getGlobal_Miss_Ratio();

		/**
		 * The meta object literal for the '<em><b>Miss Ratios</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GLOBAL_MISS_RATIO__MISS_RATIOS = eINSTANCE.getGlobal_Miss_Ratio_MissRatios();

		/**
		 * The meta object literal for the '<em><b>Referenced Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GLOBAL_MISS_RATIO__REFERENCED_EVENT = eINSTANCE.getGlobal_Miss_Ratio_ReferencedEvent();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Global_Miss_Ratio_ListImpl <em>Global Miss Ratio List</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Global_Miss_Ratio_ListImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getGlobal_Miss_Ratio_List()
		 * @generated
		 */
		EClass GLOBAL_MISS_RATIO_LIST = eINSTANCE.getGlobal_Miss_Ratio_List();

		/**
		 * The meta object literal for the '<em><b>Global Miss Ratio</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GLOBAL_MISS_RATIO_LIST__GLOBAL_MISS_RATIO = eINSTANCE.getGlobal_Miss_Ratio_List_GlobalMissRatio();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Global_Response_TimeImpl <em>Global Response Time</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Global_Response_TimeImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getGlobal_Response_Time()
		 * @generated
		 */
		EClass GLOBAL_RESPONSE_TIME = eINSTANCE.getGlobal_Response_Time();

		/**
		 * The meta object literal for the '<em><b>Referenced Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GLOBAL_RESPONSE_TIME__REFERENCED_EVENT = eINSTANCE.getGlobal_Response_Time_ReferencedEvent();

		/**
		 * The meta object literal for the '<em><b>Time Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GLOBAL_RESPONSE_TIME__TIME_VALUE = eINSTANCE.getGlobal_Response_Time_TimeValue();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Global_Response_Time_ListImpl <em>Global Response Time List</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Global_Response_Time_ListImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getGlobal_Response_Time_List()
		 * @generated
		 */
		EClass GLOBAL_RESPONSE_TIME_LIST = eINSTANCE.getGlobal_Response_Time_List();

		/**
		 * The meta object literal for the '<em><b>Global Response Time</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GLOBAL_RESPONSE_TIME_LIST__GLOBAL_RESPONSE_TIME = eINSTANCE.getGlobal_Response_Time_List_GlobalResponseTime();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Interrupt_FP_PolicyImpl <em>Interrupt FP Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Interrupt_FP_PolicyImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getInterrupt_FP_Policy()
		 * @generated
		 */
		EClass INTERRUPT_FP_POLICY = eINSTANCE.getInterrupt_FP_Policy();

		/**
		 * The meta object literal for the '<em><b>Preassigned</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERRUPT_FP_POLICY__PREASSIGNED = eINSTANCE.getInterrupt_FP_Policy_Preassigned();

		/**
		 * The meta object literal for the '<em><b>The Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute INTERRUPT_FP_POLICY__THE_PRIORITY = eINSTANCE.getInterrupt_FP_Policy_ThePriority();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Miss_RatioImpl <em>Miss Ratio</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Miss_RatioImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getMiss_Ratio()
		 * @generated
		 */
		EClass MISS_RATIO = eINSTANCE.getMiss_Ratio();

		/**
		 * The meta object literal for the '<em><b>Deadline</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MISS_RATIO__DEADLINE = eINSTANCE.getMiss_Ratio_Deadline();

		/**
		 * The meta object literal for the '<em><b>Ratio</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MISS_RATIO__RATIO = eINSTANCE.getMiss_Ratio_Ratio();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Miss_Ratio_ListImpl <em>Miss Ratio List</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Miss_Ratio_ListImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getMiss_Ratio_List()
		 * @generated
		 */
		EClass MISS_RATIO_LIST = eINSTANCE.getMiss_Ratio_List();

		/**
		 * The meta object literal for the '<em><b>Miss Ratio</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MISS_RATIO_LIST__MISS_RATIO = eINSTANCE.getMiss_Ratio_List_MissRatio();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Non_Preemptible_FP_PolicyImpl <em>Non Preemptible FP Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Non_Preemptible_FP_PolicyImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getNon_Preemptible_FP_Policy()
		 * @generated
		 */
		EClass NON_PREEMPTIBLE_FP_POLICY = eINSTANCE.getNon_Preemptible_FP_Policy();

		/**
		 * The meta object literal for the '<em><b>Preassigned</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NON_PREEMPTIBLE_FP_POLICY__PREASSIGNED = eINSTANCE.getNon_Preemptible_FP_Policy_Preassigned();

		/**
		 * The meta object literal for the '<em><b>The Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NON_PREEMPTIBLE_FP_POLICY__THE_PRIORITY = eINSTANCE.getNon_Preemptible_FP_Policy_ThePriority();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Operation_ResultsImpl <em>Operation Results</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Operation_ResultsImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getOperation_Results()
		 * @generated
		 */
		EClass OPERATION_RESULTS = eINSTANCE.getOperation_Results();

		/**
		 * The meta object literal for the '<em><b>Slack</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_RESULTS__SLACK = eINSTANCE.getOperation_Results_Slack();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_RESULTS__NAME = eINSTANCE.getOperation_Results_Name();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Polling_PolicyImpl <em>Polling Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Polling_PolicyImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getPolling_Policy()
		 * @generated
		 */
		EClass POLLING_POLICY = eINSTANCE.getPolling_Policy();

		/**
		 * The meta object literal for the '<em><b>Polling Avg Overhead</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POLLING_POLICY__POLLING_AVG_OVERHEAD = eINSTANCE.getPolling_Policy_PollingAvgOverhead();

		/**
		 * The meta object literal for the '<em><b>Polling Best Overhead</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POLLING_POLICY__POLLING_BEST_OVERHEAD = eINSTANCE.getPolling_Policy_PollingBestOverhead();

		/**
		 * The meta object literal for the '<em><b>Polling Period</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POLLING_POLICY__POLLING_PERIOD = eINSTANCE.getPolling_Policy_PollingPeriod();

		/**
		 * The meta object literal for the '<em><b>Polling Worst Overhead</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POLLING_POLICY__POLLING_WORST_OVERHEAD = eINSTANCE.getPolling_Policy_PollingWorstOverhead();

		/**
		 * The meta object literal for the '<em><b>Preassigned</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POLLING_POLICY__PREASSIGNED = eINSTANCE.getPolling_Policy_Preassigned();

		/**
		 * The meta object literal for the '<em><b>The Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute POLLING_POLICY__THE_PRIORITY = eINSTANCE.getPolling_Policy_ThePriority();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Priority_CeilingImpl <em>Priority Ceiling</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Priority_CeilingImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getPriority_Ceiling()
		 * @generated
		 */
		EClass PRIORITY_CEILING = eINSTANCE.getPriority_Ceiling();

		/**
		 * The meta object literal for the '<em><b>Ceiling</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PRIORITY_CEILING__CEILING = eINSTANCE.getPriority_Ceiling_Ceiling();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Processing_Resource_ResultsImpl <em>Processing Resource Results</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Processing_Resource_ResultsImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getProcessing_Resource_Results()
		 * @generated
		 */
		EClass PROCESSING_RESOURCE_RESULTS = eINSTANCE.getProcessing_Resource_Results();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESSING_RESOURCE_RESULTS__GROUP = eINSTANCE.getProcessing_Resource_Results_Group();

		/**
		 * The meta object literal for the '<em><b>Slack</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESSING_RESOURCE_RESULTS__SLACK = eINSTANCE.getProcessing_Resource_Results_Slack();

		/**
		 * The meta object literal for the '<em><b>Detailed Utilization</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESSING_RESOURCE_RESULTS__DETAILED_UTILIZATION = eINSTANCE.getProcessing_Resource_Results_DetailedUtilization();

		/**
		 * The meta object literal for the '<em><b>Utilization</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESSING_RESOURCE_RESULTS__UTILIZATION = eINSTANCE.getProcessing_Resource_Results_Utilization();

		/**
		 * The meta object literal for the '<em><b>Ready Queue Size</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PROCESSING_RESOURCE_RESULTS__READY_QUEUE_SIZE = eINSTANCE.getProcessing_Resource_Results_ReadyQueueSize();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESSING_RESOURCE_RESULTS__NAME = eINSTANCE.getProcessing_Resource_Results_Name();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Queue_SizeImpl <em>Queue Size</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Queue_SizeImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getQueue_Size()
		 * @generated
		 */
		EClass QUEUE_SIZE = eINSTANCE.getQueue_Size();

		/**
		 * The meta object literal for the '<em><b>Max Num</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute QUEUE_SIZE__MAX_NUM = eINSTANCE.getQueue_Size_MaxNum();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Ready_Queue_SizeImpl <em>Ready Queue Size</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Ready_Queue_SizeImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getReady_Queue_Size()
		 * @generated
		 */
		EClass READY_QUEUE_SIZE = eINSTANCE.getReady_Queue_Size();

		/**
		 * The meta object literal for the '<em><b>Max Num</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute READY_QUEUE_SIZE__MAX_NUM = eINSTANCE.getReady_Queue_Size_MaxNum();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.REAL_TIME_SITUATIONImpl <em>REAL TIME SITUATION</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.REAL_TIME_SITUATIONImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getREAL_TIME_SITUATION()
		 * @generated
		 */
		EClass REAL_TIME_SITUATION = eINSTANCE.getREAL_TIME_SITUATION();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REAL_TIME_SITUATION__GROUP = eINSTANCE.getREAL_TIME_SITUATION_Group();

		/**
		 * The meta object literal for the '<em><b>System Slack</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REAL_TIME_SITUATION__SYSTEM_SLACK = eINSTANCE.getREAL_TIME_SITUATION_SystemSlack();

		/**
		 * The meta object literal for the '<em><b>Trace</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REAL_TIME_SITUATION__TRACE = eINSTANCE.getREAL_TIME_SITUATION_Trace();

		/**
		 * The meta object literal for the '<em><b>Transaction</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REAL_TIME_SITUATION__TRANSACTION = eINSTANCE.getREAL_TIME_SITUATION_Transaction();

		/**
		 * The meta object literal for the '<em><b>Processing Resource</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REAL_TIME_SITUATION__PROCESSING_RESOURCE = eINSTANCE.getREAL_TIME_SITUATION_ProcessingResource();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REAL_TIME_SITUATION__OPERATION = eINSTANCE.getREAL_TIME_SITUATION_Operation();

		/**
		 * The meta object literal for the '<em><b>Scheduling Server</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REAL_TIME_SITUATION__SCHEDULING_SERVER = eINSTANCE.getREAL_TIME_SITUATION_SchedulingServer();

		/**
		 * The meta object literal for the '<em><b>Shared Resource</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REAL_TIME_SITUATION__SHARED_RESOURCE = eINSTANCE.getREAL_TIME_SITUATION_SharedResource();

		/**
		 * The meta object literal for the '<em><b>Generation Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REAL_TIME_SITUATION__GENERATION_DATE = eINSTANCE.getREAL_TIME_SITUATION_GenerationDate();

		/**
		 * The meta object literal for the '<em><b>Generation Profile</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REAL_TIME_SITUATION__GENERATION_PROFILE = eINSTANCE.getREAL_TIME_SITUATION_GenerationProfile();

		/**
		 * The meta object literal for the '<em><b>Generation Tool</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REAL_TIME_SITUATION__GENERATION_TOOL = eINSTANCE.getREAL_TIME_SITUATION_GenerationTool();

		/**
		 * The meta object literal for the '<em><b>Model Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REAL_TIME_SITUATION__MODEL_DATE = eINSTANCE.getREAL_TIME_SITUATION_ModelDate();

		/**
		 * The meta object literal for the '<em><b>Model Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REAL_TIME_SITUATION__MODEL_NAME = eINSTANCE.getREAL_TIME_SITUATION_ModelName();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Scheduling_Server_ResultsImpl <em>Scheduling Server Results</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Scheduling_Server_ResultsImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getScheduling_Server_Results()
		 * @generated
		 */
		EClass SCHEDULING_SERVER_RESULTS = eINSTANCE.getScheduling_Server_Results();

		/**
		 * The meta object literal for the '<em><b>Non Preemptible FP Policy</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULING_SERVER_RESULTS__NON_PREEMPTIBLE_FP_POLICY = eINSTANCE.getScheduling_Server_Results_NonPreemptibleFPPolicy();

		/**
		 * The meta object literal for the '<em><b>Fixed Priority Policy</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULING_SERVER_RESULTS__FIXED_PRIORITY_POLICY = eINSTANCE.getScheduling_Server_Results_FixedPriorityPolicy();

		/**
		 * The meta object literal for the '<em><b>Interrupt FP Policy</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULING_SERVER_RESULTS__INTERRUPT_FP_POLICY = eINSTANCE.getScheduling_Server_Results_InterruptFPPolicy();

		/**
		 * The meta object literal for the '<em><b>Polling Policy</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULING_SERVER_RESULTS__POLLING_POLICY = eINSTANCE.getScheduling_Server_Results_PollingPolicy();

		/**
		 * The meta object literal for the '<em><b>Sporadic Server Policy</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULING_SERVER_RESULTS__SPORADIC_SERVER_POLICY = eINSTANCE.getScheduling_Server_Results_SporadicServerPolicy();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULING_SERVER_RESULTS__NAME = eINSTANCE.getScheduling_Server_Results_Name();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Shared_Resource_ResultsImpl <em>Shared Resource Results</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Shared_Resource_ResultsImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getShared_Resource_Results()
		 * @generated
		 */
		EClass SHARED_RESOURCE_RESULTS = eINSTANCE.getShared_Resource_Results();

		/**
		 * The meta object literal for the '<em><b>Priority Ceiling</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHARED_RESOURCE_RESULTS__PRIORITY_CEILING = eINSTANCE.getShared_Resource_Results_PriorityCeiling();

		/**
		 * The meta object literal for the '<em><b>Utilization</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SHARED_RESOURCE_RESULTS__UTILIZATION = eINSTANCE.getShared_Resource_Results_Utilization();

		/**
		 * The meta object literal for the '<em><b>Queue Size</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SHARED_RESOURCE_RESULTS__QUEUE_SIZE = eINSTANCE.getShared_Resource_Results_QueueSize();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHARED_RESOURCE_RESULTS__NAME = eINSTANCE.getShared_Resource_Results_Name();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Simulation_Timing_ResultImpl <em>Simulation Timing Result</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Simulation_Timing_ResultImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getSimulation_Timing_Result()
		 * @generated
		 */
		EClass SIMULATION_TIMING_RESULT = eINSTANCE.getSimulation_Timing_Result();

		/**
		 * The meta object literal for the '<em><b>Worst Global Response Times</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_TIMING_RESULT__WORST_GLOBAL_RESPONSE_TIMES = eINSTANCE.getSimulation_Timing_Result_WorstGlobalResponseTimes();

		/**
		 * The meta object literal for the '<em><b>Avg Global Response Times</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_TIMING_RESULT__AVG_GLOBAL_RESPONSE_TIMES = eINSTANCE.getSimulation_Timing_Result_AvgGlobalResponseTimes();

		/**
		 * The meta object literal for the '<em><b>Best Global Response Times</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_TIMING_RESULT__BEST_GLOBAL_RESPONSE_TIMES = eINSTANCE.getSimulation_Timing_Result_BestGlobalResponseTimes();

		/**
		 * The meta object literal for the '<em><b>Jitters</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_TIMING_RESULT__JITTERS = eINSTANCE.getSimulation_Timing_Result_Jitters();

		/**
		 * The meta object literal for the '<em><b>Local Miss Ratios</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_TIMING_RESULT__LOCAL_MISS_RATIOS = eINSTANCE.getSimulation_Timing_Result_LocalMissRatios();

		/**
		 * The meta object literal for the '<em><b>Global Miss Ratios</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SIMULATION_TIMING_RESULT__GLOBAL_MISS_RATIOS = eINSTANCE.getSimulation_Timing_Result_GlobalMissRatios();

		/**
		 * The meta object literal for the '<em><b>Avg Blocking Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_TIMING_RESULT__AVG_BLOCKING_TIME = eINSTANCE.getSimulation_Timing_Result_AvgBlockingTime();

		/**
		 * The meta object literal for the '<em><b>Avg Local Response Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_TIMING_RESULT__AVG_LOCAL_RESPONSE_TIME = eINSTANCE.getSimulation_Timing_Result_AvgLocalResponseTime();

		/**
		 * The meta object literal for the '<em><b>Best Local Response Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_TIMING_RESULT__BEST_LOCAL_RESPONSE_TIME = eINSTANCE.getSimulation_Timing_Result_BestLocalResponseTime();

		/**
		 * The meta object literal for the '<em><b>Event Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_TIMING_RESULT__EVENT_NAME = eINSTANCE.getSimulation_Timing_Result_EventName();

		/**
		 * The meta object literal for the '<em><b>Max Preemption Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_TIMING_RESULT__MAX_PREEMPTION_TIME = eINSTANCE.getSimulation_Timing_Result_MaxPreemptionTime();

		/**
		 * The meta object literal for the '<em><b>Num Of Queued Activations</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_TIMING_RESULT__NUM_OF_QUEUED_ACTIVATIONS = eINSTANCE.getSimulation_Timing_Result_NumOfQueuedActivations();

		/**
		 * The meta object literal for the '<em><b>Num Of Suspensions</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_TIMING_RESULT__NUM_OF_SUSPENSIONS = eINSTANCE.getSimulation_Timing_Result_NumOfSuspensions();

		/**
		 * The meta object literal for the '<em><b>Suspension Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_TIMING_RESULT__SUSPENSION_TIME = eINSTANCE.getSimulation_Timing_Result_SuspensionTime();

		/**
		 * The meta object literal for the '<em><b>Worst Blocking Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_TIMING_RESULT__WORST_BLOCKING_TIME = eINSTANCE.getSimulation_Timing_Result_WorstBlockingTime();

		/**
		 * The meta object literal for the '<em><b>Worst Local Response Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SIMULATION_TIMING_RESULT__WORST_LOCAL_RESPONSE_TIME = eINSTANCE.getSimulation_Timing_Result_WorstLocalResponseTime();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.SlackImpl <em>Slack</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.SlackImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getSlack()
		 * @generated
		 */
		EClass SLACK = eINSTANCE.getSlack();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SLACK__VALUE = eINSTANCE.getSlack_Value();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Sporadic_Server_PolicyImpl <em>Sporadic Server Policy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Sporadic_Server_PolicyImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getSporadic_Server_Policy()
		 * @generated
		 */
		EClass SPORADIC_SERVER_POLICY = eINSTANCE.getSporadic_Server_Policy();

		/**
		 * The meta object literal for the '<em><b>Background Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPORADIC_SERVER_POLICY__BACKGROUND_PRIORITY = eINSTANCE.getSporadic_Server_Policy_BackgroundPriority();

		/**
		 * The meta object literal for the '<em><b>Initial Capacity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPORADIC_SERVER_POLICY__INITIAL_CAPACITY = eINSTANCE.getSporadic_Server_Policy_InitialCapacity();

		/**
		 * The meta object literal for the '<em><b>Max Pending Replenishments</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPORADIC_SERVER_POLICY__MAX_PENDING_REPLENISHMENTS = eINSTANCE.getSporadic_Server_Policy_MaxPendingReplenishments();

		/**
		 * The meta object literal for the '<em><b>Normal Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPORADIC_SERVER_POLICY__NORMAL_PRIORITY = eINSTANCE.getSporadic_Server_Policy_NormalPriority();

		/**
		 * The meta object literal for the '<em><b>Preassigned</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPORADIC_SERVER_POLICY__PREASSIGNED = eINSTANCE.getSporadic_Server_Policy_Preassigned();

		/**
		 * The meta object literal for the '<em><b>Replenishment Period</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SPORADIC_SERVER_POLICY__REPLENISHMENT_PERIOD = eINSTANCE.getSporadic_Server_Policy_ReplenishmentPeriod();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Timing_ResultImpl <em>Timing Result</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Timing_ResultImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getTiming_Result()
		 * @generated
		 */
		EClass TIMING_RESULT = eINSTANCE.getTiming_Result();

		/**
		 * The meta object literal for the '<em><b>Worst Global Response Times</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMING_RESULT__WORST_GLOBAL_RESPONSE_TIMES = eINSTANCE.getTiming_Result_WorstGlobalResponseTimes();

		/**
		 * The meta object literal for the '<em><b>Best Global Response Times</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMING_RESULT__BEST_GLOBAL_RESPONSE_TIMES = eINSTANCE.getTiming_Result_BestGlobalResponseTimes();

		/**
		 * The meta object literal for the '<em><b>Jitters</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMING_RESULT__JITTERS = eINSTANCE.getTiming_Result_Jitters();

		/**
		 * The meta object literal for the '<em><b>Best Local Response Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMING_RESULT__BEST_LOCAL_RESPONSE_TIME = eINSTANCE.getTiming_Result_BestLocalResponseTime();

		/**
		 * The meta object literal for the '<em><b>Event Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMING_RESULT__EVENT_NAME = eINSTANCE.getTiming_Result_EventName();

		/**
		 * The meta object literal for the '<em><b>Num Of Suspensions</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMING_RESULT__NUM_OF_SUSPENSIONS = eINSTANCE.getTiming_Result_NumOfSuspensions();

		/**
		 * The meta object literal for the '<em><b>Worst Blocking Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMING_RESULT__WORST_BLOCKING_TIME = eINSTANCE.getTiming_Result_WorstBlockingTime();

		/**
		 * The meta object literal for the '<em><b>Worst Local Response Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMING_RESULT__WORST_LOCAL_RESPONSE_TIME = eINSTANCE.getTiming_Result_WorstLocalResponseTime();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.TraceImpl <em>Trace</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.TraceImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getTrace()
		 * @generated
		 */
		EClass TRACE = eINSTANCE.getTrace();

		/**
		 * The meta object literal for the '<em><b>Pathname</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRACE__PATHNAME = eINSTANCE.getTrace_Pathname();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.Transaction_ResultsImpl <em>Transaction Results</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.Transaction_ResultsImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getTransaction_Results()
		 * @generated
		 */
		EClass TRANSACTION_RESULTS = eINSTANCE.getTransaction_Results();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSACTION_RESULTS__GROUP = eINSTANCE.getTransaction_Results_Group();

		/**
		 * The meta object literal for the '<em><b>Slack</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSACTION_RESULTS__SLACK = eINSTANCE.getTransaction_Results_Slack();

		/**
		 * The meta object literal for the '<em><b>Timing Result</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSACTION_RESULTS__TIMING_RESULT = eINSTANCE.getTransaction_Results_TimingResult();

		/**
		 * The meta object literal for the '<em><b>Simulation Timing Result</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSACTION_RESULTS__SIMULATION_TIMING_RESULT = eINSTANCE.getTransaction_Results_SimulationTimingResult();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TRANSACTION_RESULTS__NAME = eINSTANCE.getTransaction_Results_Name();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.impl.UtilizationImpl <em>Utilization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.UtilizationImpl
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getUtilization()
		 * @generated
		 */
		EClass UTILIZATION = eINSTANCE.getUtilization();

		/**
		 * The meta object literal for the '<em><b>Total</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UTILIZATION__TOTAL = eINSTANCE.getUtilization_Total();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.Affirmative_Assertion <em>Affirmative Assertion</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.Affirmative_Assertion
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getAffirmative_Assertion()
		 * @generated
		 */
		EEnum AFFIRMATIVE_ASSERTION = eINSTANCE.getAffirmative_Assertion();

		/**
		 * The meta object literal for the '{@link es.esi.gemde.vv.mast.mastresult.Assertion <em>Assertion</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.Assertion
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getAssertion()
		 * @generated
		 */
		EEnum ASSERTION = eINSTANCE.getAssertion();

		/**
		 * The meta object literal for the '<em>Affirmative Assertion Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.Affirmative_Assertion
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getAffirmative_Assertion_Object()
		 * @generated
		 */
		EDataType AFFIRMATIVE_ASSERTION_OBJECT = eINSTANCE.getAffirmative_Assertion_Object();

		/**
		 * The meta object literal for the '<em>Assertion Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.Assertion
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getAssertion_Object()
		 * @generated
		 */
		EDataType ASSERTION_OBJECT = eINSTANCE.getAssertion_Object();

		/**
		 * The meta object literal for the '<em>Date Time</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see javax.xml.datatype.XMLGregorianCalendar
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getDate_Time()
		 * @generated
		 */
		EDataType DATE_TIME = eINSTANCE.getDate_Time();

		/**
		 * The meta object literal for the '<em>External Reference</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getExternal_Reference()
		 * @generated
		 */
		EDataType EXTERNAL_REFERENCE = eINSTANCE.getExternal_Reference();

		/**
		 * The meta object literal for the '<em>Factor</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getFactor()
		 * @generated
		 */
		EDataType FACTOR = eINSTANCE.getFactor();

		/**
		 * The meta object literal for the '<em>Factor Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.Float
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getFactor_Object()
		 * @generated
		 */
		EDataType FACTOR_OBJECT = eINSTANCE.getFactor_Object();

		/**
		 * The meta object literal for the '<em>Identifier</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.String
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getIdentifier()
		 * @generated
		 */
		EDataType IDENTIFIER = eINSTANCE.getIdentifier();

		/**
		 * The meta object literal for the '<em>Normalized Execution Time</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getNormalized_Execution_Time()
		 * @generated
		 */
		EDataType NORMALIZED_EXECUTION_TIME = eINSTANCE.getNormalized_Execution_Time();

		/**
		 * The meta object literal for the '<em>Normalized Execution Time Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.Float
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getNormalized_Execution_Time_Object()
		 * @generated
		 */
		EDataType NORMALIZED_EXECUTION_TIME_OBJECT = eINSTANCE.getNormalized_Execution_Time_Object();

		/**
		 * The meta object literal for the '<em>Percentage</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getPercentage()
		 * @generated
		 */
		EDataType PERCENTAGE = eINSTANCE.getPercentage();

		/**
		 * The meta object literal for the '<em>Percentage Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.Float
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getPercentage_Object()
		 * @generated
		 */
		EDataType PERCENTAGE_OBJECT = eINSTANCE.getPercentage_Object();

		/**
		 * The meta object literal for the '<em>Priority</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getPriority()
		 * @generated
		 */
		EDataType PRIORITY = eINSTANCE.getPriority();

		/**
		 * The meta object literal for the '<em>Priority Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.Integer
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getPriority_Object()
		 * @generated
		 */
		EDataType PRIORITY_OBJECT = eINSTANCE.getPriority_Object();

		/**
		 * The meta object literal for the '<em>Time</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getTime()
		 * @generated
		 */
		EDataType TIME = eINSTANCE.getTime();

		/**
		 * The meta object literal for the '<em>Time Object</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.lang.Float
		 * @see es.esi.gemde.vv.mast.mastresult.impl.ResultPackageImpl#getTime_Object()
		 * @generated
		 */
		EDataType TIME_OBJECT = eINSTANCE.getTime_Object();

	}

} //ResultPackage
