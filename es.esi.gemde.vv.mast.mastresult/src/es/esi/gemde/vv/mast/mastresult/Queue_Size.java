/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult;

import java.math.BigInteger;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Queue Size</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Queue_Size#getMaxNum <em>Max Num</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getQueue_Size()
 * @model extendedMetaData="name='Queue_Size' kind='empty'"
 * @generated
 */
public interface Queue_Size extends EObject {
	/**
	 * Returns the value of the '<em><b>Max Num</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Num</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Num</em>' attribute.
	 * @see #setMaxNum(BigInteger)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getQueue_Size_MaxNum()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.NonNegativeInteger"
	 *        extendedMetaData="kind='attribute' name='Max_Num'"
	 * @generated
	 */
	BigInteger getMaxNum();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Queue_Size#getMaxNum <em>Max Num</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Num</em>' attribute.
	 * @see #getMaxNum()
	 * @generated
	 */
	void setMaxNum(BigInteger value);

} // Queue_Size
