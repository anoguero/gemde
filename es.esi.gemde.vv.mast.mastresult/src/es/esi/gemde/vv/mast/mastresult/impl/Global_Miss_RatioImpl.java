/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult.impl;

import es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio;
import es.esi.gemde.vv.mast.mastresult.Miss_Ratio_List;
import es.esi.gemde.vv.mast.mastresult.ResultPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Global Miss Ratio</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.Global_Miss_RatioImpl#getMissRatios <em>Miss Ratios</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.Global_Miss_RatioImpl#getReferencedEvent <em>Referenced Event</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Global_Miss_RatioImpl extends EObjectImpl implements Global_Miss_Ratio {
	/**
	 * The cached value of the '{@link #getMissRatios() <em>Miss Ratios</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMissRatios()
	 * @generated
	 * @ordered
	 */
	protected Miss_Ratio_List missRatios;

	/**
	 * The default value of the '{@link #getReferencedEvent() <em>Referenced Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencedEvent()
	 * @generated
	 * @ordered
	 */
	protected static final String REFERENCED_EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReferencedEvent() <em>Referenced Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferencedEvent()
	 * @generated
	 * @ordered
	 */
	protected String referencedEvent = REFERENCED_EVENT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Global_Miss_RatioImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResultPackage.Literals.GLOBAL_MISS_RATIO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Miss_Ratio_List getMissRatios() {
		return missRatios;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetMissRatios(Miss_Ratio_List newMissRatios, NotificationChain msgs) {
		Miss_Ratio_List oldMissRatios = missRatios;
		missRatios = newMissRatios;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResultPackage.GLOBAL_MISS_RATIO__MISS_RATIOS, oldMissRatios, newMissRatios);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMissRatios(Miss_Ratio_List newMissRatios) {
		if (newMissRatios != missRatios) {
			NotificationChain msgs = null;
			if (missRatios != null)
				msgs = ((InternalEObject)missRatios).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResultPackage.GLOBAL_MISS_RATIO__MISS_RATIOS, null, msgs);
			if (newMissRatios != null)
				msgs = ((InternalEObject)newMissRatios).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResultPackage.GLOBAL_MISS_RATIO__MISS_RATIOS, null, msgs);
			msgs = basicSetMissRatios(newMissRatios, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultPackage.GLOBAL_MISS_RATIO__MISS_RATIOS, newMissRatios, newMissRatios));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getReferencedEvent() {
		return referencedEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReferencedEvent(String newReferencedEvent) {
		String oldReferencedEvent = referencedEvent;
		referencedEvent = newReferencedEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultPackage.GLOBAL_MISS_RATIO__REFERENCED_EVENT, oldReferencedEvent, referencedEvent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResultPackage.GLOBAL_MISS_RATIO__MISS_RATIOS:
				return basicSetMissRatios(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResultPackage.GLOBAL_MISS_RATIO__MISS_RATIOS:
				return getMissRatios();
			case ResultPackage.GLOBAL_MISS_RATIO__REFERENCED_EVENT:
				return getReferencedEvent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResultPackage.GLOBAL_MISS_RATIO__MISS_RATIOS:
				setMissRatios((Miss_Ratio_List)newValue);
				return;
			case ResultPackage.GLOBAL_MISS_RATIO__REFERENCED_EVENT:
				setReferencedEvent((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResultPackage.GLOBAL_MISS_RATIO__MISS_RATIOS:
				setMissRatios((Miss_Ratio_List)null);
				return;
			case ResultPackage.GLOBAL_MISS_RATIO__REFERENCED_EVENT:
				setReferencedEvent(REFERENCED_EVENT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResultPackage.GLOBAL_MISS_RATIO__MISS_RATIOS:
				return missRatios != null;
			case ResultPackage.GLOBAL_MISS_RATIO__REFERENCED_EVENT:
				return REFERENCED_EVENT_EDEFAULT == null ? referencedEvent != null : !REFERENCED_EVENT_EDEFAULT.equals(referencedEvent);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (referencedEvent: ");
		result.append(referencedEvent);
		result.append(')');
		return result.toString();
	}

} //Global_Miss_RatioImpl
