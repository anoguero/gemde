/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult.impl;

import es.esi.gemde.vv.mast.mastresult.Fixed_Priority_Policy;
import es.esi.gemde.vv.mast.mastresult.Interrupt_FP_Policy;
import es.esi.gemde.vv.mast.mastresult.Non_Preemptible_FP_Policy;
import es.esi.gemde.vv.mast.mastresult.Polling_Policy;
import es.esi.gemde.vv.mast.mastresult.ResultPackage;
import es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results;
import es.esi.gemde.vv.mast.mastresult.Sporadic_Server_Policy;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Scheduling Server Results</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.Scheduling_Server_ResultsImpl#getNonPreemptibleFPPolicy <em>Non Preemptible FP Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.Scheduling_Server_ResultsImpl#getFixedPriorityPolicy <em>Fixed Priority Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.Scheduling_Server_ResultsImpl#getInterruptFPPolicy <em>Interrupt FP Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.Scheduling_Server_ResultsImpl#getPollingPolicy <em>Polling Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.Scheduling_Server_ResultsImpl#getSporadicServerPolicy <em>Sporadic Server Policy</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.Scheduling_Server_ResultsImpl#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Scheduling_Server_ResultsImpl extends EObjectImpl implements Scheduling_Server_Results {
	/**
	 * The cached value of the '{@link #getNonPreemptibleFPPolicy() <em>Non Preemptible FP Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNonPreemptibleFPPolicy()
	 * @generated
	 * @ordered
	 */
	protected Non_Preemptible_FP_Policy nonPreemptibleFPPolicy;

	/**
	 * The cached value of the '{@link #getFixedPriorityPolicy() <em>Fixed Priority Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFixedPriorityPolicy()
	 * @generated
	 * @ordered
	 */
	protected Fixed_Priority_Policy fixedPriorityPolicy;

	/**
	 * The cached value of the '{@link #getInterruptFPPolicy() <em>Interrupt FP Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterruptFPPolicy()
	 * @generated
	 * @ordered
	 */
	protected Interrupt_FP_Policy interruptFPPolicy;

	/**
	 * The cached value of the '{@link #getPollingPolicy() <em>Polling Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPollingPolicy()
	 * @generated
	 * @ordered
	 */
	protected Polling_Policy pollingPolicy;

	/**
	 * The cached value of the '{@link #getSporadicServerPolicy() <em>Sporadic Server Policy</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSporadicServerPolicy()
	 * @generated
	 * @ordered
	 */
	protected Sporadic_Server_Policy sporadicServerPolicy;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Scheduling_Server_ResultsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResultPackage.Literals.SCHEDULING_SERVER_RESULTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Non_Preemptible_FP_Policy getNonPreemptibleFPPolicy() {
		return nonPreemptibleFPPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNonPreemptibleFPPolicy(Non_Preemptible_FP_Policy newNonPreemptibleFPPolicy, NotificationChain msgs) {
		Non_Preemptible_FP_Policy oldNonPreemptibleFPPolicy = nonPreemptibleFPPolicy;
		nonPreemptibleFPPolicy = newNonPreemptibleFPPolicy;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResultPackage.SCHEDULING_SERVER_RESULTS__NON_PREEMPTIBLE_FP_POLICY, oldNonPreemptibleFPPolicy, newNonPreemptibleFPPolicy);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNonPreemptibleFPPolicy(Non_Preemptible_FP_Policy newNonPreemptibleFPPolicy) {
		if (newNonPreemptibleFPPolicy != nonPreemptibleFPPolicy) {
			NotificationChain msgs = null;
			if (nonPreemptibleFPPolicy != null)
				msgs = ((InternalEObject)nonPreemptibleFPPolicy).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResultPackage.SCHEDULING_SERVER_RESULTS__NON_PREEMPTIBLE_FP_POLICY, null, msgs);
			if (newNonPreemptibleFPPolicy != null)
				msgs = ((InternalEObject)newNonPreemptibleFPPolicy).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResultPackage.SCHEDULING_SERVER_RESULTS__NON_PREEMPTIBLE_FP_POLICY, null, msgs);
			msgs = basicSetNonPreemptibleFPPolicy(newNonPreemptibleFPPolicy, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultPackage.SCHEDULING_SERVER_RESULTS__NON_PREEMPTIBLE_FP_POLICY, newNonPreemptibleFPPolicy, newNonPreemptibleFPPolicy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Fixed_Priority_Policy getFixedPriorityPolicy() {
		return fixedPriorityPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFixedPriorityPolicy(Fixed_Priority_Policy newFixedPriorityPolicy, NotificationChain msgs) {
		Fixed_Priority_Policy oldFixedPriorityPolicy = fixedPriorityPolicy;
		fixedPriorityPolicy = newFixedPriorityPolicy;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResultPackage.SCHEDULING_SERVER_RESULTS__FIXED_PRIORITY_POLICY, oldFixedPriorityPolicy, newFixedPriorityPolicy);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFixedPriorityPolicy(Fixed_Priority_Policy newFixedPriorityPolicy) {
		if (newFixedPriorityPolicy != fixedPriorityPolicy) {
			NotificationChain msgs = null;
			if (fixedPriorityPolicy != null)
				msgs = ((InternalEObject)fixedPriorityPolicy).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResultPackage.SCHEDULING_SERVER_RESULTS__FIXED_PRIORITY_POLICY, null, msgs);
			if (newFixedPriorityPolicy != null)
				msgs = ((InternalEObject)newFixedPriorityPolicy).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResultPackage.SCHEDULING_SERVER_RESULTS__FIXED_PRIORITY_POLICY, null, msgs);
			msgs = basicSetFixedPriorityPolicy(newFixedPriorityPolicy, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultPackage.SCHEDULING_SERVER_RESULTS__FIXED_PRIORITY_POLICY, newFixedPriorityPolicy, newFixedPriorityPolicy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interrupt_FP_Policy getInterruptFPPolicy() {
		return interruptFPPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInterruptFPPolicy(Interrupt_FP_Policy newInterruptFPPolicy, NotificationChain msgs) {
		Interrupt_FP_Policy oldInterruptFPPolicy = interruptFPPolicy;
		interruptFPPolicy = newInterruptFPPolicy;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResultPackage.SCHEDULING_SERVER_RESULTS__INTERRUPT_FP_POLICY, oldInterruptFPPolicy, newInterruptFPPolicy);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterruptFPPolicy(Interrupt_FP_Policy newInterruptFPPolicy) {
		if (newInterruptFPPolicy != interruptFPPolicy) {
			NotificationChain msgs = null;
			if (interruptFPPolicy != null)
				msgs = ((InternalEObject)interruptFPPolicy).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResultPackage.SCHEDULING_SERVER_RESULTS__INTERRUPT_FP_POLICY, null, msgs);
			if (newInterruptFPPolicy != null)
				msgs = ((InternalEObject)newInterruptFPPolicy).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResultPackage.SCHEDULING_SERVER_RESULTS__INTERRUPT_FP_POLICY, null, msgs);
			msgs = basicSetInterruptFPPolicy(newInterruptFPPolicy, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultPackage.SCHEDULING_SERVER_RESULTS__INTERRUPT_FP_POLICY, newInterruptFPPolicy, newInterruptFPPolicy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Polling_Policy getPollingPolicy() {
		return pollingPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetPollingPolicy(Polling_Policy newPollingPolicy, NotificationChain msgs) {
		Polling_Policy oldPollingPolicy = pollingPolicy;
		pollingPolicy = newPollingPolicy;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResultPackage.SCHEDULING_SERVER_RESULTS__POLLING_POLICY, oldPollingPolicy, newPollingPolicy);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPollingPolicy(Polling_Policy newPollingPolicy) {
		if (newPollingPolicy != pollingPolicy) {
			NotificationChain msgs = null;
			if (pollingPolicy != null)
				msgs = ((InternalEObject)pollingPolicy).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResultPackage.SCHEDULING_SERVER_RESULTS__POLLING_POLICY, null, msgs);
			if (newPollingPolicy != null)
				msgs = ((InternalEObject)newPollingPolicy).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResultPackage.SCHEDULING_SERVER_RESULTS__POLLING_POLICY, null, msgs);
			msgs = basicSetPollingPolicy(newPollingPolicy, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultPackage.SCHEDULING_SERVER_RESULTS__POLLING_POLICY, newPollingPolicy, newPollingPolicy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Sporadic_Server_Policy getSporadicServerPolicy() {
		return sporadicServerPolicy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSporadicServerPolicy(Sporadic_Server_Policy newSporadicServerPolicy, NotificationChain msgs) {
		Sporadic_Server_Policy oldSporadicServerPolicy = sporadicServerPolicy;
		sporadicServerPolicy = newSporadicServerPolicy;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResultPackage.SCHEDULING_SERVER_RESULTS__SPORADIC_SERVER_POLICY, oldSporadicServerPolicy, newSporadicServerPolicy);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSporadicServerPolicy(Sporadic_Server_Policy newSporadicServerPolicy) {
		if (newSporadicServerPolicy != sporadicServerPolicy) {
			NotificationChain msgs = null;
			if (sporadicServerPolicy != null)
				msgs = ((InternalEObject)sporadicServerPolicy).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResultPackage.SCHEDULING_SERVER_RESULTS__SPORADIC_SERVER_POLICY, null, msgs);
			if (newSporadicServerPolicy != null)
				msgs = ((InternalEObject)newSporadicServerPolicy).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResultPackage.SCHEDULING_SERVER_RESULTS__SPORADIC_SERVER_POLICY, null, msgs);
			msgs = basicSetSporadicServerPolicy(newSporadicServerPolicy, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultPackage.SCHEDULING_SERVER_RESULTS__SPORADIC_SERVER_POLICY, newSporadicServerPolicy, newSporadicServerPolicy));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultPackage.SCHEDULING_SERVER_RESULTS__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResultPackage.SCHEDULING_SERVER_RESULTS__NON_PREEMPTIBLE_FP_POLICY:
				return basicSetNonPreemptibleFPPolicy(null, msgs);
			case ResultPackage.SCHEDULING_SERVER_RESULTS__FIXED_PRIORITY_POLICY:
				return basicSetFixedPriorityPolicy(null, msgs);
			case ResultPackage.SCHEDULING_SERVER_RESULTS__INTERRUPT_FP_POLICY:
				return basicSetInterruptFPPolicy(null, msgs);
			case ResultPackage.SCHEDULING_SERVER_RESULTS__POLLING_POLICY:
				return basicSetPollingPolicy(null, msgs);
			case ResultPackage.SCHEDULING_SERVER_RESULTS__SPORADIC_SERVER_POLICY:
				return basicSetSporadicServerPolicy(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResultPackage.SCHEDULING_SERVER_RESULTS__NON_PREEMPTIBLE_FP_POLICY:
				return getNonPreemptibleFPPolicy();
			case ResultPackage.SCHEDULING_SERVER_RESULTS__FIXED_PRIORITY_POLICY:
				return getFixedPriorityPolicy();
			case ResultPackage.SCHEDULING_SERVER_RESULTS__INTERRUPT_FP_POLICY:
				return getInterruptFPPolicy();
			case ResultPackage.SCHEDULING_SERVER_RESULTS__POLLING_POLICY:
				return getPollingPolicy();
			case ResultPackage.SCHEDULING_SERVER_RESULTS__SPORADIC_SERVER_POLICY:
				return getSporadicServerPolicy();
			case ResultPackage.SCHEDULING_SERVER_RESULTS__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResultPackage.SCHEDULING_SERVER_RESULTS__NON_PREEMPTIBLE_FP_POLICY:
				setNonPreemptibleFPPolicy((Non_Preemptible_FP_Policy)newValue);
				return;
			case ResultPackage.SCHEDULING_SERVER_RESULTS__FIXED_PRIORITY_POLICY:
				setFixedPriorityPolicy((Fixed_Priority_Policy)newValue);
				return;
			case ResultPackage.SCHEDULING_SERVER_RESULTS__INTERRUPT_FP_POLICY:
				setInterruptFPPolicy((Interrupt_FP_Policy)newValue);
				return;
			case ResultPackage.SCHEDULING_SERVER_RESULTS__POLLING_POLICY:
				setPollingPolicy((Polling_Policy)newValue);
				return;
			case ResultPackage.SCHEDULING_SERVER_RESULTS__SPORADIC_SERVER_POLICY:
				setSporadicServerPolicy((Sporadic_Server_Policy)newValue);
				return;
			case ResultPackage.SCHEDULING_SERVER_RESULTS__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResultPackage.SCHEDULING_SERVER_RESULTS__NON_PREEMPTIBLE_FP_POLICY:
				setNonPreemptibleFPPolicy((Non_Preemptible_FP_Policy)null);
				return;
			case ResultPackage.SCHEDULING_SERVER_RESULTS__FIXED_PRIORITY_POLICY:
				setFixedPriorityPolicy((Fixed_Priority_Policy)null);
				return;
			case ResultPackage.SCHEDULING_SERVER_RESULTS__INTERRUPT_FP_POLICY:
				setInterruptFPPolicy((Interrupt_FP_Policy)null);
				return;
			case ResultPackage.SCHEDULING_SERVER_RESULTS__POLLING_POLICY:
				setPollingPolicy((Polling_Policy)null);
				return;
			case ResultPackage.SCHEDULING_SERVER_RESULTS__SPORADIC_SERVER_POLICY:
				setSporadicServerPolicy((Sporadic_Server_Policy)null);
				return;
			case ResultPackage.SCHEDULING_SERVER_RESULTS__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResultPackage.SCHEDULING_SERVER_RESULTS__NON_PREEMPTIBLE_FP_POLICY:
				return nonPreemptibleFPPolicy != null;
			case ResultPackage.SCHEDULING_SERVER_RESULTS__FIXED_PRIORITY_POLICY:
				return fixedPriorityPolicy != null;
			case ResultPackage.SCHEDULING_SERVER_RESULTS__INTERRUPT_FP_POLICY:
				return interruptFPPolicy != null;
			case ResultPackage.SCHEDULING_SERVER_RESULTS__POLLING_POLICY:
				return pollingPolicy != null;
			case ResultPackage.SCHEDULING_SERVER_RESULTS__SPORADIC_SERVER_POLICY:
				return sporadicServerPolicy != null;
			case ResultPackage.SCHEDULING_SERVER_RESULTS__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //Scheduling_Server_ResultsImpl
