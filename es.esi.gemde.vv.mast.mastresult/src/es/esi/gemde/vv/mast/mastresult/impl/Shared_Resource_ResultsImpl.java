/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult.impl;

import es.esi.gemde.vv.mast.mastresult.Queue_Size;
import es.esi.gemde.vv.mast.mastresult.ResultPackage;
import es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results;
import es.esi.gemde.vv.mast.mastresult.Utilization;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Shared Resource Results</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.Shared_Resource_ResultsImpl#getPriorityCeiling <em>Priority Ceiling</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.Shared_Resource_ResultsImpl#getUtilization <em>Utilization</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.Shared_Resource_ResultsImpl#getQueueSize <em>Queue Size</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.Shared_Resource_ResultsImpl#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Shared_Resource_ResultsImpl extends EObjectImpl implements Shared_Resource_Results {
	/**
	 * The default value of the '{@link #getPriorityCeiling() <em>Priority Ceiling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPriorityCeiling()
	 * @generated
	 * @ordered
	 */
	protected static final int PRIORITY_CEILING_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getPriorityCeiling() <em>Priority Ceiling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPriorityCeiling()
	 * @generated
	 * @ordered
	 */
	protected int priorityCeiling = PRIORITY_CEILING_EDEFAULT;

	/**
	 * This is true if the Priority Ceiling attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean priorityCeilingESet;

	/**
	 * The cached value of the '{@link #getUtilization() <em>Utilization</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUtilization()
	 * @generated
	 * @ordered
	 */
	protected Utilization utilization;

	/**
	 * The cached value of the '{@link #getQueueSize() <em>Queue Size</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQueueSize()
	 * @generated
	 * @ordered
	 */
	protected Queue_Size queueSize;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Shared_Resource_ResultsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResultPackage.Literals.SHARED_RESOURCE_RESULTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPriorityCeiling() {
		return priorityCeiling;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPriorityCeiling(int newPriorityCeiling) {
		int oldPriorityCeiling = priorityCeiling;
		priorityCeiling = newPriorityCeiling;
		boolean oldPriorityCeilingESet = priorityCeilingESet;
		priorityCeilingESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultPackage.SHARED_RESOURCE_RESULTS__PRIORITY_CEILING, oldPriorityCeiling, priorityCeiling, !oldPriorityCeilingESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetPriorityCeiling() {
		int oldPriorityCeiling = priorityCeiling;
		boolean oldPriorityCeilingESet = priorityCeilingESet;
		priorityCeiling = PRIORITY_CEILING_EDEFAULT;
		priorityCeilingESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ResultPackage.SHARED_RESOURCE_RESULTS__PRIORITY_CEILING, oldPriorityCeiling, PRIORITY_CEILING_EDEFAULT, oldPriorityCeilingESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetPriorityCeiling() {
		return priorityCeilingESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Utilization getUtilization() {
		return utilization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetUtilization(Utilization newUtilization, NotificationChain msgs) {
		Utilization oldUtilization = utilization;
		utilization = newUtilization;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResultPackage.SHARED_RESOURCE_RESULTS__UTILIZATION, oldUtilization, newUtilization);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUtilization(Utilization newUtilization) {
		if (newUtilization != utilization) {
			NotificationChain msgs = null;
			if (utilization != null)
				msgs = ((InternalEObject)utilization).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResultPackage.SHARED_RESOURCE_RESULTS__UTILIZATION, null, msgs);
			if (newUtilization != null)
				msgs = ((InternalEObject)newUtilization).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResultPackage.SHARED_RESOURCE_RESULTS__UTILIZATION, null, msgs);
			msgs = basicSetUtilization(newUtilization, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultPackage.SHARED_RESOURCE_RESULTS__UTILIZATION, newUtilization, newUtilization));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Queue_Size getQueueSize() {
		return queueSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetQueueSize(Queue_Size newQueueSize, NotificationChain msgs) {
		Queue_Size oldQueueSize = queueSize;
		queueSize = newQueueSize;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ResultPackage.SHARED_RESOURCE_RESULTS__QUEUE_SIZE, oldQueueSize, newQueueSize);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQueueSize(Queue_Size newQueueSize) {
		if (newQueueSize != queueSize) {
			NotificationChain msgs = null;
			if (queueSize != null)
				msgs = ((InternalEObject)queueSize).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ResultPackage.SHARED_RESOURCE_RESULTS__QUEUE_SIZE, null, msgs);
			if (newQueueSize != null)
				msgs = ((InternalEObject)newQueueSize).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ResultPackage.SHARED_RESOURCE_RESULTS__QUEUE_SIZE, null, msgs);
			msgs = basicSetQueueSize(newQueueSize, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultPackage.SHARED_RESOURCE_RESULTS__QUEUE_SIZE, newQueueSize, newQueueSize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultPackage.SHARED_RESOURCE_RESULTS__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResultPackage.SHARED_RESOURCE_RESULTS__UTILIZATION:
				return basicSetUtilization(null, msgs);
			case ResultPackage.SHARED_RESOURCE_RESULTS__QUEUE_SIZE:
				return basicSetQueueSize(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResultPackage.SHARED_RESOURCE_RESULTS__PRIORITY_CEILING:
				return getPriorityCeiling();
			case ResultPackage.SHARED_RESOURCE_RESULTS__UTILIZATION:
				return getUtilization();
			case ResultPackage.SHARED_RESOURCE_RESULTS__QUEUE_SIZE:
				return getQueueSize();
			case ResultPackage.SHARED_RESOURCE_RESULTS__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResultPackage.SHARED_RESOURCE_RESULTS__PRIORITY_CEILING:
				setPriorityCeiling((Integer)newValue);
				return;
			case ResultPackage.SHARED_RESOURCE_RESULTS__UTILIZATION:
				setUtilization((Utilization)newValue);
				return;
			case ResultPackage.SHARED_RESOURCE_RESULTS__QUEUE_SIZE:
				setQueueSize((Queue_Size)newValue);
				return;
			case ResultPackage.SHARED_RESOURCE_RESULTS__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResultPackage.SHARED_RESOURCE_RESULTS__PRIORITY_CEILING:
				unsetPriorityCeiling();
				return;
			case ResultPackage.SHARED_RESOURCE_RESULTS__UTILIZATION:
				setUtilization((Utilization)null);
				return;
			case ResultPackage.SHARED_RESOURCE_RESULTS__QUEUE_SIZE:
				setQueueSize((Queue_Size)null);
				return;
			case ResultPackage.SHARED_RESOURCE_RESULTS__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResultPackage.SHARED_RESOURCE_RESULTS__PRIORITY_CEILING:
				return isSetPriorityCeiling();
			case ResultPackage.SHARED_RESOURCE_RESULTS__UTILIZATION:
				return utilization != null;
			case ResultPackage.SHARED_RESOURCE_RESULTS__QUEUE_SIZE:
				return queueSize != null;
			case ResultPackage.SHARED_RESOURCE_RESULTS__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (priorityCeiling: ");
		if (priorityCeilingESet) result.append(priorityCeiling); else result.append("<unset>");
		result.append(", name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //Shared_Resource_ResultsImpl
