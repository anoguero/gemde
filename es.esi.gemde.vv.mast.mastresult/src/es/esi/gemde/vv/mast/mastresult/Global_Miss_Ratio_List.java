/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Global Miss Ratio List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio_List#getGlobalMissRatio <em>Global Miss Ratio</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getGlobal_Miss_Ratio_List()
 * @model extendedMetaData="name='Global_Miss_Ratio_List' kind='elementOnly'"
 * @generated
 */
public interface Global_Miss_Ratio_List extends EObject {
	/**
	 * Returns the value of the '<em><b>Global Miss Ratio</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Global Miss Ratio</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Global Miss Ratio</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getGlobal_Miss_Ratio_List_GlobalMissRatio()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Global_Miss_Ratio' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Global_Miss_Ratio> getGlobalMissRatio();

} // Global_Miss_Ratio_List
