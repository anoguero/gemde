/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult.impl;

import es.esi.gemde.vv.mast.mastresult.Affirmative_Assertion;
import es.esi.gemde.vv.mast.mastresult.Assertion;
import es.esi.gemde.vv.mast.mastresult.Detailed_Utilization;
import es.esi.gemde.vv.mast.mastresult.Document_Root;
import es.esi.gemde.vv.mast.mastresult.Fixed_Priority_Policy;
import es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio;
import es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio_List;
import es.esi.gemde.vv.mast.mastresult.Global_Response_Time;
import es.esi.gemde.vv.mast.mastresult.Global_Response_Time_List;
import es.esi.gemde.vv.mast.mastresult.Interrupt_FP_Policy;
import es.esi.gemde.vv.mast.mastresult.Miss_Ratio;
import es.esi.gemde.vv.mast.mastresult.Miss_Ratio_List;
import es.esi.gemde.vv.mast.mastresult.Non_Preemptible_FP_Policy;
import es.esi.gemde.vv.mast.mastresult.Operation_Results;
import es.esi.gemde.vv.mast.mastresult.Polling_Policy;
import es.esi.gemde.vv.mast.mastresult.Priority_Ceiling;
import es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results;
import es.esi.gemde.vv.mast.mastresult.Queue_Size;
import es.esi.gemde.vv.mast.mastresult.Ready_Queue_Size;
import es.esi.gemde.vv.mast.mastresult.ResultFactory;
import es.esi.gemde.vv.mast.mastresult.ResultPackage;
import es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results;
import es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results;
import es.esi.gemde.vv.mast.mastresult.Simulation_Timing_Result;
import es.esi.gemde.vv.mast.mastresult.Slack;
import es.esi.gemde.vv.mast.mastresult.Sporadic_Server_Policy;
import es.esi.gemde.vv.mast.mastresult.Timing_Result;
import es.esi.gemde.vv.mast.mastresult.Trace;
import es.esi.gemde.vv.mast.mastresult.Transaction_Results;
import es.esi.gemde.vv.mast.mastresult.Utilization;

import es.esi.gemde.vv.mast.mastresult.util.ResultValidator;

import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ResultPackageImpl extends EPackageImpl implements ResultPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass detailed_UtilizationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass document_RootEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fixed_Priority_PolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass global_Miss_RatioEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass global_Miss_Ratio_ListEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass global_Response_TimeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass global_Response_Time_ListEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interrupt_FP_PolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass miss_RatioEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass miss_Ratio_ListEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass non_Preemptible_FP_PolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operation_ResultsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass polling_PolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass priority_CeilingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass processing_Resource_ResultsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass queue_SizeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ready_Queue_SizeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass reaL_TIME_SITUATIONEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scheduling_Server_ResultsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass shared_Resource_ResultsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass simulation_Timing_ResultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass slackEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sporadic_Server_PolicyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timing_ResultEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass traceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transaction_ResultsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass utilizationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum affirmative_AssertionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum assertionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType affirmative_Assertion_ObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType assertion_ObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType date_TimeEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType external_ReferenceEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType factorEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType factor_ObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType identifierEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType normalized_Execution_TimeEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType normalized_Execution_Time_ObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType percentageEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType percentage_ObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType priorityEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType priority_ObjectEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType timeEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType time_ObjectEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ResultPackageImpl() {
		super(eNS_URI, ResultFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ResultPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ResultPackage init() {
		if (isInited) return (ResultPackage)EPackage.Registry.INSTANCE.getEPackage(ResultPackage.eNS_URI);

		// Obtain or create and register package
		ResultPackageImpl theResultPackage = (ResultPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ResultPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ResultPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		XMLTypePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theResultPackage.createPackageContents();

		// Initialize created meta-data
		theResultPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theResultPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return ResultValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theResultPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ResultPackage.eNS_URI, theResultPackage);
		return theResultPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDetailed_Utilization() {
		return detailed_UtilizationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDetailed_Utilization_Application() {
		return (EAttribute)detailed_UtilizationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDetailed_Utilization_ContextSwitch() {
		return (EAttribute)detailed_UtilizationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDetailed_Utilization_Driver() {
		return (EAttribute)detailed_UtilizationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDetailed_Utilization_Timer() {
		return (EAttribute)detailed_UtilizationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDetailed_Utilization_Total() {
		return (EAttribute)detailed_UtilizationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDocument_Root() {
		return document_RootEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDocument_Root_Mixed() {
		return (EAttribute)document_RootEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocument_Root_XMLNSPrefixMap() {
		return (EReference)document_RootEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocument_Root_XSISchemaLocation() {
		return (EReference)document_RootEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDocument_Root_REALTIMESITUATION() {
		return (EReference)document_RootEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFixed_Priority_Policy() {
		return fixed_Priority_PolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixed_Priority_Policy_Preassigned() {
		return (EAttribute)fixed_Priority_PolicyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFixed_Priority_Policy_ThePriority() {
		return (EAttribute)fixed_Priority_PolicyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGlobal_Miss_Ratio() {
		return global_Miss_RatioEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGlobal_Miss_Ratio_MissRatios() {
		return (EReference)global_Miss_RatioEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGlobal_Miss_Ratio_ReferencedEvent() {
		return (EAttribute)global_Miss_RatioEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGlobal_Miss_Ratio_List() {
		return global_Miss_Ratio_ListEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGlobal_Miss_Ratio_List_GlobalMissRatio() {
		return (EReference)global_Miss_Ratio_ListEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGlobal_Response_Time() {
		return global_Response_TimeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGlobal_Response_Time_ReferencedEvent() {
		return (EAttribute)global_Response_TimeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGlobal_Response_Time_TimeValue() {
		return (EAttribute)global_Response_TimeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGlobal_Response_Time_List() {
		return global_Response_Time_ListEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGlobal_Response_Time_List_GlobalResponseTime() {
		return (EReference)global_Response_Time_ListEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInterrupt_FP_Policy() {
		return interrupt_FP_PolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInterrupt_FP_Policy_Preassigned() {
		return (EAttribute)interrupt_FP_PolicyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getInterrupt_FP_Policy_ThePriority() {
		return (EAttribute)interrupt_FP_PolicyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMiss_Ratio() {
		return miss_RatioEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMiss_Ratio_Deadline() {
		return (EAttribute)miss_RatioEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMiss_Ratio_Ratio() {
		return (EAttribute)miss_RatioEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMiss_Ratio_List() {
		return miss_Ratio_ListEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMiss_Ratio_List_MissRatio() {
		return (EReference)miss_Ratio_ListEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNon_Preemptible_FP_Policy() {
		return non_Preemptible_FP_PolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNon_Preemptible_FP_Policy_Preassigned() {
		return (EAttribute)non_Preemptible_FP_PolicyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNon_Preemptible_FP_Policy_ThePriority() {
		return (EAttribute)non_Preemptible_FP_PolicyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperation_Results() {
		return operation_ResultsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperation_Results_Slack() {
		return (EReference)operation_ResultsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperation_Results_Name() {
		return (EAttribute)operation_ResultsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPolling_Policy() {
		return polling_PolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolling_Policy_PollingAvgOverhead() {
		return (EAttribute)polling_PolicyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolling_Policy_PollingBestOverhead() {
		return (EAttribute)polling_PolicyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolling_Policy_PollingPeriod() {
		return (EAttribute)polling_PolicyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolling_Policy_PollingWorstOverhead() {
		return (EAttribute)polling_PolicyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolling_Policy_Preassigned() {
		return (EAttribute)polling_PolicyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPolling_Policy_ThePriority() {
		return (EAttribute)polling_PolicyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPriority_Ceiling() {
		return priority_CeilingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPriority_Ceiling_Ceiling() {
		return (EAttribute)priority_CeilingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProcessing_Resource_Results() {
		return processing_Resource_ResultsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessing_Resource_Results_Group() {
		return (EAttribute)processing_Resource_ResultsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessing_Resource_Results_Slack() {
		return (EReference)processing_Resource_ResultsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessing_Resource_Results_DetailedUtilization() {
		return (EReference)processing_Resource_ResultsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessing_Resource_Results_Utilization() {
		return (EReference)processing_Resource_ResultsEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getProcessing_Resource_Results_ReadyQueueSize() {
		return (EReference)processing_Resource_ResultsEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getProcessing_Resource_Results_Name() {
		return (EAttribute)processing_Resource_ResultsEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getQueue_Size() {
		return queue_SizeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getQueue_Size_MaxNum() {
		return (EAttribute)queue_SizeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getReady_Queue_Size() {
		return ready_Queue_SizeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getReady_Queue_Size_MaxNum() {
		return (EAttribute)ready_Queue_SizeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getREAL_TIME_SITUATION() {
		return reaL_TIME_SITUATIONEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getREAL_TIME_SITUATION_Group() {
		return (EAttribute)reaL_TIME_SITUATIONEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getREAL_TIME_SITUATION_SystemSlack() {
		return (EReference)reaL_TIME_SITUATIONEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getREAL_TIME_SITUATION_Trace() {
		return (EReference)reaL_TIME_SITUATIONEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getREAL_TIME_SITUATION_Transaction() {
		return (EReference)reaL_TIME_SITUATIONEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getREAL_TIME_SITUATION_ProcessingResource() {
		return (EReference)reaL_TIME_SITUATIONEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getREAL_TIME_SITUATION_Operation() {
		return (EReference)reaL_TIME_SITUATIONEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getREAL_TIME_SITUATION_SchedulingServer() {
		return (EReference)reaL_TIME_SITUATIONEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getREAL_TIME_SITUATION_SharedResource() {
		return (EReference)reaL_TIME_SITUATIONEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getREAL_TIME_SITUATION_GenerationDate() {
		return (EAttribute)reaL_TIME_SITUATIONEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getREAL_TIME_SITUATION_GenerationProfile() {
		return (EAttribute)reaL_TIME_SITUATIONEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getREAL_TIME_SITUATION_GenerationTool() {
		return (EAttribute)reaL_TIME_SITUATIONEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getREAL_TIME_SITUATION_ModelDate() {
		return (EAttribute)reaL_TIME_SITUATIONEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getREAL_TIME_SITUATION_ModelName() {
		return (EAttribute)reaL_TIME_SITUATIONEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScheduling_Server_Results() {
		return scheduling_Server_ResultsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScheduling_Server_Results_NonPreemptibleFPPolicy() {
		return (EReference)scheduling_Server_ResultsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScheduling_Server_Results_FixedPriorityPolicy() {
		return (EReference)scheduling_Server_ResultsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScheduling_Server_Results_InterruptFPPolicy() {
		return (EReference)scheduling_Server_ResultsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScheduling_Server_Results_PollingPolicy() {
		return (EReference)scheduling_Server_ResultsEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScheduling_Server_Results_SporadicServerPolicy() {
		return (EReference)scheduling_Server_ResultsEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScheduling_Server_Results_Name() {
		return (EAttribute)scheduling_Server_ResultsEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getShared_Resource_Results() {
		return shared_Resource_ResultsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShared_Resource_Results_PriorityCeiling() {
		return (EAttribute)shared_Resource_ResultsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShared_Resource_Results_Utilization() {
		return (EReference)shared_Resource_ResultsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getShared_Resource_Results_QueueSize() {
		return (EReference)shared_Resource_ResultsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getShared_Resource_Results_Name() {
		return (EAttribute)shared_Resource_ResultsEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSimulation_Timing_Result() {
		return simulation_Timing_ResultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulation_Timing_Result_WorstGlobalResponseTimes() {
		return (EReference)simulation_Timing_ResultEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulation_Timing_Result_AvgGlobalResponseTimes() {
		return (EReference)simulation_Timing_ResultEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulation_Timing_Result_BestGlobalResponseTimes() {
		return (EReference)simulation_Timing_ResultEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulation_Timing_Result_Jitters() {
		return (EReference)simulation_Timing_ResultEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulation_Timing_Result_LocalMissRatios() {
		return (EReference)simulation_Timing_ResultEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSimulation_Timing_Result_GlobalMissRatios() {
		return (EReference)simulation_Timing_ResultEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulation_Timing_Result_AvgBlockingTime() {
		return (EAttribute)simulation_Timing_ResultEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulation_Timing_Result_AvgLocalResponseTime() {
		return (EAttribute)simulation_Timing_ResultEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulation_Timing_Result_BestLocalResponseTime() {
		return (EAttribute)simulation_Timing_ResultEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulation_Timing_Result_EventName() {
		return (EAttribute)simulation_Timing_ResultEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulation_Timing_Result_MaxPreemptionTime() {
		return (EAttribute)simulation_Timing_ResultEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulation_Timing_Result_NumOfQueuedActivations() {
		return (EAttribute)simulation_Timing_ResultEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulation_Timing_Result_NumOfSuspensions() {
		return (EAttribute)simulation_Timing_ResultEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulation_Timing_Result_SuspensionTime() {
		return (EAttribute)simulation_Timing_ResultEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulation_Timing_Result_WorstBlockingTime() {
		return (EAttribute)simulation_Timing_ResultEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSimulation_Timing_Result_WorstLocalResponseTime() {
		return (EAttribute)simulation_Timing_ResultEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSlack() {
		return slackEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSlack_Value() {
		return (EAttribute)slackEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSporadic_Server_Policy() {
		return sporadic_Server_PolicyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSporadic_Server_Policy_BackgroundPriority() {
		return (EAttribute)sporadic_Server_PolicyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSporadic_Server_Policy_InitialCapacity() {
		return (EAttribute)sporadic_Server_PolicyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSporadic_Server_Policy_MaxPendingReplenishments() {
		return (EAttribute)sporadic_Server_PolicyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSporadic_Server_Policy_NormalPriority() {
		return (EAttribute)sporadic_Server_PolicyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSporadic_Server_Policy_Preassigned() {
		return (EAttribute)sporadic_Server_PolicyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSporadic_Server_Policy_ReplenishmentPeriod() {
		return (EAttribute)sporadic_Server_PolicyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTiming_Result() {
		return timing_ResultEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTiming_Result_WorstGlobalResponseTimes() {
		return (EReference)timing_ResultEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTiming_Result_BestGlobalResponseTimes() {
		return (EReference)timing_ResultEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTiming_Result_Jitters() {
		return (EReference)timing_ResultEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTiming_Result_BestLocalResponseTime() {
		return (EAttribute)timing_ResultEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTiming_Result_EventName() {
		return (EAttribute)timing_ResultEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTiming_Result_NumOfSuspensions() {
		return (EAttribute)timing_ResultEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTiming_Result_WorstBlockingTime() {
		return (EAttribute)timing_ResultEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTiming_Result_WorstLocalResponseTime() {
		return (EAttribute)timing_ResultEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTrace() {
		return traceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTrace_Pathname() {
		return (EAttribute)traceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransaction_Results() {
		return transaction_ResultsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransaction_Results_Group() {
		return (EAttribute)transaction_ResultsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransaction_Results_Slack() {
		return (EReference)transaction_ResultsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransaction_Results_TimingResult() {
		return (EReference)transaction_ResultsEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransaction_Results_SimulationTimingResult() {
		return (EReference)transaction_ResultsEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTransaction_Results_Name() {
		return (EAttribute)transaction_ResultsEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUtilization() {
		return utilizationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUtilization_Total() {
		return (EAttribute)utilizationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAffirmative_Assertion() {
		return affirmative_AssertionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAssertion() {
		return assertionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getAffirmative_Assertion_Object() {
		return affirmative_Assertion_ObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getAssertion_Object() {
		return assertion_ObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getDate_Time() {
		return date_TimeEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getExternal_Reference() {
		return external_ReferenceEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getFactor() {
		return factorEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getFactor_Object() {
		return factor_ObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getIdentifier() {
		return identifierEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getNormalized_Execution_Time() {
		return normalized_Execution_TimeEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getNormalized_Execution_Time_Object() {
		return normalized_Execution_Time_ObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getPercentage() {
		return percentageEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getPercentage_Object() {
		return percentage_ObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getPriority() {
		return priorityEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getPriority_Object() {
		return priority_ObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getTime() {
		return timeEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getTime_Object() {
		return time_ObjectEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResultFactory getResultFactory() {
		return (ResultFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		detailed_UtilizationEClass = createEClass(DETAILED_UTILIZATION);
		createEAttribute(detailed_UtilizationEClass, DETAILED_UTILIZATION__APPLICATION);
		createEAttribute(detailed_UtilizationEClass, DETAILED_UTILIZATION__CONTEXT_SWITCH);
		createEAttribute(detailed_UtilizationEClass, DETAILED_UTILIZATION__DRIVER);
		createEAttribute(detailed_UtilizationEClass, DETAILED_UTILIZATION__TIMER);
		createEAttribute(detailed_UtilizationEClass, DETAILED_UTILIZATION__TOTAL);

		document_RootEClass = createEClass(DOCUMENT_ROOT);
		createEAttribute(document_RootEClass, DOCUMENT_ROOT__MIXED);
		createEReference(document_RootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
		createEReference(document_RootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
		createEReference(document_RootEClass, DOCUMENT_ROOT__REALTIMESITUATION);

		fixed_Priority_PolicyEClass = createEClass(FIXED_PRIORITY_POLICY);
		createEAttribute(fixed_Priority_PolicyEClass, FIXED_PRIORITY_POLICY__PREASSIGNED);
		createEAttribute(fixed_Priority_PolicyEClass, FIXED_PRIORITY_POLICY__THE_PRIORITY);

		global_Miss_RatioEClass = createEClass(GLOBAL_MISS_RATIO);
		createEReference(global_Miss_RatioEClass, GLOBAL_MISS_RATIO__MISS_RATIOS);
		createEAttribute(global_Miss_RatioEClass, GLOBAL_MISS_RATIO__REFERENCED_EVENT);

		global_Miss_Ratio_ListEClass = createEClass(GLOBAL_MISS_RATIO_LIST);
		createEReference(global_Miss_Ratio_ListEClass, GLOBAL_MISS_RATIO_LIST__GLOBAL_MISS_RATIO);

		global_Response_TimeEClass = createEClass(GLOBAL_RESPONSE_TIME);
		createEAttribute(global_Response_TimeEClass, GLOBAL_RESPONSE_TIME__REFERENCED_EVENT);
		createEAttribute(global_Response_TimeEClass, GLOBAL_RESPONSE_TIME__TIME_VALUE);

		global_Response_Time_ListEClass = createEClass(GLOBAL_RESPONSE_TIME_LIST);
		createEReference(global_Response_Time_ListEClass, GLOBAL_RESPONSE_TIME_LIST__GLOBAL_RESPONSE_TIME);

		interrupt_FP_PolicyEClass = createEClass(INTERRUPT_FP_POLICY);
		createEAttribute(interrupt_FP_PolicyEClass, INTERRUPT_FP_POLICY__PREASSIGNED);
		createEAttribute(interrupt_FP_PolicyEClass, INTERRUPT_FP_POLICY__THE_PRIORITY);

		miss_RatioEClass = createEClass(MISS_RATIO);
		createEAttribute(miss_RatioEClass, MISS_RATIO__DEADLINE);
		createEAttribute(miss_RatioEClass, MISS_RATIO__RATIO);

		miss_Ratio_ListEClass = createEClass(MISS_RATIO_LIST);
		createEReference(miss_Ratio_ListEClass, MISS_RATIO_LIST__MISS_RATIO);

		non_Preemptible_FP_PolicyEClass = createEClass(NON_PREEMPTIBLE_FP_POLICY);
		createEAttribute(non_Preemptible_FP_PolicyEClass, NON_PREEMPTIBLE_FP_POLICY__PREASSIGNED);
		createEAttribute(non_Preemptible_FP_PolicyEClass, NON_PREEMPTIBLE_FP_POLICY__THE_PRIORITY);

		operation_ResultsEClass = createEClass(OPERATION_RESULTS);
		createEReference(operation_ResultsEClass, OPERATION_RESULTS__SLACK);
		createEAttribute(operation_ResultsEClass, OPERATION_RESULTS__NAME);

		polling_PolicyEClass = createEClass(POLLING_POLICY);
		createEAttribute(polling_PolicyEClass, POLLING_POLICY__POLLING_AVG_OVERHEAD);
		createEAttribute(polling_PolicyEClass, POLLING_POLICY__POLLING_BEST_OVERHEAD);
		createEAttribute(polling_PolicyEClass, POLLING_POLICY__POLLING_PERIOD);
		createEAttribute(polling_PolicyEClass, POLLING_POLICY__POLLING_WORST_OVERHEAD);
		createEAttribute(polling_PolicyEClass, POLLING_POLICY__PREASSIGNED);
		createEAttribute(polling_PolicyEClass, POLLING_POLICY__THE_PRIORITY);

		priority_CeilingEClass = createEClass(PRIORITY_CEILING);
		createEAttribute(priority_CeilingEClass, PRIORITY_CEILING__CEILING);

		processing_Resource_ResultsEClass = createEClass(PROCESSING_RESOURCE_RESULTS);
		createEAttribute(processing_Resource_ResultsEClass, PROCESSING_RESOURCE_RESULTS__GROUP);
		createEReference(processing_Resource_ResultsEClass, PROCESSING_RESOURCE_RESULTS__SLACK);
		createEReference(processing_Resource_ResultsEClass, PROCESSING_RESOURCE_RESULTS__DETAILED_UTILIZATION);
		createEReference(processing_Resource_ResultsEClass, PROCESSING_RESOURCE_RESULTS__UTILIZATION);
		createEReference(processing_Resource_ResultsEClass, PROCESSING_RESOURCE_RESULTS__READY_QUEUE_SIZE);
		createEAttribute(processing_Resource_ResultsEClass, PROCESSING_RESOURCE_RESULTS__NAME);

		queue_SizeEClass = createEClass(QUEUE_SIZE);
		createEAttribute(queue_SizeEClass, QUEUE_SIZE__MAX_NUM);

		ready_Queue_SizeEClass = createEClass(READY_QUEUE_SIZE);
		createEAttribute(ready_Queue_SizeEClass, READY_QUEUE_SIZE__MAX_NUM);

		reaL_TIME_SITUATIONEClass = createEClass(REAL_TIME_SITUATION);
		createEAttribute(reaL_TIME_SITUATIONEClass, REAL_TIME_SITUATION__GROUP);
		createEReference(reaL_TIME_SITUATIONEClass, REAL_TIME_SITUATION__SYSTEM_SLACK);
		createEReference(reaL_TIME_SITUATIONEClass, REAL_TIME_SITUATION__TRACE);
		createEReference(reaL_TIME_SITUATIONEClass, REAL_TIME_SITUATION__TRANSACTION);
		createEReference(reaL_TIME_SITUATIONEClass, REAL_TIME_SITUATION__PROCESSING_RESOURCE);
		createEReference(reaL_TIME_SITUATIONEClass, REAL_TIME_SITUATION__OPERATION);
		createEReference(reaL_TIME_SITUATIONEClass, REAL_TIME_SITUATION__SCHEDULING_SERVER);
		createEReference(reaL_TIME_SITUATIONEClass, REAL_TIME_SITUATION__SHARED_RESOURCE);
		createEAttribute(reaL_TIME_SITUATIONEClass, REAL_TIME_SITUATION__GENERATION_DATE);
		createEAttribute(reaL_TIME_SITUATIONEClass, REAL_TIME_SITUATION__GENERATION_PROFILE);
		createEAttribute(reaL_TIME_SITUATIONEClass, REAL_TIME_SITUATION__GENERATION_TOOL);
		createEAttribute(reaL_TIME_SITUATIONEClass, REAL_TIME_SITUATION__MODEL_DATE);
		createEAttribute(reaL_TIME_SITUATIONEClass, REAL_TIME_SITUATION__MODEL_NAME);

		scheduling_Server_ResultsEClass = createEClass(SCHEDULING_SERVER_RESULTS);
		createEReference(scheduling_Server_ResultsEClass, SCHEDULING_SERVER_RESULTS__NON_PREEMPTIBLE_FP_POLICY);
		createEReference(scheduling_Server_ResultsEClass, SCHEDULING_SERVER_RESULTS__FIXED_PRIORITY_POLICY);
		createEReference(scheduling_Server_ResultsEClass, SCHEDULING_SERVER_RESULTS__INTERRUPT_FP_POLICY);
		createEReference(scheduling_Server_ResultsEClass, SCHEDULING_SERVER_RESULTS__POLLING_POLICY);
		createEReference(scheduling_Server_ResultsEClass, SCHEDULING_SERVER_RESULTS__SPORADIC_SERVER_POLICY);
		createEAttribute(scheduling_Server_ResultsEClass, SCHEDULING_SERVER_RESULTS__NAME);

		shared_Resource_ResultsEClass = createEClass(SHARED_RESOURCE_RESULTS);
		createEAttribute(shared_Resource_ResultsEClass, SHARED_RESOURCE_RESULTS__PRIORITY_CEILING);
		createEReference(shared_Resource_ResultsEClass, SHARED_RESOURCE_RESULTS__UTILIZATION);
		createEReference(shared_Resource_ResultsEClass, SHARED_RESOURCE_RESULTS__QUEUE_SIZE);
		createEAttribute(shared_Resource_ResultsEClass, SHARED_RESOURCE_RESULTS__NAME);

		simulation_Timing_ResultEClass = createEClass(SIMULATION_TIMING_RESULT);
		createEReference(simulation_Timing_ResultEClass, SIMULATION_TIMING_RESULT__WORST_GLOBAL_RESPONSE_TIMES);
		createEReference(simulation_Timing_ResultEClass, SIMULATION_TIMING_RESULT__AVG_GLOBAL_RESPONSE_TIMES);
		createEReference(simulation_Timing_ResultEClass, SIMULATION_TIMING_RESULT__BEST_GLOBAL_RESPONSE_TIMES);
		createEReference(simulation_Timing_ResultEClass, SIMULATION_TIMING_RESULT__JITTERS);
		createEReference(simulation_Timing_ResultEClass, SIMULATION_TIMING_RESULT__LOCAL_MISS_RATIOS);
		createEReference(simulation_Timing_ResultEClass, SIMULATION_TIMING_RESULT__GLOBAL_MISS_RATIOS);
		createEAttribute(simulation_Timing_ResultEClass, SIMULATION_TIMING_RESULT__AVG_BLOCKING_TIME);
		createEAttribute(simulation_Timing_ResultEClass, SIMULATION_TIMING_RESULT__AVG_LOCAL_RESPONSE_TIME);
		createEAttribute(simulation_Timing_ResultEClass, SIMULATION_TIMING_RESULT__BEST_LOCAL_RESPONSE_TIME);
		createEAttribute(simulation_Timing_ResultEClass, SIMULATION_TIMING_RESULT__EVENT_NAME);
		createEAttribute(simulation_Timing_ResultEClass, SIMULATION_TIMING_RESULT__MAX_PREEMPTION_TIME);
		createEAttribute(simulation_Timing_ResultEClass, SIMULATION_TIMING_RESULT__NUM_OF_QUEUED_ACTIVATIONS);
		createEAttribute(simulation_Timing_ResultEClass, SIMULATION_TIMING_RESULT__NUM_OF_SUSPENSIONS);
		createEAttribute(simulation_Timing_ResultEClass, SIMULATION_TIMING_RESULT__SUSPENSION_TIME);
		createEAttribute(simulation_Timing_ResultEClass, SIMULATION_TIMING_RESULT__WORST_BLOCKING_TIME);
		createEAttribute(simulation_Timing_ResultEClass, SIMULATION_TIMING_RESULT__WORST_LOCAL_RESPONSE_TIME);

		slackEClass = createEClass(SLACK);
		createEAttribute(slackEClass, SLACK__VALUE);

		sporadic_Server_PolicyEClass = createEClass(SPORADIC_SERVER_POLICY);
		createEAttribute(sporadic_Server_PolicyEClass, SPORADIC_SERVER_POLICY__BACKGROUND_PRIORITY);
		createEAttribute(sporadic_Server_PolicyEClass, SPORADIC_SERVER_POLICY__INITIAL_CAPACITY);
		createEAttribute(sporadic_Server_PolicyEClass, SPORADIC_SERVER_POLICY__MAX_PENDING_REPLENISHMENTS);
		createEAttribute(sporadic_Server_PolicyEClass, SPORADIC_SERVER_POLICY__NORMAL_PRIORITY);
		createEAttribute(sporadic_Server_PolicyEClass, SPORADIC_SERVER_POLICY__PREASSIGNED);
		createEAttribute(sporadic_Server_PolicyEClass, SPORADIC_SERVER_POLICY__REPLENISHMENT_PERIOD);

		timing_ResultEClass = createEClass(TIMING_RESULT);
		createEReference(timing_ResultEClass, TIMING_RESULT__WORST_GLOBAL_RESPONSE_TIMES);
		createEReference(timing_ResultEClass, TIMING_RESULT__BEST_GLOBAL_RESPONSE_TIMES);
		createEReference(timing_ResultEClass, TIMING_RESULT__JITTERS);
		createEAttribute(timing_ResultEClass, TIMING_RESULT__BEST_LOCAL_RESPONSE_TIME);
		createEAttribute(timing_ResultEClass, TIMING_RESULT__EVENT_NAME);
		createEAttribute(timing_ResultEClass, TIMING_RESULT__NUM_OF_SUSPENSIONS);
		createEAttribute(timing_ResultEClass, TIMING_RESULT__WORST_BLOCKING_TIME);
		createEAttribute(timing_ResultEClass, TIMING_RESULT__WORST_LOCAL_RESPONSE_TIME);

		traceEClass = createEClass(TRACE);
		createEAttribute(traceEClass, TRACE__PATHNAME);

		transaction_ResultsEClass = createEClass(TRANSACTION_RESULTS);
		createEAttribute(transaction_ResultsEClass, TRANSACTION_RESULTS__GROUP);
		createEReference(transaction_ResultsEClass, TRANSACTION_RESULTS__SLACK);
		createEReference(transaction_ResultsEClass, TRANSACTION_RESULTS__TIMING_RESULT);
		createEReference(transaction_ResultsEClass, TRANSACTION_RESULTS__SIMULATION_TIMING_RESULT);
		createEAttribute(transaction_ResultsEClass, TRANSACTION_RESULTS__NAME);

		utilizationEClass = createEClass(UTILIZATION);
		createEAttribute(utilizationEClass, UTILIZATION__TOTAL);

		// Create enums
		affirmative_AssertionEEnum = createEEnum(AFFIRMATIVE_ASSERTION);
		assertionEEnum = createEEnum(ASSERTION);

		// Create data types
		affirmative_Assertion_ObjectEDataType = createEDataType(AFFIRMATIVE_ASSERTION_OBJECT);
		assertion_ObjectEDataType = createEDataType(ASSERTION_OBJECT);
		date_TimeEDataType = createEDataType(DATE_TIME);
		external_ReferenceEDataType = createEDataType(EXTERNAL_REFERENCE);
		factorEDataType = createEDataType(FACTOR);
		factor_ObjectEDataType = createEDataType(FACTOR_OBJECT);
		identifierEDataType = createEDataType(IDENTIFIER);
		normalized_Execution_TimeEDataType = createEDataType(NORMALIZED_EXECUTION_TIME);
		normalized_Execution_Time_ObjectEDataType = createEDataType(NORMALIZED_EXECUTION_TIME_OBJECT);
		percentageEDataType = createEDataType(PERCENTAGE);
		percentage_ObjectEDataType = createEDataType(PERCENTAGE_OBJECT);
		priorityEDataType = createEDataType(PRIORITY);
		priority_ObjectEDataType = createEDataType(PRIORITY_OBJECT);
		timeEDataType = createEDataType(TIME);
		time_ObjectEDataType = createEDataType(TIME_OBJECT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(detailed_UtilizationEClass, Detailed_Utilization.class, "Detailed_Utilization", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDetailed_Utilization_Application(), this.getPercentage(), "application", null, 0, 1, Detailed_Utilization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDetailed_Utilization_ContextSwitch(), this.getPercentage(), "contextSwitch", null, 0, 1, Detailed_Utilization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDetailed_Utilization_Driver(), this.getPercentage(), "driver", null, 0, 1, Detailed_Utilization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDetailed_Utilization_Timer(), this.getPercentage(), "timer", null, 0, 1, Detailed_Utilization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDetailed_Utilization_Total(), this.getPercentage(), "total", null, 0, 1, Detailed_Utilization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(document_RootEClass, Document_Root.class, "Document_Root", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDocument_Root_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocument_Root_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocument_Root_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDocument_Root_REALTIMESITUATION(), this.getREAL_TIME_SITUATION(), null, "rEALTIMESITUATION", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(fixed_Priority_PolicyEClass, Fixed_Priority_Policy.class, "Fixed_Priority_Policy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFixed_Priority_Policy_Preassigned(), this.getAssertion(), "preassigned", null, 0, 1, Fixed_Priority_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFixed_Priority_Policy_ThePriority(), this.getPriority(), "thePriority", null, 0, 1, Fixed_Priority_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(global_Miss_RatioEClass, Global_Miss_Ratio.class, "Global_Miss_Ratio", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGlobal_Miss_Ratio_MissRatios(), this.getMiss_Ratio_List(), null, "missRatios", null, 1, 1, Global_Miss_Ratio.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGlobal_Miss_Ratio_ReferencedEvent(), this.getExternal_Reference(), "referencedEvent", null, 0, 1, Global_Miss_Ratio.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(global_Miss_Ratio_ListEClass, Global_Miss_Ratio_List.class, "Global_Miss_Ratio_List", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGlobal_Miss_Ratio_List_GlobalMissRatio(), this.getGlobal_Miss_Ratio(), null, "globalMissRatio", null, 1, -1, Global_Miss_Ratio_List.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(global_Response_TimeEClass, Global_Response_Time.class, "Global_Response_Time", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGlobal_Response_Time_ReferencedEvent(), this.getExternal_Reference(), "referencedEvent", null, 0, 1, Global_Response_Time.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGlobal_Response_Time_TimeValue(), this.getTime(), "timeValue", null, 0, 1, Global_Response_Time.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(global_Response_Time_ListEClass, Global_Response_Time_List.class, "Global_Response_Time_List", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGlobal_Response_Time_List_GlobalResponseTime(), this.getGlobal_Response_Time(), null, "globalResponseTime", null, 1, -1, Global_Response_Time_List.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(interrupt_FP_PolicyEClass, Interrupt_FP_Policy.class, "Interrupt_FP_Policy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getInterrupt_FP_Policy_Preassigned(), this.getAffirmative_Assertion(), "preassigned", null, 0, 1, Interrupt_FP_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getInterrupt_FP_Policy_ThePriority(), this.getPriority(), "thePriority", null, 0, 1, Interrupt_FP_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(miss_RatioEClass, Miss_Ratio.class, "Miss_Ratio", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMiss_Ratio_Deadline(), this.getTime(), "deadline", null, 0, 1, Miss_Ratio.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMiss_Ratio_Ratio(), this.getPercentage(), "ratio", null, 0, 1, Miss_Ratio.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(miss_Ratio_ListEClass, Miss_Ratio_List.class, "Miss_Ratio_List", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMiss_Ratio_List_MissRatio(), this.getMiss_Ratio(), null, "missRatio", null, 1, -1, Miss_Ratio_List.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(non_Preemptible_FP_PolicyEClass, Non_Preemptible_FP_Policy.class, "Non_Preemptible_FP_Policy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNon_Preemptible_FP_Policy_Preassigned(), this.getAssertion(), "preassigned", null, 0, 1, Non_Preemptible_FP_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNon_Preemptible_FP_Policy_ThePriority(), this.getPriority(), "thePriority", null, 0, 1, Non_Preemptible_FP_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operation_ResultsEClass, Operation_Results.class, "Operation_Results", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperation_Results_Slack(), this.getSlack(), null, "slack", null, 1, 1, Operation_Results.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getOperation_Results_Name(), this.getIdentifier(), "name", null, 1, 1, Operation_Results.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(polling_PolicyEClass, Polling_Policy.class, "Polling_Policy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPolling_Policy_PollingAvgOverhead(), this.getNormalized_Execution_Time(), "pollingAvgOverhead", null, 0, 1, Polling_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPolling_Policy_PollingBestOverhead(), this.getNormalized_Execution_Time(), "pollingBestOverhead", null, 0, 1, Polling_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPolling_Policy_PollingPeriod(), this.getTime(), "pollingPeriod", null, 0, 1, Polling_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPolling_Policy_PollingWorstOverhead(), this.getNormalized_Execution_Time(), "pollingWorstOverhead", null, 0, 1, Polling_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPolling_Policy_Preassigned(), this.getAssertion(), "preassigned", null, 0, 1, Polling_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPolling_Policy_ThePriority(), this.getPriority(), "thePriority", null, 0, 1, Polling_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(priority_CeilingEClass, Priority_Ceiling.class, "Priority_Ceiling", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPriority_Ceiling_Ceiling(), this.getPriority(), "ceiling", null, 0, 1, Priority_Ceiling.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(processing_Resource_ResultsEClass, Processing_Resource_Results.class, "Processing_Resource_Results", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getProcessing_Resource_Results_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, Processing_Resource_Results.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getProcessing_Resource_Results_Slack(), this.getSlack(), null, "slack", null, 0, -1, Processing_Resource_Results.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getProcessing_Resource_Results_DetailedUtilization(), this.getDetailed_Utilization(), null, "detailedUtilization", null, 0, -1, Processing_Resource_Results.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getProcessing_Resource_Results_Utilization(), this.getUtilization(), null, "utilization", null, 0, -1, Processing_Resource_Results.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getProcessing_Resource_Results_ReadyQueueSize(), this.getReady_Queue_Size(), null, "readyQueueSize", null, 0, -1, Processing_Resource_Results.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getProcessing_Resource_Results_Name(), this.getIdentifier(), "name", null, 1, 1, Processing_Resource_Results.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(queue_SizeEClass, Queue_Size.class, "Queue_Size", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getQueue_Size_MaxNum(), theXMLTypePackage.getNonNegativeInteger(), "maxNum", null, 0, 1, Queue_Size.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ready_Queue_SizeEClass, Ready_Queue_Size.class, "Ready_Queue_Size", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getReady_Queue_Size_MaxNum(), theXMLTypePackage.getNonNegativeInteger(), "maxNum", null, 0, 1, Ready_Queue_Size.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(reaL_TIME_SITUATIONEClass, es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION.class, "REAL_TIME_SITUATION", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getREAL_TIME_SITUATION_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getREAL_TIME_SITUATION_SystemSlack(), this.getSlack(), null, "systemSlack", null, 0, -1, es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getREAL_TIME_SITUATION_Trace(), this.getTrace(), null, "trace", null, 0, -1, es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getREAL_TIME_SITUATION_Transaction(), this.getTransaction_Results(), null, "transaction", null, 0, -1, es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getREAL_TIME_SITUATION_ProcessingResource(), this.getProcessing_Resource_Results(), null, "processingResource", null, 0, -1, es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getREAL_TIME_SITUATION_Operation(), this.getOperation_Results(), null, "operation", null, 0, -1, es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getREAL_TIME_SITUATION_SchedulingServer(), this.getScheduling_Server_Results(), null, "schedulingServer", null, 0, -1, es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getREAL_TIME_SITUATION_SharedResource(), this.getShared_Resource_Results(), null, "sharedResource", null, 0, -1, es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getREAL_TIME_SITUATION_GenerationDate(), this.getDate_Time(), "generationDate", null, 1, 1, es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getREAL_TIME_SITUATION_GenerationProfile(), theXMLTypePackage.getString(), "generationProfile", null, 0, 1, es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getREAL_TIME_SITUATION_GenerationTool(), theXMLTypePackage.getString(), "generationTool", null, 1, 1, es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getREAL_TIME_SITUATION_ModelDate(), this.getDate_Time(), "modelDate", null, 1, 1, es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getREAL_TIME_SITUATION_ModelName(), this.getIdentifier(), "modelName", null, 1, 1, es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scheduling_Server_ResultsEClass, Scheduling_Server_Results.class, "Scheduling_Server_Results", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScheduling_Server_Results_NonPreemptibleFPPolicy(), this.getNon_Preemptible_FP_Policy(), null, "nonPreemptibleFPPolicy", null, 0, 1, Scheduling_Server_Results.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScheduling_Server_Results_FixedPriorityPolicy(), this.getFixed_Priority_Policy(), null, "fixedPriorityPolicy", null, 0, 1, Scheduling_Server_Results.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScheduling_Server_Results_InterruptFPPolicy(), this.getInterrupt_FP_Policy(), null, "interruptFPPolicy", null, 0, 1, Scheduling_Server_Results.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScheduling_Server_Results_PollingPolicy(), this.getPolling_Policy(), null, "pollingPolicy", null, 0, 1, Scheduling_Server_Results.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScheduling_Server_Results_SporadicServerPolicy(), this.getSporadic_Server_Policy(), null, "sporadicServerPolicy", null, 0, 1, Scheduling_Server_Results.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScheduling_Server_Results_Name(), this.getIdentifier(), "name", null, 1, 1, Scheduling_Server_Results.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(shared_Resource_ResultsEClass, Shared_Resource_Results.class, "Shared_Resource_Results", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getShared_Resource_Results_PriorityCeiling(), this.getPriority(), "priorityCeiling", null, 0, 1, Shared_Resource_Results.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getShared_Resource_Results_Utilization(), this.getUtilization(), null, "utilization", null, 0, 1, Shared_Resource_Results.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getShared_Resource_Results_QueueSize(), this.getQueue_Size(), null, "queueSize", null, 0, 1, Shared_Resource_Results.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getShared_Resource_Results_Name(), this.getIdentifier(), "name", null, 1, 1, Shared_Resource_Results.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(simulation_Timing_ResultEClass, Simulation_Timing_Result.class, "Simulation_Timing_Result", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSimulation_Timing_Result_WorstGlobalResponseTimes(), this.getGlobal_Response_Time_List(), null, "worstGlobalResponseTimes", null, 0, 1, Simulation_Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulation_Timing_Result_AvgGlobalResponseTimes(), this.getGlobal_Response_Time_List(), null, "avgGlobalResponseTimes", null, 0, 1, Simulation_Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulation_Timing_Result_BestGlobalResponseTimes(), this.getGlobal_Response_Time_List(), null, "bestGlobalResponseTimes", null, 0, 1, Simulation_Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulation_Timing_Result_Jitters(), this.getGlobal_Response_Time_List(), null, "jitters", null, 0, 1, Simulation_Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulation_Timing_Result_LocalMissRatios(), this.getMiss_Ratio_List(), null, "localMissRatios", null, 0, 1, Simulation_Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSimulation_Timing_Result_GlobalMissRatios(), this.getGlobal_Miss_Ratio_List(), null, "globalMissRatios", null, 0, 1, Simulation_Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulation_Timing_Result_AvgBlockingTime(), this.getTime(), "avgBlockingTime", null, 0, 1, Simulation_Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulation_Timing_Result_AvgLocalResponseTime(), this.getTime(), "avgLocalResponseTime", null, 0, 1, Simulation_Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulation_Timing_Result_BestLocalResponseTime(), this.getTime(), "bestLocalResponseTime", null, 0, 1, Simulation_Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulation_Timing_Result_EventName(), this.getIdentifier(), "eventName", null, 1, 1, Simulation_Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulation_Timing_Result_MaxPreemptionTime(), this.getTime(), "maxPreemptionTime", null, 0, 1, Simulation_Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulation_Timing_Result_NumOfQueuedActivations(), theXMLTypePackage.getNonNegativeInteger(), "numOfQueuedActivations", null, 0, 1, Simulation_Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulation_Timing_Result_NumOfSuspensions(), theXMLTypePackage.getNonNegativeInteger(), "numOfSuspensions", null, 0, 1, Simulation_Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulation_Timing_Result_SuspensionTime(), this.getTime(), "suspensionTime", null, 0, 1, Simulation_Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulation_Timing_Result_WorstBlockingTime(), this.getTime(), "worstBlockingTime", null, 0, 1, Simulation_Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSimulation_Timing_Result_WorstLocalResponseTime(), this.getTime(), "worstLocalResponseTime", null, 0, 1, Simulation_Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(slackEClass, Slack.class, "Slack", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSlack_Value(), this.getPercentage(), "value", null, 0, 1, Slack.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sporadic_Server_PolicyEClass, Sporadic_Server_Policy.class, "Sporadic_Server_Policy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSporadic_Server_Policy_BackgroundPriority(), this.getPriority(), "backgroundPriority", null, 0, 1, Sporadic_Server_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSporadic_Server_Policy_InitialCapacity(), this.getTime(), "initialCapacity", null, 0, 1, Sporadic_Server_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSporadic_Server_Policy_MaxPendingReplenishments(), theXMLTypePackage.getPositiveInteger(), "maxPendingReplenishments", null, 0, 1, Sporadic_Server_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSporadic_Server_Policy_NormalPriority(), this.getPriority(), "normalPriority", null, 0, 1, Sporadic_Server_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSporadic_Server_Policy_Preassigned(), this.getAssertion(), "preassigned", null, 0, 1, Sporadic_Server_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSporadic_Server_Policy_ReplenishmentPeriod(), this.getTime(), "replenishmentPeriod", null, 0, 1, Sporadic_Server_Policy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(timing_ResultEClass, Timing_Result.class, "Timing_Result", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTiming_Result_WorstGlobalResponseTimes(), this.getGlobal_Response_Time_List(), null, "worstGlobalResponseTimes", null, 0, 1, Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTiming_Result_BestGlobalResponseTimes(), this.getGlobal_Response_Time_List(), null, "bestGlobalResponseTimes", null, 0, 1, Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTiming_Result_Jitters(), this.getGlobal_Response_Time_List(), null, "jitters", null, 0, 1, Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTiming_Result_BestLocalResponseTime(), this.getTime(), "bestLocalResponseTime", null, 0, 1, Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTiming_Result_EventName(), this.getIdentifier(), "eventName", null, 1, 1, Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTiming_Result_NumOfSuspensions(), theXMLTypePackage.getNonNegativeInteger(), "numOfSuspensions", null, 0, 1, Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTiming_Result_WorstBlockingTime(), this.getTime(), "worstBlockingTime", null, 0, 1, Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTiming_Result_WorstLocalResponseTime(), this.getTime(), "worstLocalResponseTime", null, 0, 1, Timing_Result.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(traceEClass, Trace.class, "Trace", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTrace_Pathname(), theXMLTypePackage.getString(), "pathname", null, 0, 1, Trace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(transaction_ResultsEClass, Transaction_Results.class, "Transaction_Results", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTransaction_Results_Group(), ecorePackage.getEFeatureMapEntry(), "group", null, 0, -1, Transaction_Results.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransaction_Results_Slack(), this.getSlack(), null, "slack", null, 0, -1, Transaction_Results.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getTransaction_Results_TimingResult(), this.getTiming_Result(), null, "timingResult", null, 0, -1, Transaction_Results.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getTransaction_Results_SimulationTimingResult(), this.getSimulation_Timing_Result(), null, "simulationTimingResult", null, 0, -1, Transaction_Results.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getTransaction_Results_Name(), this.getIdentifier(), "name", null, 1, 1, Transaction_Results.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(utilizationEClass, Utilization.class, "Utilization", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUtilization_Total(), this.getPercentage(), "total", null, 0, 1, Utilization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(affirmative_AssertionEEnum, Affirmative_Assertion.class, "Affirmative_Assertion");
		addEEnumLiteral(affirmative_AssertionEEnum, Affirmative_Assertion.YES);

		initEEnum(assertionEEnum, Assertion.class, "Assertion");
		addEEnumLiteral(assertionEEnum, Assertion.YES);
		addEEnumLiteral(assertionEEnum, Assertion.NO);

		// Initialize data types
		initEDataType(affirmative_Assertion_ObjectEDataType, Affirmative_Assertion.class, "Affirmative_Assertion_Object", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(assertion_ObjectEDataType, Assertion.class, "Assertion_Object", IS_SERIALIZABLE, IS_GENERATED_INSTANCE_CLASS);
		initEDataType(date_TimeEDataType, XMLGregorianCalendar.class, "Date_Time", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(external_ReferenceEDataType, String.class, "External_Reference", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(factorEDataType, float.class, "Factor", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(factor_ObjectEDataType, Float.class, "Factor_Object", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(identifierEDataType, String.class, "Identifier", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(normalized_Execution_TimeEDataType, float.class, "Normalized_Execution_Time", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(normalized_Execution_Time_ObjectEDataType, Float.class, "Normalized_Execution_Time_Object", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(percentageEDataType, float.class, "Percentage", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(percentage_ObjectEDataType, Float.class, "Percentage_Object", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(priorityEDataType, int.class, "Priority", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(priority_ObjectEDataType, Integer.class, "Priority_Object", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(timeEDataType, float.class, "Time", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(time_ObjectEDataType, Float.class, "Time_Object", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http:///org/eclipse/emf/ecore/util/ExtendedMetaData
		createExtendedMetaDataAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createExtendedMetaDataAnnotations() {
		String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";		
		addAnnotation
		  (affirmative_AssertionEEnum, 
		   source, 
		   new String[] {
			 "name", "Affirmative_Assertion"
		   });		
		addAnnotation
		  (affirmative_Assertion_ObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "Affirmative_Assertion:Object",
			 "baseType", "Affirmative_Assertion"
		   });		
		addAnnotation
		  (assertionEEnum, 
		   source, 
		   new String[] {
			 "name", "Assertion"
		   });		
		addAnnotation
		  (assertion_ObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "Assertion:Object",
			 "baseType", "Assertion"
		   });		
		addAnnotation
		  (date_TimeEDataType, 
		   source, 
		   new String[] {
			 "name", "Date_Time",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#dateTime"
		   });		
		addAnnotation
		  (detailed_UtilizationEClass, 
		   source, 
		   new String[] {
			 "name", "Detailed_Utilization",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getDetailed_Utilization_Application(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Application"
		   });		
		addAnnotation
		  (getDetailed_Utilization_ContextSwitch(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Context_Switch"
		   });		
		addAnnotation
		  (getDetailed_Utilization_Driver(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Driver"
		   });		
		addAnnotation
		  (getDetailed_Utilization_Timer(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Timer"
		   });		
		addAnnotation
		  (getDetailed_Utilization_Total(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Total"
		   });		
		addAnnotation
		  (document_RootEClass, 
		   source, 
		   new String[] {
			 "name", "",
			 "kind", "mixed"
		   });		
		addAnnotation
		  (getDocument_Root_Mixed(), 
		   source, 
		   new String[] {
			 "kind", "elementWildcard",
			 "name", ":mixed"
		   });		
		addAnnotation
		  (getDocument_Root_XMLNSPrefixMap(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "xmlns:prefix"
		   });		
		addAnnotation
		  (getDocument_Root_XSISchemaLocation(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "xsi:schemaLocation"
		   });		
		addAnnotation
		  (getDocument_Root_REALTIMESITUATION(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "REAL_TIME_SITUATION",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (external_ReferenceEDataType, 
		   source, 
		   new String[] {
			 "name", "External_Reference",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#NCName",
			 "pattern", "([a-z]|[A-Z])([a-z]|[A-Z]|[0-9]|.|_)*"
		   });		
		addAnnotation
		  (factorEDataType, 
		   source, 
		   new String[] {
			 "name", "Factor",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#float",
			 "minExclusive", "0"
		   });		
		addAnnotation
		  (factor_ObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "Factor:Object",
			 "baseType", "Factor"
		   });		
		addAnnotation
		  (fixed_Priority_PolicyEClass, 
		   source, 
		   new String[] {
			 "name", "Fixed_Priority_Policy",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getFixed_Priority_Policy_Preassigned(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Preassigned"
		   });		
		addAnnotation
		  (getFixed_Priority_Policy_ThePriority(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "The_Priority"
		   });		
		addAnnotation
		  (global_Miss_RatioEClass, 
		   source, 
		   new String[] {
			 "name", "Global_Miss_Ratio",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getGlobal_Miss_Ratio_MissRatios(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Miss_Ratios",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getGlobal_Miss_Ratio_ReferencedEvent(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Referenced_Event"
		   });		
		addAnnotation
		  (global_Miss_Ratio_ListEClass, 
		   source, 
		   new String[] {
			 "name", "Global_Miss_Ratio_List",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getGlobal_Miss_Ratio_List_GlobalMissRatio(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Global_Miss_Ratio",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (global_Response_TimeEClass, 
		   source, 
		   new String[] {
			 "name", "Global_Response_Time",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getGlobal_Response_Time_ReferencedEvent(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Referenced_Event"
		   });		
		addAnnotation
		  (getGlobal_Response_Time_TimeValue(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Time_Value"
		   });		
		addAnnotation
		  (global_Response_Time_ListEClass, 
		   source, 
		   new String[] {
			 "name", "Global_Response_Time_List",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getGlobal_Response_Time_List_GlobalResponseTime(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Global_Response_Time",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (identifierEDataType, 
		   source, 
		   new String[] {
			 "name", "Identifier",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#ID",
			 "pattern", "([a-z]|[A-Z])([a-z]|[A-Z]|[0-9]|.|_)*"
		   });		
		addAnnotation
		  (interrupt_FP_PolicyEClass, 
		   source, 
		   new String[] {
			 "name", "Interrupt_FP_Policy",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getInterrupt_FP_Policy_Preassigned(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Preassigned"
		   });		
		addAnnotation
		  (getInterrupt_FP_Policy_ThePriority(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "The_Priority"
		   });		
		addAnnotation
		  (miss_RatioEClass, 
		   source, 
		   new String[] {
			 "name", "Miss_Ratio",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getMiss_Ratio_Deadline(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Deadline"
		   });		
		addAnnotation
		  (getMiss_Ratio_Ratio(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Ratio"
		   });		
		addAnnotation
		  (miss_Ratio_ListEClass, 
		   source, 
		   new String[] {
			 "name", "Miss_Ratio_List",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getMiss_Ratio_List_MissRatio(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Miss_Ratio",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (non_Preemptible_FP_PolicyEClass, 
		   source, 
		   new String[] {
			 "name", "Non_Preemptible_FP_Policy",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getNon_Preemptible_FP_Policy_Preassigned(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Preassigned"
		   });		
		addAnnotation
		  (getNon_Preemptible_FP_Policy_ThePriority(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "The_Priority"
		   });		
		addAnnotation
		  (normalized_Execution_TimeEDataType, 
		   source, 
		   new String[] {
			 "name", "Normalized_Execution_Time",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#float",
			 "minInclusive", "0.0"
		   });		
		addAnnotation
		  (normalized_Execution_Time_ObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "Normalized_Execution_Time:Object",
			 "baseType", "Normalized_Execution_Time"
		   });		
		addAnnotation
		  (operation_ResultsEClass, 
		   source, 
		   new String[] {
			 "name", "Operation_Results",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getOperation_Results_Slack(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Slack",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getOperation_Results_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (percentageEDataType, 
		   source, 
		   new String[] {
			 "name", "Percentage",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#float",
			 "minInclusive", "0.0",
			 "maxInclusive", "100.0"
		   });		
		addAnnotation
		  (percentage_ObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "Percentage:Object",
			 "baseType", "Percentage"
		   });		
		addAnnotation
		  (polling_PolicyEClass, 
		   source, 
		   new String[] {
			 "name", "Polling_Policy",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getPolling_Policy_PollingAvgOverhead(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Polling_Avg_Overhead"
		   });		
		addAnnotation
		  (getPolling_Policy_PollingBestOverhead(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Polling_Best_Overhead"
		   });		
		addAnnotation
		  (getPolling_Policy_PollingPeriod(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Polling_Period"
		   });		
		addAnnotation
		  (getPolling_Policy_PollingWorstOverhead(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Polling_Worst_Overhead"
		   });		
		addAnnotation
		  (getPolling_Policy_Preassigned(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Preassigned"
		   });		
		addAnnotation
		  (getPolling_Policy_ThePriority(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "The_Priority"
		   });		
		addAnnotation
		  (priorityEDataType, 
		   source, 
		   new String[] {
			 "name", "Priority",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#int",
			 "minInclusive", "0"
		   });		
		addAnnotation
		  (priority_CeilingEClass, 
		   source, 
		   new String[] {
			 "name", "Priority_Ceiling",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getPriority_Ceiling_Ceiling(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Ceiling"
		   });		
		addAnnotation
		  (priority_ObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "Priority:Object",
			 "baseType", "Priority"
		   });		
		addAnnotation
		  (processing_Resource_ResultsEClass, 
		   source, 
		   new String[] {
			 "name", "Processing_Resource_Results",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getProcessing_Resource_Results_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });		
		addAnnotation
		  (getProcessing_Resource_Results_Slack(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Slack",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getProcessing_Resource_Results_DetailedUtilization(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Detailed_Utilization",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getProcessing_Resource_Results_Utilization(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Utilization",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getProcessing_Resource_Results_ReadyQueueSize(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Ready_Queue_Size",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getProcessing_Resource_Results_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (queue_SizeEClass, 
		   source, 
		   new String[] {
			 "name", "Queue_Size",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getQueue_Size_MaxNum(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Max_Num"
		   });		
		addAnnotation
		  (ready_Queue_SizeEClass, 
		   source, 
		   new String[] {
			 "name", "Ready_Queue_Size",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getReady_Queue_Size_MaxNum(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Max_Num"
		   });		
		addAnnotation
		  (reaL_TIME_SITUATIONEClass, 
		   source, 
		   new String[] {
			 "name", "REAL_TIME_SITUATION_._type",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getREAL_TIME_SITUATION_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });		
		addAnnotation
		  (getREAL_TIME_SITUATION_SystemSlack(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Slack",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getREAL_TIME_SITUATION_Trace(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Trace",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getREAL_TIME_SITUATION_Transaction(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Transaction",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getREAL_TIME_SITUATION_ProcessingResource(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Processing_Resource",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getREAL_TIME_SITUATION_Operation(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Operation",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getREAL_TIME_SITUATION_SchedulingServer(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Scheduling_Server",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getREAL_TIME_SITUATION_SharedResource(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Shared_Resource",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getREAL_TIME_SITUATION_GenerationDate(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Generation_Date"
		   });		
		addAnnotation
		  (getREAL_TIME_SITUATION_GenerationProfile(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Generation_Profile"
		   });		
		addAnnotation
		  (getREAL_TIME_SITUATION_GenerationTool(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Generation_Tool"
		   });		
		addAnnotation
		  (getREAL_TIME_SITUATION_ModelDate(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Model_Date"
		   });		
		addAnnotation
		  (getREAL_TIME_SITUATION_ModelName(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Model_Name"
		   });		
		addAnnotation
		  (scheduling_Server_ResultsEClass, 
		   source, 
		   new String[] {
			 "name", "Scheduling_Server_Results",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getScheduling_Server_Results_NonPreemptibleFPPolicy(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Non_Preemptible_FP_Policy",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getScheduling_Server_Results_FixedPriorityPolicy(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Fixed_Priority_Policy",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getScheduling_Server_Results_InterruptFPPolicy(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Interrupt_FP_Policy",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getScheduling_Server_Results_PollingPolicy(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Polling_Policy",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getScheduling_Server_Results_SporadicServerPolicy(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Sporadic_Server_Policy",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getScheduling_Server_Results_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (shared_Resource_ResultsEClass, 
		   source, 
		   new String[] {
			 "name", "Shared_Resource_Results",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getShared_Resource_Results_PriorityCeiling(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Priority_Ceiling",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getShared_Resource_Results_Utilization(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Utilization",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getShared_Resource_Results_QueueSize(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Queue_Size",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getShared_Resource_Results_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (simulation_Timing_ResultEClass, 
		   source, 
		   new String[] {
			 "name", "Simulation_Timing_Result",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getSimulation_Timing_Result_WorstGlobalResponseTimes(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Worst_Global_Response_Times",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getSimulation_Timing_Result_AvgGlobalResponseTimes(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Avg_Global_Response_Times",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getSimulation_Timing_Result_BestGlobalResponseTimes(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Best_Global_Response_Times",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getSimulation_Timing_Result_Jitters(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Jitters",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getSimulation_Timing_Result_LocalMissRatios(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Local_Miss_Ratios",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getSimulation_Timing_Result_GlobalMissRatios(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Global_Miss_Ratios",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getSimulation_Timing_Result_AvgBlockingTime(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Avg_Blocking_Time"
		   });		
		addAnnotation
		  (getSimulation_Timing_Result_AvgLocalResponseTime(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Avg_Local_Response_Time"
		   });		
		addAnnotation
		  (getSimulation_Timing_Result_BestLocalResponseTime(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Best_Local_Response_Time"
		   });		
		addAnnotation
		  (getSimulation_Timing_Result_EventName(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Event_Name"
		   });		
		addAnnotation
		  (getSimulation_Timing_Result_MaxPreemptionTime(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Max_Preemption_Time"
		   });		
		addAnnotation
		  (getSimulation_Timing_Result_NumOfQueuedActivations(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Num_Of_Queued_Activations"
		   });		
		addAnnotation
		  (getSimulation_Timing_Result_NumOfSuspensions(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Num_Of_Suspensions"
		   });		
		addAnnotation
		  (getSimulation_Timing_Result_SuspensionTime(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Suspension_Time"
		   });		
		addAnnotation
		  (getSimulation_Timing_Result_WorstBlockingTime(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Worst_Blocking_Time"
		   });		
		addAnnotation
		  (getSimulation_Timing_Result_WorstLocalResponseTime(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Worst_Local_Response_Time"
		   });		
		addAnnotation
		  (slackEClass, 
		   source, 
		   new String[] {
			 "name", "Slack",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getSlack_Value(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Value"
		   });		
		addAnnotation
		  (sporadic_Server_PolicyEClass, 
		   source, 
		   new String[] {
			 "name", "Sporadic_Server_Policy",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getSporadic_Server_Policy_BackgroundPriority(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Background_Priority"
		   });		
		addAnnotation
		  (getSporadic_Server_Policy_InitialCapacity(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Initial_Capacity"
		   });		
		addAnnotation
		  (getSporadic_Server_Policy_MaxPendingReplenishments(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Max_Pending_Replenishments"
		   });		
		addAnnotation
		  (getSporadic_Server_Policy_NormalPriority(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Normal_Priority"
		   });		
		addAnnotation
		  (getSporadic_Server_Policy_Preassigned(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Preassigned"
		   });		
		addAnnotation
		  (getSporadic_Server_Policy_ReplenishmentPeriod(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Replenishment_Period"
		   });		
		addAnnotation
		  (timeEDataType, 
		   source, 
		   new String[] {
			 "name", "Time",
			 "baseType", "http://www.eclipse.org/emf/2003/XMLType#float",
			 "minInclusive", "0.0"
		   });		
		addAnnotation
		  (time_ObjectEDataType, 
		   source, 
		   new String[] {
			 "name", "Time:Object",
			 "baseType", "Time"
		   });		
		addAnnotation
		  (timing_ResultEClass, 
		   source, 
		   new String[] {
			 "name", "Timing_Result",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getTiming_Result_WorstGlobalResponseTimes(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Worst_Global_Response_Times",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getTiming_Result_BestGlobalResponseTimes(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Best_Global_Response_Times",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getTiming_Result_Jitters(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Jitters",
			 "namespace", "##targetNamespace"
		   });		
		addAnnotation
		  (getTiming_Result_BestLocalResponseTime(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Best_Local_Response_Time"
		   });		
		addAnnotation
		  (getTiming_Result_EventName(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Event_Name"
		   });		
		addAnnotation
		  (getTiming_Result_NumOfSuspensions(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Num_Of_Suspensions"
		   });		
		addAnnotation
		  (getTiming_Result_WorstBlockingTime(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Worst_Blocking_Time"
		   });		
		addAnnotation
		  (getTiming_Result_WorstLocalResponseTime(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Worst_Local_Response_Time"
		   });		
		addAnnotation
		  (traceEClass, 
		   source, 
		   new String[] {
			 "name", "Trace",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getTrace_Pathname(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Pathname"
		   });		
		addAnnotation
		  (transaction_ResultsEClass, 
		   source, 
		   new String[] {
			 "name", "Transaction_Results",
			 "kind", "elementOnly"
		   });		
		addAnnotation
		  (getTransaction_Results_Group(), 
		   source, 
		   new String[] {
			 "kind", "group",
			 "name", "group:0"
		   });		
		addAnnotation
		  (getTransaction_Results_Slack(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Slack",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getTransaction_Results_TimingResult(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Timing_Result",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getTransaction_Results_SimulationTimingResult(), 
		   source, 
		   new String[] {
			 "kind", "element",
			 "name", "Simulation_Timing_Result",
			 "namespace", "##targetNamespace",
			 "group", "#group:0"
		   });		
		addAnnotation
		  (getTransaction_Results_Name(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Name"
		   });		
		addAnnotation
		  (utilizationEClass, 
		   source, 
		   new String[] {
			 "name", "Utilization",
			 "kind", "empty"
		   });		
		addAnnotation
		  (getUtilization_Total(), 
		   source, 
		   new String[] {
			 "kind", "attribute",
			 "name", "Total"
		   });
	}

} //ResultPackageImpl
