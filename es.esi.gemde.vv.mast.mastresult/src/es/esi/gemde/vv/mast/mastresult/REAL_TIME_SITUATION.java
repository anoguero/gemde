/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult;

import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>REAL TIME SITUATION</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getGroup <em>Group</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getSystemSlack <em>System Slack</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getTrace <em>Trace</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getTransaction <em>Transaction</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getProcessingResource <em>Processing Resource</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getOperation <em>Operation</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getSchedulingServer <em>Scheduling Server</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getSharedResource <em>Shared Resource</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getGenerationDate <em>Generation Date</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getGenerationProfile <em>Generation Profile</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getGenerationTool <em>Generation Tool</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getModelDate <em>Model Date</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getModelName <em>Model Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getREAL_TIME_SITUATION()
 * @model extendedMetaData="name='REAL_TIME_SITUATION_._type' kind='elementOnly'"
 * @generated
 */
public interface REAL_TIME_SITUATION extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getREAL_TIME_SITUATION_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>System Slack</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastresult.Slack}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>System Slack</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>System Slack</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getREAL_TIME_SITUATION_SystemSlack()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Slack' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Slack> getSystemSlack();

	/**
	 * Returns the value of the '<em><b>Trace</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastresult.Trace}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trace</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trace</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getREAL_TIME_SITUATION_Trace()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Trace' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Trace> getTrace();

	/**
	 * Returns the value of the '<em><b>Transaction</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastresult.Transaction_Results}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transaction</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transaction</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getREAL_TIME_SITUATION_Transaction()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Transaction' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Transaction_Results> getTransaction();

	/**
	 * Returns the value of the '<em><b>Processing Resource</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Processing Resource</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Processing Resource</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getREAL_TIME_SITUATION_ProcessingResource()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Processing_Resource' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Processing_Resource_Results> getProcessingResource();

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastresult.Operation_Results}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getREAL_TIME_SITUATION_Operation()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Operation' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Operation_Results> getOperation();

	/**
	 * Returns the value of the '<em><b>Scheduling Server</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastresult.Scheduling_Server_Results}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scheduling Server</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scheduling Server</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getREAL_TIME_SITUATION_SchedulingServer()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Scheduling_Server' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Scheduling_Server_Results> getSchedulingServer();

	/**
	 * Returns the value of the '<em><b>Shared Resource</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared Resource</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared Resource</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getREAL_TIME_SITUATION_SharedResource()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Shared_Resource' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Shared_Resource_Results> getSharedResource();

	/**
	 * Returns the value of the '<em><b>Generation Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generation Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generation Date</em>' attribute.
	 * @see #setGenerationDate(XMLGregorianCalendar)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getREAL_TIME_SITUATION_GenerationDate()
	 * @model dataType="es.esi.gemde.vv.mast.mastresult.Date_Time" required="true"
	 *        extendedMetaData="kind='attribute' name='Generation_Date'"
	 * @generated
	 */
	XMLGregorianCalendar getGenerationDate();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getGenerationDate <em>Generation Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generation Date</em>' attribute.
	 * @see #getGenerationDate()
	 * @generated
	 */
	void setGenerationDate(XMLGregorianCalendar value);

	/**
	 * Returns the value of the '<em><b>Generation Profile</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generation Profile</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generation Profile</em>' attribute.
	 * @see #setGenerationProfile(String)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getREAL_TIME_SITUATION_GenerationProfile()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String"
	 *        extendedMetaData="kind='attribute' name='Generation_Profile'"
	 * @generated
	 */
	String getGenerationProfile();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getGenerationProfile <em>Generation Profile</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generation Profile</em>' attribute.
	 * @see #getGenerationProfile()
	 * @generated
	 */
	void setGenerationProfile(String value);

	/**
	 * Returns the value of the '<em><b>Generation Tool</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generation Tool</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generation Tool</em>' attribute.
	 * @see #setGenerationTool(String)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getREAL_TIME_SITUATION_GenerationTool()
	 * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
	 *        extendedMetaData="kind='attribute' name='Generation_Tool'"
	 * @generated
	 */
	String getGenerationTool();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getGenerationTool <em>Generation Tool</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Generation Tool</em>' attribute.
	 * @see #getGenerationTool()
	 * @generated
	 */
	void setGenerationTool(String value);

	/**
	 * Returns the value of the '<em><b>Model Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Date</em>' attribute.
	 * @see #setModelDate(XMLGregorianCalendar)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getREAL_TIME_SITUATION_ModelDate()
	 * @model dataType="es.esi.gemde.vv.mast.mastresult.Date_Time" required="true"
	 *        extendedMetaData="kind='attribute' name='Model_Date'"
	 * @generated
	 */
	XMLGregorianCalendar getModelDate();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getModelDate <em>Model Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Date</em>' attribute.
	 * @see #getModelDate()
	 * @generated
	 */
	void setModelDate(XMLGregorianCalendar value);

	/**
	 * Returns the value of the '<em><b>Model Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Name</em>' attribute.
	 * @see #setModelName(String)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getREAL_TIME_SITUATION_ModelName()
	 * @model id="true" dataType="es.esi.gemde.vv.mast.mastresult.Identifier" required="true"
	 *        extendedMetaData="kind='attribute' name='Model_Name'"
	 * @generated
	 */
	String getModelName();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.REAL_TIME_SITUATION#getModelName <em>Model Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Name</em>' attribute.
	 * @see #getModelName()
	 * @generated
	 */
	void setModelName(String value);

} // REAL_TIME_SITUATION
