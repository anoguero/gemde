/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult.validation;

import java.math.BigInteger;

/**
 * A sample validator interface for {@link es.esi.gemde.vv.mast.mastresult.Queue_Size}.
 * This doesn't really do anything, and it's not a real EMF artifact.
 * It was generated by the org.eclipse.emf.examples.generator.validator plug-in to illustrate how EMF's code generator can be extended.
 * This can be disabled with -vmargs -Dorg.eclipse.emf.examples.generator.validator=false.
 */
public interface Queue_SizeValidator {
	boolean validate();

	boolean validateMaxNum(BigInteger value);
}
