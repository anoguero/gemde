/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Polling Policy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingAvgOverhead <em>Polling Avg Overhead</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingBestOverhead <em>Polling Best Overhead</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingPeriod <em>Polling Period</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingWorstOverhead <em>Polling Worst Overhead</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPreassigned <em>Preassigned</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getThePriority <em>The Priority</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getPolling_Policy()
 * @model extendedMetaData="name='Polling_Policy' kind='empty'"
 * @generated
 */
public interface Polling_Policy extends EObject {
	/**
	 * Returns the value of the '<em><b>Polling Avg Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Polling Avg Overhead</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Polling Avg Overhead</em>' attribute.
	 * @see #isSetPollingAvgOverhead()
	 * @see #unsetPollingAvgOverhead()
	 * @see #setPollingAvgOverhead(float)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getPolling_Policy_PollingAvgOverhead()
	 * @model unsettable="true" dataType="es.esi.gemde.vv.mast.mastresult.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Polling_Avg_Overhead'"
	 * @generated
	 */
	float getPollingAvgOverhead();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingAvgOverhead <em>Polling Avg Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Polling Avg Overhead</em>' attribute.
	 * @see #isSetPollingAvgOverhead()
	 * @see #unsetPollingAvgOverhead()
	 * @see #getPollingAvgOverhead()
	 * @generated
	 */
	void setPollingAvgOverhead(float value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingAvgOverhead <em>Polling Avg Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPollingAvgOverhead()
	 * @see #getPollingAvgOverhead()
	 * @see #setPollingAvgOverhead(float)
	 * @generated
	 */
	void unsetPollingAvgOverhead();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingAvgOverhead <em>Polling Avg Overhead</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Polling Avg Overhead</em>' attribute is set.
	 * @see #unsetPollingAvgOverhead()
	 * @see #getPollingAvgOverhead()
	 * @see #setPollingAvgOverhead(float)
	 * @generated
	 */
	boolean isSetPollingAvgOverhead();

	/**
	 * Returns the value of the '<em><b>Polling Best Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Polling Best Overhead</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Polling Best Overhead</em>' attribute.
	 * @see #isSetPollingBestOverhead()
	 * @see #unsetPollingBestOverhead()
	 * @see #setPollingBestOverhead(float)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getPolling_Policy_PollingBestOverhead()
	 * @model unsettable="true" dataType="es.esi.gemde.vv.mast.mastresult.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Polling_Best_Overhead'"
	 * @generated
	 */
	float getPollingBestOverhead();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingBestOverhead <em>Polling Best Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Polling Best Overhead</em>' attribute.
	 * @see #isSetPollingBestOverhead()
	 * @see #unsetPollingBestOverhead()
	 * @see #getPollingBestOverhead()
	 * @generated
	 */
	void setPollingBestOverhead(float value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingBestOverhead <em>Polling Best Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPollingBestOverhead()
	 * @see #getPollingBestOverhead()
	 * @see #setPollingBestOverhead(float)
	 * @generated
	 */
	void unsetPollingBestOverhead();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingBestOverhead <em>Polling Best Overhead</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Polling Best Overhead</em>' attribute is set.
	 * @see #unsetPollingBestOverhead()
	 * @see #getPollingBestOverhead()
	 * @see #setPollingBestOverhead(float)
	 * @generated
	 */
	boolean isSetPollingBestOverhead();

	/**
	 * Returns the value of the '<em><b>Polling Period</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Polling Period</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Polling Period</em>' attribute.
	 * @see #isSetPollingPeriod()
	 * @see #unsetPollingPeriod()
	 * @see #setPollingPeriod(float)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getPolling_Policy_PollingPeriod()
	 * @model unsettable="true" dataType="es.esi.gemde.vv.mast.mastresult.Time"
	 *        extendedMetaData="kind='attribute' name='Polling_Period'"
	 * @generated
	 */
	float getPollingPeriod();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingPeriod <em>Polling Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Polling Period</em>' attribute.
	 * @see #isSetPollingPeriod()
	 * @see #unsetPollingPeriod()
	 * @see #getPollingPeriod()
	 * @generated
	 */
	void setPollingPeriod(float value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingPeriod <em>Polling Period</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPollingPeriod()
	 * @see #getPollingPeriod()
	 * @see #setPollingPeriod(float)
	 * @generated
	 */
	void unsetPollingPeriod();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingPeriod <em>Polling Period</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Polling Period</em>' attribute is set.
	 * @see #unsetPollingPeriod()
	 * @see #getPollingPeriod()
	 * @see #setPollingPeriod(float)
	 * @generated
	 */
	boolean isSetPollingPeriod();

	/**
	 * Returns the value of the '<em><b>Polling Worst Overhead</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Polling Worst Overhead</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Polling Worst Overhead</em>' attribute.
	 * @see #isSetPollingWorstOverhead()
	 * @see #unsetPollingWorstOverhead()
	 * @see #setPollingWorstOverhead(float)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getPolling_Policy_PollingWorstOverhead()
	 * @model unsettable="true" dataType="es.esi.gemde.vv.mast.mastresult.Normalized_Execution_Time"
	 *        extendedMetaData="kind='attribute' name='Polling_Worst_Overhead'"
	 * @generated
	 */
	float getPollingWorstOverhead();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingWorstOverhead <em>Polling Worst Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Polling Worst Overhead</em>' attribute.
	 * @see #isSetPollingWorstOverhead()
	 * @see #unsetPollingWorstOverhead()
	 * @see #getPollingWorstOverhead()
	 * @generated
	 */
	void setPollingWorstOverhead(float value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingWorstOverhead <em>Polling Worst Overhead</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPollingWorstOverhead()
	 * @see #getPollingWorstOverhead()
	 * @see #setPollingWorstOverhead(float)
	 * @generated
	 */
	void unsetPollingWorstOverhead();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPollingWorstOverhead <em>Polling Worst Overhead</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Polling Worst Overhead</em>' attribute is set.
	 * @see #unsetPollingWorstOverhead()
	 * @see #getPollingWorstOverhead()
	 * @see #setPollingWorstOverhead(float)
	 * @generated
	 */
	boolean isSetPollingWorstOverhead();

	/**
	 * Returns the value of the '<em><b>Preassigned</b></em>' attribute.
	 * The literals are from the enumeration {@link es.esi.gemde.vv.mast.mastresult.Assertion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preassigned</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preassigned</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastresult.Assertion
	 * @see #isSetPreassigned()
	 * @see #unsetPreassigned()
	 * @see #setPreassigned(Assertion)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getPolling_Policy_Preassigned()
	 * @model unsettable="true"
	 *        extendedMetaData="kind='attribute' name='Preassigned'"
	 * @generated
	 */
	Assertion getPreassigned();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPreassigned <em>Preassigned</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Preassigned</em>' attribute.
	 * @see es.esi.gemde.vv.mast.mastresult.Assertion
	 * @see #isSetPreassigned()
	 * @see #unsetPreassigned()
	 * @see #getPreassigned()
	 * @generated
	 */
	void setPreassigned(Assertion value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPreassigned <em>Preassigned</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPreassigned()
	 * @see #getPreassigned()
	 * @see #setPreassigned(Assertion)
	 * @generated
	 */
	void unsetPreassigned();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getPreassigned <em>Preassigned</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Preassigned</em>' attribute is set.
	 * @see #unsetPreassigned()
	 * @see #getPreassigned()
	 * @see #setPreassigned(Assertion)
	 * @generated
	 */
	boolean isSetPreassigned();

	/**
	 * Returns the value of the '<em><b>The Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>The Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>The Priority</em>' attribute.
	 * @see #isSetThePriority()
	 * @see #unsetThePriority()
	 * @see #setThePriority(int)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getPolling_Policy_ThePriority()
	 * @model unsettable="true" dataType="es.esi.gemde.vv.mast.mastresult.Priority"
	 *        extendedMetaData="kind='attribute' name='The_Priority'"
	 * @generated
	 */
	int getThePriority();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getThePriority <em>The Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>The Priority</em>' attribute.
	 * @see #isSetThePriority()
	 * @see #unsetThePriority()
	 * @see #getThePriority()
	 * @generated
	 */
	void setThePriority(int value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getThePriority <em>The Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetThePriority()
	 * @see #getThePriority()
	 * @see #setThePriority(int)
	 * @generated
	 */
	void unsetThePriority();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastresult.Polling_Policy#getThePriority <em>The Priority</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>The Priority</em>' attribute is set.
	 * @see #unsetThePriority()
	 * @see #getThePriority()
	 * @see #setThePriority(int)
	 * @generated
	 */
	boolean isSetThePriority();

} // Polling_Policy
