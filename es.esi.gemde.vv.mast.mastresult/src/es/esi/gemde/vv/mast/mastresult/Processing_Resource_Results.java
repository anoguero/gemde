/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.FeatureMap;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Processing Resource Results</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results#getGroup <em>Group</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results#getSlack <em>Slack</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results#getDetailedUtilization <em>Detailed Utilization</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results#getUtilization <em>Utilization</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results#getReadyQueueSize <em>Ready Queue Size</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getProcessing_Resource_Results()
 * @model extendedMetaData="name='Processing_Resource_Results' kind='elementOnly'"
 * @generated
 */
public interface Processing_Resource_Results extends EObject {
	/**
	 * Returns the value of the '<em><b>Group</b></em>' attribute list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.util.FeatureMap.Entry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Group</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Group</em>' attribute list.
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getProcessing_Resource_Results_Group()
	 * @model unique="false" dataType="org.eclipse.emf.ecore.EFeatureMapEntry" many="true"
	 *        extendedMetaData="kind='group' name='group:0'"
	 * @generated
	 */
	FeatureMap getGroup();

	/**
	 * Returns the value of the '<em><b>Slack</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastresult.Slack}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Slack</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Slack</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getProcessing_Resource_Results_Slack()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Slack' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Slack> getSlack();

	/**
	 * Returns the value of the '<em><b>Detailed Utilization</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastresult.Detailed_Utilization}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Detailed Utilization</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Detailed Utilization</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getProcessing_Resource_Results_DetailedUtilization()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Detailed_Utilization' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Detailed_Utilization> getDetailedUtilization();

	/**
	 * Returns the value of the '<em><b>Utilization</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastresult.Utilization}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Utilization</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Utilization</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getProcessing_Resource_Results_Utilization()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Utilization' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Utilization> getUtilization();

	/**
	 * Returns the value of the '<em><b>Ready Queue Size</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastresult.Ready_Queue_Size}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ready Queue Size</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ready Queue Size</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getProcessing_Resource_Results_ReadyQueueSize()
	 * @model containment="true" transient="true" volatile="true" derived="true"
	 *        extendedMetaData="kind='element' name='Ready_Queue_Size' namespace='##targetNamespace' group='#group:0'"
	 * @generated
	 */
	EList<Ready_Queue_Size> getReadyQueueSize();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getProcessing_Resource_Results_Name()
	 * @model id="true" dataType="es.esi.gemde.vv.mast.mastresult.Identifier" required="true"
	 *        extendedMetaData="kind='attribute' name='Name'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Processing_Resource_Results
