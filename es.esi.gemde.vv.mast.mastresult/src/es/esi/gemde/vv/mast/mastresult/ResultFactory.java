/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage
 * @generated
 */
public interface ResultFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ResultFactory eINSTANCE = es.esi.gemde.vv.mast.mastresult.impl.ResultFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Detailed Utilization</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Detailed Utilization</em>'.
	 * @generated
	 */
	Detailed_Utilization createDetailed_Utilization();

	/**
	 * Returns a new object of class '<em>Document Root</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Document Root</em>'.
	 * @generated
	 */
	Document_Root createDocument_Root();

	/**
	 * Returns a new object of class '<em>Fixed Priority Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Fixed Priority Policy</em>'.
	 * @generated
	 */
	Fixed_Priority_Policy createFixed_Priority_Policy();

	/**
	 * Returns a new object of class '<em>Global Miss Ratio</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Global Miss Ratio</em>'.
	 * @generated
	 */
	Global_Miss_Ratio createGlobal_Miss_Ratio();

	/**
	 * Returns a new object of class '<em>Global Miss Ratio List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Global Miss Ratio List</em>'.
	 * @generated
	 */
	Global_Miss_Ratio_List createGlobal_Miss_Ratio_List();

	/**
	 * Returns a new object of class '<em>Global Response Time</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Global Response Time</em>'.
	 * @generated
	 */
	Global_Response_Time createGlobal_Response_Time();

	/**
	 * Returns a new object of class '<em>Global Response Time List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Global Response Time List</em>'.
	 * @generated
	 */
	Global_Response_Time_List createGlobal_Response_Time_List();

	/**
	 * Returns a new object of class '<em>Interrupt FP Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Interrupt FP Policy</em>'.
	 * @generated
	 */
	Interrupt_FP_Policy createInterrupt_FP_Policy();

	/**
	 * Returns a new object of class '<em>Miss Ratio</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Miss Ratio</em>'.
	 * @generated
	 */
	Miss_Ratio createMiss_Ratio();

	/**
	 * Returns a new object of class '<em>Miss Ratio List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Miss Ratio List</em>'.
	 * @generated
	 */
	Miss_Ratio_List createMiss_Ratio_List();

	/**
	 * Returns a new object of class '<em>Non Preemptible FP Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Non Preemptible FP Policy</em>'.
	 * @generated
	 */
	Non_Preemptible_FP_Policy createNon_Preemptible_FP_Policy();

	/**
	 * Returns a new object of class '<em>Operation Results</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operation Results</em>'.
	 * @generated
	 */
	Operation_Results createOperation_Results();

	/**
	 * Returns a new object of class '<em>Polling Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Polling Policy</em>'.
	 * @generated
	 */
	Polling_Policy createPolling_Policy();

	/**
	 * Returns a new object of class '<em>Priority Ceiling</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Priority Ceiling</em>'.
	 * @generated
	 */
	Priority_Ceiling createPriority_Ceiling();

	/**
	 * Returns a new object of class '<em>Processing Resource Results</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Processing Resource Results</em>'.
	 * @generated
	 */
	Processing_Resource_Results createProcessing_Resource_Results();

	/**
	 * Returns a new object of class '<em>Queue Size</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Queue Size</em>'.
	 * @generated
	 */
	Queue_Size createQueue_Size();

	/**
	 * Returns a new object of class '<em>Ready Queue Size</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ready Queue Size</em>'.
	 * @generated
	 */
	Ready_Queue_Size createReady_Queue_Size();

	/**
	 * Returns a new object of class '<em>REAL TIME SITUATION</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>REAL TIME SITUATION</em>'.
	 * @generated
	 */
	REAL_TIME_SITUATION createREAL_TIME_SITUATION();

	/**
	 * Returns a new object of class '<em>Scheduling Server Results</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Scheduling Server Results</em>'.
	 * @generated
	 */
	Scheduling_Server_Results createScheduling_Server_Results();

	/**
	 * Returns a new object of class '<em>Shared Resource Results</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Shared Resource Results</em>'.
	 * @generated
	 */
	Shared_Resource_Results createShared_Resource_Results();

	/**
	 * Returns a new object of class '<em>Simulation Timing Result</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Simulation Timing Result</em>'.
	 * @generated
	 */
	Simulation_Timing_Result createSimulation_Timing_Result();

	/**
	 * Returns a new object of class '<em>Slack</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Slack</em>'.
	 * @generated
	 */
	Slack createSlack();

	/**
	 * Returns a new object of class '<em>Sporadic Server Policy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sporadic Server Policy</em>'.
	 * @generated
	 */
	Sporadic_Server_Policy createSporadic_Server_Policy();

	/**
	 * Returns a new object of class '<em>Timing Result</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Timing Result</em>'.
	 * @generated
	 */
	Timing_Result createTiming_Result();

	/**
	 * Returns a new object of class '<em>Trace</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trace</em>'.
	 * @generated
	 */
	Trace createTrace();

	/**
	 * Returns a new object of class '<em>Transaction Results</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Transaction Results</em>'.
	 * @generated
	 */
	Transaction_Results createTransaction_Results();

	/**
	 * Returns a new object of class '<em>Utilization</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Utilization</em>'.
	 * @generated
	 */
	Utilization createUtilization();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ResultPackage getResultPackage();

} //ResultFactory
