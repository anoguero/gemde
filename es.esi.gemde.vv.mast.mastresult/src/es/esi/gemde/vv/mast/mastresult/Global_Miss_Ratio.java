/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Global Miss Ratio</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio#getMissRatios <em>Miss Ratios</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio#getReferencedEvent <em>Referenced Event</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getGlobal_Miss_Ratio()
 * @model extendedMetaData="name='Global_Miss_Ratio' kind='elementOnly'"
 * @generated
 */
public interface Global_Miss_Ratio extends EObject {
	/**
	 * Returns the value of the '<em><b>Miss Ratios</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Miss Ratios</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Miss Ratios</em>' containment reference.
	 * @see #setMissRatios(Miss_Ratio_List)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getGlobal_Miss_Ratio_MissRatios()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Miss_Ratios' namespace='##targetNamespace'"
	 * @generated
	 */
	Miss_Ratio_List getMissRatios();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio#getMissRatios <em>Miss Ratios</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Miss Ratios</em>' containment reference.
	 * @see #getMissRatios()
	 * @generated
	 */
	void setMissRatios(Miss_Ratio_List value);

	/**
	 * Returns the value of the '<em><b>Referenced Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced Event</em>' attribute.
	 * @see #setReferencedEvent(String)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getGlobal_Miss_Ratio_ReferencedEvent()
	 * @model dataType="es.esi.gemde.vv.mast.mastresult.External_Reference"
	 *        extendedMetaData="kind='attribute' name='Referenced_Event'"
	 * @generated
	 */
	String getReferencedEvent();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Global_Miss_Ratio#getReferencedEvent <em>Referenced Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Referenced Event</em>' attribute.
	 * @see #getReferencedEvent()
	 * @generated
	 */
	void setReferencedEvent(String value);

} // Global_Miss_Ratio
