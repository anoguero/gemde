/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult.impl;

import es.esi.gemde.vv.mast.mastresult.*;

import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.xml.type.XMLTypeFactory;
import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ResultFactoryImpl extends EFactoryImpl implements ResultFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ResultFactory init() {
		try {
			ResultFactory theResultFactory = (ResultFactory)EPackage.Registry.INSTANCE.getEFactory("http://mast.unican.es/xmlmast/result"); 
			if (theResultFactory != null) {
				return theResultFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ResultFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResultFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ResultPackage.DETAILED_UTILIZATION: return createDetailed_Utilization();
			case ResultPackage.DOCUMENT_ROOT: return createDocument_Root();
			case ResultPackage.FIXED_PRIORITY_POLICY: return createFixed_Priority_Policy();
			case ResultPackage.GLOBAL_MISS_RATIO: return createGlobal_Miss_Ratio();
			case ResultPackage.GLOBAL_MISS_RATIO_LIST: return createGlobal_Miss_Ratio_List();
			case ResultPackage.GLOBAL_RESPONSE_TIME: return createGlobal_Response_Time();
			case ResultPackage.GLOBAL_RESPONSE_TIME_LIST: return createGlobal_Response_Time_List();
			case ResultPackage.INTERRUPT_FP_POLICY: return createInterrupt_FP_Policy();
			case ResultPackage.MISS_RATIO: return createMiss_Ratio();
			case ResultPackage.MISS_RATIO_LIST: return createMiss_Ratio_List();
			case ResultPackage.NON_PREEMPTIBLE_FP_POLICY: return createNon_Preemptible_FP_Policy();
			case ResultPackage.OPERATION_RESULTS: return createOperation_Results();
			case ResultPackage.POLLING_POLICY: return createPolling_Policy();
			case ResultPackage.PRIORITY_CEILING: return createPriority_Ceiling();
			case ResultPackage.PROCESSING_RESOURCE_RESULTS: return createProcessing_Resource_Results();
			case ResultPackage.QUEUE_SIZE: return createQueue_Size();
			case ResultPackage.READY_QUEUE_SIZE: return createReady_Queue_Size();
			case ResultPackage.REAL_TIME_SITUATION: return createREAL_TIME_SITUATION();
			case ResultPackage.SCHEDULING_SERVER_RESULTS: return createScheduling_Server_Results();
			case ResultPackage.SHARED_RESOURCE_RESULTS: return createShared_Resource_Results();
			case ResultPackage.SIMULATION_TIMING_RESULT: return createSimulation_Timing_Result();
			case ResultPackage.SLACK: return createSlack();
			case ResultPackage.SPORADIC_SERVER_POLICY: return createSporadic_Server_Policy();
			case ResultPackage.TIMING_RESULT: return createTiming_Result();
			case ResultPackage.TRACE: return createTrace();
			case ResultPackage.TRANSACTION_RESULTS: return createTransaction_Results();
			case ResultPackage.UTILIZATION: return createUtilization();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ResultPackage.AFFIRMATIVE_ASSERTION:
				return createAffirmative_AssertionFromString(eDataType, initialValue);
			case ResultPackage.ASSERTION:
				return createAssertionFromString(eDataType, initialValue);
			case ResultPackage.AFFIRMATIVE_ASSERTION_OBJECT:
				return createAffirmative_Assertion_ObjectFromString(eDataType, initialValue);
			case ResultPackage.ASSERTION_OBJECT:
				return createAssertion_ObjectFromString(eDataType, initialValue);
			case ResultPackage.DATE_TIME:
				return createDate_TimeFromString(eDataType, initialValue);
			case ResultPackage.EXTERNAL_REFERENCE:
				return createExternal_ReferenceFromString(eDataType, initialValue);
			case ResultPackage.FACTOR:
				return createFactorFromString(eDataType, initialValue);
			case ResultPackage.FACTOR_OBJECT:
				return createFactor_ObjectFromString(eDataType, initialValue);
			case ResultPackage.IDENTIFIER:
				return createIdentifierFromString(eDataType, initialValue);
			case ResultPackage.NORMALIZED_EXECUTION_TIME:
				return createNormalized_Execution_TimeFromString(eDataType, initialValue);
			case ResultPackage.NORMALIZED_EXECUTION_TIME_OBJECT:
				return createNormalized_Execution_Time_ObjectFromString(eDataType, initialValue);
			case ResultPackage.PERCENTAGE:
				return createPercentageFromString(eDataType, initialValue);
			case ResultPackage.PERCENTAGE_OBJECT:
				return createPercentage_ObjectFromString(eDataType, initialValue);
			case ResultPackage.PRIORITY:
				return createPriorityFromString(eDataType, initialValue);
			case ResultPackage.PRIORITY_OBJECT:
				return createPriority_ObjectFromString(eDataType, initialValue);
			case ResultPackage.TIME:
				return createTimeFromString(eDataType, initialValue);
			case ResultPackage.TIME_OBJECT:
				return createTime_ObjectFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ResultPackage.AFFIRMATIVE_ASSERTION:
				return convertAffirmative_AssertionToString(eDataType, instanceValue);
			case ResultPackage.ASSERTION:
				return convertAssertionToString(eDataType, instanceValue);
			case ResultPackage.AFFIRMATIVE_ASSERTION_OBJECT:
				return convertAffirmative_Assertion_ObjectToString(eDataType, instanceValue);
			case ResultPackage.ASSERTION_OBJECT:
				return convertAssertion_ObjectToString(eDataType, instanceValue);
			case ResultPackage.DATE_TIME:
				return convertDate_TimeToString(eDataType, instanceValue);
			case ResultPackage.EXTERNAL_REFERENCE:
				return convertExternal_ReferenceToString(eDataType, instanceValue);
			case ResultPackage.FACTOR:
				return convertFactorToString(eDataType, instanceValue);
			case ResultPackage.FACTOR_OBJECT:
				return convertFactor_ObjectToString(eDataType, instanceValue);
			case ResultPackage.IDENTIFIER:
				return convertIdentifierToString(eDataType, instanceValue);
			case ResultPackage.NORMALIZED_EXECUTION_TIME:
				return convertNormalized_Execution_TimeToString(eDataType, instanceValue);
			case ResultPackage.NORMALIZED_EXECUTION_TIME_OBJECT:
				return convertNormalized_Execution_Time_ObjectToString(eDataType, instanceValue);
			case ResultPackage.PERCENTAGE:
				return convertPercentageToString(eDataType, instanceValue);
			case ResultPackage.PERCENTAGE_OBJECT:
				return convertPercentage_ObjectToString(eDataType, instanceValue);
			case ResultPackage.PRIORITY:
				return convertPriorityToString(eDataType, instanceValue);
			case ResultPackage.PRIORITY_OBJECT:
				return convertPriority_ObjectToString(eDataType, instanceValue);
			case ResultPackage.TIME:
				return convertTimeToString(eDataType, instanceValue);
			case ResultPackage.TIME_OBJECT:
				return convertTime_ObjectToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Detailed_Utilization createDetailed_Utilization() {
		Detailed_UtilizationImpl detailed_Utilization = new Detailed_UtilizationImpl();
		return detailed_Utilization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Document_Root createDocument_Root() {
		Document_RootImpl document_Root = new Document_RootImpl();
		return document_Root;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Fixed_Priority_Policy createFixed_Priority_Policy() {
		Fixed_Priority_PolicyImpl fixed_Priority_Policy = new Fixed_Priority_PolicyImpl();
		return fixed_Priority_Policy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Global_Miss_Ratio createGlobal_Miss_Ratio() {
		Global_Miss_RatioImpl global_Miss_Ratio = new Global_Miss_RatioImpl();
		return global_Miss_Ratio;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Global_Miss_Ratio_List createGlobal_Miss_Ratio_List() {
		Global_Miss_Ratio_ListImpl global_Miss_Ratio_List = new Global_Miss_Ratio_ListImpl();
		return global_Miss_Ratio_List;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Global_Response_Time createGlobal_Response_Time() {
		Global_Response_TimeImpl global_Response_Time = new Global_Response_TimeImpl();
		return global_Response_Time;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Global_Response_Time_List createGlobal_Response_Time_List() {
		Global_Response_Time_ListImpl global_Response_Time_List = new Global_Response_Time_ListImpl();
		return global_Response_Time_List;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interrupt_FP_Policy createInterrupt_FP_Policy() {
		Interrupt_FP_PolicyImpl interrupt_FP_Policy = new Interrupt_FP_PolicyImpl();
		return interrupt_FP_Policy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Miss_Ratio createMiss_Ratio() {
		Miss_RatioImpl miss_Ratio = new Miss_RatioImpl();
		return miss_Ratio;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Miss_Ratio_List createMiss_Ratio_List() {
		Miss_Ratio_ListImpl miss_Ratio_List = new Miss_Ratio_ListImpl();
		return miss_Ratio_List;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Non_Preemptible_FP_Policy createNon_Preemptible_FP_Policy() {
		Non_Preemptible_FP_PolicyImpl non_Preemptible_FP_Policy = new Non_Preemptible_FP_PolicyImpl();
		return non_Preemptible_FP_Policy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation_Results createOperation_Results() {
		Operation_ResultsImpl operation_Results = new Operation_ResultsImpl();
		return operation_Results;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Polling_Policy createPolling_Policy() {
		Polling_PolicyImpl polling_Policy = new Polling_PolicyImpl();
		return polling_Policy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Priority_Ceiling createPriority_Ceiling() {
		Priority_CeilingImpl priority_Ceiling = new Priority_CeilingImpl();
		return priority_Ceiling;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Processing_Resource_Results createProcessing_Resource_Results() {
		Processing_Resource_ResultsImpl processing_Resource_Results = new Processing_Resource_ResultsImpl();
		return processing_Resource_Results;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Queue_Size createQueue_Size() {
		Queue_SizeImpl queue_Size = new Queue_SizeImpl();
		return queue_Size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ready_Queue_Size createReady_Queue_Size() {
		Ready_Queue_SizeImpl ready_Queue_Size = new Ready_Queue_SizeImpl();
		return ready_Queue_Size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public REAL_TIME_SITUATION createREAL_TIME_SITUATION() {
		REAL_TIME_SITUATIONImpl reaL_TIME_SITUATION = new REAL_TIME_SITUATIONImpl();
		return reaL_TIME_SITUATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Scheduling_Server_Results createScheduling_Server_Results() {
		Scheduling_Server_ResultsImpl scheduling_Server_Results = new Scheduling_Server_ResultsImpl();
		return scheduling_Server_Results;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Shared_Resource_Results createShared_Resource_Results() {
		Shared_Resource_ResultsImpl shared_Resource_Results = new Shared_Resource_ResultsImpl();
		return shared_Resource_Results;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Simulation_Timing_Result createSimulation_Timing_Result() {
		Simulation_Timing_ResultImpl simulation_Timing_Result = new Simulation_Timing_ResultImpl();
		return simulation_Timing_Result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Slack createSlack() {
		SlackImpl slack = new SlackImpl();
		return slack;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Sporadic_Server_Policy createSporadic_Server_Policy() {
		Sporadic_Server_PolicyImpl sporadic_Server_Policy = new Sporadic_Server_PolicyImpl();
		return sporadic_Server_Policy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Timing_Result createTiming_Result() {
		Timing_ResultImpl timing_Result = new Timing_ResultImpl();
		return timing_Result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Trace createTrace() {
		TraceImpl trace = new TraceImpl();
		return trace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Transaction_Results createTransaction_Results() {
		Transaction_ResultsImpl transaction_Results = new Transaction_ResultsImpl();
		return transaction_Results;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Utilization createUtilization() {
		UtilizationImpl utilization = new UtilizationImpl();
		return utilization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Affirmative_Assertion createAffirmative_AssertionFromString(EDataType eDataType, String initialValue) {
		Affirmative_Assertion result = Affirmative_Assertion.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAffirmative_AssertionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Assertion createAssertionFromString(EDataType eDataType, String initialValue) {
		Assertion result = Assertion.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAssertionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Affirmative_Assertion createAffirmative_Assertion_ObjectFromString(EDataType eDataType, String initialValue) {
		return createAffirmative_AssertionFromString(ResultPackage.Literals.AFFIRMATIVE_ASSERTION, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAffirmative_Assertion_ObjectToString(EDataType eDataType, Object instanceValue) {
		return convertAffirmative_AssertionToString(ResultPackage.Literals.AFFIRMATIVE_ASSERTION, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Assertion createAssertion_ObjectFromString(EDataType eDataType, String initialValue) {
		return createAssertionFromString(ResultPackage.Literals.ASSERTION, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAssertion_ObjectToString(EDataType eDataType, Object instanceValue) {
		return convertAssertionToString(ResultPackage.Literals.ASSERTION, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XMLGregorianCalendar createDate_TimeFromString(EDataType eDataType, String initialValue) {
		return (XMLGregorianCalendar)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.DATE_TIME, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDate_TimeToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.DATE_TIME, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createExternal_ReferenceFromString(EDataType eDataType, String initialValue) {
		return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.NC_NAME, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertExternal_ReferenceToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.NC_NAME, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Float createFactorFromString(EDataType eDataType, String initialValue) {
		return (Float)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.FLOAT, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFactorToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.FLOAT, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Float createFactor_ObjectFromString(EDataType eDataType, String initialValue) {
		return createFactorFromString(ResultPackage.Literals.FACTOR, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFactor_ObjectToString(EDataType eDataType, Object instanceValue) {
		return convertFactorToString(ResultPackage.Literals.FACTOR, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String createIdentifierFromString(EDataType eDataType, String initialValue) {
		return (String)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.ID, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIdentifierToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.ID, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Float createNormalized_Execution_TimeFromString(EDataType eDataType, String initialValue) {
		return (Float)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.FLOAT, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertNormalized_Execution_TimeToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.FLOAT, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Float createNormalized_Execution_Time_ObjectFromString(EDataType eDataType, String initialValue) {
		return createNormalized_Execution_TimeFromString(ResultPackage.Literals.NORMALIZED_EXECUTION_TIME, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertNormalized_Execution_Time_ObjectToString(EDataType eDataType, Object instanceValue) {
		return convertNormalized_Execution_TimeToString(ResultPackage.Literals.NORMALIZED_EXECUTION_TIME, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Float createPercentageFromString(EDataType eDataType, String initialValue) {
		return (Float)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.FLOAT, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPercentageToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.FLOAT, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Float createPercentage_ObjectFromString(EDataType eDataType, String initialValue) {
		return createPercentageFromString(ResultPackage.Literals.PERCENTAGE, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPercentage_ObjectToString(EDataType eDataType, Object instanceValue) {
		return convertPercentageToString(ResultPackage.Literals.PERCENTAGE, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer createPriorityFromString(EDataType eDataType, String initialValue) {
		return (Integer)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.INT, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPriorityToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.INT, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer createPriority_ObjectFromString(EDataType eDataType, String initialValue) {
		return createPriorityFromString(ResultPackage.Literals.PRIORITY, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPriority_ObjectToString(EDataType eDataType, Object instanceValue) {
		return convertPriorityToString(ResultPackage.Literals.PRIORITY, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Float createTimeFromString(EDataType eDataType, String initialValue) {
		return (Float)XMLTypeFactory.eINSTANCE.createFromString(XMLTypePackage.Literals.FLOAT, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTimeToString(EDataType eDataType, Object instanceValue) {
		return XMLTypeFactory.eINSTANCE.convertToString(XMLTypePackage.Literals.FLOAT, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Float createTime_ObjectFromString(EDataType eDataType, String initialValue) {
		return createTimeFromString(ResultPackage.Literals.TIME, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTime_ObjectToString(EDataType eDataType, Object instanceValue) {
		return convertTimeToString(ResultPackage.Literals.TIME, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResultPackage getResultPackage() {
		return (ResultPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ResultPackage getPackage() {
		return ResultPackage.eINSTANCE;
	}

} //ResultFactoryImpl
