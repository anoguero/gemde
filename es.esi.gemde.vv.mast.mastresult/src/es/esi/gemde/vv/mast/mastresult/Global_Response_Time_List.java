/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Global Response Time List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Global_Response_Time_List#getGlobalResponseTime <em>Global Response Time</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getGlobal_Response_Time_List()
 * @model extendedMetaData="name='Global_Response_Time_List' kind='elementOnly'"
 * @generated
 */
public interface Global_Response_Time_List extends EObject {
	/**
	 * Returns the value of the '<em><b>Global Response Time</b></em>' containment reference list.
	 * The list contents are of type {@link es.esi.gemde.vv.mast.mastresult.Global_Response_Time}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Global Response Time</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Global Response Time</em>' containment reference list.
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getGlobal_Response_Time_List_GlobalResponseTime()
	 * @model containment="true" required="true"
	 *        extendedMetaData="kind='element' name='Global_Response_Time' namespace='##targetNamespace'"
	 * @generated
	 */
	EList<Global_Response_Time> getGlobalResponseTime();

} // Global_Response_Time_List
