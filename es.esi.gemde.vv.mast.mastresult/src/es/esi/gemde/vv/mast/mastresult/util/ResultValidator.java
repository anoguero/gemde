/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult.util;

import es.esi.gemde.vv.mast.mastresult.*;

import java.util.Map;

import javax.xml.datatype.XMLGregorianCalendar;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import org.eclipse.emf.ecore.xml.type.util.XMLTypeUtil;
import org.eclipse.emf.ecore.xml.type.util.XMLTypeValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage
 * @generated
 */
public class ResultValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final ResultValidator INSTANCE = new ResultValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "es.esi.gemde.vv.mast.mastresult";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * The cached base package validator.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XMLTypeValidator xmlTypeValidator;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResultValidator() {
		super();
		xmlTypeValidator = XMLTypeValidator.INSTANCE;
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return ResultPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case ResultPackage.DETAILED_UTILIZATION:
				return validateDetailed_Utilization((Detailed_Utilization)value, diagnostics, context);
			case ResultPackage.DOCUMENT_ROOT:
				return validateDocument_Root((Document_Root)value, diagnostics, context);
			case ResultPackage.FIXED_PRIORITY_POLICY:
				return validateFixed_Priority_Policy((Fixed_Priority_Policy)value, diagnostics, context);
			case ResultPackage.GLOBAL_MISS_RATIO:
				return validateGlobal_Miss_Ratio((Global_Miss_Ratio)value, diagnostics, context);
			case ResultPackage.GLOBAL_MISS_RATIO_LIST:
				return validateGlobal_Miss_Ratio_List((Global_Miss_Ratio_List)value, diagnostics, context);
			case ResultPackage.GLOBAL_RESPONSE_TIME:
				return validateGlobal_Response_Time((Global_Response_Time)value, diagnostics, context);
			case ResultPackage.GLOBAL_RESPONSE_TIME_LIST:
				return validateGlobal_Response_Time_List((Global_Response_Time_List)value, diagnostics, context);
			case ResultPackage.INTERRUPT_FP_POLICY:
				return validateInterrupt_FP_Policy((Interrupt_FP_Policy)value, diagnostics, context);
			case ResultPackage.MISS_RATIO:
				return validateMiss_Ratio((Miss_Ratio)value, diagnostics, context);
			case ResultPackage.MISS_RATIO_LIST:
				return validateMiss_Ratio_List((Miss_Ratio_List)value, diagnostics, context);
			case ResultPackage.NON_PREEMPTIBLE_FP_POLICY:
				return validateNon_Preemptible_FP_Policy((Non_Preemptible_FP_Policy)value, diagnostics, context);
			case ResultPackage.OPERATION_RESULTS:
				return validateOperation_Results((Operation_Results)value, diagnostics, context);
			case ResultPackage.POLLING_POLICY:
				return validatePolling_Policy((Polling_Policy)value, diagnostics, context);
			case ResultPackage.PRIORITY_CEILING:
				return validatePriority_Ceiling((Priority_Ceiling)value, diagnostics, context);
			case ResultPackage.PROCESSING_RESOURCE_RESULTS:
				return validateProcessing_Resource_Results((Processing_Resource_Results)value, diagnostics, context);
			case ResultPackage.QUEUE_SIZE:
				return validateQueue_Size((Queue_Size)value, diagnostics, context);
			case ResultPackage.READY_QUEUE_SIZE:
				return validateReady_Queue_Size((Ready_Queue_Size)value, diagnostics, context);
			case ResultPackage.REAL_TIME_SITUATION:
				return validateREAL_TIME_SITUATION((REAL_TIME_SITUATION)value, diagnostics, context);
			case ResultPackage.SCHEDULING_SERVER_RESULTS:
				return validateScheduling_Server_Results((Scheduling_Server_Results)value, diagnostics, context);
			case ResultPackage.SHARED_RESOURCE_RESULTS:
				return validateShared_Resource_Results((Shared_Resource_Results)value, diagnostics, context);
			case ResultPackage.SIMULATION_TIMING_RESULT:
				return validateSimulation_Timing_Result((Simulation_Timing_Result)value, diagnostics, context);
			case ResultPackage.SLACK:
				return validateSlack((Slack)value, diagnostics, context);
			case ResultPackage.SPORADIC_SERVER_POLICY:
				return validateSporadic_Server_Policy((Sporadic_Server_Policy)value, diagnostics, context);
			case ResultPackage.TIMING_RESULT:
				return validateTiming_Result((Timing_Result)value, diagnostics, context);
			case ResultPackage.TRACE:
				return validateTrace((Trace)value, diagnostics, context);
			case ResultPackage.TRANSACTION_RESULTS:
				return validateTransaction_Results((Transaction_Results)value, diagnostics, context);
			case ResultPackage.UTILIZATION:
				return validateUtilization((Utilization)value, diagnostics, context);
			case ResultPackage.AFFIRMATIVE_ASSERTION:
				return validateAffirmative_Assertion((Affirmative_Assertion)value, diagnostics, context);
			case ResultPackage.ASSERTION:
				return validateAssertion((Assertion)value, diagnostics, context);
			case ResultPackage.AFFIRMATIVE_ASSERTION_OBJECT:
				return validateAffirmative_Assertion_Object((Affirmative_Assertion)value, diagnostics, context);
			case ResultPackage.ASSERTION_OBJECT:
				return validateAssertion_Object((Assertion)value, diagnostics, context);
			case ResultPackage.DATE_TIME:
				return validateDate_Time((XMLGregorianCalendar)value, diagnostics, context);
			case ResultPackage.EXTERNAL_REFERENCE:
				return validateExternal_Reference((String)value, diagnostics, context);
			case ResultPackage.FACTOR:
				return validateFactor((Float)value, diagnostics, context);
			case ResultPackage.FACTOR_OBJECT:
				return validateFactor_Object((Float)value, diagnostics, context);
			case ResultPackage.IDENTIFIER:
				return validateIdentifier((String)value, diagnostics, context);
			case ResultPackage.NORMALIZED_EXECUTION_TIME:
				return validateNormalized_Execution_Time((Float)value, diagnostics, context);
			case ResultPackage.NORMALIZED_EXECUTION_TIME_OBJECT:
				return validateNormalized_Execution_Time_Object((Float)value, diagnostics, context);
			case ResultPackage.PERCENTAGE:
				return validatePercentage((Float)value, diagnostics, context);
			case ResultPackage.PERCENTAGE_OBJECT:
				return validatePercentage_Object((Float)value, diagnostics, context);
			case ResultPackage.PRIORITY:
				return validatePriority((Integer)value, diagnostics, context);
			case ResultPackage.PRIORITY_OBJECT:
				return validatePriority_Object((Integer)value, diagnostics, context);
			case ResultPackage.TIME:
				return validateTime((Float)value, diagnostics, context);
			case ResultPackage.TIME_OBJECT:
				return validateTime_Object((Float)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDetailed_Utilization(Detailed_Utilization detailed_Utilization, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(detailed_Utilization, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDocument_Root(Document_Root document_Root, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(document_Root, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFixed_Priority_Policy(Fixed_Priority_Policy fixed_Priority_Policy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(fixed_Priority_Policy, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGlobal_Miss_Ratio(Global_Miss_Ratio global_Miss_Ratio, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(global_Miss_Ratio, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGlobal_Miss_Ratio_List(Global_Miss_Ratio_List global_Miss_Ratio_List, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(global_Miss_Ratio_List, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGlobal_Response_Time(Global_Response_Time global_Response_Time, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(global_Response_Time, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGlobal_Response_Time_List(Global_Response_Time_List global_Response_Time_List, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(global_Response_Time_List, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateInterrupt_FP_Policy(Interrupt_FP_Policy interrupt_FP_Policy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(interrupt_FP_Policy, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMiss_Ratio(Miss_Ratio miss_Ratio, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(miss_Ratio, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMiss_Ratio_List(Miss_Ratio_List miss_Ratio_List, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(miss_Ratio_List, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNon_Preemptible_FP_Policy(Non_Preemptible_FP_Policy non_Preemptible_FP_Policy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(non_Preemptible_FP_Policy, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOperation_Results(Operation_Results operation_Results, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(operation_Results, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePolling_Policy(Polling_Policy polling_Policy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(polling_Policy, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePriority_Ceiling(Priority_Ceiling priority_Ceiling, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(priority_Ceiling, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProcessing_Resource_Results(Processing_Resource_Results processing_Resource_Results, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(processing_Resource_Results, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateQueue_Size(Queue_Size queue_Size, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(queue_Size, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateReady_Queue_Size(Ready_Queue_Size ready_Queue_Size, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(ready_Queue_Size, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateREAL_TIME_SITUATION(REAL_TIME_SITUATION reaL_TIME_SITUATION, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(reaL_TIME_SITUATION, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateScheduling_Server_Results(Scheduling_Server_Results scheduling_Server_Results, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(scheduling_Server_Results, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateShared_Resource_Results(Shared_Resource_Results shared_Resource_Results, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(shared_Resource_Results, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSimulation_Timing_Result(Simulation_Timing_Result simulation_Timing_Result, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(simulation_Timing_Result, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSlack(Slack slack, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(slack, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSporadic_Server_Policy(Sporadic_Server_Policy sporadic_Server_Policy, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(sporadic_Server_Policy, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTiming_Result(Timing_Result timing_Result, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(timing_Result, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTrace(Trace trace, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(trace, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTransaction_Results(Transaction_Results transaction_Results, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(transaction_Results, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUtilization(Utilization utilization, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(utilization, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAffirmative_Assertion(Affirmative_Assertion affirmative_Assertion, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssertion(Assertion assertion, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAffirmative_Assertion_Object(Affirmative_Assertion affirmative_Assertion_Object, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAssertion_Object(Assertion assertion_Object, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDate_Time(XMLGregorianCalendar date_Time, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExternal_Reference(String external_Reference, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateExternal_Reference_Pattern(external_Reference, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @see #validateExternal_Reference_Pattern
	 */
	public static final  PatternMatcher [][] EXTERNAL_REFERENCE__PATTERN__VALUES =
		new PatternMatcher [][] {
			new PatternMatcher [] {
				XMLTypeUtil.createPatternMatcher("([a-z]|[A-Z])([a-z]|[A-Z]|[0-9]|.|_)*")
			},
			new PatternMatcher [] {
				XMLTypeUtil.createPatternMatcher("[\\i-[:]][\\c-[:]]*")
			},
			new PatternMatcher [] {
				XMLTypeUtil.createPatternMatcher("\\i\\c*")
			}
		};

	/**
	 * Validates the Pattern constraint of '<em>External Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExternal_Reference_Pattern(String external_Reference, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validatePattern(ResultPackage.Literals.EXTERNAL_REFERENCE, external_Reference, EXTERNAL_REFERENCE__PATTERN__VALUES, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFactor(float factor, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateFactor_Min(factor, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @see #validateFactor_Min
	 */
	public static final float FACTOR__MIN__VALUE = 0.0F;

	/**
	 * Validates the Min constraint of '<em>Factor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFactor_Min(float factor, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = factor > FACTOR__MIN__VALUE;
		if (!result && diagnostics != null)
			reportMinViolation(ResultPackage.Literals.FACTOR, factor, FACTOR__MIN__VALUE, false, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFactor_Object(Float factor_Object, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateFactor_Min(factor_Object, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIdentifier(String identifier, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateIdentifier_Pattern(identifier, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @see #validateIdentifier_Pattern
	 */
	public static final  PatternMatcher [][] IDENTIFIER__PATTERN__VALUES =
		new PatternMatcher [][] {
			new PatternMatcher [] {
				XMLTypeUtil.createPatternMatcher("([a-z]|[A-Z])([a-z]|[A-Z]|[0-9]|.|_)*")
			},
			new PatternMatcher [] {
				XMLTypeUtil.createPatternMatcher("[\\i-[:]][\\c-[:]]*")
			},
			new PatternMatcher [] {
				XMLTypeUtil.createPatternMatcher("\\i\\c*")
			}
		};

	/**
	 * Validates the Pattern constraint of '<em>Identifier</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIdentifier_Pattern(String identifier, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validatePattern(ResultPackage.Literals.IDENTIFIER, identifier, IDENTIFIER__PATTERN__VALUES, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNormalized_Execution_Time(float normalized_Execution_Time, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateNormalized_Execution_Time_Min(normalized_Execution_Time, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @see #validateNormalized_Execution_Time_Min
	 */
	public static final float NORMALIZED_EXECUTION_TIME__MIN__VALUE = 0.0F;

	/**
	 * Validates the Min constraint of '<em>Normalized Execution Time</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNormalized_Execution_Time_Min(float normalized_Execution_Time, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = normalized_Execution_Time >= NORMALIZED_EXECUTION_TIME__MIN__VALUE;
		if (!result && diagnostics != null)
			reportMinViolation(ResultPackage.Literals.NORMALIZED_EXECUTION_TIME, normalized_Execution_Time, NORMALIZED_EXECUTION_TIME__MIN__VALUE, true, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNormalized_Execution_Time_Object(Float normalized_Execution_Time_Object, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateNormalized_Execution_Time_Min(normalized_Execution_Time_Object, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePercentage(float percentage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validatePercentage_Min(percentage, diagnostics, context);
		if (result || diagnostics != null) result &= validatePercentage_Max(percentage, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @see #validatePercentage_Min
	 */
	public static final float PERCENTAGE__MIN__VALUE = 0.0F;

	/**
	 * Validates the Min constraint of '<em>Percentage</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePercentage_Min(float percentage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = percentage >= PERCENTAGE__MIN__VALUE;
		if (!result && diagnostics != null)
			reportMinViolation(ResultPackage.Literals.PERCENTAGE, percentage, PERCENTAGE__MIN__VALUE, true, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @see #validatePercentage_Max
	 */
	public static final float PERCENTAGE__MAX__VALUE = 100.0F;

	/**
	 * Validates the Max constraint of '<em>Percentage</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePercentage_Max(float percentage, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = percentage <= PERCENTAGE__MAX__VALUE;
		if (!result && diagnostics != null)
			reportMaxViolation(ResultPackage.Literals.PERCENTAGE, percentage, PERCENTAGE__MAX__VALUE, true, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePercentage_Object(Float percentage_Object, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validatePercentage_Min(percentage_Object, diagnostics, context);
		if (result || diagnostics != null) result &= validatePercentage_Max(percentage_Object, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePriority(int priority, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validatePriority_Min(priority, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @see #validatePriority_Min
	 */
	public static final int PRIORITY__MIN__VALUE = 0;

	/**
	 * Validates the Min constraint of '<em>Priority</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePriority_Min(int priority, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = priority >= PRIORITY__MIN__VALUE;
		if (!result && diagnostics != null)
			reportMinViolation(ResultPackage.Literals.PRIORITY, priority, PRIORITY__MIN__VALUE, true, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePriority_Object(Integer priority_Object, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validatePriority_Min(priority_Object, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTime(float time, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateTime_Min(time, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @see #validateTime_Min
	 */
	public static final float TIME__MIN__VALUE = 0.0F;

	/**
	 * Validates the Min constraint of '<em>Time</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTime_Min(float time, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = time >= TIME__MIN__VALUE;
		if (!result && diagnostics != null)
			reportMinViolation(ResultPackage.Literals.TIME, time, TIME__MIN__VALUE, true, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTime_Object(Float time_Object, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validateTime_Min(time_Object, diagnostics, context);
		return result;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //ResultValidator
