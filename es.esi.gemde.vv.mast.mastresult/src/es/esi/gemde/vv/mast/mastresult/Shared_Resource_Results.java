/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Shared Resource Results</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results#getPriorityCeiling <em>Priority Ceiling</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results#getUtilization <em>Utilization</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results#getQueueSize <em>Queue Size</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getShared_Resource_Results()
 * @model extendedMetaData="name='Shared_Resource_Results' kind='elementOnly'"
 * @generated
 */
public interface Shared_Resource_Results extends EObject {
	/**
	 * Returns the value of the '<em><b>Priority Ceiling</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority Ceiling</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority Ceiling</em>' attribute.
	 * @see #isSetPriorityCeiling()
	 * @see #unsetPriorityCeiling()
	 * @see #setPriorityCeiling(int)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getShared_Resource_Results_PriorityCeiling()
	 * @model unsettable="true" dataType="es.esi.gemde.vv.mast.mastresult.Priority"
	 *        extendedMetaData="kind='element' name='Priority_Ceiling' namespace='##targetNamespace'"
	 * @generated
	 */
	int getPriorityCeiling();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results#getPriorityCeiling <em>Priority Ceiling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority Ceiling</em>' attribute.
	 * @see #isSetPriorityCeiling()
	 * @see #unsetPriorityCeiling()
	 * @see #getPriorityCeiling()
	 * @generated
	 */
	void setPriorityCeiling(int value);

	/**
	 * Unsets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results#getPriorityCeiling <em>Priority Ceiling</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPriorityCeiling()
	 * @see #getPriorityCeiling()
	 * @see #setPriorityCeiling(int)
	 * @generated
	 */
	void unsetPriorityCeiling();

	/**
	 * Returns whether the value of the '{@link es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results#getPriorityCeiling <em>Priority Ceiling</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Priority Ceiling</em>' attribute is set.
	 * @see #unsetPriorityCeiling()
	 * @see #getPriorityCeiling()
	 * @see #setPriorityCeiling(int)
	 * @generated
	 */
	boolean isSetPriorityCeiling();

	/**
	 * Returns the value of the '<em><b>Utilization</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Utilization</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Utilization</em>' containment reference.
	 * @see #setUtilization(Utilization)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getShared_Resource_Results_Utilization()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Utilization' namespace='##targetNamespace'"
	 * @generated
	 */
	Utilization getUtilization();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results#getUtilization <em>Utilization</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Utilization</em>' containment reference.
	 * @see #getUtilization()
	 * @generated
	 */
	void setUtilization(Utilization value);

	/**
	 * Returns the value of the '<em><b>Queue Size</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Queue Size</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Queue Size</em>' containment reference.
	 * @see #setQueueSize(Queue_Size)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getShared_Resource_Results_QueueSize()
	 * @model containment="true"
	 *        extendedMetaData="kind='element' name='Queue_Size' namespace='##targetNamespace'"
	 * @generated
	 */
	Queue_Size getQueueSize();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results#getQueueSize <em>Queue Size</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Queue Size</em>' containment reference.
	 * @see #getQueueSize()
	 * @generated
	 */
	void setQueueSize(Queue_Size value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see es.esi.gemde.vv.mast.mastresult.ResultPackage#getShared_Resource_Results_Name()
	 * @model id="true" dataType="es.esi.gemde.vv.mast.mastresult.Identifier" required="true"
	 *        extendedMetaData="kind='attribute' name='Name'"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link es.esi.gemde.vv.mast.mastresult.Shared_Resource_Results#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

} // Shared_Resource_Results
