/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastresult.impl;

import es.esi.gemde.vv.mast.mastresult.Detailed_Utilization;
import es.esi.gemde.vv.mast.mastresult.Processing_Resource_Results;
import es.esi.gemde.vv.mast.mastresult.Ready_Queue_Size;
import es.esi.gemde.vv.mast.mastresult.ResultPackage;
import es.esi.gemde.vv.mast.mastresult.Slack;
import es.esi.gemde.vv.mast.mastresult.Utilization;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Processing Resource Results</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.Processing_Resource_ResultsImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.Processing_Resource_ResultsImpl#getSlack <em>Slack</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.Processing_Resource_ResultsImpl#getDetailedUtilization <em>Detailed Utilization</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.Processing_Resource_ResultsImpl#getUtilization <em>Utilization</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.Processing_Resource_ResultsImpl#getReadyQueueSize <em>Ready Queue Size</em>}</li>
 *   <li>{@link es.esi.gemde.vv.mast.mastresult.impl.Processing_Resource_ResultsImpl#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class Processing_Resource_ResultsImpl extends EObjectImpl implements Processing_Resource_Results {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Processing_Resource_ResultsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ResultPackage.Literals.PROCESSING_RESOURCE_RESULTS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, ResultPackage.PROCESSING_RESOURCE_RESULTS__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Slack> getSlack() {
		return getGroup().list(ResultPackage.Literals.PROCESSING_RESOURCE_RESULTS__SLACK);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Detailed_Utilization> getDetailedUtilization() {
		return getGroup().list(ResultPackage.Literals.PROCESSING_RESOURCE_RESULTS__DETAILED_UTILIZATION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Utilization> getUtilization() {
		return getGroup().list(ResultPackage.Literals.PROCESSING_RESOURCE_RESULTS__UTILIZATION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Ready_Queue_Size> getReadyQueueSize() {
		return getGroup().list(ResultPackage.Literals.PROCESSING_RESOURCE_RESULTS__READY_QUEUE_SIZE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ResultPackage.PROCESSING_RESOURCE_RESULTS__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__SLACK:
				return ((InternalEList<?>)getSlack()).basicRemove(otherEnd, msgs);
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__DETAILED_UTILIZATION:
				return ((InternalEList<?>)getDetailedUtilization()).basicRemove(otherEnd, msgs);
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__UTILIZATION:
				return ((InternalEList<?>)getUtilization()).basicRemove(otherEnd, msgs);
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__READY_QUEUE_SIZE:
				return ((InternalEList<?>)getReadyQueueSize()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__SLACK:
				return getSlack();
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__DETAILED_UTILIZATION:
				return getDetailedUtilization();
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__UTILIZATION:
				return getUtilization();
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__READY_QUEUE_SIZE:
				return getReadyQueueSize();
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__SLACK:
				getSlack().clear();
				getSlack().addAll((Collection<? extends Slack>)newValue);
				return;
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__DETAILED_UTILIZATION:
				getDetailedUtilization().clear();
				getDetailedUtilization().addAll((Collection<? extends Detailed_Utilization>)newValue);
				return;
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__UTILIZATION:
				getUtilization().clear();
				getUtilization().addAll((Collection<? extends Utilization>)newValue);
				return;
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__READY_QUEUE_SIZE:
				getReadyQueueSize().clear();
				getReadyQueueSize().addAll((Collection<? extends Ready_Queue_Size>)newValue);
				return;
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__GROUP:
				getGroup().clear();
				return;
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__SLACK:
				getSlack().clear();
				return;
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__DETAILED_UTILIZATION:
				getDetailedUtilization().clear();
				return;
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__UTILIZATION:
				getUtilization().clear();
				return;
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__READY_QUEUE_SIZE:
				getReadyQueueSize().clear();
				return;
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__GROUP:
				return group != null && !group.isEmpty();
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__SLACK:
				return !getSlack().isEmpty();
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__DETAILED_UTILIZATION:
				return !getDetailedUtilization().isEmpty();
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__UTILIZATION:
				return !getUtilization().isEmpty();
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__READY_QUEUE_SIZE:
				return !getReadyQueueSize().isEmpty();
			case ResultPackage.PROCESSING_RESOURCE_RESULTS__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(", name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //Processing_Resource_ResultsImpl
