/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.provider;


import es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link es.esi.gemde.vv.mast.mastmodel.FP_Packet_Based_Scheduler} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FP_Packet_Based_SchedulerItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FP_Packet_Based_SchedulerItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addMax_PriorityPropertyDescriptor(object);
			addMin_PriorityPropertyDescriptor(object);
			addPacket_Overhead_Avg_SizePropertyDescriptor(object);
			addPacket_Overhead_Max_SizePropertyDescriptor(object);
			addPacket_Overhead_Min_SizePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Max Priority feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMax_PriorityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FP_Packet_Based_Scheduler_Max_Priority_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FP_Packet_Based_Scheduler_Max_Priority_feature", "_UI_FP_Packet_Based_Scheduler_type"),
				 ModelPackage.Literals.FP_PACKET_BASED_SCHEDULER__MAX_PRIORITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Min Priority feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMin_PriorityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FP_Packet_Based_Scheduler_Min_Priority_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FP_Packet_Based_Scheduler_Min_Priority_feature", "_UI_FP_Packet_Based_Scheduler_type"),
				 ModelPackage.Literals.FP_PACKET_BASED_SCHEDULER__MIN_PRIORITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Packet Overhead Avg Size feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPacket_Overhead_Avg_SizePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FP_Packet_Based_Scheduler_Packet_Overhead_Avg_Size_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FP_Packet_Based_Scheduler_Packet_Overhead_Avg_Size_feature", "_UI_FP_Packet_Based_Scheduler_type"),
				 ModelPackage.Literals.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_AVG_SIZE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Packet Overhead Max Size feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPacket_Overhead_Max_SizePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FP_Packet_Based_Scheduler_Packet_Overhead_Max_Size_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FP_Packet_Based_Scheduler_Packet_Overhead_Max_Size_feature", "_UI_FP_Packet_Based_Scheduler_type"),
				 ModelPackage.Literals.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_MAX_SIZE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Packet Overhead Min Size feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPacket_Overhead_Min_SizePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FP_Packet_Based_Scheduler_Packet_Overhead_Min_Size_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FP_Packet_Based_Scheduler_Packet_Overhead_Min_Size_feature", "_UI_FP_Packet_Based_Scheduler_type"),
				 ModelPackage.Literals.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_MIN_SIZE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns FP_Packet_Based_Scheduler.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/FP_Packet_Based_Scheduler"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		FP_Packet_Based_Scheduler fP_Packet_Based_Scheduler = (FP_Packet_Based_Scheduler)object;
		return getString("_UI_FP_Packet_Based_Scheduler_type") + " " + fP_Packet_Based_Scheduler.getMax_Priority();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(FP_Packet_Based_Scheduler.class)) {
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__MAX_PRIORITY:
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__MIN_PRIORITY:
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_AVG_SIZE:
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_MAX_SIZE:
			case ModelPackage.FP_PACKET_BASED_SCHEDULER__PACKET_OVERHEAD_MIN_SIZE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MastModelEditPlugin.INSTANCE;
	}

}
