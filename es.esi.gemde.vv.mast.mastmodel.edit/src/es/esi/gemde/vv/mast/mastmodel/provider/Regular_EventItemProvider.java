/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.provider;


import es.esi.gemde.vv.mast.mastmodel.ModelFactory;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Regular_Event;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.util.FeatureMapUtil;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link es.esi.gemde.vv.mast.mastmodel.Regular_Event} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class Regular_EventItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Regular_EventItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addEventPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Event feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEventPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Regular_Event_Event_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Regular_Event_Event_feature", "_UI_Regular_Event_type"),
				 ModelPackage.Literals.REGULAR_EVENT__EVENT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ModelPackage.Literals.REGULAR_EVENT__GROUP);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Regular_Event.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Regular_Event"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Regular_Event)object).getEvent();
		return label == null || label.length() == 0 ?
			getString("_UI_Regular_Event_type") :
			getString("_UI_Regular_Event_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Regular_Event.class)) {
			case ModelPackage.REGULAR_EVENT__EVENT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ModelPackage.REGULAR_EVENT__GROUP:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.REGULAR_EVENT__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.REGULAR_EVENT__MAX_OUTPUT_JITTER_REQ,
					 ModelFactory.eINSTANCE.createMax_Output_Jitter_Req())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.REGULAR_EVENT__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.REGULAR_EVENT__HARD_GLOBAL_DEADLINE,
					 ModelFactory.eINSTANCE.createHard_Global_Deadline())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.REGULAR_EVENT__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.REGULAR_EVENT__SOFT_GLOBAL_DEADLINE,
					 ModelFactory.eINSTANCE.createSoft_Global_Deadline())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.REGULAR_EVENT__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.REGULAR_EVENT__GLOBAL_MAX_MISS_RATIO,
					 ModelFactory.eINSTANCE.createGlobal_Max_Miss_Ratio())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.REGULAR_EVENT__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.REGULAR_EVENT__HARD_LOCAL_DEADLINE,
					 ModelFactory.eINSTANCE.createHard_Local_Deadline())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.REGULAR_EVENT__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.REGULAR_EVENT__SOFT_LOCAL_DEADLINE,
					 ModelFactory.eINSTANCE.createSoft_Local_Deadline())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.REGULAR_EVENT__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.REGULAR_EVENT__LOCAL_MAX_MISS_RATIO,
					 ModelFactory.eINSTANCE.createLocal_Max_Miss_Ratio())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.REGULAR_EVENT__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.REGULAR_EVENT__COMPOSITE_TIMING_REQUIREMENT,
					 ModelFactory.eINSTANCE.createComposite_Timing_Requirement())));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MastModelEditPlugin.INSTANCE;
	}

}
