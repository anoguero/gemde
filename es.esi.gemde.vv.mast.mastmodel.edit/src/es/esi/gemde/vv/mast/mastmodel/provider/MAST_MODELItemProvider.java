/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.provider;


import es.esi.gemde.vv.mast.mastmodel.MAST_MODEL;
import es.esi.gemde.vv.mast.mastmodel.ModelFactory;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.util.FeatureMapUtil;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link es.esi.gemde.vv.mast.mastmodel.MAST_MODEL} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MAST_MODELItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAST_MODELItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addModel_DatePropertyDescriptor(object);
			addModel_NamePropertyDescriptor(object);
			addSystem_PiP_BehaviourPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Model Date feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addModel_DatePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MAST_MODEL_Model_Date_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MAST_MODEL_Model_Date_feature", "_UI_MAST_MODEL_type"),
				 ModelPackage.Literals.MAST_MODEL__MODEL_DATE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Model Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addModel_NamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MAST_MODEL_Model_Name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MAST_MODEL_Model_Name_feature", "_UI_MAST_MODEL_type"),
				 ModelPackage.Literals.MAST_MODEL__MODEL_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the System Pi PBehaviour feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSystem_PiP_BehaviourPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_MAST_MODEL_System_PiP_Behaviour_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_MAST_MODEL_System_PiP_Behaviour_feature", "_UI_MAST_MODEL_type"),
				 ModelPackage.Literals.MAST_MODEL__SYSTEM_PI_PBEHAVIOUR,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ModelPackage.Literals.MAST_MODEL__GROUP);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MAST_MODEL.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MAST_MODEL"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((MAST_MODEL)object).getModel_Name();
		return label == null || label.length() == 0 ?
			getString("_UI_MAST_MODEL_type") :
			getString("_UI_MAST_MODEL_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MAST_MODEL.class)) {
			case ModelPackage.MAST_MODEL__MODEL_DATE:
			case ModelPackage.MAST_MODEL__MODEL_NAME:
			case ModelPackage.MAST_MODEL__SYSTEM_PI_PBEHAVIOUR:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ModelPackage.MAST_MODEL__GROUP:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.MAST_MODEL__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.MAST_MODEL__REGULAR_PROCESSOR,
					 ModelFactory.eINSTANCE.createRegular_Processor())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.MAST_MODEL__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.MAST_MODEL__PACKET_BASED_NETWORK,
					 ModelFactory.eINSTANCE.createPacket_Based_Network())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.MAST_MODEL__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.MAST_MODEL__REGULAR_SCHEDULING_SERVER,
					 ModelFactory.eINSTANCE.createRegular_Scheduling_Server())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.MAST_MODEL__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.MAST_MODEL__IMMEDIATE_CEILING_RESOURCE,
					 ModelFactory.eINSTANCE.createImmediate_Ceiling_Resource())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.MAST_MODEL__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.MAST_MODEL__PRIORITY_INHERITANCE_RESOURCE,
					 ModelFactory.eINSTANCE.createPriority_Inheritance_Resource())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.MAST_MODEL__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.MAST_MODEL__SRP_RESOURCE,
					 ModelFactory.eINSTANCE.createSRP_Resource())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.MAST_MODEL__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.MAST_MODEL__SIMPLE_OPERATION,
					 ModelFactory.eINSTANCE.createSimple_Operation())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.MAST_MODEL__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.MAST_MODEL__MESSAGE_TRANSMISSION,
					 ModelFactory.eINSTANCE.createMessage_Transmission())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.MAST_MODEL__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.MAST_MODEL__COMPOSITE_OPERATION,
					 ModelFactory.eINSTANCE.createComposite_Operation())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.MAST_MODEL__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.MAST_MODEL__ENCLOSING_OPERATION,
					 ModelFactory.eINSTANCE.createEnclosing_Operation())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.MAST_MODEL__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.MAST_MODEL__REGULAR_TRANSACTION,
					 ModelFactory.eINSTANCE.createRegular_Transaction())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.MAST_MODEL__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.MAST_MODEL__PRIMARY_SCHEDULER,
					 ModelFactory.eINSTANCE.createPrimary_Scheduler())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.MAST_MODEL__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.MAST_MODEL__SECONDARY_SCHEDULER,
					 ModelFactory.eINSTANCE.createSecondary_Scheduler())));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MastModelEditPlugin.INSTANCE;
	}

}
