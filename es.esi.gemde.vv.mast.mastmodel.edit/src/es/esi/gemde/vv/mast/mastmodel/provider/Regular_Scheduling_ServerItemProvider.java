/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.provider;


import es.esi.gemde.vv.mast.mastmodel.ModelFactory;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link es.esi.gemde.vv.mast.mastmodel.Regular_Scheduling_Server} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class Regular_Scheduling_ServerItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Regular_Scheduling_ServerItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
			addSchedulerPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Regular_Scheduling_Server_Name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Regular_Scheduling_Server_Name_feature", "_UI_Regular_Scheduling_Server_type"),
				 ModelPackage.Literals.REGULAR_SCHEDULING_SERVER__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Scheduler feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSchedulerPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Regular_Scheduling_Server_Scheduler_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Regular_Scheduling_Server_Scheduler_feature", "_UI_Regular_Scheduling_Server_type"),
				 ModelPackage.Literals.REGULAR_SCHEDULING_SERVER__SCHEDULER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ModelPackage.Literals.REGULAR_SCHEDULING_SERVER__NON_PREEMPTIBLE_FP_POLICY);
			childrenFeatures.add(ModelPackage.Literals.REGULAR_SCHEDULING_SERVER__FIXED_PRIORITY_POLICY);
			childrenFeatures.add(ModelPackage.Literals.REGULAR_SCHEDULING_SERVER__INTERRUPT_FP_POLICY);
			childrenFeatures.add(ModelPackage.Literals.REGULAR_SCHEDULING_SERVER__POLLING_POLICY);
			childrenFeatures.add(ModelPackage.Literals.REGULAR_SCHEDULING_SERVER__SPORADIC_SERVER_POLICY);
			childrenFeatures.add(ModelPackage.Literals.REGULAR_SCHEDULING_SERVER__EDF_POLICY);
			childrenFeatures.add(ModelPackage.Literals.REGULAR_SCHEDULING_SERVER__SRP_PARAMETERS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Regular_Scheduling_Server.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Regular_Scheduling_Server"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Regular_Scheduling_Server)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Regular_Scheduling_Server_type") :
			getString("_UI_Regular_Scheduling_Server_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Regular_Scheduling_Server.class)) {
			case ModelPackage.REGULAR_SCHEDULING_SERVER__NAME:
			case ModelPackage.REGULAR_SCHEDULING_SERVER__SCHEDULER:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ModelPackage.REGULAR_SCHEDULING_SERVER__NON_PREEMPTIBLE_FP_POLICY:
			case ModelPackage.REGULAR_SCHEDULING_SERVER__FIXED_PRIORITY_POLICY:
			case ModelPackage.REGULAR_SCHEDULING_SERVER__INTERRUPT_FP_POLICY:
			case ModelPackage.REGULAR_SCHEDULING_SERVER__POLLING_POLICY:
			case ModelPackage.REGULAR_SCHEDULING_SERVER__SPORADIC_SERVER_POLICY:
			case ModelPackage.REGULAR_SCHEDULING_SERVER__EDF_POLICY:
			case ModelPackage.REGULAR_SCHEDULING_SERVER__SRP_PARAMETERS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.REGULAR_SCHEDULING_SERVER__NON_PREEMPTIBLE_FP_POLICY,
				 ModelFactory.eINSTANCE.createNon_Preemptible_FP_Policy()));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.REGULAR_SCHEDULING_SERVER__FIXED_PRIORITY_POLICY,
				 ModelFactory.eINSTANCE.createFixed_Priority_Policy()));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.REGULAR_SCHEDULING_SERVER__INTERRUPT_FP_POLICY,
				 ModelFactory.eINSTANCE.createInterrupt_FP_Policy()));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.REGULAR_SCHEDULING_SERVER__POLLING_POLICY,
				 ModelFactory.eINSTANCE.createPolling_Policy()));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.REGULAR_SCHEDULING_SERVER__SPORADIC_SERVER_POLICY,
				 ModelFactory.eINSTANCE.createSporadic_Server_Policy()));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.REGULAR_SCHEDULING_SERVER__EDF_POLICY,
				 ModelFactory.eINSTANCE.createEDF_Policy()));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.REGULAR_SCHEDULING_SERVER__SRP_PARAMETERS,
				 ModelFactory.eINSTANCE.createSRP_Parameters()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MastModelEditPlugin.INSTANCE;
	}

}
