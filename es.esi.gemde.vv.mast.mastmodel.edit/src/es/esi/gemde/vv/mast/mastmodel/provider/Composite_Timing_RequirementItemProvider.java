/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.provider;


import es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement;
import es.esi.gemde.vv.mast.mastmodel.ModelFactory;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.util.FeatureMapUtil;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link es.esi.gemde.vv.mast.mastmodel.Composite_Timing_Requirement} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class Composite_Timing_RequirementItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Composite_Timing_RequirementItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ModelPackage.Literals.COMPOSITE_TIMING_REQUIREMENT__GROUP);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Composite_Timing_Requirement.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Composite_Timing_Requirement"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_Composite_Timing_Requirement_type");
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Composite_Timing_Requirement.class)) {
			case ModelPackage.COMPOSITE_TIMING_REQUIREMENT__GROUP:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.COMPOSITE_TIMING_REQUIREMENT__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.COMPOSITE_TIMING_REQUIREMENT__MAX_OUTPUT_JITTER_REQ,
					 ModelFactory.eINSTANCE.createMax_Output_Jitter_Req())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.COMPOSITE_TIMING_REQUIREMENT__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.COMPOSITE_TIMING_REQUIREMENT__HARD_GLOBAL_DEADLINE,
					 ModelFactory.eINSTANCE.createHard_Global_Deadline())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.COMPOSITE_TIMING_REQUIREMENT__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.COMPOSITE_TIMING_REQUIREMENT__SOFT_GLOBAL_DEADLINE,
					 ModelFactory.eINSTANCE.createSoft_Global_Deadline())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.COMPOSITE_TIMING_REQUIREMENT__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.COMPOSITE_TIMING_REQUIREMENT__GLOBAL_MAX_MISS_RATIO,
					 ModelFactory.eINSTANCE.createGlobal_Max_Miss_Ratio())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.COMPOSITE_TIMING_REQUIREMENT__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.COMPOSITE_TIMING_REQUIREMENT__HARD_LOCAL_DEADLINE,
					 ModelFactory.eINSTANCE.createHard_Local_Deadline())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.COMPOSITE_TIMING_REQUIREMENT__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.COMPOSITE_TIMING_REQUIREMENT__SOFT_LOCAL_DEADLINE,
					 ModelFactory.eINSTANCE.createSoft_Local_Deadline())));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.COMPOSITE_TIMING_REQUIREMENT__GROUP,
				 FeatureMapUtil.createEntry
					(ModelPackage.Literals.COMPOSITE_TIMING_REQUIREMENT__LOCAL_MAX_MISS_RATIO,
					 ModelFactory.eINSTANCE.createLocal_Max_Miss_Ratio())));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MastModelEditPlugin.INSTANCE;
	}

}
