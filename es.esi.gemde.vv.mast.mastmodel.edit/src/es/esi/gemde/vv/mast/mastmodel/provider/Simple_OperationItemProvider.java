/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.provider;


import es.esi.gemde.vv.mast.mastmodel.ModelFactory;
import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.Simple_Operation;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link es.esi.gemde.vv.mast.mastmodel.Simple_Operation} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class Simple_OperationItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Simple_OperationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addShared_Resources_ListPropertyDescriptor(object);
			addShared_Resources_To_LockPropertyDescriptor(object);
			addShared_Resources_To_UnlockPropertyDescriptor(object);
			addAverage_Case_Execution_TimePropertyDescriptor(object);
			addBest_Case_Execution_TimePropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addWorst_Case_Execution_TimePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Shared Resources List feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addShared_Resources_ListPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Simple_Operation_Shared_Resources_List_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Simple_Operation_Shared_Resources_List_feature", "_UI_Simple_Operation_type"),
				 ModelPackage.Literals.SIMPLE_OPERATION__SHARED_RESOURCES_LIST,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Shared Resources To Lock feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addShared_Resources_To_LockPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Simple_Operation_Shared_Resources_To_Lock_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Simple_Operation_Shared_Resources_To_Lock_feature", "_UI_Simple_Operation_type"),
				 ModelPackage.Literals.SIMPLE_OPERATION__SHARED_RESOURCES_TO_LOCK,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Shared Resources To Unlock feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addShared_Resources_To_UnlockPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Simple_Operation_Shared_Resources_To_Unlock_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Simple_Operation_Shared_Resources_To_Unlock_feature", "_UI_Simple_Operation_type"),
				 ModelPackage.Literals.SIMPLE_OPERATION__SHARED_RESOURCES_TO_UNLOCK,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Average Case Execution Time feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAverage_Case_Execution_TimePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Simple_Operation_Average_Case_Execution_Time_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Simple_Operation_Average_Case_Execution_Time_feature", "_UI_Simple_Operation_type"),
				 ModelPackage.Literals.SIMPLE_OPERATION__AVERAGE_CASE_EXECUTION_TIME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Best Case Execution Time feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBest_Case_Execution_TimePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Simple_Operation_Best_Case_Execution_Time_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Simple_Operation_Best_Case_Execution_Time_feature", "_UI_Simple_Operation_type"),
				 ModelPackage.Literals.SIMPLE_OPERATION__BEST_CASE_EXECUTION_TIME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Simple_Operation_Name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Simple_Operation_Name_feature", "_UI_Simple_Operation_type"),
				 ModelPackage.Literals.SIMPLE_OPERATION__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Worst Case Execution Time feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addWorst_Case_Execution_TimePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Simple_Operation_Worst_Case_Execution_Time_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Simple_Operation_Worst_Case_Execution_Time_feature", "_UI_Simple_Operation_type"),
				 ModelPackage.Literals.SIMPLE_OPERATION__WORST_CASE_EXECUTION_TIME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ModelPackage.Literals.SIMPLE_OPERATION__OVERRIDDEN_FIXED_PRIORITY);
			childrenFeatures.add(ModelPackage.Literals.SIMPLE_OPERATION__OVERRIDDEN_PERMANENT_FP);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Simple_Operation.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Simple_Operation"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Simple_Operation)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Simple_Operation_type") :
			getString("_UI_Simple_Operation_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Simple_Operation.class)) {
			case ModelPackage.SIMPLE_OPERATION__SHARED_RESOURCES_LIST:
			case ModelPackage.SIMPLE_OPERATION__SHARED_RESOURCES_TO_LOCK:
			case ModelPackage.SIMPLE_OPERATION__SHARED_RESOURCES_TO_UNLOCK:
			case ModelPackage.SIMPLE_OPERATION__AVERAGE_CASE_EXECUTION_TIME:
			case ModelPackage.SIMPLE_OPERATION__BEST_CASE_EXECUTION_TIME:
			case ModelPackage.SIMPLE_OPERATION__NAME:
			case ModelPackage.SIMPLE_OPERATION__WORST_CASE_EXECUTION_TIME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ModelPackage.SIMPLE_OPERATION__OVERRIDDEN_FIXED_PRIORITY:
			case ModelPackage.SIMPLE_OPERATION__OVERRIDDEN_PERMANENT_FP:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.SIMPLE_OPERATION__OVERRIDDEN_FIXED_PRIORITY,
				 ModelFactory.eINSTANCE.createOverridden_Fixed_Priority()));

		newChildDescriptors.add
			(createChildParameter
				(ModelPackage.Literals.SIMPLE_OPERATION__OVERRIDDEN_PERMANENT_FP,
				 ModelFactory.eINSTANCE.createOverridden_Permanent_FP()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MastModelEditPlugin.INSTANCE;
	}

}
