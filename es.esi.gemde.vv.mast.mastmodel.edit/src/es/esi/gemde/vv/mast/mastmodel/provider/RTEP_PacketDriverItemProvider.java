/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package es.esi.gemde.vv.mast.mastmodel.provider;


import es.esi.gemde.vv.mast.mastmodel.ModelPackage;
import es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link es.esi.gemde.vv.mast.mastmodel.RTEP_PacketDriver} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class RTEP_PacketDriverItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RTEP_PacketDriverItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFailure_TimeoutPropertyDescriptor(object);
			addMessage_PartitioningPropertyDescriptor(object);
			addNumber_Of_StationsPropertyDescriptor(object);
			addPacket_Discard_OperationPropertyDescriptor(object);
			addPacket_Interrupt_ServerPropertyDescriptor(object);
			addPacket_ISR_OperationPropertyDescriptor(object);
			addPacket_Receive_OperationPropertyDescriptor(object);
			addPacket_Retransmission_OperationPropertyDescriptor(object);
			addPacket_Send_OperationPropertyDescriptor(object);
			addPacket_ServerPropertyDescriptor(object);
			addPacket_Transmission_RetriesPropertyDescriptor(object);
			addRTA_Overhead_ModelPropertyDescriptor(object);
			addToken_Check_OperationPropertyDescriptor(object);
			addToken_DelayPropertyDescriptor(object);
			addToken_Manage_OperationPropertyDescriptor(object);
			addToken_Retransmission_OperationPropertyDescriptor(object);
			addToken_Transmission_RetriesPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Failure Timeout feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFailure_TimeoutPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RTEP_PacketDriver_Failure_Timeout_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RTEP_PacketDriver_Failure_Timeout_feature", "_UI_RTEP_PacketDriver_type"),
				 ModelPackage.Literals.RTEP_PACKET_DRIVER__FAILURE_TIMEOUT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Message Partitioning feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMessage_PartitioningPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RTEP_PacketDriver_Message_Partitioning_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RTEP_PacketDriver_Message_Partitioning_feature", "_UI_RTEP_PacketDriver_type"),
				 ModelPackage.Literals.RTEP_PACKET_DRIVER__MESSAGE_PARTITIONING,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Number Of Stations feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNumber_Of_StationsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RTEP_PacketDriver_Number_Of_Stations_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RTEP_PacketDriver_Number_Of_Stations_feature", "_UI_RTEP_PacketDriver_type"),
				 ModelPackage.Literals.RTEP_PACKET_DRIVER__NUMBER_OF_STATIONS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Packet Discard Operation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPacket_Discard_OperationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RTEP_PacketDriver_Packet_Discard_Operation_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RTEP_PacketDriver_Packet_Discard_Operation_feature", "_UI_RTEP_PacketDriver_type"),
				 ModelPackage.Literals.RTEP_PACKET_DRIVER__PACKET_DISCARD_OPERATION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Packet Interrupt Server feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPacket_Interrupt_ServerPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RTEP_PacketDriver_Packet_Interrupt_Server_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RTEP_PacketDriver_Packet_Interrupt_Server_feature", "_UI_RTEP_PacketDriver_type"),
				 ModelPackage.Literals.RTEP_PACKET_DRIVER__PACKET_INTERRUPT_SERVER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Packet ISR Operation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPacket_ISR_OperationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RTEP_PacketDriver_Packet_ISR_Operation_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RTEP_PacketDriver_Packet_ISR_Operation_feature", "_UI_RTEP_PacketDriver_type"),
				 ModelPackage.Literals.RTEP_PACKET_DRIVER__PACKET_ISR_OPERATION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Packet Receive Operation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPacket_Receive_OperationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RTEP_PacketDriver_Packet_Receive_Operation_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RTEP_PacketDriver_Packet_Receive_Operation_feature", "_UI_RTEP_PacketDriver_type"),
				 ModelPackage.Literals.RTEP_PACKET_DRIVER__PACKET_RECEIVE_OPERATION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Packet Retransmission Operation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPacket_Retransmission_OperationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RTEP_PacketDriver_Packet_Retransmission_Operation_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RTEP_PacketDriver_Packet_Retransmission_Operation_feature", "_UI_RTEP_PacketDriver_type"),
				 ModelPackage.Literals.RTEP_PACKET_DRIVER__PACKET_RETRANSMISSION_OPERATION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Packet Send Operation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPacket_Send_OperationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RTEP_PacketDriver_Packet_Send_Operation_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RTEP_PacketDriver_Packet_Send_Operation_feature", "_UI_RTEP_PacketDriver_type"),
				 ModelPackage.Literals.RTEP_PACKET_DRIVER__PACKET_SEND_OPERATION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Packet Server feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPacket_ServerPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RTEP_PacketDriver_Packet_Server_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RTEP_PacketDriver_Packet_Server_feature", "_UI_RTEP_PacketDriver_type"),
				 ModelPackage.Literals.RTEP_PACKET_DRIVER__PACKET_SERVER,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Packet Transmission Retries feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPacket_Transmission_RetriesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RTEP_PacketDriver_Packet_Transmission_Retries_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RTEP_PacketDriver_Packet_Transmission_Retries_feature", "_UI_RTEP_PacketDriver_type"),
				 ModelPackage.Literals.RTEP_PACKET_DRIVER__PACKET_TRANSMISSION_RETRIES,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the RTA Overhead Model feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRTA_Overhead_ModelPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RTEP_PacketDriver_RTA_Overhead_Model_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RTEP_PacketDriver_RTA_Overhead_Model_feature", "_UI_RTEP_PacketDriver_type"),
				 ModelPackage.Literals.RTEP_PACKET_DRIVER__RTA_OVERHEAD_MODEL,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Token Check Operation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addToken_Check_OperationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RTEP_PacketDriver_Token_Check_Operation_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RTEP_PacketDriver_Token_Check_Operation_feature", "_UI_RTEP_PacketDriver_type"),
				 ModelPackage.Literals.RTEP_PACKET_DRIVER__TOKEN_CHECK_OPERATION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Token Delay feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addToken_DelayPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RTEP_PacketDriver_Token_Delay_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RTEP_PacketDriver_Token_Delay_feature", "_UI_RTEP_PacketDriver_type"),
				 ModelPackage.Literals.RTEP_PACKET_DRIVER__TOKEN_DELAY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Token Manage Operation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addToken_Manage_OperationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RTEP_PacketDriver_Token_Manage_Operation_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RTEP_PacketDriver_Token_Manage_Operation_feature", "_UI_RTEP_PacketDriver_type"),
				 ModelPackage.Literals.RTEP_PACKET_DRIVER__TOKEN_MANAGE_OPERATION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Token Retransmission Operation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addToken_Retransmission_OperationPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RTEP_PacketDriver_Token_Retransmission_Operation_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RTEP_PacketDriver_Token_Retransmission_Operation_feature", "_UI_RTEP_PacketDriver_type"),
				 ModelPackage.Literals.RTEP_PACKET_DRIVER__TOKEN_RETRANSMISSION_OPERATION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Token Transmission Retries feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addToken_Transmission_RetriesPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_RTEP_PacketDriver_Token_Transmission_Retries_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_RTEP_PacketDriver_Token_Transmission_Retries_feature", "_UI_RTEP_PacketDriver_type"),
				 ModelPackage.Literals.RTEP_PACKET_DRIVER__TOKEN_TRANSMISSION_RETRIES,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns RTEP_PacketDriver.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/RTEP_PacketDriver"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		RTEP_PacketDriver rteP_PacketDriver = (RTEP_PacketDriver)object;
		return getString("_UI_RTEP_PacketDriver_type") + " " + rteP_PacketDriver.getFailure_Timeout();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(RTEP_PacketDriver.class)) {
			case ModelPackage.RTEP_PACKET_DRIVER__FAILURE_TIMEOUT:
			case ModelPackage.RTEP_PACKET_DRIVER__MESSAGE_PARTITIONING:
			case ModelPackage.RTEP_PACKET_DRIVER__NUMBER_OF_STATIONS:
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_DISCARD_OPERATION:
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_INTERRUPT_SERVER:
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_ISR_OPERATION:
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_RECEIVE_OPERATION:
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_RETRANSMISSION_OPERATION:
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_SEND_OPERATION:
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_SERVER:
			case ModelPackage.RTEP_PACKET_DRIVER__PACKET_TRANSMISSION_RETRIES:
			case ModelPackage.RTEP_PACKET_DRIVER__RTA_OVERHEAD_MODEL:
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_CHECK_OPERATION:
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_DELAY:
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_MANAGE_OPERATION:
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_RETRANSMISSION_OPERATION:
			case ModelPackage.RTEP_PACKET_DRIVER__TOKEN_TRANSMISSION_RETRIES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MastModelEditPlugin.INSTANCE;
	}

}
