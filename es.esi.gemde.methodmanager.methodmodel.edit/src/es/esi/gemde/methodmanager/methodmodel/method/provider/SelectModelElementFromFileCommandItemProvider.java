/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.provider;


import es.esi.gemde.methodmanager.methodmodel.method.MethodPackage;
import es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SelectModelElementFromFileCommandItemProvider
  extends SimpleCommandItemProvider
  implements
    IEditingDomainItemProvider,
    IStructuredItemContentProvider,
    ITreeItemContentProvider,
    IItemLabelProvider,
    IItemPropertySource
{
  /**
   * This constructs an instance from a factory and a notifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SelectModelElementFromFileCommandItemProvider(AdapterFactory adapterFactory)
  {
    super(adapterFactory);
  }

  /**
   * This returns the property descriptors for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
  {
    if (itemPropertyDescriptors == null)
    {
      super.getPropertyDescriptors(object);

      addModelFilePropertyDescriptor(object);
      addAddToListPropertyDescriptor(object);
      addListPropertyDescriptor(object);
      addIdentifierPropertyDescriptor(object);
    }
    return itemPropertyDescriptors;
  }

  /**
   * This adds a property descriptor for the Model File feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addModelFilePropertyDescriptor(Object object)
  {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_SelectModelElementFromFileCommand_modelFile_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_SelectModelElementFromFileCommand_modelFile_feature", "_UI_SelectModelElementFromFileCommand_type"),
         MethodPackage.Literals.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__MODEL_FILE,
         true,
         false,
         true,
         null,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Add To List feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addAddToListPropertyDescriptor(Object object)
  {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_SelectModelElementFromFileCommand_addToList_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_SelectModelElementFromFileCommand_addToList_feature", "_UI_SelectModelElementFromFileCommand_type"),
         MethodPackage.Literals.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__ADD_TO_LIST,
         true,
         false,
         true,
         null,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the List feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addListPropertyDescriptor(Object object)
  {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_SelectModelElementFromFileCommand_list_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_SelectModelElementFromFileCommand_list_feature", "_UI_SelectModelElementFromFileCommand_type"),
         MethodPackage.Literals.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__LIST,
         true,
         false,
         true,
         null,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Identifier feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addIdentifierPropertyDescriptor(Object object)
  {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_SelectModelElementFromFileCommand_identifier_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_SelectModelElementFromFileCommand_identifier_feature", "_UI_SelectModelElementFromFileCommand_type"),
         MethodPackage.Literals.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__IDENTIFIER,
         false,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This returns SelectModelElementFromFileCommand.gif.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object getImage(Object object)
  {
    return overlayImage(object, getResourceLocator().getImage("full/obj16/SelectModelElementFromFileCommand"));
  }

  /**
   * This returns the label text for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getText(Object object)
  {
    String label = ((SelectModelElementFromFileCommand)object).getName();
    return label == null || label.length() == 0 ?
      getString("_UI_SelectModelElementFromFileCommand_type") :
      getString("_UI_SelectModelElementFromFileCommand_type") + " " + label;
  }

  /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void notifyChanged(Notification notification)
  {
    updateChildren(notification);

    switch (notification.getFeatureID(SelectModelElementFromFileCommand.class))
    {
      case MethodPackage.SELECT_MODEL_ELEMENT_FROM_FILE_COMMAND__IDENTIFIER:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
        return;
    }
    super.notifyChanged(notification);
  }

  /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object)
  {
    super.collectNewChildDescriptors(newChildDescriptors, object);
  }

}
