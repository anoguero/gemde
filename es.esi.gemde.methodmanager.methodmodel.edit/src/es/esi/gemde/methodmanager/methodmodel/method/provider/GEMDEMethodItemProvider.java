/**
 */
package es.esi.gemde.methodmanager.methodmodel.method.provider;


import es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod;
import es.esi.gemde.methodmanager.methodmodel.method.MethodFactory;
import es.esi.gemde.methodmanager.methodmodel.method.MethodPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class GEMDEMethodItemProvider
  extends ItemProviderAdapter
  implements
    IEditingDomainItemProvider,
    IStructuredItemContentProvider,
    ITreeItemContentProvider,
    IItemLabelProvider,
    IItemPropertySource
{
  /**
   * This constructs an instance from a factory and a notifier.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public GEMDEMethodItemProvider(AdapterFactory adapterFactory)
  {
    super(adapterFactory);
  }

  /**
   * This returns the property descriptors for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object)
  {
    if (itemPropertyDescriptors == null)
    {
      super.getPropertyDescriptors(object);

      addNamePropertyDescriptor(object);
      addDescriptionPropertyDescriptor(object);
      addProjectNamePropertyDescriptor(object);
      addInitialPhasePropertyDescriptor(object);
    }
    return itemPropertyDescriptors;
  }

  /**
   * This adds a property descriptor for the Name feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addNamePropertyDescriptor(Object object)
  {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_GEMDEMethod_name_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_GEMDEMethod_name_feature", "_UI_GEMDEMethod_type"),
         MethodPackage.Literals.GEMDE_METHOD__NAME,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Description feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addDescriptionPropertyDescriptor(Object object)
  {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_GEMDEMethod_description_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_GEMDEMethod_description_feature", "_UI_GEMDEMethod_type"),
         MethodPackage.Literals.GEMDE_METHOD__DESCRIPTION,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Project Name feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addProjectNamePropertyDescriptor(Object object)
  {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_GEMDEMethod_projectName_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_GEMDEMethod_projectName_feature", "_UI_GEMDEMethod_type"),
         MethodPackage.Literals.GEMDE_METHOD__PROJECT_NAME,
         true,
         false,
         false,
         ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
         null,
         null));
  }

  /**
   * This adds a property descriptor for the Initial Phase feature.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void addInitialPhasePropertyDescriptor(Object object)
  {
    itemPropertyDescriptors.add
      (createItemPropertyDescriptor
        (((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
         getResourceLocator(),
         getString("_UI_GEMDEMethod_initialPhase_feature"),
         getString("_UI_PropertyDescriptor_description", "_UI_GEMDEMethod_initialPhase_feature", "_UI_GEMDEMethod_type"),
         MethodPackage.Literals.GEMDE_METHOD__INITIAL_PHASE,
         true,
         false,
         true,
         null,
         null,
         null));
  }

  /**
   * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
   * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
   * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object)
  {
    if (childrenFeatures == null)
    {
      super.getChildrenFeatures(object);
      childrenFeatures.add(MethodPackage.Literals.GEMDE_METHOD__PHASES);
      childrenFeatures.add(MethodPackage.Literals.GEMDE_METHOD__VARIABLES);
      childrenFeatures.add(MethodPackage.Literals.GEMDE_METHOD__COMMANDS);
    }
    return childrenFeatures;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EStructuralFeature getChildFeature(Object object, Object child)
  {
    // Check the type of the specified child object and return the proper feature to use for
    // adding (see {@link AddCommand}) it as a child.

    return super.getChildFeature(object, child);
  }

  /**
   * This returns GEMDEMethod.gif.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object getImage(Object object)
  {
    return overlayImage(object, getResourceLocator().getImage("full/obj16/GEMDEMethod"));
  }

  /**
   * This returns the label text for the adapted class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getText(Object object)
  {
    String label = ((GEMDEMethod)object).getName();
    return label == null || label.length() == 0 ?
      getString("_UI_GEMDEMethod_type") :
      getString("_UI_GEMDEMethod_type") + " " + label;
  }

  /**
   * This handles model notifications by calling {@link #updateChildren} to update any cached
   * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void notifyChanged(Notification notification)
  {
    updateChildren(notification);

    switch (notification.getFeatureID(GEMDEMethod.class))
    {
      case MethodPackage.GEMDE_METHOD__NAME:
      case MethodPackage.GEMDE_METHOD__DESCRIPTION:
      case MethodPackage.GEMDE_METHOD__PROJECT_NAME:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
        return;
      case MethodPackage.GEMDE_METHOD__PHASES:
      case MethodPackage.GEMDE_METHOD__VARIABLES:
      case MethodPackage.GEMDE_METHOD__COMMANDS:
        fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
        return;
    }
    super.notifyChanged(notification);
  }

  /**
   * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
   * that can be created under this object.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object)
  {
    super.collectNewChildDescriptors(newChildDescriptors, object);

    newChildDescriptors.add
      (createChildParameter
        (MethodPackage.Literals.GEMDE_METHOD__PHASES,
         MethodFactory.eINSTANCE.createPhase()));

    newChildDescriptors.add
      (createChildParameter
        (MethodPackage.Literals.GEMDE_METHOD__VARIABLES,
         MethodFactory.eINSTANCE.createMethodVariable()));

    newChildDescriptors.add
      (createChildParameter
        (MethodPackage.Literals.GEMDE_METHOD__COMMANDS,
         MethodFactory.eINSTANCE.createComplexCommand()));

    newChildDescriptors.add
      (createChildParameter
        (MethodPackage.Literals.GEMDE_METHOD__COMMANDS,
         MethodFactory.eINSTANCE.createCustomCommand()));

    newChildDescriptors.add
      (createChildParameter
        (MethodPackage.Literals.GEMDE_METHOD__COMMANDS,
         MethodFactory.eINSTANCE.createCallWizardCommand()));

    newChildDescriptors.add
      (createChildParameter
        (MethodPackage.Literals.GEMDE_METHOD__COMMANDS,
         MethodFactory.eINSTANCE.createSelectFileCommand()));

    newChildDescriptors.add
      (createChildParameter
        (MethodPackage.Literals.GEMDE_METHOD__COMMANDS,
         MethodFactory.eINSTANCE.createSelectFolderCommand()));

    newChildDescriptors.add
      (createChildParameter
        (MethodPackage.Literals.GEMDE_METHOD__COMMANDS,
         MethodFactory.eINSTANCE.createSelectMetamodelCommand()));

    newChildDescriptors.add
      (createChildParameter
        (MethodPackage.Literals.GEMDE_METHOD__COMMANDS,
         MethodFactory.eINSTANCE.createSelectModelElementFromFileCommand()));

    newChildDescriptors.add
      (createChildParameter
        (MethodPackage.Literals.GEMDE_METHOD__COMMANDS,
         MethodFactory.eINSTANCE.createSelectModelElementFromEObjectCommand()));

    newChildDescriptors.add
      (createChildParameter
        (MethodPackage.Literals.GEMDE_METHOD__COMMANDS,
         MethodFactory.eINSTANCE.createSelectValidationCommand()));

    newChildDescriptors.add
      (createChildParameter
        (MethodPackage.Literals.GEMDE_METHOD__COMMANDS,
         MethodFactory.eINSTANCE.createSelectTransformationCommand()));

    newChildDescriptors.add
      (createChildParameter
        (MethodPackage.Literals.GEMDE_METHOD__COMMANDS,
         MethodFactory.eINSTANCE.createLaunchValidationCommand()));

    newChildDescriptors.add
      (createChildParameter
        (MethodPackage.Literals.GEMDE_METHOD__COMMANDS,
         MethodFactory.eINSTANCE.createLaunchTransformationCommand()));

    newChildDescriptors.add
      (createChildParameter
        (MethodPackage.Literals.GEMDE_METHOD__COMMANDS,
         MethodFactory.eINSTANCE.createOpenEditorCommand()));
  }

  /**
   * Return the resource locator for this item provider's resources.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ResourceLocator getResourceLocator()
  {
    return MethodEditPlugin.INSTANCE;
  }

}
