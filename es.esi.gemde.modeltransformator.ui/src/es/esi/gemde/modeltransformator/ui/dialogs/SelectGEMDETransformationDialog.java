package es.esi.gemde.modeltransformator.ui.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import es.esi.gemde.core.CorePlugin;
import es.esi.gemde.core.resources.IGemdeResource;
import es.esi.gemde.core.resources.OverlaidImageDescriptor;
import es.esi.gemde.modeltransformator.ModelTransformatorPlugin;
import es.esi.gemde.modeltransformator.service.ITransformation;

public class SelectGEMDETransformationDialog extends Dialog {

	private Button okButton;
	private ITransformation transformation;
	
	public SelectGEMDETransformationDialog(Shell parentShell) {
		super(parentShell);
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Select GEMDE Transformation");
		newShell.setImage(CorePlugin.getImage(CorePlugin.TRANSFORMATION_ICON));
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		parent.setLayout(new GridLayout(1, true));
		
		Label label = new Label(parent, SWT.NONE);
		GridData labelData = new GridData(GridData.FILL_HORIZONTAL);
		labelData.horizontalSpan = 1;
		label.setLayoutData(labelData);
		label.setText("Available Transformations:");
		
		TreeViewer tree = new TreeViewer(parent, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		GridData treeData = new GridData(GridData.FILL_BOTH);
		treeData.horizontalSpan = 1;
		tree.getTree().setLayoutData(treeData);
		
		tree.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				Object obj = ((StructuredSelection)event.getSelection()).getFirstElement();
				if (obj instanceof ITransformation) {
					transformation = (ITransformation) obj;
				}
				checkCompleted();
			}
		});
		
		// Fill the tree with
		Tree t = tree.getTree();
		t.clearAll(true);
		for (ITransformation transform : ModelTransformatorPlugin.getService().getTransformationRepository()) {
			TreeItem ti = new TreeItem(t, SWT.NONE);
			ti.setData(transform);
			ti.setText(transform.getName());
			if (transform.getResourceType().equals(IGemdeResource.ResourceType.PROTECTED)) {
				OverlaidImageDescriptor id = new OverlaidImageDescriptor(ModelTransformatorPlugin.getService().getEngineImage(transform.getEngine()), CorePlugin.getImage(CorePlugin.LOCK_OVERLAY), CorePlugin.BOTTOM_RIGHT);
				ti.setImage(id.getImage());
			}
			else if (transform.getResourceType().equals(IGemdeResource.ResourceType.NETWORKED)) {
				OverlaidImageDescriptor id = new OverlaidImageDescriptor(ModelTransformatorPlugin.getService().getEngineImage(transform.getEngine()), CorePlugin.getImage(CorePlugin.NET_OVERLAY), CorePlugin.BOTTOM_RIGHT);
				ti.setImage(id.getImage());
			}
			else {
				ti.setImage(ModelTransformatorPlugin.getService().getEngineImage(transform.getEngine()));
			}
		}
		
		return parent;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// Only create an OK button
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		okButton = getButton(IDialogConstants.OK_ID);
		checkCompleted();
	}
	
	private void checkCompleted() {
		if (okButton != null) {
			okButton.setEnabled(transformation != null);
		}
	}
	
	public ITransformation getSelectedTransformation() {
		return transformation;
	}
}
