/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/

package es.esi.gemde.modeltransformator.ui.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import es.esi.gemde.core.CorePlugin;
import es.esi.gemde.core.resources.ExtendedWizardDialog;
import es.esi.gemde.core.resources.IGemdeResource.ResourceType;
import es.esi.gemde.core.resources.OverlaidImageDescriptor;
import es.esi.gemde.modeltransformator.ModelTransformatorPlugin;
import es.esi.gemde.modeltransformator.service.IModelTransformationService;
import es.esi.gemde.modeltransformator.service.ITransformation;
import es.esi.gemde.modeltransformator.ui.wizards.TransformationCreationWizard;



/**
 * A dialog for managing the Transformation Repository of the Model Transformation Service.
 *
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 * @version 1.0.4
 * @since 1.0
 *
 */
public class TransformationManagementDialog extends Dialog {

	/* (non-Javadoc)
	 * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("GEMDE Transformation Repository Viewer");
		newShell.setImage(CorePlugin.getImage(CorePlugin.TRANSFORMATION_REPOSITORY_ICON));
		newShell.setSize(600, 400);
	}

	private TreeViewer transformationViewer;
	private Button createButton;
	private Button editButton;
	private Button removeButton;
	private IModelTransformationService server;
	
	private SelectionListener createButtonListener = new SelectionListener () {

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			// Not needed
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			TransformationCreationWizard wizard = new TransformationCreationWizard();
			WizardDialog dialog = new ExtendedWizardDialog(getShell(), wizard, CorePlugin.getImage(CorePlugin.TRANSFORMATION_ICON, CorePlugin.NEW_OVERLAY, CorePlugin.TOP_RIGHT));
			int result = dialog.open();
			
			if (result == Dialog.OK) {
				server.addTransformation(wizard.getCreatedTransformation());
			}
			updateTransformationData();
		}

		
		
	};
	
	private SelectionListener editButtonListener = new SelectionListener () {

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			// Not needed
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			if (editButton.isEnabled()) {
				Object selection = ((StructuredSelection)transformationViewer.getSelection()).getFirstElement();
				if (selection instanceof ITransformation) {
					TransformationCreationWizard wizard = new TransformationCreationWizard((ITransformation)selection);
					WizardDialog dialog = new ExtendedWizardDialog(getShell(), wizard, CorePlugin.getImage(CorePlugin.TRANSFORMATION_ICON, CorePlugin.NEW_OVERLAY, CorePlugin.TOP_RIGHT));
					int result = dialog.open();
					
					if (result == Dialog.OK) {
						server.removeTransformation(((ITransformation)selection).getName());
						server.addTransformation(wizard.getCreatedTransformation());
					}
					updateTransformationData();
				}
			}
		}

		
		
	};
	
	private SelectionListener removeButtonListener = new SelectionListener () {

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			// Not needed
			
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			if (removeButton.isEnabled()) {
				Object selection = ((StructuredSelection)transformationViewer.getSelection()).getFirstElement();
				if (selection instanceof ITransformation) {
					server.removeTransformation(((ITransformation)selection).getName());
					updateTransformationData();
				}
			}
		}
		
	};
	
	private ISelectionChangedListener transformationListener = new ISelectionChangedListener() {
		
		@Override
		public void selectionChanged(SelectionChangedEvent event) {
			Object selection = ((StructuredSelection)transformationViewer.getSelection()).getFirstElement();
			if (selection instanceof ITransformation) {
				transformationViewer.getTree().setToolTipText(((ITransformation)selection).getDescription());
				if (!((ITransformation)selection).getResourceType().equals(ResourceType.NORMAL)) {
					removeButton.setEnabled(false);
					editButton.setEnabled(false);
				}
				else {
					removeButton.setEnabled(true);
					editButton.setEnabled(true);
				}
				
			}
			else {
				removeButton.setEnabled(false);
				editButton.setEnabled(false);
				transformationViewer.getTree().setToolTipText("");
			}
		}
	};
	
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// Only create an OK button
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite topPanel = (Composite)super.createDialogArea(parent);
		
		topPanel.setSize(600, 400);

		GridLayout layout = (GridLayout)topPanel.getLayout();
		layout.numColumns = 4;
		
		transformationViewer = new TreeViewer(topPanel, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		GridData transformationViewerData = new GridData(GridData.FILL_BOTH);
		transformationViewerData.horizontalSpan = 4;
		transformationViewer.getTree().setLayoutData(transformationViewerData);
		updateTransformationData();
		
		Label emptyLabel = new Label(topPanel, SWT.NONE);
		GridData emptyLabelData = new GridData(GridData.FILL_HORIZONTAL);
		emptyLabelData.horizontalSpan = 1;
		emptyLabel.setText("");
		emptyLabel.setLayoutData(emptyLabelData);
		
		createButton = new Button(topPanel, SWT.PUSH | SWT.CENTER);
		GridData createButtonData = new GridData(GridData.FILL_HORIZONTAL);
		createButtonData.horizontalSpan = 1;
		createButton.setLayoutData(createButtonData);
		createButton.setText("Create Transformation");
		
		editButton = new Button(topPanel, SWT.PUSH | SWT.CENTER);
		GridData editButtonData = new GridData(GridData.FILL_HORIZONTAL);
		editButtonData.horizontalSpan = 1;
		editButton.setLayoutData(editButtonData);
		editButton.setText("Edit Transformation");
		editButton.setEnabled(false);
		
		removeButton = new Button(topPanel, SWT.PUSH | SWT.CENTER);
		GridData removeButtonData = new GridData(GridData.FILL_HORIZONTAL);
		removeButtonData.horizontalSpan = 1;
		removeButton.setLayoutData(removeButtonData);
		removeButton.setText("Remove Transformation");
		removeButton.setEnabled(false);
		
		// Add Listeners
		createButton.addSelectionListener(createButtonListener);
		editButton.addSelectionListener(editButtonListener);
		removeButton.addSelectionListener(removeButtonListener);
		transformationViewer.addSelectionChangedListener(transformationListener);
		
		
		return topPanel;
	}

	/**
	 * Initializes the dialog.
	 *
	 *@param parentShell the parent shell.
	 */
	public TransformationManagementDialog(Shell parentShell) {
		super(parentShell);
		server = ModelTransformatorPlugin.getService();
	}
	
	/**
	 * Internal method used to update the data of the TreeViewer showing the constraints in the repository
	 */
	private void updateTransformationData () {
		Tree data = transformationViewer.getTree();
		data.removeAll();
		for (ITransformation transformation : server.getTransformationRepository()) {
			TreeItem item = new TreeItem(data, SWT.NONE);
			Image icon = ModelTransformatorPlugin.getService().getEngineImage(transformation.getEngine());
			if (transformation.getResourceType().equals(ResourceType.PROTECTED)) {
				icon = new OverlaidImageDescriptor(icon, CorePlugin.getImage(CorePlugin.LOCK_OVERLAY, null, 0), CorePlugin.BOTTOM_RIGHT).getImage();
			}
			else if (transformation.getResourceType().equals(ResourceType.NETWORKED)) {
				icon = new OverlaidImageDescriptor(icon, CorePlugin.getImage(CorePlugin.NET_OVERLAY, null, 0), CorePlugin.BOTTOM_RIGHT).getImage();
			}
			item.setText(transformation.getName());
			item.setImage(icon);
			item.setData(transformation);
		}
		transformationViewer.setSelection(TreeSelection.EMPTY);
	}

}
