/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/

package es.esi.gemde.modeltransformator.ui.actions;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.internal.Workbench;

import es.esi.gemde.core.resources.ExtendedWizardDialog;
import es.esi.gemde.modeltransformator.ModelTransformatorPlugin;
import es.esi.gemde.modeltransformator.exceptions.NoSuchEngineException;
import es.esi.gemde.modeltransformator.exceptions.TransformationEngineException;
import es.esi.gemde.modeltransformator.service.ITransformation;
import es.esi.gemde.modeltransformator.ui.wizards.TransformationCreationWizard;


/**
 * Action that implements the logic to run a transformation. 
 * It calls the ValidationCreationiWizard to define it beforehand.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
@SuppressWarnings("restriction")
public class RunOtherTransformationAction extends Action {

	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		ISelection selection = Workbench.getInstance().getActiveWorkbenchWindow().getSelectionService().getSelection();
		if (selection instanceof StructuredSelection && ((StructuredSelection)selection).getFirstElement() instanceof EObject) {
			// First we call the TransformationCreationWizard to create a transformation
			TransformationCreationWizard wizard = new TransformationCreationWizard();
			WizardDialog dialog = new ExtendedWizardDialog(new Shell(), wizard, ModelTransformatorPlugin.imageDescriptorFromPlugin(ModelTransformatorPlugin.PLUGIN_ID, "/icons/transformation.gif").createImage());
			int status = dialog.open();
			
			if (status != Dialog.OK) {
				MessageDialog.openError(new Shell(), "Couldn't run the transformation", "The transformation was not created correctly. Transformation process will be aborted.");
			}
			else {
				ITransformation createdTransform = wizard.getCreatedTransformation();
				
				// Once we have created the transformation we store it in the repository for further uses
				ModelTransformatorPlugin.getService().addTransformation(createdTransform);
				
				// Select the output path for the transformation
				DirectoryDialog dialog2 = new DirectoryDialog(new Shell());
				dialog2.setText("Select the output path");
				dialog2.setMessage("Select the directory where the outputs of the transformation will be placed");
				String outputPath = dialog2.open();
				
				if (outputPath != null) {
				
					// Lastly we run the current transformation on the selected target
					EObject rootTarget = (EObject)((StructuredSelection)selection).getFirstElement();
					List<EObject> targets = new Vector<EObject>();
					targets.add(rootTarget);
					
					IStatus result = null;
					try {
						result = ModelTransformatorPlugin.getService().executeTransformation(targets, createdTransform.getName(), createdTransform.getEngine(), outputPath);
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
						MessageDialog.openError(new Shell(), "Illegal Argument Exception", "The " + createdTransform.getEngine() + " engine returned a exception:\n" + e.getMessage());
					} catch (TransformationEngineException e) {
						e.printStackTrace();
						MessageDialog.openError(new Shell(), "Transformation Engine Exception", "The " + createdTransform.getEngine() + " engine returned a exception:\n" + e.getMessage());
					} catch (NoSuchEngineException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					switch (result.getSeverity()) {
					case IStatus.ERROR:
						MessageDialog.openError(new Shell(), "Transformation returned ERROR", createdTransform.getEngine() + " provided messages:\n" + result.getMessage());
						break;
					case IStatus.OK:
						MessageDialog.openInformation(new Shell(), "Transformation successfully parsed", createdTransform.getEngine() + " provided messages:\n" + result.getMessage());
						break;
					}
				}
			}
			
		}
	}

	/**
	 * Default constructor. It establishes "Other..." as text for this action.
	 *
	 */
	public RunOtherTransformationAction() {
		super();
		setText("Other...");
	}

}
