/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.modeltransformator.ui.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;

import es.esi.gemde.core.CorePlugin;
import es.esi.gemde.core.resources.ExtendedWizardDialog;
import es.esi.gemde.core.utils.GEMDEUtils;
import es.esi.gemde.modeltransformator.ModelTransformatorPlugin;
import es.esi.gemde.modeltransformator.ui.wizards.ExecuteTransformationWizard;


/**
 * Action that calls the Transformation Execution wizard
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class ExecuteTransformationAction implements IWorkbenchWindowActionDelegate {
	private IWorkbenchWindow window;
	/**
	 * The constructor.
	 */
	public ExecuteTransformationAction() {
	}

	/**
	 * The action has been activated. The argument of the
	 * method represents the 'real' action sitting
	 * in the workbench UI.
	 * @see IWorkbenchWindowActionDelegate#run
	 */
	public void run(IAction action) {
		if (ModelTransformatorPlugin.getService().getTransformationRepository() != null && ModelTransformatorPlugin.getService().getTransformationRepository().length > 0) {
			ExecuteTransformationWizard wizard = new ExecuteTransformationWizard();
			WizardDialog dialog = new ExtendedWizardDialog(window.getShell(), wizard, CorePlugin.getImage(CorePlugin.EXEC_TRANSFORMATION_ICON));
			
			int result = dialog.open();
			
			if (result == Dialog.OK) {
				GEMDEUtils.refreshWorkspace();
			}
		}
		else {
			MessageDialog.openError(window.getShell(), "No transformations registered", "There are no transformations currently registered in GEMDE. Transformation execution will be aborted.");
		}
	}

	/**
	 * Selection in the workbench has been changed. We 
	 * can change the state of the 'real' action here
	 * if we want, but this can only happen after 
	 * the delegate has been created.
	 * @see IWorkbenchWindowActionDelegate#selectionChanged
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

	/**
	 * We can use this method to dispose of any system
	 * resources we previously allocated.
	 * @see IWorkbenchWindowActionDelegate#dispose
	 */
	public void dispose() {
	}

	/**
	 * We will cache window object in order to
	 * be able to provide parent shell for the message dialog.
	 * @see IWorkbenchWindowActionDelegate#init
	 */
	public void init(IWorkbenchWindow window) {
		this.window = window;
	}
}