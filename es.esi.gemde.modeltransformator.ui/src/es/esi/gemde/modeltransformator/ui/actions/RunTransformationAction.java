/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/

package es.esi.gemde.modeltransformator.ui.actions;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.internal.Workbench;

import es.esi.gemde.modeltransformator.ModelTransformatorPlugin;
import es.esi.gemde.modeltransformator.exceptions.NoSuchEngineException;
import es.esi.gemde.modeltransformator.exceptions.TransformationEngineException;
import es.esi.gemde.modeltransformator.service.ITransformation;


/**
 * Action that runs a transformation selected from the context menu.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
@SuppressWarnings("restriction")
public class RunTransformationAction extends Action {

	private ITransformation transformation;
	
	/**
	 * Constructor including the validation to be executed.
	 *
	 *@param validation the selected batch validation.
	 */
	public RunTransformationAction (ITransformation transform) {
		super();
		setText("Run '" + transform.getName() + "'");
		this.transformation = transform;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		ISelection selection = Workbench.getInstance().getActiveWorkbenchWindow().getSelectionService().getSelection();
		if (selection instanceof StructuredSelection && ((StructuredSelection)selection).getFirstElement() instanceof EObject) {
			EObject rootTarget = (EObject)((StructuredSelection)selection).getFirstElement(); 
			
			DirectoryDialog dialog = new DirectoryDialog(new Shell());
			dialog.setText("Select the output path");
			dialog.setMessage("Select the directory where the outputs of the transformation will be placed");
			String outputPath = dialog.open();
			
			if (outputPath != null) {
				IStatus result = null;
				try {
					List<EObject> input = new Vector<EObject>();
					input.add(rootTarget);
					result = ModelTransformatorPlugin.getService().executeTransformation(input, transformation.getName(), transformation.getEngine(), outputPath);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
					MessageDialog.openError(new Shell(), "Illegal Argument Exception", "The " + transformation.getEngine() + " engine returned a exception:\n" + e.getMessage());
				} catch (TransformationEngineException e) {
					e.printStackTrace();
					MessageDialog.openError(new Shell(), "Transformation Engine Exception", "The " + transformation.getEngine() + " engine returned a exception:\n" + e.getMessage());
				} catch (NoSuchEngineException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				switch (result.getSeverity()) {
				case IStatus.ERROR:
					MessageDialog.openError(new Shell(), "Transformation returned ERROR", transformation.getEngine() + " provided messages:\n" + result.getMessage());
					break;
				case IStatus.OK:
					MessageDialog.openInformation(new Shell(), "Transformation successfully parsed", transformation.getEngine() + " provided messages:\n" + result.getMessage());
					break;
				}
			}
		}
	}
}

