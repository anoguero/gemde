/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/

package es.esi.gemde.modeltransformator.ui.resources;

import java.util.Vector;

import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.Separator;
import org.eclipse.ui.actions.CompoundContributionItem;

import es.esi.gemde.modeltransformator.ModelTransformatorPlugin;
import es.esi.gemde.modeltransformator.service.ITransformation;
import es.esi.gemde.modeltransformator.ui.actions.RunOtherTransformationAction;
import es.esi.gemde.modeltransformator.ui.actions.RunTransformationAction;


/**
 * A refinement of the {@link CompoundContributionItem} class to create the elements
 * of the of Run GEMDE Transformation menu in the main Eclipse popup menu. 
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class RunTransformationContributionItem extends CompoundContributionItem {


	/* (non-Javadoc)
	 * @see org.eclipse.ui.actions.CompoundContributionItem#getContributionItems()
	 */
	@Override
	protected IContributionItem[] getContributionItems() {
		Vector<IContributionItem> list = new Vector<IContributionItem>();
		
		ITransformation[] transforms = ModelTransformatorPlugin.getService().getTransformationRepository();
		
		for (ITransformation t : transforms) {
			list.add(new ActionContributionItem(new RunTransformationAction(t)));
		}
		
		if (list.size() > 0){ 
			list.add(new Separator("other"));
		}
		
		list.add(new ActionContributionItem(new RunOtherTransformationAction()));
		
		return list.toArray(new IContributionItem[0]);
	}

}
