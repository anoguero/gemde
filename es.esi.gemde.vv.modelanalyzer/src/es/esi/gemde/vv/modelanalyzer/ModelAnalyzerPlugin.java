/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import es.esi.gemde.vv.modelanalyzer.service.IModelAnalysisService;
import es.esi.gemde.vv.modelanalyzer.service.impl.ModelAnalysisService;

/**
 * The activator class controls the plug-in life cycle
 */
public class ModelAnalyzerPlugin extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "es.esi.gemde.vv.modelanalyzer";

	// The shared instance
	private static ModelAnalyzerPlugin plugin;
	
	// Shared instance of the service
	private static ModelAnalysisService service = new ModelAnalysisService();
	
	/**
	 * The constructor
	 */
	public ModelAnalyzerPlugin() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		// Delegate extension loading to the service instance
		service.loadExtensions();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static ModelAnalyzerPlugin getDefault() {
		return plugin;
	}

	/**
	 * Returns the shared service instance
	 *
	 * @return the service instance
	 */
	public static IModelAnalysisService getService() {
		return service;
	}

}
