/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer.exceptions;

/**
 * Exception thrown by {@link es.esi.gemde.vv.modelanalyzer.service.IModelAnalysisService} if the requested tool 
 * is not available in the registry.  
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 * @see es.esi.gemde.vv.modelanalyzer.service.IModelAnalysisService#runTool(String, String, org.eclipse.emf.ecore.EObject[])
 *
 */
@SuppressWarnings("serial")
public class NoSuchToolException extends Exception {

	/**
	 * Constructor of the class
	 *
	 *@param message a message to be attached to the exception.
	 */
	public NoSuchToolException(String message) {
		super(message);
	}

}
