/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer.exceptions;

/**
 * Exception thrown by {@link es.esi.gemde.vv.modelanalyzer.service.IAnalysisTool} methods if the requested operation 
 * is not available for the given tool implementation.  
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 * @see es.esi.gemde.vv.modelanalyzer.service.IAnalysisTool#runOperation(String, org.eclipse.emf.ecore.EObject[])
 * @see es.esi.gemde.vv.modelanalyzer.service.IAnalysisTool#getRequiredInputList(String)
 * @see es.esi.gemde.vv.modelanalyzer.service.IAnalysisTool#checkInputs(String, org.eclipse.emf.ecore.EObject[])
 *
 */
@SuppressWarnings("serial")
public class NoSuchOperationException extends Exception {

	/**
	 * Constructor of the class
	 *
	 *@param message a message to be attached to the exception.
	 */
	public NoSuchOperationException(String message) {
		super(message);
	}

}
