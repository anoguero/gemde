/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer.service.impl;

import java.util.List;
import java.util.Vector;

import es.esi.gemde.vv.modelanalyzer.service.IResultRecord;
import es.esi.gemde.vv.modelanalyzer.service.IResultType;

/**
 * Implementation of the {@link IResultType} interface. 
 * It is not intended to be used outside this plug-in.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class ResultType implements IResultType {

	private List<IResultRecord> records;
	private String typeName;
	
	/**
	 * Constructor of the class. 
	 * Not intended to be used outside this plug-in.
	 *
	 *@param type the name/description of the constructed type.
	 */
	public ResultType (String type) {
		typeName = type;
		records = new Vector<IResultRecord>();
	}
	
	/**
	 * This method adds a new record instance to this type provided it is not null.
	 * Not intended to be used outside this plug-in.
	 * 
	 * @param record the new record to be added.
	 */
	public void addRecord(IResultRecord record) {
		if (record != null) {
			records.add(record);
		}
	}
	
	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IResultType#getRecords()
	 */
	@Override
	public IResultRecord[] getRecords() {
		return records.toArray(new IResultRecord[0]);
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IResultType#getType()
	 */
	@Override
	public String getType() {
		return typeName;
	}

}
