/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer.service.impl;

import es.esi.gemde.vv.modelanalyzer.service.IResultRecord;

/**
 * Implementation of the {@link IResultRecord} interface.
 * Not intended to be used outside this plug-in.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class ResultRecord implements IResultRecord {

	private String target;
	private Object value;
	
	/**
	 * Constructor of the class. Not intended to be used outside this plug-in.
	 *
	 *@param target the name of the target of this record, that is, the name of the variable represented by value.
	 *@param value the value of this record as an Object.
	 */
	public ResultRecord (String target, Object value) {
		this.target = target;
		this.value = value;
	}
	
	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IResultRecord#getTarget()
	 */
	@Override
	public String getTarget() {
		return target;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IResultRecord#getValue()
	 */
	@Override
	public Object getValue() {
		return value;
	}

}
