/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer.service;

import java.util.List;

import es.esi.gemde.vv.modelanalyzer.exceptions.ToolException;

/**
 * Interface that represents a single operation of a analysis tool.
 * It is intended to be implemented by contributing plug-ins with the
 * required logic to execute them.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 * @see AbstractToolOperation
 *
 */
public interface IToolOperation {
	
	/**
	 * Returns the name of this operation.
	 * 
	 * @return the name of the operation.
	 */
	public String getName();
	
	/**
	 * Returns the textual description of this tool. Useful for tooltip implementations.
	 * 
	 * @return the textual description of the tool.
	 */
	public String getDescription();
	
	/**
	 * Returns the ordered list containing the Class objects of the required inputs for the operation.
	 * 
	 * @return the ordered list with the required Class objects.
	 */
	public List<Class<?>> getRequiredInputList ();
	
	/**
	 * Returns the ordered list containing the names of the required input parameters. 
	 * These names will be used the UI layer of GEMDE.
	 * 
	 * @return the ordered list with the names of the required inputs. 
	 */
	public List<String> getRequiredInputNames ();
	
	/**
	 * Checks whether the provided inputs list is adequate for the current operation.
	 * 
	 * @param inputs an array containing the inputs list to be checked
	 * @return true is the given input list is suitable for the current operation. False otherwise.
	 */
	public boolean checkInputs(Object [] inputs);
	

	/**
	 * Runs this operation in the given context.
	 * 
	 * @return the results of the analysis as an implementation of {@link IAnalysisResult}.
	 * @throws ToolException if an error occurs during the execution of the tool for the current operation.
	 */
	public IAnalysisResult runOperation(Object [] inputs) throws ToolException;

}
