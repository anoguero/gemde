/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer.service;

import java.util.List;
import java.util.Vector;

/**
 * Partial implementation of the {@link IToolOperation} interface.
 * It is intended to be sub-classed by tool contributing plug-ins 
 * to reduce implementation effort.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 * @see IToolOperation
 *
 */
public abstract class AbstractToolOperation implements IToolOperation {
	
	private String name;
	private String description;
	private List<Class<?>> requiredInputTypes;
	private List<String> requiredInputNames;
	
	
	/**
	 * Constructor of the class defining a name.
	 *
	 *@param name the name of the tool.
	 *@param description the textual description of the tool.
	 */
	protected AbstractToolOperation(String name, String description) {
		this.name = name;
		this.description = description;
		this.requiredInputTypes = new Vector<Class<?>>();
		this.requiredInputNames = new Vector<String>();
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IToolOperation#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IToolOperation#checkInputs(org.eclipse.emf.ecore.EObject[])
	 */
	@Override
	public boolean checkInputs(Object[] inputs) {
		if (inputs.length != requiredInputTypes.size()) {
			return false;
		}
		for (int i = 0; i < inputs.length; i++) {
			if (!requiredInputTypes.get(i).isInstance(inputs[i])) {
				return false;
			}
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IToolOperation#getRequiredInputNames()
	 */
	@Override
	public List<String> getRequiredInputNames() {
		return requiredInputNames;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IToolOperation#getRequiredInputList()
	 */
	@Override
	public List<Class<?>> getRequiredInputList() {
		return requiredInputTypes;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IToolOperation#getDescription()
	 */
	@Override
	public String getDescription() {
		return description;
	}
	
	protected void addInput(String name, Class<?> type) {
		requiredInputNames.add(name);
		requiredInputTypes.add(type);
	}
	
}
