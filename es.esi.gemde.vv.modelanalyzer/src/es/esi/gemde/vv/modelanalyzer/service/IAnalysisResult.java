/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer.service;

/**
 * Interface that represents the results of an analysis tool as a set
 * of {@link IResultType}s.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public interface IAnalysisResult {
	
	/**
	 * Returns all the types contained in this results instance.
	 * 
	 * @return an array of {@link IResultType} containing the results. It might be empty, but must not be null.
	 */
	IResultType [] getResults();
}
