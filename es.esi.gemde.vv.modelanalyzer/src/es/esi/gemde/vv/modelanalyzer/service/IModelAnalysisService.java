/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer.service;

import es.esi.gemde.vv.modelanalyzer.exceptions.NoSuchOperationException;
import es.esi.gemde.vv.modelanalyzer.exceptions.NoSuchToolException;
import es.esi.gemde.vv.modelanalyzer.exceptions.ToolException;

/**
 * Specification of the interface of the GEMDE Model Analysis Service.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public interface IModelAnalysisService {

	/**
	 * Returns the names of the tools in the repository.
	 * 
	 * @return an array with the names of the tools in the repository.
	 */
	public String [] getToolRepositoryNames();
	
	/**
	 * Returns the tools in the repository.
	 * 
	 * @return an array with the tools in the repository.
	 */
	public IAnalysisTool [] getToolRepository();
	
	/**
	 * Returns the tools in the repository that match the given category.
	 * 
	 * @param category the given category name.
	 * @return an array with the tools in the repository that match the category.
	 */
	public IAnalysisTool [] getToolsInCategory(String category);
	
	/**
	 * Returns the tool implementation that matches the given name or null.
	 * 
	 * @param toolName the name of the tool.
	 * @return the implementation of the tool or null.
	 */
	public IAnalysisTool getToolByName (String toolName);
	
	/**
	 * Returns all the operations available for a tool.
	 * 
	 * @param toolName the name of the tool.
	 * @return an array containing the operations of the given tool.
	 */
	public IToolOperation [] getToolOperations(String toolName) throws NoSuchToolException;
	
	/**
	 * Returns the names of all the operations available for a tool.
	 * 
	 * @param toolName the name of the tool.
	 * @return an array containing the names of the operations available for the given tool.
	 */
	public String [] getToolOperationNames(String toolName) throws NoSuchToolException;
	
	/**
	 * Returns the names of all the analysis tool categories defined in GEMDE.
	 * 
	 * @return the array with the categories as strings.
	 * @deprecated use @link IModelAnalysisService#getCategoriesWithTool() instead.
	 */
	public String [] getCategories();
	
	/**
	 * Returns the names of all the analysis tool categories for which
	 * at least one tool has been contributed.
	 * 
	 * @return the array with the names of the categories as strings.
	 */
	public String [] getCategoriesWithTool();
	
	/**
	 * This method instructs the model analysis service to run a tool operation on the given inputs. 
	 * 
	 * @param toolName the name of the tool to be invoked.
	 * @param operation the name of the tool operation that will be executed.
	 * @param inputs an array containing the inputs for the operation execution.
	 * @return the results of the analysis as an implementation of {@link IAnalysisResult}. 
	 * @throws NoSuchToolException if the given tool name is not present in the tool registry.
	 * @throws NoSuchOperationException if the given operation name if not available for the given tool.
	 * @throws ToolException if an exception occurred during the execution of the analysis tool.
	 */
	public IAnalysisResult runTool(String toolName, String operation, Object [] inputs) throws NoSuchToolException, NoSuchOperationException, ToolException;
	
}
