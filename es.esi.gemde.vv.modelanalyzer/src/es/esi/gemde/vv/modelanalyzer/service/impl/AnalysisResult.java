/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer.service.impl;

import java.util.List;
import java.util.Vector;

import es.esi.gemde.vv.modelanalyzer.service.IAnalysisResult;
import es.esi.gemde.vv.modelanalyzer.service.IResultType;

/**
 * Implementation of the {@link IAnalysisResult} interface.
 * This class is not intended to be used outside this plug-in.
 * In order to create instances of this class, the {@link es.esi.gemde.vv.modelanalyzer.service.ResultFactory} should 
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class AnalysisResult implements IAnalysisResult {

	private List<IResultType> types;
	
	/**
	 * Constructor of the class. Not intended to be used outside this plug-in.
	 *
	 */
	public AnalysisResult () {
		types = new Vector<IResultType>();
	}
	
	/**
	 * This method adds a result type to the result type list of this result if it is not null.
	 * It is intended to be used inside this plug-in only.
	 * 
	 * @param type the result type to be added to the list.
	 */
	public void addResultType (IResultType type) {
		if (type != null) {
			types.add(type);
		}
	}
	
	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IAnalysisResult#getResults()
	 */
	@Override
	public IResultType[] getResults() {
		return types.toArray(new IResultType[0]);
	}

}
