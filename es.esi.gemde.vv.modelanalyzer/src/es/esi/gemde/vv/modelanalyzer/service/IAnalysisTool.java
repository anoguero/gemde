/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer.service;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import es.esi.gemde.vv.modelanalyzer.exceptions.NoSuchOperationException;
import es.esi.gemde.vv.modelanalyzer.exceptions.ToolException;

/**
 * Interface defining a model analysis tool in GEMDE.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public interface IAnalysisTool {

	/**
	 * Returns the name of this tool.
	 * 
	 * @return the name of the tool.
	 */
	public String getName();
	
	/**
	 * Returns the operations available for this tool.
	 * 
	 * @return an array containing the operation implementations.
	 */
	public IToolOperation [] getOperations();
	
	/**
	 * Returns the implementation of an operation that matches the given name.
	 * 
	 * @param opName the name of the operation.
	 * @return the operation implementation that matches the given operation name or null.
	 */
	public IToolOperation getOperationByName(String opName);
	
	/**
	 * Returns the category of this tool.
	 * 
	 * @return the category of the tool.
	 */
	public String getCategory();
	
	/**
	 * Return the methods for which an adapter has been contributed.
	 * 
	 * @return the instances of the methods for which an adapter has been contributed as an array.
	 */
	//public IMethod [] getAdapatableMethods();
	
	/**
	 * Registers a new adapter for the tool.
	 * 
	 * @param adapter the new adapter implementation to be registered.
	 */
	//public void registerAdapter(IMethodologyAdapter adapter);
	
	/**
	 * Returns the ordered list containing the Class objects of the required inputs for the given operation name.
	 * 
	 * @param operation the name of the tool to be run.
	 * @return the ordered list with the required Class objects.
	 * @throws NoSuchOperationException if the requested operation is not available for the current tool.
	 */
	public List<Class<?>> getRequiredInputList (String operation) throws NoSuchOperationException;

	/**
	 * Checks whether the selected operation of the current tool can be run with the given input list.
	 * 
	 * @param operation the name of the operation to be checked.
	 * @param inputs an array containing the inputs list to be checked.
	 * @return true if the input list is correct for the given operation. False otherwise.
	 * @throws NoSuchOperationException if the requested operation is not available for the current tool.
	 */
	public boolean checkInputs (String operation, EObject [] inputs) throws NoSuchOperationException;

	/**
	 * Runs the selected operation of the current tool in the given context and returns the results of the analysis.
	 * 
	 * @param operation the name of the operation to be run.
	 * @param inputs an array of EObjects containing the inputs for the given operation. 
	 * @return the results of the analysis.
	 * @throws NoSuchOperationException if the requested operation is not available for the current tool.
	 * @throws ToolException if an exception should occur during the invocation of the operation.
	 */
	public IAnalysisResult runOperation(String operation, Object[] inputs) throws NoSuchOperationException, ToolException;
	
}
