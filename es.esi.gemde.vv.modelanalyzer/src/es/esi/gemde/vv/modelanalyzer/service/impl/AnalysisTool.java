/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer.service.impl;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

import es.esi.gemde.vv.modelanalyzer.exceptions.NoSuchOperationException;
import es.esi.gemde.vv.modelanalyzer.exceptions.ToolException;
import es.esi.gemde.vv.modelanalyzer.service.IAnalysisResult;
import es.esi.gemde.vv.modelanalyzer.service.IAnalysisTool;
import es.esi.gemde.vv.modelanalyzer.service.IToolOperation;

/**
 * Implementation of the {@link IAnalysisTool} interface.
 * It is intended to be used internally by the ModelAnalysisService of GEMDE.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class AnalysisTool implements IAnalysisTool {

	private String name;
	private String category;
	private List<IToolOperation> ownedOperations;
	//private List<IMethodologyAdapter> adapters;
	
	
	/**
	 * Constructor for the class
	 *
	 *@param name the name of the tool as a String.
	 *@param category the category of the tool as a {@link AnalysisToolCategoryKind}.
	 *@param operations a list containing the implementations of the operations provided by this tool.
	 */
	public AnalysisTool(String name, String category, List<IToolOperation> operations) {
		this.name = name;
		this.category = category;
		ownedOperations = operations;
	}
	
	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IAnalysisTool#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IAnalysisTool#getAdapatableMethods()
	 */
	/*@Override
	public IMethod[] getAdapatableMethods() {
		Vector<IMethod> methods = new Vector<IMethod>();
		
		for (IMethodologyAdapter adapter : adapters) {
			methods.add(adapter.getMethod());
		}
		
		return methods.toArray(new IMethod[0]);
	}*/

	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IAnalysisTool#getCategory()
	 */
	@Override
	public String getCategory() {
		return category;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IAnalysisTool#getOperations()
	 */
	@Override
	public IToolOperation[] getOperations() {
		return ownedOperations.toArray(new IToolOperation[0]);
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IAnalysisTool#getOperationByName(java.lang.String)
	 */
	@Override
	public IToolOperation getOperationByName(String opName) {
		return getOperation(opName);
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IAnalysisTool#registerAdapter(es.esi.gemde.vv.modelanalyzer.service.IMethodologyAdapter)
	 */
	/*@Override
	public void registerAdapter(IMethodologyAdapter adapter) {
		if (!adapters.contains(adapter)) {
			adapters.add(adapter);
		}
	}*/

	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IAnalysisTool#checkInputs(java.lang.String, org.eclipse.emf.ecore.EObject[])
	 */
	@Override
	public boolean checkInputs(String operation, EObject[] inputs) throws NoSuchOperationException {
		IToolOperation op = getOperation(operation);
		if (op == null) {
			throw new NoSuchOperationException("Operation '" + operation + "' is not available in tool '" + name);
		}
		else {
			return op.checkInputs(inputs);
		}
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IAnalysisTool#getRequiredInputList(java.lang.String)
	 */
	@Override
	public List<Class<?>> getRequiredInputList(String operation) throws NoSuchOperationException {
		IToolOperation op = getOperation(operation);
		if (op == null) {
			throw new NoSuchOperationException("Operation '" + operation + "' is not available in tool '" + name);
		}
		else {
			return op.getRequiredInputList();
		}
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.service.IAnalysisTool#runOperation(java.lang.String, org.eclipse.emf.ecore.EObject[])
	 */
	@Override
	public IAnalysisResult runOperation(String operation, Object[] inputs) throws NoSuchOperationException, ToolException {
		IToolOperation op = getOperation(operation);
		if (op == null) {
			throw new NoSuchOperationException("Operation '" + operation + "' is not available in tool '" + name);
		}
		else {
			return op.runOperation(inputs);
		}
	}

	/**
	 * Internal method that looks for the operation object that matches a given name.
	 * 
	 * @param operation the operation name to find.
	 * @return the operation implementation object.
	 */
	private IToolOperation getOperation(String operation) {
		IToolOperation op = null;
		for (IToolOperation current : ownedOperations) {
			if (current.getName() != null && current.getName().equals(operation)) {
				op = current;
			}
		}
		return op;
	}

}
