/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer.service;

import java.util.HashMap;

import es.esi.gemde.vv.modelanalyzer.service.impl.AnalysisResult;
import es.esi.gemde.vv.modelanalyzer.service.impl.ResultRecord;
import es.esi.gemde.vv.modelanalyzer.service.impl.ResultType;

/**
 * Factory class to create {@link IAnalysisResult} instances.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class ResultFactory {

	/**
	 * Factory method that creates an {@link IAnalysisResult} from a map of records.
	 * The required input is a map of String (the name of the type) and another map 
	 * containing the records for that type.
	 * The map of records is a map between the name of the target for that record 
	 * and its associated value as an Object. 
	 * 
	 * @param records a map from result type to records, as maps of targets to values.
	 * @return the created {@link IAnalysisResult} instance.
	 */
	public static IAnalysisResult createResult(HashMap<String, HashMap<String, Object>> records) {
		AnalysisResult result = new AnalysisResult();
		
		if (records == null) {
			return null;
		}
		
		for (String type : records.keySet()) {
			ResultType resultType = new ResultType(type);
			for (String target : records.get(type).keySet()) {
				ResultRecord rec = new ResultRecord(target, records.get(type).get(target));
				resultType.addRecord(rec);
			}
			result.addResultType(resultType);
		}
		
		return result;
	}
	
}
