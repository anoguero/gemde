package es.esi.gemde.network;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import com.tecnalia.registry.client.ui.EclipseRegistryClient;

public class GemdeNetworkActivator implements BundleActivator {

	private static BundleContext context;
	private static GemdeRegistryObserver eclipseRegistryObserver;
	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		GemdeNetworkActivator.context = bundleContext;
			eclipseRegistryObserver=new GemdeRegistryObserver();
			EclipseRegistryClient.getInstance().addObserver(eclipseRegistryObserver);
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		GemdeNetworkActivator.context = null;
		EclipseRegistryClient.getInstance().removeObserver(eclipseRegistryObserver);

	}

}
