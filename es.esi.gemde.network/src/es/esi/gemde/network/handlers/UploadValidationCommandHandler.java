/**
 * 
 */
package es.esi.gemde.network.handlers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;

import com.tecnalia.registry.client.ui.EclipseRegistryClient;
import com.tecnalia.registry.client.ui.RegistryClientUIActivator;
import com.tecnalia.registry.client.ui.preferences.PreferencesRegistry;
import com.tecnalia.registry.resource.Resource;
import com.tecnalia.registry.resource.gemde.ResourceConstraint;
import com.tecnalia.registry.resource.gemde.ResourceValidation;
import com.tecnalia.util.ui.XEclipseUI;

import es.esi.gemde.modelvalidator.ModelValidatorPlugin;
import es.esi.gemde.modelvalidator.service.IBatchValidation;
import es.esi.gemde.modelvalidator.service.IModelValidatorConstraint;
import es.esi.gemde.modelvalidator.ui.dialogs.SelectGEMDEValidationDialog;

/**
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 * @author Angel Rego (angel.rego@tecnalia.com)
 *
 */
public class UploadValidationCommandHandler extends AbstractHandler {
	
	private static final String VALIDATION_ID_PARAM_ID = "es.esi.gemde.network.commandparameters.validation";
	private static final String LITERAL_UPLOAD_VALIDATION="Upload validation"; 

	/* (non-Javadoc)
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		// Get the selected validation
		String validationID = event.getParameter(VALIDATION_ID_PARAM_ID);
		IBatchValidation validation = null;
		
		if (validationID == null || validationID == "") {
			SelectGEMDEValidationDialog d = new SelectGEMDEValidationDialog(new Shell());
			if (d.open() == Window.OK) {
				validation = d.getSelectedValidation();
			}
			else {
				return null;
			}
		}
		else {
			validation = ModelValidatorPlugin.getServer().getBatchValidation(validationID);	
		}
		
		if (validation == null) {
			throw new ExecutionException("Provided validation is not valid. Validation couldn't be found in the Registry.");
		}
		
		// Perform upload to the Registry
		try {
			upload(validation);
			XEclipseUI.messageBoxInfo(LITERAL_UPLOAD_VALIDATION, "The validation '"+validation.getName()+"' was succesfully uploaded to the Registry Server");
		} catch (Exception e) {
			String message=LITERAL_UPLOAD_VALIDATION+". "+validation.getName()+" failed. "+e.getMessage();
			XEclipseUI.messageBoxError(LITERAL_UPLOAD_VALIDATION, "The validation '"+validation.getName()+"' was succesfully uploaded to the Registry Server");
			throw new ExecutionException(message,e);
		}
		return null;
	}
	
   
	/**
	 * Upload a GEMDE Validation to the RegistryServer.
	 * @see com.tecnalia.registry.RegistryClient
	 * @param validation
	 * @throws Exception
	 */
	private void upload(IBatchValidation validation) throws Exception{
		//get user-password
		String user=RegistryClientUIActivator.getDefault().getPreferenceStore().getString(PreferencesRegistry.KEY_USER_REGISTRY);
		String password=RegistryClientUIActivator.getDefault().getPreferenceStore().getString(PreferencesRegistry.KEY_PASWORD_REGISTRY);
		//load Validation into Resource object
		Map<String,Resource> mapResources=new HashMap<String,Resource>();
		ResourceValidation resourceValidation=new ResourceValidation();
		resourceValidation.name=validation.getName();
		resourceValidation.description=validation.getDescription();
		mapResources.put(resourceValidation.id, resourceValidation);
		for(IModelValidatorConstraint constraint:validation.getSelectedConstraints()){
			ResourceConstraint resourceConstraint=new ResourceConstraint();
			resourceConstraint.name=constraint.getName();
			resourceConstraint.description=constraint.getDescription();
			resourceConstraint.category=constraint.getCategory();
			resourceConstraint.severity=Integer.toString(constraint.getSeverity());
			resourceConstraint.body=constraint.getBody();
			resourceValidation.constraints.add(resourceConstraint.id);
			mapResources.put(resourceConstraint.id, resourceConstraint);
		}
		//perform upload
		EclipseRegistryClient.getInstance().upload(user, password, resourceValidation.id, mapResources, new ArrayList<String>());
	}
	
}
