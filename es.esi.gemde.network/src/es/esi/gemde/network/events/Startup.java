package es.esi.gemde.network.events;
import org.eclipse.ui.IStartup;


public class Startup implements IStartup {

	/**
	 * Start plugin, no implementation is required sinc the GemdeNetworkActivator.start() will be called
	 */
	@Override
	public void earlyStartup() {
	}

}
