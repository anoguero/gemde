/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.modelingassistant.ModelingAssistantProvider;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;

import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CallWizardCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ComplexCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConditionalSubTaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CustomCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.GEMDEMethodEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchTransformationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchValidationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.OpenEditorCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.PhaseEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.PhasePhaseTasksCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.RepeatedSubTaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFileCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFolderCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectMetamodelCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromEObjectCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromFileCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectTransformationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectValidationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SubTaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.TaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.TaskTaskSubTasksCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.part.Messages;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodDiagramEditorPlugin;

/**
 * @generated
 */
public class MethodModelingAssistantProvider extends ModelingAssistantProvider {

	/**
	 * @generated
	 */
	public List getTypesForPopupBar(IAdaptable host) {
		IGraphicalEditPart editPart = (IGraphicalEditPart) host
				.getAdapter(IGraphicalEditPart.class);
		if (editPart instanceof GEMDEMethodEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(15);
			types.add(MethodElementTypes.Phase_2001);
			types.add(MethodElementTypes.CustomCommand_2002);
			types.add(MethodElementTypes.CallWizardCommand_2003);
			types.add(MethodElementTypes.SelectFileCommand_2004);
			types.add(MethodElementTypes.SelectFolderCommand_2005);
			types.add(MethodElementTypes.SelectMetamodelCommand_2006);
			types.add(MethodElementTypes.SelectModelElementFromFileCommand_2007);
			types.add(MethodElementTypes.SelectModelElementFromEObjectCommand_2008);
			types.add(MethodElementTypes.SelectValidationCommand_2009);
			types.add(MethodElementTypes.SelectTransformationCommand_2010);
			types.add(MethodElementTypes.LaunchValidationCommand_2011);
			types.add(MethodElementTypes.LaunchTransformationCommand_2012);
			types.add(MethodElementTypes.OpenEditorCommand_2013);
			types.add(MethodElementTypes.MethodVariable_2015);
			types.add(MethodElementTypes.ComplexCommand_2014);
			return types;
		}
		if (editPart instanceof CustomCommandEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(2);
			types.add(MethodElementTypes.VariableCommandParameter_3005);
			types.add(MethodElementTypes.ConstantCommandParameter_3006);
			return types;
		}
		if (editPart instanceof CallWizardCommandEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(2);
			types.add(MethodElementTypes.VariableCommandParameter_3005);
			types.add(MethodElementTypes.ConstantCommandParameter_3006);
			return types;
		}
		if (editPart instanceof SelectFileCommandEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(2);
			types.add(MethodElementTypes.VariableCommandParameter_3005);
			types.add(MethodElementTypes.ConstantCommandParameter_3006);
			return types;
		}
		if (editPart instanceof SelectFolderCommandEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(2);
			types.add(MethodElementTypes.VariableCommandParameter_3005);
			types.add(MethodElementTypes.ConstantCommandParameter_3006);
			return types;
		}
		if (editPart instanceof SelectMetamodelCommandEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(2);
			types.add(MethodElementTypes.VariableCommandParameter_3005);
			types.add(MethodElementTypes.ConstantCommandParameter_3006);
			return types;
		}
		if (editPart instanceof SelectModelElementFromFileCommandEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(2);
			types.add(MethodElementTypes.VariableCommandParameter_3005);
			types.add(MethodElementTypes.ConstantCommandParameter_3006);
			return types;
		}
		if (editPart instanceof SelectModelElementFromEObjectCommandEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(2);
			types.add(MethodElementTypes.VariableCommandParameter_3005);
			types.add(MethodElementTypes.ConstantCommandParameter_3006);
			return types;
		}
		if (editPart instanceof SelectValidationCommandEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(2);
			types.add(MethodElementTypes.VariableCommandParameter_3005);
			types.add(MethodElementTypes.ConstantCommandParameter_3006);
			return types;
		}
		if (editPart instanceof SelectTransformationCommandEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(2);
			types.add(MethodElementTypes.VariableCommandParameter_3005);
			types.add(MethodElementTypes.ConstantCommandParameter_3006);
			return types;
		}
		if (editPart instanceof LaunchValidationCommandEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(2);
			types.add(MethodElementTypes.VariableCommandParameter_3005);
			types.add(MethodElementTypes.ConstantCommandParameter_3006);
			return types;
		}
		if (editPart instanceof LaunchTransformationCommandEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(2);
			types.add(MethodElementTypes.VariableCommandParameter_3005);
			types.add(MethodElementTypes.ConstantCommandParameter_3006);
			return types;
		}
		if (editPart instanceof OpenEditorCommandEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(2);
			types.add(MethodElementTypes.VariableCommandParameter_3005);
			types.add(MethodElementTypes.ConstantCommandParameter_3006);
			return types;
		}
		if (editPart instanceof PhasePhaseTasksCompartmentEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(1);
			types.add(MethodElementTypes.Task_3001);
			return types;
		}
		if (editPart instanceof TaskTaskSubTasksCompartmentEditPart) {
			ArrayList<IElementType> types = new ArrayList<IElementType>(3);
			types.add(MethodElementTypes.SubTask_3002);
			types.add(MethodElementTypes.ConditionalSubTask_3003);
			types.add(MethodElementTypes.RepeatedSubTask_3004);
			return types;
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getRelTypesOnSource(IAdaptable source) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof PhaseEditPart) {
			return ((PhaseEditPart) sourceEditPart).getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof ComplexCommandEditPart) {
			return ((ComplexCommandEditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof TaskEditPart) {
			return ((TaskEditPart) sourceEditPart).getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof SubTaskEditPart) {
			return ((SubTaskEditPart) sourceEditPart).getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof ConditionalSubTaskEditPart) {
			return ((ConditionalSubTaskEditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof RepeatedSubTaskEditPart) {
			return ((RepeatedSubTaskEditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getRelTypesOnTarget(IAdaptable target) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		if (targetEditPart instanceof PhaseEditPart) {
			return ((PhaseEditPart) targetEditPart).getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof CustomCommandEditPart) {
			return ((CustomCommandEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof CallWizardCommandEditPart) {
			return ((CallWizardCommandEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof SelectFileCommandEditPart) {
			return ((SelectFileCommandEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof SelectFolderCommandEditPart) {
			return ((SelectFolderCommandEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof SelectMetamodelCommandEditPart) {
			return ((SelectMetamodelCommandEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof SelectModelElementFromFileCommandEditPart) {
			return ((SelectModelElementFromFileCommandEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof SelectModelElementFromEObjectCommandEditPart) {
			return ((SelectModelElementFromEObjectCommandEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof SelectValidationCommandEditPart) {
			return ((SelectValidationCommandEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof SelectTransformationCommandEditPart) {
			return ((SelectTransformationCommandEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof LaunchValidationCommandEditPart) {
			return ((LaunchValidationCommandEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof LaunchTransformationCommandEditPart) {
			return ((LaunchTransformationCommandEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof OpenEditorCommandEditPart) {
			return ((OpenEditorCommandEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof TaskEditPart) {
			return ((TaskEditPart) targetEditPart).getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof SubTaskEditPart) {
			return ((SubTaskEditPart) targetEditPart).getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof ConditionalSubTaskEditPart) {
			return ((ConditionalSubTaskEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof RepeatedSubTaskEditPart) {
			return ((RepeatedSubTaskEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getRelTypesOnSourceAndTarget(IAdaptable source,
			IAdaptable target) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof PhaseEditPart) {
			return ((PhaseEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof ComplexCommandEditPart) {
			return ((ComplexCommandEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof TaskEditPart) {
			return ((TaskEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof SubTaskEditPart) {
			return ((SubTaskEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof ConditionalSubTaskEditPart) {
			return ((ConditionalSubTaskEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof RepeatedSubTaskEditPart) {
			return ((RepeatedSubTaskEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getTypesForSource(IAdaptable target,
			IElementType relationshipType) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		if (targetEditPart instanceof PhaseEditPart) {
			return ((PhaseEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof CustomCommandEditPart) {
			return ((CustomCommandEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof CallWizardCommandEditPart) {
			return ((CallWizardCommandEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof SelectFileCommandEditPart) {
			return ((SelectFileCommandEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof SelectFolderCommandEditPart) {
			return ((SelectFolderCommandEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof SelectMetamodelCommandEditPart) {
			return ((SelectMetamodelCommandEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof SelectModelElementFromFileCommandEditPart) {
			return ((SelectModelElementFromFileCommandEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof SelectModelElementFromEObjectCommandEditPart) {
			return ((SelectModelElementFromEObjectCommandEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof SelectValidationCommandEditPart) {
			return ((SelectValidationCommandEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof SelectTransformationCommandEditPart) {
			return ((SelectTransformationCommandEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof LaunchValidationCommandEditPart) {
			return ((LaunchValidationCommandEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof LaunchTransformationCommandEditPart) {
			return ((LaunchTransformationCommandEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof OpenEditorCommandEditPart) {
			return ((OpenEditorCommandEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof TaskEditPart) {
			return ((TaskEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof SubTaskEditPart) {
			return ((SubTaskEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof ConditionalSubTaskEditPart) {
			return ((ConditionalSubTaskEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof RepeatedSubTaskEditPart) {
			return ((RepeatedSubTaskEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getTypesForTarget(IAdaptable source,
			IElementType relationshipType) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof PhaseEditPart) {
			return ((PhaseEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof ComplexCommandEditPart) {
			return ((ComplexCommandEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof TaskEditPart) {
			return ((TaskEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof SubTaskEditPart) {
			return ((SubTaskEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof ConditionalSubTaskEditPart) {
			return ((ConditionalSubTaskEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof RepeatedSubTaskEditPart) {
			return ((RepeatedSubTaskEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public EObject selectExistingElementForSource(IAdaptable target,
			IElementType relationshipType) {
		return selectExistingElement(target,
				getTypesForSource(target, relationshipType));
	}

	/**
	 * @generated
	 */
	public EObject selectExistingElementForTarget(IAdaptable source,
			IElementType relationshipType) {
		return selectExistingElement(source,
				getTypesForTarget(source, relationshipType));
	}

	/**
	 * @generated
	 */
	protected EObject selectExistingElement(IAdaptable host, Collection types) {
		if (types.isEmpty()) {
			return null;
		}
		IGraphicalEditPart editPart = (IGraphicalEditPart) host
				.getAdapter(IGraphicalEditPart.class);
		if (editPart == null) {
			return null;
		}
		Diagram diagram = (Diagram) editPart.getRoot().getContents().getModel();
		HashSet<EObject> elements = new HashSet<EObject>();
		for (Iterator<EObject> it = diagram.getElement().eAllContents(); it
				.hasNext();) {
			EObject element = it.next();
			if (isApplicableElement(element, types)) {
				elements.add(element);
			}
		}
		if (elements.isEmpty()) {
			return null;
		}
		return selectElement((EObject[]) elements.toArray(new EObject[elements
				.size()]));
	}

	/**
	 * @generated
	 */
	protected boolean isApplicableElement(EObject element, Collection types) {
		IElementType type = ElementTypeRegistry.getInstance().getElementType(
				element);
		return types.contains(type);
	}

	/**
	 * @generated
	 */
	protected EObject selectElement(EObject[] elements) {
		Shell shell = Display.getCurrent().getActiveShell();
		ILabelProvider labelProvider = new AdapterFactoryLabelProvider(
				MethodDiagramEditorPlugin.getInstance()
						.getItemProvidersAdapterFactory());
		ElementListSelectionDialog dialog = new ElementListSelectionDialog(
				shell, labelProvider);
		dialog.setMessage(Messages.MethodModelingAssistantProviderMessage);
		dialog.setTitle(Messages.MethodModelingAssistantProviderTitle);
		dialog.setMultipleSelection(false);
		dialog.setElements(elements);
		EObject selected = null;
		if (dialog.open() == Window.OK) {
			selected = (EObject) dialog.getFirstResult();
		}
		return selected;
	}
}
