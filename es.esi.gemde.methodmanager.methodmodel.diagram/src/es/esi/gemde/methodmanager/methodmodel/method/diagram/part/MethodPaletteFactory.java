/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.part;

import java.util.Collections;
import java.util.List;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeConnectionTool;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeCreationTool;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

import es.esi.gemde.methodmanager.methodmodel.method.diagram.providers.MethodElementTypes;

/**
 * @generated
 */
public class MethodPaletteFactory {

	/**
	 * @generated
	 */
	public void fillPalette(PaletteRoot paletteRoot) {
		paletteRoot.add(createObjects1Group());
		paletteRoot.add(createConnections2Group());
	}

	/**
	 * Creates "Objects" palette tool group
	 * @generated
	 */
	private PaletteContainer createObjects1Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.Objects1Group_title);
		paletteContainer.setId("createObjects1Group"); //$NON-NLS-1$
		paletteContainer.add(createAddCallWizardCommand1CreationTool());
		paletteContainer.add(createAddComplexCommand2CreationTool());
		paletteContainer.add(createAddConditionalSubTask3CreationTool());
		paletteContainer.add(createAddConstantCommandParameter4CreationTool());
		paletteContainer.add(createAddCustomCommand5CreationTool());
		paletteContainer.add(createAddInternalVariable6CreationTool());
		paletteContainer
				.add(createAddLaunchTransformationCommand7CreationTool());
		paletteContainer.add(createAddLaunchValidationCommand8CreationTool());
		paletteContainer.add(createAddMethodPhase9CreationTool());
		paletteContainer.add(createAddOpenEditorCommand10CreationTool());
		paletteContainer.add(createAddRepeatedSubTask11CreationTool());
		paletteContainer
				.add(createAddSelectEObjectfromFileCommand12CreationTool());
		paletteContainer
				.add(createAddSelectEObjectfromObjectCommand13CreationTool());
		paletteContainer.add(createAddSelectFile14CreationTool());
		paletteContainer.add(createAddSelectFolderCommand15CreationTool());
		paletteContainer.add(createAddSelectMetamodel16CreationTool());
		paletteContainer
				.add(createAddSelectTransformationCommand17CreationTool());
		paletteContainer.add(createAddSelectValidationCommand18CreationTool());
		paletteContainer.add(createAddSubTask19CreationTool());
		paletteContainer.add(createAddTask20CreationTool());
		paletteContainer.add(createAddVariable21CreationTool());
		paletteContainer.add(createAddVariableCommandParameter22CreationTool());
		return paletteContainer;
	}

	/**
	 * Creates "Connections" palette tool group
	 * @generated
	 */
	private PaletteContainer createConnections2Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.Connections2Group_title);
		paletteContainer.setId("createConnections2Group"); //$NON-NLS-1$
		paletteContainer.add(createComposingCommand1CreationTool());
		paletteContainer.add(createConditionalSubTask2CreationTool());
		paletteContainer.add(createNextPhase3CreationTool());
		paletteContainer.add(createNextSubTask4CreationTool());
		paletteContainer.add(createNextTask5CreationTool());
		paletteContainer.add(createRepeatedSubTask6CreationTool());
		return paletteContainer;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddCallWizardCommand1CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddCallWizardCommand1CreationTool_title,
				Messages.AddCallWizardCommand1CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.CallWizardCommand_2003));
		entry.setId("createAddCallWizardCommand1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.CallWizardCommand_2003));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddComplexCommand2CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddComplexCommand2CreationTool_title,
				Messages.AddComplexCommand2CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.ComplexCommand_2014));
		entry.setId("createAddComplexCommand2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.ComplexCommand_2014));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddConditionalSubTask3CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddConditionalSubTask3CreationTool_title,
				Messages.AddConditionalSubTask3CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.ConditionalSubTask_3003));
		entry.setId("createAddConditionalSubTask3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.ConditionalSubTask_3003));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddConstantCommandParameter4CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddConstantCommandParameter4CreationTool_title,
				Messages.AddConstantCommandParameter4CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.ConstantCommandParameter_3006));
		entry.setId("createAddConstantCommandParameter4CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.ConstantCommandParameter_3006));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddCustomCommand5CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddCustomCommand5CreationTool_title,
				Messages.AddCustomCommand5CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.CustomCommand_2002));
		entry.setId("createAddCustomCommand5CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.CustomCommand_2002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddInternalVariable6CreationTool() {
		ToolEntry entry = new ToolEntry(
				Messages.AddInternalVariable6CreationTool_title,
				Messages.AddInternalVariable6CreationTool_desc, null, null) {
		};
		entry.setId("createAddInternalVariable6CreationTool"); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddLaunchTransformationCommand7CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddLaunchTransformationCommand7CreationTool_title,
				Messages.AddLaunchTransformationCommand7CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.LaunchTransformationCommand_2012));
		entry.setId("createAddLaunchTransformationCommand7CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.LaunchTransformationCommand_2012));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddLaunchValidationCommand8CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddLaunchValidationCommand8CreationTool_title,
				Messages.AddLaunchValidationCommand8CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.LaunchValidationCommand_2011));
		entry.setId("createAddLaunchValidationCommand8CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.LaunchValidationCommand_2011));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddMethodPhase9CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddMethodPhase9CreationTool_title,
				Messages.AddMethodPhase9CreationTool_desc,
				Collections.singletonList(MethodElementTypes.Phase_2001));
		entry.setId("createAddMethodPhase9CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.Phase_2001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddOpenEditorCommand10CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddOpenEditorCommand10CreationTool_title,
				Messages.AddOpenEditorCommand10CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.OpenEditorCommand_2013));
		entry.setId("createAddOpenEditorCommand10CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.OpenEditorCommand_2013));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddRepeatedSubTask11CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddRepeatedSubTask11CreationTool_title,
				Messages.AddRepeatedSubTask11CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.RepeatedSubTask_3004));
		entry.setId("createAddRepeatedSubTask11CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.RepeatedSubTask_3004));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddSelectEObjectfromFileCommand12CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddSelectEObjectfromFileCommand12CreationTool_title,
				Messages.AddSelectEObjectfromFileCommand12CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.SelectModelElementFromFileCommand_2007));
		entry.setId("createAddSelectEObjectfromFileCommand12CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.SelectModelElementFromFileCommand_2007));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddSelectEObjectfromObjectCommand13CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddSelectEObjectfromObjectCommand13CreationTool_title,
				Messages.AddSelectEObjectfromObjectCommand13CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.SelectModelElementFromEObjectCommand_2008));
		entry.setId("createAddSelectEObjectfromObjectCommand13CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.SelectModelElementFromEObjectCommand_2008));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddSelectFile14CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddSelectFile14CreationTool_title,
				Messages.AddSelectFile14CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.SelectFileCommand_2004));
		entry.setId("createAddSelectFile14CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.SelectFileCommand_2004));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddSelectFolderCommand15CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddSelectFolderCommand15CreationTool_title,
				Messages.AddSelectFolderCommand15CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.SelectFolderCommand_2005));
		entry.setId("createAddSelectFolderCommand15CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.SelectFolderCommand_2005));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddSelectMetamodel16CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddSelectMetamodel16CreationTool_title,
				Messages.AddSelectMetamodel16CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.SelectMetamodelCommand_2006));
		entry.setId("createAddSelectMetamodel16CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.SelectMetamodelCommand_2006));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddSelectTransformationCommand17CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddSelectTransformationCommand17CreationTool_title,
				Messages.AddSelectTransformationCommand17CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.SelectTransformationCommand_2010));
		entry.setId("createAddSelectTransformationCommand17CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.SelectTransformationCommand_2010));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddSelectValidationCommand18CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddSelectValidationCommand18CreationTool_title,
				Messages.AddSelectValidationCommand18CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.SelectValidationCommand_2009));
		entry.setId("createAddSelectValidationCommand18CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.SelectValidationCommand_2009));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddSubTask19CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddSubTask19CreationTool_title,
				Messages.AddSubTask19CreationTool_desc,
				Collections.singletonList(MethodElementTypes.SubTask_3002));
		entry.setId("createAddSubTask19CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.SubTask_3002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddTask20CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddTask20CreationTool_title,
				Messages.AddTask20CreationTool_desc,
				Collections.singletonList(MethodElementTypes.Task_3001));
		entry.setId("createAddTask20CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.Task_3001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddVariable21CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddVariable21CreationTool_title,
				Messages.AddVariable21CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.MethodVariable_2015));
		entry.setId("createAddVariable21CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.MethodVariable_2015));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAddVariableCommandParameter22CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AddVariableCommandParameter22CreationTool_title,
				Messages.AddVariableCommandParameter22CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.VariableCommandParameter_3005));
		entry.setId("createAddVariableCommandParameter22CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodElementTypes
				.getImageDescriptor(MethodElementTypes.VariableCommandParameter_3005));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createComposingCommand1CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.ComposingCommand1CreationTool_title,
				Messages.ComposingCommand1CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.ComplexCommandCommands_4006));
		entry.setId("createComposingCommand1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodDiagramEditorPlugin
				.findImageDescriptor("/es.esi.gemde.methodmanager.methodmodel.edit/icons/full/obj16/ComposingCommandTool.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createConditionalSubTask2CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.ConditionalSubTask2CreationTool_title,
				Messages.ConditionalSubTask2CreationTool_desc,
				Collections.singletonList(MethodElementTypes.Condition_4001));
		entry.setId("createConditionalSubTask2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodDiagramEditorPlugin
				.findImageDescriptor("/es.esi.gemde.methodmanager.methodmodel.edit/icons/full/obj16/ConditionLinkTool.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createNextPhase3CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.NextPhase3CreationTool_title,
				Messages.NextPhase3CreationTool_desc,
				Collections.singletonList(MethodElementTypes.PhaseNext_4002));
		entry.setId("createNextPhase3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodDiagramEditorPlugin
				.findImageDescriptor("/es.esi.gemde.methodmanager.methodmodel.edit/icons/full/obj16/NextPhaseTool.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createNextSubTask4CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.NextSubTask4CreationTool_title,
				Messages.NextSubTask4CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.AbstractSubTaskNext_4004));
		entry.setId("createNextSubTask4CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodDiagramEditorPlugin
				.findImageDescriptor("/es.esi.gemde.methodmanager.methodmodel.edit/icons/full/obj16/NextSubTaskTool.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createNextTask5CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.NextTask5CreationTool_title,
				Messages.NextTask5CreationTool_desc,
				Collections.singletonList(MethodElementTypes.TaskNext_4003));
		entry.setId("createNextTask5CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodDiagramEditorPlugin
				.findImageDescriptor("/es.esi.gemde.methodmanager.methodmodel.edit/icons/full/obj16/NextTaskTool.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createRepeatedSubTask6CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.RepeatedSubTask6CreationTool_title,
				Messages.RepeatedSubTask6CreationTool_desc,
				Collections
						.singletonList(MethodElementTypes.RepeatedSubTaskRepeatedElement_4005));
		entry.setId("createRepeatedSubTask6CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(MethodDiagramEditorPlugin
				.findImageDescriptor("/es.esi.gemde.methodmanager.methodmodel.edit/icons/full/obj16/RepeatedSubTaskTool.gif")); //$NON-NLS-1$
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private static class NodeToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> elementTypes;

		/**
		 * @generated
		 */
		private NodeToolEntry(String title, String description,
				List<IElementType> elementTypes) {
			super(title, description, null, null);
			this.elementTypes = elementTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeCreationTool(elementTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}

	/**
	 * @generated
	 */
	private static class LinkToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> relationshipTypes;

		/**
		 * @generated
		 */
		private LinkToolEntry(String title, String description,
				List<IElementType> relationshipTypes) {
			super(title, description, null, null);
			this.relationshipTypes = relationshipTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeConnectionTool(relationshipTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}
}
