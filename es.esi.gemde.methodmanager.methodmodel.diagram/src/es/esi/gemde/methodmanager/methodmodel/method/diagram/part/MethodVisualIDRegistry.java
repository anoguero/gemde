/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.structure.DiagramStructure;

import es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod;
import es.esi.gemde.methodmanager.methodmodel.method.MethodPackage;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.AbstractSubTaskNextEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CallWizardCommandCallWizardCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CallWizardCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CallWizardCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ComplexCommandCommandsEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ComplexCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ComplexCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConditionEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConditionValueEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConditionalSubTaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConditionalSubTaskTitleEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConstantCommandParameterEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConstantCommandParameterIdentifierEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CustomCommandCustomCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CustomCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CustomCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.GEMDEMethodEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchTransformationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchTransformationCommandLaunchTransformationCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchTransformationCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchValidationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchValidationCommandLaunchValidationCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchValidationCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.MethodVariableEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.MethodVariableNameTypeEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.OpenEditorCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.OpenEditorCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.OpenEditorCommandOpenEditorCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.PhaseEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.PhaseNextEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.PhasePhaseTasksCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.PhaseTitleEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.RepeatedSubTaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.RepeatedSubTaskRepeatedElementEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.RepeatedSubTaskTitleRepetitionVectorEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFileCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFileCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFileCommandSelectFileCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFolderCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFolderCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFolderCommandSelectFolderCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectMetamodelCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectMetamodelCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectMetamodelCommandSelectMetamodelCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromEObjectCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromEObjectCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromEObjectCommandSelectModelElementFromEObjectCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromFileCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromFileCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromFileCommandSelectModelElementFromFileCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectTransformationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectTransformationCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectTransformationCommandSelectTransformationCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectValidationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectValidationCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectValidationCommandSelectValidationCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SubTaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SubTaskTitleEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.TaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.TaskNextEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.TaskTaskSubTasksCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.TaskTitleEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.VariableCommandParameterEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.VariableCommandParameterIdentifierEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.WrappingLabel2EditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.WrappingLabel3EditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.WrappingLabel4EditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.WrappingLabel5EditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.WrappingLabelEditPart;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented
 * by a domain model object.
 * 
 * @generated
 */
public class MethodVisualIDRegistry {

	/**
	 * @generated
	 */
	private static final String DEBUG_KEY = "es.esi.gemde.methodmanager.methodmodel.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (GEMDEMethodEditPart.MODEL_ID.equals(view.getType())) {
				return GEMDEMethodEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodVisualIDRegistry
				.getVisualID(view.getType());
	}

	/**
	 * @generated
	 */
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	 * @generated
	 */
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(
					Platform.getDebugOption(DEBUG_KEY))) {
				MethodDiagramEditorPlugin.getInstance().logError(
						"Unable to parse view type as a visualID number: "
								+ type);
			}
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static String getType(int visualID) {
		return Integer.toString(visualID);
	}

	/**
	 * @generated
	 */
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (MethodPackage.eINSTANCE.getGEMDEMethod().isSuperTypeOf(
				domainElement.eClass())
				&& isDiagram((GEMDEMethod) domainElement)) {
			return GEMDEMethodEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodVisualIDRegistry
				.getModelID(containerView);
		if (!GEMDEMethodEditPart.MODEL_ID.equals(containerModelID)
				&& !"method".equals(containerModelID)) { //$NON-NLS-1$
			return -1;
		}
		int containerVisualID;
		if (GEMDEMethodEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = GEMDEMethodEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case GEMDEMethodEditPart.VISUAL_ID:
			if (MethodPackage.eINSTANCE.getPhase().isSuperTypeOf(
					domainElement.eClass())) {
				return PhaseEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getCustomCommand().isSuperTypeOf(
					domainElement.eClass())) {
				return CustomCommandEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getCallWizardCommand().isSuperTypeOf(
					domainElement.eClass())) {
				return CallWizardCommandEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getSelectFileCommand().isSuperTypeOf(
					domainElement.eClass())) {
				return SelectFileCommandEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getSelectFolderCommand().isSuperTypeOf(
					domainElement.eClass())) {
				return SelectFolderCommandEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getSelectMetamodelCommand()
					.isSuperTypeOf(domainElement.eClass())) {
				return SelectMetamodelCommandEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getSelectModelElementFromFileCommand()
					.isSuperTypeOf(domainElement.eClass())) {
				return SelectModelElementFromFileCommandEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE
					.getSelectModelElementFromEObjectCommand().isSuperTypeOf(
							domainElement.eClass())) {
				return SelectModelElementFromEObjectCommandEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getSelectValidationCommand()
					.isSuperTypeOf(domainElement.eClass())) {
				return SelectValidationCommandEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getSelectTransformationCommand()
					.isSuperTypeOf(domainElement.eClass())) {
				return SelectTransformationCommandEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getLaunchValidationCommand()
					.isSuperTypeOf(domainElement.eClass())) {
				return LaunchValidationCommandEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getLaunchTransformationCommand()
					.isSuperTypeOf(domainElement.eClass())) {
				return LaunchTransformationCommandEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getOpenEditorCommand().isSuperTypeOf(
					domainElement.eClass())) {
				return OpenEditorCommandEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getMethodVariable().isSuperTypeOf(
					domainElement.eClass())) {
				return MethodVariableEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getComplexCommand().isSuperTypeOf(
					domainElement.eClass())) {
				return ComplexCommandEditPart.VISUAL_ID;
			}
			break;
		case PhasePhaseTasksCompartmentEditPart.VISUAL_ID:
			if (MethodPackage.eINSTANCE.getTask().isSuperTypeOf(
					domainElement.eClass())) {
				return TaskEditPart.VISUAL_ID;
			}
			break;
		case TaskTaskSubTasksCompartmentEditPart.VISUAL_ID:
			if (MethodPackage.eINSTANCE.getSubTask().isSuperTypeOf(
					domainElement.eClass())) {
				return SubTaskEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getConditionalSubTask().isSuperTypeOf(
					domainElement.eClass())) {
				return ConditionalSubTaskEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getRepeatedSubTask().isSuperTypeOf(
					domainElement.eClass())) {
				return RepeatedSubTaskEditPart.VISUAL_ID;
			}
			break;
		case CustomCommandCustomCommandParametersCompartmentEditPart.VISUAL_ID:
			if (MethodPackage.eINSTANCE.getVariableCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return VariableCommandParameterEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getConstantCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return ConstantCommandParameterEditPart.VISUAL_ID;
			}
			break;
		case CallWizardCommandCallWizardCommandParametersCompartmentEditPart.VISUAL_ID:
			if (MethodPackage.eINSTANCE.getVariableCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return VariableCommandParameterEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getConstantCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return ConstantCommandParameterEditPart.VISUAL_ID;
			}
			break;
		case SelectFileCommandSelectFileCommandParametersCompartmentEditPart.VISUAL_ID:
			if (MethodPackage.eINSTANCE.getVariableCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return VariableCommandParameterEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getConstantCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return ConstantCommandParameterEditPart.VISUAL_ID;
			}
			break;
		case SelectFolderCommandSelectFolderCommandParametersCompartmentEditPart.VISUAL_ID:
			if (MethodPackage.eINSTANCE.getVariableCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return VariableCommandParameterEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getConstantCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return ConstantCommandParameterEditPart.VISUAL_ID;
			}
			break;
		case SelectMetamodelCommandSelectMetamodelCommandParametersCompartmentEditPart.VISUAL_ID:
			if (MethodPackage.eINSTANCE.getVariableCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return VariableCommandParameterEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getConstantCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return ConstantCommandParameterEditPart.VISUAL_ID;
			}
			break;
		case SelectModelElementFromFileCommandSelectModelElementFromFileCommandParametersCompartmentEditPart.VISUAL_ID:
			if (MethodPackage.eINSTANCE.getVariableCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return VariableCommandParameterEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getConstantCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return ConstantCommandParameterEditPart.VISUAL_ID;
			}
			break;
		case SelectModelElementFromEObjectCommandSelectModelElementFromEObjectCommandParametersCompartmentEditPart.VISUAL_ID:
			if (MethodPackage.eINSTANCE.getVariableCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return VariableCommandParameterEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getConstantCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return ConstantCommandParameterEditPart.VISUAL_ID;
			}
			break;
		case SelectValidationCommandSelectValidationCommandParametersCompartmentEditPart.VISUAL_ID:
			if (MethodPackage.eINSTANCE.getVariableCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return VariableCommandParameterEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getConstantCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return ConstantCommandParameterEditPart.VISUAL_ID;
			}
			break;
		case SelectTransformationCommandSelectTransformationCommandParametersCompartmentEditPart.VISUAL_ID:
			if (MethodPackage.eINSTANCE.getVariableCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return VariableCommandParameterEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getConstantCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return ConstantCommandParameterEditPart.VISUAL_ID;
			}
			break;
		case LaunchValidationCommandLaunchValidationCommandParametersCompartmentEditPart.VISUAL_ID:
			if (MethodPackage.eINSTANCE.getVariableCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return VariableCommandParameterEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getConstantCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return ConstantCommandParameterEditPart.VISUAL_ID;
			}
			break;
		case LaunchTransformationCommandLaunchTransformationCommandParametersCompartmentEditPart.VISUAL_ID:
			if (MethodPackage.eINSTANCE.getVariableCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return VariableCommandParameterEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getConstantCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return ConstantCommandParameterEditPart.VISUAL_ID;
			}
			break;
		case OpenEditorCommandOpenEditorCommandParametersCompartmentEditPart.VISUAL_ID:
			if (MethodPackage.eINSTANCE.getVariableCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return VariableCommandParameterEditPart.VISUAL_ID;
			}
			if (MethodPackage.eINSTANCE.getConstantCommandParameter()
					.isSuperTypeOf(domainElement.eClass())) {
				return ConstantCommandParameterEditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodVisualIDRegistry
				.getModelID(containerView);
		if (!GEMDEMethodEditPart.MODEL_ID.equals(containerModelID)
				&& !"method".equals(containerModelID)) { //$NON-NLS-1$
			return false;
		}
		int containerVisualID;
		if (GEMDEMethodEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = GEMDEMethodEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case GEMDEMethodEditPart.VISUAL_ID:
			if (PhaseEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (CustomCommandEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (CallWizardCommandEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (SelectFileCommandEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (SelectFolderCommandEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (SelectMetamodelCommandEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (SelectModelElementFromFileCommandEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (SelectModelElementFromEObjectCommandEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (SelectValidationCommandEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (SelectTransformationCommandEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (LaunchValidationCommandEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (LaunchTransformationCommandEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (OpenEditorCommandEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MethodVariableEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ComplexCommandEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case PhaseEditPart.VISUAL_ID:
			if (PhaseTitleEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (PhasePhaseTasksCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case CustomCommandEditPart.VISUAL_ID:
			if (CustomCommandNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (CustomCommandCustomCommandParametersCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case CallWizardCommandEditPart.VISUAL_ID:
			if (CallWizardCommandNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (CallWizardCommandCallWizardCommandParametersCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SelectFileCommandEditPart.VISUAL_ID:
			if (SelectFileCommandNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (SelectFileCommandSelectFileCommandParametersCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SelectFolderCommandEditPart.VISUAL_ID:
			if (SelectFolderCommandNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (SelectFolderCommandSelectFolderCommandParametersCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SelectMetamodelCommandEditPart.VISUAL_ID:
			if (SelectMetamodelCommandNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (SelectMetamodelCommandSelectMetamodelCommandParametersCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SelectModelElementFromFileCommandEditPart.VISUAL_ID:
			if (SelectModelElementFromFileCommandNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (SelectModelElementFromFileCommandSelectModelElementFromFileCommandParametersCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SelectModelElementFromEObjectCommandEditPart.VISUAL_ID:
			if (SelectModelElementFromEObjectCommandNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (SelectModelElementFromEObjectCommandSelectModelElementFromEObjectCommandParametersCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SelectValidationCommandEditPart.VISUAL_ID:
			if (SelectValidationCommandNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (SelectValidationCommandSelectValidationCommandParametersCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SelectTransformationCommandEditPart.VISUAL_ID:
			if (SelectTransformationCommandNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (SelectTransformationCommandSelectTransformationCommandParametersCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case LaunchValidationCommandEditPart.VISUAL_ID:
			if (LaunchValidationCommandNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (LaunchValidationCommandLaunchValidationCommandParametersCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case LaunchTransformationCommandEditPart.VISUAL_ID:
			if (LaunchTransformationCommandNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (LaunchTransformationCommandLaunchTransformationCommandParametersCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case OpenEditorCommandEditPart.VISUAL_ID:
			if (OpenEditorCommandNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (OpenEditorCommandOpenEditorCommandParametersCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case MethodVariableEditPart.VISUAL_ID:
			if (MethodVariableNameTypeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ComplexCommandEditPart.VISUAL_ID:
			if (ComplexCommandNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case TaskEditPart.VISUAL_ID:
			if (TaskTitleEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (TaskTaskSubTasksCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SubTaskEditPart.VISUAL_ID:
			if (SubTaskTitleEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ConditionalSubTaskEditPart.VISUAL_ID:
			if (ConditionalSubTaskTitleEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RepeatedSubTaskEditPart.VISUAL_ID:
			if (RepeatedSubTaskTitleRepetitionVectorEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case VariableCommandParameterEditPart.VISUAL_ID:
			if (VariableCommandParameterIdentifierEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ConstantCommandParameterEditPart.VISUAL_ID:
			if (ConstantCommandParameterIdentifierEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case PhasePhaseTasksCompartmentEditPart.VISUAL_ID:
			if (TaskEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case TaskTaskSubTasksCompartmentEditPart.VISUAL_ID:
			if (SubTaskEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ConditionalSubTaskEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RepeatedSubTaskEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case CustomCommandCustomCommandParametersCompartmentEditPart.VISUAL_ID:
			if (VariableCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ConstantCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case CallWizardCommandCallWizardCommandParametersCompartmentEditPart.VISUAL_ID:
			if (VariableCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ConstantCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SelectFileCommandSelectFileCommandParametersCompartmentEditPart.VISUAL_ID:
			if (VariableCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ConstantCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SelectFolderCommandSelectFolderCommandParametersCompartmentEditPart.VISUAL_ID:
			if (VariableCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ConstantCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SelectMetamodelCommandSelectMetamodelCommandParametersCompartmentEditPart.VISUAL_ID:
			if (VariableCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ConstantCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SelectModelElementFromFileCommandSelectModelElementFromFileCommandParametersCompartmentEditPart.VISUAL_ID:
			if (VariableCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ConstantCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SelectModelElementFromEObjectCommandSelectModelElementFromEObjectCommandParametersCompartmentEditPart.VISUAL_ID:
			if (VariableCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ConstantCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SelectValidationCommandSelectValidationCommandParametersCompartmentEditPart.VISUAL_ID:
			if (VariableCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ConstantCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SelectTransformationCommandSelectTransformationCommandParametersCompartmentEditPart.VISUAL_ID:
			if (VariableCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ConstantCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case LaunchValidationCommandLaunchValidationCommandParametersCompartmentEditPart.VISUAL_ID:
			if (VariableCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ConstantCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case LaunchTransformationCommandLaunchTransformationCommandParametersCompartmentEditPart.VISUAL_ID:
			if (VariableCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ConstantCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case OpenEditorCommandOpenEditorCommandParametersCompartmentEditPart.VISUAL_ID:
			if (VariableCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ConstantCommandParameterEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ConditionEditPart.VISUAL_ID:
			if (ConditionValueEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case PhaseNextEditPart.VISUAL_ID:
			if (WrappingLabelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case TaskNextEditPart.VISUAL_ID:
			if (WrappingLabel2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case AbstractSubTaskNextEditPart.VISUAL_ID:
			if (WrappingLabel3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RepeatedSubTaskRepeatedElementEditPart.VISUAL_ID:
			if (WrappingLabel4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ComplexCommandCommandsEditPart.VISUAL_ID:
			if (WrappingLabel5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (MethodPackage.eINSTANCE.getCondition().isSuperTypeOf(
				domainElement.eClass())) {
			return ConditionEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * User can change implementation of this method to handle some specific
	 * situations not covered by default logic.
	 * 
	 * @generated
	 */
	private static boolean isDiagram(GEMDEMethod element) {
		return true;
	}

	/**
	 * @generated
	 */
	public static boolean checkNodeVisualID(View containerView,
			EObject domainElement, int candidate) {
		if (candidate == -1) {
			//unrecognized id is always bad
			return false;
		}
		int basic = getNodeVisualID(containerView, domainElement);
		return basic == candidate;
	}

	/**
	 * @generated
	 */
	public static boolean isCompartmentVisualID(int visualID) {
		switch (visualID) {
		case PhasePhaseTasksCompartmentEditPart.VISUAL_ID:
		case TaskTaskSubTasksCompartmentEditPart.VISUAL_ID:
		case CustomCommandCustomCommandParametersCompartmentEditPart.VISUAL_ID:
		case CallWizardCommandCallWizardCommandParametersCompartmentEditPart.VISUAL_ID:
		case SelectFileCommandSelectFileCommandParametersCompartmentEditPart.VISUAL_ID:
		case SelectFolderCommandSelectFolderCommandParametersCompartmentEditPart.VISUAL_ID:
		case SelectMetamodelCommandSelectMetamodelCommandParametersCompartmentEditPart.VISUAL_ID:
		case SelectModelElementFromFileCommandSelectModelElementFromFileCommandParametersCompartmentEditPart.VISUAL_ID:
		case SelectModelElementFromEObjectCommandSelectModelElementFromEObjectCommandParametersCompartmentEditPart.VISUAL_ID:
		case SelectValidationCommandSelectValidationCommandParametersCompartmentEditPart.VISUAL_ID:
		case SelectTransformationCommandSelectTransformationCommandParametersCompartmentEditPart.VISUAL_ID:
		case LaunchValidationCommandLaunchValidationCommandParametersCompartmentEditPart.VISUAL_ID:
		case LaunchTransformationCommandLaunchTransformationCommandParametersCompartmentEditPart.VISUAL_ID:
		case OpenEditorCommandOpenEditorCommandParametersCompartmentEditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static boolean isSemanticLeafVisualID(int visualID) {
		switch (visualID) {
		case GEMDEMethodEditPart.VISUAL_ID:
			return false;
		case ComplexCommandEditPart.VISUAL_ID:
		case MethodVariableEditPart.VISUAL_ID:
		case SubTaskEditPart.VISUAL_ID:
		case ConditionalSubTaskEditPart.VISUAL_ID:
		case RepeatedSubTaskEditPart.VISUAL_ID:
		case VariableCommandParameterEditPart.VISUAL_ID:
		case ConstantCommandParameterEditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static final DiagramStructure TYPED_INSTANCE = new DiagramStructure() {
		/**
		 * @generated
		 */
		@Override
		public int getVisualID(View view) {
			return es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodVisualIDRegistry
					.getVisualID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public String getModelID(View view) {
			return es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodVisualIDRegistry
					.getModelID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public int getNodeVisualID(View containerView, EObject domainElement) {
			return es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodVisualIDRegistry
					.getNodeVisualID(containerView, domainElement);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean checkNodeVisualID(View containerView,
				EObject domainElement, int candidate) {
			return es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodVisualIDRegistry
					.checkNodeVisualID(containerView, domainElement, candidate);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isCompartmentVisualID(int visualID) {
			return es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodVisualIDRegistry
					.isCompartmentVisualID(visualID);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isSemanticLeafVisualID(int visualID) {
			return es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodVisualIDRegistry
					.isSemanticLeafVisualID(visualID);
		}
	};

}
