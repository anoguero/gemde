/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.ConstantCommandParameterCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.VariableCommandParameterCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.providers.MethodElementTypes;

/**
 * @generated
 */
public class SelectModelElementFromFileCommandSelectModelElementFromFileCommandParametersCompartmentItemSemanticEditPolicy
		extends MethodBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public SelectModelElementFromFileCommandSelectModelElementFromFileCommandParametersCompartmentItemSemanticEditPolicy() {
		super(MethodElementTypes.SelectModelElementFromFileCommand_2007);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (MethodElementTypes.VariableCommandParameter_3005 == req
				.getElementType()) {
			return getGEFWrapper(new VariableCommandParameterCreateCommand(req));
		}
		if (MethodElementTypes.ConstantCommandParameter_3006 == req
				.getElementType()) {
			return getGEFWrapper(new ConstantCommandParameterCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
