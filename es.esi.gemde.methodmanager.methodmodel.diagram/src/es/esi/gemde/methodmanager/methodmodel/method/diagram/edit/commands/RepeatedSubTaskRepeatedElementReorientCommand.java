/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientReferenceRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

import es.esi.gemde.methodmanager.methodmodel.method.RepeatedSubTask;
import es.esi.gemde.methodmanager.methodmodel.method.SubTask;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.policies.MethodBaseItemSemanticEditPolicy;

/**
 * @generated
 */
public class RepeatedSubTaskRepeatedElementReorientCommand extends
		EditElementCommand {

	/**
	 * @generated
	 */
	private final int reorientDirection;

	/**
	 * @generated
	 */
	private final EObject referenceOwner;

	/**
	 * @generated
	 */
	private final EObject oldEnd;

	/**
	 * @generated
	 */
	private final EObject newEnd;

	/**
	 * @generated
	 */
	public RepeatedSubTaskRepeatedElementReorientCommand(
			ReorientReferenceRelationshipRequest request) {
		super(request.getLabel(), null, request);
		reorientDirection = request.getDirection();
		referenceOwner = request.getReferenceOwner();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		if (false == referenceOwner instanceof RepeatedSubTask) {
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return canReorientTarget();
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean canReorientSource() {
		if (!(oldEnd instanceof SubTask && newEnd instanceof RepeatedSubTask)) {
			return false;
		}
		return MethodBaseItemSemanticEditPolicy.getLinkConstraints()
				.canExistRepeatedSubTaskRepeatedElement_4005(getNewSource(),
						getOldTarget());
	}

	/**
	 * @generated
	 */
	protected boolean canReorientTarget() {
		if (!(oldEnd instanceof SubTask && newEnd instanceof SubTask)) {
			return false;
		}
		return MethodBaseItemSemanticEditPolicy.getLinkConstraints()
				.canExistRepeatedSubTaskRepeatedElement_4005(getOldSource(),
						getNewTarget());
	}

	/**
	 * @generated
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException(
					"Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientSource() throws ExecutionException {
		getOldSource().setRepeatedElement(null);
		getNewSource().setRepeatedElement(getOldTarget());
		return CommandResult.newOKCommandResult(referenceOwner);
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientTarget() throws ExecutionException {
		getOldSource().setRepeatedElement(getNewTarget());
		return CommandResult.newOKCommandResult(referenceOwner);
	}

	/**
	 * @generated
	 */
	protected RepeatedSubTask getOldSource() {
		return (RepeatedSubTask) referenceOwner;
	}

	/**
	 * @generated
	 */
	protected RepeatedSubTask getNewSource() {
		return (RepeatedSubTask) newEnd;
	}

	/**
	 * @generated
	 */
	protected SubTask getOldTarget() {
		return (SubTask) oldEnd;
	}

	/**
	 * @generated
	 */
	protected SubTask getNewTarget() {
		return (SubTask) newEnd;
	}
}
