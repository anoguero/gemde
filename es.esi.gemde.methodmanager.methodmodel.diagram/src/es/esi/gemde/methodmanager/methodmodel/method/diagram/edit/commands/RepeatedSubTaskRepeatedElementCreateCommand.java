/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;

import es.esi.gemde.methodmanager.methodmodel.method.RepeatedSubTask;
import es.esi.gemde.methodmanager.methodmodel.method.SubTask;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.policies.MethodBaseItemSemanticEditPolicy;

/**
 * @generated
 */
public class RepeatedSubTaskRepeatedElementCreateCommand extends
		EditElementCommand {

	/**
	 * @generated
	 */
	private final EObject source;

	/**
	 * @generated
	 */
	private final EObject target;

	/**
	 * @generated
	 */
	public RepeatedSubTaskRepeatedElementCreateCommand(
			CreateRelationshipRequest request, EObject source, EObject target) {
		super(request.getLabel(), null, request);
		this.source = source;
		this.target = target;
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		if (source == null && target == null) {
			return false;
		}
		if (source != null && false == source instanceof RepeatedSubTask) {
			return false;
		}
		if (target != null && false == target instanceof SubTask) {
			return false;
		}
		if (getSource() == null) {
			return true; // link creation is in progress; source is not defined yet
		}
		// target may be null here but it's possible to check constraint
		return MethodBaseItemSemanticEditPolicy.getLinkConstraints()
				.canCreateRepeatedSubTaskRepeatedElement_4005(getSource(),
						getTarget());
	}

	/**
	 * @generated
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException(
					"Invalid arguments in create link command"); //$NON-NLS-1$
		}

		if (getSource() != null && getTarget() != null) {
			getSource().setRepeatedElement(getTarget());
		}
		return CommandResult.newOKCommandResult();

	}

	/**
	 * @generated
	 */
	protected void setElementToEdit(EObject element) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @generated
	 */
	protected RepeatedSubTask getSource() {
		return (RepeatedSubTask) source;
	}

	/**
	 * @generated
	 */
	protected SubTask getTarget() {
		return (SubTask) target;
	}
}
