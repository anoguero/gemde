/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyReferenceCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyReferenceRequest;

import es.esi.gemde.methodmanager.methodmodel.method.diagram.providers.MethodElementTypes;

/**
 * @generated
 */
public class RepeatedSubTaskRepeatedElementItemSemanticEditPolicy extends
		MethodBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public RepeatedSubTaskRepeatedElementItemSemanticEditPolicy() {
		super(MethodElementTypes.RepeatedSubTaskRepeatedElement_4005);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyReferenceCommand(DestroyReferenceRequest req) {
		return getGEFWrapper(new DestroyReferenceCommand(req));
	}

}
