/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.navigator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gmf.runtime.emf.core.GMFEditingDomainFactory;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonContentProvider;

import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.AbstractSubTaskNextEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CallWizardCommandCallWizardCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CallWizardCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ComplexCommandCommandsEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ComplexCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConditionEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConditionalSubTaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConstantCommandParameterEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CustomCommandCustomCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CustomCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.GEMDEMethodEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchTransformationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchTransformationCommandLaunchTransformationCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchValidationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchValidationCommandLaunchValidationCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.MethodVariableEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.OpenEditorCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.OpenEditorCommandOpenEditorCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.PhaseEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.PhaseNextEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.PhasePhaseTasksCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.RepeatedSubTaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.RepeatedSubTaskRepeatedElementEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFileCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFileCommandSelectFileCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFolderCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFolderCommandSelectFolderCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectMetamodelCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectMetamodelCommandSelectMetamodelCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromEObjectCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromEObjectCommandSelectModelElementFromEObjectCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromFileCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromFileCommandSelectModelElementFromFileCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectTransformationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectTransformationCommandSelectTransformationCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectValidationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectValidationCommandSelectValidationCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SubTaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.TaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.TaskNextEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.TaskTaskSubTasksCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.VariableCommandParameterEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.part.Messages;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodVisualIDRegistry;

/**
 * @generated
 */
public class MethodNavigatorContentProvider implements ICommonContentProvider {

	/**
	 * @generated
	 */
	private static final Object[] EMPTY_ARRAY = new Object[0];

	/**
	 * @generated
	 */
	private Viewer myViewer;

	/**
	 * @generated
	 */
	private AdapterFactoryEditingDomain myEditingDomain;

	/**
	 * @generated
	 */
	private WorkspaceSynchronizer myWorkspaceSynchronizer;

	/**
	 * @generated
	 */
	private Runnable myViewerRefreshRunnable;

	/**
	 * @generated
	 */
	@SuppressWarnings({ "unchecked", "serial", "rawtypes" })
	public MethodNavigatorContentProvider() {
		TransactionalEditingDomain editingDomain = GMFEditingDomainFactory.INSTANCE
				.createEditingDomain();
		myEditingDomain = (AdapterFactoryEditingDomain) editingDomain;
		myEditingDomain.setResourceToReadOnlyMap(new HashMap() {
			public Object get(Object key) {
				if (!containsKey(key)) {
					put(key, Boolean.TRUE);
				}
				return super.get(key);
			}
		});
		myViewerRefreshRunnable = new Runnable() {
			public void run() {
				if (myViewer != null) {
					myViewer.refresh();
				}
			}
		};
		myWorkspaceSynchronizer = new WorkspaceSynchronizer(editingDomain,
				new WorkspaceSynchronizer.Delegate() {
					public void dispose() {
					}

					public boolean handleResourceChanged(final Resource resource) {
						unloadAllResources();
						asyncRefresh();
						return true;
					}

					public boolean handleResourceDeleted(Resource resource) {
						unloadAllResources();
						asyncRefresh();
						return true;
					}

					public boolean handleResourceMoved(Resource resource,
							final URI newURI) {
						unloadAllResources();
						asyncRefresh();
						return true;
					}
				});
	}

	/**
	 * @generated
	 */
	public void dispose() {
		myWorkspaceSynchronizer.dispose();
		myWorkspaceSynchronizer = null;
		myViewerRefreshRunnable = null;
		myViewer = null;
		unloadAllResources();
		((TransactionalEditingDomain) myEditingDomain).dispose();
		myEditingDomain = null;
	}

	/**
	 * @generated
	 */
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		myViewer = viewer;
	}

	/**
	 * @generated
	 */
	void unloadAllResources() {
		for (Resource nextResource : myEditingDomain.getResourceSet()
				.getResources()) {
			nextResource.unload();
		}
	}

	/**
	 * @generated
	 */
	void asyncRefresh() {
		if (myViewer != null && !myViewer.getControl().isDisposed()) {
			myViewer.getControl().getDisplay()
					.asyncExec(myViewerRefreshRunnable);
		}
	}

	/**
	 * @generated
	 */
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof IFile) {
			IFile file = (IFile) parentElement;
			URI fileURI = URI.createPlatformResourceURI(file.getFullPath()
					.toString(), true);
			Resource resource = myEditingDomain.getResourceSet().getResource(
					fileURI, true);
			ArrayList<MethodNavigatorItem> result = new ArrayList<MethodNavigatorItem>();
			ArrayList<View> topViews = new ArrayList<View>(resource
					.getContents().size());
			for (EObject o : resource.getContents()) {
				if (o instanceof View) {
					topViews.add((View) o);
				}
			}
			result.addAll(createNavigatorItems(
					selectViewsByType(topViews, GEMDEMethodEditPart.MODEL_ID),
					file, false));
			return result.toArray();
		}

		if (parentElement instanceof MethodNavigatorGroup) {
			MethodNavigatorGroup group = (MethodNavigatorGroup) parentElement;
			return group.getChildren();
		}

		if (parentElement instanceof MethodNavigatorItem) {
			MethodNavigatorItem navigatorItem = (MethodNavigatorItem) parentElement;
			if (navigatorItem.isLeaf() || !isOwnView(navigatorItem.getView())) {
				return EMPTY_ARRAY;
			}
			return getViewChildren(navigatorItem.getView(), parentElement);
		}

		/*
		 * Due to plugin.xml restrictions this code will be called only for views representing
		 * shortcuts to this diagram elements created on other diagrams. 
		 */
		if (parentElement instanceof IAdaptable) {
			View view = (View) ((IAdaptable) parentElement)
					.getAdapter(View.class);
			if (view != null) {
				return getViewChildren(view, parentElement);
			}
		}

		return EMPTY_ARRAY;
	}

	/**
	 * @generated
	 */
	private Object[] getViewChildren(View view, Object parentElement) {
		switch (MethodVisualIDRegistry.getVisualID(view)) {

		case OpenEditorCommandEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Node sv = (Node) view;
			MethodNavigatorGroup incominglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_OpenEditorCommand_2013_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(OpenEditorCommandOpenEditorCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(VariableCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(OpenEditorCommandOpenEditorCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(ConstantCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(ComplexCommandCommandsEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}

		case SelectFileCommandEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Node sv = (Node) view;
			MethodNavigatorGroup incominglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_SelectFileCommand_2004_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectFileCommandSelectFileCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(VariableCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectFileCommandSelectFileCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(ConstantCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(ComplexCommandCommandsEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}

		case TaskNextEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			MethodNavigatorGroup target = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_TaskNext_4003_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			MethodNavigatorGroup source = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_TaskNext_4003_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					MethodVisualIDRegistry.getType(TaskEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					MethodVisualIDRegistry.getType(TaskEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case SelectModelElementFromFileCommandEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Node sv = (Node) view;
			MethodNavigatorGroup incominglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_SelectModelElementFromFileCommand_2007_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectModelElementFromFileCommandSelectModelElementFromFileCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(VariableCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectModelElementFromFileCommandSelectModelElementFromFileCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(ConstantCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(ComplexCommandCommandsEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}

		case SelectFolderCommandEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Node sv = (Node) view;
			MethodNavigatorGroup incominglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_SelectFolderCommand_2005_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectFolderCommandSelectFolderCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(VariableCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectFolderCommandSelectFolderCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(ConstantCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(ComplexCommandCommandsEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}

		case CallWizardCommandEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Node sv = (Node) view;
			MethodNavigatorGroup incominglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_CallWizardCommand_2003_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(CallWizardCommandCallWizardCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(VariableCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(CallWizardCommandCallWizardCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(ConstantCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(ComplexCommandCommandsEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}

		case AbstractSubTaskNextEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			MethodNavigatorGroup target = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_AbstractSubTaskNext_4004_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			MethodNavigatorGroup source = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_AbstractSubTaskNext_4004_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					MethodVisualIDRegistry.getType(SubTaskEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(ConditionalSubTaskEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(RepeatedSubTaskEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					MethodVisualIDRegistry.getType(SubTaskEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(ConditionalSubTaskEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(RepeatedSubTaskEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case LaunchValidationCommandEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Node sv = (Node) view;
			MethodNavigatorGroup incominglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_LaunchValidationCommand_2011_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(LaunchValidationCommandLaunchValidationCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(VariableCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(LaunchValidationCommandLaunchValidationCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(ConstantCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(ComplexCommandCommandsEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}

		case RepeatedSubTaskEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Node sv = (Node) view;
			MethodNavigatorGroup incominglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_RepeatedSubTask_3004_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			MethodNavigatorGroup outgoinglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_RepeatedSubTask_3004_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(AbstractSubTaskNextEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(AbstractSubTaskNextEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(RepeatedSubTaskRepeatedElementEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case ConditionalSubTaskEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Node sv = (Node) view;
			MethodNavigatorGroup outgoinglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_ConditionalSubTask_3003_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			MethodNavigatorGroup incominglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_ConditionalSubTask_3003_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry.getType(ConditionEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(AbstractSubTaskNextEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(AbstractSubTaskNextEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}

		case SelectValidationCommandEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Node sv = (Node) view;
			MethodNavigatorGroup incominglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_SelectValidationCommand_2009_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectValidationCommandSelectValidationCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(VariableCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectValidationCommandSelectValidationCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(ConstantCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(ComplexCommandCommandsEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}

		case RepeatedSubTaskRepeatedElementEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			MethodNavigatorGroup target = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_RepeatedSubTaskRepeatedElement_4005_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			MethodNavigatorGroup source = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_RepeatedSubTaskRepeatedElement_4005_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					MethodVisualIDRegistry.getType(SubTaskEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(RepeatedSubTaskEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case PhaseNextEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			MethodNavigatorGroup target = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_PhaseNext_4002_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			MethodNavigatorGroup source = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_PhaseNext_4002_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					MethodVisualIDRegistry.getType(PhaseEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					MethodVisualIDRegistry.getType(PhaseEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case TaskEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Node sv = (Node) view;
			MethodNavigatorGroup incominglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_Task_3001_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			MethodNavigatorGroup outgoinglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_Task_3001_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(TaskTaskSubTasksCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					MethodVisualIDRegistry.getType(SubTaskEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(TaskTaskSubTasksCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					MethodVisualIDRegistry
							.getType(ConditionalSubTaskEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(TaskTaskSubTasksCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					MethodVisualIDRegistry
							.getType(RepeatedSubTaskEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry.getType(TaskNextEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry.getType(TaskNextEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case SubTaskEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Node sv = (Node) view;
			MethodNavigatorGroup incominglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_SubTask_3002_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			MethodNavigatorGroup outgoinglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_SubTask_3002_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry.getType(ConditionEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(AbstractSubTaskNextEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(AbstractSubTaskNextEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(RepeatedSubTaskRepeatedElementEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case SelectTransformationCommandEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Node sv = (Node) view;
			MethodNavigatorGroup incominglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_SelectTransformationCommand_2010_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectTransformationCommandSelectTransformationCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(VariableCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectTransformationCommandSelectTransformationCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(ConstantCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(ComplexCommandCommandsEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}

		case SelectMetamodelCommandEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Node sv = (Node) view;
			MethodNavigatorGroup incominglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_SelectMetamodelCommand_2006_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectMetamodelCommandSelectMetamodelCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(VariableCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectMetamodelCommandSelectMetamodelCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(ConstantCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(ComplexCommandCommandsEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}

		case GEMDEMethodEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			result.addAll(getForeignShortcuts((Diagram) view, parentElement));
			Diagram sv = (Diagram) view;
			MethodNavigatorGroup links = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_GEMDEMethod_1000_links,
					"icons/linksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(Collections.singleton(sv),
					MethodVisualIDRegistry.getType(PhaseEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(CustomCommandEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(CallWizardCommandEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectFileCommandEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectFolderCommandEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectMetamodelCommandEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectModelElementFromFileCommandEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectModelElementFromEObjectCommandEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectValidationCommandEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectTransformationCommandEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(LaunchValidationCommandEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(LaunchTransformationCommandEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(OpenEditorCommandEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(MethodVariableEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(ComplexCommandEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getDiagramLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry.getType(ConditionEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry.getType(PhaseNextEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry.getType(TaskNextEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(AbstractSubTaskNextEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(RepeatedSubTaskRepeatedElementEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(ComplexCommandCommandsEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			if (!links.isEmpty()) {
				result.add(links);
			}
			return result.toArray();
		}

		case ConditionEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			MethodNavigatorGroup target = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_Condition_4001_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			MethodNavigatorGroup source = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_Condition_4001_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					MethodVisualIDRegistry.getType(SubTaskEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(ConditionalSubTaskEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case CustomCommandEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Node sv = (Node) view;
			MethodNavigatorGroup incominglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_CustomCommand_2002_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(CustomCommandCustomCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(VariableCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(CustomCommandCustomCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(ConstantCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(ComplexCommandCommandsEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}

		case SelectModelElementFromEObjectCommandEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Node sv = (Node) view;
			MethodNavigatorGroup incominglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_SelectModelElementFromEObjectCommand_2008_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectModelElementFromEObjectCommandSelectModelElementFromEObjectCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(VariableCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectModelElementFromEObjectCommandSelectModelElementFromEObjectCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(ConstantCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(ComplexCommandCommandsEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}

		case ComplexCommandCommandsEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			MethodNavigatorGroup target = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_ComplexCommandCommands_4006_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			MethodNavigatorGroup source = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_ComplexCommandCommands_4006_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(CustomCommandEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(CallWizardCommandEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectFileCommandEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectFolderCommandEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectMetamodelCommandEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectModelElementFromFileCommandEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectModelElementFromEObjectCommandEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectValidationCommandEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(SelectTransformationCommandEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(LaunchValidationCommandEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(LaunchTransformationCommandEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(OpenEditorCommandEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(ComplexCommandEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case ComplexCommandEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Node sv = (Node) view;
			MethodNavigatorGroup outgoinglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_ComplexCommand_2014_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(ComplexCommandCommandsEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case PhaseEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Node sv = (Node) view;
			MethodNavigatorGroup incominglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_Phase_2001_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			MethodNavigatorGroup outgoinglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_Phase_2001_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(PhasePhaseTasksCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					MethodVisualIDRegistry.getType(TaskEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry.getType(PhaseNextEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry.getType(PhaseNextEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case LaunchTransformationCommandEditPart.VISUAL_ID: {
			LinkedList<MethodAbstractNavigatorItem> result = new LinkedList<MethodAbstractNavigatorItem>();
			Node sv = (Node) view;
			MethodNavigatorGroup incominglinks = new MethodNavigatorGroup(
					Messages.NavigatorGroupName_LaunchTransformationCommand_2012_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(LaunchTransformationCommandLaunchTransformationCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(VariableCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(LaunchTransformationCommandLaunchTransformationCommandParametersCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(
					connectedViews,
					MethodVisualIDRegistry
							.getType(ConstantCommandParameterEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					MethodVisualIDRegistry
							.getType(ComplexCommandCommandsEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}
		}
		return EMPTY_ARRAY;
	}

	/**
	 * @generated
	 */
	private Collection<View> getLinksSourceByType(Collection<Edge> edges,
			String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (Edge nextEdge : edges) {
			View nextEdgeSource = nextEdge.getSource();
			if (type.equals(nextEdgeSource.getType())
					&& isOwnView(nextEdgeSource)) {
				result.add(nextEdgeSource);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getLinksTargetByType(Collection<Edge> edges,
			String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (Edge nextEdge : edges) {
			View nextEdgeTarget = nextEdge.getTarget();
			if (type.equals(nextEdgeTarget.getType())
					&& isOwnView(nextEdgeTarget)) {
				result.add(nextEdgeTarget);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getOutgoingLinksByType(
			Collection<? extends View> nodes, String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (View nextNode : nodes) {
			result.addAll(selectViewsByType(nextNode.getSourceEdges(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getIncomingLinksByType(
			Collection<? extends View> nodes, String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (View nextNode : nodes) {
			result.addAll(selectViewsByType(nextNode.getTargetEdges(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getChildrenByType(
			Collection<? extends View> nodes, String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (View nextNode : nodes) {
			result.addAll(selectViewsByType(nextNode.getChildren(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getDiagramLinksByType(
			Collection<Diagram> diagrams, String type) {
		ArrayList<View> result = new ArrayList<View>();
		for (Diagram nextDiagram : diagrams) {
			result.addAll(selectViewsByType(nextDiagram.getEdges(), type));
		}
		return result;
	}

	// TODO refactor as static method
	/**
	 * @generated
	 */
	private Collection<View> selectViewsByType(Collection<View> views,
			String type) {
		ArrayList<View> result = new ArrayList<View>();
		for (View nextView : views) {
			if (type.equals(nextView.getType()) && isOwnView(nextView)) {
				result.add(nextView);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return GEMDEMethodEditPart.MODEL_ID.equals(MethodVisualIDRegistry
				.getModelID(view));
	}

	/**
	 * @generated
	 */
	private Collection<MethodNavigatorItem> createNavigatorItems(
			Collection<View> views, Object parent, boolean isLeafs) {
		ArrayList<MethodNavigatorItem> result = new ArrayList<MethodNavigatorItem>(
				views.size());
		for (View nextView : views) {
			result.add(new MethodNavigatorItem(nextView, parent, isLeafs));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<MethodNavigatorItem> getForeignShortcuts(
			Diagram diagram, Object parent) {
		LinkedList<View> result = new LinkedList<View>();
		for (Iterator<View> it = diagram.getChildren().iterator(); it.hasNext();) {
			View nextView = it.next();
			if (!isOwnView(nextView)
					&& nextView.getEAnnotation("Shortcut") != null) { //$NON-NLS-1$
				result.add(nextView);
			}
		}
		return createNavigatorItems(result, parent, false);
	}

	/**
	 * @generated
	 */
	public Object getParent(Object element) {
		if (element instanceof MethodAbstractNavigatorItem) {
			MethodAbstractNavigatorItem abstractNavigatorItem = (MethodAbstractNavigatorItem) element;
			return abstractNavigatorItem.getParent();
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean hasChildren(Object element) {
		return element instanceof IFile || getChildren(element).length > 0;
	}

}
