/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.RulerGridPreferencePage;

import es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramRulersAndGridPreferencePage extends RulerGridPreferencePage {

	/**
	 * @generated
	 */
	public DiagramRulersAndGridPreferencePage() {
		setPreferenceStore(MethodDiagramEditorPlugin.getInstance()
				.getPreferenceStore());
	}
}
