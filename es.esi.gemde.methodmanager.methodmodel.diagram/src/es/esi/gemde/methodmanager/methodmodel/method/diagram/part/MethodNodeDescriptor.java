/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.part;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.tooling.runtime.update.UpdaterNodeDescriptor;

/**
 * @generated
 */
public class MethodNodeDescriptor extends UpdaterNodeDescriptor {
	/**
	 * @generated
	 */
	public MethodNodeDescriptor(EObject modelElement, int visualID) {
		super(modelElement, visualID);
	}

}
