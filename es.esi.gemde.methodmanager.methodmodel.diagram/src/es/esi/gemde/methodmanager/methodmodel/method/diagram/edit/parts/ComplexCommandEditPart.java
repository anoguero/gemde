/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.graphics.Color;

import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.policies.ComplexCommandItemSemanticEditPolicy;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodVisualIDRegistry;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.providers.MethodElementTypes;

/**
 * @generated
 */
public class ComplexCommandEditPart extends ShapeNodeEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 2014;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public ComplexCommandEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new ComplexCommandItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy lep = new org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				EditPolicy result = child
						.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated
	 */
	protected IFigure createNodeShape() {
		return primaryShape = new ComplexCommandFigure();
	}

	/**
	 * @generated
	 */
	public ComplexCommandFigure getPrimaryShape() {
		return (ComplexCommandFigure) primaryShape;
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof ComplexCommandNameEditPart) {
			((ComplexCommandNameEditPart) childEditPart)
					.setLabel(getPrimaryShape()
							.getFigureComplexCommandLabelFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof ComplexCommandNameEditPart) {
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {
		return getContentPane();
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(40, 40);
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createNodeFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane.
	 * Respects layout one may have set for generated figure.
	 * @param nodeShape instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(5);
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	protected void setForegroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setForegroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setBackgroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setBackgroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineWidth(int width) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineWidth(width);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineType(int style) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineStyle(style);
		}
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(MethodVisualIDRegistry
				.getType(ComplexCommandNameEditPart.VISUAL_ID));
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMARelTypesOnSource() {
		ArrayList<IElementType> types = new ArrayList<IElementType>(1);
		types.add(MethodElementTypes.ComplexCommandCommands_4006);
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMARelTypesOnSourceAndTarget(
			IGraphicalEditPart targetEditPart) {
		LinkedList<IElementType> types = new LinkedList<IElementType>();
		if (targetEditPart instanceof CustomCommandEditPart) {
			types.add(MethodElementTypes.ComplexCommandCommands_4006);
		}
		if (targetEditPart instanceof CallWizardCommandEditPart) {
			types.add(MethodElementTypes.ComplexCommandCommands_4006);
		}
		if (targetEditPart instanceof SelectFileCommandEditPart) {
			types.add(MethodElementTypes.ComplexCommandCommands_4006);
		}
		if (targetEditPart instanceof SelectFolderCommandEditPart) {
			types.add(MethodElementTypes.ComplexCommandCommands_4006);
		}
		if (targetEditPart instanceof SelectMetamodelCommandEditPart) {
			types.add(MethodElementTypes.ComplexCommandCommands_4006);
		}
		if (targetEditPart instanceof SelectModelElementFromFileCommandEditPart) {
			types.add(MethodElementTypes.ComplexCommandCommands_4006);
		}
		if (targetEditPart instanceof SelectModelElementFromEObjectCommandEditPart) {
			types.add(MethodElementTypes.ComplexCommandCommands_4006);
		}
		if (targetEditPart instanceof SelectValidationCommandEditPart) {
			types.add(MethodElementTypes.ComplexCommandCommands_4006);
		}
		if (targetEditPart instanceof SelectTransformationCommandEditPart) {
			types.add(MethodElementTypes.ComplexCommandCommands_4006);
		}
		if (targetEditPart instanceof LaunchValidationCommandEditPart) {
			types.add(MethodElementTypes.ComplexCommandCommands_4006);
		}
		if (targetEditPart instanceof LaunchTransformationCommandEditPart) {
			types.add(MethodElementTypes.ComplexCommandCommands_4006);
		}
		if (targetEditPart instanceof OpenEditorCommandEditPart) {
			types.add(MethodElementTypes.ComplexCommandCommands_4006);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public List<IElementType> getMATypesForTarget(IElementType relationshipType) {
		LinkedList<IElementType> types = new LinkedList<IElementType>();
		if (relationshipType == MethodElementTypes.ComplexCommandCommands_4006) {
			types.add(MethodElementTypes.CustomCommand_2002);
			types.add(MethodElementTypes.CallWizardCommand_2003);
			types.add(MethodElementTypes.SelectFileCommand_2004);
			types.add(MethodElementTypes.SelectFolderCommand_2005);
			types.add(MethodElementTypes.SelectMetamodelCommand_2006);
			types.add(MethodElementTypes.SelectModelElementFromFileCommand_2007);
			types.add(MethodElementTypes.SelectModelElementFromEObjectCommand_2008);
			types.add(MethodElementTypes.SelectValidationCommand_2009);
			types.add(MethodElementTypes.SelectTransformationCommand_2010);
			types.add(MethodElementTypes.LaunchValidationCommand_2011);
			types.add(MethodElementTypes.LaunchTransformationCommand_2012);
			types.add(MethodElementTypes.OpenEditorCommand_2013);
		}
		return types;
	}

	/**
	 * @generated
	 */
	protected void handleNotificationEvent(Notification event) {
		if (event.getNotifier() == getModel()
				&& EcorePackage.eINSTANCE.getEModelElement_EAnnotations()
						.equals(event.getFeature())) {
			handleMajorSemanticChange();
		} else {
			super.handleNotificationEvent(event);
		}
	}

	/**
	 * @generated
	 */
	public class ComplexCommandFigure extends RectangleFigure {

		/**
		 * @generated
		 */
		private WrappingLabel fFigureComplexCommandLabelFigure;

		/**
		 * @generated
		 */
		public ComplexCommandFigure() {
			this.setForegroundColor(THIS_FORE);
			this.setBackgroundColor(THIS_BACK);
			this.setBorder(new MarginBorder(getMapMode().DPtoLP(5),
					getMapMode().DPtoLP(5), getMapMode().DPtoLP(5),
					getMapMode().DPtoLP(5)));
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContents() {

			fFigureComplexCommandLabelFigure = new WrappingLabel();

			fFigureComplexCommandLabelFigure.setText("ComplexCommand");

			this.add(fFigureComplexCommandLabelFigure);

		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureComplexCommandLabelFigure() {
			return fFigureComplexCommandLabelFigure;
		}

	}

	/**
	 * @generated
	 */
	static final Color THIS_FORE = new Color(null, 102, 76, 0);

	/**
	 * @generated
	 */
	static final Color THIS_BACK = new Color(null, 230, 204, 128);

}
