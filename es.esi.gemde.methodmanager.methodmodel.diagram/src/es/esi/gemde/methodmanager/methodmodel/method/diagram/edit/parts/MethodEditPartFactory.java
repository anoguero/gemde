/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.directedit.locator.CellEditorLocatorAccess;

import es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodVisualIDRegistry;

/**
 * @generated
 */
public class MethodEditPartFactory implements EditPartFactory {

	/**
	 * @generated
	 */
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof View) {
			View view = (View) model;
			switch (MethodVisualIDRegistry.getVisualID(view)) {

			case GEMDEMethodEditPart.VISUAL_ID:
				return new GEMDEMethodEditPart(view);

			case PhaseEditPart.VISUAL_ID:
				return new PhaseEditPart(view);

			case PhaseTitleEditPart.VISUAL_ID:
				return new PhaseTitleEditPart(view);

			case CustomCommandEditPart.VISUAL_ID:
				return new CustomCommandEditPart(view);

			case CustomCommandNameEditPart.VISUAL_ID:
				return new CustomCommandNameEditPart(view);

			case CallWizardCommandEditPart.VISUAL_ID:
				return new CallWizardCommandEditPart(view);

			case CallWizardCommandNameEditPart.VISUAL_ID:
				return new CallWizardCommandNameEditPart(view);

			case SelectFileCommandEditPart.VISUAL_ID:
				return new SelectFileCommandEditPart(view);

			case SelectFileCommandNameEditPart.VISUAL_ID:
				return new SelectFileCommandNameEditPart(view);

			case SelectFolderCommandEditPart.VISUAL_ID:
				return new SelectFolderCommandEditPart(view);

			case SelectFolderCommandNameEditPart.VISUAL_ID:
				return new SelectFolderCommandNameEditPart(view);

			case SelectMetamodelCommandEditPart.VISUAL_ID:
				return new SelectMetamodelCommandEditPart(view);

			case SelectMetamodelCommandNameEditPart.VISUAL_ID:
				return new SelectMetamodelCommandNameEditPart(view);

			case SelectModelElementFromFileCommandEditPart.VISUAL_ID:
				return new SelectModelElementFromFileCommandEditPart(view);

			case SelectModelElementFromFileCommandNameEditPart.VISUAL_ID:
				return new SelectModelElementFromFileCommandNameEditPart(view);

			case SelectModelElementFromEObjectCommandEditPart.VISUAL_ID:
				return new SelectModelElementFromEObjectCommandEditPart(view);

			case SelectModelElementFromEObjectCommandNameEditPart.VISUAL_ID:
				return new SelectModelElementFromEObjectCommandNameEditPart(
						view);

			case SelectValidationCommandEditPart.VISUAL_ID:
				return new SelectValidationCommandEditPart(view);

			case SelectValidationCommandNameEditPart.VISUAL_ID:
				return new SelectValidationCommandNameEditPart(view);

			case SelectTransformationCommandEditPart.VISUAL_ID:
				return new SelectTransformationCommandEditPart(view);

			case SelectTransformationCommandNameEditPart.VISUAL_ID:
				return new SelectTransformationCommandNameEditPart(view);

			case LaunchValidationCommandEditPart.VISUAL_ID:
				return new LaunchValidationCommandEditPart(view);

			case LaunchValidationCommandNameEditPart.VISUAL_ID:
				return new LaunchValidationCommandNameEditPart(view);

			case LaunchTransformationCommandEditPart.VISUAL_ID:
				return new LaunchTransformationCommandEditPart(view);

			case LaunchTransformationCommandNameEditPart.VISUAL_ID:
				return new LaunchTransformationCommandNameEditPart(view);

			case OpenEditorCommandEditPart.VISUAL_ID:
				return new OpenEditorCommandEditPart(view);

			case OpenEditorCommandNameEditPart.VISUAL_ID:
				return new OpenEditorCommandNameEditPart(view);

			case MethodVariableEditPart.VISUAL_ID:
				return new MethodVariableEditPart(view);

			case MethodVariableNameTypeEditPart.VISUAL_ID:
				return new MethodVariableNameTypeEditPart(view);

			case ComplexCommandEditPart.VISUAL_ID:
				return new ComplexCommandEditPart(view);

			case ComplexCommandNameEditPart.VISUAL_ID:
				return new ComplexCommandNameEditPart(view);

			case TaskEditPart.VISUAL_ID:
				return new TaskEditPart(view);

			case TaskTitleEditPart.VISUAL_ID:
				return new TaskTitleEditPart(view);

			case SubTaskEditPart.VISUAL_ID:
				return new SubTaskEditPart(view);

			case SubTaskTitleEditPart.VISUAL_ID:
				return new SubTaskTitleEditPart(view);

			case ConditionalSubTaskEditPart.VISUAL_ID:
				return new ConditionalSubTaskEditPart(view);

			case ConditionalSubTaskTitleEditPart.VISUAL_ID:
				return new ConditionalSubTaskTitleEditPart(view);

			case RepeatedSubTaskEditPart.VISUAL_ID:
				return new RepeatedSubTaskEditPart(view);

			case RepeatedSubTaskTitleRepetitionVectorEditPart.VISUAL_ID:
				return new RepeatedSubTaskTitleRepetitionVectorEditPart(view);

			case VariableCommandParameterEditPart.VISUAL_ID:
				return new VariableCommandParameterEditPart(view);

			case VariableCommandParameterIdentifierEditPart.VISUAL_ID:
				return new VariableCommandParameterIdentifierEditPart(view);

			case ConstantCommandParameterEditPart.VISUAL_ID:
				return new ConstantCommandParameterEditPart(view);

			case ConstantCommandParameterIdentifierEditPart.VISUAL_ID:
				return new ConstantCommandParameterIdentifierEditPart(view);

			case PhasePhaseTasksCompartmentEditPart.VISUAL_ID:
				return new PhasePhaseTasksCompartmentEditPart(view);

			case TaskTaskSubTasksCompartmentEditPart.VISUAL_ID:
				return new TaskTaskSubTasksCompartmentEditPart(view);

			case CustomCommandCustomCommandParametersCompartmentEditPart.VISUAL_ID:
				return new CustomCommandCustomCommandParametersCompartmentEditPart(
						view);

			case CallWizardCommandCallWizardCommandParametersCompartmentEditPart.VISUAL_ID:
				return new CallWizardCommandCallWizardCommandParametersCompartmentEditPart(
						view);

			case SelectFileCommandSelectFileCommandParametersCompartmentEditPart.VISUAL_ID:
				return new SelectFileCommandSelectFileCommandParametersCompartmentEditPart(
						view);

			case SelectFolderCommandSelectFolderCommandParametersCompartmentEditPart.VISUAL_ID:
				return new SelectFolderCommandSelectFolderCommandParametersCompartmentEditPart(
						view);

			case SelectMetamodelCommandSelectMetamodelCommandParametersCompartmentEditPart.VISUAL_ID:
				return new SelectMetamodelCommandSelectMetamodelCommandParametersCompartmentEditPart(
						view);

			case SelectModelElementFromFileCommandSelectModelElementFromFileCommandParametersCompartmentEditPart.VISUAL_ID:
				return new SelectModelElementFromFileCommandSelectModelElementFromFileCommandParametersCompartmentEditPart(
						view);

			case SelectModelElementFromEObjectCommandSelectModelElementFromEObjectCommandParametersCompartmentEditPart.VISUAL_ID:
				return new SelectModelElementFromEObjectCommandSelectModelElementFromEObjectCommandParametersCompartmentEditPart(
						view);

			case SelectValidationCommandSelectValidationCommandParametersCompartmentEditPart.VISUAL_ID:
				return new SelectValidationCommandSelectValidationCommandParametersCompartmentEditPart(
						view);

			case SelectTransformationCommandSelectTransformationCommandParametersCompartmentEditPart.VISUAL_ID:
				return new SelectTransformationCommandSelectTransformationCommandParametersCompartmentEditPart(
						view);

			case LaunchValidationCommandLaunchValidationCommandParametersCompartmentEditPart.VISUAL_ID:
				return new LaunchValidationCommandLaunchValidationCommandParametersCompartmentEditPart(
						view);

			case LaunchTransformationCommandLaunchTransformationCommandParametersCompartmentEditPart.VISUAL_ID:
				return new LaunchTransformationCommandLaunchTransformationCommandParametersCompartmentEditPart(
						view);

			case OpenEditorCommandOpenEditorCommandParametersCompartmentEditPart.VISUAL_ID:
				return new OpenEditorCommandOpenEditorCommandParametersCompartmentEditPart(
						view);

			case ConditionEditPart.VISUAL_ID:
				return new ConditionEditPart(view);

			case ConditionValueEditPart.VISUAL_ID:
				return new ConditionValueEditPart(view);

			case PhaseNextEditPart.VISUAL_ID:
				return new PhaseNextEditPart(view);

			case WrappingLabelEditPart.VISUAL_ID:
				return new WrappingLabelEditPart(view);

			case TaskNextEditPart.VISUAL_ID:
				return new TaskNextEditPart(view);

			case WrappingLabel2EditPart.VISUAL_ID:
				return new WrappingLabel2EditPart(view);

			case AbstractSubTaskNextEditPart.VISUAL_ID:
				return new AbstractSubTaskNextEditPart(view);

			case WrappingLabel3EditPart.VISUAL_ID:
				return new WrappingLabel3EditPart(view);

			case RepeatedSubTaskRepeatedElementEditPart.VISUAL_ID:
				return new RepeatedSubTaskRepeatedElementEditPart(view);

			case WrappingLabel4EditPart.VISUAL_ID:
				return new WrappingLabel4EditPart(view);

			case ComplexCommandCommandsEditPart.VISUAL_ID:
				return new ComplexCommandCommandsEditPart(view);

			case WrappingLabel5EditPart.VISUAL_ID:
				return new WrappingLabel5EditPart(view);

			}
		}
		return createUnrecognizedEditPart(context, model);
	}

	/**
	 * @generated
	 */
	private EditPart createUnrecognizedEditPart(EditPart context, Object model) {
		// Handle creation of unrecognized child node EditParts here
		return null;
	}

	/**
	 * @generated
	 */
	public static CellEditorLocator getTextCellEditorLocator(
			ITextAwareEditPart source) {
		return CellEditorLocatorAccess.INSTANCE
				.getTextCellEditorLocator(source);
	}

}
