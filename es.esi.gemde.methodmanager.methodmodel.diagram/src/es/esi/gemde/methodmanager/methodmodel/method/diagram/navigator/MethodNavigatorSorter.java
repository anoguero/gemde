/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.navigator;

import org.eclipse.jface.viewers.ViewerSorter;

import es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodVisualIDRegistry;

/**
 * @generated
 */
public class MethodNavigatorSorter extends ViewerSorter {

	/**
	 * @generated
	 */
	private static final int GROUP_CATEGORY = 7016;

	/**
	 * @generated
	 */
	private static final int SHORTCUTS_CATEGORY = 7015;

	/**
	 * @generated
	 */
	public int category(Object element) {
		if (element instanceof MethodNavigatorItem) {
			MethodNavigatorItem item = (MethodNavigatorItem) element;
			if (item.getView().getEAnnotation("Shortcut") != null) { //$NON-NLS-1$
				return SHORTCUTS_CATEGORY;
			}
			return MethodVisualIDRegistry.getVisualID(item.getView());
		}
		return GROUP_CATEGORY;
	}

}
