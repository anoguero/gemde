/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.TaskCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.providers.MethodElementTypes;

/**
 * @generated
 */
public class PhasePhaseTasksCompartmentItemSemanticEditPolicy extends
		MethodBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public PhasePhaseTasksCompartmentItemSemanticEditPolicy() {
		super(MethodElementTypes.Phase_2001);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (MethodElementTypes.Task_3001 == req.getElementType()) {
			return getGEFWrapper(new TaskCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
