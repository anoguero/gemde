/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;

import es.esi.gemde.methodmanager.methodmodel.method.diagram.providers.MethodElementTypes;

/**
 * @generated
 */
public class ConditionItemSemanticEditPolicy extends
		MethodBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public ConditionItemSemanticEditPolicy() {
		super(MethodElementTypes.Condition_4001);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyElementCommand(DestroyElementRequest req) {
		return getGEFWrapper(new DestroyElementCommand(req));
	}

}
