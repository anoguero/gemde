/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts;

import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.Graphics;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ConnectionNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITreeBranchEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.graphics.Color;

import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.policies.RepeatedSubTaskRepeatedElementItemSemanticEditPolicy;

/**
 * @generated
 */
public class RepeatedSubTaskRepeatedElementEditPart extends
		ConnectionNodeEditPart implements ITreeBranchEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 4005;

	/**
	 * @generated
	 */
	public RepeatedSubTaskRepeatedElementEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new RepeatedSubTaskRepeatedElementItemSemanticEditPolicy());
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */

	protected Connection createConnectionFigure() {
		return new RepeatedSubTaskRepeatedElementFigure();
	}

	/**
	 * @generated
	 */
	public RepeatedSubTaskRepeatedElementFigure getPrimaryShape() {
		return (RepeatedSubTaskRepeatedElementFigure) getFigure();
	}

	/**
	 * @generated
	 */
	public class RepeatedSubTaskRepeatedElementFigure extends
			PolylineConnectionEx {

		/**
		 * @generated
		 */
		public RepeatedSubTaskRepeatedElementFigure() {
			this.setLineWidth(2);
			this.setLineStyle(Graphics.LINE_DOT);
			this.setForegroundColor(THIS_FORE);

		}

	}

	/**
	 * @generated
	 */
	static final Color THIS_FORE = new Color(null, 122, 163, 204);

}
