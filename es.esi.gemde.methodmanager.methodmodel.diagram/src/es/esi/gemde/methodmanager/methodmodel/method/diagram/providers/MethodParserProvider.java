/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;

import es.esi.gemde.methodmanager.methodmodel.method.MethodPackage;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CallWizardCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ComplexCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConditionValueEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConditionalSubTaskTitleEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConstantCommandParameterIdentifierEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CustomCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchTransformationCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchValidationCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.MethodVariableNameTypeEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.OpenEditorCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.PhaseTitleEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.RepeatedSubTaskTitleRepetitionVectorEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFileCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFolderCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectMetamodelCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromEObjectCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromFileCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectTransformationCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectValidationCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SubTaskTitleEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.TaskTitleEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.VariableCommandParameterIdentifierEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.parsers.MessageFormatParser;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodVisualIDRegistry;

/**
 * @generated
 */
public class MethodParserProvider extends AbstractProvider implements
		IParserProvider {

	/**
	 * @generated
	 */
	private IParser phaseTitle_5005Parser;

	/**
	 * @generated
	 */
	private IParser getPhaseTitle_5005Parser() {
		if (phaseTitle_5005Parser == null) {
			EAttribute[] features = new EAttribute[] { MethodPackage.eINSTANCE
					.getMethodStep_Title() };
			MessageFormatParser parser = new MessageFormatParser(features);
			phaseTitle_5005Parser = parser;
		}
		return phaseTitle_5005Parser;
	}

	/**
	 * @generated
	 */
	private IParser customCommandName_5008Parser;

	/**
	 * @generated
	 */
	private IParser getCustomCommandName_5008Parser() {
		if (customCommandName_5008Parser == null) {
			EAttribute[] features = new EAttribute[] { MethodPackage.eINSTANCE
					.getMethodCommand_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			customCommandName_5008Parser = parser;
		}
		return customCommandName_5008Parser;
	}

	/**
	 * @generated
	 */
	private IParser callWizardCommandName_5009Parser;

	/**
	 * @generated
	 */
	private IParser getCallWizardCommandName_5009Parser() {
		if (callWizardCommandName_5009Parser == null) {
			EAttribute[] features = new EAttribute[] { MethodPackage.eINSTANCE
					.getMethodCommand_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			callWizardCommandName_5009Parser = parser;
		}
		return callWizardCommandName_5009Parser;
	}

	/**
	 * @generated
	 */
	private IParser selectFileCommandName_5010Parser;

	/**
	 * @generated
	 */
	private IParser getSelectFileCommandName_5010Parser() {
		if (selectFileCommandName_5010Parser == null) {
			EAttribute[] features = new EAttribute[] { MethodPackage.eINSTANCE
					.getMethodCommand_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			selectFileCommandName_5010Parser = parser;
		}
		return selectFileCommandName_5010Parser;
	}

	/**
	 * @generated
	 */
	private IParser selectFolderCommandName_5011Parser;

	/**
	 * @generated
	 */
	private IParser getSelectFolderCommandName_5011Parser() {
		if (selectFolderCommandName_5011Parser == null) {
			EAttribute[] features = new EAttribute[] { MethodPackage.eINSTANCE
					.getMethodCommand_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			selectFolderCommandName_5011Parser = parser;
		}
		return selectFolderCommandName_5011Parser;
	}

	/**
	 * @generated
	 */
	private IParser selectMetamodelCommandName_5012Parser;

	/**
	 * @generated
	 */
	private IParser getSelectMetamodelCommandName_5012Parser() {
		if (selectMetamodelCommandName_5012Parser == null) {
			EAttribute[] features = new EAttribute[] { MethodPackage.eINSTANCE
					.getMethodCommand_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			selectMetamodelCommandName_5012Parser = parser;
		}
		return selectMetamodelCommandName_5012Parser;
	}

	/**
	 * @generated
	 */
	private IParser selectModelElementFromFileCommandName_5013Parser;

	/**
	 * @generated
	 */
	private IParser getSelectModelElementFromFileCommandName_5013Parser() {
		if (selectModelElementFromFileCommandName_5013Parser == null) {
			EAttribute[] features = new EAttribute[] { MethodPackage.eINSTANCE
					.getMethodCommand_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			selectModelElementFromFileCommandName_5013Parser = parser;
		}
		return selectModelElementFromFileCommandName_5013Parser;
	}

	/**
	 * @generated
	 */
	private IParser selectModelElementFromEObjectCommandName_5014Parser;

	/**
	 * @generated
	 */
	private IParser getSelectModelElementFromEObjectCommandName_5014Parser() {
		if (selectModelElementFromEObjectCommandName_5014Parser == null) {
			EAttribute[] features = new EAttribute[] { MethodPackage.eINSTANCE
					.getMethodCommand_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			selectModelElementFromEObjectCommandName_5014Parser = parser;
		}
		return selectModelElementFromEObjectCommandName_5014Parser;
	}

	/**
	 * @generated
	 */
	private IParser selectValidationCommandName_5015Parser;

	/**
	 * @generated
	 */
	private IParser getSelectValidationCommandName_5015Parser() {
		if (selectValidationCommandName_5015Parser == null) {
			EAttribute[] features = new EAttribute[] { MethodPackage.eINSTANCE
					.getMethodCommand_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			selectValidationCommandName_5015Parser = parser;
		}
		return selectValidationCommandName_5015Parser;
	}

	/**
	 * @generated
	 */
	private IParser selectTransformationCommandName_5016Parser;

	/**
	 * @generated
	 */
	private IParser getSelectTransformationCommandName_5016Parser() {
		if (selectTransformationCommandName_5016Parser == null) {
			EAttribute[] features = new EAttribute[] { MethodPackage.eINSTANCE
					.getMethodCommand_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			selectTransformationCommandName_5016Parser = parser;
		}
		return selectTransformationCommandName_5016Parser;
	}

	/**
	 * @generated
	 */
	private IParser launchValidationCommandName_5017Parser;

	/**
	 * @generated
	 */
	private IParser getLaunchValidationCommandName_5017Parser() {
		if (launchValidationCommandName_5017Parser == null) {
			EAttribute[] features = new EAttribute[] { MethodPackage.eINSTANCE
					.getMethodCommand_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			launchValidationCommandName_5017Parser = parser;
		}
		return launchValidationCommandName_5017Parser;
	}

	/**
	 * @generated
	 */
	private IParser launchTransformationCommandName_5018Parser;

	/**
	 * @generated
	 */
	private IParser getLaunchTransformationCommandName_5018Parser() {
		if (launchTransformationCommandName_5018Parser == null) {
			EAttribute[] features = new EAttribute[] { MethodPackage.eINSTANCE
					.getMethodCommand_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			launchTransformationCommandName_5018Parser = parser;
		}
		return launchTransformationCommandName_5018Parser;
	}

	/**
	 * @generated
	 */
	private IParser openEditorCommandName_5019Parser;

	/**
	 * @generated
	 */
	private IParser getOpenEditorCommandName_5019Parser() {
		if (openEditorCommandName_5019Parser == null) {
			EAttribute[] features = new EAttribute[] { MethodPackage.eINSTANCE
					.getMethodCommand_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			openEditorCommandName_5019Parser = parser;
		}
		return openEditorCommandName_5019Parser;
	}

	/**
	 * @generated
	 */
	private IParser complexCommandName_5020Parser;

	/**
	 * @generated
	 */
	private IParser getComplexCommandName_5020Parser() {
		if (complexCommandName_5020Parser == null) {
			EAttribute[] features = new EAttribute[] { MethodPackage.eINSTANCE
					.getMethodCommand_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			complexCommandName_5020Parser = parser;
		}
		return complexCommandName_5020Parser;
	}

	/**
	 * @generated
	 */
	private IParser methodVariableNameType_5021Parser;

	/**
	 * @generated
	 */
	private IParser getMethodVariableNameType_5021Parser() {
		if (methodVariableNameType_5021Parser == null) {
			EAttribute[] features = new EAttribute[] {
					MethodPackage.eINSTANCE.getMethodVariable_Name(),
					MethodPackage.eINSTANCE.getMethodVariable_Type() };
			MessageFormatParser parser = new MessageFormatParser(features);
			parser.setViewPattern("{0}:{1}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}:{1}"); //$NON-NLS-1$
			parser.setEditPattern("{0}:{1}"); //$NON-NLS-1$
			methodVariableNameType_5021Parser = parser;
		}
		return methodVariableNameType_5021Parser;
	}

	/**
	 * @generated
	 */
	private IParser taskTitle_5004Parser;

	/**
	 * @generated
	 */
	private IParser getTaskTitle_5004Parser() {
		if (taskTitle_5004Parser == null) {
			EAttribute[] features = new EAttribute[] { MethodPackage.eINSTANCE
					.getMethodStep_Title() };
			MessageFormatParser parser = new MessageFormatParser(features);
			taskTitle_5004Parser = parser;
		}
		return taskTitle_5004Parser;
	}

	/**
	 * @generated
	 */
	private IParser subTaskTitle_5001Parser;

	/**
	 * @generated
	 */
	private IParser getSubTaskTitle_5001Parser() {
		if (subTaskTitle_5001Parser == null) {
			EAttribute[] features = new EAttribute[] { MethodPackage.eINSTANCE
					.getMethodStep_Title() };
			MessageFormatParser parser = new MessageFormatParser(features);
			subTaskTitle_5001Parser = parser;
		}
		return subTaskTitle_5001Parser;
	}

	/**
	 * @generated
	 */
	private IParser conditionalSubTaskTitle_5002Parser;

	/**
	 * @generated
	 */
	private IParser getConditionalSubTaskTitle_5002Parser() {
		if (conditionalSubTaskTitle_5002Parser == null) {
			EAttribute[] features = new EAttribute[] { MethodPackage.eINSTANCE
					.getMethodStep_Title() };
			MessageFormatParser parser = new MessageFormatParser(features);
			conditionalSubTaskTitle_5002Parser = parser;
		}
		return conditionalSubTaskTitle_5002Parser;
	}

	/**
	 * @generated
	 */
	private IParser repeatedSubTaskTitleRepetitionVector_5003Parser;

	/**
	 * @generated
	 */
	private IParser getRepeatedSubTaskTitleRepetitionVector_5003Parser() {
		if (repeatedSubTaskTitleRepetitionVector_5003Parser == null) {
			EAttribute[] features = new EAttribute[] {
					MethodPackage.eINSTANCE.getMethodStep_Title(),
					MethodPackage.eINSTANCE
							.getRepeatedSubTask_RepetitionVector() };
			MessageFormatParser parser = new MessageFormatParser(features);
			parser.setViewPattern("{0} {1}"); //$NON-NLS-1$
			parser.setEditorPattern("{0} {1}"); //$NON-NLS-1$
			parser.setEditPattern("{0} {1}"); //$NON-NLS-1$
			repeatedSubTaskTitleRepetitionVector_5003Parser = parser;
		}
		return repeatedSubTaskTitleRepetitionVector_5003Parser;
	}

	/**
	 * @generated
	 */
	private IParser variableCommandParameterIdentifier_5006Parser;

	/**
	 * @generated
	 */
	private IParser getVariableCommandParameterIdentifier_5006Parser() {
		if (variableCommandParameterIdentifier_5006Parser == null) {
			EAttribute[] features = new EAttribute[] { MethodPackage.eINSTANCE
					.getCommandParameter_Identifier() };
			MessageFormatParser parser = new MessageFormatParser(features);
			variableCommandParameterIdentifier_5006Parser = parser;
		}
		return variableCommandParameterIdentifier_5006Parser;
	}

	/**
	 * @generated
	 */
	private IParser constantCommandParameterIdentifier_5007Parser;

	/**
	 * @generated
	 */
	private IParser getConstantCommandParameterIdentifier_5007Parser() {
		if (constantCommandParameterIdentifier_5007Parser == null) {
			EAttribute[] features = new EAttribute[] { MethodPackage.eINSTANCE
					.getCommandParameter_Identifier() };
			MessageFormatParser parser = new MessageFormatParser(features);
			constantCommandParameterIdentifier_5007Parser = parser;
		}
		return constantCommandParameterIdentifier_5007Parser;
	}

	/**
	 * @generated
	 */
	private IParser conditionValue_6001Parser;

	/**
	 * @generated
	 */
	private IParser getConditionValue_6001Parser() {
		if (conditionValue_6001Parser == null) {
			EAttribute[] features = new EAttribute[] { MethodPackage.eINSTANCE
					.getCondition_Value() };
			MessageFormatParser parser = new MessageFormatParser(features);
			conditionValue_6001Parser = parser;
		}
		return conditionValue_6001Parser;
	}

	/**
	 * @generated
	 */
	protected IParser getParser(int visualID) {
		switch (visualID) {
		case PhaseTitleEditPart.VISUAL_ID:
			return getPhaseTitle_5005Parser();
		case CustomCommandNameEditPart.VISUAL_ID:
			return getCustomCommandName_5008Parser();
		case CallWizardCommandNameEditPart.VISUAL_ID:
			return getCallWizardCommandName_5009Parser();
		case SelectFileCommandNameEditPart.VISUAL_ID:
			return getSelectFileCommandName_5010Parser();
		case SelectFolderCommandNameEditPart.VISUAL_ID:
			return getSelectFolderCommandName_5011Parser();
		case SelectMetamodelCommandNameEditPart.VISUAL_ID:
			return getSelectMetamodelCommandName_5012Parser();
		case SelectModelElementFromFileCommandNameEditPart.VISUAL_ID:
			return getSelectModelElementFromFileCommandName_5013Parser();
		case SelectModelElementFromEObjectCommandNameEditPart.VISUAL_ID:
			return getSelectModelElementFromEObjectCommandName_5014Parser();
		case SelectValidationCommandNameEditPart.VISUAL_ID:
			return getSelectValidationCommandName_5015Parser();
		case SelectTransformationCommandNameEditPart.VISUAL_ID:
			return getSelectTransformationCommandName_5016Parser();
		case LaunchValidationCommandNameEditPart.VISUAL_ID:
			return getLaunchValidationCommandName_5017Parser();
		case LaunchTransformationCommandNameEditPart.VISUAL_ID:
			return getLaunchTransformationCommandName_5018Parser();
		case OpenEditorCommandNameEditPart.VISUAL_ID:
			return getOpenEditorCommandName_5019Parser();
		case MethodVariableNameTypeEditPart.VISUAL_ID:
			return getMethodVariableNameType_5021Parser();
		case ComplexCommandNameEditPart.VISUAL_ID:
			return getComplexCommandName_5020Parser();
		case TaskTitleEditPart.VISUAL_ID:
			return getTaskTitle_5004Parser();
		case SubTaskTitleEditPart.VISUAL_ID:
			return getSubTaskTitle_5001Parser();
		case ConditionalSubTaskTitleEditPart.VISUAL_ID:
			return getConditionalSubTaskTitle_5002Parser();
		case RepeatedSubTaskTitleRepetitionVectorEditPart.VISUAL_ID:
			return getRepeatedSubTaskTitleRepetitionVector_5003Parser();
		case VariableCommandParameterIdentifierEditPart.VISUAL_ID:
			return getVariableCommandParameterIdentifier_5006Parser();
		case ConstantCommandParameterIdentifierEditPart.VISUAL_ID:
			return getConstantCommandParameterIdentifier_5007Parser();
		case ConditionValueEditPart.VISUAL_ID:
			return getConditionValue_6001Parser();
		}
		return null;
	}

	/**
	 * Utility method that consults ParserService
	 * @generated
	 */
	public static IParser getParser(IElementType type, EObject object,
			String parserHint) {
		return ParserService.getInstance().getParser(
				new HintAdapter(type, object, parserHint));
	}

	/**
	 * @generated
	 */
	public IParser getParser(IAdaptable hint) {
		String vid = (String) hint.getAdapter(String.class);
		if (vid != null) {
			return getParser(MethodVisualIDRegistry.getVisualID(vid));
		}
		View view = (View) hint.getAdapter(View.class);
		if (view != null) {
			return getParser(MethodVisualIDRegistry.getVisualID(view));
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean provides(IOperation operation) {
		if (operation instanceof GetParserOperation) {
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			if (MethodElementTypes.getElement(hint) == null) {
				return false;
			}
			return getParser(hint) != null;
		}
		return false;
	}

	/**
	 * @generated
	 */
	private static class HintAdapter extends ParserHintAdapter {

		/**
		 * @generated
		 */
		private final IElementType elementType;

		/**
		 * @generated
		 */
		public HintAdapter(IElementType type, EObject object, String parserHint) {
			super(object, parserHint);
			assert type != null;
			elementType = type;
		}

		/**
		 * @generated
		 */
		public Object getAdapter(Class adapter) {
			if (IElementType.class.equals(adapter)) {
				return elementType;
			}
			return super.getAdapter(adapter);
		}
	}

}
