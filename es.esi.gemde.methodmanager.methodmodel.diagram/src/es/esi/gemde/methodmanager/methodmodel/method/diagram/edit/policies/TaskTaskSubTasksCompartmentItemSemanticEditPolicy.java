/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.ConditionalSubTaskCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.RepeatedSubTaskCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.SubTaskCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.providers.MethodElementTypes;

/**
 * @generated
 */
public class TaskTaskSubTasksCompartmentItemSemanticEditPolicy extends
		MethodBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public TaskTaskSubTasksCompartmentItemSemanticEditPolicy() {
		super(MethodElementTypes.Task_3001);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (MethodElementTypes.SubTask_3002 == req.getElementType()) {
			return getGEFWrapper(new SubTaskCreateCommand(req));
		}
		if (MethodElementTypes.ConditionalSubTask_3003 == req.getElementType()) {
			return getGEFWrapper(new ConditionalSubTaskCreateCommand(req));
		}
		if (MethodElementTypes.RepeatedSubTask_3004 == req.getElementType()) {
			return getGEFWrapper(new RepeatedSubTaskCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
