/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.providers;

import es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodDiagramEditorPlugin;

/**
 * @generated
 */
public class ElementInitializers {

	protected ElementInitializers() {
		// use #getInstance to access cached instance
	}

	/**
	 * @generated
	 */
	public static ElementInitializers getInstance() {
		ElementInitializers cached = MethodDiagramEditorPlugin.getInstance()
				.getElementInitializers();
		if (cached == null) {
			MethodDiagramEditorPlugin.getInstance().setElementInitializers(
					cached = new ElementInitializers());
		}
		return cached;
	}
}
