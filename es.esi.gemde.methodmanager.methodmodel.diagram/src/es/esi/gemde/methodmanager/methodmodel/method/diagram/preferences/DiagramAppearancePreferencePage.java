/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.AppearancePreferencePage;

import es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramAppearancePreferencePage extends AppearancePreferencePage {

	/**
	 * @generated
	 */
	public DiagramAppearancePreferencePage() {
		setPreferenceStore(MethodDiagramEditorPlugin.getInstance()
				.getPreferenceStore());
	}
}
