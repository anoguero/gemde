/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.update.DiagramUpdater;

import es.esi.gemde.methodmanager.methodmodel.method.AbstractSubTask;
import es.esi.gemde.methodmanager.methodmodel.method.CallWizardCommand;
import es.esi.gemde.methodmanager.methodmodel.method.CommandParameter;
import es.esi.gemde.methodmanager.methodmodel.method.ComplexCommand;
import es.esi.gemde.methodmanager.methodmodel.method.Condition;
import es.esi.gemde.methodmanager.methodmodel.method.ConditionalSubTask;
import es.esi.gemde.methodmanager.methodmodel.method.CustomCommand;
import es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod;
import es.esi.gemde.methodmanager.methodmodel.method.LaunchTransformationCommand;
import es.esi.gemde.methodmanager.methodmodel.method.LaunchValidationCommand;
import es.esi.gemde.methodmanager.methodmodel.method.MethodCommand;
import es.esi.gemde.methodmanager.methodmodel.method.MethodPackage;
import es.esi.gemde.methodmanager.methodmodel.method.MethodVariable;
import es.esi.gemde.methodmanager.methodmodel.method.OpenEditorCommand;
import es.esi.gemde.methodmanager.methodmodel.method.Phase;
import es.esi.gemde.methodmanager.methodmodel.method.RepeatedSubTask;
import es.esi.gemde.methodmanager.methodmodel.method.SelectFileCommand;
import es.esi.gemde.methodmanager.methodmodel.method.SelectFolderCommand;
import es.esi.gemde.methodmanager.methodmodel.method.SelectMetamodelCommand;
import es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromEObjectCommand;
import es.esi.gemde.methodmanager.methodmodel.method.SelectModelElementFromFileCommand;
import es.esi.gemde.methodmanager.methodmodel.method.SelectTransformationCommand;
import es.esi.gemde.methodmanager.methodmodel.method.SelectValidationCommand;
import es.esi.gemde.methodmanager.methodmodel.method.SimpleCommand;
import es.esi.gemde.methodmanager.methodmodel.method.SubTask;
import es.esi.gemde.methodmanager.methodmodel.method.Task;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.AbstractSubTaskNextEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CallWizardCommandCallWizardCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CallWizardCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ComplexCommandCommandsEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ComplexCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConditionEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConditionalSubTaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConstantCommandParameterEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CustomCommandCustomCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CustomCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.GEMDEMethodEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchTransformationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchTransformationCommandLaunchTransformationCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchValidationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchValidationCommandLaunchValidationCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.MethodVariableEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.OpenEditorCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.OpenEditorCommandOpenEditorCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.PhaseEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.PhaseNextEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.PhasePhaseTasksCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.RepeatedSubTaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.RepeatedSubTaskRepeatedElementEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFileCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFileCommandSelectFileCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFolderCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFolderCommandSelectFolderCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectMetamodelCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectMetamodelCommandSelectMetamodelCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromEObjectCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromEObjectCommandSelectModelElementFromEObjectCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromFileCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromFileCommandSelectModelElementFromFileCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectTransformationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectTransformationCommandSelectTransformationCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectValidationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectValidationCommandSelectValidationCommandParametersCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SubTaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.TaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.TaskNextEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.TaskTaskSubTasksCompartmentEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.VariableCommandParameterEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.providers.MethodElementTypes;

/**
 * @generated
 */
public class MethodDiagramUpdater {

	/**
	 * @generated
	 */
	public static boolean isShortcutOrphaned(View view) {
		return !view.isSetElement() || view.getElement() == null
				|| view.getElement().eIsProxy();
	}

	/**
	 * @generated
	 */
	public static List<MethodNodeDescriptor> getSemanticChildren(View view) {
		switch (MethodVisualIDRegistry.getVisualID(view)) {
		case GEMDEMethodEditPart.VISUAL_ID:
			return getGEMDEMethod_1000SemanticChildren(view);
		case PhasePhaseTasksCompartmentEditPart.VISUAL_ID:
			return getPhasePhaseTasksCompartment_7001SemanticChildren(view);
		case TaskTaskSubTasksCompartmentEditPart.VISUAL_ID:
			return getTaskTaskSubTasksCompartment_7002SemanticChildren(view);
		case CustomCommandCustomCommandParametersCompartmentEditPart.VISUAL_ID:
			return getCustomCommandCustomCommandParametersCompartment_7003SemanticChildren(view);
		case CallWizardCommandCallWizardCommandParametersCompartmentEditPart.VISUAL_ID:
			return getCallWizardCommandCallWizardCommandParametersCompartment_7004SemanticChildren(view);
		case SelectFileCommandSelectFileCommandParametersCompartmentEditPart.VISUAL_ID:
			return getSelectFileCommandSelectFileCommandParametersCompartment_7005SemanticChildren(view);
		case SelectFolderCommandSelectFolderCommandParametersCompartmentEditPart.VISUAL_ID:
			return getSelectFolderCommandSelectFolderCommandParametersCompartment_7006SemanticChildren(view);
		case SelectMetamodelCommandSelectMetamodelCommandParametersCompartmentEditPart.VISUAL_ID:
			return getSelectMetamodelCommandSelectMetamodelCommandParametersCompartment_7007SemanticChildren(view);
		case SelectModelElementFromFileCommandSelectModelElementFromFileCommandParametersCompartmentEditPart.VISUAL_ID:
			return getSelectModelElementFromFileCommandSelectModelElementFromFileCommandParametersCompartment_7008SemanticChildren(view);
		case SelectModelElementFromEObjectCommandSelectModelElementFromEObjectCommandParametersCompartmentEditPart.VISUAL_ID:
			return getSelectModelElementFromEObjectCommandSelectModelElementFromEObjectCommandParametersCompartment_7009SemanticChildren(view);
		case SelectValidationCommandSelectValidationCommandParametersCompartmentEditPart.VISUAL_ID:
			return getSelectValidationCommandSelectValidationCommandParametersCompartment_7010SemanticChildren(view);
		case SelectTransformationCommandSelectTransformationCommandParametersCompartmentEditPart.VISUAL_ID:
			return getSelectTransformationCommandSelectTransformationCommandParametersCompartment_7011SemanticChildren(view);
		case LaunchValidationCommandLaunchValidationCommandParametersCompartmentEditPart.VISUAL_ID:
			return getLaunchValidationCommandLaunchValidationCommandParametersCompartment_7012SemanticChildren(view);
		case LaunchTransformationCommandLaunchTransformationCommandParametersCompartmentEditPart.VISUAL_ID:
			return getLaunchTransformationCommandLaunchTransformationCommandParametersCompartment_7013SemanticChildren(view);
		case OpenEditorCommandOpenEditorCommandParametersCompartmentEditPart.VISUAL_ID:
			return getOpenEditorCommandOpenEditorCommandParametersCompartment_7014SemanticChildren(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodNodeDescriptor> getGEMDEMethod_1000SemanticChildren(
			View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		GEMDEMethod modelElement = (GEMDEMethod) view.getElement();
		LinkedList<MethodNodeDescriptor> result = new LinkedList<MethodNodeDescriptor>();
		for (Iterator<?> it = modelElement.getPhases().iterator(); it.hasNext();) {
			Phase childElement = (Phase) it.next();
			int visualID = MethodVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == PhaseEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getCommands().iterator(); it
				.hasNext();) {
			MethodCommand childElement = (MethodCommand) it.next();
			int visualID = MethodVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == CustomCommandEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == CallWizardCommandEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == SelectFileCommandEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == SelectFolderCommandEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == SelectMetamodelCommandEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == SelectModelElementFromFileCommandEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == SelectModelElementFromEObjectCommandEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == SelectValidationCommandEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == SelectTransformationCommandEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == LaunchValidationCommandEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == LaunchTransformationCommandEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == OpenEditorCommandEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ComplexCommandEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getVariables().iterator(); it
				.hasNext();) {
			MethodVariable childElement = (MethodVariable) it.next();
			int visualID = MethodVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == MethodVariableEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodNodeDescriptor> getPhasePhaseTasksCompartment_7001SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		Phase modelElement = (Phase) containerView.getElement();
		LinkedList<MethodNodeDescriptor> result = new LinkedList<MethodNodeDescriptor>();
		for (Iterator<?> it = modelElement.getTasks().iterator(); it.hasNext();) {
			Task childElement = (Task) it.next();
			int visualID = MethodVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == TaskEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodNodeDescriptor> getTaskTaskSubTasksCompartment_7002SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		Task modelElement = (Task) containerView.getElement();
		LinkedList<MethodNodeDescriptor> result = new LinkedList<MethodNodeDescriptor>();
		for (Iterator<?> it = modelElement.getSubTasks().iterator(); it
				.hasNext();) {
			AbstractSubTask childElement = (AbstractSubTask) it.next();
			int visualID = MethodVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == SubTaskEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ConditionalSubTaskEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == RepeatedSubTaskEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodNodeDescriptor> getCustomCommandCustomCommandParametersCompartment_7003SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		CustomCommand modelElement = (CustomCommand) containerView.getElement();
		LinkedList<MethodNodeDescriptor> result = new LinkedList<MethodNodeDescriptor>();
		for (Iterator<?> it = modelElement.getParameters().iterator(); it
				.hasNext();) {
			CommandParameter childElement = (CommandParameter) it.next();
			int visualID = MethodVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == VariableCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ConstantCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodNodeDescriptor> getCallWizardCommandCallWizardCommandParametersCompartment_7004SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		CallWizardCommand modelElement = (CallWizardCommand) containerView
				.getElement();
		LinkedList<MethodNodeDescriptor> result = new LinkedList<MethodNodeDescriptor>();
		for (Iterator<?> it = modelElement.getParameters().iterator(); it
				.hasNext();) {
			CommandParameter childElement = (CommandParameter) it.next();
			int visualID = MethodVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == VariableCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ConstantCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodNodeDescriptor> getSelectFileCommandSelectFileCommandParametersCompartment_7005SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		SelectFileCommand modelElement = (SelectFileCommand) containerView
				.getElement();
		LinkedList<MethodNodeDescriptor> result = new LinkedList<MethodNodeDescriptor>();
		for (Iterator<?> it = modelElement.getParameters().iterator(); it
				.hasNext();) {
			CommandParameter childElement = (CommandParameter) it.next();
			int visualID = MethodVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == VariableCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ConstantCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodNodeDescriptor> getSelectFolderCommandSelectFolderCommandParametersCompartment_7006SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		SelectFolderCommand modelElement = (SelectFolderCommand) containerView
				.getElement();
		LinkedList<MethodNodeDescriptor> result = new LinkedList<MethodNodeDescriptor>();
		for (Iterator<?> it = modelElement.getParameters().iterator(); it
				.hasNext();) {
			CommandParameter childElement = (CommandParameter) it.next();
			int visualID = MethodVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == VariableCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ConstantCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodNodeDescriptor> getSelectMetamodelCommandSelectMetamodelCommandParametersCompartment_7007SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		SelectMetamodelCommand modelElement = (SelectMetamodelCommand) containerView
				.getElement();
		LinkedList<MethodNodeDescriptor> result = new LinkedList<MethodNodeDescriptor>();
		for (Iterator<?> it = modelElement.getParameters().iterator(); it
				.hasNext();) {
			CommandParameter childElement = (CommandParameter) it.next();
			int visualID = MethodVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == VariableCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ConstantCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodNodeDescriptor> getSelectModelElementFromFileCommandSelectModelElementFromFileCommandParametersCompartment_7008SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		SelectModelElementFromFileCommand modelElement = (SelectModelElementFromFileCommand) containerView
				.getElement();
		LinkedList<MethodNodeDescriptor> result = new LinkedList<MethodNodeDescriptor>();
		for (Iterator<?> it = modelElement.getParameters().iterator(); it
				.hasNext();) {
			CommandParameter childElement = (CommandParameter) it.next();
			int visualID = MethodVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == VariableCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ConstantCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodNodeDescriptor> getSelectModelElementFromEObjectCommandSelectModelElementFromEObjectCommandParametersCompartment_7009SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		SelectModelElementFromEObjectCommand modelElement = (SelectModelElementFromEObjectCommand) containerView
				.getElement();
		LinkedList<MethodNodeDescriptor> result = new LinkedList<MethodNodeDescriptor>();
		for (Iterator<?> it = modelElement.getParameters().iterator(); it
				.hasNext();) {
			CommandParameter childElement = (CommandParameter) it.next();
			int visualID = MethodVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == VariableCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ConstantCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodNodeDescriptor> getSelectValidationCommandSelectValidationCommandParametersCompartment_7010SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		SelectValidationCommand modelElement = (SelectValidationCommand) containerView
				.getElement();
		LinkedList<MethodNodeDescriptor> result = new LinkedList<MethodNodeDescriptor>();
		for (Iterator<?> it = modelElement.getParameters().iterator(); it
				.hasNext();) {
			CommandParameter childElement = (CommandParameter) it.next();
			int visualID = MethodVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == VariableCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ConstantCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodNodeDescriptor> getSelectTransformationCommandSelectTransformationCommandParametersCompartment_7011SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		SelectTransformationCommand modelElement = (SelectTransformationCommand) containerView
				.getElement();
		LinkedList<MethodNodeDescriptor> result = new LinkedList<MethodNodeDescriptor>();
		for (Iterator<?> it = modelElement.getParameters().iterator(); it
				.hasNext();) {
			CommandParameter childElement = (CommandParameter) it.next();
			int visualID = MethodVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == VariableCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ConstantCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodNodeDescriptor> getLaunchValidationCommandLaunchValidationCommandParametersCompartment_7012SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		LaunchValidationCommand modelElement = (LaunchValidationCommand) containerView
				.getElement();
		LinkedList<MethodNodeDescriptor> result = new LinkedList<MethodNodeDescriptor>();
		for (Iterator<?> it = modelElement.getParameters().iterator(); it
				.hasNext();) {
			CommandParameter childElement = (CommandParameter) it.next();
			int visualID = MethodVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == VariableCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ConstantCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodNodeDescriptor> getLaunchTransformationCommandLaunchTransformationCommandParametersCompartment_7013SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		LaunchTransformationCommand modelElement = (LaunchTransformationCommand) containerView
				.getElement();
		LinkedList<MethodNodeDescriptor> result = new LinkedList<MethodNodeDescriptor>();
		for (Iterator<?> it = modelElement.getParameters().iterator(); it
				.hasNext();) {
			CommandParameter childElement = (CommandParameter) it.next();
			int visualID = MethodVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == VariableCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ConstantCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodNodeDescriptor> getOpenEditorCommandOpenEditorCommandParametersCompartment_7014SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		OpenEditorCommand modelElement = (OpenEditorCommand) containerView
				.getElement();
		LinkedList<MethodNodeDescriptor> result = new LinkedList<MethodNodeDescriptor>();
		for (Iterator<?> it = modelElement.getParameters().iterator(); it
				.hasNext();) {
			CommandParameter childElement = (CommandParameter) it.next();
			int visualID = MethodVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == VariableCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ConstantCommandParameterEditPart.VISUAL_ID) {
				result.add(new MethodNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getContainedLinks(View view) {
		switch (MethodVisualIDRegistry.getVisualID(view)) {
		case GEMDEMethodEditPart.VISUAL_ID:
			return getGEMDEMethod_1000ContainedLinks(view);
		case PhaseEditPart.VISUAL_ID:
			return getPhase_2001ContainedLinks(view);
		case CustomCommandEditPart.VISUAL_ID:
			return getCustomCommand_2002ContainedLinks(view);
		case CallWizardCommandEditPart.VISUAL_ID:
			return getCallWizardCommand_2003ContainedLinks(view);
		case SelectFileCommandEditPart.VISUAL_ID:
			return getSelectFileCommand_2004ContainedLinks(view);
		case SelectFolderCommandEditPart.VISUAL_ID:
			return getSelectFolderCommand_2005ContainedLinks(view);
		case SelectMetamodelCommandEditPart.VISUAL_ID:
			return getSelectMetamodelCommand_2006ContainedLinks(view);
		case SelectModelElementFromFileCommandEditPart.VISUAL_ID:
			return getSelectModelElementFromFileCommand_2007ContainedLinks(view);
		case SelectModelElementFromEObjectCommandEditPart.VISUAL_ID:
			return getSelectModelElementFromEObjectCommand_2008ContainedLinks(view);
		case SelectValidationCommandEditPart.VISUAL_ID:
			return getSelectValidationCommand_2009ContainedLinks(view);
		case SelectTransformationCommandEditPart.VISUAL_ID:
			return getSelectTransformationCommand_2010ContainedLinks(view);
		case LaunchValidationCommandEditPart.VISUAL_ID:
			return getLaunchValidationCommand_2011ContainedLinks(view);
		case LaunchTransformationCommandEditPart.VISUAL_ID:
			return getLaunchTransformationCommand_2012ContainedLinks(view);
		case OpenEditorCommandEditPart.VISUAL_ID:
			return getOpenEditorCommand_2013ContainedLinks(view);
		case MethodVariableEditPart.VISUAL_ID:
			return getMethodVariable_2015ContainedLinks(view);
		case ComplexCommandEditPart.VISUAL_ID:
			return getComplexCommand_2014ContainedLinks(view);
		case TaskEditPart.VISUAL_ID:
			return getTask_3001ContainedLinks(view);
		case SubTaskEditPart.VISUAL_ID:
			return getSubTask_3002ContainedLinks(view);
		case ConditionalSubTaskEditPart.VISUAL_ID:
			return getConditionalSubTask_3003ContainedLinks(view);
		case RepeatedSubTaskEditPart.VISUAL_ID:
			return getRepeatedSubTask_3004ContainedLinks(view);
		case VariableCommandParameterEditPart.VISUAL_ID:
			return getVariableCommandParameter_3005ContainedLinks(view);
		case ConstantCommandParameterEditPart.VISUAL_ID:
			return getConstantCommandParameter_3006ContainedLinks(view);
		case ConditionEditPart.VISUAL_ID:
			return getCondition_4001ContainedLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getIncomingLinks(View view) {
		switch (MethodVisualIDRegistry.getVisualID(view)) {
		case PhaseEditPart.VISUAL_ID:
			return getPhase_2001IncomingLinks(view);
		case CustomCommandEditPart.VISUAL_ID:
			return getCustomCommand_2002IncomingLinks(view);
		case CallWizardCommandEditPart.VISUAL_ID:
			return getCallWizardCommand_2003IncomingLinks(view);
		case SelectFileCommandEditPart.VISUAL_ID:
			return getSelectFileCommand_2004IncomingLinks(view);
		case SelectFolderCommandEditPart.VISUAL_ID:
			return getSelectFolderCommand_2005IncomingLinks(view);
		case SelectMetamodelCommandEditPart.VISUAL_ID:
			return getSelectMetamodelCommand_2006IncomingLinks(view);
		case SelectModelElementFromFileCommandEditPart.VISUAL_ID:
			return getSelectModelElementFromFileCommand_2007IncomingLinks(view);
		case SelectModelElementFromEObjectCommandEditPart.VISUAL_ID:
			return getSelectModelElementFromEObjectCommand_2008IncomingLinks(view);
		case SelectValidationCommandEditPart.VISUAL_ID:
			return getSelectValidationCommand_2009IncomingLinks(view);
		case SelectTransformationCommandEditPart.VISUAL_ID:
			return getSelectTransformationCommand_2010IncomingLinks(view);
		case LaunchValidationCommandEditPart.VISUAL_ID:
			return getLaunchValidationCommand_2011IncomingLinks(view);
		case LaunchTransformationCommandEditPart.VISUAL_ID:
			return getLaunchTransformationCommand_2012IncomingLinks(view);
		case OpenEditorCommandEditPart.VISUAL_ID:
			return getOpenEditorCommand_2013IncomingLinks(view);
		case MethodVariableEditPart.VISUAL_ID:
			return getMethodVariable_2015IncomingLinks(view);
		case ComplexCommandEditPart.VISUAL_ID:
			return getComplexCommand_2014IncomingLinks(view);
		case TaskEditPart.VISUAL_ID:
			return getTask_3001IncomingLinks(view);
		case SubTaskEditPart.VISUAL_ID:
			return getSubTask_3002IncomingLinks(view);
		case ConditionalSubTaskEditPart.VISUAL_ID:
			return getConditionalSubTask_3003IncomingLinks(view);
		case RepeatedSubTaskEditPart.VISUAL_ID:
			return getRepeatedSubTask_3004IncomingLinks(view);
		case VariableCommandParameterEditPart.VISUAL_ID:
			return getVariableCommandParameter_3005IncomingLinks(view);
		case ConstantCommandParameterEditPart.VISUAL_ID:
			return getConstantCommandParameter_3006IncomingLinks(view);
		case ConditionEditPart.VISUAL_ID:
			return getCondition_4001IncomingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getOutgoingLinks(View view) {
		switch (MethodVisualIDRegistry.getVisualID(view)) {
		case PhaseEditPart.VISUAL_ID:
			return getPhase_2001OutgoingLinks(view);
		case CustomCommandEditPart.VISUAL_ID:
			return getCustomCommand_2002OutgoingLinks(view);
		case CallWizardCommandEditPart.VISUAL_ID:
			return getCallWizardCommand_2003OutgoingLinks(view);
		case SelectFileCommandEditPart.VISUAL_ID:
			return getSelectFileCommand_2004OutgoingLinks(view);
		case SelectFolderCommandEditPart.VISUAL_ID:
			return getSelectFolderCommand_2005OutgoingLinks(view);
		case SelectMetamodelCommandEditPart.VISUAL_ID:
			return getSelectMetamodelCommand_2006OutgoingLinks(view);
		case SelectModelElementFromFileCommandEditPart.VISUAL_ID:
			return getSelectModelElementFromFileCommand_2007OutgoingLinks(view);
		case SelectModelElementFromEObjectCommandEditPart.VISUAL_ID:
			return getSelectModelElementFromEObjectCommand_2008OutgoingLinks(view);
		case SelectValidationCommandEditPart.VISUAL_ID:
			return getSelectValidationCommand_2009OutgoingLinks(view);
		case SelectTransformationCommandEditPart.VISUAL_ID:
			return getSelectTransformationCommand_2010OutgoingLinks(view);
		case LaunchValidationCommandEditPart.VISUAL_ID:
			return getLaunchValidationCommand_2011OutgoingLinks(view);
		case LaunchTransformationCommandEditPart.VISUAL_ID:
			return getLaunchTransformationCommand_2012OutgoingLinks(view);
		case OpenEditorCommandEditPart.VISUAL_ID:
			return getOpenEditorCommand_2013OutgoingLinks(view);
		case MethodVariableEditPart.VISUAL_ID:
			return getMethodVariable_2015OutgoingLinks(view);
		case ComplexCommandEditPart.VISUAL_ID:
			return getComplexCommand_2014OutgoingLinks(view);
		case TaskEditPart.VISUAL_ID:
			return getTask_3001OutgoingLinks(view);
		case SubTaskEditPart.VISUAL_ID:
			return getSubTask_3002OutgoingLinks(view);
		case ConditionalSubTaskEditPart.VISUAL_ID:
			return getConditionalSubTask_3003OutgoingLinks(view);
		case RepeatedSubTaskEditPart.VISUAL_ID:
			return getRepeatedSubTask_3004OutgoingLinks(view);
		case VariableCommandParameterEditPart.VISUAL_ID:
			return getVariableCommandParameter_3005OutgoingLinks(view);
		case ConstantCommandParameterEditPart.VISUAL_ID:
			return getConstantCommandParameter_3006OutgoingLinks(view);
		case ConditionEditPart.VISUAL_ID:
			return getCondition_4001OutgoingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getGEMDEMethod_1000ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getPhase_2001ContainedLinks(
			View view) {
		Phase modelElement = (Phase) view.getElement();
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_Phase_Next_4002(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getCustomCommand_2002ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getCallWizardCommand_2003ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectFileCommand_2004ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectFolderCommand_2005ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectMetamodelCommand_2006ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectModelElementFromFileCommand_2007ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectModelElementFromEObjectCommand_2008ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectValidationCommand_2009ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectTransformationCommand_2010ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getLaunchValidationCommand_2011ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getLaunchTransformationCommand_2012ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getOpenEditorCommand_2013ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getComplexCommand_2014ContainedLinks(
			View view) {
		ComplexCommand modelElement = (ComplexCommand) view.getElement();
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_ComplexCommand_Commands_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getMethodVariable_2015ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getTask_3001ContainedLinks(
			View view) {
		Task modelElement = (Task) view.getElement();
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_Task_Next_4003(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSubTask_3002ContainedLinks(
			View view) {
		SubTask modelElement = (SubTask) view.getElement();
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_AbstractSubTask_Next_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getConditionalSubTask_3003ContainedLinks(
			View view) {
		ConditionalSubTask modelElement = (ConditionalSubTask) view
				.getElement();
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_Condition_4001(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_AbstractSubTask_Next_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getRepeatedSubTask_3004ContainedLinks(
			View view) {
		RepeatedSubTask modelElement = (RepeatedSubTask) view.getElement();
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_AbstractSubTask_Next_4004(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_RepeatedSubTask_RepeatedElement_4005(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getVariableCommandParameter_3005ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getConstantCommandParameter_3006ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getCondition_4001ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getPhase_2001IncomingLinks(
			View view) {
		Phase modelElement = (Phase) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_Phase_Next_4002(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getCustomCommand_2002IncomingLinks(
			View view) {
		CustomCommand modelElement = (CustomCommand) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_ComplexCommand_Commands_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getCallWizardCommand_2003IncomingLinks(
			View view) {
		CallWizardCommand modelElement = (CallWizardCommand) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_ComplexCommand_Commands_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectFileCommand_2004IncomingLinks(
			View view) {
		SelectFileCommand modelElement = (SelectFileCommand) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_ComplexCommand_Commands_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectFolderCommand_2005IncomingLinks(
			View view) {
		SelectFolderCommand modelElement = (SelectFolderCommand) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_ComplexCommand_Commands_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectMetamodelCommand_2006IncomingLinks(
			View view) {
		SelectMetamodelCommand modelElement = (SelectMetamodelCommand) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_ComplexCommand_Commands_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectModelElementFromFileCommand_2007IncomingLinks(
			View view) {
		SelectModelElementFromFileCommand modelElement = (SelectModelElementFromFileCommand) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_ComplexCommand_Commands_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectModelElementFromEObjectCommand_2008IncomingLinks(
			View view) {
		SelectModelElementFromEObjectCommand modelElement = (SelectModelElementFromEObjectCommand) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_ComplexCommand_Commands_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectValidationCommand_2009IncomingLinks(
			View view) {
		SelectValidationCommand modelElement = (SelectValidationCommand) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_ComplexCommand_Commands_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectTransformationCommand_2010IncomingLinks(
			View view) {
		SelectTransformationCommand modelElement = (SelectTransformationCommand) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_ComplexCommand_Commands_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getLaunchValidationCommand_2011IncomingLinks(
			View view) {
		LaunchValidationCommand modelElement = (LaunchValidationCommand) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_ComplexCommand_Commands_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getLaunchTransformationCommand_2012IncomingLinks(
			View view) {
		LaunchTransformationCommand modelElement = (LaunchTransformationCommand) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_ComplexCommand_Commands_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getOpenEditorCommand_2013IncomingLinks(
			View view) {
		OpenEditorCommand modelElement = (OpenEditorCommand) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_ComplexCommand_Commands_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getComplexCommand_2014IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getMethodVariable_2015IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getTask_3001IncomingLinks(View view) {
		Task modelElement = (Task) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_Task_Next_4003(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSubTask_3002IncomingLinks(
			View view) {
		SubTask modelElement = (SubTask) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Condition_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingFeatureModelFacetLinks_AbstractSubTask_Next_4004(
				modelElement, crossReferences));
		result.addAll(getIncomingFeatureModelFacetLinks_RepeatedSubTask_RepeatedElement_4005(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getConditionalSubTask_3003IncomingLinks(
			View view) {
		ConditionalSubTask modelElement = (ConditionalSubTask) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_AbstractSubTask_Next_4004(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getRepeatedSubTask_3004IncomingLinks(
			View view) {
		RepeatedSubTask modelElement = (RepeatedSubTask) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_AbstractSubTask_Next_4004(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getVariableCommandParameter_3005IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getConstantCommandParameter_3006IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getCondition_4001IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getPhase_2001OutgoingLinks(
			View view) {
		Phase modelElement = (Phase) view.getElement();
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_Phase_Next_4002(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getCustomCommand_2002OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getCallWizardCommand_2003OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectFileCommand_2004OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectFolderCommand_2005OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectMetamodelCommand_2006OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectModelElementFromFileCommand_2007OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectModelElementFromEObjectCommand_2008OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectValidationCommand_2009OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSelectTransformationCommand_2010OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getLaunchValidationCommand_2011OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getLaunchTransformationCommand_2012OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getOpenEditorCommand_2013OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getComplexCommand_2014OutgoingLinks(
			View view) {
		ComplexCommand modelElement = (ComplexCommand) view.getElement();
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_ComplexCommand_Commands_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getMethodVariable_2015OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getTask_3001OutgoingLinks(View view) {
		Task modelElement = (Task) view.getElement();
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_Task_Next_4003(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getSubTask_3002OutgoingLinks(
			View view) {
		SubTask modelElement = (SubTask) view.getElement();
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_AbstractSubTask_Next_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getConditionalSubTask_3003OutgoingLinks(
			View view) {
		ConditionalSubTask modelElement = (ConditionalSubTask) view
				.getElement();
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Condition_4001(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_AbstractSubTask_Next_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getRepeatedSubTask_3004OutgoingLinks(
			View view) {
		RepeatedSubTask modelElement = (RepeatedSubTask) view.getElement();
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_AbstractSubTask_Next_4004(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_RepeatedSubTask_RepeatedElement_4005(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getVariableCommandParameter_3005OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getConstantCommandParameter_3006OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<MethodLinkDescriptor> getCondition_4001OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	private static Collection<MethodLinkDescriptor> getContainedTypeModelFacetLinks_Condition_4001(
			ConditionalSubTask container) {
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		for (Iterator<?> links = container.getConditionalLinks().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Condition) {
				continue;
			}
			Condition link = (Condition) linkObject;
			if (ConditionEditPart.VISUAL_ID != MethodVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			SubTask dst = link.getTo();
			ConditionalSubTask src = link.getFrom();
			result.add(new MethodLinkDescriptor(src, dst, link,
					MethodElementTypes.Condition_4001,
					ConditionEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<MethodLinkDescriptor> getIncomingTypeModelFacetLinks_Condition_4001(
			SubTask target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != MethodPackage.eINSTANCE
					.getCondition_To()
					|| false == setting.getEObject() instanceof Condition) {
				continue;
			}
			Condition link = (Condition) setting.getEObject();
			if (ConditionEditPart.VISUAL_ID != MethodVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			ConditionalSubTask src = link.getFrom();
			result.add(new MethodLinkDescriptor(src, target, link,
					MethodElementTypes.Condition_4001,
					ConditionEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<MethodLinkDescriptor> getIncomingFeatureModelFacetLinks_Phase_Next_4002(
			Phase target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == MethodPackage.eINSTANCE
					.getPhase_Next()) {
				result.add(new MethodLinkDescriptor(setting.getEObject(),
						target, MethodElementTypes.PhaseNext_4002,
						PhaseNextEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<MethodLinkDescriptor> getIncomingFeatureModelFacetLinks_Task_Next_4003(
			Task target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == MethodPackage.eINSTANCE
					.getTask_Next()) {
				result.add(new MethodLinkDescriptor(setting.getEObject(),
						target, MethodElementTypes.TaskNext_4003,
						TaskNextEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<MethodLinkDescriptor> getIncomingFeatureModelFacetLinks_AbstractSubTask_Next_4004(
			AbstractSubTask target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == MethodPackage.eINSTANCE
					.getAbstractSubTask_Next()) {
				result.add(new MethodLinkDescriptor(setting.getEObject(),
						target, MethodElementTypes.AbstractSubTaskNext_4004,
						AbstractSubTaskNextEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<MethodLinkDescriptor> getIncomingFeatureModelFacetLinks_RepeatedSubTask_RepeatedElement_4005(
			SubTask target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == MethodPackage.eINSTANCE
					.getRepeatedSubTask_RepeatedElement()) {
				result.add(new MethodLinkDescriptor(setting.getEObject(),
						target,
						MethodElementTypes.RepeatedSubTaskRepeatedElement_4005,
						RepeatedSubTaskRepeatedElementEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<MethodLinkDescriptor> getIncomingFeatureModelFacetLinks_ComplexCommand_Commands_4006(
			SimpleCommand target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == MethodPackage.eINSTANCE
					.getComplexCommand_Commands()) {
				result.add(new MethodLinkDescriptor(setting.getEObject(),
						target, MethodElementTypes.ComplexCommandCommands_4006,
						ComplexCommandCommandsEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<MethodLinkDescriptor> getOutgoingTypeModelFacetLinks_Condition_4001(
			ConditionalSubTask source) {
		ConditionalSubTask container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof ConditionalSubTask) {
				container = (ConditionalSubTask) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		for (Iterator<?> links = container.getConditionalLinks().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Condition) {
				continue;
			}
			Condition link = (Condition) linkObject;
			if (ConditionEditPart.VISUAL_ID != MethodVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			SubTask dst = link.getTo();
			ConditionalSubTask src = link.getFrom();
			if (src != source) {
				continue;
			}
			result.add(new MethodLinkDescriptor(src, dst, link,
					MethodElementTypes.Condition_4001,
					ConditionEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<MethodLinkDescriptor> getOutgoingFeatureModelFacetLinks_Phase_Next_4002(
			Phase source) {
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		Phase destination = source.getNext();
		if (destination == null) {
			return result;
		}
		result.add(new MethodLinkDescriptor(source, destination,
				MethodElementTypes.PhaseNext_4002, PhaseNextEditPart.VISUAL_ID));
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<MethodLinkDescriptor> getOutgoingFeatureModelFacetLinks_Task_Next_4003(
			Task source) {
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		Task destination = source.getNext();
		if (destination == null) {
			return result;
		}
		result.add(new MethodLinkDescriptor(source, destination,
				MethodElementTypes.TaskNext_4003, TaskNextEditPart.VISUAL_ID));
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<MethodLinkDescriptor> getOutgoingFeatureModelFacetLinks_AbstractSubTask_Next_4004(
			AbstractSubTask source) {
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		AbstractSubTask destination = source.getNext();
		if (destination == null) {
			return result;
		}
		result.add(new MethodLinkDescriptor(source, destination,
				MethodElementTypes.AbstractSubTaskNext_4004,
				AbstractSubTaskNextEditPart.VISUAL_ID));
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<MethodLinkDescriptor> getOutgoingFeatureModelFacetLinks_RepeatedSubTask_RepeatedElement_4005(
			RepeatedSubTask source) {
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		SubTask destination = source.getRepeatedElement();
		if (destination == null) {
			return result;
		}
		result.add(new MethodLinkDescriptor(source, destination,
				MethodElementTypes.RepeatedSubTaskRepeatedElement_4005,
				RepeatedSubTaskRepeatedElementEditPart.VISUAL_ID));
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<MethodLinkDescriptor> getOutgoingFeatureModelFacetLinks_ComplexCommand_Commands_4006(
			ComplexCommand source) {
		LinkedList<MethodLinkDescriptor> result = new LinkedList<MethodLinkDescriptor>();
		for (Iterator<?> destinations = source.getCommands().iterator(); destinations
				.hasNext();) {
			SimpleCommand destination = (SimpleCommand) destinations.next();
			result.add(new MethodLinkDescriptor(source, destination,
					MethodElementTypes.ComplexCommandCommands_4006,
					ComplexCommandCommandsEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static final DiagramUpdater TYPED_INSTANCE = new DiagramUpdater() {
		/**
		 * @generated
		 */
		@Override
		public List<MethodNodeDescriptor> getSemanticChildren(View view) {
			return MethodDiagramUpdater.getSemanticChildren(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<MethodLinkDescriptor> getContainedLinks(View view) {
			return MethodDiagramUpdater.getContainedLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<MethodLinkDescriptor> getIncomingLinks(View view) {
			return MethodDiagramUpdater.getIncomingLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<MethodLinkDescriptor> getOutgoingLinks(View view) {
			return MethodDiagramUpdater.getOutgoingLinks(view);
		}
	};

}
