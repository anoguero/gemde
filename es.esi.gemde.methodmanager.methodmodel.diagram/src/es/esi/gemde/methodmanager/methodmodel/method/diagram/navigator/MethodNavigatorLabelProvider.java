/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.navigator;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.common.ui.services.parser.CommonParserHint;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;

import es.esi.gemde.methodmanager.methodmodel.method.GEMDEMethod;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.AbstractSubTaskNextEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CallWizardCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CallWizardCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ComplexCommandCommandsEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ComplexCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ComplexCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConditionEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConditionValueEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConditionalSubTaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConditionalSubTaskTitleEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConstantCommandParameterEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConstantCommandParameterIdentifierEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CustomCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CustomCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.GEMDEMethodEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchTransformationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchTransformationCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchValidationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchValidationCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.MethodVariableEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.MethodVariableNameTypeEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.OpenEditorCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.OpenEditorCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.PhaseEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.PhaseNextEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.PhaseTitleEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.RepeatedSubTaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.RepeatedSubTaskRepeatedElementEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.RepeatedSubTaskTitleRepetitionVectorEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFileCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFileCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFolderCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFolderCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectMetamodelCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectMetamodelCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromEObjectCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromEObjectCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromFileCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromFileCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectTransformationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectTransformationCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectValidationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectValidationCommandNameEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SubTaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SubTaskTitleEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.TaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.TaskNextEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.TaskTitleEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.VariableCommandParameterEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.VariableCommandParameterIdentifierEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodDiagramEditorPlugin;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodVisualIDRegistry;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.providers.MethodElementTypes;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.providers.MethodParserProvider;

/**
 * @generated
 */
public class MethodNavigatorLabelProvider extends LabelProvider implements
		ICommonLabelProvider, ITreePathLabelProvider {

	/**
	 * @generated
	 */
	static {
		MethodDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?UnknownElement", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
		MethodDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?ImageNotFound", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof MethodNavigatorItem
				&& !isOwnView(((MethodNavigatorItem) element).getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	 * @generated
	 */
	public Image getImage(Object element) {
		if (element instanceof MethodNavigatorGroup) {
			MethodNavigatorGroup group = (MethodNavigatorGroup) element;
			return MethodDiagramEditorPlugin.getInstance().getBundledImage(
					group.getIcon());
		}

		if (element instanceof MethodNavigatorItem) {
			MethodNavigatorItem navigatorItem = (MethodNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		// Due to plugin.xml content will be called only for "own" views
		if (element instanceof IAdaptable) {
			View view = (View) ((IAdaptable) element).getAdapter(View.class);
			if (view != null && isOwnView(view)) {
				return getImage(view);
			}
		}

		return super.getImage(element);
	}

	/**
	 * @generated
	 */
	public Image getImage(View view) {
		switch (MethodVisualIDRegistry.getVisualID(view)) {
		case OpenEditorCommandEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://es.esi.gemde/methodmanager/methodmodel?OpenEditorCommand", MethodElementTypes.OpenEditorCommand_2013); //$NON-NLS-1$
		case SelectFileCommandEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://es.esi.gemde/methodmanager/methodmodel?SelectFileCommand", MethodElementTypes.SelectFileCommand_2004); //$NON-NLS-1$
		case TaskNextEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://es.esi.gemde/methodmanager/methodmodel?Task?next", MethodElementTypes.TaskNext_4003); //$NON-NLS-1$
		case SelectModelElementFromFileCommandEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://es.esi.gemde/methodmanager/methodmodel?SelectModelElementFromFileCommand", MethodElementTypes.SelectModelElementFromFileCommand_2007); //$NON-NLS-1$
		case SelectFolderCommandEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://es.esi.gemde/methodmanager/methodmodel?SelectFolderCommand", MethodElementTypes.SelectFolderCommand_2005); //$NON-NLS-1$
		case CallWizardCommandEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://es.esi.gemde/methodmanager/methodmodel?CallWizardCommand", MethodElementTypes.CallWizardCommand_2003); //$NON-NLS-1$
		case AbstractSubTaskNextEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://es.esi.gemde/methodmanager/methodmodel?AbstractSubTask?next", MethodElementTypes.AbstractSubTaskNext_4004); //$NON-NLS-1$
		case MethodVariableEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://es.esi.gemde/methodmanager/methodmodel?MethodVariable", MethodElementTypes.MethodVariable_2015); //$NON-NLS-1$
		case LaunchValidationCommandEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://es.esi.gemde/methodmanager/methodmodel?LaunchValidationCommand", MethodElementTypes.LaunchValidationCommand_2011); //$NON-NLS-1$
		case RepeatedSubTaskEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://es.esi.gemde/methodmanager/methodmodel?RepeatedSubTask", MethodElementTypes.RepeatedSubTask_3004); //$NON-NLS-1$
		case ConditionalSubTaskEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://es.esi.gemde/methodmanager/methodmodel?ConditionalSubTask", MethodElementTypes.ConditionalSubTask_3003); //$NON-NLS-1$
		case SelectValidationCommandEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://es.esi.gemde/methodmanager/methodmodel?SelectValidationCommand", MethodElementTypes.SelectValidationCommand_2009); //$NON-NLS-1$
		case RepeatedSubTaskRepeatedElementEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://es.esi.gemde/methodmanager/methodmodel?RepeatedSubTask?repeatedElement", MethodElementTypes.RepeatedSubTaskRepeatedElement_4005); //$NON-NLS-1$
		case PhaseNextEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://es.esi.gemde/methodmanager/methodmodel?Phase?next", MethodElementTypes.PhaseNext_4002); //$NON-NLS-1$
		case TaskEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://es.esi.gemde/methodmanager/methodmodel?Task", MethodElementTypes.Task_3001); //$NON-NLS-1$
		case SubTaskEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://es.esi.gemde/methodmanager/methodmodel?SubTask", MethodElementTypes.SubTask_3002); //$NON-NLS-1$
		case SelectTransformationCommandEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://es.esi.gemde/methodmanager/methodmodel?SelectTransformationCommand", MethodElementTypes.SelectTransformationCommand_2010); //$NON-NLS-1$
		case SelectMetamodelCommandEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://es.esi.gemde/methodmanager/methodmodel?SelectMetamodelCommand", MethodElementTypes.SelectMetamodelCommand_2006); //$NON-NLS-1$
		case GEMDEMethodEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Diagram?http://es.esi.gemde/methodmanager/methodmodel?GEMDEMethod", MethodElementTypes.GEMDEMethod_1000); //$NON-NLS-1$
		case VariableCommandParameterEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://es.esi.gemde/methodmanager/methodmodel?VariableCommandParameter", MethodElementTypes.VariableCommandParameter_3005); //$NON-NLS-1$
		case ConditionEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://es.esi.gemde/methodmanager/methodmodel?Condition", MethodElementTypes.Condition_4001); //$NON-NLS-1$
		case CustomCommandEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://es.esi.gemde/methodmanager/methodmodel?CustomCommand", MethodElementTypes.CustomCommand_2002); //$NON-NLS-1$
		case SelectModelElementFromEObjectCommandEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://es.esi.gemde/methodmanager/methodmodel?SelectModelElementFromEObjectCommand", MethodElementTypes.SelectModelElementFromEObjectCommand_2008); //$NON-NLS-1$
		case ComplexCommandCommandsEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://es.esi.gemde/methodmanager/methodmodel?ComplexCommand?commands", MethodElementTypes.ComplexCommandCommands_4006); //$NON-NLS-1$
		case ComplexCommandEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://es.esi.gemde/methodmanager/methodmodel?ComplexCommand", MethodElementTypes.ComplexCommand_2014); //$NON-NLS-1$
		case ConstantCommandParameterEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://es.esi.gemde/methodmanager/methodmodel?ConstantCommandParameter", MethodElementTypes.ConstantCommandParameter_3006); //$NON-NLS-1$
		case PhaseEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://es.esi.gemde/methodmanager/methodmodel?Phase", MethodElementTypes.Phase_2001); //$NON-NLS-1$
		case LaunchTransformationCommandEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://es.esi.gemde/methodmanager/methodmodel?LaunchTransformationCommand", MethodElementTypes.LaunchTransformationCommand_2012); //$NON-NLS-1$
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = MethodDiagramEditorPlugin.getInstance()
				.getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null && elementType != null
				&& MethodElementTypes.isKnownElementType(elementType)) {
			image = MethodElementTypes.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public String getText(Object element) {
		if (element instanceof MethodNavigatorGroup) {
			MethodNavigatorGroup group = (MethodNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof MethodNavigatorItem) {
			MethodNavigatorItem navigatorItem = (MethodNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		// Due to plugin.xml content will be called only for "own" views
		if (element instanceof IAdaptable) {
			View view = (View) ((IAdaptable) element).getAdapter(View.class);
			if (view != null && isOwnView(view)) {
				return getText(view);
			}
		}

		return super.getText(element);
	}

	/**
	 * @generated
	 */
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (MethodVisualIDRegistry.getVisualID(view)) {
		case OpenEditorCommandEditPart.VISUAL_ID:
			return getOpenEditorCommand_2013Text(view);
		case SelectFileCommandEditPart.VISUAL_ID:
			return getSelectFileCommand_2004Text(view);
		case TaskNextEditPart.VISUAL_ID:
			return getTaskNext_4003Text(view);
		case SelectModelElementFromFileCommandEditPart.VISUAL_ID:
			return getSelectModelElementFromFileCommand_2007Text(view);
		case SelectFolderCommandEditPart.VISUAL_ID:
			return getSelectFolderCommand_2005Text(view);
		case CallWizardCommandEditPart.VISUAL_ID:
			return getCallWizardCommand_2003Text(view);
		case AbstractSubTaskNextEditPart.VISUAL_ID:
			return getAbstractSubTaskNext_4004Text(view);
		case MethodVariableEditPart.VISUAL_ID:
			return getMethodVariable_2015Text(view);
		case LaunchValidationCommandEditPart.VISUAL_ID:
			return getLaunchValidationCommand_2011Text(view);
		case RepeatedSubTaskEditPart.VISUAL_ID:
			return getRepeatedSubTask_3004Text(view);
		case ConditionalSubTaskEditPart.VISUAL_ID:
			return getConditionalSubTask_3003Text(view);
		case SelectValidationCommandEditPart.VISUAL_ID:
			return getSelectValidationCommand_2009Text(view);
		case RepeatedSubTaskRepeatedElementEditPart.VISUAL_ID:
			return getRepeatedSubTaskRepeatedElement_4005Text(view);
		case PhaseNextEditPart.VISUAL_ID:
			return getPhaseNext_4002Text(view);
		case TaskEditPart.VISUAL_ID:
			return getTask_3001Text(view);
		case SubTaskEditPart.VISUAL_ID:
			return getSubTask_3002Text(view);
		case SelectTransformationCommandEditPart.VISUAL_ID:
			return getSelectTransformationCommand_2010Text(view);
		case SelectMetamodelCommandEditPart.VISUAL_ID:
			return getSelectMetamodelCommand_2006Text(view);
		case GEMDEMethodEditPart.VISUAL_ID:
			return getGEMDEMethod_1000Text(view);
		case VariableCommandParameterEditPart.VISUAL_ID:
			return getVariableCommandParameter_3005Text(view);
		case ConditionEditPart.VISUAL_ID:
			return getCondition_4001Text(view);
		case CustomCommandEditPart.VISUAL_ID:
			return getCustomCommand_2002Text(view);
		case SelectModelElementFromEObjectCommandEditPart.VISUAL_ID:
			return getSelectModelElementFromEObjectCommand_2008Text(view);
		case ComplexCommandCommandsEditPart.VISUAL_ID:
			return getComplexCommandCommands_4006Text(view);
		case ComplexCommandEditPart.VISUAL_ID:
			return getComplexCommand_2014Text(view);
		case ConstantCommandParameterEditPart.VISUAL_ID:
			return getConstantCommandParameter_3006Text(view);
		case PhaseEditPart.VISUAL_ID:
			return getPhase_2001Text(view);
		case LaunchTransformationCommandEditPart.VISUAL_ID:
			return getLaunchTransformationCommand_2012Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	 * @generated
	 */
	private String getTaskNext_4003Text(View view) {
		IParser parser = MethodParserProvider.getParser(
				MethodElementTypes.TaskNext_4003,
				view.getElement() != null ? view.getElement() : view,
				CommonParserHint.DESCRIPTION);
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getSelectMetamodelCommand_2006Text(View view) {
		IParser parser = MethodParserProvider.getParser(
				MethodElementTypes.SelectMetamodelCommand_2006, view
						.getElement() != null ? view.getElement() : view,
				MethodVisualIDRegistry
						.getType(SelectMetamodelCommandNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5012); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getPhaseNext_4002Text(View view) {
		IParser parser = MethodParserProvider.getParser(
				MethodElementTypes.PhaseNext_4002,
				view.getElement() != null ? view.getElement() : view,
				CommonParserHint.DESCRIPTION);
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getSelectModelElementFromFileCommand_2007Text(View view) {
		IParser parser = MethodParserProvider
				.getParser(
						MethodElementTypes.SelectModelElementFromFileCommand_2007,
						view.getElement() != null ? view.getElement() : view,
						MethodVisualIDRegistry
								.getType(SelectModelElementFromFileCommandNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5013); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCustomCommand_2002Text(View view) {
		IParser parser = MethodParserProvider.getParser(
				MethodElementTypes.CustomCommand_2002,
				view.getElement() != null ? view.getElement() : view,
				MethodVisualIDRegistry
						.getType(CustomCommandNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5008); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getAbstractSubTaskNext_4004Text(View view) {
		IParser parser = MethodParserProvider.getParser(
				MethodElementTypes.AbstractSubTaskNext_4004,
				view.getElement() != null ? view.getElement() : view,
				CommonParserHint.DESCRIPTION);
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6004); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRepeatedSubTask_3004Text(View view) {
		IParser parser = MethodParserProvider
				.getParser(
						MethodElementTypes.RepeatedSubTask_3004,
						view.getElement() != null ? view.getElement() : view,
						MethodVisualIDRegistry
								.getType(RepeatedSubTaskTitleRepetitionVectorEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getConditionalSubTask_3003Text(View view) {
		IParser parser = MethodParserProvider.getParser(
				MethodElementTypes.ConditionalSubTask_3003,
				view.getElement() != null ? view.getElement() : view,
				MethodVisualIDRegistry
						.getType(ConditionalSubTaskTitleEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getSelectFileCommand_2004Text(View view) {
		IParser parser = MethodParserProvider.getParser(
				MethodElementTypes.SelectFileCommand_2004,
				view.getElement() != null ? view.getElement() : view,
				MethodVisualIDRegistry
						.getType(SelectFileCommandNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5010); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getLaunchTransformationCommand_2012Text(View view) {
		IParser parser = MethodParserProvider
				.getParser(
						MethodElementTypes.LaunchTransformationCommand_2012,
						view.getElement() != null ? view.getElement() : view,
						MethodVisualIDRegistry
								.getType(LaunchTransformationCommandNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5018); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getSelectTransformationCommand_2010Text(View view) {
		IParser parser = MethodParserProvider
				.getParser(
						MethodElementTypes.SelectTransformationCommand_2010,
						view.getElement() != null ? view.getElement() : view,
						MethodVisualIDRegistry
								.getType(SelectTransformationCommandNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5016); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getTask_3001Text(View view) {
		IParser parser = MethodParserProvider.getParser(
				MethodElementTypes.Task_3001,
				view.getElement() != null ? view.getElement() : view,
				MethodVisualIDRegistry.getType(TaskTitleEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5004); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getOpenEditorCommand_2013Text(View view) {
		IParser parser = MethodParserProvider.getParser(
				MethodElementTypes.OpenEditorCommand_2013,
				view.getElement() != null ? view.getElement() : view,
				MethodVisualIDRegistry
						.getType(OpenEditorCommandNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5019); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getSelectFolderCommand_2005Text(View view) {
		IParser parser = MethodParserProvider.getParser(
				MethodElementTypes.SelectFolderCommand_2005,
				view.getElement() != null ? view.getElement() : view,
				MethodVisualIDRegistry
						.getType(SelectFolderCommandNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5011); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getComplexCommandCommands_4006Text(View view) {
		IParser parser = MethodParserProvider.getParser(
				MethodElementTypes.ComplexCommandCommands_4006,
				view.getElement() != null ? view.getElement() : view,
				CommonParserHint.DESCRIPTION);
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6006); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getGEMDEMethod_1000Text(View view) {
		GEMDEMethod domainModelElement = (GEMDEMethod) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 1000); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getSelectModelElementFromEObjectCommand_2008Text(View view) {
		IParser parser = MethodParserProvider
				.getParser(
						MethodElementTypes.SelectModelElementFromEObjectCommand_2008,
						view.getElement() != null ? view.getElement() : view,
						MethodVisualIDRegistry
								.getType(SelectModelElementFromEObjectCommandNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5014); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getComplexCommand_2014Text(View view) {
		IParser parser = MethodParserProvider.getParser(
				MethodElementTypes.ComplexCommand_2014,
				view.getElement() != null ? view.getElement() : view,
				MethodVisualIDRegistry
						.getType(ComplexCommandNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5020); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getPhase_2001Text(View view) {
		IParser parser = MethodParserProvider.getParser(
				MethodElementTypes.Phase_2001,
				view.getElement() != null ? view.getElement() : view,
				MethodVisualIDRegistry.getType(PhaseTitleEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5005); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getSelectValidationCommand_2009Text(View view) {
		IParser parser = MethodParserProvider
				.getParser(
						MethodElementTypes.SelectValidationCommand_2009,
						view.getElement() != null ? view.getElement() : view,
						MethodVisualIDRegistry
								.getType(SelectValidationCommandNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5015); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRepeatedSubTaskRepeatedElement_4005Text(View view) {
		IParser parser = MethodParserProvider.getParser(
				MethodElementTypes.RepeatedSubTaskRepeatedElement_4005,
				view.getElement() != null ? view.getElement() : view,
				CommonParserHint.DESCRIPTION);
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6005); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCondition_4001Text(View view) {
		IParser parser = MethodParserProvider.getParser(
				MethodElementTypes.Condition_4001,
				view.getElement() != null ? view.getElement() : view,
				MethodVisualIDRegistry
						.getType(ConditionValueEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getSubTask_3002Text(View view) {
		IParser parser = MethodParserProvider.getParser(
				MethodElementTypes.SubTask_3002,
				view.getElement() != null ? view.getElement() : view,
				MethodVisualIDRegistry.getType(SubTaskTitleEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getMethodVariable_2015Text(View view) {
		IParser parser = MethodParserProvider.getParser(
				MethodElementTypes.MethodVariable_2015,
				view.getElement() != null ? view.getElement() : view,
				MethodVisualIDRegistry
						.getType(MethodVariableNameTypeEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5021); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getConstantCommandParameter_3006Text(View view) {
		IParser parser = MethodParserProvider
				.getParser(
						MethodElementTypes.ConstantCommandParameter_3006,
						view.getElement() != null ? view.getElement() : view,
						MethodVisualIDRegistry
								.getType(ConstantCommandParameterIdentifierEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5007); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getVariableCommandParameter_3005Text(View view) {
		IParser parser = MethodParserProvider
				.getParser(
						MethodElementTypes.VariableCommandParameter_3005,
						view.getElement() != null ? view.getElement() : view,
						MethodVisualIDRegistry
								.getType(VariableCommandParameterIdentifierEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5006); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getLaunchValidationCommand_2011Text(View view) {
		IParser parser = MethodParserProvider
				.getParser(
						MethodElementTypes.LaunchValidationCommand_2011,
						view.getElement() != null ? view.getElement() : view,
						MethodVisualIDRegistry
								.getType(LaunchValidationCommandNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5017); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCallWizardCommand_2003Text(View view) {
		IParser parser = MethodParserProvider.getParser(
				MethodElementTypes.CallWizardCommand_2003,
				view.getElement() != null ? view.getElement() : view,
				MethodVisualIDRegistry
						.getType(CallWizardCommandNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			MethodDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5009); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return GEMDEMethodEditPart.MODEL_ID.equals(MethodVisualIDRegistry
				.getModelID(view));
	}

}
