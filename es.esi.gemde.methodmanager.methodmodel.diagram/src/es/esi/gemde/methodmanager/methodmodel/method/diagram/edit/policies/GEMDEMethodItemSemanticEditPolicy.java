/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.policies;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.commands.DuplicateEObjectsCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;

import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.CallWizardCommandCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.ComplexCommandCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.CustomCommandCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.LaunchTransformationCommandCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.LaunchValidationCommandCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.MethodVariableCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.OpenEditorCommandCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.PhaseCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.SelectFileCommandCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.SelectFolderCommandCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.SelectMetamodelCommandCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.SelectModelElementFromEObjectCommandCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.SelectModelElementFromFileCommandCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.SelectTransformationCommandCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands.SelectValidationCommandCreateCommand;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.providers.MethodElementTypes;

/**
 * @generated
 */
public class GEMDEMethodItemSemanticEditPolicy extends
		MethodBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public GEMDEMethodItemSemanticEditPolicy() {
		super(MethodElementTypes.GEMDEMethod_1000);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (MethodElementTypes.Phase_2001 == req.getElementType()) {
			return getGEFWrapper(new PhaseCreateCommand(req));
		}
		if (MethodElementTypes.CustomCommand_2002 == req.getElementType()) {
			return getGEFWrapper(new CustomCommandCreateCommand(req));
		}
		if (MethodElementTypes.CallWizardCommand_2003 == req.getElementType()) {
			return getGEFWrapper(new CallWizardCommandCreateCommand(req));
		}
		if (MethodElementTypes.SelectFileCommand_2004 == req.getElementType()) {
			return getGEFWrapper(new SelectFileCommandCreateCommand(req));
		}
		if (MethodElementTypes.SelectFolderCommand_2005 == req.getElementType()) {
			return getGEFWrapper(new SelectFolderCommandCreateCommand(req));
		}
		if (MethodElementTypes.SelectMetamodelCommand_2006 == req
				.getElementType()) {
			return getGEFWrapper(new SelectMetamodelCommandCreateCommand(req));
		}
		if (MethodElementTypes.SelectModelElementFromFileCommand_2007 == req
				.getElementType()) {
			return getGEFWrapper(new SelectModelElementFromFileCommandCreateCommand(
					req));
		}
		if (MethodElementTypes.SelectModelElementFromEObjectCommand_2008 == req
				.getElementType()) {
			return getGEFWrapper(new SelectModelElementFromEObjectCommandCreateCommand(
					req));
		}
		if (MethodElementTypes.SelectValidationCommand_2009 == req
				.getElementType()) {
			return getGEFWrapper(new SelectValidationCommandCreateCommand(req));
		}
		if (MethodElementTypes.SelectTransformationCommand_2010 == req
				.getElementType()) {
			return getGEFWrapper(new SelectTransformationCommandCreateCommand(
					req));
		}
		if (MethodElementTypes.LaunchValidationCommand_2011 == req
				.getElementType()) {
			return getGEFWrapper(new LaunchValidationCommandCreateCommand(req));
		}
		if (MethodElementTypes.LaunchTransformationCommand_2012 == req
				.getElementType()) {
			return getGEFWrapper(new LaunchTransformationCommandCreateCommand(
					req));
		}
		if (MethodElementTypes.OpenEditorCommand_2013 == req.getElementType()) {
			return getGEFWrapper(new OpenEditorCommandCreateCommand(req));
		}
		if (MethodElementTypes.MethodVariable_2015 == req.getElementType()) {
			return getGEFWrapper(new MethodVariableCreateCommand(req));
		}
		if (MethodElementTypes.ComplexCommand_2014 == req.getElementType()) {
			return getGEFWrapper(new ComplexCommandCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getDuplicateCommand(DuplicateElementsRequest req) {
		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost())
				.getEditingDomain();
		return getGEFWrapper(new DuplicateAnythingCommand(editingDomain, req));
	}

	/**
	 * @generated
	 */
	private static class DuplicateAnythingCommand extends
			DuplicateEObjectsCommand {

		/**
		 * @generated
		 */
		public DuplicateAnythingCommand(
				TransactionalEditingDomain editingDomain,
				DuplicateElementsRequest req) {
			super(editingDomain, req.getLabel(), req
					.getElementsToBeDuplicated(), req
					.getAllDuplicatedElementsMap());
		}

	}

}
