/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

import es.esi.gemde.methodmanager.methodmodel.method.Condition;
import es.esi.gemde.methodmanager.methodmodel.method.ConditionalSubTask;
import es.esi.gemde.methodmanager.methodmodel.method.SubTask;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.policies.MethodBaseItemSemanticEditPolicy;

/**
 * @generated
 */
public class ConditionReorientCommand extends EditElementCommand {

	/**
	 * @generated
	 */
	private final int reorientDirection;

	/**
	 * @generated
	 */
	private final EObject oldEnd;

	/**
	 * @generated
	 */
	private final EObject newEnd;

	/**
	 * @generated
	 */
	public ConditionReorientCommand(ReorientRelationshipRequest request) {
		super(request.getLabel(), request.getRelationship(), request);
		reorientDirection = request.getDirection();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		if (false == getElementToEdit() instanceof Condition) {
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return canReorientTarget();
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean canReorientSource() {
		if (!(oldEnd instanceof ConditionalSubTask && newEnd instanceof ConditionalSubTask)) {
			return false;
		}
		SubTask target = getLink().getTo();
		if (!(getLink().eContainer() instanceof ConditionalSubTask)) {
			return false;
		}
		ConditionalSubTask container = (ConditionalSubTask) getLink()
				.eContainer();
		return MethodBaseItemSemanticEditPolicy.getLinkConstraints()
				.canExistCondition_4001(container, getLink(), getNewSource(),
						target);
	}

	/**
	 * @generated
	 */
	protected boolean canReorientTarget() {
		if (!(oldEnd instanceof SubTask && newEnd instanceof SubTask)) {
			return false;
		}
		ConditionalSubTask source = getLink().getFrom();
		if (!(getLink().eContainer() instanceof ConditionalSubTask)) {
			return false;
		}
		ConditionalSubTask container = (ConditionalSubTask) getLink()
				.eContainer();
		return MethodBaseItemSemanticEditPolicy.getLinkConstraints()
				.canExistCondition_4001(container, getLink(), source,
						getNewTarget());
	}

	/**
	 * @generated
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException(
					"Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientSource() throws ExecutionException {
		getLink().setFrom(getNewSource());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientTarget() throws ExecutionException {
		getLink().setTo(getNewTarget());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected Condition getLink() {
		return (Condition) getElementToEdit();
	}

	/**
	 * @generated
	 */
	protected ConditionalSubTask getOldSource() {
		return (ConditionalSubTask) oldEnd;
	}

	/**
	 * @generated
	 */
	protected ConditionalSubTask getNewSource() {
		return (ConditionalSubTask) newEnd;
	}

	/**
	 * @generated
	 */
	protected SubTask getOldTarget() {
		return (SubTask) oldEnd;
	}

	/**
	 * @generated
	 */
	protected SubTask getNewTarget() {
		return (SubTask) newEnd;
	}
}
