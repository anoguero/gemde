/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	 * @generated
	 */
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Messages() {
	}

	/**
	 * @generated
	 */
	public static String MethodCreationWizardTitle;

	/**
	 * @generated
	 */
	public static String MethodCreationWizard_DiagramModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String MethodCreationWizard_DiagramModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String MethodCreationWizard_DomainModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String MethodCreationWizard_DomainModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String MethodCreationWizardOpenEditorError;

	/**
	 * @generated
	 */
	public static String MethodCreationWizardCreationError;

	/**
	 * @generated
	 */
	public static String MethodCreationWizardPageExtensionError;

	/**
	 * @generated
	 */
	public static String MethodDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String MethodDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String MethodDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	 * @generated
	 */
	public static String MethodDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	 * @generated
	 */
	public static String MethodDocumentProvider_isModifiable;

	/**
	 * @generated
	 */
	public static String MethodDocumentProvider_handleElementContentChanged;

	/**
	 * @generated
	 */
	public static String MethodDocumentProvider_IncorrectInputError;

	/**
	 * @generated
	 */
	public static String MethodDocumentProvider_NoDiagramInResourceError;

	/**
	 * @generated
	 */
	public static String MethodDocumentProvider_DiagramLoadingError;

	/**
	 * @generated
	 */
	public static String MethodDocumentProvider_UnsynchronizedFileSaveError;

	/**
	 * @generated
	 */
	public static String MethodDocumentProvider_SaveDiagramTask;

	/**
	 * @generated
	 */
	public static String MethodDocumentProvider_SaveNextResourceTask;

	/**
	 * @generated
	 */
	public static String MethodDocumentProvider_SaveAsOperation;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_WizardTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_OpenModelFileDialogTitle;

	/**
	 * @generated
	 */
	public static String MethodNewDiagramFileWizard_CreationPageName;

	/**
	 * @generated
	 */
	public static String MethodNewDiagramFileWizard_CreationPageTitle;

	/**
	 * @generated
	 */
	public static String MethodNewDiagramFileWizard_CreationPageDescription;

	/**
	 * @generated
	 */
	public static String MethodNewDiagramFileWizard_RootSelectionPageName;

	/**
	 * @generated
	 */
	public static String MethodNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String MethodNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String MethodNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	 * @generated
	 */
	public static String MethodNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	 * @generated
	 */
	public static String MethodNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	 * @generated
	 */
	public static String MethodNewDiagramFileWizard_InitDiagramCommand;

	/**
	 * @generated
	 */
	public static String MethodNewDiagramFileWizard_IncorrectRootError;

	/**
	 * @generated
	 */
	public static String MethodDiagramEditor_SavingDeletedFile;

	/**
	 * @generated
	 */
	public static String MethodDiagramEditor_SaveAsErrorTitle;

	/**
	 * @generated
	 */
	public static String MethodDiagramEditor_SaveAsErrorMessage;

	/**
	 * @generated
	 */
	public static String MethodDiagramEditor_SaveErrorTitle;

	/**
	 * @generated
	 */
	public static String MethodDiagramEditor_SaveErrorMessage;

	/**
	 * @generated
	 */
	public static String MethodElementChooserDialog_SelectModelElementTitle;

	/**
	 * @generated
	 */
	public static String ModelElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String ValidateActionMessage;

	/**
	 * @generated
	 */
	public static String Objects1Group_title;

	/**
	 * @generated
	 */
	public static String Connections2Group_title;

	/**
	 * @generated
	 */
	public static String AddCallWizardCommand1CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddCallWizardCommand1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddComplexCommand2CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddComplexCommand2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddConditionalSubTask3CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddConditionalSubTask3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddConstantCommandParameter4CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddConstantCommandParameter4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddCustomCommand5CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddCustomCommand5CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddInternalVariable6CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddInternalVariable6CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddLaunchTransformationCommand7CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddLaunchTransformationCommand7CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddLaunchValidationCommand8CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddLaunchValidationCommand8CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddMethodPhase9CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddMethodPhase9CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddOpenEditorCommand10CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddOpenEditorCommand10CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddRepeatedSubTask11CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddRepeatedSubTask11CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddSelectEObjectfromFileCommand12CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddSelectEObjectfromFileCommand12CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddSelectEObjectfromObjectCommand13CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddSelectEObjectfromObjectCommand13CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddSelectFile14CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddSelectFile14CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddSelectFolderCommand15CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddSelectFolderCommand15CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddSelectMetamodel16CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddSelectMetamodel16CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddSelectTransformationCommand17CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddSelectTransformationCommand17CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddSelectValidationCommand18CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddSelectValidationCommand18CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddSubTask19CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddSubTask19CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddTask20CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddTask20CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddVariable21CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddVariable21CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AddVariableCommandParameter22CreationTool_title;

	/**
	 * @generated
	 */
	public static String AddVariableCommandParameter22CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ComposingCommand1CreationTool_title;

	/**
	 * @generated
	 */
	public static String ComposingCommand1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ConditionalSubTask2CreationTool_title;

	/**
	 * @generated
	 */
	public static String ConditionalSubTask2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String NextPhase3CreationTool_title;

	/**
	 * @generated
	 */
	public static String NextPhase3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String NextSubTask4CreationTool_title;

	/**
	 * @generated
	 */
	public static String NextSubTask4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String NextTask5CreationTool_title;

	/**
	 * @generated
	 */
	public static String NextTask5CreationTool_desc;

	/**
	 * @generated
	 */
	public static String RepeatedSubTask6CreationTool_title;

	/**
	 * @generated
	 */
	public static String RepeatedSubTask6CreationTool_desc;

	/**
	 * @generated
	 */
	public static String PhasePhaseTasksCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String TaskTaskSubTasksCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String CustomCommandCustomCommandParametersCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String CallWizardCommandCallWizardCommandParametersCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String SelectFileCommandSelectFileCommandParametersCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String SelectFolderCommandSelectFolderCommandParametersCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String SelectMetamodelCommandSelectMetamodelCommandParametersCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String SelectModelElementFromFileCommandSelectModelElementFromFileCommandParametersCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String SelectModelElementFromEObjectCommandSelectModelElementFromEObjectCommandParametersCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String SelectValidationCommandSelectValidationCommandParametersCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String SelectTransformationCommandSelectTransformationCommandParametersCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String LaunchValidationCommandLaunchValidationCommandParametersCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String LaunchTransformationCommandLaunchTransformationCommandParametersCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String OpenEditorCommandOpenEditorCommandParametersCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String CommandName_OpenDiagram;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_SelectTransformationCommand_2010_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_TaskNext_4003_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_TaskNext_4003_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ComplexCommand_2014_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_CallWizardCommand_2003_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_PhaseNext_4002_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_PhaseNext_4002_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AbstractSubTaskNext_4004_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AbstractSubTaskNext_4004_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_OpenEditorCommand_2013_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_SelectModelElementFromFileCommand_2007_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RepeatedSubTask_3004_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RepeatedSubTask_3004_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ConditionalSubTask_3003_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ConditionalSubTask_3003_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Task_3001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Task_3001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_SelectMetamodelCommand_2006_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_SelectFileCommand_2004_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_CustomCommand_2002_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_GEMDEMethod_1000_links;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_SelectValidationCommand_2009_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_LaunchTransformationCommand_2012_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Phase_2001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Phase_2001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RepeatedSubTaskRepeatedElement_4005_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RepeatedSubTaskRepeatedElement_4005_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Condition_4001_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Condition_4001_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_SubTask_3002_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_SubTask_3002_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_SelectModelElementFromEObjectCommand_2008_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_SelectFolderCommand_2005_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ComplexCommandCommands_4006_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ComplexCommandCommands_4006_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_LaunchValidationCommand_2011_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	 * @generated
	 */
	public static String AbstractParser_UnexpectedValueType;

	/**
	 * @generated
	 */
	public static String AbstractParser_WrongStringConversion;

	/**
	 * @generated
	 */
	public static String AbstractParser_UnknownLiteral;

	/**
	 * @generated
	 */
	public static String MessageFormatParser_InvalidInputError;

	/**
	 * @generated
	 */
	public static String MethodModelingAssistantProviderTitle;

	/**
	 * @generated
	 */
	public static String MethodModelingAssistantProviderMessage;

	//TODO: put accessor fields manually	
}
