/*
 * 
 */
package es.esi.gemde.methodmanager.methodmodel.method.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;

import es.esi.gemde.methodmanager.methodmodel.method.MethodPackage;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.AbstractSubTaskNextEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CallWizardCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ComplexCommandCommandsEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ComplexCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConditionEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConditionalSubTaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.ConstantCommandParameterEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.CustomCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.GEMDEMethodEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchTransformationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.LaunchValidationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.MethodVariableEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.OpenEditorCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.PhaseEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.PhaseNextEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.RepeatedSubTaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.RepeatedSubTaskRepeatedElementEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFileCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectFolderCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectMetamodelCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromEObjectCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectModelElementFromFileCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectTransformationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SelectValidationCommandEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.SubTaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.TaskEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.TaskNextEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.edit.parts.VariableCommandParameterEditPart;
import es.esi.gemde.methodmanager.methodmodel.method.diagram.part.MethodDiagramEditorPlugin;

/**
 * @generated
 */
public class MethodElementTypes {

	/**
	 * @generated
	 */
	private MethodElementTypes() {
	}

	/**
	 * @generated
	 */
	private static Map<IElementType, ENamedElement> elements;

	/**
	 * @generated
	 */
	private static ImageRegistry imageRegistry;

	/**
	 * @generated
	 */
	private static Set<IElementType> KNOWN_ELEMENT_TYPES;

	/**
	 * @generated
	 */
	public static final IElementType GEMDEMethod_1000 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.GEMDEMethod_1000"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Phase_2001 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.Phase_2001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType CustomCommand_2002 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.CustomCommand_2002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType CallWizardCommand_2003 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.CallWizardCommand_2003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType SelectFileCommand_2004 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.SelectFileCommand_2004"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType SelectFolderCommand_2005 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.SelectFolderCommand_2005"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType SelectMetamodelCommand_2006 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.SelectMetamodelCommand_2006"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType SelectModelElementFromFileCommand_2007 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.SelectModelElementFromFileCommand_2007"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType SelectModelElementFromEObjectCommand_2008 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.SelectModelElementFromEObjectCommand_2008"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType SelectValidationCommand_2009 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.SelectValidationCommand_2009"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType SelectTransformationCommand_2010 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.SelectTransformationCommand_2010"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType LaunchValidationCommand_2011 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.LaunchValidationCommand_2011"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType LaunchTransformationCommand_2012 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.LaunchTransformationCommand_2012"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType OpenEditorCommand_2013 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.OpenEditorCommand_2013"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ComplexCommand_2014 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.ComplexCommand_2014"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType MethodVariable_2015 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.MethodVariable_2015"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Task_3001 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.Task_3001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType SubTask_3002 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.SubTask_3002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ConditionalSubTask_3003 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.ConditionalSubTask_3003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType RepeatedSubTask_3004 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.RepeatedSubTask_3004"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType VariableCommandParameter_3005 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.VariableCommandParameter_3005"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ConstantCommandParameter_3006 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.ConstantCommandParameter_3006"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Condition_4001 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.Condition_4001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType PhaseNext_4002 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.PhaseNext_4002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType TaskNext_4003 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.TaskNext_4003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType AbstractSubTaskNext_4004 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.AbstractSubTaskNext_4004"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType RepeatedSubTaskRepeatedElement_4005 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.RepeatedSubTaskRepeatedElement_4005"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType ComplexCommandCommands_4006 = getElementType("es.esi.gemde.methodmanager.methodmodel.diagram.ComplexCommandCommands_4006"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	private static ImageRegistry getImageRegistry() {
		if (imageRegistry == null) {
			imageRegistry = new ImageRegistry();
		}
		return imageRegistry;
	}

	/**
	 * @generated
	 */
	private static String getImageRegistryKey(ENamedElement element) {
		return element.getName();
	}

	/**
	 * @generated
	 */
	private static ImageDescriptor getProvidedImageDescriptor(
			ENamedElement element) {
		if (element instanceof EStructuralFeature) {
			EStructuralFeature feature = ((EStructuralFeature) element);
			EClass eContainingClass = feature.getEContainingClass();
			EClassifier eType = feature.getEType();
			if (eContainingClass != null && !eContainingClass.isAbstract()) {
				element = eContainingClass;
			} else if (eType instanceof EClass
					&& !((EClass) eType).isAbstract()) {
				element = eType;
			}
		}
		if (element instanceof EClass) {
			EClass eClass = (EClass) element;
			if (!eClass.isAbstract()) {
				return MethodDiagramEditorPlugin.getInstance()
						.getItemImageDescriptor(
								eClass.getEPackage().getEFactoryInstance()
										.create(eClass));
			}
		}
		// TODO : support structural features
		return null;
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(ENamedElement element) {
		String key = getImageRegistryKey(element);
		ImageDescriptor imageDescriptor = getImageRegistry().getDescriptor(key);
		if (imageDescriptor == null) {
			imageDescriptor = getProvidedImageDescriptor(element);
			if (imageDescriptor == null) {
				imageDescriptor = ImageDescriptor.getMissingImageDescriptor();
			}
			getImageRegistry().put(key, imageDescriptor);
		}
		return imageDescriptor;
	}

	/**
	 * @generated
	 */
	public static Image getImage(ENamedElement element) {
		String key = getImageRegistryKey(element);
		Image image = getImageRegistry().get(key);
		if (image == null) {
			ImageDescriptor imageDescriptor = getProvidedImageDescriptor(element);
			if (imageDescriptor == null) {
				imageDescriptor = ImageDescriptor.getMissingImageDescriptor();
			}
			getImageRegistry().put(key, imageDescriptor);
			image = getImageRegistry().get(key);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(IAdaptable hint) {
		ENamedElement element = getElement(hint);
		if (element == null) {
			return null;
		}
		return getImageDescriptor(element);
	}

	/**
	 * @generated
	 */
	public static Image getImage(IAdaptable hint) {
		ENamedElement element = getElement(hint);
		if (element == null) {
			return null;
		}
		return getImage(element);
	}

	/**
	 * Returns 'type' of the ecore object associated with the hint.
	 * 
	 * @generated
	 */
	public static ENamedElement getElement(IAdaptable hint) {
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null) {
			elements = new IdentityHashMap<IElementType, ENamedElement>();

			elements.put(GEMDEMethod_1000,
					MethodPackage.eINSTANCE.getGEMDEMethod());

			elements.put(Phase_2001, MethodPackage.eINSTANCE.getPhase());

			elements.put(CustomCommand_2002,
					MethodPackage.eINSTANCE.getCustomCommand());

			elements.put(CallWizardCommand_2003,
					MethodPackage.eINSTANCE.getCallWizardCommand());

			elements.put(SelectFileCommand_2004,
					MethodPackage.eINSTANCE.getSelectFileCommand());

			elements.put(SelectFolderCommand_2005,
					MethodPackage.eINSTANCE.getSelectFolderCommand());

			elements.put(SelectMetamodelCommand_2006,
					MethodPackage.eINSTANCE.getSelectMetamodelCommand());

			elements.put(SelectModelElementFromFileCommand_2007,
					MethodPackage.eINSTANCE
							.getSelectModelElementFromFileCommand());

			elements.put(SelectModelElementFromEObjectCommand_2008,
					MethodPackage.eINSTANCE
							.getSelectModelElementFromEObjectCommand());

			elements.put(SelectValidationCommand_2009,
					MethodPackage.eINSTANCE.getSelectValidationCommand());

			elements.put(SelectTransformationCommand_2010,
					MethodPackage.eINSTANCE.getSelectTransformationCommand());

			elements.put(LaunchValidationCommand_2011,
					MethodPackage.eINSTANCE.getLaunchValidationCommand());

			elements.put(LaunchTransformationCommand_2012,
					MethodPackage.eINSTANCE.getLaunchTransformationCommand());

			elements.put(OpenEditorCommand_2013,
					MethodPackage.eINSTANCE.getOpenEditorCommand());

			elements.put(MethodVariable_2015,
					MethodPackage.eINSTANCE.getMethodVariable());

			elements.put(ComplexCommand_2014,
					MethodPackage.eINSTANCE.getComplexCommand());

			elements.put(Task_3001, MethodPackage.eINSTANCE.getTask());

			elements.put(SubTask_3002, MethodPackage.eINSTANCE.getSubTask());

			elements.put(ConditionalSubTask_3003,
					MethodPackage.eINSTANCE.getConditionalSubTask());

			elements.put(RepeatedSubTask_3004,
					MethodPackage.eINSTANCE.getRepeatedSubTask());

			elements.put(VariableCommandParameter_3005,
					MethodPackage.eINSTANCE.getVariableCommandParameter());

			elements.put(ConstantCommandParameter_3006,
					MethodPackage.eINSTANCE.getConstantCommandParameter());

			elements.put(Condition_4001, MethodPackage.eINSTANCE.getCondition());

			elements.put(PhaseNext_4002,
					MethodPackage.eINSTANCE.getPhase_Next());

			elements.put(TaskNext_4003, MethodPackage.eINSTANCE.getTask_Next());

			elements.put(AbstractSubTaskNext_4004,
					MethodPackage.eINSTANCE.getAbstractSubTask_Next());

			elements.put(RepeatedSubTaskRepeatedElement_4005,
					MethodPackage.eINSTANCE
							.getRepeatedSubTask_RepeatedElement());

			elements.put(ComplexCommandCommands_4006,
					MethodPackage.eINSTANCE.getComplexCommand_Commands());
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	 * @generated
	 */
	private static IElementType getElementType(String id) {
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	 * @generated
	 */
	public static boolean isKnownElementType(IElementType elementType) {
		if (KNOWN_ELEMENT_TYPES == null) {
			KNOWN_ELEMENT_TYPES = new HashSet<IElementType>();
			KNOWN_ELEMENT_TYPES.add(GEMDEMethod_1000);
			KNOWN_ELEMENT_TYPES.add(Phase_2001);
			KNOWN_ELEMENT_TYPES.add(CustomCommand_2002);
			KNOWN_ELEMENT_TYPES.add(CallWizardCommand_2003);
			KNOWN_ELEMENT_TYPES.add(SelectFileCommand_2004);
			KNOWN_ELEMENT_TYPES.add(SelectFolderCommand_2005);
			KNOWN_ELEMENT_TYPES.add(SelectMetamodelCommand_2006);
			KNOWN_ELEMENT_TYPES.add(SelectModelElementFromFileCommand_2007);
			KNOWN_ELEMENT_TYPES.add(SelectModelElementFromEObjectCommand_2008);
			KNOWN_ELEMENT_TYPES.add(SelectValidationCommand_2009);
			KNOWN_ELEMENT_TYPES.add(SelectTransformationCommand_2010);
			KNOWN_ELEMENT_TYPES.add(LaunchValidationCommand_2011);
			KNOWN_ELEMENT_TYPES.add(LaunchTransformationCommand_2012);
			KNOWN_ELEMENT_TYPES.add(OpenEditorCommand_2013);
			KNOWN_ELEMENT_TYPES.add(MethodVariable_2015);
			KNOWN_ELEMENT_TYPES.add(ComplexCommand_2014);
			KNOWN_ELEMENT_TYPES.add(Task_3001);
			KNOWN_ELEMENT_TYPES.add(SubTask_3002);
			KNOWN_ELEMENT_TYPES.add(ConditionalSubTask_3003);
			KNOWN_ELEMENT_TYPES.add(RepeatedSubTask_3004);
			KNOWN_ELEMENT_TYPES.add(VariableCommandParameter_3005);
			KNOWN_ELEMENT_TYPES.add(ConstantCommandParameter_3006);
			KNOWN_ELEMENT_TYPES.add(Condition_4001);
			KNOWN_ELEMENT_TYPES.add(PhaseNext_4002);
			KNOWN_ELEMENT_TYPES.add(TaskNext_4003);
			KNOWN_ELEMENT_TYPES.add(AbstractSubTaskNext_4004);
			KNOWN_ELEMENT_TYPES.add(RepeatedSubTaskRepeatedElement_4005);
			KNOWN_ELEMENT_TYPES.add(ComplexCommandCommands_4006);
		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

	/**
	 * @generated
	 */
	public static IElementType getElementType(int visualID) {
		switch (visualID) {
		case GEMDEMethodEditPart.VISUAL_ID:
			return GEMDEMethod_1000;
		case PhaseEditPart.VISUAL_ID:
			return Phase_2001;
		case CustomCommandEditPart.VISUAL_ID:
			return CustomCommand_2002;
		case CallWizardCommandEditPart.VISUAL_ID:
			return CallWizardCommand_2003;
		case SelectFileCommandEditPart.VISUAL_ID:
			return SelectFileCommand_2004;
		case SelectFolderCommandEditPart.VISUAL_ID:
			return SelectFolderCommand_2005;
		case SelectMetamodelCommandEditPart.VISUAL_ID:
			return SelectMetamodelCommand_2006;
		case SelectModelElementFromFileCommandEditPart.VISUAL_ID:
			return SelectModelElementFromFileCommand_2007;
		case SelectModelElementFromEObjectCommandEditPart.VISUAL_ID:
			return SelectModelElementFromEObjectCommand_2008;
		case SelectValidationCommandEditPart.VISUAL_ID:
			return SelectValidationCommand_2009;
		case SelectTransformationCommandEditPart.VISUAL_ID:
			return SelectTransformationCommand_2010;
		case LaunchValidationCommandEditPart.VISUAL_ID:
			return LaunchValidationCommand_2011;
		case LaunchTransformationCommandEditPart.VISUAL_ID:
			return LaunchTransformationCommand_2012;
		case OpenEditorCommandEditPart.VISUAL_ID:
			return OpenEditorCommand_2013;
		case MethodVariableEditPart.VISUAL_ID:
			return MethodVariable_2015;
		case ComplexCommandEditPart.VISUAL_ID:
			return ComplexCommand_2014;
		case TaskEditPart.VISUAL_ID:
			return Task_3001;
		case SubTaskEditPart.VISUAL_ID:
			return SubTask_3002;
		case ConditionalSubTaskEditPart.VISUAL_ID:
			return ConditionalSubTask_3003;
		case RepeatedSubTaskEditPart.VISUAL_ID:
			return RepeatedSubTask_3004;
		case VariableCommandParameterEditPart.VISUAL_ID:
			return VariableCommandParameter_3005;
		case ConstantCommandParameterEditPart.VISUAL_ID:
			return ConstantCommandParameter_3006;
		case ConditionEditPart.VISUAL_ID:
			return Condition_4001;
		case PhaseNextEditPart.VISUAL_ID:
			return PhaseNext_4002;
		case TaskNextEditPart.VISUAL_ID:
			return TaskNext_4003;
		case AbstractSubTaskNextEditPart.VISUAL_ID:
			return AbstractSubTaskNext_4004;
		case RepeatedSubTaskRepeatedElementEditPart.VISUAL_ID:
			return RepeatedSubTaskRepeatedElement_4005;
		case ComplexCommandCommandsEditPart.VISUAL_ID:
			return ComplexCommandCommands_4006;
		}
		return null;
	}

}
