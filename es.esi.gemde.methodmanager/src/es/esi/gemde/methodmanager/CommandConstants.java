package es.esi.gemde.methodmanager;

public abstract class CommandConstants {

	public static final String WIZARD_PARAM_ID = 			"es.esi.gemde.methodmanager.commandparameters.wizard";
	public static final String TRANSFORMATION_PARAM_ID = 	"es.esi.gemde.methodmanager.commandparameters.transformation";
	public static final String INPUTS_PARAM_ID = 			"es.esi.gemde.methodmanager.commandparameters.inputs";
	public static final String OUTPUTS_PARAM_ID = 		"es.esi.gemde.methodmanager.commandparameters.outputs";
	public static final String OUTPUTPATH_PARAM_ID = 		"es.esi.gemde.methodmanager.commandparameters.outputpath";
	public static final String VALIDATION_PARAM_ID = 		"es.esi.gemde.methodmanager.commandparameters.validation";
	public static final String EOBJECT_PARAM_ID = 		"es.esi.gemde.methodmanager.commandparameters.eobject";
	public static final String MODELFILE_PARAM_ID = 		"es.esi.gemde.methodmanager.commandparameters.modelfile";
	public static final String EDITORID_PARAM_ID = 		"es.esi.gemde.methodmanager.commandparameters.editorid";
	public static final String FILE_PARAM_ID = 			"es.esi.gemde.methodmanager.commandparameters.file";
	public static final String ONLYWORKSPACE_PARAM_ID = 	"es.esi.gemde.methodmanager.commandparameters.onlyworkspace";
	public static final String ADDTOLIST_PARAM_ID = 		"es.esi.gemde.methodmanager.commandparameters.addtolist";
	public static final String LIST_PARAM_ID = 			"es.esi.gemde.methodmanager.commandparameters.list";

}
