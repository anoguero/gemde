package es.esi.gemde.methodmanager.service.ui;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import es.esi.gemde.core.CorePlugin;

public class SelectMetamodelDialog extends Dialog {

	private Button okButton;
	private EPackage metamodel;
	
	public SelectMetamodelDialog(Shell parentShell) {
		super(parentShell);
	}
	
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Select Metamodel");
		newShell.setImage(CorePlugin.getImage(CorePlugin.METAMODEL_ICON));
		newShell.setSize(400, 300);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		parent.setLayout(new GridLayout(1, true));
		
		Label label = new Label(parent, SWT.NONE);
		GridData labelData = new GridData(GridData.FILL_HORIZONTAL);
		labelData.horizontalSpan = 1;
		label.setLayoutData(labelData);
		label.setText("Available Metamodels:");
		
		TreeViewer tree = new TreeViewer(parent, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		GridData treeData = new GridData(GridData.FILL_BOTH);
		treeData.horizontalSpan = 1;
		tree.getTree().setLayoutData(treeData);
		
		tree.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				Object obj = ((StructuredSelection)event.getSelection()).getFirstElement();
				if (obj instanceof EPackage) {
					metamodel = (EPackage) obj;
				}
				checkCompleted();
			}
		});
		
		// Fill the tree with
		Tree t = tree.getTree();
		t.clearAll(true);
		for (String regKey : EPackage.Registry.INSTANCE.keySet().toArray(new String [0])) {
			EPackage data = null;
			if (EPackage.Registry.INSTANCE.get(regKey) != null && EPackage.Registry.INSTANCE.get(regKey) instanceof EPackage) {
				data = (EPackage)EPackage.Registry.INSTANCE.get(regKey);
			}
			else if (EPackage.Registry.INSTANCE.get(regKey) != null && EPackage.Registry.INSTANCE.get(regKey) instanceof EPackage.Descriptor) {
				data = ((EPackage.Descriptor)EPackage.Registry.INSTANCE.get(regKey)).getEPackage();
			}
			
			if (data != null) {
				TreeItem ti = new TreeItem(t, SWT.NONE);
				ti.setData(data);
				ti.setText(data.getName() + ":" + data.getNsURI());
				ti.setImage(CorePlugin.getImage(CorePlugin.METAMODEL_ICON));
			}
		}
		
		return parent;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// Only create an OK button
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		okButton = getButton(IDialogConstants.OK_ID);
		checkCompleted();
	}
	
	private void checkCompleted() {
		if (okButton != null) {
			okButton.setEnabled(metamodel != null);
		}
	}
	
	public EPackage getSelectedMetamodel() {
		return metamodel;
	}
}
