package es.esi.gemde.methodmanager.service.ui;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import es.esi.gemde.core.CorePlugin;

public class SelectEObjectDialog extends Dialog {

	private Button okButton;
	private EObject eobject;
	private EObject root;
	
	public SelectEObjectDialog(Shell parentShell, EObject root) {
		super(parentShell);
		this.root = root;
	}

	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("Select Model Element");
		newShell.setImage(CorePlugin.getImage(CorePlugin.INPUT_ICON));
		newShell.setSize(400, 300);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		parent.setLayout(new GridLayout(1, true));
		
		Label label = new Label(parent, SWT.NONE);
		GridData labelData = new GridData(GridData.FILL_HORIZONTAL);
		labelData.horizontalSpan = 1;
		label.setLayoutData(labelData);
		label.setText("Available Metamodels:");
		
		TreeViewer tree = new TreeViewer(parent, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		GridData treeData = new GridData(GridData.FILL_BOTH);
		treeData.horizontalSpan = 1;
		tree.getTree().setLayoutData(treeData);
		
		tree.addSelectionChangedListener(new ISelectionChangedListener() {
			
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				Object obj = ((StructuredSelection)event.getSelection()).getFirstElement();
				if (obj instanceof EObject) {
					eobject = (EObject) obj;
				}
				checkCompleted();
			}
		});
		
		ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
		tree.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
		tree.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		tree.setSorter(new ViewerSorter());
		
		tree.setInput(root.eResource());
		
		return parent;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		// Only create an OK button
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL, true);
		okButton = getButton(IDialogConstants.OK_ID);
		checkCompleted();
	}
	
	private void checkCompleted() {
		if (okButton != null) {
			okButton.setEnabled(eobject != null);
		}
	}
	
	public EObject getSelectedEObject() {
		return eobject;
	}
}
