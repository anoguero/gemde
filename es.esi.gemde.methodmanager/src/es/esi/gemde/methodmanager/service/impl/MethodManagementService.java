package es.esi.gemde.methodmanager.service.impl;

import java.util.Vector;

import es.esi.gemde.methodmanager.service.IMethod;
import es.esi.gemde.methodmanager.service.IMethodManagementService;

public class MethodManagementService implements IMethodManagementService {

	private IMethod activeMethod;
	private Vector<IMethod> availableMethods;
	
	public MethodManagementService () {
		availableMethods = new Vector<IMethod>();
	}
	
	/* (non-Javadoc)
	 * @see es.esi.gemde.methodmanager.service.IMethodManagementService#getActiveMethod()
	 */
	@Override
	public IMethod getActiveMethod() {
		return activeMethod;
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.methodmanager.service.IMethodManagementService#setActiveMethod(es.esi.gemde.methodmanager.service.IMethod)
	 */
	@Override
	public void setActiveMethod(String methodName) {
		for (IMethod method : availableMethods) {
			if (method.getName().equals(methodName)) {
				activeMethod = method;
				return;
			}
		}
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.methodmanager.service.IMethodManagementService#registerMethod(es.esi.gemde.methodmanager.service.IMethod)
	 */
	@Override
	public void registerMethod(IMethod method) {
		if (method != null) {
			availableMethods.add(method);
			
			// Initialize the active method
			if (activeMethod == null) {
				activeMethod = method;
			}
		}
	}

}
