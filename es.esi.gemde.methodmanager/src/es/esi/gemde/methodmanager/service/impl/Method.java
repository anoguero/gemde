package es.esi.gemde.methodmanager.service.impl;

import es.esi.gemde.methodmanager.service.IMethod;

public class Method implements IMethod {

	private String name;
	
	public Method (String name) {
		this.name = name;
	}
	
	@Override
	public String getName() {
		return name;
	}

}
