package es.esi.gemde.methodmanager.service.impl;

import java.util.HashMap;

/**
 * 
 * A class used to handle the command parameters in GEMDE methods
 * 
 * @author Adrian Noguero (adrian.noguero@tecnalia.com)
 *
 */
public class CommandParametersRegistry {

	/**
	 * The shared instance
	 */
	private static CommandParametersRegistry instance;
	
	/**
	 * The map used as registry
	 */
	private HashMap<String, Object> paramsRegistry;
	
	/**
	 * Private constructor for the shared instance
	 */
	private CommandParametersRegistry() {
		paramsRegistry = new HashMap<String, Object>();
	}
	
	/**
	 * Returns the shared instance of the params registry
	 * 
	 * @return the instance of the registry
	 */
	public static CommandParametersRegistry instance() {
		if (instance == null) {
			instance = new CommandParametersRegistry();
		}
		return instance;
	}
	
	/**
	 * Put a parameter in the registry. This method is synchronized.
	 * 
	 * @param identifier. Parameter ID.
	 * @param obj. The parameter value.
	 */
	synchronized public void putParam(String identifier, Object obj) {
		paramsRegistry.put(identifier, obj);
	}
	
	/**
	 * Gets the value of a parameter or null if no value exists. The method is synchronized.
	 * 
	 * @param identifier. The identifier of the parameter.
	 * @return the parameter value as an object or null if none exists.
	 */
	synchronized public Object getParam(String identifier) {
		return paramsRegistry.get(identifier);
	}
	
	/**
	 * Clear the parameter registry
	 */
	synchronized public void clear() {
		paramsRegistry.clear();
	}
}
