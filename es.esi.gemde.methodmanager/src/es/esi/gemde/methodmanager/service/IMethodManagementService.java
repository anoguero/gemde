package es.esi.gemde.methodmanager.service;

public interface IMethodManagementService {

	public IMethod getActiveMethod();
	
	public void setActiveMethod(String methodName);
	
	public void registerMethod(IMethod method);
	
}
