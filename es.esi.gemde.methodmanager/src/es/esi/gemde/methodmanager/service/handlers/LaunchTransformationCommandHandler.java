package es.esi.gemde.methodmanager.service.handlers;

import java.io.File;
import java.util.Vector;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

import es.esi.gemde.methodmanager.CommandConstants;
import es.esi.gemde.methodmanager.service.impl.CommandParametersRegistry;
import es.esi.gemde.modeltransformator.ModelTransformatorPlugin;
import es.esi.gemde.modeltransformator.service.ITransformation;

public class LaunchTransformationCommandHandler extends AbstractHandler {
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		String transformation = event.getParameter(CommandConstants.TRANSFORMATION_PARAM_ID);
		String inputs = event.getParameter(CommandConstants.INPUTS_PARAM_ID);
		// FIXME: String outputs = event.getParameter(CommandConstants.OUTPUTS_PARAM_ID);
		String outputpath = event.getParameter(CommandConstants.OUTPUTPATH_PARAM_ID);
		
		if (transformation == null || !ModelTransformatorPlugin.getService().transformationExists(transformation)) {
			throw new ExecutionException("Provided transformation does not exist. Cannot launch transformation.");
		}
		
		if (outputpath == null) {
			throw new ExecutionException("Output folder was not provided. Cannot launch transformation.");
		}
		
		File f = new File(outputpath);
		if (!f.exists() || !f.isDirectory()) {
			throw new ExecutionException("Output folder is not valid. Cannot launch transformation.");
		}
		
		Vector<EObject> inputObjects = new Vector<EObject>();
		
		if (inputs != null) {
			Object raw = CommandParametersRegistry.instance().getParam(inputs);
			if (raw instanceof Vector<?>) {
				Vector<?> v = (Vector<?>) raw;
				for (Object o : v) {
					if (o instanceof EObject) {
						inputObjects.add((EObject) o);
					}
					else {
						throw new ExecutionException("Provided input(s) is(are) not valid. Cannot launch transformation.");
					}
				}
			}
			else if (raw instanceof EObject) {
				inputObjects.add((EObject) raw);
			}
			else {
				throw new ExecutionException("Provided input(s) is(are) not valid. Cannot launch transformation.");
			}
		}
		
		ITransformation t = ModelTransformatorPlugin.getService().getTransformation(transformation);
		IStatus res = null;
		try {
			res = ModelTransformatorPlugin.getService().executeTransformation(inputObjects, transformation, t.getEngine(), outputpath);
		} catch (Exception e) {
			throw new ExecutionException("An exception raised while executing the transformation.", e);
		} 
		
		if (res != null) {
			if (res.isOK()) {
				MessageDialog.openInformation(new Shell(), "Transformation Completed", "The transformation has been successfully executed by the engine.");
			}
			else {
				MessageDialog.openError(new Shell(), "Transformation Failes", "The transformation engine reported an error during the execution of the transformation.");
			}
		}
		
		return null;
	}

}