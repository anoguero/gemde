package es.esi.gemde.methodmanager.service.handlers;

import java.io.File;
import java.util.Vector;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.widgets.Shell;

import es.esi.gemde.methodmanager.CommandConstants;
import es.esi.gemde.methodmanager.service.impl.CommandParametersRegistry;
import es.esi.gemde.methodmanager.service.ui.SelectEObjectDialog;

public class SelectElementFromFileCommandHandler extends AbstractHandler {
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		String modelfile = event.getParameter(CommandConstants.MODELFILE_PARAM_ID);
		String addToList = event.getParameter(CommandConstants.ADDTOLIST_PARAM_ID);
		String listObjID = event.getParameter(CommandConstants.LIST_PARAM_ID);
		
		if (modelfile == null) {
			throw new ExecutionException("No model file was provided.");
		}
		
		File f = new File(modelfile);
		if (!f.exists() || !f.isFile()) {
			throw new ExecutionException("Provided model file parameter is not valid");
		}
		
		ResourceSet rs = new ResourceSetImpl();
		Resource r = rs.getResource(URI.createFileURI(modelfile), true);
		
		EObject root = r.getContents().get(0);
		
		SelectEObjectDialog d = new SelectEObjectDialog(new Shell(), root);
		if (d.open() == Dialog.OK) {
			EObject selection = d.getSelectedEObject();
			
			if (addToList != null && addToList.equals("true")) {
				Object listObj = CommandParametersRegistry.instance().getParam(listObjID);
				if (listObj == null || !(listObj instanceof Vector<?>)) {
					Vector<EObject> list = new Vector<EObject>();
					list.add(selection);
					String objString = list.toString().indexOf(' ') == -1 ? list.toString() : list.toString().substring(0, list.toString().indexOf(' '));
					CommandParametersRegistry.instance().putParam(objString, list);
					return objString;
				}
				else {
					@SuppressWarnings("unchecked")
					Vector<EObject> list = (Vector<EObject>) listObj;
					list.add(selection);
					String objString = list.toString().indexOf(' ') == -1 ? list.toString() : list.toString().substring(0, list.toString().indexOf(' '));
					CommandParametersRegistry.instance().putParam(objString, list);
					return objString;
				}
			}
			else {
				String objString = selection.toString().indexOf(' ') == -1 ? selection.toString() : selection.toString().substring(0, selection.toString().indexOf(' '));
				CommandParametersRegistry.instance().putParam(objString, selection);
				return objString;
			}
		}
		
		return null;
	}

}