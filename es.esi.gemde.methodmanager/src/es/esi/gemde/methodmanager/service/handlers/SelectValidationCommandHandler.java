package es.esi.gemde.methodmanager.service.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.widgets.Shell;

import es.esi.gemde.modelvalidator.ui.dialogs.SelectGEMDEValidationDialog;

public class SelectValidationCommandHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		SelectGEMDEValidationDialog dialog = new SelectGEMDEValidationDialog(new Shell());
		if (dialog.open() == Dialog.OK) {
			return dialog.getSelectedValidation().getName();
		}
		return null;
	}

}
