package es.esi.gemde.methodmanager.service.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.widgets.Shell;

import es.esi.gemde.modeltransformator.ui.dialogs.SelectGEMDETransformationDialog;

public class SelectTransformationCommandHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		SelectGEMDETransformationDialog dialog = new SelectGEMDETransformationDialog(new Shell());
		if (dialog.open() == Dialog.OK) {
			return dialog.getSelectedTransformation().getName();
		}
		return null;
	}

}
