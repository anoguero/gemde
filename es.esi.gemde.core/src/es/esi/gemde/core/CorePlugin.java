package es.esi.gemde.core;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import es.esi.gemde.core.resources.OverlaidImageDescriptor;

/**
 * The activator class controls the plug-in life cycle
 */
public class CorePlugin extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "es.esi.gemde.core";

	// The shared instance
	private static CorePlugin plugin;
	
	/**
	 * The constructor
	 */
	public CorePlugin() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static CorePlugin getDefault() {
		return plugin;
	}
	
	// Overlay position constants
	public static final int TOP_RIGHT = 0;
	public static final int TOP_LEFT = 1;
	public static final int BOTTOM_RIGHT = 2;
	public static final int BOTTOM_LEFT = 3;
	
	// ICON Constants
	public static final String CONSTRAINT_ICON = "icons/Constraint.gif";

	public static final String CATEGORY_ICON = "icons/category_obj.gif";

	public static final String VALIDATION_ICON = "icons/validate.gif";

	public static final String ERROR_OVERLAY = "icons/error_ovr.gif";

	public static final String INFO_OVERLAY = "icons/info_ovr.gif";

	public static final String WARNING_OVERLAY = "icons/warn_ovr.gif";

	public static final String NEW_OVERLAY = "icons/new_ovr.gif";

	public static final String LOCK_OVERLAY = "icons/lock_ovr.gif";
	
	public static final String NET_OVERLAY = "icons/net_ovr.png";

	public static final String CONSTRAINT_REPOSITORY_ICON = "icons/oclexample_obj.gif";

	public static final String VALIDATION_REPOSITORY_ICON = "icons/tasks_tsk.gif";

	public static final String METAMODEL_ICON = "icons/ecore.gif";

	public static final String SAVE_ICON = "icons/save.gif";

	public static final String EXPORT_ICON = "icons/export.gif";

	public static final String TRANSFORMATION_REPOSITORY_ICON = "icons/transformation_repository.gif";

	public static final String TRANSFORMATION_ICON = "icons/transformation.gif";

	public static final String INPUT_ICON = "icons/input.gif";

	public static final String EXEC_TRANSFORMATION_ICON = "icons/exec_transform.gif";
	
	public static final String CONSTANT_PARAM_ICON = "icons/ConstantCommandParameter.gif";
	
	public static final String VARIABLE_PARAM_ICON = "icons/VariableCommandParameter.gif";

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

	/**
	 * This method obtains an icon image registered in this plug-in. The icons
	 * are described by the constant strings statically defined in this class.
	 * Additionally, this methods allows specifying an image to overlay to the
	 * current one, and the position where this image should be overlaid.
	 * These parameters must also be specified using the constants defined in 
	 * this class.
	 * 
	 * @param icon the constant String defining the icon to be loaded.
	 * @param overlay the constant String defining the icon to be overlaid
	 * @param overlayPosition the constant defining the position at which the 
	 * overlay image will be drawn, namely TOP_RIGHT, TOP_LEFT, 
	 * BOTTOM_RIGHT or BOTTOM_LEFT.
	 * @return the generated Image, or null if the image couldn't be read or 
	 * an error occurred.
	 */
	public static Image getImage(String icon, String overlay, int overlayPosition) {
		if (getDefault() != null) {
			// Using the registry for performance
			ImageRegistry registry = getDefault().getImageRegistry();

			Image iconImage = registry.get(icon);
			if (iconImage == null) {
				// Hasn't been inserted in the registry yet
				iconImage = getImageDescriptor(icon).createImage();
				if (iconImage == null) {
					return null;
				}
				registry.put(icon, iconImage);
			}
			if (overlay == null) {
				return iconImage;
			}
			Image overlayedImage = registry.get(overlay);
			if (overlayedImage == null) {
				// Hasn't been inserted in the registry yet
				overlayedImage = getImageDescriptor(overlay).createImage();
				if (overlayedImage == null) {
					return null;
				}
				registry.put(overlay, overlayedImage);
			}

			OverlaidImageDescriptor composite = new OverlaidImageDescriptor(iconImage, overlayedImage, overlayPosition);
			return composite.getImage();

		}
		else {
			// If the plugin is not initialized we access the images
			// using the getImageDescriptor() method only.
			Image iconImage = getImageDescriptor(icon).createImage();

			if (iconImage == null) {
				return null;
			}

			if (overlay == null) {
				return iconImage;
			}

			Image overlayedImage = getImageDescriptor(overlay).createImage();

			if (overlayedImage == null) {
				return null;
			}

			OverlaidImageDescriptor composite = new OverlaidImageDescriptor(iconImage, overlayedImage, overlayPosition);
			return composite.getImage();

		}

	}


	/**
	 * This method obtains an icon image registered in this plug-in. The icons
	 * are described by the constant strings statically defined in this class.
	 * 
	 * This method is equivalent to: ModelValidatorUI.getImage(icon, null, 0);
	 * 
	 * @param icon the constant String defining the icon to be loaded.
	 * @return the generated Image, or null if the image couldn't be read or 
	 * an error occurred.
	 */
	public static Image getImage(String icon) {
		return getImage(icon, null, 0);
	}

}
