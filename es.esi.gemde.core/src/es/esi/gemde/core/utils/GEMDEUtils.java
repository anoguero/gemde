package es.esi.gemde.core.utils;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;

public abstract class GEMDEUtils {

	/**
	 * Converts a {@link URI} to a {@link File} is possible.
	 * 
	 * @param uri the URI to be converted. Must represent a file.
	 * @return the file represented by the URI or null.
	 */
	public static final File uri2file(URI uri) {
		File f = null;
		try {
		  f = new File(uri);
		} 
		catch(IllegalArgumentException e) {
			try {
				f = new File(uri.toURL().getPath());
			} catch (MalformedURLException e1) {
				return null;
			}
		}
		return f;
	}
	
	/**
	 * This method creates a directory in the workspace for temporary data storage.
	 * 
	 * @param pluginID the ID of the plugin to generate the temporary directory.
	 * @return the {@link File} representing the directory.
	 */
	public static File createDir (String pluginID) {
		// We store the file to a temporary folder in the workspace metadata region
		IPath wsPath = ResourcesPlugin.getWorkspace().getRoot().getLocation();
		
		// Create the directories if needed
		File dir = wsPath.append(".metadata/.plugins").toFile();
		if (!dir.exists() || !dir.isDirectory()) {
			dir.mkdir();
		}
		
		dir = wsPath.append(".metadata/.plugins/" + pluginID).toFile();
		if (!dir.exists() || !dir.isDirectory()) {
			dir.mkdir();
		}
		
		return dir;
	}
	
	/**
	 * This method empties the temporal directory of the plugin.
	 * 
	 *  @param pluginID the ID of the PLUGIN whose temp directory will be cleaned.
	 */
	public static void cleanupDir (String pluginID) {
		// We store the file to a temporary folder in the workspace metadata region
		IPath wsPath = ResourcesPlugin.getWorkspace().getRoot().getLocation();
		
		// Go to the temporal ATL Engine tool directory
		File dir = wsPath.append(".metadata/.plugins/" + pluginID).toFile();
		if (dir.exists() && dir.isDirectory()) {
			for (File f : dir.listFiles()) {
				f.delete();
			}
		}
	}
	
	/**
	 * Refresh the Eclipse worspace
	 */
	public static void refreshWorkspace() {
		try {
			ResourcesPlugin.getWorkspace().getRoot().refreshLocal(IResource.DEPTH_INFINITE, null);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

	public static boolean isFileInWorkspace(File f) {
		String wsr = ResourcesPlugin.getWorkspace().getRoot().getLocation().toOSString();
		return f.getAbsolutePath().startsWith(wsr);
	}
	
	public static IResource toWorkspaceFile(File f) {
		if (!isFileInWorkspace(f)) {
			return null;
		}
		
		String wsr = ResourcesPlugin.getWorkspace().getRoot().getLocation().toOSString();
		String relPath2File = f.getAbsolutePath().replace(wsr, "");
		
		if (f.isDirectory()) {
			return ResourcesPlugin.getWorkspace().getRoot().getFolder(new Path(relPath2File));
		}
		else {
			return ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(relPath2File));
		}
	}
	
}
