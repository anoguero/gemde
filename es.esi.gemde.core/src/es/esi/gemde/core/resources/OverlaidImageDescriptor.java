/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/

package es.esi.gemde.core.resources;

import org.eclipse.jface.resource.CompositeImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Point;

import es.esi.gemde.core.CorePlugin;


/**
 * This class refines the {@link CompositeImageDescriptor} class to create
 * the overlaid icons required by the {@link es.esi.gemde.modelvalidator.ModelValidatorPlugin} class.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class OverlaidImageDescriptor extends CompositeImageDescriptor {

	private Image base;
	private Image overlay;
	private int pos;
	private Point size;
	
	/**
	 * A constructor defining the base image, the overlaid image and the 
	 * position, with respect to the base image, at which the overlaid
	 * image should be drawn. 
	 *
	 *@param base the base image. Shouldn't be null.
	 *@param overlay the overlaid image. Shouldn't be null.
	 *@param pos an integer defining the relative overlay position (as defined in {@link ModelValidatorPlugin})
	 *@see ModelValidatorPlugin
	 */
	public OverlaidImageDescriptor(Image base, Image overlay, int pos) {
		this.base = base;
		this.overlay = overlay;
		this.pos = pos;
		size = new Point(base.getBounds().width, base.getBounds().height);
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.resource.CompositeImageDescriptor#drawCompositeImage(int, int)
	 */
	@Override
	protected void drawCompositeImage(int width, int height) {
		drawImage(base.getImageData(), 0, 0);
		ImageData imageData = overlay.getImageData();
		switch (pos) {
		case CorePlugin.TOP_LEFT:
			drawImage(imageData, 0, 0);
			break;
		case CorePlugin.TOP_RIGHT:
			drawImage(imageData, size.x - imageData.width, 0);
			break;
		case CorePlugin.BOTTOM_LEFT:
			drawImage(imageData, 0, size.y - imageData.height);
			break;
		case CorePlugin.BOTTOM_RIGHT:
			drawImage(imageData, size.x - imageData.width, size.y - imageData.height);
			break;
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.resource.CompositeImageDescriptor#getSize()
	 */
	@Override
	protected Point getSize() {
		return size;
	}
	
	/**
	 * Get the image formed by overlaying different images on the base image
	 * 
	 * @return composite image
	 */
	public Image getImage() {
		return createImage();
	}
	
}