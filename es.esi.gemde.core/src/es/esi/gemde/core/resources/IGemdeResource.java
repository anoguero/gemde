package es.esi.gemde.core.resources;

/**
 * Interface used by elements that can be stored inside GEMDE repositories
 * 
 * @author adrian.noguero@tecnalia.com
 *
 */
public interface IGemdeResource {
	/**
	 * GEMDE Resource type, used to identify the source of the element in the repository
	 * 
	 * @author adrian.noguero@tecnalia.com
	 *
	 */
	enum ResourceType {
		NORMAL,
		PROTECTED,
		NETWORKED
	}
	
	/**
	 * Return the type of the current repository element object
	 * 
	 * @return the type instance
	 */
	public ResourceType getResourceType();
}
