/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/

package es.esi.gemde.core.resources;

import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;


/**
 * A refined version of the {@link WizardDialog} class that allows specifying the icon to be shown in the wizard window.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class ExtendedWizardDialog extends WizardDialog {

	private Image icon = null;
	
	/**
	 * Constructor with no icon definition.
	 *
	 *@param parentShell the parent shell.
	 *@param newWizard the IWizard instance to be shown.
	 */
	public ExtendedWizardDialog(Shell parentShell, IWizard newWizard) {
		super(parentShell, newWizard);
	}
	
	/**
	 * Constructor with an icon definition.
	 *
	 *@param parentShell the parent shell.
	 *@param newWizard the IWizard instance to be shown.
	 *@param icon the icon Image to be shown in the wizard window.
	 */
	public ExtendedWizardDialog(Shell parentShell, IWizard newWizard, Image icon) {
		super(parentShell, newWizard);
		if (icon != null) {
			this.icon = icon;
		}
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.WizardDialog#configureShell(org.eclipse.swt.widgets.Shell)
	 */
	@Override
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		if (icon != null) {
			newShell.setImage(icon);
		}
	}

}
