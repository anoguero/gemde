/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer.ui.wizards.resources;

import java.util.Vector;

import es.esi.gemde.vv.modelanalyzer.service.IAnalysisTool;
import es.esi.gemde.vv.modelanalyzer.service.IToolOperation;

/**
 * Class that implements the set of intermediate AnalysisWizardPage implementations
 * of the GEMDE ModelAnalysisWizard.
 * Must be implemented by Methodology Adapters.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public abstract class AnalysisWizardPageSet {

	protected Vector<AnalysisWizardPage> set;
	protected Object [] params;
	protected IAnalysisTool tool;
	protected IToolOperation op;
	
	/**
	 * Constructor
	 */
	public AnalysisWizardPageSet() {
		set = new Vector<AnalysisWizardPage>();
	}
	
	/**
	 * Constructor of the class that initializes the size of the page set.
	 *
	 *@param size initial size of the pages set.
	 */
	public AnalysisWizardPageSet(int size) {
		this();
		set.setSize(size);
	}
	
	/**
	 * Setter of the tool parameter.
	 * 
	 * @param tool the implementation of the tool selected by the user.
	 */
	public void setTool(IAnalysisTool tool) {
		this.tool = tool;
	}
	
	/**
	 * Setter of the operation parameter.
	 * 
	 * @param op the implementation of the operation selected by the user.
	 */
	public void setOperation(IToolOperation op) {
		this.op = op;
	}
	
	/**
	 * This method returns the next page of the set taking the current one as parameter.
	 * 
	 * @param current the current page of the set.
	 * @return the next page of the set or null if no more pages are available.
	 */
	public AnalysisWizardPage getNextPage(AnalysisWizardPage current) {
		current.used = true;
		int pos = set.indexOf(current);
		if ((pos+1) < set.size()) {
			
			if (set.get(pos+1) == null || set.get(pos+1).isUsed()) {
				updateNextPageInstance(current);
			}
			
			return set.get(pos+1);
		}
		else {
			return null;
		}
	}
	
	/**
	 * This method returns the previous page of the set taking the current one as parameter.
	 * 
	 * @param current the current page of the set.
	 * @return the previous page of the set or null if the current page is the first one
	 */
	public AnalysisWizardPage getPreviousPage(AnalysisWizardPage current) {
		current.used = true;
		int pos = set.indexOf(current);
		if (pos > 0) {
			return set.get(pos-1);
		}
		else {
			return null;
		}
	}
	
	/**
	 * This method returns the first page of the set. It creates one if necessary.
	 * 
	 * @return the first page of the set.
	 */
	public AnalysisWizardPage getFirstPage () {
		if (set.get(0) == null || set.get(0).isUsed()) {
			return createFirstPage();
		}
		return set.firstElement();
	}
	
	/**
	 * Returns the last page of this set.
	 * 
	 * @return the last page of the set.
	 */
	public AnalysisWizardPage getLastPage () {
		return set.get(set.size()-1); 
	}
	
	/**
	 * Returns the gathered parameters for the analysis tool.
	 * 
	 * @return the gathered parameters as an array of Object or null if no parameters have been set.
	 */
	public Object [] getParams() {
		return params;
	}
	
	/**
	 * This method must update the instance of the next page of the set.
	 * Must be implemented by subclasses.
	 * 
	 * @param current the instance of the current page of the set.
	 */
	protected abstract void updateNextPageInstance (AnalysisWizardPage current);
	
	/**
	 * This method must create the instance of the first page of the set.
	 * Must be implemented by subclasses.
	 * 
	 * @return the created instance of the first page of the set.
	 */
	protected abstract AnalysisWizardPage createFirstPage();
	
}
