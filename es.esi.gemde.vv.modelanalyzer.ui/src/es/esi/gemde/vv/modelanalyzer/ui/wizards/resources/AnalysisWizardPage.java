/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer.ui.wizards.resources;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.WizardPage;

/**
 * Extension of the WizardPage class for the analysis wizard that stores the position of each page.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public abstract class AnalysisWizardPage extends WizardPage {

	protected boolean used; 

	/**
	 * Constructor from superclass
	 *
	 *@param pageName
	 *@param title
	 *@param titleImage
	 */
	public AnalysisWizardPage(String pageName, String title,
			ImageDescriptor titleImage) {
		super(pageName, title, titleImage);
		used = false;
	}

	/**
	 * Constructor from superclass
	 *
	 *@param pageName
	 */
	public AnalysisWizardPage(String pageName) {
		super(pageName);
		used = false;
	}
	
	/**
	 * Set whether this page has been already used or not.
	 * 
	 * @param used
	 */
	public void setUsed(boolean used) {
		this.used = used;
	}
	
	/**
	 * Returns the value of the used field.
	 * 
	 * @return current value of the used field.
	 */
	public boolean isUsed() {
		return used;
	}
	
}
