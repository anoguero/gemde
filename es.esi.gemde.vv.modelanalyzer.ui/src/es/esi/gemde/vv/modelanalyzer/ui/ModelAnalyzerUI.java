/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer.ui;

import java.util.Vector;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import es.esi.gemde.core.resources.OverlaidImageDescriptor;
import es.esi.gemde.vv.modelanalyzer.ui.resources.UIMethodAdapter;
import es.esi.gemde.vv.modelanalyzer.ui.wizards.resources.AnalysisWizardPageSet;

/**
 * The activator class controls the plug-in life cycle
 */
public class ModelAnalyzerUI extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "es.esi.gemde.vv.modelanalyzer.ui";

	// The shared instance
	private static ModelAnalyzerUI plugin;
	
	// The Methodology Adapter extensioin point ID
	private static final String METHOD_ADAPTER_EXTENSION_ID = "es.esi.gemde.vv.modelanalyzer.ui.methodologyadapter";
	
	// ICON CONSTANTS
	public static final String RUN_ANALYSIS_ICON = "icons/analyze.gif";
	
	// The registered methodology adapters
	private static Vector<UIMethodAdapter> adapters = new Vector<UIMethodAdapter>();
	
	/**
	 * The constructor
	 */
	public ModelAnalyzerUI() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		
		loadAdapterExtensions();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static ModelAnalyzerUI getDefault() {
		return plugin;
	}
	
	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
	
	/**
	 * This method obtains an icon image registered in this plug-in. The icons
	 * are described by the constant strings statically defined in this class.
	 * Additionally, this methods allows specifying an image to overlay to the
	 * current one, and the position where this image should be overlaid.
	 * These parameters must also be specified using the constants defined in 
	 * this class.
	 * 
	 * @param icon the constant String defining the icon to be loaded.
	 * @param overlay the constant String defining the icon to be overlaid
	 * @param overlayPosition the constant defining the position at which the 
	 * overlay image will be drawn, namely TOP_RIGHT, TOP_LEFT, 
	 * BOTTOM_RIGHT or BOTTOM_LEFT.
	 * @return the generated Image, or null if the image couldn't be read or 
	 * an error occurred.
	 */
	public static Image getImage(String icon, String overlay, int overlayPosition) {
		if (getDefault() != null) {
			// Using the registry for performance
			ImageRegistry registry = getDefault().getImageRegistry();
	
			Image iconImage = registry.get(icon);
			if (iconImage == null) {
				// Hasn't been inserted in the registry yet
				iconImage = getImageDescriptor(icon).createImage();
				if (iconImage == null) {
					return null;
				}
				registry.put(icon, iconImage);
			}
			if (overlay == null) {
				return iconImage;
			}
			Image overlayedImage = registry.get(overlay);
			if (overlayedImage == null) {
				// Hasn't been inserted in the registry yet
				overlayedImage = getImageDescriptor(overlay).createImage();
				if (overlayedImage == null) {
					return null;
				}
				registry.put(overlay, overlayedImage);
			}
			
			OverlaidImageDescriptor composite = new OverlaidImageDescriptor(iconImage, overlayedImage, overlayPosition);
			return composite.getImage();
			
		}
		else {
			// If the plugin is not initialized we access the images
			// using the getImageDescriptor() method only.
			Image iconImage = getImageDescriptor(icon).createImage();
			
			if (iconImage == null) {
				return null;
			}
			
			if (overlay == null) {
				return iconImage;
			}
			
			Image overlayedImage = getImageDescriptor(overlay).createImage();
			
			if (overlayedImage == null) {
				return null;
			}
			
			OverlaidImageDescriptor composite = new OverlaidImageDescriptor(iconImage, overlayedImage, overlayPosition);
			return composite.getImage();
			
		}
	
	}
	
	/**
	 * This method obtains an icon image registered in this plug-in. The icons
	 * are described by the constant strings statically defined in this class.
	 * 
	 * This method is equivalent to: ModelTransformatorUI.getImage(icon, null, 0);
	 * 
	 * @param icon the constant String defining the icon to be loaded.
	 * @return the generated Image, or null if the image couldn't be read or 
	 * an error occurred.
	 */
	public static Image getImage(String icon) {
		return getImage(icon, null, 0);
	}
	
	/**
	 * Returns the current list of methodology adapters.
	 * 
	 * @return a list containing the methodology adapters contributed by plugins.
	 */
	public static UIMethodAdapter [] getAdapters() {
		return adapters.toArray(new UIMethodAdapter[0]);
	}

	/**
	 * Internal method that loads the contributed methodology adapters to GEMDE.
	 */
	private void loadAdapterExtensions() {
		// Get the elements contributing in the extension point
		IConfigurationElement[] config = Platform.getExtensionRegistry().getConfigurationElementsFor(METHOD_ADAPTER_EXTENSION_ID);
		
		for (IConfigurationElement adapter : config) {
			try {
				// Get the attributes of the extension and check them.
				String tool = adapter.getAttribute("adapted_tool_name");
				String method = adapter.getAttribute("method");
				final Object pagesSet = adapter.createExecutableExtension("class");
				if (pagesSet instanceof AnalysisWizardPageSet) {
					adapters.add(new UIMethodAdapter(tool, method, (AnalysisWizardPageSet) pagesSet));
				}
				else {
					System.err.println("Couldn't load the methodology adapter: " + adapter.getContributor().getName());
				}
				
				
			}
			catch (Exception e) {
				e.printStackTrace();
				System.err.println("Couldn't load the methodology adapter: " + adapter.getContributor().getName());
			}
		}	
			
	}

}
