/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer.ui.wizards;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import es.esi.gemde.methodmanager.MethodManagerPlugin;
import es.esi.gemde.vv.modelanalyzer.ModelAnalyzerPlugin;
import es.esi.gemde.vv.modelanalyzer.exceptions.NoSuchOperationException;
import es.esi.gemde.vv.modelanalyzer.exceptions.NoSuchToolException;
import es.esi.gemde.vv.modelanalyzer.exceptions.ToolException;
import es.esi.gemde.vv.modelanalyzer.service.IAnalysisResult;
import es.esi.gemde.vv.modelanalyzer.service.IAnalysisTool;
import es.esi.gemde.vv.modelanalyzer.service.IResultRecord;
import es.esi.gemde.vv.modelanalyzer.service.IResultType;
import es.esi.gemde.vv.modelanalyzer.service.IToolOperation;
import es.esi.gemde.vv.modelanalyzer.ui.ModelAnalyzerUI;
import es.esi.gemde.vv.modelanalyzer.ui.resources.UIMethodAdapter;
import es.esi.gemde.vv.modelanalyzer.ui.wizards.resources.AnalysisWizardPage;
import es.esi.gemde.vv.modelanalyzer.ui.wizards.resources.AnalysisWizardPageSet;

/**
 * Wizard implementation used to define an analysis operation in GEMDE.
 * The same wizard can be used to perform different analysis operations
 * on several models.
 * This wizard also shows the results of each analysis operation.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class ModelAnalysisWizard extends Wizard {

	private boolean canFinish;
	private String tech;
	private IAnalysisTool tool;
	private IToolOperation operation;
	private Object [] params;
	private IWizardPage firstPage;
	private IWizardPage lastPage;
	private AnalysisWizardPageSet pagesSet;
	private HashMap<String, IWizardPage> map;
	private IAnalysisResult results;
	private boolean needsAdaptation;
	private UIMethodAdapter requiredAdapter;
	
	/**
	 * Default constructor of the wizard
	 *
	 */
	public ModelAnalysisWizard() {
		super();
		setWindowTitle("Run an analysis");
		canFinish = false;
		tool = null;
		operation = null;
		params = null;
		map = new HashMap<String, IWizardPage>();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#performFinish()
	 */
	@Override
	public boolean performFinish() {
		// TODO Auto-generated method stub
		return false;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#needsPreviousAndNextButtons()
	 */
	@Override
	public boolean needsPreviousAndNextButtons() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#addPages()
	 */
	@Override
	public void addPages() {
		IWizardPage page = new SelectAnalysisToolPage();
		addPage(page);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#getStartingPage()
	 */
	@Override
	public IWizardPage getStartingPage() {
		return firstPage;
	}
	
	/*private IWizardPage getNextNonNullPage(int startIndex) {
		for (int i = startIndex+1; i < ownedPages.size(); i++) {
			if (ownedPages.get(i) != null) {
				return ownedPages.get(i);
			}
		}
		return null;
	}*/

	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#getNextPage(org.eclipse.jface.wizard.IWizardPage)
	 */
	@Override
	public IWizardPage getNextPage(IWizardPage page) {
		
		((AnalysisWizardPage)page).setUsed(true);
		if (page == firstPage) {
			// Create the pages set!!
			if (needsAdaptation) {
				if (pagesSet == null || pagesSet instanceof DefaultPageSet) {
					if (tool != null && operation != null) {
						pagesSet = requiredAdapter.getPagesSet();
						pagesSet.setTool(tool);
						pagesSet.setOperation(operation);
					}
					else {
						return null;
					}
				}
			}
			else {
				if (pagesSet == null || !(pagesSet instanceof DefaultPageSet)) {
					if (tool != null && operation != null) {
						pagesSet = new DefaultPageSet();
						pagesSet.setTool(tool);
						pagesSet.setOperation(operation);
					}
					else {
						return null;
					}
				}
				
			}
			IWizardPage next = pagesSet.getFirstPage();
			addPage(next);
			return next;
			
		}
		else if (page == lastPage) {
			return null;
		}
		else {
			AnalysisWizardPage next = pagesSet.getNextPage((AnalysisWizardPage) page);
			if (next == null) {
				params = pagesSet.getParams();
				next = new ResultsPage();
				addPage(next);
			}
			else if (next.isUsed()) {
				// Should never happen!!
				System.err.println("Next page in the set returned a used value!!");
			}
			else {
				addPage(next);
			}
			return next;
		}
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#getPreviousPage(org.eclipse.jface.wizard.IWizardPage)
	 */
	@Override
	public IWizardPage getPreviousPage(IWizardPage page) {

		if (page == firstPage) {
			return null;
		}
		else if (page == lastPage) {
			IWizardPage prev = pagesSet.getLastPage();
			return prev;
		}
		else {
			IWizardPage prev = pagesSet.getPreviousPage((AnalysisWizardPage) page);
			if (prev == null) {
				return firstPage;
			}
			return prev;
		}
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#addPage(org.eclipse.jface.wizard.IWizardPage)
	 */
	@Override
	public void addPage(IWizardPage page) {
		
		if (page instanceof SelectAnalysisToolPage) {
			firstPage = page;
		}
		else if (page instanceof ResultsPage) {
			lastPage = page;
		}
		
		if (map.containsKey(page.getName())) {
			map.remove(page.getName());
		}
		
		map.put(page.getName(), page);
		page.setWizard(this);
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#getPage(java.lang.String)
	 */
	@Override
	public IWizardPage getPage(String name) {
		return map.get(name);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#getPageCount()
	 */
	@Override
	public int getPageCount() {
		return map.values().size();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#getPages()
	 */
	@Override
	public IWizardPage[] getPages() {
		return map.values().toArray(new IWizardPage[0]);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.wizard.Wizard#canFinish()
	 */
	@Override
	public boolean canFinish() {
		return canFinish;
	}
	
	/**
	 * Wizard page implementation used to select the analysis category, tool and operation
	 * that will be used for the analysis. 
	 *
	 * @author Adri�n Noguero (adrian.noguero@esi.es)
	 * @version 1.0
	 * @since 1.0
	 *
	 */
	private class SelectAnalysisToolPage extends AnalysisWizardPage {

		private Combo toolCombo;
		private Combo opsCombo;
		private Button adapterCheck;
		
		/**
		 * Constructor of the class
		 *
		 */
		public SelectAnalysisToolPage() {
			super("Select Analysis Tool");
		}
		
		// Listeners definition
		private SelectionListener techListener = new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// Not needed
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				tech = ((Combo)e.widget).getItem(((Combo)e.widget).getSelectionIndex());
				tool = null;
				operation = null;
				
				if (tech != null && !tech.equals("")) {
					toolCombo.removeAll();
					toolCombo.add("");
					
					for (IAnalysisTool tool : ModelAnalyzerPlugin.getService().getToolsInCategory(tech)) {
						toolCombo.add(tool.getName());
					}
					toolCombo.setEnabled(true);
				}
				else {
					toolCombo.removeAll();
					toolCombo.setEnabled(false);
				}
				opsCombo.removeAll();
				opsCombo.setEnabled(false);
				setPageComplete(false);
			}
			
		};
		
		private SelectionListener toolListener = new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// Not needed
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				tool = ModelAnalyzerPlugin.getService().getToolByName(((Combo)e.widget).getItem(((Combo)e.widget).getSelectionIndex()));
				operation = null;
				if (tool != null) {
					opsCombo.removeAll();
					opsCombo.add("");
					
					for (IToolOperation op : tool.getOperations()) {
						opsCombo.add(op.getName());
					}
					
					opsCombo.setEnabled(true);
					
					// Enable the methodology adapter selection button.
					if (MethodManagerPlugin.getService().getActiveMethod() != null) {
						String activeMethod = MethodManagerPlugin.getService().getActiveMethod().getName();
						String toolName = tool.getName();
						boolean found = false;
						for (UIMethodAdapter adapter : ModelAnalyzerUI.getAdapters()) {
							if (adapter.getToolName().equals(toolName) && adapter.getMethod().equals(activeMethod)) {
								found = true;
							}
						}
						adapterCheck.setEnabled(found);
					}
					else {
						adapterCheck.setEnabled(false);
					}
					
				}
				else {
					adapterCheck.setEnabled(false);
					opsCombo.removeAll();
					opsCombo.setEnabled(false);
				}
				setPageComplete(false);
			}
			
		};
		
		private SelectionListener opListener = new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// Not needed
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				operation = tool.getOperationByName(((Combo)e.widget).getItem(((Combo)e.widget).getSelectionIndex()));
				if (operation != null) {
					setPageComplete(true);
				}
				else {
					setPageComplete(false);
				}
			}
			
		};
		
		private SelectionListener adapterButtonListener = new SelectionListener() {
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// Not used
			}

			@Override
			public void widgetSelected(SelectionEvent e) {
				needsAdaptation = ((Button)e.getSource()).getSelection();
				if (needsAdaptation) {
					String methodName = MethodManagerPlugin.getService().getActiveMethod().getName();
					for (UIMethodAdapter adapter : ModelAnalyzerUI.getAdapters()) {
						if (adapter.getMethod().equals(methodName) && adapter.getToolName().equals(tool.getName())) {
							requiredAdapter = adapter;
						}
					}
				}
			}
			
		};

		/* (non-Javadoc)
		 * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
		 */
		@Override
		public void createControl(Composite parent) {
			Composite topPanel = new Composite(parent, SWT.NONE);
			topPanel.setLayout(new GridLayout(6, true));
			
			Label analysisLabel = new Label(topPanel, SWT.NONE);
			GridData analysisLabelData = new GridData(GridData.FILL_HORIZONTAL);
			analysisLabelData.horizontalSpan = 6;
			analysisLabel.setText("Select an analysis technique:");
			analysisLabel.setLayoutData(analysisLabelData);
			
			Combo techCombo = new Combo(topPanel, SWT.BORDER | SWT.READ_ONLY);
			GridData techComboData = new GridData(GridData.FILL_HORIZONTAL);
			techComboData.horizontalSpan = 6;
			techCombo.setLayoutData(techComboData);
			techCombo.add("");
			String [] categories = ModelAnalyzerPlugin.getService().getCategoriesWithTool();
			if (categories != null) {
				for (String tech : categories) {
					techCombo.add(tech);
				}
			}
			techCombo.select(0);
			
			Label toolLabel = new Label(topPanel, SWT.NONE);
			GridData toolLabelData = new GridData(GridData.FILL_HORIZONTAL);
			toolLabelData.horizontalSpan = 6;
			toolLabel.setText("Select an analysis tool:");
			toolLabel.setLayoutData(toolLabelData);
			
			toolCombo = new Combo(topPanel, SWT.BORDER | SWT.READ_ONLY);
			GridData toolComboData = new GridData(GridData.FILL_HORIZONTAL);
			toolComboData.horizontalSpan = 6;
			toolCombo.setLayoutData(toolComboData);
			toolCombo.add("");
			toolCombo.select(0);
			toolCombo.setEnabled(false);
			
			Label opsLabel = new Label(topPanel, SWT.NONE);
			GridData opsLabelData = new GridData(GridData.FILL_HORIZONTAL);
			opsLabelData.horizontalSpan = 6;
			opsLabel.setText("Select a tool operation:");
			opsLabel.setLayoutData(opsLabelData);
			
			opsCombo = new Combo(topPanel, SWT.BORDER | SWT.READ_ONLY);
			GridData opsComboData = new GridData(GridData.FILL_HORIZONTAL);
			opsComboData.horizontalSpan = 6;
			opsCombo.setLayoutData(opsComboData);
			opsCombo.add("");
			opsCombo.select(0);
			opsCombo.setEnabled(false);
			
			
			adapterCheck = new Button(topPanel, SWT.CHECK | SWT.LEFT);
			GridData adapterButData = new GridData(GridData.FILL_HORIZONTAL);
			adapterButData.horizontalSpan = 6;
			adapterCheck.setLayoutData(adapterButData);
			if (MethodManagerPlugin.getService().getActiveMethod() != null) {
				adapterCheck.setText("Activate methodology adaptation for active method (" + MethodManagerPlugin.getService().getActiveMethod().getName() + ")");
				adapterCheck.setEnabled(false);
				adapterCheck.setSelection(false);
			}
			else {
				adapterCheck.setText("No active methods");
				adapterCheck.setEnabled(false);
				adapterCheck.setSelection(false);
			}
			
			// Listeners
			techCombo.addSelectionListener(techListener);
			toolCombo.addSelectionListener(toolListener);
			opsCombo.addSelectionListener(opListener);
			adapterCheck.addSelectionListener(adapterButtonListener);
			
			// Set the description
			setTitle("Select the analysis tool");
			setDescription("Select an analysis technique from those available in GEMDE. Select a tool implementing the selected technique and a concrete operation of the tool.");
			
			// Set the control!!
			setControl(topPanel);
			setPageComplete(false);
			
		}
		
	}
	
	
	/**
	 * Wizard page used to execute the current analysis operation
	 * with the selected parameters and to show the analysis results.  
	 *
	 * @author Adri�n Noguero (adrian.noguero@esi.es)
	 * @version 1.0
	 * @since 1.0
	 *
	 */
	private class ResultsPage extends AnalysisWizardPage {
		
		private Composite resultsPane;
		private Composite topPanel;
		private boolean error = false;
		private Exception exc;
		
		private SelectionListener runListener = new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				// Reset the error flag
				error = false;
				
				// Run the tool. We use a progress dialog to improve the UI ;)
				try {
					ProgressMonitorDialog dialog = new ProgressMonitorDialog(getShell());
					dialog.run(true, false, new IRunnableWithProgress(){ 
						
						@Override
						public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
							try {
								monitor.beginTask("Executing the analysis...", IProgressMonitor.UNKNOWN);
								results = ModelAnalyzerPlugin.getService().runTool(tool.getName(), operation.getName(), params);
							} catch (NoSuchToolException e) {
								// Should never occur
								results = null;
								e.printStackTrace();
							} catch (NoSuchOperationException e) {
								// Should never occur
								results = null;
								e.printStackTrace();
							} catch (ToolException e) {
								results = null;
								error = true;
								exc = e;
							}
							finally {
								monitor.done();
							}
							
						}
					});
				} catch (InvocationTargetException e1) {
					// Not relevant
					e1.printStackTrace();
				} catch (InterruptedException e1) {
					// Not relevant
					e1.printStackTrace();
				}
				
				if (error) {
					MessageDialog.openError(new Shell(), "Tool exception occurred!", "Analysis couldn't be completed because the analysis tool reported an exception:\n" + exc.getMessage().replaceAll("\r", ""));
				}
				
				// First dispose the current result pane and create a new one!
				resultsPane.dispose();
				resultsPane = createResultsPane();
				
				// Force redrawing the composite with the results!!

				// First lay out again the controls of the top panel 
				topPanel.layout(true, true);
				
				// Then update the shell to repaint the new configuration. 
				// No pack() is used to avoid resizing!!
				topPanel.getShell().update();
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// Not used
			}
		};

		/**
		 * Constructor of the class.
		 * 
		 */
		public ResultsPage() {
			super("Analysis Results");
		}

		/* (non-Javadoc)
		 * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
		 */
		@Override
		public void createControl(Composite parent) {
			Composite topPanel = new Composite(parent, SWT.NONE);
			topPanel.setLayout(new GridLayout(6, true));
			
			Label summaryLabel = new Label(topPanel, SWT.NONE);
			GridData summaryLabelData = new GridData(GridData.FILL_HORIZONTAL);
			summaryLabelData.horizontalSpan = 6;
			summaryLabel.setText("Selected analysis technique:");
			summaryLabel.setLayoutData(summaryLabelData);
			
			Label emptyLabel = new Label(topPanel, SWT.NONE);
			GridData emptyLabelData = new GridData(GridData.FILL_HORIZONTAL);
			emptyLabelData.horizontalSpan = 4;
			emptyLabel.setText(operation.getName() + " (" + tool.getName() + ") ->");
			emptyLabel.setLayoutData(emptyLabelData);
			
			Button runButton = new Button(topPanel, SWT.PUSH | SWT.CENTER);
			GridData butData = new GridData(GridData.FILL_HORIZONTAL);
			butData.horizontalSpan = 2;
			runButton.setLayoutData(butData);
			runButton.setText("Run Analysis");
			
			resultsPane = new Composite(topPanel, SWT.NONE);
			GridData resultsPaneData = new GridData(GridData.FILL_BOTH);
			resultsPaneData.horizontalSpan = 6;
			resultsPane.setLayoutData(resultsPaneData);
			
			runButton.addSelectionListener(runListener);
			
			this.topPanel = topPanel;
			
			// Set the description
			setTitle("Analysis Results");
			setDescription("Run the selected analysis and get the results.");
			
			// Set the control!!
			setControl(topPanel);
			setPageComplete(false);
			
			used = true;
		}

		private Composite createResultsPane() {
			// Create a composite that shows the current results
			if (results != null) {
				TabFolder tabbedPane = new TabFolder(topPanel, SWT.BORDER);
				GridData tabbedPaneData = new GridData(GridData.FILL_BOTH);
				tabbedPaneData.horizontalSpan = 6;
				tabbedPane.setLayoutData(tabbedPaneData);
				
				for (IResultType type : results.getResults()) {
					TabItem tab = new TabItem(tabbedPane, SWT.NONE);
					tab.setText(type.getType());
					
					// Create the controls for this tab...
					Table table = new Table(tabbedPane, SWT.MULTI | SWT.BORDER | SWT.FULL_SELECTION);
					
					table.setLinesVisible (true);
					table.setHeaderVisible (true);
					
					TableColumn col1 = new TableColumn(table, SWT.NONE);
					TableColumn col2 = new TableColumn(table, SWT.NONE);
					col1.setText("Target");
					col2.setText("Value");
					
					for (IResultRecord record : type.getRecords()) {
						TableItem row = new TableItem(table, SWT.NONE);
						row.setText(0, record.getTarget());
						row.setText(1, record.getValue().toString());
					}
					
					col1.pack();
					col2.pack();
					
					// Set the table as the control for the current tab
					tab.setControl(table);
				}
				
				tabbedPane.pack();
				return tabbedPane;
			}
			else {
				// If no results are available we return an empty composite
				return new Composite(topPanel, SWT.NONE);
			}
		}
		
	}

}
