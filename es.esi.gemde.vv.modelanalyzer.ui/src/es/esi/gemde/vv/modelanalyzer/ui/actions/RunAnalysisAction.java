/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer.ui.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;

import es.esi.gemde.core.resources.ExtendedWizardDialog;
import es.esi.gemde.vv.modelanalyzer.ModelAnalyzerPlugin;
import es.esi.gemde.vv.modelanalyzer.ui.ModelAnalyzerUI;
import es.esi.gemde.vv.modelanalyzer.ui.wizards.ModelAnalysisWizard;


/**
 * Action that launches the model analysis wizard to perform analysis on models.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class RunAnalysisAction implements IWorkbenchWindowActionDelegate {
	private IWorkbenchWindow window;
	
	/**
	 * The constructor.
	 */
	public RunAnalysisAction() {
	}

	/**
	 * This method just calls the Analysis Wizard
	 * 
	 * @see IWorkbenchWindowActionDelegate#run
	 */
	public void run(IAction action) {
		
		if (ModelAnalyzerPlugin.getService().getCategoriesWithTool() != null) {
			ModelAnalysisWizard wizard = new ModelAnalysisWizard();
			WizardDialog dialog = new ExtendedWizardDialog(window.getShell(), wizard, ModelAnalyzerUI.getImage(ModelAnalyzerUI.RUN_ANALYSIS_ICON));
			
			int result = dialog.open();
			
			if (result == Dialog.OK) {
				
			}
		}
		else {
			MessageDialog.openError(window.getShell(), "No tools registered", "There are no analysis tools currently registered in GEMDE. Analysis will be aborted.");
		}
	}

	/**
	 * Selection in the workbench has been changed. We 
	 * can change the state of the 'real' action here
	 * if we want, but this can only happen after 
	 * the delegate has been created.
	 * @see IWorkbenchWindowActionDelegate#selectionChanged
	 */
	public void selectionChanged(IAction action, ISelection selection) {
	}

	/**
	 * We can use this method to dispose of any system
	 * resources we previously allocated.
	 * @see IWorkbenchWindowActionDelegate#dispose
	 */
	public void dispose() {
	}

	/**
	 * We will cache window object in order to
	 * be able to provide parent shell for the message dialog.
	 * @see IWorkbenchWindowActionDelegate#init
	 */
	public void init(IWorkbenchWindow window) {
		this.window = window;
	}
}