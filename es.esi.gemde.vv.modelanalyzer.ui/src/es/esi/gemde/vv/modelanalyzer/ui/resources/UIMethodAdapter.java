/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer.ui.resources;

import es.esi.gemde.vv.modelanalyzer.ui.wizards.resources.AnalysisWizardPageSet;

/**
 * Convenience class that contains all the information of a tool adapter.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class UIMethodAdapter {
	
	private String adaptedToolName;
	private String methodName;
	private AnalysisWizardPageSet analysisPagesSet;
	
	/**
	 * Constructor of the class
	 *
	 *@param tool the name of the adapted tool.
	 *@param method the name of the method for which this tool is adapted.
	 *@param pagesSet the implementation of the wizard pages set that performs the adaptation.
	 *@throws IllegalArgumentException if any of the provided parameters is null.
	 */
	public UIMethodAdapter (String tool, String method, AnalysisWizardPageSet pagesSet) throws IllegalArgumentException {
		if (tool == null || method == null || pagesSet == null) {
			throw new IllegalArgumentException("Null parameters are not allowed!!");
		}
		adaptedToolName = tool;
		methodName = method;
		analysisPagesSet = pagesSet;
	}
	
	/**
	 * Getter of the tool name.
	 * 
	 * @return the name of the adapted tool.
	 */
	public String getToolName() {
		return adaptedToolName;
	}
	
	/**
	 * Getter of the method name.
	 * 
	 * @return the name of the adapted method.
	 */
	public String getMethod() {
		return methodName;
	}
	
	/**
	 * Getter of the wizard pages set implementation.
	 * 
	 * @return the implementation of the analysis wizard pages set for the adaptation.
	 */
	public AnalysisWizardPageSet getPagesSet() {
		return analysisPagesSet;
	}

}
