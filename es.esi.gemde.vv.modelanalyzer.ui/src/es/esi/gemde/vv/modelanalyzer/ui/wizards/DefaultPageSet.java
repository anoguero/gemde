/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer.ui.wizards;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;

import es.esi.gemde.vv.modelanalyzer.ui.resources.ParametersObtainerComposite;
import es.esi.gemde.vv.modelanalyzer.ui.wizards.resources.AnalysisWizardPage;
import es.esi.gemde.vv.modelanalyzer.ui.wizards.resources.AnalysisWizardPageSet;

/**
 * Default implementation of the AnalysisWizardPageSet class.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class DefaultPageSet extends AnalysisWizardPageSet {
	
	/**
	 * Constructor of the class.
	 *
	 */
	public DefaultPageSet () {
		super(1);
	}
	
	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.ui.wizards.resources.AnalysisWizardPageSet#updatePageInstance(int)
	 */
	@Override
	protected void updateNextPageInstance(AnalysisWizardPage current) {
		// There's no next page... ignoring this call.
	}

	/* (non-Javadoc)
	 * @see es.esi.gemde.vv.modelanalyzer.ui.wizards.resources.AnalysisWizardPageSet#createFirstPage()
	 */
	@Override
	protected AnalysisWizardPage createFirstPage() {
		set.setElementAt(new ParametersSelectionPage(), 0);
		return set.get(0);
	}
	
	/**
	 * Wizard page implementation employed to select the parameters required by
	 * the currently selected tool operation.
	 *
	 * @author Adri�n Noguero (adrian.noguero@esi.es)
	 * @version 1.0
	 * @since 1.0
	 *
	 */
	private class ParametersSelectionPage extends AnalysisWizardPage {

		
		
		/**
		 * Constructor of the class.
		 *
		 */
		public ParametersSelectionPage() {
			super("Select the parameters of the operation");
		}

		/* (non-Javadoc)
		 * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
		 */
		@Override
		public void createControl(Composite parent) {
			
			final ParametersObtainerComposite obtainer = new ParametersObtainerComposite(parent, SWT.NONE, op);
			
			obtainer.addListener(ParametersObtainerComposite.CHANGE_EVENT, new Listener() {
				
				@Override
				public void handleEvent(Event event) {
					params = obtainer.getParameters();
					setPageComplete(obtainer.isCompleted());
				}
			});
			
			// Set the description
			setTitle("Select the input parameters");
			setDescription("Select the input parameters required for the selected analysis technique.");
			
			// Set the control!!
			setControl(obtainer);
			setPageComplete(false);
			
			used = true;
		}
		
	}

}
