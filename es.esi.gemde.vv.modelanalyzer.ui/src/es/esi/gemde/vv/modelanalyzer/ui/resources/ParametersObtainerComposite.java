/*******************************************************************************
 * Copyright (c) 2010 European Software Institute - Tecnalia.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Author - Adri�n Noguero (adrian.noguero@esi.es)
 *     
 *******************************************************************************/
package es.esi.gemde.vv.modelanalyzer.ui.resources;

import java.util.List;
import java.util.Vector;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerSorter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import es.esi.gemde.vv.modelanalyzer.service.IToolOperation;

/**
 * Special composite widget used to capture the parameters of an analysis operation.
 * The composite adapts to the input list from the provided {@link IToolOperation} implementation.
 *
 * @author Adri�n Noguero (adrian.noguero@esi.es)
 * @version 1.0
 * @since 1.0
 *
 */
public class ParametersObtainerComposite extends Composite {

	public static final int CHANGE_EVENT = 7500;
	private IToolOperation op;
	private Vector<Control> ownedControls;
	private Object [] results;
	
	/**
	 * Constructor of the Composite
	 *
	 *@param parent the parent composite
	 *@param style the style values. These are only used in the constructor of the parent class.
	 *@param op the implementation of the {@link IToolOperation} interface this composite will be adapted to.
	 *@throws IllegalArgumentException if a null operation is provided.
	 */
	public ParametersObtainerComposite(Composite parent, int style, IToolOperation op) throws IllegalArgumentException {
		super(parent, style);
		if (op == null) {
			throw new IllegalArgumentException("Cannot create the composite for a null operation");
		}
		this.op = op;
		ownedControls = new Vector<Control>();
		results = new Object[op.getRequiredInputList().size()];
		
		List<Class<?>> inputTypes = op.getRequiredInputList();
		List<String> inputNames = op.getRequiredInputNames();
		
		this.setLayout(new GridLayout(6, true));
		
		for (int i = 0; i < inputTypes.size(); i++) {
			if (inputTypes.get(i).equals(int.class) || inputTypes.get(i).equals(long.class) || inputTypes.get(i).equals(Integer.class) || inputTypes.get(i).equals(Long.class)) {
				Label inputName = new Label(this, SWT.NONE);
				GridData inputLabelData = new GridData();
				inputLabelData.horizontalSpan = 1;
				inputName.setText(inputNames.get(i) + ":");
				inputName.setLayoutData(inputLabelData);
				
				Text input = new Text(this, SWT.BORDER | SWT.LEFT | SWT.SINGLE);
				GridData inputTextData = new GridData(GridData.FILL_HORIZONTAL);
				inputTextData.horizontalSpan = 5;
				input.setLayoutData(inputTextData);
				
				ModifyListener intListener = new ModifyListener () {

					@Override
					public void modifyText(ModifyEvent e) {
						try {
							Integer readVal = Integer.parseInt(((Text)e.widget).getText());
							results[ownedControls.indexOf(((Text)e.widget))] = readVal;
						}
						catch (Exception ex) {
							results[ownedControls.indexOf(((Text)e.widget))] = null;
						}
						notifyListeners(CHANGE_EVENT, new Event());
					}
					
				};
				
				input.addModifyListener(intListener);
				
				ownedControls.add(input);
			}
			else if (inputTypes.get(i).equals(String.class)) {
				Label inputName = new Label(this, SWT.NONE);
				GridData inputLabelData = new GridData();
				inputLabelData.horizontalSpan = 1;
				inputName.setText(inputNames.get(i) + ":");
				inputName.setLayoutData(inputLabelData);
				
				Text input = new Text(this, SWT.BORDER | SWT.LEFT | SWT.SINGLE);
				GridData inputTextData = new GridData(GridData.FILL_HORIZONTAL);
				inputTextData.horizontalSpan = 5;
				input.setLayoutData(inputTextData);
				
				ModifyListener stringListener = new ModifyListener () {

					@Override
					public void modifyText(ModifyEvent e) {
						results[ownedControls.indexOf(((Text)e.widget))] = ((Text)e.widget).getText();
						notifyListeners(CHANGE_EVENT, new Event());
					}
					
				};
				
				input.addModifyListener(stringListener);
				
				ownedControls.add(input);
			}
			else if (inputTypes.get(i).equals(float.class) || inputTypes.get(i).equals(double.class) || inputTypes.get(i).equals(Float.class) || inputTypes.get(i).equals(Double.class)) {
				Label inputName = new Label(this, SWT.NONE);
				GridData inputLabelData = new GridData();
				inputLabelData.horizontalSpan = 1;
				inputName.setText(inputNames.get(i) + ":");
				inputName.setLayoutData(inputLabelData);
				
				Text input = new Text(this, SWT.BORDER | SWT.LEFT | SWT.SINGLE);
				GridData inputTextData = new GridData(GridData.FILL_HORIZONTAL);
				inputTextData.horizontalSpan = 5;
				input.setLayoutData(inputTextData);
				
				ModifyListener floatListener = new ModifyListener () {

					@Override
					public void modifyText(ModifyEvent e) {
						try {
							Float readVal = Float.parseFloat(((Text)e.widget).getText());
							results[ownedControls.indexOf(((Text)e.widget))] = readVal;
						}
						catch (Exception ex) {
							results[ownedControls.indexOf(((Text)e.widget))] = null;
						}
						notifyListeners(CHANGE_EVENT, new Event());
					}
					
				};
				
				input.addModifyListener(floatListener);
				
				ownedControls.add(input);
			}
			else if (inputTypes.get(i).equals(boolean.class) || inputTypes.get(i).equals(Boolean.class)) {
				Label inputName = new Label(this, SWT.NONE);
				GridData inputLabelData = new GridData(GridData.FILL_HORIZONTAL);
				inputLabelData.horizontalSpan = 5;
				inputName.setText(inputNames.get(i) + ":");
				inputName.setLayoutData(inputLabelData);
				
				Button check = new Button(this, SWT.CHECK);
				GridData butData = new GridData();
				butData.horizontalSpan = 1;
				check.setLayoutData(butData);
				check.setText("True");
				
				SelectionListener listener = new SelectionListener() {
					
					@Override
					public void widgetSelected(SelectionEvent e) {
						results[ownedControls.indexOf(((Button)e.widget))] = ((Button)e.widget).getSelection();
						notifyListeners(CHANGE_EVENT, new Event());
					}
					
					@Override
					public void widgetDefaultSelected(SelectionEvent e) {
						// Not used
					}
				};
				
				check.addSelectionListener(listener);
				
				ownedControls.add(check);
				results[ownedControls.indexOf(check)] = check.getSelection();
			}
			else if (EObject.class.isAssignableFrom(inputTypes.get(i))) {
				Label selectModel = new Label(this, SWT.NONE);
				GridData selectLabelData = new GridData();
				selectLabelData.horizontalSpan = 1;
				selectModel.setText("Select Model:");
				selectModel.setLayoutData(selectLabelData);
				
				Text modelPath = new Text(this, SWT.BORDER | SWT.LEFT | SWT.SINGLE);
				GridData pathTextData = new GridData(GridData.FILL_HORIZONTAL);
				pathTextData.horizontalSpan = 4;
				modelPath.setLayoutData(pathTextData);
				modelPath.setEditable(false);
				
				
				Button browse = new Button(this, SWT.PUSH | SWT.CENTER);
				GridData butData = new GridData(GridData.FILL_HORIZONTAL);
				butData.horizontalSpan = 1;
				browse.setLayoutData(butData);
				browse.setText("Browse");
				
				Label modelElement = new Label(this, SWT.NONE);
				GridData elementLabelData = new GridData(GridData.FILL_HORIZONTAL);
				elementLabelData.horizontalSpan = 6;
				modelElement.setText("Select " + inputNames.get(i) + " Element:");
				modelElement.setLayoutData(elementLabelData);
				
				TreeViewer modelViewer = new TreeViewer(this, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
				GridData modelViewerData = new GridData(GridData.FILL_BOTH);
				modelViewerData.horizontalSpan = 6;
				modelViewer.getTree().setLayoutData(modelViewerData);

				ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);
				modelViewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
				modelViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
				//modelViewer.addFilter(modelFilter);
				modelViewer.setSorter(new ViewerSorter());
				
				MyBrowseListener buttonListener = new MyBrowseListener(modelPath, modelViewer);
				browse.addSelectionListener(buttonListener);
				
				ISelectionChangedListener selectObjectListener = new ISelectionChangedListener() {

					@Override
					public void selectionChanged(SelectionChangedEvent e) {
						Object selectedElement = ((StructuredSelection)e.getSelection()).getFirstElement();
						if (selectedElement instanceof EObject) {
							results[ownedControls.indexOf(((TreeViewer)e.getSource()).getTree())] = selectedElement;
						}
						else {
							results[ownedControls.indexOf(((TreeViewer)e.getSource()).getTree())] = null;
						}
						notifyListeners(CHANGE_EVENT, new Event());
					}

				};
				
				modelViewer.addSelectionChangedListener(selectObjectListener);
				
				ownedControls.add(modelViewer.getTree());
			}
		}
	}

	/**
	 * Checks whether all the parameters have been provided for the current operation.
	 * 
	 * @return true if all the parameters have been correctly provided. False otherwise.
	 */
	public boolean isCompleted() {
		return op.checkInputs(results);
	}
	
	/**
	 * Returns the parameters list read from the composite.
	 * 
	 * @return the parameters list returned as an array.
	 */
	public Object[] getParameters() {
		return results;
	}
	
	/**
	 * Special Selection Listener implementation used for the TreeViewer in this composite.
	 *
	 * @author Adri�n Noguero (adrian.noguero@esi.es)
	 * @version 1.0
	 * @since 1.0
	 *
	 */
	private class MyBrowseListener implements SelectionListener {
		
		private TreeViewer viewer;
		private Text path;
		
		/**
		 * Constructor of the listener with a Text and a TreeViewer elements.
		 *
		 *@param modelPath
		 *@param modelViewer
		 */
		public MyBrowseListener(Text modelPath, TreeViewer modelViewer) {
			path = modelPath;
			viewer = modelViewer;
		}

		@Override
		public void widgetDefaultSelected(SelectionEvent e) {
			// Not needed
		}

		@Override
		public void widgetSelected(SelectionEvent e) {
			FileDialog dialog = new FileDialog(new Shell());
			dialog.setText("Select a model file");
			String strPath = dialog.open();
			if (strPath != null) {
				// Load the model from the file
				ResourceSet rs = new ResourceSetImpl();
				URI modelURI = URI.createFileURI(strPath);
				Resource resource = null;
				try {
					resource = rs.getResource(modelURI, true);
					path.setText(strPath);
					viewer.setInput(null);
					viewer.setInput(resource);
				}
				catch (Exception ex) {
					MessageDialog.openError(getShell(), "Not valid model file", "The provided model file couldn't be parsed as an EMF resource");
					path.setText("");
					viewer.setInput(null);
				}
			}
		}
	}

	

}
